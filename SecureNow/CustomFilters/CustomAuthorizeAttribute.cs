﻿using SecureNow.Container;
using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PHIncentiveApp.CustomFilters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private string roleName;

        DBModels db = new DBModels();

        public CustomAuthorizeAttribute(string inputRoleName)
        {
            
            roleName = inputRoleName;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool retVal = false;
           
            if (!base.AuthorizeCore(httpContext))
            {
                retVal =  false;
            }
            else
            {
                //check the role of the user and whether it matches the 
               
                string userName = HttpContext.Current.User.Identity.Name;
                EmployeeDetailsDTO userValid = (from d in db.employeeBaseDetails
                                                      join e in db.employeeRole on d.adpId equals e.adpId
                                                      where (d.email.Equals(userName) 
                                                      && e.role.Equals(roleName))
                                                      select new EmployeeDetailsDTO
                                                      {
                                                          role = e.role
                                                      }).FirstOrDefault();
                // bool userValid = db.EmployeeBaseDetailsModel.Any(user => user.email == userName && user.systemRole == roleName);
                if (userValid != null)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }

            return retVal;
        }


    }
}