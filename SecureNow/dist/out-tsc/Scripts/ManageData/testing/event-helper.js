"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventHelper = {
    raiseClick: raiseClick,
    raiseInput: raiseInput,
    raiseKeydown: raiseKeydown,
    raiseKeyup: raiseKeyup,
    raise: raise
};
function raiseClick(element) {
    element.click();
}
function raiseInput(input, value) {
    input.value = value;
    input.dispatchEvent(new Event('input'));
}
function raiseKeydown(input, code) {
    input.dispatchEvent(new KeyboardEvent('keydown', {
        bubbles: true,
        cancelable: true,
        code: code
    }));
}
function raiseKeyup(input, code) {
    input.dispatchEvent(new KeyboardEvent('keyup', {
        bubbles: true,
        cancelable: true,
        code: code
    }));
}
function raise(element, event) {
    element.dispatchEvent(event);
}
//# sourceMappingURL=event-helper.js.map