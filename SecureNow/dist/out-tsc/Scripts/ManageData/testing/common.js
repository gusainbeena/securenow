"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
function createGenericTestComponent(html, type) {
    testing_1.TestBed.overrideComponent(type, { set: { template: html } });
    var fixture = testing_1.TestBed.createComponent(type);
    fixture.detectChanges();
    return fixture;
}
exports.createGenericTestComponent = createGenericTestComponent;
//# sourceMappingURL=common.js.map