"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dropdown_directive_1 = require("./dropdown.directive");
var DropdownToggleDirective = /** @class */ (function () {
    function DropdownToggleDirective(dropdown, elementRef) {
        this.dropdown = dropdown;
        dropdown.toggleElement = elementRef.nativeElement;
    }
    DropdownToggleDirective = __decorate([
        core_1.Directive({
            selector: '[ngxDropdownToggle]',
            // tslint:disable-next-line:use-host-property-decorator
            host: {
                'class': 'dropdown-toggle',
                'aria-haspopup': 'true',
                '[attr.aria-expanded]': 'dropdown.isOpen',
                '(click)': 'dropdown.toggle()'
            }
        }),
        __metadata("design:paramtypes", [dropdown_directive_1.DropdownDirective,
            core_1.ElementRef])
    ], DropdownToggleDirective);
    return DropdownToggleDirective;
}());
exports.DropdownToggleDirective = DropdownToggleDirective;
//# sourceMappingURL=dropdown-toggle.directive.js.map