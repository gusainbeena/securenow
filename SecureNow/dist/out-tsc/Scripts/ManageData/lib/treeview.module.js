"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var dropdown_directive_1 = require("./dropdown.directive");
var dropdown_menu_directive_1 = require("./dropdown-menu.directive");
var dropdown_toggle_directive_1 = require("./dropdown-toggle.directive");
var dropdown_treeview_component_1 = require("./dropdown-treeview.component");
var treeview_component_1 = require("./treeview.component");
var treeview_item_component_1 = require("./treeview-item.component");
var treeview_pipe_1 = require("./treeview.pipe");
var treeview_i18n_1 = require("./treeview-i18n");
var treeview_config_1 = require("./treeview-config");
var treeview_event_parser_1 = require("./treeview-event-parser");
var TreeviewModule = /** @class */ (function () {
    function TreeviewModule() {
    }
    TreeviewModule_1 = TreeviewModule;
    TreeviewModule.forRoot = function () {
        return {
            ngModule: TreeviewModule_1,
            providers: [
                treeview_config_1.TreeviewConfig,
                { provide: treeview_i18n_1.TreeviewI18n, useClass: treeview_i18n_1.TreeviewI18nDefault },
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.DefaultTreeviewEventParser }
            ]
        };
    };
    var TreeviewModule_1;
    TreeviewModule = TreeviewModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule,
                common_1.CommonModule
            ],
            declarations: [
                treeview_component_1.TreeviewComponent,
                treeview_item_component_1.TreeviewItemComponent,
                treeview_pipe_1.TreeviewPipe,
                dropdown_directive_1.DropdownDirective,
                dropdown_menu_directive_1.DropdownMenuDirective,
                dropdown_toggle_directive_1.DropdownToggleDirective,
                dropdown_treeview_component_1.DropdownTreeviewComponent
            ], exports: [
                treeview_component_1.TreeviewComponent,
                treeview_pipe_1.TreeviewPipe,
                dropdown_treeview_component_1.DropdownTreeviewComponent
            ]
        })
    ], TreeviewModule);
    return TreeviewModule;
}());
exports.TreeviewModule = TreeviewModule;
//# sourceMappingURL=treeview.module.js.map