"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lodash_1 = require("lodash");
var treeview_item_1 = require("./treeview-item");
var treeview_config_1 = require("./treeview-config");
var TreeviewItemComponent = /** @class */ (function () {
    function TreeviewItemComponent(defaultConfig) {
        var _this = this;
        this.defaultConfig = defaultConfig;
        this.checkedChange = new core_1.EventEmitter();
        this.onCollapseExpand = function () {
            _this.item.collapsed = !_this.item.collapsed;
        };
        this.onCheckedChange = function () {
            var checked = _this.item.checked;
            if (!lodash_1.isNil(_this.item.children) && !_this.config.decoupleChildFromParent) {
                _this.item.children.forEach(function (child) { return child.setCheckedRecursive(checked); });
            }
            _this.checkedChange.emit(checked);
        };
        this.config = this.defaultConfig;
    }
    TreeviewItemComponent.prototype.onChildCheckedChange = function (child, checked) {
        if (!this.config.decoupleChildFromParent) {
            var itemChecked = null;
            for (var _i = 0, _a = this.item.children; _i < _a.length; _i++) {
                var childItem = _a[_i];
                if (itemChecked === null) {
                    itemChecked = childItem.checked;
                }
                else if (itemChecked !== childItem.checked) {
                    itemChecked = undefined;
                    break;
                }
            }
            if (itemChecked === null) {
                itemChecked = false;
            }
            if (this.item.checked !== itemChecked) {
                this.item.checked = itemChecked;
            }
        }
        this.checkedChange.emit(checked);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", treeview_config_1.TreeviewConfig)
    ], TreeviewItemComponent.prototype, "config", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], TreeviewItemComponent.prototype, "template", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", treeview_item_1.TreeviewItem)
    ], TreeviewItemComponent.prototype, "item", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], TreeviewItemComponent.prototype, "checkedChange", void 0);
    TreeviewItemComponent = __decorate([
        core_1.Component({
            selector: 'ngx-treeview-item',
            templateUrl: './treeview-item.component.html',
            styleUrls: ['./treeview-item.component.scss']
        }),
        __metadata("design:paramtypes", [treeview_config_1.TreeviewConfig])
    ], TreeviewItemComponent);
    return TreeviewItemComponent;
}());
exports.TreeviewItemComponent = TreeviewItemComponent;
//# sourceMappingURL=treeview-item.component.js.map