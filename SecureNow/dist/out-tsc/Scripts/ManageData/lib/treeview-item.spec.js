"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var treeview_item_1 = require("./treeview-item");
describe('TreeviewItem', function () {
    it('should throw error if TreeItem param is null of undefined', function () {
        var error = new Error('Item must be defined');
        expect(function () { return new treeview_item_1.TreeviewItem(null); }).toThrow(error);
        expect(function () { return new treeview_item_1.TreeviewItem(undefined); }).toThrow(error);
    });
    it('should throw error if TreeItem text is not a string', function () {
        var error = new Error('A text of item must be string object');
        var fakeString = 1;
        expect(function () { return new treeview_item_1.TreeviewItem({ text: null, value: 1 }); }).toThrow(error);
        expect(function () { return new treeview_item_1.TreeviewItem({ text: undefined, value: 1 }); }).toThrow(error);
        expect(function () { return new treeview_item_1.TreeviewItem({ text: fakeString, value: 1 }); }).toThrow(error);
    });
    it('should throw error if TreeviewItem children is assigned an empty array', function () {
        var error = new Error('Children must be not an empty array');
        var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
        expect(function () { return treeviewItem.children = []; }).toThrow(error);
    });
    it('should allow to create TreeviewItem with empty children', function () {
        var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1, children: [] });
        expect(treeviewItem.children).toBeUndefined();
    });
    describe('checked', function () {
        it('should have value is true by default', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
            expect(treeviewItem.checked).toBeTruthy();
        });
        it('should correct checked value when input second param', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                checked: false,
                children: [
                    { text: 'Child 1', value: 11, checked: true }
                ]
            }, true);
            expect(treeviewItem.checked).toBe(true);
        });
        it('should set checked value correctly when invoke correctChecked', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                checked: false,
                children: [
                    { text: 'Child 1', value: 11, checked: true }
                ]
            });
            expect(treeviewItem.checked).toBe(true);
            treeviewItem.children.push(new treeview_item_1.TreeviewItem({
                text: 'Child 2',
                value: 12,
                checked: false
            }));
            treeviewItem.correctChecked();
            expect(treeviewItem.checked).toBe(undefined);
        });
        it('should not change checked value if item is disabled', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                checked: true,
                disabled: true
            });
            expect(treeviewItem.checked).toBe(true);
            treeviewItem.checked = false;
            expect(treeviewItem.checked).toBe(true);
        });
    });
    describe('setCheckedRecursive', function () {
        it('should apply checked value to children if item is enabled', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                checked: false,
                children: [
                    { text: 'Child 1', value: 11, checked: false }
                ]
            });
            expect(treeviewItem.children[0].checked).toBe(false);
            treeviewItem.setCheckedRecursive(true);
            expect(treeviewItem.children[0].checked).toBe(true);
        });
        it('should not apply checked value to children if item is disabled', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                disabled: true,
                children: [
                    { text: 'Child 1', value: 11 }
                ]
            });
            expect(treeviewItem.children[0].checked).toBe(true);
            treeviewItem.setCheckedRecursive(true);
            expect(treeviewItem.children[0].checked).toBe(true);
        });
    });
    describe('collapsed', function () {
        it('should set value is false by default', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
            expect(treeviewItem.collapsed).toBeFalsy();
        });
        it('should affectly change collapsed value', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1, collapsed: true });
            expect(treeviewItem.collapsed).toBeTruthy();
            treeviewItem.collapsed = false;
            expect(treeviewItem.collapsed).toBeFalsy();
            treeviewItem.collapsed = false;
            expect(treeviewItem.collapsed).toBeFalsy();
        });
    });
    describe('setCollapsedRecursive', function () {
        it('should apply collapsed value to children', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                collapsed: false,
                children: [
                    { text: 'Child 1', value: 11, collapsed: false }
                ]
            });
            expect(treeviewItem.children[0].collapsed).toBe(false);
            treeviewItem.setCollapsedRecursive(true);
            expect(treeviewItem.children[0].collapsed).toBe(true);
        });
    });
    describe('disabled', function () {
        it('should set value is false by default', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
            expect(treeviewItem.disabled).toBeFalsy();
        });
        it('should initialize children are disabled if initializing parent is disabled', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                disabled: true,
                children: [
                    { text: 'Child', value: 11, disabled: false }
                ]
            });
            expect(treeviewItem.children[0].disabled).toBeTruthy();
        });
        it('should change disabled value of children to false if changing disabled of parent to false', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                children: [
                    { text: 'Child 1', value: 11 }
                ]
            });
            expect(treeviewItem.children[0].disabled).toBe(false);
            treeviewItem.disabled = true;
            expect(treeviewItem.children[0].disabled).toBe(true);
            treeviewItem.disabled = true;
            expect(treeviewItem.children[0].disabled).toBe(true);
        });
    });
    describe('children', function () {
        it('should throw error if change value to empty list', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
            var error = new Error('Children must be not an empty array');
            expect(function () { return treeviewItem.children = []; }).toThrow(error);
        });
        it('should affectly change children value', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
            var children = [
                new treeview_item_1.TreeviewItem({ text: 'Child 1', value: 11 })
            ];
            expect(treeviewItem.children).toBeUndefined();
            treeviewItem.children = children;
            expect(treeviewItem.children).toBe(children);
            treeviewItem.children = children;
            expect(treeviewItem.children).toBe(children);
        });
        it('should accept undefined value', function () {
            var treeviewItem = new treeview_item_1.TreeviewItem({
                text: 'Parent',
                value: 1,
                children: [
                    { text: 'Child 1', value: 11 }
                ]
            });
            expect(treeviewItem.children).toBeDefined();
            treeviewItem.children = undefined;
            expect(treeviewItem.children).toBeUndefined();
        });
    });
    describe('getSelection', function () {
        describe('no children', function () {
            it('should return empty list if item is unchecked', function () {
                var parentItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1, checked: false });
                var selection = parentItem.getSelection();
                expect(selection.checkedItems).toEqual([]);
                expect(selection.uncheckedItems).toEqual([parentItem]);
            });
            it('should return a list of current item if item is unchecked', function () {
                var parentItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1 });
                var selection = parentItem.getSelection();
                expect(selection.checkedItems).toEqual([parentItem]);
                expect(selection.uncheckedItems).toEqual([]);
            });
        });
        describe('has children', function () {
            it('should return list of checked items', function () {
                var parentItem = new treeview_item_1.TreeviewItem({ text: 'Parent', value: 1, checked: false });
                var childItem1 = new treeview_item_1.TreeviewItem({ text: 'Child 1', value: 11, checked: true });
                var childItem2 = new treeview_item_1.TreeviewItem({ text: 'Child 2', value: 12, checked: false });
                var childItem21 = new treeview_item_1.TreeviewItem({ text: 'Child 21', value: 121, checked: true });
                var childItem22 = new treeview_item_1.TreeviewItem({ text: 'Child 22', value: 122, checked: false });
                childItem2.children = [childItem21, childItem22];
                parentItem.children = [childItem1, childItem2];
                var selection = parentItem.getSelection();
                expect(selection.checkedItems).toEqual([childItem1, childItem21]);
                expect(selection.uncheckedItems).toEqual([childItem22]);
            });
        });
    });
});
//# sourceMappingURL=treeview-item.spec.js.map