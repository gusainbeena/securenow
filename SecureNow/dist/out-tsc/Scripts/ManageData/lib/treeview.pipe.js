"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lodash_1 = require("lodash");
var treeview_item_1 = require("./treeview-item");
var TreeviewPipe = /** @class */ (function () {
    function TreeviewPipe() {
    }
    TreeviewPipe.prototype.transform = function (objects, textField) {
        if (lodash_1.isNil(objects)) {
            return undefined;
        }
        return objects.map(function (object) { return new treeview_item_1.TreeviewItem({ text: object[textField], value: object }); });
    };
    TreeviewPipe = __decorate([
        core_1.Pipe({
            name: 'ngxTreeview'
        })
    ], TreeviewPipe);
    return TreeviewPipe;
}());
exports.TreeviewPipe = TreeviewPipe;
//# sourceMappingURL=treeview.pipe.js.map