"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var treeview_pipe_1 = require("./treeview.pipe");
describe('TreeviewPipe', function () {
    var pipe = new treeview_pipe_1.TreeviewPipe();
    it('transforms null or undefined to undefined', function () {
        expect(pipe.transform(null, undefined)).toBe(undefined, 'case of null');
        expect(pipe.transform(undefined, undefined)).toBe(undefined, 'case of undefined');
    });
    it('transforms a list of objects to list of TreeItem objects', function () {
        var objects = [{
                name: 'leo',
                age: '18'
            }, {
                name: 'vo',
                age: '14'
            }];
        var treeItems = pipe.transform(objects, 'name');
        expect(objects.length === treeItems.length).toBe(true, 'same length');
        expect(objects[0].name === treeItems[0].text && objects[1].name === treeItems[1].text).toBe(true, 'validate text');
        expect(objects[0] === treeItems[0].value && objects[1] === treeItems[1].value).toBe(true, 'validate value');
    });
});
//# sourceMappingURL=treeview.pipe.spec.js.map