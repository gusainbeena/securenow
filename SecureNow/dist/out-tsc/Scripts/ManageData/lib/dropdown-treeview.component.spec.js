"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var treeview_component_1 = require("./treeview.component");
var dropdown_treeview_component_1 = require("./dropdown-treeview.component");
var treeview_item_component_1 = require("./treeview-item.component");
var treeview_config_1 = require("./treeview-config");
var treeview_item_1 = require("./treeview-item");
var treeview_i18n_1 = require("./treeview-i18n");
var treeview_event_parser_1 = require("./treeview-event-parser");
var testing_2 = require("../testing");
var fakeData = {
    config: undefined,
    items: undefined,
    selectedChange: function (data) { },
    hide: function () { }
};
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        this.config = fakeData.config;
        this.items = fakeData.items;
        this.selectedChange = fakeData.selectedChange;
        this.hide = fakeData.hide;
    }
    TestComponent = __decorate([
        core_1.Component({
            selector: 'ngx-test',
            template: ''
        })
    ], TestComponent);
    return TestComponent;
}());
var createTestComponent = function (html) {
    return testing_2.createGenericTestComponent(html, TestComponent);
};
describe('DropdownTreeviewComponent', function () {
    var template = '<ngx-dropdown-treeview [items]="items" (selectedChange)="selectedChange($event)"></ngx-dropdown-treeview>';
    var spy;
    var button;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                platform_browser_1.BrowserModule
            ],
            declarations: [
                TestComponent,
                treeview_component_1.TreeviewComponent,
                treeview_item_component_1.TreeviewItemComponent,
                dropdown_treeview_component_1.DropdownTreeviewComponent
            ],
            providers: [
                treeview_config_1.TreeviewConfig,
                { provide: treeview_i18n_1.TreeviewI18n, useClass: treeview_i18n_1.TreeviewI18nDefault },
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.DefaultTreeviewEventParser }
            ]
        });
        spy = spyOn(fakeData, 'selectedChange');
    });
    beforeEach(testing_1.fakeAsync(function () {
        spy.calls.reset();
        fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
        var fixture = createTestComponent(template);
        fixture.detectChanges();
        testing_1.tick();
        button = fixture.debugElement.query(platform_browser_1.By.css('button'));
    }));
    it('should initialize with default config', function () {
        var defaultConfig = new treeview_config_1.TreeviewConfig();
        var component = testing_1.TestBed.createComponent(dropdown_treeview_component_1.DropdownTreeviewComponent).componentInstance;
        testing_2.expect(component.config).toEqual(defaultConfig);
    });
    it('should raise event selectedChange when initializing', function () {
        testing_2.expect(spy.calls.any()).toBeTruthy();
    });
    it('should display button text "All"', function () {
        testing_2.expect(button.nativeElement).toHaveTrimmedText('All');
    });
});
//# sourceMappingURL=dropdown-treeview.component.spec.js.map