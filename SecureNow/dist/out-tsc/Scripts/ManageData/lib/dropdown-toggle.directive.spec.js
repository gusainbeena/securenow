"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var dropdown_toggle_directive_1 = require("./dropdown-toggle.directive");
var dropdown_directive_1 = require("./dropdown.directive");
var testing_2 = require("../testing");
var TestComponent = /** @class */ (function () {
    function TestComponent() {
    }
    TestComponent = __decorate([
        core_1.Component({
            selector: 'ngx-test',
            template: ''
        })
    ], TestComponent);
    return TestComponent;
}());
var createTestComponent = function (html) {
    return testing_2.createGenericTestComponent(html, TestComponent);
};
describe('DropdownToggleDirective', function () {
    var fixture;
    var element;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                platform_browser_1.BrowserModule
            ],
            declarations: [
                TestComponent,
                dropdown_directive_1.DropdownDirective,
                dropdown_toggle_directive_1.DropdownToggleDirective
            ]
        });
    });
    beforeEach(function () {
        fixture = createTestComponent('<div ngxDropdown><div ngxDropdownToggle></div></div>');
        fixture.detectChanges();
        element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_toggle_directive_1.DropdownToggleDirective));
    });
    it('should work', function () {
        testing_2.expect(element).toBeDefined();
        testing_2.expect(element.nativeElement).toHaveCssClass('dropdown-toggle');
        testing_2.expect(element.nativeElement.attributes['aria-haspopup'].value).toBe('true');
    });
    it('should set attribute "aria-expanded" is "false" when initializing', function () {
        testing_2.expect(element.nativeElement.attributes['aria-expanded'].value).toBe('false');
    });
    describe('click element', function () {
        beforeEach(function () {
            element.nativeElement.click();
            fixture.detectChanges();
        });
        it('should change attribute "aria-expanded" to "true"', function () {
            testing_2.expect(element.nativeElement.attributes['aria-expanded'].value).toBe('true');
        });
        describe('click element again', function () {
            beforeEach(function () {
                element.nativeElement.click();
                fixture.detectChanges();
            });
            it('should change attribute "aria-expanded" to "false"', function () {
                testing_2.expect(element.nativeElement.attributes['aria-expanded'].value).toBe('false');
            });
        });
    });
});
//# sourceMappingURL=dropdown-toggle.directive.spec.js.map