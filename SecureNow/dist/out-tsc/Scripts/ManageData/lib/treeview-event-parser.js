"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lodash_1 = require("lodash");
var TreeviewEventParser = /** @class */ (function () {
    function TreeviewEventParser() {
    }
    TreeviewEventParser = __decorate([
        core_1.Injectable()
    ], TreeviewEventParser);
    return TreeviewEventParser;
}());
exports.TreeviewEventParser = TreeviewEventParser;
var DefaultTreeviewEventParser = /** @class */ (function (_super) {
    __extends(DefaultTreeviewEventParser, _super);
    function DefaultTreeviewEventParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DefaultTreeviewEventParser.prototype.getSelectedChange = function (component) {
        var checkedItems = component.selection.checkedItems;
        if (!lodash_1.isNil(checkedItems)) {
            return checkedItems.map(function (item) { return item.value; });
        }
        return [];
    };
    DefaultTreeviewEventParser = __decorate([
        core_1.Injectable()
    ], DefaultTreeviewEventParser);
    return DefaultTreeviewEventParser;
}(TreeviewEventParser));
exports.DefaultTreeviewEventParser = DefaultTreeviewEventParser;
var DownlineTreeviewEventParser = /** @class */ (function (_super) {
    __extends(DownlineTreeviewEventParser, _super);
    function DownlineTreeviewEventParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DownlineTreeviewEventParser.prototype.getSelectedChange = function (component) {
        var _this = this;
        var items = component.items;
        if (!lodash_1.isNil(items)) {
            var result_1 = [];
            items.forEach(function (item) {
                var links = _this.getLinks(item, null);
                if (!lodash_1.isNil(links)) {
                    result_1 = result_1.concat(links);
                }
            });
            return result_1;
        }
        return [];
    };
    DownlineTreeviewEventParser.prototype.getLinks = function (item, parent) {
        var _this = this;
        if (!lodash_1.isNil(item.children)) {
            var link_1 = {
                item: item,
                parent: parent
            };
            var result_2 = [];
            item.children.forEach(function (child) {
                var links = _this.getLinks(child, link_1);
                if (!lodash_1.isNil(links)) {
                    result_2 = result_2.concat(links);
                }
            });
            return result_2;
        }
        if (item.checked) {
            return [{
                    item: item,
                    parent: parent
                }];
        }
        return null;
    };
    DownlineTreeviewEventParser = __decorate([
        core_1.Injectable()
    ], DownlineTreeviewEventParser);
    return DownlineTreeviewEventParser;
}(TreeviewEventParser));
exports.DownlineTreeviewEventParser = DownlineTreeviewEventParser;
var OrderDownlineTreeviewEventParser = /** @class */ (function (_super) {
    __extends(OrderDownlineTreeviewEventParser, _super);
    function OrderDownlineTreeviewEventParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.currentDownlines = [];
        _this.parser = new DownlineTreeviewEventParser();
        return _this;
    }
    OrderDownlineTreeviewEventParser.prototype.getSelectedChange = function (component) {
        var newDownlines = this.parser.getSelectedChange(component);
        if (this.currentDownlines.length === 0) {
            this.currentDownlines = newDownlines;
        }
        else {
            var intersectDownlines_1 = [];
            this.currentDownlines.forEach(function (downline) {
                var foundIndex = -1;
                var length = newDownlines.length;
                for (var i = 0; i < length; i++) {
                    if (downline.item.value === newDownlines[i].item.value) {
                        foundIndex = i;
                        break;
                    }
                }
                if (foundIndex !== -1) {
                    intersectDownlines_1.push(newDownlines[foundIndex]);
                    newDownlines.splice(foundIndex, 1);
                }
            });
            this.currentDownlines = intersectDownlines_1.concat(newDownlines);
        }
        return this.currentDownlines;
    };
    OrderDownlineTreeviewEventParser = __decorate([
        core_1.Injectable()
    ], OrderDownlineTreeviewEventParser);
    return OrderDownlineTreeviewEventParser;
}(TreeviewEventParser));
exports.OrderDownlineTreeviewEventParser = OrderDownlineTreeviewEventParser;
//# sourceMappingURL=treeview-event-parser.js.map