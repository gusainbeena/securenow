"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var of_1 = require("rxjs/observable/of");
var operators_1 = require("rxjs/operators");
var HttpRequestObj_1 = require("./HttpRequestObj");
var HttpService = /** @class */ (function () {
    function HttpService(http) {
        //get the user ip details using a jsonp response
        this.http = http;
        this.httpOptions = {
            headers: new http_1.HttpHeaders({ 'Content-Type': 'application/json' })
        };
        this.trackObj = {};
        this.location = {};
        this.ipInfoToken = '7c8b8f2eab6adf';
        /*let parentRef = this;
        $.get("https://ipinfo.io?token="+this.ipInfoToken, function(response) {
            parentRef.trackObj = {
               'ip': response.ip+'@'+response.hostname,
               'loc' : response.city + ' , ' +  response.region  + ' , ' + response.country,
               'coord' : response.loc,
               'asn' : response.org
            }
    
            //parentRef.httpOptions.headers.set('Authorization', parentRef.trackObj);
    
            var hrd = new HttpHeaders().append('Authorization', parentRef.trackObj);
            parentRef.httpOptions.headers = hrd;
    
        }, "jsonp");*/
    }
    //GENRIC POST request
    HttpService.prototype.postRequest = function (requestObj, url, spinner) {
        var parentRef = this;
        var httpRequestObj = new HttpRequestObj_1.HttpRequestObj(requestObj);
        if (spinner == true) {
            $('#hidepageoverlay').css('display', 'block');
        }
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.post(url, httpRequestObj, parentRef.httpOptions)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    if (spinner == true) {
                        $('#hidepageoverlay').css('display', 'none');
                    }
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    //REST API GET ALL RECORDS
    HttpService.prototype.getAll = function (url) {
        var parentRef = this;
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        /*if(navigator.geolocation) {
         navigator.geolocation.getCurrentPosition((position) => {
           this.location['latitude']  = position.coords.latitude;
           this.location['longitude'] = position.coords.longitude;
           //console.log(this.location);
         });
      
         var hrd = new HttpHeaders().append('Authorization', this.location);
         console.log(this.location);
         parentRef.httpOptions.headers = hrd;*/
        //this.httpOptions.headers.append('Authorization',this.location);
        //httpEvent:HttpResponse<HttpResponseObj>
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.get(url, parentRef.httpOptions)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    //REST API GET SPECIFIC RECORD BASED ON ID
    HttpService.prototype.find = function (id, url) {
        var parentRef = this;
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.get(url + '/' + id)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    //CREATE A NEW RECORD
    HttpService.prototype.save = function (requestObj, url) {
        var parentRef = this;
        var httpRequestObj = new HttpRequestObj_1.HttpRequestObj(requestObj);
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.post(url, httpRequestObj, parentRef.httpOptions)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    //UPDATE AN EXISTING RECORD
    HttpService.prototype.update = function (requestObj, url, id) {
        var parentRef = this;
        var httpRequestObj = new HttpRequestObj_1.HttpRequestObj(requestObj);
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.put(url + '/' + id, httpRequestObj, parentRef.httpOptions)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    //DELETE AN EXISTING RECORD
    HttpService.prototype.delete = function (url, id) {
        var parentRef = this;
        //create new observable of the return type which will be executed as completed
        //as and when we get back the response from http
        return rxjs_1.Observable.create(function subscribe(observer) {
            parentRef.http.delete(url + '/' + id)
                .pipe(operators_1.catchError(parentRef.handleError('getHeroes', [])))
                .subscribe(function (httpEvent) {
                if (httpEvent['errors'] == null) {
                    observer.next(httpEvent);
                    observer.complete();
                }
                else {
                    //show validation errors or business errors
                    observer.complete();
                }
            });
        });
    };
    HttpService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            //this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of_1.of(result);
        };
    };
    HttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], HttpService);
    return HttpService;
}());
exports.HttpService = HttpService;
//# sourceMappingURL=http.service.js.map