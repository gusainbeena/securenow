"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureOpTemplate';
var ConfigureOpTemplateComponent = /** @class */ (function () {
    function ConfigureOpTemplateComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        this.isSpinner = true;
        this.keyValueData = {};
        this.metas = [];
        this.meta = [];
        this.fields = [];
        this.field = [];
        this.performance = [];
        this.employeeComponent = [];
        this.customerComponent = [];
        this.performanceComponent = [];
        this.businessVertical = [];
        this.businessVerticalList = [];
        this.column = [];
        this.temp = [];
        this.metaData = {};
        this.months = [];
        this.columnData = {};
        this.currentYear = [];
        this.excelColumn = [];
        this.columnsId = [];
        this.finalExcel = [];
        this.columnList = [];
        this.monthList = [];
        this.monthsList = [];
        //public BGtargetList: any = [];
        //public BUtargetList: any = [];
        this.roleList = [];
        this.salesList = [];
        this.performanceList = [];
        this.previousExcelColumn = [];
        this.previousColumnsId = [];
        this.employeeAttributes = [];
        this.customerAttributes = [];
        this.salesHorizontals = [];
        this.selectedEmpAttributes = [];
        this._secureFileService = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            enableCheckAll: false
        };
    }
    ConfigureOpTemplateComponent.prototype.ngOnInit = function () {
        var _this = this;
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        $('#removeButton').css('display', 'block');
        $('#col').css('display', 'block');
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/getTemplateAttributes', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.metas = datas.rsBody.metas;
                _this.months = datas.rsBody.months;
                _this.monthsList = datas.rsBody.monthsList;
                _this.employeeList = datas.rsBody.employeeList;
                _this.customerList = datas.rsBody.customerList;
                _this.employeeComponent = datas.rsBody.employeeComponent;
                _this.customerComponent = datas.rsBody.customerComponent;
                _this.employeeComponent = datas.rsBody.employeeComponent;
                _this.customerComponent = datas.rsBody.customerComponent;
                _this.performanceComponent = datas.rsBody.performanceComponent;
                _this.businessVertical = datas.rsBody.businessVertical;
                _this.salesHorizontals = datas.rsBody.salesHorizontals;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
                _this.currentYear = datas.rsBody.currentYear;
                _this.onSelectAllPerformance(datas.rsBody.performanceComponent);
                _this.onSelectAllBusinessVertical(datas.rsBody.businessVertical);
                _this.onSelectAllSales(datas.rsBody.salesHorizontals);
                //this.onSelectAllBUTarget(datas.rsBody.BUType);
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.onItemSelectSales = function (item) {
        this.isSpinner = false;
        // this.salesList.push(item);
    };
    ConfigureOpTemplateComponent.prototype.onSelectAllSales = function (items) {
        this.salesList = items;
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onDeSelectAllSales = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onItemDeSelectSales = function (item) {
        this.isSpinner = false;
        var i = this.salesList.indexOf(item); // RETURNS 1.
        //this.salesList.splice(i, 1);
    };
    ConfigureOpTemplateComponent.prototype.onItemSelectMonth = function (item, i) {
        this.isSpinner = false;
        this.monthList.push(item);
    };
    ConfigureOpTemplateComponent.prototype.onSelectAllMonth = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onDeSelectAllMonth = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onItemDeSelectMonth = function (item, i) {
        this.isSpinner = false;
        var i = this.monthList.indexOf(item); // RETURNS 1.
        this.monthList.splice(i, 1);
    };
    //------------------------------------
    ConfigureOpTemplateComponent.prototype.onItemSelectBusinessVertical = function (item) {
        this.isSpinner = false;
        this.businessVerticalList.push(item);
    };
    ConfigureOpTemplateComponent.prototype.onSelectAllBusinessVertical = function (items) {
        this.businessVerticalList = items;
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onDeSelectAllBusinessVertical = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onItemDeSelectBusinessVertical = function (item) {
        this.isSpinner = false;
        var i = this.businessVerticalList.indexOf(item); // RETURNS 1.
        this.businessVerticalList.splice(i, 1);
    };
    //------------------------------
    ConfigureOpTemplateComponent.prototype.onItemSelectBUTarget = function (item) {
        this.isSpinner = false;
        //this.BUtargetList.push(item);
    };
    ConfigureOpTemplateComponent.prototype.onSelectAllBUTarget = function (items) {
        //this.BUtargetList = items;
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onDeSelectAllBUTarget = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onItemDeSelectBUTarget = function (item) {
        this.isSpinner = false;
        //var i = this.BUtargetList.indexOf(item); // RETURNS 1.
        //this.BUtargetList.splice(i, 1);
    };
    //----------------------------------
    ConfigureOpTemplateComponent.prototype.onItemSelectPerformance = function (item) {
        this.isSpinner = false;
        this.performanceList.push(item);
    };
    ConfigureOpTemplateComponent.prototype.onSelectAllPerformance = function (items) {
        this.performanceList = items;
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onDeSelectAllPerformance = function (items) {
        this.isSpinner = false;
    };
    ConfigureOpTemplateComponent.prototype.onItemDeSelectPerformance = function (item) {
        this.isSpinner = false;
        var i = this.performanceList.indexOf(item); // RETURNS 1.
        this.performanceList.splice(i, 1);
    };
    ConfigureOpTemplateComponent.prototype.OnDataTypeChange = function (i) {
        var _this = this;
        this.metaData.id = i;
        this.metaData.keyName = this.meta[this.metaData.id];
        $("#templateScope").css('display', 'contents');
        if (this.metaData.keyName == "RoleHierarchy") {
            $("#multifieldRoles" + i).css('display', 'block');
            $("#multifieldSales" + i).css('display', 'none');
            $("#multifieldPerformance" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'none');
        }
        else if (this.metaData.keyName == "PerformanceComponent") {
            $("#multifieldPerformance" + i).css('display', 'block');
            $("#multifieldSales" + i).css('display', 'none');
            $("#multifieldRoles" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'block');
        }
        else if (this.metaData.keyName == "salesHorizontal") {
            $("#multifieldSales" + i).css('display', 'block');
            $("#multifieldRoles" + i).css('display', 'none');
            $("#multifieldPerformance" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'none');
        }
        this.httpService.postRequest(this.metaData, '../ManageTemplate/getMetaDataFields', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.fields[i] = datas.rsBody.fields;
                _this.id = datas.rsBody.id;
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.AddPerformance = function (i) {
        var _this = this;
        this.metaData.id = i;
        this.metaData.keyName = this.meta[this.metaData.id];
        this.columnData.keyName = this.field[i];
        this.columnData.columnList = this.columnList;
        this.columnData.excelColumn = this.excelColumn;
        //this.columnData.BGtargetList = this.BGtargetList;
        //this.columnData.BUtargetList = this.BUtargetList;
        this.columnData.monthList = this.monthList;
        this.columnData.performanceList = this.performanceList;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/addPerformance', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                var colList = [];
                _this.temp = datas.rsBody.columnList;
                _this.temp.forEach(function (value) {
                    colList.push(value);
                });
                $("#addPerformanceBtn" + i).css('display', 'none');
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.removeColumn = function (i) {
        var _this = this;
        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/removeColumn', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.columnData.excelColumn = datas.rsBody.excelColumn;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnData.finalExcel = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.OnAddcustomerComponent = function () {
        $('#custCompBtn').css('display', 'block');
        $('#custCompText').css('display', 'block');
    };
    ConfigureOpTemplateComponent.prototype.OnAddemployeeComponent = function () {
        $('#empCompBtn').css('display', 'block');
        $('#empCompText').css('display', 'block');
    };
    ConfigureOpTemplateComponent.prototype.OnColumnSelect = function (item) {
        this.addColumn = item;
    };
    ConfigureOpTemplateComponent.prototype.OnStartMonthSelect = function (i) {
        var start = i;
        this.startMonth = i;
    };
    ConfigureOpTemplateComponent.prototype.OnYearSelect = function (i) {
        var start = i;
        this.year = i;
    };
    ConfigureOpTemplateComponent.prototype.OnEndMonthSelect = function (i) {
        var end = i;
        this.endMonth = i;
    };
    ConfigureOpTemplateComponent.prototype.addNewColumn = function (newColumn) {
        var _this = this;
        this.newColumn = newColumn;
        this.columnData.addColumn = this.addColumn;
        this.columnData.newColumn = this.newColumn;
        this.columnData.excelColumn = this.columnData.finalExcel;
        this.httpService.postRequest(this.columnData, '../AdminDepartment/addNewColumn', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.columnData.excelColumn = datas.rsBody.excelColumn;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnData.finalExcel = datas.rsBody.excelColumn;
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.viewEmpComponent = function () {
        $("#employeeComponentPopup").modal();
    };
    ConfigureOpTemplateComponent.prototype.viewCustComponent = function () {
        $("#customerComponentPopup").modal();
    };
    ConfigureOpTemplateComponent.prototype.createTemplate = function () {
        var _this = this;
        this.columnData.salesList = this.salesList;
        this.columnData.roleList = this.roleList;
        this.columnData.performance = this.temp;
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.columnData, '../ManageTemplate/createTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.message == "error") {
                    _this.message = datas.rsBody.message;
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Empty") {
                    _this.message = "Please Fill Values";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.finalExcel == datas.rsBody.finalExcel;
                    _this.relatedKeyValue = datas.rsBody.relatedKeyValue;
                    $("#sampleTemplate").css('display', 'block');
                    $("#downTempBtn").css('display', 'block');
                    $("#addNewColumnListBtn").addClass("ng-hide");
                }
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.generateTemplate = function () {
        var _this = this;
        /*if (this.startMonth == "" || this.startMonth == null) {
             this.columnData.startMonth = 1;
         }
         else {
             this.columnData.startMonth = this.startMonth;
         }
         if (this.endMonth == "" || this.endMonth == null) {
             this.columnData.endMonth = 12
         }
         else {
             this.columnData.endMonth = this.endMonth;
         }*/
        //if (this.startMonth == "" || this.endMonth == "") {
        this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
        // }
        //else {
        this.columnData.startMonth = this.startMonth;
        this.columnData.endMonth = this.endMonth;
        //}
        this.columnData.year = this.year;
        this.columnData.performanceList = this.performanceList;
        //this.columnData.BGtargetList = this.BGtargetList;
        this.columnData.businessVerticalList = this.businessVerticalList;
        this.columnData.salesHorizontals = this.salesList;
        this.columnData.salesList = this.salesList;
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.columnData, '../ManageTemplate/generateOpTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.message == "Month Error") {
                    _this.message = "End month Should be greater than start month. Please Select Carefully";
                    _this.title = "Select Month error";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "OP Template for given period is already exist") {
                    _this.title = "OP Template Already Exist";
                    _this.message = "OP Template for given period is already exist";
                    _this.previousExcelColumn = datas.rsBody.previousExcelColumn;
                    _this.previousColumnsId = datas.rsBody.previousColumnId;
                    $("#previousTemplatePopup").modal();
                }
                if (datas.rsBody.message == "Please Select Start Month and End Month") {
                    _this.title = "Select Month error";
                    _this.message = "Please Select Start Month and End Month";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "error") {
                    _this.message = datas.rsBody.message;
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.title = "Duplicate Entry";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Empty") {
                    _this.title = "Value Error";
                    _this.message = "Please Fill Values";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.columnData.columnsId = datas.rsBody.columnsId;
                    _this.finalExcel == datas.rsBody.finalExcel;
                    _this.columnsId = datas.rsBody.columnsId;
                    $("#sampleTemplate").css('display', 'block');
                    $("#downTempBtn").css('display', 'block');
                    $("#addNewColumnListBtn").addClass("ng-hide");
                }
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.DownloadTemplate = function () {
        var _this = this;
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12;
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }
        this.httpService.postRequest(this.columnData, '../ManageTemplate/downloadTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.msg == "duplicate") {
                    _this.message = datas.rsBody.message;
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.columnData.filepath = datas.rsBody.filepath;
                    _this.filepath = datas.rsBody.filepath;
                    _this.fileName = datas.rsBody.fileName;
                    $("#successPopup").modal();
                }
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.ExportDataSetToExcel = function () {
        var _this = this;
        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        $('#alertbottomleft').addClass('ng-hide');
        $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName + '&path=' + this.filepath);
        $('#ExportExcelData')[0].click();
        $('#text').html('-Select Month-');
        $("#collapse1").collapse('hide');
        $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
        this.keyValueData.keyType = "OP";
        this.keyValueData.keyName = "Template";
        if (this.startMonth != null || this.startMonth == "") {
            this.keyValueData.startMonth = this.startMonth;
        }
        else {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "") {
            this.keyValueData.endMonth = this.endMonth;
        }
        else {
            this.keyValueData.endMonth = 12;
        }
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        $('#ExportExcelDataBtn').css('display', 'none');
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/insertOPTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.excelColumn = datas.rsBody.excelColumn;
                $('#ExportExcelData').css('display', 'none');
                $('#downTempBtn').css('display', 'none');
                $('#afterDownload').css('display', 'block');
                $('#beforeDownload').css('display', 'none');
                $('#addNewColumnBtn').css('display', 'none');
                $('#ExportExcelData').attr('disabled', true);
                $('#addNewColumnBtn').attr('disabled', true);
                //$('#downTempBtn').attr('disabled', true);
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.addemployeeComponent = function (newEmployee) {
        var _this = this;
        this.newEmployee = newEmployee;
        //this.columnData.removeColumnId = this.removeColumnId;
        this.httpService.postRequest(this.newEmployee, '../ManageTemplate/addemployeeComponent', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.employeeComponent = datas.rsBody.employeeComponent;
                _this.employeeList = datas.rsBody.employeeList;
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.addcustomerComponent = function (newCustomer) {
        var _this = this;
        this.newCustomer = newCustomer;
        //this.columnData.removeColumnId = this.removeColumnId;
        this.httpService.postRequest(this.newCustomer, '../ManageTemplate/addcustomerComponent', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.customerComponent = datas.rsBody.customerComponent;
                _this.customerList = datas.rsBody.customerList;
            }
        });
    };
    ConfigureOpTemplateComponent.prototype.ngAfterViewInit = function () {
    };
    ConfigureOpTemplateComponent.prototype.ngOnDestroy = function () {
    };
    ConfigureOpTemplateComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ConfigureOpTemplateComponent);
    return ConfigureOpTemplateComponent;
}());
exports.ConfigureOpTemplateComponent = ConfigureOpTemplateComponent;
//# sourceMappingURL=configureoptemplate.component.js.map