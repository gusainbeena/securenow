"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/http");
var ng_multiselect_dropdown_1 = require("ng-multiselect-dropdown");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var d3methodbinding_service_1 = require("../../service/d3methodbinding.service");
var configureoptemplate_routing_module_1 = require("./configureoptemplate-routing.module");
var configureoptemplate_component_1 = require("./configureoptemplate.component");
var ngx_treeview_1 = require("ngx-treeview");
var ConfigureOpTemplateModule = /** @class */ (function () {
    function ConfigureOpTemplateModule() {
    }
    ConfigureOpTemplateModule = __decorate([
        core_1.NgModule({
            declarations: [
                configureoptemplate_component_1.ConfigureOpTemplateComponent
            ],
            imports: [
                common_1.CommonModule,
                configureoptemplate_routing_module_1.ConfigureOpTemplateRoutingModule,
                ng_multiselect_dropdown_1.NgMultiSelectDropDownModule.forRoot(),
                http_1.HttpClientModule,
                forms_1.FormsModule,
                ngx_treeview_1.TreeviewModule.forRoot(),
                http_2.HttpModule
            ],
            providers: [http_service_1.HttpService, modelservice_1.ModalService, d3methodbinding_service_1.D3BindingsService],
            bootstrap: [configureoptemplate_component_1.ConfigureOpTemplateComponent],
            exports: []
        })
    ], ConfigureOpTemplateModule);
    return ConfigureOpTemplateModule;
}());
exports.ConfigureOpTemplateModule = ConfigureOpTemplateModule;
//# sourceMappingURL=configureoptemplate.module.js.map