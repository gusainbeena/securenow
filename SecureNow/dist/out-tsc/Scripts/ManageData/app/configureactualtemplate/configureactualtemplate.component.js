"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureActualTemplate';
var ConfigureActualTemplateComponent = /** @class */ (function () {
    function ConfigureActualTemplateComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        this.isSpinner = true;
        this.keyValueData = {};
        this.metas = [];
        this.meta = [];
        this.fields = [];
        this.field = [];
        this.performance = [];
        this.column = [];
        this.actualComponent = [];
        this.performanceComponent = [];
        this.salesHorizontal = [];
        this.months = [];
        this.columnData = {};
        this.excelColumn = [];
        this.columnsId = [];
        this.finalExcel = [];
        this.columnList = [];
        this.monthsList = [];
        this.salesList = [];
        this.performanceList = [];
        this._secureFileService = [];
        this.currrentYear = [];
        this.previousExcelColumn = [];
        this.previousColumnsId = [];
        this.editHeader = [{}];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            enableCheckAll: false
        };
    }
    ConfigureActualTemplateComponent.prototype.ngOnInit = function () {
        var _this = this;
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelData').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/getActualTemplateAttributes', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.monthsList = datas.rsBody.monthsList;
                _this.actualComponent = datas.rsBody.actualComponent;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
                _this.performanceComponent = datas.rsBody.performanceComponent;
                _this.finalExcel = datas.rsBody.excelColumn;
                _this.salesHorizontal = datas.rsBody.salesHorizontal;
                _this.editHeader = datas.rsBody.excelColumn;
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.removeActualColumn = function (i) {
        var _this = this;
        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/removeActualColumn', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.columnData.excelColumn = datas.rsBody.excelColumn;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnData.finalExcel = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.editLabel = function (i) {
        var _this = this;
        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        //this.columnData.finalExcel = this.excelColumn;
        //this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/editLabel', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.newLabel = datas.rsBody.label;
                $("#editLabelPopup").modal();
                //this.columnData.excelColumn = datas.rsBody.excelColumn;
                //this.excelColumn = datas.rsBody.excelColumn;
                //this.columnData.finalExcel = datas.rsBody.excelColumn;
                //this.columnsId = datas.rsBody.columnsId;
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.addLabel = function (label, i) {
        var _this = this;
        this.removeColumnId = i;
        this.newLabel = label;
        //this.columnData.finalExcel = this.excelColumn;
        //this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/addLabel', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.newLabel = datas.rsBody.label;
                $("#editLabelPopup").modal();
                //this.columnData.excelColumn = datas.rsBody.excelColumn;
                //this.excelColumn = datas.rsBody.excelColumn;
                //this.columnData.finalExcel = datas.rsBody.excelColumn;
                //this.columnsId = datas.rsBody.columnsId;
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.onItemSelectSales = function (item) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onSelectAllSales = function (items) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onDeSelectAllSales = function (items) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onItemDeSelectSales = function (item) {
        this.isSpinner = false;
        var i = this.salesList.indexOf(item); // RETURNS 1.
        this.salesList.splice(i, 1);
    };
    ConfigureActualTemplateComponent.prototype.onItemSelectPerformance = function (item) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onSelectAllPerformance = function (items) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onDeSelectAllPerformance = function (items) {
        this.isSpinner = false;
    };
    ConfigureActualTemplateComponent.prototype.onItemDeSelectPerformance = function (item) {
        this.isSpinner = false;
        var i = this.performance.indexOf(item); // RETURNS 1.
        this.performance.splice(i, 1);
    };
    ConfigureActualTemplateComponent.prototype.OnStartMonthSelect = function (i) {
        var start = i;
        this.startMonth = i;
    };
    ConfigureActualTemplateComponent.prototype.OnEndMonthSelect = function (i) {
        var end = i;
        this.endMonth = i;
    };
    ConfigureActualTemplateComponent.prototype.OnPerformanceSelect = function (i) {
        var end = i;
        this.performanceSelection = i;
    };
    ConfigureActualTemplateComponent.prototype.editActualHeader = function () {
        $("#headerRow").css('display', 'contents');
    };
    ConfigureActualTemplateComponent.prototype.generateTemplate = function () {
        var _this = this;
        this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
        this.columnData.startMonth = this.startMonth;
        this.columnData.endMonth = this.endMonth;
        this.columnData.performanceSelection = this.performance;
        this.columnData.salesList = this.salesList;
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.columnData, '../ManageTemplate/generateActualTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.message == "Month Error") {
                    _this.message = "End month Should be greater than start month. Please Select Carefully";
                    _this.title = "Select Month error";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Actual Template for given period is already exist") {
                    _this.title = "Actual Template Already Exist";
                    _this.message = "Actual Template for given period is already exist";
                    _this.previousExcelColumn = datas.rsBody.previousExcelColumn;
                    _this.previousColumnsId = datas.rsBody.previousColumnId;
                    $("#previousTemplatePopup").modal();
                }
                if (datas.rsBody.message == "Please Select Start Month and End Month") {
                    _this.title = "Select Month error";
                    _this.message = "Please Select Start Month and End Month";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Please Select Performance Component") {
                    _this.title = "Performance error";
                    _this.message = "Please Select Performance Component";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Empty") {
                    _this.title = "Value Error";
                    _this.message = "Please Fill Values";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.excelColumn = datas.rsBody.excelColumn;
                    _this.editHeader = Object.assign([], datas.rsBody.excelColumn);
                    _this.columnData.finalExcel = datas.rsBody.excelColumn;
                    _this.columnData.columnsId = datas.rsBody.columnsId;
                    _this.finalExcel == datas.rsBody.excelColumn;
                    _this.columnsId = datas.rsBody.columnsId;
                    $("#sampleTemplate").css('display', 'block');
                    $("#downTempBtn").css('display', 'block');
                    $("#addNewColumnListBtn").addClass("ng-hide");
                }
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.DownloadTemplate = function () {
        var _this = this;
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12;
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }
        this.columnData.editHeader = Object.assign([], this.editHeader);
        this.httpService.postRequest(this.columnData, '../ManageTemplate/downloadActualTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.msg == "duplicate") {
                    _this.message = datas.rsBody.message;
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.columnData.filepath = datas.rsBody.filepath;
                    _this.filepath = datas.rsBody.filepath;
                    _this.fileName = datas.rsBody.fileName;
                    $("#successPopup").modal();
                }
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.selectExcelColumn = function () {
        $('#columnSeletionPopup').modal();
    };
    ConfigureActualTemplateComponent.prototype.ExportDataSetToExcel = function () {
        var _this = this;
        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        this.keyValueData.keyType = "ACTUAL";
        this.keyValueData.keyName = "Template";
        this.keyValueData.startMonth = this.startMonth;
        this.keyValueData.endMonth = this.endMonth;
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        this.keyValueData.editHeader = Object.assign([], this.editHeader);
        $('#ExportExcelDataBtn').css('display', 'none');
        if (this.startMonth != null || this.startMonth == "") {
            this.keyValueData.startMonth = this.startMonth;
        }
        else {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "") {
            this.keyValueData.endMonth = this.endMonth;
        }
        else {
            this.keyValueData.endMonth = 12;
        }
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/insertActualTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                var path = _this.filepath;
                $('#alertbottomleft').addClass('ng-hide');
                $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + _this.fileName + '&path=' + _this.filepath);
                $('#ExportExcelData')[0].click();
                $('#text').html('-Select Month-');
                $("#collapse1").collapse('hide');
                $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
                _this.excelColumn = datas.rsBody.excelColumn;
                $('#ExportExcelData').css('display', 'none');
                $('#downTempBtn').css('display', 'none');
                $('#ExportExcelDataBtn').css('display', 'none');
                $('#addNewColumnBtn').css('display', 'none');
                $('#ExportExcelData').attr('disabled', true);
                $('#addNewColumnBtn').attr('disabled', true);
                $('#afterDownload').css('display', 'table');
                $('#beforeDownload').css('display', 'none');
            }
        });
    };
    ConfigureActualTemplateComponent.prototype.ngAfterViewInit = function () {
    };
    ConfigureActualTemplateComponent.prototype.ngOnDestroy = function () {
    };
    ConfigureActualTemplateComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ConfigureActualTemplateComponent);
    return ConfigureActualTemplateComponent;
}());
exports.ConfigureActualTemplateComponent = ConfigureActualTemplateComponent;
//# sourceMappingURL=configureactualtemplate.component.js.map