"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var bargraph_component_1 = require("../bargraph/bargraph.component");
//import { LineGraphComponent } from '../linegraph/linegraph.component';
var piechart_component_1 = require("../piechart/piechart.component");
var ad_item_1 = require("./ad-item");
var AdService = /** @class */ (function () {
    function AdService() {
    }
    AdService.prototype.getAds = function (type, data) {
        switch (type) {
            case "bar":
                return new ad_item_1.AdItem(bargraph_component_1.MultiBarGraph, data);
            case "pie":
                return new ad_item_1.AdItem(piechart_component_1.PieChartComponent, data);
            case "line":
                return new ad_item_1.AdItem(bargraph_component_1.MultiBarGraph, data);
            case "BTG":
                return new ad_item_1.AdItem(bargraph_component_1.MultiBarGraph, data);
            case "gauge":
                return new ad_item_1.AdItem(bargraph_component_1.MultiBarGraph, data);
            default:
                break;
        }
    };
    AdService = __decorate([
        core_1.Injectable()
    ], AdService);
    return AdService;
}());
exports.AdService = AdService;
//# sourceMappingURL=ad.service.js.map