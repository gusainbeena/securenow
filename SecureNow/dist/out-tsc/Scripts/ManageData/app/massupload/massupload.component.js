"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var ng2_file_upload_1 = require("ng2-file-upload");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'MassUpload';
var MassUploadComponent = /** @class */ (function () {
    function MassUploadComponent(httpService, router, modal, route) {
        this.httpService = httpService;
        this.router = router;
        this.modal = modal;
        this.route = route;
        this.dashboardData = {};
        this.keyValueData = {};
        this.requestData = {};
        this.targetType = {};
        this.employeeMaster = {};
        this.incentiveData = {};
        this.ChannelList = [];
        this.year = '';
        this.dataSourceFileName = '';
        this.subChannelList = '';
        this.roleList = '';
        this.actualTypeList = '';
        this.weightageList = '';
        this.uploader = new ng2_file_upload_1.FileUploader({ url: '../ManageUploadData/UploadMasterSheet' });
        this.incentive = [];
        this.incentivList = {};
        this.listData = [];
        this.yearData = [];
        this.monthData = [];
        this.quarterData = [];
        this.responseData = {};
        this.downloadFile = {};
        this.error = {};
    }
    MassUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getNextEmployeeList = 0;
        this.getPage = 0;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.yearData = data.rsBody.resulats;
        });
        //getMonth List			
        this.getMonthList();
    };
    MassUploadComponent.prototype.getMonthList = function () {
        var _this = this;
        this.keyValueData = {};
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.quarterData = data.rsBody.resulats;
        });
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.monthData = data.rsBody.resulats;
        });
        this.keyValueData.keyType = "TARGETKEY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.targetType = data.rsBody.resulats;
        });
    };
    MassUploadComponent.prototype.ngOnEmployeeMasterSubmit = function () {
        var _this = this;
        this.employeeMaster.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest(this.employeeMaster, '../ManageUploadData/UploadMasterData', true).subscribe(function (data) {
            if (data == undefined) {
                var id = data.rsBody.data;
            }
            else {
                var id = data.rsBody.data;
            }
            _this.responseData.id = id;
            _this.ngOnCheckStatus(_this.responseData);
        });
    };
    MassUploadComponent.prototype.ngOnCheckStatus = function (inputData) {
        var _this = this;
        this.httpService.postRequest(inputData, '../ManageUploadData/GetUploadStatus', true).subscribe(function (data) {
            _this.responseData = data.rsBody;
            if (_this.responseData != "" && _this.responseData != null) {
                _this.employeeMaster = {};
                _this.router.navigate(['mass-upload']);
                _this.modal.open('sucessPopup');
                _this.ngOnInit();
            }
            else {
                _this.ngOnCheckStatus(inputData);
            }
        });
    };
    MassUploadComponent.prototype.ngOnDeleteRequest = function (target) {
        var _this = this;
        this.employeeMaster = this.listData[target];
        this.employeeMaster.requestType = 'MassUploadForSoldToCode';
        this.httpService.postRequest(this.employeeMaster, '../ManageUploadData/CancelUploadRequest', true).subscribe(function (data) {
            _this.error = data.rsBody;
            _this.modal.open('error');
            _this.ngOnInit();
        });
    };
    MassUploadComponent.prototype.ngOnGetNext = function () {
        var _this = this;
        this.getPage = this.getPage + 1;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    MassUploadComponent.prototype.ngOnGetPrevious = function () {
        var _this = this;
        this.getPage = this.getPage - 1;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    MassUploadComponent.prototype.modalclose = function (id) {
        this.modal.close(id);
    };
    MassUploadComponent.prototype.ngAfterViewInit = function () {
    };
    MassUploadComponent.prototype.fileSelect = function () {
        $('#imageUpload').click();
    };
    MassUploadComponent.prototype.imageUpload = function () {
        var _this = this;
        this.uploader.uploadAll();
        //Get response
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            var responsePath = JSON.parse(response);
            {
                _this.employeeMaster.dataSourceFileName = responsePath.rsBody;
            }
            ;
        };
    };
    MassUploadComponent.prototype.ngOnTemplateDownload = function () {
        var triggerClickk = setTimeout(function () {
            clearTimeout(triggerClickk);
            $('#downloadReport')[0].click();
        });
    };
    MassUploadComponent.prototype.ngOnDownloadSourceReport = function (data) {
        this.downloadFile = data;
        $('#DownloadSourcePath').attr('href', '../Comman/DownloadOpSourceReport?name=' + this.downloadFile);
        $('#DownloadSourcePath')[0].click();
    };
    MassUploadComponent.prototype.ngOnDownloadErrorReport = function (getData) {
        var _this = this;
        this.downloadFile = getData;
        this.httpService.postRequest(this.downloadFile, '../Comman/CheckFileExistOrNot', true).subscribe(function (data) {
            _this.result = data;
            if (_this.result == "NotExist") {
                _this.modal.open('notFound');
                _this.filename = "";
            }
            else {
                $('#DownloadErrorPath').attr('href', '../Comman/DownloadOpErrorReport?name=' + _this.downloadFile);
                $('#DownloadErrorPath')[0].click();
            }
        });
    };
    MassUploadComponent.prototype.ngOnDestroy = function () {
    };
    MassUploadComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            modelservice_1.ModalService,
            router_1.ActivatedRoute])
    ], MassUploadComponent);
    return MassUploadComponent;
}());
exports.MassUploadComponent = MassUploadComponent;
//# sourceMappingURL=massupload.component.js.map