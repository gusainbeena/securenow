"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
require("rxjs/add/observable/interval");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ActualUpload';
var ActualUploadComponent = /** @class */ (function () {
    function ActualUploadComponent(httpService, router, modal, route) {
        this.httpService = httpService;
        this.router = router;
        this.modal = modal;
        this.route = route;
        this.dashboardData = {};
        this.keyValueData = {};
        this.employeeActual = {};
        this.yearData = [];
        this.monthData = [];
        this.productList = [];
        this.userList = [];
        this.model = {};
    }
    ActualUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../common/GetKeyValueData', true).subscribe(function (data) {
            _this.yearData = data.rsBody.resulats;
        });
        this.ngUserType();
        //getMonth List			
        this.getMonthList();
    };
    ActualUploadComponent.prototype.ngActualRequestReject = function () {
        var _this = this;
        this.httpService.postRequest(this.keyValueData, '../ManageUploadData/GetEmployeeList', true).subscribe(function (data) {
            _this.userList = data.rsBody.data;
        });
    };
    ActualUploadComponent.prototype.ngUserType = function () {
        var _this = this;
        this.httpService.postRequest(this.keyValueData, '../ManageUploadData/GetEmployeeList', true).subscribe(function (data) {
            _this.userList = data.rsBody.data;
        });
    };
    ActualUploadComponent.prototype.getMonthList = function () {
        var _this = this;
        this.keyValueData = {};
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../common/GetKeyValueData', true).subscribe(function (data) {
            _this.monthData = data.rsBody.resulats;
        });
        this.getProductList();
    };
    ActualUploadComponent.prototype.getProductList = function () {
        var _this = this;
        this.keyValueData = {};
        this.keyValueData.keyName = "BusinessVertical";
        this.httpService.postRequest(this.keyValueData, '../ManageUploadData/GetProductList', true).subscribe(function (data) {
            _this.productList = data.rsBody.data;
        });
    };
    ActualUploadComponent.prototype.ngActualRequestSubmit = function () {
        var _this = this;
        this.httpService.postRequest(this.employeeActual, '../ManageUploadData/SubmitActualRequest', true).subscribe(function (data) {
            data = data.rsBody.data;
            _this.employeeActual = {};
        });
    };
    ActualUploadComponent.prototype.ngAfterViewInit = function () {
    };
    ActualUploadComponent.prototype.ngOnDestroy = function () {
    };
    ActualUploadComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            modelservice_1.ModalService,
            router_1.ActivatedRoute])
    ], ActualUploadComponent);
    return ActualUploadComponent;
}());
exports.ActualUploadComponent = ActualUploadComponent;
//# sourceMappingURL=actualupload.component.js.map