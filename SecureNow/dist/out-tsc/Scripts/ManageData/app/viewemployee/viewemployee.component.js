"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewEmployee';
var ViewEmployeeComponent = /** @class */ (function () {
    function ViewEmployeeComponent(httpService, router, route, cdref, modal) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.cdref = cdref;
        this.modal = modal;
        this.searchByBranch = {};
        this.keyValueData = {};
        this.keyvalue = {};
        this.responseChannelData = [];
        this.monthData = [];
        this.gradeData = [];
        this.yearData = [];
        this.pageDetail = {};
        this.employeeDetials = {
            employeeRole: [{
                    yearRecorded: "", startMonth: "", endMonth: "", grade: "", channelCode: "", role: "", subChannelCode: "", region: "", area: "", subArea: ""
                }]
        };
        this.getList = {};
        this.roleList = [];
        this.subChannelList = [];
        this.employeeDetails = [];
        this.employeeBaseDetails = [];
        this.responseDeleteRoleData = [];
        this.selectVal = {};
        this.regionList = [];
        this.areaList = [];
        this.subAreaList = [];
        this.requiredFlag = false;
        this.employeeRole = {};
        this.seprationRole = {};
        this.seprationData = {};
        this.selectedId = {};
        this.empRole = {};
        this.errorMessage = {};
        this.sucessResponseData = [];
        this.lastIndex = {};
        this.employeeGrade = { grade: "", startMonth: "", endMonth: "" };
        this.response = {};
    }
    ViewEmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        $("#edit-h-id").css('display', 'block');
        $("#endMonth_id").css("display", "none");
        this.getNextEmployeeList = 0;
        this.httpService.postRequest({}, '../ManageEmployee/GetEmployeeDetailsList', false).subscribe(function (data) {
            _this.responseData = data.rsBody.resulats;
            _this.ngOnGetChannel();
        });
    };
    //get channel list
    ViewEmployeeComponent.prototype.ngOnGetChannel = function () {
        var _this = this;
        //get month data
        this.keyValueData.keyType = "CHANNEL";
        this.keyValueData.keyName = "CHANNEL";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.responseChannelData = data.rsBody.resulats;
            _this.ngOnMonth();
        });
    };
    //get month list
    ViewEmployeeComponent.prototype.ngOnMonth = function () {
        var _this = this;
        //get month data
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetMonthKeyValueData', false).subscribe(function (data) {
            _this.monthData = data.rsBody.resulats;
            _this.updatedmonthData = Object.assign([], _this.monthData);
            _this.ngOnGrade();
        });
    };
    //get grade list
    ViewEmployeeComponent.prototype.ngOnGrade = function () {
        var _this = this;
        //get grade data
        this.keyValueData.keyType = "GRADE";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.gradeData = data.rsBody.resulats;
            _this.ngOnYear();
        });
    };
    //get year list
    ViewEmployeeComponent.prototype.ngOnYear = function () {
        var _this = this;
        //get year data
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.yearData = data.rsBody.resulats;
            _this.yearRecorded = _this.yearData[0].keyValue;
        });
    };
    //Add box for add new grade
    ViewEmployeeComponent.prototype.ngOnAddnewGrade = function (targetIndex) {
        this.employeeGrade = { grade: "", startMonth: "", endMonth: "" };
        $("#gradeUpdateNew_" + targetIndex).css("display", "block");
        $("#addGradeButton_" + targetIndex).css("display", "none");
        $(".removeGradeButton_" + targetIndex).css("display", "block");
    };
    //remove box for grade
    ViewEmployeeComponent.prototype.ngOnRemovenewGrade = function (targetIndex) {
        $("#gradeUpdateNew_" + targetIndex).css("display", "none");
        $("#addGradeButton_" + targetIndex).css("display", "block");
        $(".removeGradeButton_" + targetIndex).css("display", "none");
    };
    ViewEmployeeComponent.prototype.ngOnSearchBranch = function () {
        var _this = this;
        this.httpService.postRequest(this.searchByBranch, '../ManageEmployee/GetEmployeeDetailsList', true).subscribe(function (data) {
            _this.responseData = data.rsBody.resulats;
            _this.ngOnGetChannel();
        });
    };
    ViewEmployeeComponent.prototype.ngOnGetNextEmployee = function () {
        var _this = this;
        this.getNextEmployeeList = this.getNextEmployeeList + 1;
        this.pageDetail.pageNumber = this.getNextEmployeeList;
        this.httpService.postRequest(this.pageDetail, '../ManageEmployee/NextAndPriviousEmployeeDetails', false).subscribe(function (data) {
            _this.responseData = data.rsBody.resulats;
            _this.ngOnGetChannel();
        });
    };
    ViewEmployeeComponent.prototype.ngOnGetPreviousEmployee = function () {
        var _this = this;
        this.getNextEmployeeList = this.getNextEmployeeList - 1;
        this.pageDetail.pageNumber = this.getNextEmployeeList;
        this.httpService.postRequest(this.pageDetail, '../ManageEmployee/NextAndPriviousEmployeeDetails', false).subscribe(function (data) {
            _this.responseData = data.rsBody.resulats;
            _this.ngOnGetChannel();
        });
    };
    ViewEmployeeComponent.prototype.ngOnChangeGetList = function () {
        var _this = this;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].role = '';
            this.employeeDetials.employeeRole[i].region = '';
            this.employeeDetials.employeeRole[i].branchCode = '';
            this.employeeDetials.employeeRole[i].subChannelCode = '';
        }
        this.httpService.postRequest(this.employeeDetials.employeeRole, '../ManageEmployee/GetRoleAreaAndBranchData', false).subscribe(function (data) {
            _this.getList = data.rsBody;
            _this.roleList = _this.getList.resulats;
            if (_this.getList.subChannel.length > 0) {
                _this.subChannelList = _this.getList.subChannel;
                $("#employeeSubChannel").css('display', 'block');
            }
            else {
                $("#employeeSubChannel").css('display', 'none');
            }
            if (_this.roleList != null) {
                $("#employeeRole").css('display', 'block');
            }
            else {
                $("#employeeRole").css('display', 'none');
            }
            $("#employeeBranch").css('display', 'none');
            $("#employeeRegion").css('display', 'none');
            $("#employeeArea").css('display', 'none');
        });
    };
    ViewEmployeeComponent.prototype.ngOnChangeLineOfBusinessGetList = function () {
        var _this = this;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].role = '';
            this.employeeDetials.employeeRole[i].region = '';
            this.employeeDetials.employeeRole[i].branchDetailsFK = '';
        }
        this.httpService.postRequest(this.employeeDetials.employeeRole, '../ManageEmployee/GetRoleAreaAndBranchData', false).subscribe(function (data) {
            _this.getList = data.rsBody;
            _this.roleList = _this.getList.resulats;
            $("#employeeRole").css('display', 'block');
        });
    };
    ViewEmployeeComponent.prototype.ngOnEditEmployeeDetailsSubmit = function (targetIndex) {
        this.selectID = this.responseData[targetIndex].id;
        this.ngOnGetEmployeeDetailsSubmit();
    };
    ViewEmployeeComponent.prototype.ngOnGetEmployeeDetailsSubmit = function () {
        var _this = this;
        this.employeeDetails = {};
        this.employeeDetails.id = this.selectID;
        this.httpService.postRequest(this.employeeDetails, '../ManageEmployee/EditEmployeeDetails', true).subscribe(function (data) {
            _this.employeeBaseDetails = data.rsBody.employeeBaseDetail;
            _this.employeeDetials.responseRoleData = data.rsBody.employeeRole;
            if (_this.employeeBaseDetails.status == "RESIGNED") {
                $(".enable-disable").prop('disabled', true);
                $(".endMonth-enable").prop('disabled', true);
                $("#update-basic-button").css('display', 'none');
                $("#submit-basic-button").css('display', 'none');
                $("#leaving-id").css('display', 'block');
                $("#leaving-id").prop('disabled', 'true');
                $("#endMonth-id").css('display', 'block');
                $("#endMonth-id").prop('disabled', true);
                $("#add-role-button").css('display', 'none');
                _this.employeeDetials.responseRoleData.status = "RESIGNED";
                if (_this.employeeBaseDetails.dolStr != null && _this.employeeBaseDetails.dolStr != "") {
                    var strAry = _this.employeeBaseDetails.dolStr.split("-");
                    _this.employeeBaseDetails.endMonth = strAry[1].charAt(strAry[1].length - 1);
                }
            }
            else {
                $(".enable-disable").prop('disabled', false);
                $(".endMonth-enable").prop('disabled', false);
                $("#update-basic-button").css('display', 'block');
                $("#submit-basic-button").css('display', 'none');
                $("#leaving-id").css('display', 'block');
                $("#endMonth-id").css('display', 'block');
                $("#leaving-id").prop('disabled', 'false');
                $("#endMonth-id").prop('disabled', 'false');
                $("#add-role-button").css('display', 'block');
                _this.employeeDetials.responseRoleData.status = "ACTIVE";
            }
            $("#view-details-id").css('display', 'none');
            $("#edit-details-id").css('display', 'block');
            $("#role-display-details-id").css('display', 'block');
            for (var i = 0; i < _this.employeeDetials.responseRoleData.length; i++) {
                if (_this.employeeDetials.responseRoleData[i].requiredData > 0) {
                    if (_this.employeeDetials.responseRoleData[i].requiredData.indexOf("SUBCHANNELREQUIRED") > -1) {
                        $("#employeeSubChannel" + i).css('display', 'block');
                    }
                    else {
                        $("#employeeSubChannel" + i).css('display', 'none');
                    }
                    if (_this.employeeDetials.responseRoleData[i].requiredData.indexOf("REGIONREQUIRED") > -1) {
                        $("#employeeRegion" + i).css('display', 'block');
                    }
                    else {
                        $("#employeeRegion" + i).css('display', 'none');
                    }
                    if (_this.employeeDetials.responseRoleData[i].requiredData.indexOf("AREACODEREQUIRED") > -1) {
                        $("#employeeArea" + i).css('display', 'block');
                    }
                    else {
                        $("#employeeArea" + i).css('display', 'none');
                    }
                    if (_this.employeeDetials.responseRoleData[i].requiredData.indexOf("SUBAREACODEREQUIRED") > -1) {
                        $("#employeeSubArea" + i).css('display', 'block');
                    }
                    else {
                        $("#employeeSubArea" + i).css('display', 'none');
                    }
                }
            }
        });
    };
    ViewEmployeeComponent.prototype.ngOnAddRoleSubmit = function () {
        this.ngOnEmployeeRoleDetailsSubmit();
    };
    ViewEmployeeComponent.prototype.ngOnEmployeeRoleDetailsSubmit = function () {
        var _this = this;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].adpId = this.employeeBaseDetails.adpId;
            this.employeeDetials.employeeRole[i].doj = this.employeeBaseDetails.doj;
            this.employeeDetials.employeeRole[i].yearRecorded = this.employeeBaseDetails.yearRecorded;
        }
        this.employeeDetials.type = "update";
        this.httpService.postRequest(this.employeeDetials, '../ManageEmployee/CreateEmployeeDetails', true).subscribe(function (data) {
            _this.employeeDetials.responseRoleData = data.rsBody.sucess;
            _this.modal.open('updateSucessPopup');
            //$scope.employeeDetials.employeeRole = {};
            _this.employeeDetials.employeeRole = [{}];
            $("#createRoleDetails").css('display', 'none');
            $("#add-role-button").css('display', 'block');
            $("#remove-role-button").css('display', 'none');
            _this.employeeDetials.responseRoleData.status = "ACTIVE";
        });
    };
    ViewEmployeeComponent.prototype.ngOnUpdateRoleSubmit = function () {
        this.modal.open("responseDataPopup");
        this.ngOnUpdateRoleDetailsSubmit('refresh');
    };
    ViewEmployeeComponent.prototype.ngOnUpdateRoleDetailsSubmit = function (argArr) {
        var _this = this;
        this.httpService.postRequest(this.employeeDetials, '../ManageEmployee/UpdateEmployeeRoleDetails', true).subscribe(function (data) {
            _this.responseDeleteRoleData = data.rsBody.retData;
            if (_this.responseDeleteRoleData.length > 0) {
                _this.modal.open('customerDetailsForUpdateMonth1');
            }
            else {
                _this.modal.open('sucessPopup');
                _this.employeeDetials.responseRoleData.status = "RESIGNED";
            }
        });
    };
    ViewEmployeeComponent.prototype.ngOnGetRequiredField = function (type) {
        this.ngOnChangeGetListInfo(type);
    };
    ViewEmployeeComponent.prototype.ngOnChangeGetListInfo = function (type) {
        var _this = this;
        this.selectVal = type;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].type = type;
        }
        this.httpService.postRequest(this.employeeDetials.employeeRole, '../ManageEmployee/GetRelatedParam', true).subscribe(function (data) {
            if (data.rsBody.type == "ROLE") {
                _this.roleList = data.rsBody.result;
                if (_this.roleList != null) {
                    $("#employeeRole").css('display', 'block');
                }
                else {
                    $("#employeeRole").css('display', 'none');
                }
            }
            $("#teritoryCodeData").css('display', 'none');
            $("#employeeSubChannel").css('display', 'none');
            $("#employeeRegion").css('display', 'none');
            $("#employeeArea").css('display', 'none');
            $("#employeeSubArea").css('display', 'none');
            _this.requiredFlag = false;
            if (data.rsBody.result.length > 0) {
                if (data.rsBody.result.indexOf("SUBCHANNELREQUIRED") > -1) {
                    $("#employeeSubChannel").css('display', 'block');
                    _this.subChannelList = data.rsBody.paramValue.subChannelList;
                    if (_this.selectVal == "") {
                        _this.regionList = [];
                        _this.areaList = [];
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("REGIONREQUIRED") > -1) {
                    $("#employeeRegion").css('display', 'block');
                    if (_this.selectVal == "SUBCHANNELREQUIRED") {
                        _this.regionList = data.rsBody.paramValue.regionList;
                        _this.areaList = [];
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("AREACODEREQUIRED") > -1) {
                    $("#employeeArea").css('display', 'block');
                    $("#teritoryCodeData").css('display', 'block');
                    if (_this.selectVal == "REGIONREQUIRED" || _this.selectVal == "SUBCHANNELREQUIRED") {
                        _this.areaList = data.rsBody.paramValue.areaList;
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("SUBAREACODEREQUIRED") > -1) {
                    $("#employeeSubArea").css('display', 'block');
                    $("#teritoryCodeData").css('display', 'block');
                    if (_this.selectVal == "AREACODEREQUIRED") {
                        _this.subAreaList = data.rsBody.paramValue.subAreaList;
                    }
                }
            }
        });
    };
    ViewEmployeeComponent.prototype.ngOnAddEmployeeRoleDetails = function () {
        $("#remove-role-button").css('display', 'block');
        $("#createRoleDetails").css('display', 'block');
        $("#add-role-button").css('display', 'none');
        $("#addNewRole").css('display', 'block');
        $("#role-list-id").css('display', 'none');
        $("#employeeBranchs").css('display', 'none');
        $("#employeeRegions").css('display', 'none');
        $("#employeeAreas").css('display', 'none');
        $("#employeeRoles").css('display', 'none');
        // Create Array On Click
        this.employeeDetials.employeeRole = {};
        this.employeeDetials.employeeRole = [{
                yearRecorded: this.yearRecorded,
            }];
        $("#remove-role-button").css('display', 'block');
        $('html, body').animate({ scrollTop: 550 }, 'slow');
        return false;
    };
    ViewEmployeeComponent.prototype.ngOnRemoveEmployeeRoleDetails = function () {
        $("#remove-role-button").css('display', 'none');
        $("#add-role-button").css('display', 'block');
        $("#createRoleDetails").css('display', 'none');
        this.employeeDetials.employeeRole = [];
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    };
    ViewEmployeeComponent.prototype.ngOnUpdateEmployeeBasicDetailsSubmit = function () {
        var _this = this;
        this.requiredFlag = true;
        this.httpService.postRequest((this.employeeBaseDetails), '../ManageEmployee/UpdateEmployeeBaseDetails', true).subscribe(function (data) {
            var response = data.rsBody.exceptionBlock.msg.validationException;
            if (response.email == "invalid email address") {
                // $('#errorPopup').bPopup();
                _this.modal.open('updateErrorPopup');
            }
            else if (response.password == "value is required") {
                _this.modal.open('updateErrorPopup');
            }
            else if (response == "success") {
                if (_this.employeeBaseDetails.status != "ACTIVE") {
                }
                _this.modal.open('updateSucessPopup');
            }
        });
    };
    ViewEmployeeComponent.prototype.ngOnSeprationYesSubmit = function () {
        this.selectID = this.employeeRole;
        this.ngOnSeprationEmployeeRole('refresh');
    };
    ViewEmployeeComponent.prototype.ngOnSeprationNoSubmit = function () {
        this.selectID = this.employeeRole;
        this.ngOnSeprationEmployeeRole('refresh');
    };
    ViewEmployeeComponent.prototype.ngOnSeprationEmployeeRole = function (argArr) {
        var _this = this;
        this.modal.open("showCustRelationship1");
        this.seprationRole = this.selectID;
        this.httpService.postRequest(this.seprationRole, '../ManageEmployee/SeprationEmployeeRole', true).subscribe(function (data) {
            _this.seprationData = data.rsBody;
            $("#update-basic-button").css('display', 'none');
            $("#sucess-span").css('display', 'block');
            $(".endMonth-enable").prop('disabled', true);
        });
    };
    ViewEmployeeComponent.prototype.ngOnClickRoleSubmit = function (targetIndex) {
        this.selectedId = this.employeeDetials.responseRoleData[targetIndex].id;
        this.modal.open('deleteEmployeeRolePopup1');
    };
    ViewEmployeeComponent.prototype.ngOnDeleteSubmit = function () {
        this.selectID = this.selectedId;
        this.modal.open("deleteEmployeeRolePopup1");
        this.ngOnDeleteEmployeeRole('refresh');
    };
    ViewEmployeeComponent.prototype.ngOnDeleteEmployeeRole = function (argArr) {
        var _this = this;
        this.empRole.id = this.selectID;
        this.httpService.postRequest(this.empRole, '../ManageEmployee/DeleteEmployeeRole', true).subscribe(function (data) {
            _this.responseDeleteRoleData = data.rsBody.retVal;
            if (_this.responseDeleteRoleData.length > 0) {
                _this.modal.open('customerDetails1');
            }
            else if (data.rsBody.errorMessage != null) {
                _this.errorMessage = data.rsBody.errorMessage;
                _this.modal.open('crSucessPopup');
            }
            else {
                for (var i = 0; i < _this.employeeDetials.responseRoleData.length; i++) {
                    if (_this.employeeDetials.responseRoleData[i].id == _this.selectID) {
                        _this.employeeDetials.responseRoleData.splice(i, 1);
                        break;
                    }
                }
            }
        });
    };
    ViewEmployeeComponent.prototype.ngOnReginedSubmit = function () {
        var inputData = this.employeeBaseDetails.status;
        if (inputData == "RESIGNED") {
            $("#add-role-button").css('display', 'none');
            $("#role-display-details-id").css('display', 'none');
            $(".enable-disable").prop('disabled', true);
            $(".role-endMonth-enable").prop('disabled', true);
            $("#endMonth_id").css("display", "block");
        }
        else {
            $("#add-role-button").css('display', 'block');
            $("#role-display-details-id").css('display', 'block');
            $(".enable-disable").prop('disabled', false);
            $("#endMonth_id").css("display", "none");
        }
    };
    ViewEmployeeComponent.prototype.ngOnCreateVacantRoleSubmit = function () {
        var _this = this;
        this.httpService.postRequest(this.employeeDetials, '../ManageEmployee/CreateVacantRole', true).subscribe(function (data) {
            _this.sucessResponseData = data;
            _this.modal.open('updateSucessPopup');
            $("#Employeebasicdetail").removeClass('active');
            $("#AddRole").removeClass('active');
            $("#BasicDiv").removeClass('active');
            $("#NewRoleDiv").removeClass('active');
            $("#Role").addClass('active');
            $("#RoleDiv").addClass('active');
            _this.ngOnGetEmployeeDetailsSubmit();
        });
    };
    ViewEmployeeComponent.prototype.ngOnEmployeeGradeSubmit = function (index) {
        var _this = this;
        this.lastIndex = index;
        this.employeeGrade.employeeRoleFK = this.employeeDetials.responseRoleData[index].id;
        this.employeeGrade.endMonth = this.employeeDetials.responseRoleData[index].endMonth;
        this.httpService.postRequest(this.employeeGrade, '../ManageEmployee/UpdateEmployeeGrades', true).subscribe(function (data) {
            _this.response = data.rsBody;
            _this.employeeGrade = [];
            _this.ngOnGetEmployeeDetailsSubmit();
        });
    };
    ViewEmployeeComponent.prototype.ngOnUpdateEmployeeRoleSubmit = function () {
        if (this.employeeDetials.responseRoleData.length > 0) {
            this.modal.open('responseDataPopup');
        }
    };
    ViewEmployeeComponent.prototype.ngOnModalClose = function (id) {
        this.modal.close(id);
    };
    ViewEmployeeComponent.prototype.ngOnEmailMeSubmit = function () {
    };
    ViewEmployeeComponent.prototype.ngOnToggleDiv = function () {
    };
    ViewEmployeeComponent.prototype.ngAfterViewInit = function () {
    };
    ViewEmployeeComponent.prototype.ngOnDestroy = function () {
    };
    ViewEmployeeComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute,
            core_1.ChangeDetectorRef,
            modelservice_1.ModalService])
    ], ViewEmployeeComponent);
    return ViewEmployeeComponent;
}());
exports.ViewEmployeeComponent = ViewEmployeeComponent;
//# sourceMappingURL=viewemployee.component.js.map