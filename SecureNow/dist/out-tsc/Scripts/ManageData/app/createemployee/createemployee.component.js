"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateEmployee';
var CreateEmployeeComponent = /** @class */ (function () {
    function CreateEmployeeComponent(httpService, router, route, cdref, modal) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.cdref = cdref;
        this.modal = modal;
        this.monthData = [];
        this.gradeData = [];
        this.yearData = [];
        this.employeeBaseDetails = {};
        this.roleYear = [];
        this.keyValueData = {};
        this.roleList = [];
        this.subChannelList = [];
        this.selectVal = "";
        this.regionList = [];
        this.areaList = [];
        this.subAreaList = [];
        this.employeeDetials = {};
        this.employeeData = {};
        this.channelCode = [];
        this.keyvalue = {};
    }
    CreateEmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        //Remove CSS
        // $("#create-h-id").css('display', 'block');
        $(".form-steps").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        //get channel data
        this.keyValueData.keyType = "CHANNEL";
        this.keyValueData.CHANNEL = "CHANNEL";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.responseChannelData = data.rsBody.resulats;
            _this.ngOnMonth();
        });
    };
    CreateEmployeeComponent.prototype.ngOnMonth = function () {
        var _this = this;
        //get month data
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetMonthKeyValueData', false).subscribe(function (data) {
            _this.monthData = data.rsBody.resulats;
            _this.ngOnGrade();
        });
    };
    CreateEmployeeComponent.prototype.ngOnGrade = function () {
        var _this = this;
        //get grade data
        this.keyValueData.keyType = "GRADE";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.gradeData = data.rsBody.resulats;
            _this.ngOnYear();
        });
    };
    CreateEmployeeComponent.prototype.ngOnYear = function () {
        var _this = this;
        //get year data
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate', false).subscribe(function (data) {
            _this.yearData = data.rsBody.resulats;
            _this.employeeBaseDetails.yearRecorded = _this.yearData[0].keyValue;
            _this.employeeBaseDetails.status = "ACTIVE";
            _this.roleYear = _this.yearData[0].keyValue;
            $(".remove-disable").prop('disabled', false);
        });
    };
    CreateEmployeeComponent.prototype.ngOnGetRequiredField = function (type, targetIndex) {
        this.ngOnChangeGetListInfo(type);
    };
    CreateEmployeeComponent.prototype.ngOnChangeGetListInfo = function (type) {
        var _this = this;
        //get year data
        this.selectVal = type;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].type = type;
        }
        this.httpService.postRequest(this.employeeDetials.employeeRole, '../ManageEmployee/GetRelatedParam', true).subscribe(function (data) {
            if (data.rsBody.type == "ROLE") {
                _this.roleList = data.rsBody.result;
                if (_this.roleList != null) {
                    $("#employeeRole").css('display', 'block');
                }
                else {
                    $("#employeeRole").css('display', 'none');
                }
            }
            $("#teritoryCodeData").css('display', 'none');
            $("#employeeSubChannel").css('display', 'none');
            $("#employeeRegion").css('display', 'none');
            $("#employeeArea").css('display', 'none');
            $("#employeeSubArea").css('display', 'none');
            _this.requiredFlag = false;
            if (data.rsBody.result.length > 0) {
                if (data.rsBody.result.indexOf("SUBCHANNELREQUIRED") > -1) {
                    $("#employeeSubChannel").css('display', 'block');
                    _this.subChannelList = data.rsBody.paramValue.subChannelList;
                    if (_this.selectVal == "") {
                        _this.regionList = [];
                        _this.areaList = [];
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("REGIONREQUIRED") > -1) {
                    $("#employeeRegion").css('display', 'block');
                    if (_this.selectVal == "SUBCHANNELREQUIRED") {
                        _this.regionList = data.rsBody.paramValue.regionList;
                        _this.areaList = [];
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("AREACODEREQUIRED") > -1) {
                    $("#employeeArea").css('display', 'block');
                    $("#teritoryCodeData").css('display', 'block');
                    if (_this.selectVal == "REGIONREQUIRED" || _this.selectVal == "SUBCHANNELREQUIRED") {
                        _this.areaList = data.rsBody.paramValue.areaList;
                        _this.subAreaList = [];
                    }
                }
                if (data.rsBody.result.indexOf("SUBAREACODEREQUIRED") > -1) {
                    $("#employeeSubArea").css('display', 'block');
                    $("#teritoryCodeData").css('display', 'block');
                    if (_this.selectVal == "AREACODEREQUIRED") {
                        _this.subAreaList = data.rsBody.paramValue.subAreaList;
                    }
                }
            }
        });
    };
    CreateEmployeeComponent.prototype.ngOnemployeeBasicDetailsSubmit = function () {
        var _this = this;
        this.httpService.postRequest(this.employeeBaseDetails, '../ManageEmployee/ValidateEmployeeBasicDetails', true).subscribe(function (data) {
            _this.employeeData = data.rsBody;
            $("#step-one").css('display', 'none');
            $(".step-one").removeClass('progress-bar-success');
            $(".step-one").addClass('progress-bar-inverse');
            $(".step-two").addClass('progress-bar-success');
            $(".step-two").removeClass('progress-bar-inverse');
            $("#step-twos").css('display', 'block');
            if (_this.employeeDetials.employeeRole == undefined) {
                _this.employeeDetials.employeeRole = [{
                        yearRecorded: _this.roleYear
                    }];
            }
        });
    };
    CreateEmployeeComponent.prototype.ngOnpreviouSubmit = function () {
        $("#step-twos").css('display', 'none');
        $(".step-two").removeClass('progress-bar-success');
        $(".step-two").addClass('progress-bar-inverse');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    };
    CreateEmployeeComponent.prototype.ngOnemployeeDetailsSubmit = function () {
        var _this = this;
        this.employeeDetials.employeeBaseDetail = this.employeeData;
        this.employeeDetials.type = "create";
        this.httpService.postRequest(this.employeeDetials, '../ManageEmployee/CreateEmployeeDetails', true).subscribe(function (data) {
            _this.responseEmpData = data.rsBody.resultsDb;
            _this.customerData = data.rsBody.customer;
            if (data.rsBody.message == "sucess") {
                setTimeout(function () {
                    if (data.rsBody.customer != "") {
                        this.modal.open('sucessCustomerPopup');
                    }
                    else {
                        this.modal.open('sucessPopup');
                    }
                }, 1000);
            }
            else {
                _this.modal.open('customerDetailsForUpdateMonth');
            }
        });
    };
    CreateEmployeeComponent.prototype.ngOncloseSuccessPopUp = function (id) {
        this.modal.close(id);
        if (this.customerData != "") {
            //$('#sucessCustomerPopup').modal().hide();
            this.modal.open("sucessCustomerPopup");
            $('.modal-backdrop').remove();
            setTimeout(function () {
                // Utilities.transitionTo('create-employee', $scope);
                this.router.navigate(['create-employee'], this.responseEmpsData);
            }, 1000);
        }
        else {
            this.modal.open("sucessPopup");
            $('.modal-backdrop').remove();
            setTimeout(function () {
                //  Utilities.transitionTo('create-employee', $scope);
                this.router.navigate(['create-employee'], this.responseEmpsData);
            }, 1000);
        }
    };
    CreateEmployeeComponent.prototype.ngOnendExistRoleSubmit = function () {
        var _this = this;
        this.httpService.postRequest(this.employeeDetials, '../ManageEmployee/EndExistingRoleAndCreateEmployeeDetails', true).subscribe(function (data) {
            _this.responseEmpsData = data;
            _this.modal.open('sucessPopup');
            //Utilities.transitionTo('view-employee', $scope);
            _this.router.navigate(['view-employee'], _this.responseEmpsData);
        });
    };
    CreateEmployeeComponent.prototype.ngOnnewEntry = function (obj, index) {
        this.keyvalue.keyType = this.channelCode;
        this.keyvalue.keyName = obj;
        if (obj == "SUBAREACODE") {
            this.keyvalue.relatedKeyType = "AREACODE";
            this.keyvalue.relatedKeyValue = this.employeeDetials.employeeRole[index].areaCode[0];
        }
        this.modal.open('myModal');
    };
    CreateEmployeeComponent.prototype.ngAfterViewInit = function () {
    };
    CreateEmployeeComponent.prototype.ngOnDestroy = function () {
    };
    CreateEmployeeComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute,
            core_1.ChangeDetectorRef,
            modelservice_1.ModalService])
    ], CreateEmployeeComponent);
    return CreateEmployeeComponent;
}());
exports.CreateEmployeeComponent = CreateEmployeeComponent;
//# sourceMappingURL=createemployee.component.js.map