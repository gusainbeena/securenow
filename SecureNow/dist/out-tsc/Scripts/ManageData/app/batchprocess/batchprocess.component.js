"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'BatchProcess';
var BatchProcessComponent = /** @class */ (function () {
    function BatchProcessComponent(httpService, router, modal, route) {
        this.httpService = httpService;
        this.router = router;
        this.modal = modal;
        this.route = route;
        this.time = { hour: 13, minute: 30 };
        this.meridian = true;
        this.dashboardData = {};
        this.request = {};
        this.day = '';
        this.month = '';
        this.year = '';
        this.responseData = [];
        this.batchRequests = [];
        this.batchParameters = [];
        this.bprocess = [];
        this.listData = [];
        this.uploadedBatch = {};
        this.requestData = {};
        this.batchDate = {};
        this.bprocessDTO = [];
        this.allOU = [];
        this.timer = {
            hour: null,
            minute: null,
            second: null,
        };
        this.hour = 0;
    }
    BatchProcessComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getPage = 0;
        this.httpService.postRequest({}, '/BatchProcess/GetUserDetails', true).subscribe(function (data) {
            if (data.rsBody.result = "success") {
                if (data.rsBody.bprocess.length == 0) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display');
                    _this.batchRequests = data.rsBody.batchRequests;
                }
                // next and previous 
                if (_this.batchRequests.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                }
                _this.batchDate.year = data.rsBody.batchDate[0];
                _this.batchDate.month = data.rsBody.batchDate[1];
                _this.batchDate.day = data.rsBody.batchDate[2];
                //this.batchParameters = data.rsBody.batchParameters;
                _this.bprocess = data.rsBody.bprocess;
                _this.currentOU = data.rsBody.currentOU;
                _this.allOU = data.rsBody.allOU;
                if (_this.allOU.length != 0) {
                    $("#ou").removeClass("ng-hide");
                }
                _this.selectOU = data.rsBody.selectedOU;
            }
        });
    };
    BatchProcessComponent.prototype.selectOUfunction = function (item) {
        this.selectOU = item;
    };
    BatchProcessComponent.prototype.go = function () {
        var _this = this;
        this.year = this.batchDate.year.toString();
        this.month = this.batchDate.month.toString();
        this.day = this.batchDate.day.toString();
        if (this.month.length == 1) {
            this.month = "0" + this.month;
        }
        if (this.day.length == 1) {
            this.day = "0" + this.day;
        }
        if (this.selectOU == null || this.selectOU == undefined) {
            this.selectOU = "INDIA";
            this.uploadedBatch.selectOU = this.selectOU;
        }
        else {
            this.uploadedBatch.selectOU = this.selectOU;
        }
        this.uploadedBatch.batchDate = this.year + "-" + this.month + "-" + this.day;
        this.httpService.postRequest(this.uploadedBatch, '/BatchProcess/FetchData', true).subscribe(function (data) {
            if (data.rsBody.result = "success") {
                _this.bprocess = data.rsBody.bprocess;
                if (_this.bprocess.length == 0) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
            }
        });
    };
    BatchProcessComponent.prototype.rescheduleBatch = function (e) {
        var _this = this;
        this.index = e;
        this.batchId = this.batchRequests[e].id;
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest(this.uploadedBatch, '/BatchProcess/RescheduleBatch', true).subscribe(function (data) {
            if (data.rsBody.result = "success") {
                _this.request = data.rsBody.request;
                var time = _this.request['scheduleTime'];
                //time = this.convert(time);
                var ampm = time.slice(5);
                _this.hour = +(time.slice(0, -6));
                var minute = time.slice(2, -3);
                if (ampm == "PM") {
                    _this.hour = _this.hour + 12;
                }
                _this.timer.hour = _this.hour.toString();
                _this.timer.minute = minute;
                _this.timer.second = "0";
                var id = _this.request['id'];
                _this.batchRequests[_this.index].scheduleTime = _this.timer;
                $("#scheduleTimePopup").modal();
            }
        });
    };
    BatchProcessComponent.prototype.resetRunTime = function () {
        var _this = this;
        var hour = this.time.hour;
        var ampm = "AM";
        if (hour > 12) {
            hour = hour - 12;
            ampm = "PM";
        }
        if (hour == 0) {
            hour = 12;
        }
        this.time.hour = hour;
        this.uploadedBatch.scheduleTime = this.time.hour + ":" + this.time.minute + " " + ampm;
        //this.uploadedBatch.scheduleTime = this.time;
        this.httpService.postRequest(this.uploadedBatch, '/BatchProcess/ResetTime', true).subscribe(function (data) {
            if (data.rsBody.result = "success") {
                _this.request = data.rsBody.request;
                var time = _this.request['scheduleTime'];
                //time = this.convert(time);
                var ampm = time.slice(5);
                _this.hour = (time.slice(0, -6));
                var minute = time.slice(2, -3);
                if (ampm == "PM") {
                    _this.hour = _this.hour + 12;
                }
                _this.timer.hour = _this.hour.toString();
                _this.timer.minute = minute;
                _this.timer.second = "0";
                var id = _this.request['id'];
                _this.batchRequests[_this.index].scheduleTime = _this.timer;
                //this.batchRequests[this.index].scheduleTime = this.request['scheduleTime'];
                //$("#scheduleTimePopup").modal();
                _this.ngOnInit();
            }
        });
    };
    BatchProcessComponent.prototype.getNextBatch = function () {
        var _this = this;
        this.getPage = this.getPage + 1;
        this.uploadedBatch.page = this.getPage;
        //this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest(this.uploadedBatch, '../BatchProcess/BatchDataList', true).subscribe(function (data) {
            _this.bprocessDTO = data.rsBody.bprocessDTO;
            $("#mainRow").css('display', 'none');
            $("#backRow").css('display', 'flex');
            if (_this.bprocessDTO == null) {
                $("#table_found_id").css('display', 'none');
                $("#table_not_found_id").css('display', 'block');
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#table_found_id").css('display', 'block');
                $("#table_not_found_id").css('display', 'none');
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'none');
            }
            // next and previous 
            if (_this.bprocessDTO.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    BatchProcessComponent.prototype.getPreviousBatch = function () {
        var _this = this;
        if (this.getPage == 0) {
            $('#example1_previous').prop('disabled', true);
        }
        this.getPage = this.getPage - 1;
        this.uploadedBatch.page = this.getPage;
        this.uploadedBatch.id = this.batchId;
        //this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest(this.uploadedBatch, '../BatchProcess/BatchDataList', true).subscribe(function (data) {
            _this.bprocessDTO = data.rsBody.bprocessDTO;
            $("#mainRow").css('display', 'none');
            $("#backRow").css('display', 'flex');
            if (_this.bprocessDTO == null) {
                $("#table_found_id").css('display', 'none');
                $("#table_not_found_id").css('display', 'block');
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#table_found_id").css('display', 'block');
                $("#table_not_found_id").css('display', 'none');
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'none');
            }
            // next and previous 
            if (_this.bprocessDTO.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    BatchProcessComponent.prototype.batchLogs = function (e) {
        this.batchId = this.batchRequests[e].id;
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest(this.uploadedBatch, '../BatchProcess/BatchLogs', true).subscribe(function (data) {
            if (data.rsBody.result == "success") {
                if (data.rsBody.logsAvail == "error") {
                    $("#noLogFound").modal();
                }
            }
        });
    };
    BatchProcessComponent.prototype.runBatch = function (e) {
        this.batchId = this.batchRequests[e].id;
    };
    BatchProcessComponent.prototype.back = function () {
        $("#mainRow").css('display', 'flex');
        $("#backRow").css('display', 'none');
        $("#table_found_id").css('display', 'none');
        $("#row_not_found_id").css('display', 'none');
        $("#row_table_id").css('display', 'block');
    };
    BatchProcessComponent.prototype.Explore = function (e) {
        var _this = this;
        this.batchId = this.bprocess[e].id;
        this.uploadedBatch.id = this.batchId;
        if (this.selectOU == null || this.selectOU == undefined) {
            this.selectOU = "INDIA";
            this.uploadedBatch.selectedOU = this.selectOU;
        }
        else {
            this.uploadedBatch.selectedOU = this.selectOU;
        }
        this.httpService.postRequest(this.uploadedBatch, '../BatchProcess/Explore', true).subscribe(function (data) {
            if (data.rsBody.result == "success") {
                $("#mainRow").css('display', 'none');
                $("#backRow").css('display', 'flex');
                _this.bprocessDTO = data.rsBody.bprocessDTO;
                if (_this.bprocessDTO.count != 0) {
                    $("#table_found_id").css('display', 'block');
                    $("#table_not_found_id").css('display', 'none');
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#table_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                    $("#table_found_id").css('display', 'none');
                }
                // next and previous 
                if (_this.bprocessDTO.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                }
            }
        });
    };
    BatchProcessComponent.prototype.viewPreviousBatches = function (e) {
        var _this = this;
        this.batchId = this.batchParameters[e].id;
        var id = e;
        $("#innerTable").removeClass('d-none');
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest(this.uploadedBatch, '../BatchProcess/ListedBatch', true).subscribe(function (data) {
            if (data.rsBody.result == "success") {
                _this.batchRequests = data.rsBody.batchRequests;
            }
        });
    };
    BatchProcessComponent.prototype.ViewErros = function (e) {
        this.batchId = this.batchParameters[e].id;
        var id = e;
    };
    BatchProcessComponent.prototype.getNextBatchParameters = function () {
        var _this = this;
        this.getPage = this.getPage + 1;
        this.requestData.page = this.getPage;
        this.httpService.postRequest(this.requestData, '../BatchProcess/BatchParameterList', true).subscribe(function (data) {
            // this.batchRequests = data.rsBody.data;
            _this.batchParameters = data.rsBody.batchParameters;
            _this.currentOU = data.rsBody.currentOU;
            if (_this.batchParameters == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.batchParameters.length < 8) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    BatchProcessComponent.prototype.getPreviousBatchParameters = function () {
        var _this = this;
        if (this.getPage == 0) {
            $('#example1_previous').prop('disabled', true);
        }
        this.getPage = this.getPage - 1;
        this.requestData.page = this.getPage;
        this.httpService.postRequest(this.requestData, '../BatchProcess/BatchParameterList', true).subscribe(function (data) {
            _this.batchParameters = data.rsBody.batchParameters;
            if (_this.batchParameters == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.batchParameters.length < 8) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    BatchProcessComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            modelservice_1.ModalService,
            router_1.ActivatedRoute])
    ], BatchProcessComponent);
    return BatchProcessComponent;
}());
exports.BatchProcessComponent = BatchProcessComponent;
//# sourceMappingURL=batchprocess.component.js.map