"use strict";
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var _global = (typeof window === 'undefined' ? core_1.ɵglobal : window);
/**
 * Jasmine matching function with Angular matchers mixed in.
 *
 * ## Example
 *
 * {@example testing/ts/matchers.ts region='toHaveText'}
 */
exports.expect = _global.expect;
// Some Map polyfills don't polyfill Map.toString correctly, which
// gives us bad error messages in tests.
// The only way to do this in Jasmine is to monkey patch a method
// to the object :-(
Map.prototype['jasmineToString'] = function () {
    var m = this;
    if (!m) {
        return '' + m;
    }
    var res = [];
    m.forEach(function (v, k) { res.push(k + ":" + v); });
    return "{ " + res.join(',') + " }";
};
_global.beforeEach(function () {
    jasmine.addMatchers({
        // Custom handler for Map as Jasmine does not support it yet
        toEqual: function (util) {
            return {
                compare: function (actual, expected) {
                    return { pass: util.equals(actual, expected, [compareMap]) };
                }
            };
            function compareMap(actual, expected) {
                if (actual instanceof Map) {
                    var pass_1 = actual.size === expected.size;
                    if (pass_1) {
                        actual.forEach(function (v, k) { pass_1 = pass_1 && util.equals(v, expected.get(k)); });
                    }
                    return pass_1;
                }
                else {
                    // TODO(misko): we should change the return, but jasmine.d.ts is not null safe
                    // tslint:disable-next-line:no-non-null-assertion
                    return undefined;
                }
            }
        },
        toBePromise: function () {
            return {
                compare: function (actual) {
                    var pass = typeof actual === 'object' && typeof actual.then === 'function';
                    return { pass: pass, get message() { return 'Expected ' + actual + ' to be a promise'; } };
                }
            };
        },
        toBeAnInstanceOf: function () {
            return {
                compare: function (actual, expectedClass) {
                    var pass = typeof actual === 'object' && actual instanceof expectedClass;
                    return {
                        pass: pass,
                        get message() {
                            return 'Expected ' + actual + ' to be an instance of ' + expectedClass;
                        }
                    };
                }
            };
        },
        toHaveText: function () {
            return {
                compare: function (actual, expectedText) {
                    var actualText = elementText(actual);
                    return {
                        pass: actualText === expectedText,
                        get message() { return 'Expected ' + actualText + ' to be equal to ' + expectedText; }
                    };
                }
            };
        },
        toHaveTrimmedText: function () {
            return {
                compare: function (actual, expectedText) {
                    var actualText = elementText(actual).trim();
                    return {
                        pass: actualText === expectedText,
                        get message() { return 'Expected ' + actualText + ' to be equal to ' + expectedText; }
                    };
                }
            };
        },
        toHaveCssClass: function () {
            return { compare: buildError(false), negativeCompare: buildError(true) };
            function buildError(isNot) {
                return function (actual, className) {
                    return {
                        pass: platform_browser_1.ɵgetDOM().hasClass(actual, className) === !isNot,
                        get message() {
                            return "Expected " + actual.outerHTML + " " + (isNot ? 'not ' : '') + "to contain the CSS class \"" + className + "\"";
                        }
                    };
                };
            }
        },
        toHaveCssStyle: function () {
            return {
                compare: function (actual, styles) {
                    var allPassed;
                    if (typeof styles === 'string') {
                        allPassed = platform_browser_1.ɵgetDOM().hasStyle(actual, styles);
                    }
                    else {
                        allPassed = Object.keys(styles).length !== 0;
                        Object.keys(styles).forEach(function (prop) {
                            allPassed = allPassed && platform_browser_1.ɵgetDOM().hasStyle(actual, prop, styles[prop]);
                        });
                    }
                    return {
                        pass: allPassed,
                        get message() {
                            var expectedValueStr = typeof styles === 'string' ? styles : JSON.stringify(styles);
                            return "Expected " + actual.outerHTML + " " + (!allPassed ? ' ' : 'not ') + "to contain the\n                      CSS " + (typeof styles === 'string' ? 'property' : 'styles') + " \"" + expectedValueStr + "\"";
                        }
                    };
                }
            };
        },
        toContainError: function () {
            return {
                compare: function (actual, expectedText) {
                    var errorMessage = actual.toString();
                    return {
                        pass: errorMessage.indexOf(expectedText) > -1,
                        get message() { return 'Expected ' + errorMessage + ' to contain ' + expectedText; }
                    };
                }
            };
        },
        toImplement: function () {
            return {
                compare: function (actualObject, expectedInterface) {
                    var intProps = Object.keys(expectedInterface.prototype);
                    var missedMethods = [];
                    intProps.forEach(function (k) {
                        if (!actualObject.constructor.prototype[k]) {
                            missedMethods.push(k);
                        }
                    });
                    return {
                        pass: missedMethods.length === 0,
                        get message() {
                            return 'Expected ' + actualObject + ' to have the following methods: ' +
                                missedMethods.join(', ');
                        }
                    };
                }
            };
        },
        toHaveMap: function () {
            return {
                compare: function (actualObject, expected) {
                    var pass = true;
                    var failureName = '';
                    for (var propertyName in expected) {
                        if (!isMatchProperties(expected[propertyName], actualObject[propertyName])) {
                            pass = false;
                            failureName = propertyName;
                            break;
                        }
                    }
                    return {
                        pass: pass,
                        get message() {
                            return 'Expected ' + failureName + ' to match value';
                        }
                    };
                }
            };
            function isMatchProperties(src, dest) {
                if (dest instanceof Object) {
                    for (var propertyName in dest) {
                        if (!isMatchProperties(src[propertyName], dest[propertyName])) {
                            return false;
                        }
                    }
                }
                else {
                    if (src !== dest) {
                        return false;
                    }
                }
                return true;
            }
        }
    });
});
function elementText(n) {
    var hasNodes = function (node) {
        var children = platform_browser_1.ɵgetDOM().childNodes(node);
        return children && children.length > 0;
    };
    if (n instanceof Array) {
        return n.map(elementText).join('');
    }
    if (platform_browser_1.ɵgetDOM().isCommentNode(n)) {
        return '';
    }
    if (platform_browser_1.ɵgetDOM().isElementNode(n) && platform_browser_1.ɵgetDOM().tagName(n) === 'CONTENT') {
        return elementText(Array.prototype.slice.apply(platform_browser_1.ɵgetDOM().getDistributedNodes(n)));
    }
    if (platform_browser_1.ɵgetDOM().hasShadowRoot(n)) {
        return elementText(platform_browser_1.ɵgetDOM().childNodesAsList(platform_browser_1.ɵgetDOM().getShadowRoot(n)));
    }
    if (hasNodes(n)) {
        return elementText(platform_browser_1.ɵgetDOM().childNodesAsList(n));
    }
    // tslint:disable-next-line:no-non-null-assertion
    return platform_browser_1.ɵgetDOM().getText(n);
}
//# sourceMappingURL=jasmine-matchers.js.map