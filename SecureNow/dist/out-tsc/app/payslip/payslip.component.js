"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'PaySlip';
var PaySlipComponent = /** @class */ (function () {
    function PaySlipComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        //public keyValueData: Object = {};
        this.keyValueData = {};
        this.responseData = [];
        this.IncentiveCalculationRequest = {};
        this.incentiveApproveDetails = {};
        this.viewIncentiveDetails = [];
        this.updateResponseData = {};
        this.employeeRole = [];
        this.responseStatus = {};
        this.monthData = {};
        this.employeeSheet = {};
        this.incentiveDetails = {};
        this.reportsType = {};
        this.ChannelList = [];
        this.incentivList = [];
        this.channel = '';
        this.subChannelList = '';
        this.roleList = '';
        this.actualTypeList = '';
        this.weightageList = '';
        this.keyType = '';
        this.keyName = '';
        this.subChannel = '';
        this.actualType = '';
        this.incentive = [];
        this.yearData = {};
        this.qtrList = {};
        this.MonthList = {};
        this.incentiveInitArray = [{
                id: 'The performance numbers generated by the system are incorrect',
                text: 'The performance numbers generated by the system are incorrect'
            }, {
                id: 'There are items pending at my end that could impact the performance numbers',
                text: 'There are items pending at my end that could impact the performance numbers'
            }];
    }
    PaySlipComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getNextEmployeeList = 0;
        this.pageNumber = 0;
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.monthData = datas.rsBody;
                _this.getIncentiveList();
            }
        });
    };
    PaySlipComponent.prototype.getIncentiveList = function () {
        var _this = this;
        this.getNextEmployeeList = 0;
        this.httpService.postRequest({}, '../Comman/GetIncentiveList', true).subscribe(function (data) {
            if (data.rsBody.result == "success") {
                _this.responseData = data.rsBody.resulats;
                for (var i = 0; i < _this.responseData.length; i++) {
                    for (var j = 0; j < _this.monthData.length; j++) {
                        if (_this.responseData[i].incentiveStartMonth == _this.monthData[j].keyValue) {
                            _this.responseData[i].incentiveStartMonth = _this.monthData[j].description;
                        }
                        if (_this.responseData[i].incentiveEndMonth == _this.monthData[j].keyValue) {
                            _this.responseData[i].incentiveEndMonth = _this.monthData[j].description;
                        }
                    }
                }
            }
            if (_this.responseData.length < 20) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getNextEmployeeList > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getNextEmployeeList == 0) {
                $("#example1_previous").addClass("disabled");
            }
            if (data.rsBody.sucess == "No Data Found") {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            _this.getYear();
        });
    };
    PaySlipComponent.prototype.getQTR = function () {
        var _this = this;
        this.keyValueData.keyType = "QTR";
        this.httpService.postRequest(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.qtrList = data.rsBody;
                _this.getMonth();
            }
        });
    };
    PaySlipComponent.prototype.getMonth = function () {
        var _this = this;
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.MonthList = data.rsBody;
            }
        });
    };
    PaySlipComponent.prototype.getYear = function () {
        var _this = this;
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.yearData = data.rsBody;
                _this.getQTR();
            }
        });
    };
    PaySlipComponent.prototype.viewIncentiveSubmit = function (e) {
        this.currentQuarter = this.responseData[e].quarter;
        this.generatePayslip = true;
        this.selectID = this.responseData[e].id;
        this.getIncentiveSubmit();
    };
    //Refresh List
    PaySlipComponent.prototype.popUpData = function () {
        this.getIncentiveSubmit();
    };
    PaySlipComponent.prototype.getIncentiveSubmit = function () {
        var _this = this;
        this.incentive = {};
        this.incentive.id = this.selectID;
        this.httpService.postRequest(this.incentive, '../AdminDepartment/GetViewIncentive', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.IncentiveCalculationRequest = data.rsBody.retval.incentiveRequest[0];
                _this.generatePayslip = true;
                _this.employeeRole = data.rsBody.retval.incentiveCofirmation;
                for (var i = 0; i < _this.employeeRole.length; i++) {
                    _this.employeeRole[i].Selected = false;
                }
                if (_this.IncentiveCalculationRequest.status === "CLOSED") {
                    $(".enable-disable").prop('disabled', true);
                    $("#view-emp-details-id").css('display', 'block');
                }
                else if (_this.IncentiveCalculationRequest.status === "CANCELLED") {
                    $(".enable-disable").prop('disabled', true);
                    $("#view-emp-details-id").css('dispaly', 'none');
                }
                else {
                    $(".enable-disable").prop('disabled', false);
                    $("#view-emp-details-id").css('display', 'block');
                }
                $("#view-table-id").css('display', 'none');
                $("#edit-details-id").css('display', 'block');
            }
        });
    };
    PaySlipComponent.prototype.searchBranch = function () {
        var _this = this;
        this.httpService.postRequest(this.incentiveDetails, '../Comman/GetIncentiveList', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.qtrList = data.rsBody.resulats;
                _this.responseData = data.rsBody.resulats;
                for (var i = 0; i < _this.responseData.length; i++) {
                    for (var j = 0; j < _this.monthData.length; j++) {
                        if (_this.responseData[i].incentiveStartMonth == _this.monthData[j].keyValue) {
                            _this.responseData[i].incentiveStartMonth = _this.monthData[j].description;
                        }
                        if (_this.responseData[i].incentiveEndMonth == _this.monthData[j].keyValue) {
                            _this.responseData[i].incentiveEndMonth = _this.monthData[j].description;
                        }
                    }
                    if (_this.responseData.length < 20) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (_this.getNextEmployeeList > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (_this.getNextEmployeeList == 0) {
                        $("#example1_previous").addClass("disabled");
                    }
                    if (data.rsBody == "No Data Found") {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display', 'block');
                    }
                    _this.getYear();
                }
            }
        });
    };
    /* reGenerateSheetSubmit() {
 
         this.httpService.postRequest<PaySlipComponent>({}, '/EmployeeDepartment/ApproveAndRejectIncentiveSheet').subscribe(
             data => {
                 if (data.rsBody.result === 'success') {
                     if (this.keyType == "GRADE") {
                         $('#incentiveBasicPay').removeClass('ng-hide');
                     }
                     else {
                         $('#incentiveBasicPay').addClass('ng-hide');
                     }
 
                 }
             });
     }*/
    PaySlipComponent.prototype.getNextEmployee = function () {
        var _this = this;
        this.getNextEmployeeList = this.getNextEmployeeList + 1;
        this.pageNumber = this.getNextEmployeeList;
        this.incentivList['pageNumber'] = this.pageNumber;
        this.httpService.postRequest(this.incentivList, '../AdminDepartment/NextAndPriviousIncentive', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.incentive = data.rsBody.incentive;
                $("#example1_previous").removeClass("ng-hide");
                if (_this.incentive.length < 20) {
                    $("#example1_next").addClass("ng-hide");
                }
            }
        });
    };
    PaySlipComponent.prototype.getPreviousEmployee = function () {
        var _this = this;
        this.getNextEmployeeList = this.getNextEmployeeList - 1;
        this.pageNumber = this.getNextEmployeeList;
        this.incentivList['pageNumber'] = this.pageNumber;
        this.httpService.postRequest(this.incentivList, '../AdminDepartment/NextAndPriviousIncentive', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.incentive = data.rsBody.incentive;
                $("#example1_previous").removeClass("ng-hide");
                if (_this.pageNumber <= 0) {
                    $("#example1_previous").addClass("ng-hide");
                }
                //sthis.ngOnInit();
            }
        });
    };
    PaySlipComponent.prototype.incentiveStatus = function () {
        if (this.IncentiveCalculationRequest.status == "CLOSED") {
            $("#update-basic-button").removeClass('ng-hide');
            $("#CommentBox").css('display', 'block');
        }
        else if (this.IncentiveCalculationRequest.status == "CANCELLED") {
            $("#update-basic-button").removeClass('ng-hide');
            $(".view-incentive-class").css('display', 'none');
            $("#CommentBox").css('display', 'block');
        }
        else {
            $("#update-basic-button").addClass('ng-hide');
            $("#CommentBox").css('display', 'none');
        }
    };
    //Approve request
    PaySlipComponent.prototype.approveIncentive = function (e) {
        this.id = this.employeeRole[e].id;
        $("#responseDataPopup").modal();
    };
    PaySlipComponent.prototype.approveIncentiveSubmit = function (e) {
        var _this = this;
        $("#responseDataPopup").modal('hide');
        this.incentiveApproveDetails = {};
        this.incentiveApproveDetails.id = this.id;
        this.incentiveApproveDetails.status = "APPROVED";
        this.httpService.postRequest(this.incentiveApproveDetails, '../EmployeeDepartment/ApproveAndRejectIncentiveSheet', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                $("#deleteCorporatePopup").modal('hide');
                _this.responseStatus = data.rsBody;
                $("#sucessPopup").modal();
            }
        });
    };
    PaySlipComponent.prototype.rejectIncentiveSubmit = function () {
        var _this = this;
        $("#deleteCorporatePopup").modal('hide');
        this.incentiveApproveDetails.id = this.id;
        this.incentiveApproveDetails.status = "REJECTED";
        this.httpService.postRequest(this.incentiveApproveDetails, '../EmployeeDepartment/ApproveAndRejectIncentiveSheet', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                $("#deleteCorporatePopup").modal('hide');
                _this.responseStatus = data.rsBody;
                $("#sucessPopup").modal();
            }
        });
    };
    // Update Incentive Request
    PaySlipComponent.prototype.updateIncentiveSubmit = function () {
        var _this = this;
        this.httpService.postRequest(this.IncentiveCalculationRequest, '../AdminDepartment/UpdateIncentive', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.updateResponseData = data.rsBody;
                if (_this.updateResponseData == "sucess") {
                    $("#sucessPopup").modal();
                    $("#update-basic-button").addClass('ng-hide');
                    $("#sucess-basic-button").removeClass('ng-hide');
                    $(".enable-disable").prop('disabled', true);
                }
                else {
                    $("#customerDetails").modal();
                }
            }
        });
    };
    // Downloads Incentive Sheets
    // Download Incentive sheet for employees
    PaySlipComponent.prototype.downloadPerformanceSheetSubmit = function (e) {
        var _this = this;
        this.employeeSheet = {};
        this.selectedId = this.employeeRole[e].id;
        this.employeeSheet.id = this.selectedId;
        this.httpService.postRequest(this.employeeSheet, '../EmployeeDepartment/DownloadPerformanceSheet', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.fileName = data;
                if (_this.fileName == "path") {
                    $("#NotExist").modal();
                }
                else {
                    $('#downloadSheet').attr('href', '../ExcelReport/ExportExcel?name=' + _this.fileName);
                    $('#downloadSheet')[0].click();
                }
            }
        });
    };
    PaySlipComponent.prototype.SendPayslipMail = function (e) {
        this.reGePeSelectedId = this.employeeRole[e].id;
        $("#showCustRelationship").modal();
    };
    //User list for BM and Ch
    PaySlipComponent.prototype.GetUserList = function (e) {
        var _this = this;
        this.incentiveDetails = {};
        this.incentivId = this.employeeRole[e].id;
        this.incentiveDetails.id = this.incentivId;
        this.httpService.postRequest(this.incentiveDetails, '../AdminDepartment/GetUserListViewIncentive', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.employeeRole = data.rsBody.retVal.incentiveCofirmation;
                for (var i = 0; i < _this.employeeRole.length; i++) {
                    _this.employeeRole[i].Selected = false;
                }
                _this.generatePayslip = false;
            }
        });
    };
    //Send payslip To User
    PaySlipComponent.prototype.SendMail = function (e) {
        this.httpService.postRequest(this.employeeRole, '../ExcelReport/PaySlipSend', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                $("#sucessPopup").modal();
            }
        });
    };
    // View For Comments 
    PaySlipComponent.prototype.viewIncentiveDetailsSubmit = function (e) {
        var _this = this;
        this.incentiveDetails = {};
        this.incentivId = this.employeeRole[e].id;
        this.incentiveDetails.id = this.incentivId;
        this.httpService.postRequest(this.employeeRole, '../Comman/GetEmployeeIncentiveDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.viewIncentiveDetails = data;
                _this.viewIncentiveDetails.incentiveError = JSON.parse(_this.viewIncentiveDetails.incentiveError);
                $("#viewsIncentiveDetails").bPopup();
            }
        });
    };
    // Downloads Incentive Sheet For Current Selected Quarter
    PaySlipComponent.prototype.incentiveSheetForQuarter = function () {
        var _this = this;
        this.fileName = this.responseData[this.id].incentiveName + ".xlsx";
        this.httpService.postRequest(this.employeeRole, '../ExcelReport/CheckFileExistOrNot', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                if (data.rsBody == "NotExist") {
                    $("#NotExist").bPopup();
                    _this.fileName = "";
                }
                else {
                    $('#downloadSheet').attr('href', '../ExcelReport/ExportExcel?name=' + _this.fileName);
                    $('#downloadSheet')[0].click();
                }
            }
        });
    };
    //For Select All for RM Option
    PaySlipComponent.prototype.checkAll = function () {
        if (this.selectedAll) {
            this.selectedAll = true;
        }
        else {
            this.selectedAll = false;
        }
        /*angular.forEach(this.employeeRole, function (item) {
            if (item.role == "Regional Sales Manager" && item.status == "PAYSLIP GENERATED") {
                item.Selected = this.selectedAll;
            }
            else {
                item.Selected = false;
            }
        });*/
    };
    //For Select All for BM , SE & MTSE Option
    PaySlipComponent.prototype.checkAllBM = function () {
        if (this.selectedAll) {
            this.selectedAll = true;
        }
        else {
            this.selectedAll = false;
        }
        /*angular.forEach(this.employeeRole, function (item) {
            item.Selected = this.selectedAll;
        });*/
    };
    //get employee master excel sheet
    PaySlipComponent.prototype.downloadPayslip = function (e) {
        var _this = this;
        this.reportsType = this.employeeRole[e];
        this.reportsType.quarter = this.currentQuarter;
        this.httpService.postRequest(this.reportsType, '../ExcelReport/DownLoadReport', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.path = data.rsBody.filepath;
                _this.httpService.postRequest(_this.path, '../ExcelReport/CheckFileExistOrNot', true).subscribe(function (data) {
                    if (data.rsBody.message == "NotExist") {
                        $("#NotExist").modal();
                        _this.fileName = "";
                    }
                    else {
                        $('#pdfDownload').attr('href', '../ExcelReport/downloadPDFPaySlip?name=' + _this.path);
                        $('#pdfDownload')[0].click();
                    }
                });
            }
        });
    };
    PaySlipComponent.prototype.ngAfterViewInit = function () {
    };
    PaySlipComponent.prototype.ngOnDestroy = function () {
    };
    PaySlipComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], PaySlipComponent);
    return PaySlipComponent;
}());
exports.PaySlipComponent = PaySlipComponent;
//# sourceMappingURL=payslip.component.js.map