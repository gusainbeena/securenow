"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateIncentive';
var CreateIncentiveComponent = /** @class */ (function () {
    function CreateIncentiveComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        //public keyValueData: Object = {};
        this.keyValueData = {};
        this.gradeData = {};
        this.IncentiveCalculationRequest = {};
        this.yearData = [];
        this.qtrList = [];
        this.ChannelList = [];
        this.channel = '';
        this.subChannelList = '';
        this.roleList = '';
        this.actualTypeList = '';
        this.weightageList = '';
        this.keyType = '';
        this.keyName = '';
        this.subChannel = '';
        this.actualType = '';
        this.incentive = [];
        this.incentivList = {};
        this.flag = false;
        this.incentiveEmpList = "";
    }
    CreateIncentiveComponent.prototype.ngOnInit = function () {
        var _this = this;
        $('.form-steps').css('display', 'block');
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.yearData = datas.rsBody.resulats;
                _this.IncentiveCalculationRequest.year = _this.yearData[0].keyValue;
                _this.IncentiveCalculationRequest.outOfTarn = false;
                _this.getQTR();
            }
        });
    };
    CreateIncentiveComponent.prototype.getQTR = function () {
        var _this = this;
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.qtrList = data.rsBody.resulats;
                _this.IncentiveCalculationRequest.quarter = data.rsBody.currentQuarter;
                _this.IncentiveCalculationRequest.quarter = data.rsBody.currentQuarter.keyValue;
            }
        });
    };
    CreateIncentiveComponent.prototype.getEmployeeForIncentive = function () {
        var _this = this;
        this.httpService.postRequest(this.IncentiveCalculationRequest, '../ManageIncentive/GenerateIncentiveRequest', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                if (data.rsBody.message == 'An active incentive request for this or some other quarter is still pending closure.A new incentive request cannot be raised.') {
                    _this.incentiveEmpList = data.rsBody.message;
                    $("#responseDataPopupnew").modal();
                }
                else {
                    $("#sucessPopupnew").modal();
                }
            }
        });
    };
    // Incentive Selection
    CreateIncentiveComponent.prototype.customIncentive = function () {
        if (this.flag == false) {
            this.flag = true;
            this.getQTR();
            this.IncentiveCalculationRequest.quarter = '';
            this.IncentiveCalculationRequest.incentiveStartMonth = '';
            this.IncentiveCalculationRequest.incentiveEndMonth = '';
            this.IncentiveCalculationRequest.incentiveName = '';
            this.IncentiveCalculationRequest.outOfTarn = true;
        }
        else {
            this.getQTR();
            this.flag = false;
            this.IncentiveCalculationRequest.incentiveName = '';
            this.IncentiveCalculationRequest.outOfTarn = false;
        }
    };
    CreateIncentiveComponent.prototype.autoMonthSelect = function () {
        this.keyValueData.keyType = "QUARTER";
        $("#chkbox").removeClass("ng-hide");
        $('#incentiveName').removeClass("ng-hide");
        this.IncentiveCalculationRequest.incentiveName = 'Incentive_' + this.IncentiveCalculationRequest.quarter + '_' + this.IncentiveCalculationRequest.year;
        /*this.httpService.postRequest<CreateIncentiveComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData').subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.qtrList = data.rsBody;

                }
            });*/
    };
    CreateIncentiveComponent.prototype.ngAfterViewInit = function () {
    };
    CreateIncentiveComponent.prototype.ngOnDestroy = function () {
    };
    CreateIncentiveComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], CreateIncentiveComponent);
    return CreateIncentiveComponent;
}());
exports.CreateIncentiveComponent = CreateIncentiveComponent;
//# sourceMappingURL=createincentive.component.js.map