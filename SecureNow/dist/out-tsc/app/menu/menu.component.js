"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CorrectionData';
var MenuComponent = /** @class */ (function () {
    function MenuComponent(httpService, router, modal, route) {
        this.httpService = httpService;
        this.router = router;
        this.modal = modal;
        this.route = route;
        this.request = {};
        this.monthList = [];
        this.dashboardData = {};
        this.keyValueData = [];
        this.yearList = [];
        this.monthNameArray = [];
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent.prototype.runBatch = function () {
        var _this = this;
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest({}, '../AdminDepartment/RunBatch', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.message = datas.rsBody.message;
                if (_this.message != "Success") {
                    $("#BatchRunError").modal();
                }
                else {
                    $("#BatchRun").modal();
                    _this.masterBatchStatus = true;
                    setTimeout(function () {
                        this.checkBatchStatus();
                    }, 120000);
                }
            }
        });
    };
    MenuComponent.prototype.checkBatchStatus = function () {
        var _this = this;
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest({}, '../AdminDepartment/checkBatchStatus', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.batchStatus = datas.rsBody.message;
                if (_this.batchStatus === 'SUCCESS') {
                    _this.masterBatchStatus = false;
                    $('#successAlert').css('display', 'block');
                    setTimeout(function () {
                        this.batchStatus = '';
                        $('#successAlert').css('display', 'none');
                        this.reloadsSubmit();
                    }, 10000);
                }
                else if (_this.batchStatus === 'FAILED') {
                    _this.masterBatchStatus = false;
                    $('#failedAlert').css('display', 'block');
                    setTimeout(function () {
                        this.batchStatus = '';
                        $('#failedAlert').css('display', 'none');
                    }, 10000);
                }
                else {
                    setTimeout(function () {
                        this.checkBatchStatus();
                    }, 30000);
                }
            }
        });
    };
    MenuComponent.prototype.ngOnExcelPerformanceSubmit = function (type) {
        var _this = this;
        this.downloadFilType = type;
        this.httpService.postRequest({}, '../ExcelReport/GetYearList', true).subscribe(function (data) {
            _this.modal.open('ExceldownloadPopUpPerformance');
            _this.yearList = data.rsBody.key;
        });
    };
    MenuComponent.prototype.ngOnExcelSubmit = function (type) {
        var _this = this;
        this.downloadFilType = type;
        this.httpService.postRequest({}, '../ExcelReport/GetYearList', true).subscribe(function (data) {
            _this.modal.open('ExceldownloadPopUp');
            _this.yearList = data.rsBody.key;
        });
    };
    MenuComponent.prototype.ngOnGetMonthList = function (obj) {
        this.monthNameArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.monthList = [];
        $('#text').html = '-Select Month-';
        if (this.monthList.indexOf(obj) != -1) {
            var i = this.monthList.indexOf(obj);
            this.monthList.splice(i, 1);
        }
        else {
            this.monthList.push(obj);
        }
        var val = [];
        for (var _i = 0, _a = this.monthList; _i < _a.length; _i++) {
            var i_1 = _a[_i];
            val.push(this.monthNameArray[i_1 - 1]);
        }
        $('#text').html(val.toString());
    };
    MenuComponent.prototype.ngOnPerformanceData = function () {
        var _this = this;
        this.request.month = this.monthList;
        this.request.year = this.selectedYear;
        if (this.monthList.length == 0) {
            alert("Please select atleast one month");
        }
        else {
            this.httpService.postRequest(this.request, '../ExcelReport/getPerformanceSheet', true).subscribe(function (data) {
                _this.fileName = data;
                var path = _this.fileName;
                $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + path);
                $('#ExportExcelData')[0].click();
                _this.monthList = [];
                $('#text').html('-Select Month-');
                $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
            });
        }
    };
    MenuComponent.prototype.ngOnDownloadSubmitData = function () {
        var _this = this;
        var path = this.downloadFilType + "_" + this.selectedYear + ".xlsx";
        $('#alertbottomleft').removeClass("ng-hide");
        var inputData = path;
        $('#alertbottomleft').removeClass('ng-hide');
        this.httpService.postRequest(inputData, '../ExcelReport/CheckFileExistOrNot', true).subscribe(function (data) {
            if (data.rsBody == "NotExist") {
                _this.modal.open('NotExist');
                _this.fileName = "";
            }
            else {
                var path = _this.downloadFilType + "_" + _this.selectedYear + ".xlsx";
                $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + path);
                $('#ExportExcelData')[0].click();
            }
        });
    };
    MenuComponent.prototype.modalclose = function (id) {
        this.modal.close(id);
    };
    MenuComponent.prototype.ngAfterViewInit = function () {
    };
    MenuComponent.prototype.ngOnDestroy = function () {
    };
    MenuComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            modelservice_1.ModalService,
            router_1.ActivatedRoute])
    ], MenuComponent);
    return MenuComponent;
}());
exports.MenuComponent = MenuComponent;
//# sourceMappingURL=menu.component.js.map