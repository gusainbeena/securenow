"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/http");
var platform_browser_1 = require("@angular/platform-browser");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var menu_routing_module_1 = require("./menu-routing.module");
var menu_component_1 = require("./menu.component");
var MenuAppModule = /** @class */ (function () {
    function MenuAppModule() {
    }
    MenuAppModule = __decorate([
        core_1.NgModule({
            declarations: [
                menu_component_1.MenuComponent,
            ],
            imports: [
                common_1.CommonModule,
                menu_routing_module_1.MenuRoutingModule,
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                http_2.HttpModule
            ],
            providers: [http_service_1.HttpService, modelservice_1.ModalService],
            bootstrap: [menu_component_1.MenuComponent],
            exports: []
        })
    ], MenuAppModule);
    return MenuAppModule;
}());
exports.MenuAppModule = MenuAppModule;
//# sourceMappingURL=menu.module.js.map