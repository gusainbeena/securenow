"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureEmployeeTemplate';
var ConfigureEmployeeTemplateComponent = /** @class */ (function () {
    function ConfigureEmployeeTemplateComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        this.isSpinner = true;
        this.keyValueData = {};
        this.employeeComponent = [];
        this.column = [];
        this.temp = [];
        this.metaData = {};
        this.months = [];
        this.columnData = {};
        this.currentYear = [];
        this.excelColumn = [];
        this.columnsId = [];
        this.finalExcel = [];
        this.columnList = [];
        this.monthList = [];
        this.monthsList = [];
        this.previousExcelColumn = [];
        this.previousColumnsId = [];
        this.editHeader = [{}];
        this.employeeAttributes = [];
        this._secureFileService = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            enableCheckAll: false
        };
    }
    ConfigureEmployeeTemplateComponent.prototype.ngOnInit = function () {
        var _this = this;
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        $('#removeButton').css('display', 'block');
        $('#col').css('display', 'block');
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/getEmployeeTemplateAttributes', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.months = datas.rsBody.months;
                _this.monthsList = datas.rsBody.monthsList;
                _this.employeeList = datas.rsBody.employeeList;
                _this.employeeList2 = datas.rsBody.employeeList;
                _this.employeeComponent = datas.rsBody.employeeComponent;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
                _this.currentYear = datas.rsBody.currentYear;
                _this.onSelectAllEmployee(datas.rsBody.employeeComponent);
                _this.editHeader = datas.rsBody.excelColumn;
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.onItemSelectMonth = function (item, i) {
        this.isSpinner = false;
        this.monthList.push(item);
    };
    ConfigureEmployeeTemplateComponent.prototype.onSelectAllMonth = function (items) {
        this.isSpinner = false;
    };
    ConfigureEmployeeTemplateComponent.prototype.onDeSelectAllMonth = function (items) {
        this.isSpinner = false;
    };
    ConfigureEmployeeTemplateComponent.prototype.onItemDeSelectMonth = function (item, i) {
        this.isSpinner = false;
        var i = this.monthList.indexOf(item); // RETURNS 1.
        this.monthList.splice(i, 1);
    };
    //------------------------------------
    ConfigureEmployeeTemplateComponent.prototype.onItemSelectEmployee = function (item) {
        this.isSpinner = false;
        this.employeeList.push(item);
    };
    ConfigureEmployeeTemplateComponent.prototype.onSelectAllEmployee = function (items) {
        this.employeeList = items;
        this.isSpinner = false;
    };
    ConfigureEmployeeTemplateComponent.prototype.onDeSelectAllEmployee = function (items) {
        this.isSpinner = false;
    };
    ConfigureEmployeeTemplateComponent.prototype.onItemDeSelectEmployee = function (item) {
        this.isSpinner = false;
        var i = this.employeeList.indexOf(item); // RETURNS 1.
        this.employeeList.splice(i, 1);
    };
    ConfigureEmployeeTemplateComponent.prototype.removeColumn = function (i) {
        var _this = this;
        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest(this.columnData, '../ManageTemplate/removeColumn', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.columnData.excelColumn = datas.rsBody.excelColumn;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnData.finalExcel = datas.rsBody.excelColumn;
                _this.columnsId = datas.rsBody.columnsId;
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.OnAddemployeeComponent = function () {
        $('#empCompBtn2').css('display', 'block');
        $('#empCompText').css('display', 'block');
        $('#empCompText2').css('display', 'block');
    };
    ConfigureEmployeeTemplateComponent.prototype.OnColumnSelect = function (item) {
        this.addColumn = item;
    };
    ConfigureEmployeeTemplateComponent.prototype.OnStartMonthSelect = function (i) {
        var start = i;
        this.startMonth = i;
    };
    ConfigureEmployeeTemplateComponent.prototype.OnYearSelect = function (i) {
        var start = i;
        this.year = i;
    };
    ConfigureEmployeeTemplateComponent.prototype.OnEndMonthSelect = function (i) {
        var end = i;
        this.endMonth = i;
    };
    ConfigureEmployeeTemplateComponent.prototype.addNewColumn = function (newColumn) {
        var _this = this;
        this.newColumn = newColumn;
        this.columnData.addColumn = this.addColumn;
        this.columnData.newColumn = this.newColumn;
        this.columnData.excelColumn = this.columnData.finalExcel;
        this.httpService.postRequest(this.columnData, '../AdminDepartment/addNewColumn', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.columnData.excelColumn = datas.rsBody.excelColumn;
                _this.excelColumn = datas.rsBody.excelColumn;
                _this.columnData.finalExcel = datas.rsBody.excelColumn;
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.viewEmpComponent = function () {
        $("#employeeComponentPopup").modal();
    };
    ConfigureEmployeeTemplateComponent.prototype.viewCustComponent = function () {
        $("#customerComponentPopup").modal();
    };
    ConfigureEmployeeTemplateComponent.prototype.createTemplate = function () {
        var _this = this;
        this.columnData.performance = this.temp;
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.columnData, '../ManageTemplate/createEmployeeTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.message == "error") {
                    _this.message = datas.rsBody.message;
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Empty") {
                    _this.message = "Please Fill Values";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.finalExcel == datas.rsBody.finalExcel;
                    _this.relatedKeyValue = datas.rsBody.relatedKeyValue;
                    $("#sampleTemplate").css('display', 'block');
                    $("#downTempBtn").css('display', 'block');
                    $("#addNewColumnListBtn").addClass("ng-hide");
                }
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.generateTemplate = function () {
        var _this = this;
        this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
        this.columnData.startMonth = this.startMonth;
        this.columnData.endMonth = this.endMonth;
        this.columnData.year = this.year;
        this.columnData.employeeList = this.employeeList;
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest(this.columnData, '../ManageTemplate/generateEmployeeTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.message == "Month Error") {
                    _this.message = "End month Should be greater than start month. Please Select Carefully";
                    _this.title = "Select Month error";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Please Select Start Month and End Month") {
                    _this.title = "Select Month error";
                    _this.message = "Please Select Start Month and End Month";
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "error") {
                    _this.message = datas.rsBody.message;
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.title = "Duplicate Entry";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                if (datas.rsBody.message == "Empty") {
                    _this.title = "Value Error";
                    _this.message = "Please Fill Values";
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.excelColumn = datas.rsBody.finalExcel;
                    _this.columnData.finalExcel = datas.rsBody.finalExcel;
                    _this.columnData.columnsId = datas.rsBody.columnsId;
                    _this.finalExcel == datas.rsBody.finalExcel;
                    _this.columnsId = datas.rsBody.columnsId;
                    _this.editHeader = datas.rsBody.excelColumn;
                    $("#sampleTemplate").css('display', 'block');
                    $("#downTempBtn").css('display', 'block');
                    $("#addNewColumnListBtn").addClass("ng-hide");
                }
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.DownloadTemplate = function () {
        var _this = this;
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12;
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }
        this.httpService.postRequest(this.columnData, '../ManageTemplate/downloadEmployeeTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                if (datas.rsBody.msg == "duplicate") {
                    _this.message = datas.rsBody.message;
                    $("#sampleTemplate").css('display', 'none');
                    $("#downTempBtn").css('display', 'none');
                    $("#errorPopup").modal();
                }
                else {
                    _this.columnData.filepath = datas.rsBody.filepath;
                    _this.filepath = datas.rsBody.filepath;
                    _this.fileName = datas.rsBody.fileName;
                    $("#successPopup").modal();
                }
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.ExportDataSetToExcel = function () {
        var _this = this;
        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        $('#alertbottomleft').addClass('ng-hide');
        $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName + '&path=' + this.filepath);
        $('#ExportExcelData')[0].click();
        $('#text').html('-Select Month-');
        $("#collapse1").collapse('hide');
        $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
        this.keyValueData.keyType = "EMPLOYEE_MASTER";
        this.keyValueData.keyName = "Template";
        if (this.startMonth != null || this.startMonth == "") {
            this.keyValueData.startMonth = this.startMonth;
        }
        else {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "") {
            this.keyValueData.endMonth = this.endMonth;
        }
        else {
            this.keyValueData.endMonth = 12;
        }
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        $('#ExportExcelDataBtn').css('display', 'none');
        this.httpService.postRequest(this.keyValueData, '../ManageTemplate/insertEmployeeTemplate', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.excelColumn = datas.rsBody.excelColumn;
                $('#ExportExcelData').css('display', 'none');
                $('#downTempBtn').css('display', 'none');
                $('#afterDownload').css('display', 'inline-table');
                $('#beforeDownload').css('display', 'none');
                $('#addNewColumnBtn').css('display', 'none');
                $('#ExportExcelData').attr('disabled', true);
                $('#addNewColumnBtn').attr('disabled', true);
                //$('#downTempBtn').attr('disabled', true);
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.addemployeeComponent = function (newEmployee) {
        var _this = this;
        this.newEmployee = newEmployee;
        this.httpService.postRequest(this.newEmployee, '../ManageTemplate/addemployeeComponent', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.employeeComponent = datas.rsBody.employeeComponent;
                _this.employeeList2 = datas.rsBody.employeeList;
                _this.onSelectAllEmployee(datas.rsBody.employeeComponent);
            }
        });
    };
    ConfigureEmployeeTemplateComponent.prototype.ngAfterViewInit = function () {
    };
    ConfigureEmployeeTemplateComponent.prototype.ngOnDestroy = function () {
    };
    ConfigureEmployeeTemplateComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ConfigureEmployeeTemplateComponent);
    return ConfigureEmployeeTemplateComponent;
}());
exports.ConfigureEmployeeTemplateComponent = ConfigureEmployeeTemplateComponent;
//# sourceMappingURL=configureemployeetemplate.component.js.map