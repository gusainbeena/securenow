"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var d3 = require("d3");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'PieChart';
var PieChartComponent = /** @class */ (function () {
    function PieChartComponent() {
        this._graphDesiredData = [];
        this._graphData = {};
    }
    Object.defineProperty(PieChartComponent.prototype, "containerElement", {
        set: function (containerElement) {
            //console.log(graphData);
            this._containerElement = containerElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PieChartComponent.prototype, "graphData", {
        set: function (graphData) {
            //console.log(graphData);
            this._graphData = graphData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PieChartComponent.prototype, "graphXAxis", {
        set: function (graphXAxis) {
            //console.log(graphXAxis);
            this._graphXAxis = graphXAxis;
        },
        enumerable: true,
        configurable: true
    });
    PieChartComponent.prototype.DeStructureData = function () {
        this._graphDesiredData = [];
        if (this._graphXAxis.length > 0) {
            for (var j = 0; j < this._graphXAxis.length; j++) {
                var temp = {};
                temp['label'] = this._graphXAxis[j];
                temp['value'] = ((this._graphData[this._graphXAxis[j]]['actual'] / this._graphData[this._graphXAxis[j]]['target']) * 100) || 0;
                this._graphDesiredData.push(temp);
            }
        }
        else {
        }
        this.drawGraph();
    };
    PieChartComponent.prototype.drawGraph = function () {
        var graphData = Object.assign([], this._graphDesiredData);
        var _containerElement = this._containerElement;
        var height = 400;
        nv.addGraph(function () {
            var chart = nv.models.pieChart()
                .x(function (d) { return d.label; })
                .y(function (d) { return d.value; })
                .height(height)
                .showLabels(true);
            d3.select('#' + _containerElement + ' #PaiChart svg')
                .datum(graphData)
                .transition().duration(10000)
                .call(chart);
            //this.reDesignGraph("PaiChart");
            return chart;
        });
    };
    PieChartComponent.prototype.ngOnInit = function () {
        this.DeStructureData();
        //console.log(this._graphData);
    };
    PieChartComponent.prototype.ngOnDestroy = function () {
        console.log('destroying component..');
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], PieChartComponent.prototype, "containerElement", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], PieChartComponent.prototype, "graphData", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], PieChartComponent.prototype, "graphXAxis", null);
    PieChartComponent = __decorate([
        core_1.Component({
            selector: 'PieChart',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [])
    ], PieChartComponent);
    return PieChartComponent;
}());
exports.PieChartComponent = PieChartComponent;
//# sourceMappingURL=piechart.component.js.map