"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/http");
var http_service_1 = require("../../service/http.service");
var d3methodbinding_service_1 = require("../../service/d3methodbinding.service");
var globaldashboard_routing_module_1 = require("./globaldashboard-routing.module");
var globaldashboard_component_1 = require("./globaldashboard.component");
var modal_service_1 = require("../../_directives/modal.service");
var GlobalDashboardModule = /** @class */ (function () {
    function GlobalDashboardModule() {
    }
    GlobalDashboardModule = __decorate([
        core_1.NgModule({
            declarations: [
                globaldashboard_component_1.GlobalDashboardComponent
            ],
            imports: [
                common_1.CommonModule,
                globaldashboard_routing_module_1.GlobalDashboardRoutingModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                http_2.HttpModule
            ],
            providers: [http_service_1.HttpService, d3methodbinding_service_1.D3BindingsService, modal_service_1.ModalComponent],
            bootstrap: [globaldashboard_component_1.GlobalDashboardComponent],
            exports: []
        })
    ], GlobalDashboardModule);
    return GlobalDashboardModule;
}());
exports.GlobalDashboardModule = GlobalDashboardModule;
//# sourceMappingURL=globaldashboard.module.js.map