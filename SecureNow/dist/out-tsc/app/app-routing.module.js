"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var routes = [
    {
        path: 'access-control',
        loadChildren: './accesscontrol/accesscontrol.module#AccessControlModule'
    },
    {
        path: 'create-employee',
        loadChildren: './createemployee/createemployee.module#CreateEmployeeModule'
    },
    {
        path: 'view-employee',
        loadChildren: './viewemployee/viewemployee.module#ViewEmployeeModule'
    },
    {
        path: 'create-customer',
        loadChildren: './createcustomer/createcustomer.module#CreateCustomerModule'
    },
    {
        path: 'view-customer',
        loadChildren: './viewcustomer/viewcustomer.module#ViewCustomerModule'
    },
    {
        path: 'target-upload',
        loadChildren: './opupload/opupload.module#OpUploadModule'
    },
    {
        path: 'actual-upload',
        loadChildren: './actualupload/actualupload.module#ActualUploadModule'
    },
    {
        path: 'approve-actual-upload',
        loadChildren: './approveactualupload/approveactualupload.module#ApproveActualUploadModule'
    },
    {
        path: 'create-incentive',
        loadChildren: './createincentive/createincentive.module#CreateIncentiveModule'
    },
    {
        path: 'view-incentive',
        loadChildren: './viewincentive/viewincentive.module#ViewIncentiveModule'
    },
    {
        path: 'pay-slip',
        loadChildren: './payslip/payslip.module#PaySlipModule'
    },
    {
        path: 'overdue-upload',
        loadChildren: './overdueupload/overdueupload.module#OverdueUploadModule'
    },
    {
        path: 'mass-upload',
        loadChildren: './massupload/massupload.module#MassUploadModule'
    },
    {
        path: 'configure-actual-template',
        loadChildren: './configureactualtemplate/configureactualtemplate.module#ConfigureActualTemplateModule'
    },
    {
        path: 'configure-op-template',
        loadChildren: './configureoptemplate/configureoptemplate.module#ConfigureOpTemplateModule'
    },
    {
        path: 'configure-metadata',
        loadChildren: './configuremetadata/configuremetadata.module#ConfigureMetaDataModule'
    },
    {
        path: 'configure-meta-data',
        loadChildren: './configuremetadata/configuremetadata.module#ConfigureMetaDataModule'
    },
    {
        path: 'global-dashboard',
        loadChildren: './globaldashboard/globaldashboard.module#GlobalDashboardModule'
    },
    {
        path: 'my-dashboard',
        loadChildren: './mydashboard/mydashboard.module#MyDashboardModule'
    },
    {
        path: 'batch-process',
        loadChildren: './batchprocess/batchprocess.module#BatchProcessModule'
    },
    {
        path: '',
        loadChildren: './globaldashboard/globaldashboard.module#GlobalDashboardModule'
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: "reload" })],
            exports: [
                router_1.RouterModule,
            ],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map