"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var ng2_file_upload_1 = require("ng2-file-upload");
var modelservice_1 = require("../../service/modelservice");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'OverdueUpload';
var OverdueUploadComponent = /** @class */ (function () {
    function OverdueUploadComponent(httpService, router, modal, route) {
        this.httpService = httpService;
        this.router = router;
        this.modal = modal;
        this.route = route;
        this.dashboardData = {};
        this.keyValueData = {};
        this.requestData = {};
        this.opData = {};
        this.overDueData = {};
        this.incentiveData = {};
        this.ChannelList = [];
        this.year = '';
        this.dataSourceFileName = '';
        this.subChannelList = '';
        this.roleList = '';
        this.actualTypeList = '';
        this.weightageList = '';
        this.uploader = new ng2_file_upload_1.FileUploader({ url: '../ManageUploadData/UploadMasterSheet' });
        this.incentive = [];
        this.incentivList = {};
        this.listData = [];
        this.yearData = [];
        this.monthData = [];
        this.responseData = {};
        this.downloadFile = {};
        this.error = {};
        this.File = {};
        this.lines2 = [];
        this.errorList = [];
        this.warningList = [];
        this.lines = [];
    }
    OverdueUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getNextEmployeeList = 0;
        this.getPage = 0;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.yearData = data.rsBody.resulats;
        });
        //getMonth List			
        this.getMonthList();
    };
    OverdueUploadComponent.prototype.getMonthList = function () {
        var _this = this;
        this.keyValueData = {};
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.monthData = data.rsBody.resulats;
        });
        this.keyValueData.keyType = "TARGETKEY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            _this.opData = data.rsBody.resulats;
        });
    };
    OverdueUploadComponent.prototype.ngOnOverDueDataSubmit = function () {
        var _this = this;
        this.overDueData.requestType = "EmployeeMasterDataPort";
        this.httpService.postRequest(this.overDueData, '../ManageUploadData/UploadMasterData', true).subscribe(function (data) {
            if (data == undefined) {
                var id = data.rsBody.data;
            }
            else {
                var id = data.rsBody.data;
            }
            _this.responseData.id = id;
            _this.responseData.id = data.rsBody.data;
            _this.responseData.fileName = data.rsBody.fileName;
            _this.responseData.filePath = data.rsBody.filePath;
            _this.responseData.uploadType = data.rsBody.uploadType;
            _this.ngOnCheckStatus(_this.responseData);
            if (_this.responseData.fileName != null) {
                _this.errorHandler(_this.responseData);
            }
        });
    };
    OverdueUploadComponent.prototype.ngOnCheckStatus = function (inputData) {
        var _this = this;
        this.httpService.postRequest(inputData, '../ManageUploadData/GetUploadStatus', true).subscribe(function (data) {
            _this.responseData = data.rsBody;
            if (_this.responseData != "" && _this.responseData != null) {
                _this.overDueData = {};
                _this.router.navigate(['overdue-upload']);
                _this.modal.open('sucessPopup');
                _this.ngOnInit();
            }
            else {
                _this.ngOnCheckStatus(inputData);
            }
        });
    };
    OverdueUploadComponent.prototype.ngOnDeleteRequest = function (target) {
        var _this = this;
        this.overDueData = this.listData[target];
        this.overDueData.requestType = 'OverDueDataPort';
        this.httpService.postRequest(this.overDueData, '../ManageUploadData/CancelUploadRequest', true).subscribe(function (data) {
            _this.error = data.rsBody;
            _this.modal.open('error');
            _this.ngOnInit();
        });
    };
    OverdueUploadComponent.prototype.ngOnGetNext = function () {
        var _this = this;
        this.getPage = this.getPage + 1;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    OverdueUploadComponent.prototype.ngOnGetPrevious = function () {
        var _this = this;
        this.getPage = this.getPage - 1;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(function (data) {
            _this.listData = data.rsBody.data;
            if (_this.listData == null) {
                $("#row_not_found_id").css('display', 'block');
                $("#row_table_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'none');
                $("#row_table_id").css('display', 'block');
            }
            // next and previous 
            if (_this.listData.length < 5) {
                $("#example1_previous").addClass("disabled");
                $("#example1_next").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
                $("#example1_next").addClass("ng-hide");
            }
            else {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_next").removeClass("ng-hide");
                $("#example1_next").removeClass("disabled");
            }
            if (_this.getPage > 0) {
                $("#example1_previous").removeClass("ng-hide");
                $("#example1_previous").removeClass("disabled");
            }
            if (_this.getPage == 0) {
                $("#example1_previous").addClass("disabled");
                $("#example1_previous").addClass("ng-hide");
            }
        });
    };
    OverdueUploadComponent.prototype.modalclose = function (id) {
        this.modal.close(id);
    };
    OverdueUploadComponent.prototype.ngAfterViewInit = function () {
    };
    OverdueUploadComponent.prototype.fileSelect = function () {
        $('#imageUpload').click();
    };
    OverdueUploadComponent.prototype.imageUpload = function () {
        var _this = this;
        this.uploader.uploadAll();
        //Get response
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            var responsePath = JSON.parse(response);
            {
                _this.overDueData.dataSourceFileName = responsePath.rsBody;
            }
            ;
        };
    };
    OverdueUploadComponent.prototype.ngOnTemplateDownload = function () {
        var triggerClickk = setTimeout(function () {
            clearTimeout(triggerClickk);
            $('#downloadReport')[0].click();
        });
    };
    OverdueUploadComponent.prototype.ngOnDownloadSourceReport = function (data) {
        this.downloadFile = data;
        $('#DownloadSourcePath').attr('href', '../Comman/DownloadOpSourceReport?name=' + this.downloadFile);
        $('#DownloadSourcePath')[0].click();
    };
    OverdueUploadComponent.prototype.ngOnDownloadErrorReport = function (getData) {
        var _this = this;
        this.downloadFile = getData;
        this.httpService.postRequest(this.downloadFile, '../Comman/CheckFileExistOrNot', true).subscribe(function (data) {
            _this.result = data.rsBody;
            if (_this.result == "NotExist") {
                _this.modal.open('notFound');
                _this.filename = "";
            }
            else {
                $('#DownloadErrorPath').attr('href', '../Comman/DownloadOpErrorReport?name=' + _this.downloadFile);
                $('#DownloadErrorPath')[0].click();
            }
        });
    };
    OverdueUploadComponent.prototype.ngOnDestroy = function () {
    };
    OverdueUploadComponent.prototype.errorHandler = function (parameter) {
        var _this = this;
        this.File.fileName = parameter.fileName;
        this.File.filePath = parameter.filePath;
        this.File.id = parameter.id;
        this.File.uploadType = parameter.uploadType;
        this.File.quarter = parameter.quarter;
        this.httpService.postRequest(this.File, '../ManageUploadData/Errorhandler', true).subscribe(function (data) {
            if (data.rsBody.result == 'success') {
                $('#consolePanel').removeClass('d-none');
                //this.lines = null;
                if (data.rsBody.error == null) {
                    _this.lines2 = data.rsBody.lines;
                    _this.errorList = data.rsBody.errorList;
                    if (_this.errorList.length == 0) {
                        _this.success = data.rsBody.success;
                        $('#successBlock').removeClass('d-none');
                    }
                    for (var i = 0; i <= _this.lines2.length; i++) {
                        _this.lines.push(_this.lines2[i]);
                        //this.sub = Observable.interval(10000)
                        //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                    }
                    _this.warningList = data.rsBody.warningList;
                }
                else {
                    //this.lines = null;
                    _this.lines2 = data.rsBody.lines;
                    for (var i = 0; i <= _this.lines2.length; i++) {
                        _this.lines.push(_this.lines2[i]);
                        //this.sub = Observable.interval(10000)
                        //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                    }
                    _this.errorList = data.rsBody.errorList;
                    if (_this.errorList.length > 200) {
                        _this.errorList = [];
                        _this.errorList.push("Number of error are too high to handle please first correct your data sheet than proceed");
                    }
                    _this.warningList = data.rsBody.warningList;
                    _this.reason = data.rsBody.error;
                }
            }
        });
    };
    OverdueUploadComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            modelservice_1.ModalService,
            router_1.ActivatedRoute])
    ], OverdueUploadComponent);
    return OverdueUploadComponent;
}());
exports.OverdueUploadComponent = OverdueUploadComponent;
//# sourceMappingURL=overdueupload.component.js.map