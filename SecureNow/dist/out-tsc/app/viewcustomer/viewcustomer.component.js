"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewCustomer';
var ViewCustomerComponent = /** @class */ (function () {
    function ViewCustomerComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        //public keyValueData: Object = {};
        this.keyValueData = {};
        this.shipToRequire = {};
        this.customerDeatils = {};
        this.custDetials = {};
        this.customerTargets = [];
        this.customerRelationship = {};
        this.responseData = [];
        this.getPreviousCustomerList = {};
        this.responseStateData = [];
        this.responseRelationData = {};
        this.yearData = [];
        this.yearRecorded = [];
        this.dataTypeList = [];
        this.BUData = [];
        this.responseChannelData = [];
        this.monthData = [];
        this.regionData = {};
        this.searchByBranch = {};
        this.customerDetails = {};
        this.customerBaseDetails = [];
        this.soldToPartyCodes = [];
        this.targetList = [];
        this.targetData = [];
        this.secondaryTargetList = {};
        this.sucessResponseData = {};
        this.getList = {};
        this.subChannelList = [];
        this.regionList = {};
        this.branchListData = {};
        this.subAreaData = [];
        this.responseAreaData = [];
        this.employeeKAMData = {};
        this.employeeData = [];
        this.getEmployeeList = {};
        this.shipToPartyCodes = {};
        this.lengthdata = {};
        this.customerBaseDetailsFK = {};
        this.deleteRel = {};
        this.sucessData = {};
        this.inputData = {};
        this.gradeData = {};
        this.doaStr = {};
        this.success = '';
        this.date = '';
    }
    //Get channel List//
    ViewCustomerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.custDetials = {
            customerDeatils: {},
            customerRelationship: {},
            customerTargets: []
        };
        this.doaStr = {
            year: null,
            month: null,
            day: null
        };
        $("#submit-basic-button").css('display', 'none');
        $(".h3-edit").css('display', 'block');
        $(".h3-create").css('display', 'none');
        // $(".docStrId").css('display', 'block');
        $(".viewCustTar").css('display', 'block');
        $(".addNewCustTar").css('display', 'block');
        $("#update-basic-button").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        this.getNextCustomerList = 0;
        this.httpService.postRequest({}, '../ManageCustomer/GetCustomerDetailsList', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.responseData = datas.rsBody.resulats;
                if (datas.rsBody.sucess == "No Data Found") {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display');
                }
                // next and previous 
                if (_this.responseData.length < 20) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getNextCustomerList > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getNextCustomerList == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                }
                _this.loadState();
            }
        });
    };
    //Get State List
    ViewCustomerComponent.prototype.loadState = function () {
        var _this = this;
        this.keyValueData.keyType = "STATE";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.responseStateData = data.rsBody.resulats;
                _this.getYearList();
            }
        });
    };
    //Get Year List
    ViewCustomerComponent.prototype.getYearList = function () {
        var _this = this;
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.yearData = data.rsBody.resulats;
                _this.yearRecorded = _this.yearData[0].keyValue;
                _this.getBUList();
            }
        });
    };
    //Get BU List
    ViewCustomerComponent.prototype.getBUList = function () {
        var _this = this;
        this.keyValueData.keyType = "MetaData"; //"OPERATIONAL";
        this.keyValueData.keyName = "businessVertical";
        this.httpService.postRequest(this.keyValueData, '../ManageCustomer/GetBUBGListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.BUData = data.rsBody.resulats;
                _this.getChannel();
                _this.getDataTypeList();
            }
        });
    };
    //Get Data Type List
    ViewCustomerComponent.prototype.getDataTypeList = function () {
        var _this = this;
        this.keyValueData.keyType = "MetaData"; // "OPERATIONAL";
        this.keyValueData.keyName = "businessVertical"; //"BU";
        this.httpService.postRequest(this.keyValueData, '../ManageCustomer/GetDataTypeList', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.dataTypeList = data.rsBody.dataTypeList;
                _this.getMonthList();
            }
        });
    };
    // get Channel List
    ViewCustomerComponent.prototype.getChannel = function () {
        var _this = this;
        this.keyValueData.keyType = "Channel"; //"CHANNEL";
        this.keyValueData.keyName = "SalesHorizontal";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.responseChannelData = data.rsBody.resulats;
                _this.getMonthList();
            }
        });
    };
    //Get Month List
    ViewCustomerComponent.prototype.getMonthList = function () {
        var _this = this;
        this.httpService.postRequest({}, '../Comman/GetMonthKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.monthData = data.rsBody.resulats;
                _this.getRegionList();
            }
        });
    };
    //Get REgion List
    ViewCustomerComponent.prototype.getRegionList = function () {
        var _this = this;
        this.keyValueData.keyType = "Region";
        this.keyValueData.keyName = "salesHorizontal";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.regionData = data.rsBody.resulats;
            }
        });
    };
    // Search By Name And Adp ID
    ViewCustomerComponent.prototype.searchBranch = function () {
        var _this = this;
        this.keyValueData.keyType = "MetaData"; //"OPERATIONAL";
        this.keyValueData.keyName = "businessvertical"; //"BU";
        this.httpService.postRequest(this.searchByBranch, '../ManageCustomer/GetCustomerDetailsList', true).subscribe(function (data) {
            if (data.rsBody.result == 'success') {
                _this.responseData = data.rsBody.resulats;
                if (data.rsBody.sucess == "No Data Found") {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display');
                }
                // next and previous 
                if (_this.responseData.length < 20) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getNextCustomerList > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getNextCustomerList == 0) {
                    $("#example1_previous").addClass("disabled");
                }
                _this.loadState();
            }
        });
    };
    //Next And Privious Data
    // next button 
    ViewCustomerComponent.prototype.getNextCustomer = function () {
        var _this = this;
        this.getNextCustomerList = this.getNextCustomerList + 1;
        var inputData = {
            pageNumber: this.getNextCustomerList
        };
        this.httpService.postRequest(inputData, '../ManageCustomer/NextAndPriviousCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result == 'success') {
                _this.responseData = data.rsBody.resulats;
                if (data.rsBody.sucess == "No Data Found") {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display');
                }
                // next and previous 
                if (_this.responseData.length < 20) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getNextCustomerList > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getNextCustomerList == 0) {
                    $("#example1_previous").addClass("disabled");
                }
                _this.loadState();
            }
        });
    };
    // previous button 
    ViewCustomerComponent.prototype.getPreviousCustomer = function () {
        var _this = this;
        this.getNextCustomerList = this.getNextCustomerList - 1;
        var inputData = {
            pageNumber: this.getNextCustomerList
        };
        this.httpService.postRequest(inputData, '../ManageCustomer/NextAndPriviousCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result == 'success') {
                _this.responseData = data.rsBody.resulats;
                if (data.rsBody.sucess == "No Data Found") {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (_this.responseData.length < 20) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (_this.getNextCustomerList > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (_this.getNextCustomerList == 0) {
                    $("#example1_previous").addClass("disabled");
                }
                _this.loadState();
            }
        });
    };
    // edit Customer Details
    ViewCustomerComponent.prototype.editCustomerDetailsSubmit = function (e) {
        this.id = this.responseData[e].id;
        this.customerId = this.responseData[e].id;
        this.getCustomerDetailsSubmit();
    };
    ViewCustomerComponent.prototype.convert = function (str) {
        var date = new Date(str), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    };
    ViewCustomerComponent.prototype.getCustomerDetailsSubmit = function () {
        var _this = this;
        this.customerDetails.id = this.customerId;
        this.httpService.postRequest(this.customerDetails, '../ManageCustomer/EditCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                var s = data.rsBody.retVal.customerDeatils.doaStr;
                //creating object date
                var x = _this.convert(data.rsBody.retVal.customerDeatils.doaStr);
                /*var fields = s.split(/-/);
                var day = fields[0];
                var month = fields[1];
                var year = fields[2];
                this.doaStr.year = parseInt(year);
                this.doaStr.month = parseInt(month);
                this.doaStr.day = parseInt(day);
                this.customerBaseDetails.doaStr = this.doaStr;*/
                if (data.rsBody.retVal.customerDeatils.status == "CLOSED") {
                    $(".docStrId").css('display', 'block');
                }
                //this.doaStr = data.rsBody.retVal.customerDeatils.doaStr.split(;
                _this.customerBaseDetails = data.rsBody.retVal.customerDeatils;
                _this.custDetials.responseRelationData = data.rsBody.retVal.customerRelationship;
                _this.targetList = data.rsBody.retVal.customerTargets;
                _this.secondaryTargetList = data.rsBody.retVal.secondaryCustomerTargets;
                var dateString = data.rsBody.retVal.customerDeatils.doaStr; // "13/10/2014";
                //var dataSplit = null;
                //if (dateString != null) {
                //     dataSplit = dateString.split('-');
                //}
                // else {
                //var dataSplit = dateString.split('-');
                //}
                var dateConverted = {
                    day: 1,
                    month: 1,
                    year: 1001
                };
                var dateConverted2 = {
                    day: 1,
                    month: 1,
                    year: 1001
                };
                //dateConverted.day = parseInt(dataSplit[0]);
                //dateConverted.month = parseInt(dataSplit[1]);
                //dateConverted.year = parseInt(dataSplit[2]);
                //this.customerBaseDetails.doaStr = dateConverted;
                if (data.rsBody.retVal.customerDeatils.doaStr != null) {
                    var dateString = data.rsBody.retVal.customerDeatils.doaStr;
                    var dataSplit = dateString.split('-');
                    dateConverted.day = parseInt(dataSplit[0]);
                    dateConverted.month = parseInt(dataSplit[1]);
                    dateConverted.year = parseInt(dataSplit[2]);
                    _this.customerBaseDetails.doaStr = dateConverted;
                }
                if (data.rsBody.retVal.customerDeatils.docStr != null) {
                    var dateString2 = data.rsBody.retVal.customerDeatils.docStr;
                    var dataSplit2 = dateString2.split('-');
                    dateConverted2.day = parseInt(dataSplit2[0]);
                    dateConverted2.month = parseInt(dataSplit2[1]);
                    dateConverted2.year = parseInt(dataSplit2[2]);
                    _this.customerBaseDetails.docStr = dateConverted2;
                }
                if (_this.customerBaseDetails.status == "CLOSED") {
                    $(".remove-disable").prop('disabled', true);
                    $(".remove-disable-status").prop('disabled', true);
                    $("#add-target-button").css('display', 'none');
                    $("#add-role-button").css('display', 'none');
                    $("#update-basic-button").css('display', 'block');
                    _this.customerBaseDetails.status = "CLOSED";
                    _this.custDetials.responseRelationData.status = "CLOSED";
                }
                else {
                    $("#view-customer-id").css('display', 'block');
                    $(".remove-disable").prop('disabled', false);
                    $(".remove-disable-status").prop('disabled', false);
                    $("#add-target-button").css('display', 'block');
                    $("#add-role-button").css('display', 'block');
                    //$("#update-basic-button").css('display', 'block');
                    _this.customerBaseDetails.status = "ACTIVE";
                    $("#edit-table-id").css('display', 'block');
                    $("#view-customer-id").css('display', 'block');
                    $("#view-table-id").css('display', 'none');
                    _this.custDetials.responseRelationData.status = "ACTIVE";
                }
                $("#edit-table-id").css('display', 'block');
                $("#view-customer-id").css('display', 'block');
                $("#view-table-id").css('display', 'none');
            }
        });
    };
    // Add new Targets
    // Add Targets For The Customer
    ViewCustomerComponent.prototype.addtargetList = function () {
        $("#remove-role-button").css('display', 'block');
        this.targetList.push({
            buCode: "",
            customerTargets: [
                {
                    month: "1",
                },
                {
                    month: "2",
                },
                {
                    month: "3"
                },
                {
                    month: "4",
                },
                {
                    month: "5",
                },
                {
                    month: "6"
                },
                {
                    month: "7",
                },
                {
                    month: "8",
                },
                {
                    month: "9"
                },
                {
                    month: "10",
                },
                {
                    month: "11",
                },
                {
                    month: "12"
                }
            ]
        });
    };
    // Remove  Targets
    ViewCustomerComponent.prototype.removetargetList = function (e) {
        this.targetList.splice(e, 1);
    };
    // Update Customer Details
    ViewCustomerComponent.prototype.updateCustomerDetailsSubmit = function () {
        var _this = this;
        this.custDetials.customerTargets = this.targetList;
        delete this.custDetials.responseRelationData;
        delete this.custDetials.customerRelationship;
        this.custDetials.customerDeatils = this.customerBaseDetails;
        this.httpService.postRequest(this.custDetials, '../ManageCustomer/UpdateCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.sucessResponseData = data.rsBody.sucess;
                $('#sucessPopup').bPopup();
                //modal.open('updateSuccessPopup')
                $("#update-basic-button").css('display', 'none');
                $("#sucess-span").css('display', 'block');
                //Utilities.transitionTo('view-customer', $scope);
            }
        });
    };
    // Add Customer Relation 
    ViewCustomerComponent.prototype.addCustomerRelationDetails = function () {
        $("#add-relationship").css('display', 'block');
        $("#add-role-button").css('display', 'none');
        $("#updateCustomerBtn").css('display', 'none');
        // Create Array On Click
        this.custDetials.customerRelationship = {};
        this.custDetials.customerRelationship = [{
                yearRecorded: this.yearRecorded,
            }];
        $("#remove-role-button").css('display', 'block');
        $('html, body').animate({ scrollTop: 700 }, 'slow');
        return false;
    };
    // Remove Customer Relation Details
    ViewCustomerComponent.prototype.removeCustomerRelationDetails = function () {
        $("#add-relationship").css('display', 'none');
        $("#add-role-button").css('display', 'block');
        this.custDetials.customerRelationship = {};
        this.custDetials.customerRelationship = [];
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    };
    ViewCustomerComponent.prototype.updateCustomer = function () {
        var _this = this;
        var day = this.customerBaseDetails.doaStr.day.toString();
        var month = this.customerBaseDetails.doaStr.month.toString();
        console.log(month.length);
        if (month.length == 1) {
            month = "0" + month;
        }
        if (day.length == 1) {
            day = "0" + day;
        }
        var year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;
        if (this.customerBaseDetails.docStr != null) {
            var day = this.customerBaseDetails.docStr.day.toString();
            var month = this.customerBaseDetails.docStr.month.toString();
            console.log(month.length);
            if (month.length == 1) {
                month = "0" + month;
            }
            if (day.length == 1) {
                day = "0" + day;
            }
            var year = this.customerBaseDetails.docStr.year.toString();
            this.customerBaseDetails.docStr = day + '-' + month + '-' + year;
        }
        this.custDetials.customerDeatils = this.customerBaseDetails;
        this.httpService.postRequest(this.customerBaseDetails, '../ManageCustomer/UpdateCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.customerBaseDetails = data.rsBody.customerDetails;
                var dateString = data.rsBody.customerDetails.doaStr; // "13/10/2014";
                var dataSplit = dateString.split('-');
                var dateConverted = {
                    day: 1,
                    month: 1,
                    year: 1001
                };
                var dateConverted2 = {
                    day: 1,
                    month: 1,
                    year: 1001
                };
                dateConverted.day = parseInt(dataSplit[0]);
                dateConverted.month = parseInt(dataSplit[1]);
                dateConverted.year = parseInt(dataSplit[2]);
                _this.customerBaseDetails.doaStr = dateConverted;
                var dateString2 = data.rsBody.customerDetails.docStr; // "13/10/2014";
                var dataSplit2 = dateString2.split('-');
                dateConverted2.day = parseInt(dataSplit2[0]);
                dateConverted2.month = parseInt(dataSplit2[1]);
                dateConverted2.year = parseInt(dataSplit2[2]);
                _this.customerBaseDetails.docStr = dateConverted2;
                $("#add-relationship").css('display', 'none');
                $("#add-role-button").css('display', 'block');
                $("#view-customer-id").css('display', 'block');
            }
        });
    };
    // Get Channel List for Select Channel
    ViewCustomerComponent.prototype.onChangeGetList = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].subChannelCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].subArea = '';
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.getList = data.rsBody.resulats;
                _this.custDetials.customerRelationship[0].branchDetailsFK = '';
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                if (_this.getList.subChannel != null && _this.getList.subChannel.length > 0) {
                    _this.subChannelList = _this.getList.subChannel;
                    $("#customerSubChannel").css('display', 'block');
                }
                else {
                    $("#customerSubChannel").css('display', 'none');
                }
                if (_this.getList.regionList != null && _this.getList.regionList.length > 0) {
                    _this.regionList = _this.getList.regionList;
                    $("#customerRegion").css('display', 'block');
                }
                else {
                    $("#customerRegion").css('display', 'none');
                }
                // Subarea
                if (_this.getList.subArea != null && _this.getList.subArea.length > 0) {
                    _this.subAreaData = _this.getList.subArea;
                    $("#subarea").css('display', 'block');
                }
                else {
                    $("#subarea").css('display', 'none');
                }
                if (_this.getList.branchList != null && _this.getList.branchList.length > 0) {
                    _this.branchListData = _this.getList.branchList;
                    $("#customerBranch").css('display', 'block');
                }
                else {
                    $("#customerBranch").css('display', 'none');
                }
                if (_this.getList.areaCode != null && _this.getList.areaCode.length > 0) {
                    _this.responseAreaData = _this.getList.areaCode;
                    $("#customerArea").css('display', 'block');
                }
                else {
                    $("#customerArea").css('display', 'none');
                }
                if (_this.getList.employeeBaseDetails != null && _this.getList.employeeBaseDetails.length > 0) {
                    _this.employeeKAMData = _this.getList.employeeBaseDetails;
                    _this.employeeData = _this.getList.employeeBaseDetails;
                    $("#customerEmpKAMDetails").css('display', 'block');
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#relationship-detail").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpKAMDetails").css('display', 'none');
                    if (data.rsBody.errorMessage != null) {
                        $("#customerEmpMessage").css('display', 'block');
                        _this.errorMessage = data.rsBody.errorMessage;
                        $("#relationship-detail").addClass('ng-hide');
                    }
                    else {
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                }
                // $("#customerEmpDetails").css('display', 'none');
                $("#shipTOPartyCode").css('display', 'none');
            }
        });
    };
    ViewCustomerComponent.prototype.kamEmployeeList = function () {
        $("#customerArea").css('display', 'none');
    };
    // Get SubChannel List 
    ViewCustomerComponent.prototype.onChangeGetSubChannelList = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.getList = data.rsBody.resulats;
                _this.custDetials.customerRelationship[0].branchDetailsFK = '';
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                if (_this.getList.areaCode != null && _this.getList.areaCode.length > 0) {
                    _this.responseAreaData = _this.getList.areaCode;
                    $("#customerArea").css('display', 'block');
                }
                else {
                    $("#customerArea").css('display', 'none');
                }
                if (_this.getList.branchList != null && _this.getList.branchList.length > 0) {
                    _this.branchListData = _this.getList.branchList;
                    $("#customerBranch").css('display', 'block');
                }
                else {
                    $("#customerBranch").css('display', 'none');
                }
                if (_this.getList.employeeBaseDetails != null && _this.getList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#relationship-detail").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    if (data.rsBody.errorMessage != null && data.rsBody.errorMessage != "") {
                        $("#customerEmpMessage").css('display', 'block');
                        _this.errorMessage = data.errorMessage;
                        $("#relationship-detail").addClass('ng-hide');
                    }
                    else {
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                }
                $("#shipTOPartyCode").css('display', 'none');
            }
        });
    };
    // Get Employee List Usin Brnch
    ViewCustomerComponent.prototype.getEmployeeSelecedBranch = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetEmployeeListRelatedBranchData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                _this.getEmployeeList = data.rsBody;
                _this.shipToRequire = data.rsBody.shipToRequire;
                if (_this.getEmployeeList.employeeBaseDetails != null && _this.getEmployeeList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getEmployeeList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#relationship-detail").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    $("#customerEmpMessage").css('display', 'block');
                    _this.errorMessage = data.rsBody.errorMessage;
                    $("#relationship-detail").addClass('ng-hide');
                }
                if (_this.shipToRequire != null) {
                    if (_this.shipToRequire.keyName == "MTSE" && _this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                        _this.lengthdata = _this.shipToPartyCodes.length;
                        $("#shipTOPartyCode").css('display', 'block');
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                }
                else {
                    $("#shipTOPartyCode").css('display', 'none');
                }
                $("#customerEmpKAMDetails").css('display', 'none');
            }
        });
    };
    //Get Employee List Usin Brnch
    ViewCustomerComponent.prototype.getEmployeeSelectedArea = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetEmployeeListRelatedBranchData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                _this.getEmployeeList = data.rsBody;
                _this.shipToRequire = data.rsBody.shipToRequire;
                if (_this.getEmployeeList.employeeBaseDetails != null && _this.getEmployeeList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getEmployeeList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#relationship-detail").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    $("#customerEmpMessage").css('display', 'block');
                    _this.errorMessage = data.rsBody.errorMessage;
                    $("#relationship-detail").addClass('ng-hide');
                }
                if (_this.shipToRequire != null) {
                    if (_this.shipToRequire.keyName == "MTSE" && _this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                        _this.lengthdata = _this.shipToPartyCodes.length;
                        $("#shipTOPartyCode").css('display', 'block');
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                }
                else {
                    $("#shipTOPartyCode").css('display', 'none');
                }
                $("#customerEmpKAMDetails").css('display', 'none');
            }
        });
    };
    // Add Ship To Party Codes
    ViewCustomerComponent.prototype.addShipToCode = function () {
        this.shipToPartyCodes.push({});
        this.lengthdata = this.shipToPartyCodes.length;
    };
    // Remove Ship To party Code
    ViewCustomerComponent.prototype.removeShipToCode = function (e) {
        this.shipToPartyCodes.splice(e, 1);
        this.lengthdata = this.shipToPartyCodes.length;
    };
    // Customer Relatonship Submit
    ViewCustomerComponent.prototype.customerRelationshipSubmit = function () {
        var _this = this;
        //this.custDetials.customerRelationship = [{
        //    yearRecorded: this.yearRecorded,
        //}];
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].customerGeoDetailsFK = this.custDetials.customerBaseDetailsFK;
            this.custDetials.customerRelationship[i].shipToPartyCodes = this.custDetials.shipToPartyCodes;
            if (this.custDetials.customerRelationship[i].branchDetailsFK == "") {
                this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            }
            if (this.custDetials.customerRelationship[i].employeeRoleFK == "") {
                this.custDetials.customerRelationship[i].employeeRoleFK = 0;
            }
        }
        //this.customerRelationship = this.custDetials.customerRelationship[0];
        this.custDetials.customerDeatils = this.customerBaseDetails;
        //this.custDetials.customerRelationship = this.customerRelationship;
        //this.success = this.responseRelationData.status
        var day = this.customerBaseDetails.doaStr.day.toString();
        var month = this.customerBaseDetails.doaStr.month.toString();
        console.log(month.length);
        if (month.length == 1) {
            month = "0" + month;
        }
        var year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;
        this.custDetials.customerDeatils = this.customerBaseDetails;
        delete this.custDetials.responseRelationData;
        //this.success = "";
        this.httpService.postRequest(this.custDetials, '../ManageCustomer/CustomerRelationSubmit', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                if (data.rsBody.retVal.isValid == false) {
                    //this.custDetials.responseRelationData = data.rsBody.retData;
                    $("#add-relationship").css('display', 'none');
                    $("#add-role-button").css('display', 'block');
                    _this.getCustomerDetailsSubmit();
                    $("#updateErrorPopup").modal();
                }
                else {
                    _this.custDetials.responseRelationData = data.rsBody.retData;
                    _this.custDetials.responseRelationData = data.rsBody.retData.customerRelationship;
                    var dateString = _this.custDetials.customerDeatils.doaStr; // "13/10/2014";
                    var dataSplit = dateString.split('-');
                    var dateConverted = {
                        day: 1,
                        month: 1,
                        year: 1001
                    };
                    dateConverted.day = parseInt(dataSplit[0]);
                    dateConverted.month = parseInt(dataSplit[1]);
                    dateConverted.year = parseInt(dataSplit[2]);
                    _this.customerBaseDetails.doaStr = dateConverted;
                    $("#add-relationship").css('display', 'none');
                    $("#add-role-button").css('display', 'block');
                    $("#view-customer-id").css('display', 'block');
                    //this.custDetials.customerRelationship = {};
                    //this.getCustomerDetailsSubmit();
                }
            }
        });
    };
    // Delete Customer Relationship
    ViewCustomerComponent.prototype.deleteRelSubmit = function (e) {
        this.id = this.custDetials.responseRelationData[e].id;
        $('#deleteEmployeeRolePopup').modal();
    };
    ViewCustomerComponent.prototype.deleteSubmit = function () {
        this.id = this.id;
        //$("#deleteEmployeeRolePopup").modal().close();
        //$("#deleteEmployeeRolePopup").close();
        this.deleteRelationship();
    };
    // Customer Relatonship Submit
    ViewCustomerComponent.prototype.deleteRelationship = function () {
        /*pre populate data deleted*/
        var _this = this;
        this.deleteRel.id = this.id;
        this.httpService.postRequest(this.deleteRel, '../ManageCustomer/DeleteCustomerRelationship', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.sucessData = data.rsBody.result;
                for (var i = 0; i < _this.custDetials.responseRelationData.length; i++) {
                    if (_this.custDetials.responseRelationData[i].id == _this.inputData.id) {
                        _this.custDetials.responseRelationData.splice(i, 1);
                        break;
                    }
                }
                _this.getCustomerDetailsSubmit();
            }
        });
    };
    ViewCustomerComponent.prototype.ngReginedSubmit = function () {
        var inputData = this.customerBaseDetails.status;
        if (inputData == "CLOSED") {
            $("#customerTargets").css('display', 'none');
            $("#view-customer-id").css('display', 'none');
            $("#add-role-button").css('display', 'none');
            $(".remove-disable").prop('disabled', true);
            $(".docStrId").css('display', 'block');
        }
        else {
            $("#customerTargets").css('display', 'block');
            $("#view-customer-id").css('display', 'block');
            $(".docStrId").css('display', 'none');
            $("#add-role-button").css('display', 'none');
            $(".remove-disable").prop('disabled', false);
        }
    };
    ViewCustomerComponent.prototype.ngAfterViewInit = function () {
    };
    ViewCustomerComponent.prototype.ngOnDestroy = function () {
    };
    ViewCustomerComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ViewCustomerComponent);
    return ViewCustomerComponent;
}());
exports.ViewCustomerComponent = ViewCustomerComponent;
//# sourceMappingURL=viewcustomer.component.js.map