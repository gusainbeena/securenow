"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var ValidationHandler_1 = require("../ValidationHandler");
var lib_1 = require("../../lib");
//import { BookService } from './book.service';
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'AccessControl';
var AccessControlComponent = /** @class */ (function () {
    function AccessControlComponent(httpservice, routes, activatedroute, validation, cdref) {
        var _this = this;
        this.httpservice = httpservice;
        this.routes = routes;
        this.activatedroute = activatedroute;
        this.validation = validation;
        this.cdref = cdref;
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            enableCheckAll: false
        };
        this.items = [];
        //public selectedProfileOUs: Array<TreeviewItem> = [];
        this.finalDataList = { selectedRole: [] };
        this.newDataList = {};
        this.roleStr = [];
        this.servicesStr = [];
        this.OUsStr = [];
        this.OUsKeys = [];
        this.operationalUnit = [];
        //public operationalUnit: Array<string> = [];
        this.permissionsList = [];
        this.monthData = [];
        this.yearList = [];
        this.roleData = {};
        this.newrole = false;
        this.roleBase = true;
        this.roleHierarchyList = [];
        this.roleHierarchyStrList = [];
        this.navigationSubscription = this.routes.events.subscribe(function (e) {
            _this.finalDataList = { selectedRole: [] };
            _this.newDataList = {};
            _this.pageNo = 0;
            _this.roleStr = [];
            _this.servicesStr = [];
            _this.OUsStr = [];
            _this.permissionsList = [];
            _this.monthData = [];
            _this.yearList = [];
            _this.roleData = {};
            _this.newrole = false;
            _this.userLevelAccess = 'adpID';
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof router_1.NavigationEnd) {
                // this.items = this.getBooks();
                _this.getRoutesInitialStateWithRoleBase();
            }
        });
    }
    AccessControlComponent.prototype.ngOnInit = function () {
        /*$('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
        this.getAllRoles();*/
    };
    AccessControlComponent.prototype.getAllRoles = function () {
        var _this = this;
        this.currentTab = "AccessControl";
        this.subTab = "Role";
        this.roleStr = [];
        this.httpservice.postRequest({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageProfilePermission/GetExistingMetaData', true).subscribe(function (datas) {
            if (datas.rsBody.result === "success") {
                for (var i = 0; i < datas.rsBody.existingList.length; i++) {
                    _this.roleStr.push(datas.rsBody.existingList[i]);
                }
            }
        });
        this.getAllPermissions();
    };
    AccessControlComponent.prototype.trivarseTree = function (e) {
        var _this = this;
        if (e.children != undefined) {
            e.children.map(function (itemList) {
                var indexData = _this.finalDataList.selectedOUs.indexOf(itemList.value);
                if (indexData != -1) {
                    itemList.checked = true;
                }
                else {
                    itemList.checked = false;
                }
                _this.trivarseTree(itemList);
            });
        }
    };
    AccessControlComponent.prototype.trivarseTreeDisable = function (e) {
        var _this = this;
        if (e.children != undefined) {
            e.children.map(function (itemList) {
                var indexData = _this.finalDataList.selectedOUs.indexOf(itemList.value);
                if (indexData != -1) {
                    itemList.checked = true;
                    itemList.disabled = true;
                }
                else {
                    itemList.checked = false;
                    itemList.disabled = true;
                }
                _this.trivarseTree(itemList);
            });
        }
    };
    AccessControlComponent.prototype.getExistingAccess = function (e) {
        var _this = this;
        if (!this.newrole && this.roleBase) {
            if (e.target.id != "") {
                this.finalDataList["profile"] = [e.target.id];
            }
            else {
                $('.rolelink').parent().removeClass('active');
                $(e.target.parentElement).addClass('active');
                this.finalDataList["profile"] = [e.target.parentElement.id];
            }
            this.httpservice.postRequest(this.finalDataList, '../ManageProfilePermission/GetExistingAccessForProfile', true).subscribe(function (datas) {
                if (datas.rsBody.result === "success") {
                    _this.finalDataList = Object.assign({}, datas.rsBody.listData);
                    _this.items = [];
                    _this.finalDataList.ouList.map(function (item) {
                        var treeNode = new lib_1.TreeviewItem(item);
                        _this.items.push(treeNode);
                    });
                    _this.items.map(function (itemList) {
                        var index = _this.finalDataList.selectedOUs.indexOf(itemList.value);
                        if (index != -1) {
                            itemList.checked = true;
                        }
                        else {
                            itemList.checked = false;
                        }
                        _this.trivarseTree(itemList);
                    });
                    $('#permissionList').removeClass('d-none');
                    $('#serviceList').removeClass('d-none');
                    $('#ouList').removeClass('d-none');
                }
            });
        }
        else {
            if (Object.keys(this.finalDataList).indexOf('profile') == -1) {
                //this.finalDataList["profile"] = [e.target.id];
                this.finalDataList.profile = [e.target.id];
            }
            else {
                if (this.finalDataList["profile"].indexOf(e.target.id) == -1) {
                    this.finalDataList["profile"].push(e.target.id);
                }
                else {
                    this.finalDataList["profile"].splice(this.finalDataList["profile"].indexOf(e.target.id), 1);
                }
            }
            this.httpservice.postRequest(this.finalDataList, '../ManageProfilePermission/GetExistingAccessForMultiProfile', true).subscribe(function (datas) {
                if (datas.rsBody.result === "success") {
                    _this.finalDataList = Object.assign({}, datas.rsBody.listData);
                    _this.items = [];
                    _this.finalDataList.ouList.map(function (item) {
                        var treeNode = new lib_1.TreeviewItem(item);
                        _this.items.push(treeNode);
                    });
                    _this.items.map(function (itemList) {
                        var index = _this.finalDataList.selectedOUs.indexOf(itemList.value);
                        if (index != -1) {
                            itemList.checked = true;
                            itemList.disabled = true;
                        }
                        else {
                            itemList.checked = false;
                            itemList.disabled = true;
                        }
                        _this.trivarseTreeDisable(itemList);
                    });
                    $('#permissionList').removeClass('d-none');
                    $('#serviceList').removeClass('d-none');
                    $('#ouList').removeClass('d-none');
                }
            });
        }
    };
    AccessControlComponent.prototype.getRoutesInitialStateWithRoleBase = function () {
        $('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
        this.getAllRoles();
    };
    AccessControlComponent.prototype.getRoutesInitialStateWithUserBase = function () {
        $('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
    };
    AccessControlComponent.prototype.getAllPermissions = function () {
        var _this = this;
        this.currentTab = "AccessControl";
        this.subTab = "Permission";
        var parentRef = this;
        this.httpservice.postRequest({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageProfilePermission/GetExistingMetaData', true).subscribe(function (datas) {
            if (datas.rsBody.result === "success") {
                _this.permissionsList = [];
                for (var _i = 0, _a = datas.rsBody.existingList; _i < _a.length; _i++) {
                    var item = _a[_i];
                    _this.permissionsList.push(item.labelOnScreen);
                }
            }
        });
        //this.getAllOUs();
    };
    AccessControlComponent.prototype.getServiceWisePermissions = function (e, i) {
        var classes = [];
        classes = Object.assign([], e.target.classList);
        if (classes.indexOf('selectallservice' + i) != -1) {
            if ($('input[type="checkbox"][class*="selectallservice' + i + '"]:checked').length == $('input[type="checkbox"][class*="selectallservice' + i + '"]').length) {
                $('input[type="checkbox"][id="selectallservice' + i + '"]').prop('checked', true);
            }
            else {
                $('input[type="checkbox"][id="selectallservice' + i + '"]').prop('checked', false);
            }
            var elem = $('input[type="checkbox"][class*="selectallservice' + i + '"]');
        }
        if (e.target.checked === true) {
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('disabled', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('checked', true);
            $('#chkboxrow' + i).attr('checkMandatory', true);
            this.finalDataList["selectedFunctions"][i].selected = true;
            this.finalDataList["selectedFunctions"][i].selectAll = true;
            for (var i_1 = 0; i_1 < this.finalDataList["selectedPermissions"][e.target.id].length; i_1++) {
                this.finalDataList["selectedPermissions"][e.target.id][i_1].selected = true;
            }
        }
        else {
            $('#chkboxrow' + i).attr('checkMandatory', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('checked', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('disabled', true);
            this.finalDataList["selectedFunctions"][i].selected = false;
            this.finalDataList["selectedFunctions"][i].selectAll = false;
            for (var i_2 = 0; i_2 < this.finalDataList["selectedPermissions"][e.target.id].length; i_2++) {
                this.finalDataList["selectedPermissions"][e.target.id][i_2].selected = false;
            }
        }
    };
    AccessControlComponent.prototype.addNewRole = function () {
        this.newrole = true;
        this.getYearList();
    };
    AccessControlComponent.prototype.saveNewRole = function () {
        var _this = this;
        this.roleData['key'] = "AccessControl";
        this.roleData['subkey'] = "Role";
        var status = this.validation.validateDOM('newrole');
        if (!status) {
            this.httpservice.postRequest(this.roleData, '../ManageProfilePermission/CreateMetaData', true).subscribe(function (datas) {
                if (datas.rsBody.result == 'success') {
                    $("#success").modal();
                    _this.routes.navigate(["access-control"]);
                }
                else {
                    if (datas.rsBody.exceptionBlock != null) {
                        $('html,body').animate({ scrollTop: $('#newrole').offset().top }, 'slow');
                        _this.validation.displayErrors(datas.rsBody.exceptionBlock.msg, 'newrole', '');
                    }
                }
            });
        }
        else {
            $('html,body').animate({ scrollTop: $('#newrole').offset().top }, 'slow');
        }
    };
    AccessControlComponent.prototype.getYearList = function () {
        var _this = this;
        this.httpservice.postRequest({ keyType: "YEAR" }, '../Comman/GetKeyValueData', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.yearList = datas.rsBody.resulats;
                _this.roleData.year = _this.CFY;
                _this.getMonthList();
            }
        });
    };
    AccessControlComponent.prototype.getMonthList = function () {
        var _this = this;
        this.httpservice.postRequest({}, '../Comman/GetMonthKeyValueData', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.monthData = datas.rsBody.resulats;
                _this.roleData['startMonth'] = _this.monthData[0].keyValue;
                _this.roleData['endMonth'] = _this.monthData[_this.monthData.length - 1].keyValue;
            }
        });
    };
    AccessControlComponent.prototype.grantAccess = function (id) {
        var _this = this;
        this.roleData['key'] = "AccessControl";
        this.roleData['subkey'] = "Role";
        this.finalDataList.treeViewItem = this.items;
        var status = this.validation.validateDOM(id);
        if (!status) {
            this.httpservice.postRequest(this.finalDataList, '../ManageProfilePermission/CreateAccessPermissions', true).subscribe(function (datas) {
                if (datas.rsBody.result == 'success') {
                    $("#success").modal();
                    _this.routes.navigate(["access-control"]);
                }
                else {
                    if (datas.rsBody.exceptionBlock != null) {
                        $('html,body').animate({ scrollTop: $('#' + id).offset().top }, 'slow');
                        _this.validation.displayErrors(datas.rsBody.exceptionBlock.msg, id, '');
                    }
                }
            });
        }
        else {
            $('html,body').animate({ scrollTop: $('#' + id).offset().top }, 'slow');
        }
    };
    AccessControlComponent.prototype.changeToUserBase = function (type) {
        if (type === 'user') {
            this.roleBase = false;
        }
        else {
            this.roleBase = true;
        }
        this.routes.navigate(["access-control"]);
    };
    AccessControlComponent.prototype.getAdpIdData = function (id) {
        var _this = this;
        var text = $('#' + id);
        if ($('#' + id)[0].value != undefined && $('#' + id)[0].value != "") {
            this.finalDataList["user"] = {};
            this.finalDataList["user"]["adpId"] = $('#' + id)[0].value;
            this.httpservice.postRequest(this.finalDataList, '../ManageProfilePermission/ADPIdData', true).subscribe(function (datas) {
                if (datas.rsBody.result == 'success') {
                    _this.finalDataList["user"] = Object.assign({}, datas.rsBody.empDet);
                    $('#roleList').removeClass('d-none');
                    $('#empDataPanel').removeClass('d-none');
                    _this.finalDataList.user.adpId = datas.rsBody.empDet.adpId;
                }
                else {
                    _this.finalDataList["user"] = {};
                    $('#roleList').addClass('d-none');
                }
            });
        }
        else {
            alert("please enter a valid employee id");
        }
    };
    AccessControlComponent.prototype.onRoleItemSelect = function (e) {
        if (this.finalDataList.selectedRole != undefined && this.finalDataList.selectedRole.length > 0) {
            $('#roleList').removeClass('d-none');
        }
        else {
            $('#roleList').addClass('d-none');
        }
    };
    AccessControlComponent.prototype.closePanel = function () {
        this.userLevelAccess == "";
        $('#empDataPanel').addClass('d-none');
    };
    AccessControlComponent.prototype.setUserLevelAccess = function (e) {
        var _this = this;
        this.userLevelAccess = e.target.id;
        this.currentTab = "RoleHierarchy";
        this.subTab = "RoleHierarchy";
        this.finalDataList["user"] = undefined;
        if (e.target.id === 'reportingRole') {
            this.httpservice.postRequest({ pageNumber: this.pageNo, metaDataLevel: 1, currentTab: this.currentTab }, '../ManageProfilePermission/getEmployeeAttributes', true).subscribe(function (datas) {
                if (datas.rsBody.result == 'success') {
                    _this.roleHierarchyStrList = [];
                    for (var i = 0; i < datas.rsBody.attributesList.length; i++) {
                        _this.roleHierarchyStrList.push(datas.rsBody.attributesList[i].labelOnScreen);
                    }
                }
            });
        }
    };
    AccessControlComponent.prototype.selectAll = function (e, i) {
        var id = e.target.id;
        if ($('input[type="checkbox"][id="' + id + '"]').is(':checked')) {
            $('.' + id).prop('checked', true);
        }
        else {
            $('.' + id).prop('checked', false);
        }
        var val = id.substr(id.length - 1);
        var val2 = this.finalDataList["selectedFunctions"][val].row.keyValue;
        if (e.target.checked === true) {
            this.finalDataList["selectedFunctions"][i].selectAll = true;
            for (var i_3 = 0; i_3 < this.finalDataList["selectedPermissions"][val2].length; i_3++) {
                //this.finalDataList["selectedPermissions"][e.target.id][i].selected = true;
                this.finalDataList["selectedPermissions"][val2][i_3].selected = true;
            }
        }
        else {
            this.finalDataList["selectedFunctions"][i].selectAll = false;
            for (var i_4 = 0; i_4 < this.finalDataList["selectedPermissions"][val2].length; i_4++) {
                //this.finalDataList["selectedPermissions"][e.target.id][i].selected = false;
                this.finalDataList["selectedPermissions"][val2][i_4].selected = false;
            }
        }
    };
    AccessControlComponent.prototype.checkboxMentain = function (e, i, j) {
        var classes = [];
        classes = Object.assign([], e.target.classList);
        var id = e.target.id.split('_')[0];
        if (classes.indexOf('selectallpermission' + i) != -1) {
            if ($('input[type="checkbox"][class*="selectallpermission' + i + '"]:checked').length == $('input[type="checkbox"][class*="selectallpermission' + i + '"]').length) {
                $('input[type="checkbox"][id="selectallpermission' + i + '"]').prop('checked', true);
            }
            else {
                $('input[type="checkbox"][id="selectallpermission' + i + '"]').prop('checked', false);
            }
            var elem = $('input[type="checkbox"][class*="selectallpermission' + i + '"]');
        }
        if (e.target.checked === true) {
            //this.finalDataList["selectedFunctions"][i].selectAll = true;
            this.finalDataList["selectedPermissions"][id][j].selected = true;
        }
        else {
            //this.finalDataList["selectedFunctions"][i].selectAll = false;
            this.finalDataList["selectedPermissions"][id][j].selected = false;
        }
    };
    AccessControlComponent.prototype.setAllOUs = function (e, key, i) {
    };
    AccessControlComponent.prototype.setOUs = function (e, key, i, j) {
        if (e.target.checked === true) {
            this.finalDataList["selectedOperationalUnits"][key][j].selected = true;
        }
        else {
            this.finalDataList["selectedOperationalUnits"][key][j].selected = false;
        }
    };
    AccessControlComponent.prototype.handleCollaps = function (item, e, list) {
        $(e.target).toggleClass("fa-caret-down fa-caret-right");
    };
    AccessControlComponent.prototype.handleOUSelection = function (item, e) {
        item.selected = e.target.checked;
        var ary = e.target.className.split(' ')[1];
        if (e.target.checked) {
            if ($('input[type="checkbox"][class*="' + ary + '"]:checked').length == $('input[type="checkbox"][class*="' + ary + '"]').length) {
                $('#' + ary).css("");
                $('#' + ary).trigger('click');
            }
            else {
                //$('input[type="checkbox"][id="allSalesComponentSelect"]').prop('checked', false);
            }
            if (item.selected == true) {
                $('#' + item.parentNode.keyValue + '_chkbox').trigger('click');
            }
            else {
            }
            if (item.childNodes.length > 0) {
                item.childNodes.map(function (node) {
                    if (e.target.checked != node.selected) {
                        node.selected = true;
                        $('#' + node.nodeVal.keyValue + '_chkbox').trigger('click');
                    }
                });
            }
            else {
                item.selected = true;
            }
        }
        else {
            if ($('input[type="checkbox"][class*="' + ary + '"]:checked').length == $('input[type="checkbox"][class*="' + ary + '"]').length) {
                $('#' + ary).trigger('click');
            }
            else {
                // $('#' + ary).trigger('click');
            }
            if (item.childNodes.length > 0) {
                item.childNodes.map(function (node) {
                    if (e.target.checked != node.selected) {
                        node.selected = false;
                        $('#' + node.nodeVal.keyValue + '_chkbox').trigger('click');
                    }
                });
            }
            else {
                item.selected = false;
            }
        }
        var v = this.operationalUnit;
    };
    AccessControlComponent.prototype.hideErrors = function (id) {
        this.validation.hideErrors(id);
    };
    AccessControlComponent.prototype.ngAfterViewInit = function () {
    };
    AccessControlComponent.prototype.ngOnDestroy = function () {
    };
    AccessControlComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService, router_1.Router, router_1.ActivatedRoute, ValidationHandler_1.ValidationHandler, core_1.ChangeDetectorRef])
    ], AccessControlComponent);
    return AccessControlComponent;
}());
exports.AccessControlComponent = AccessControlComponent;
//# sourceMappingURL=accesscontrol.component.js.map