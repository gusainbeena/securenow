"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateCustomer';
var CreateCustomerComponent = /** @class */ (function () {
    function CreateCustomerComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        //public keyValueData: Object = {};
        this.keyValueData = {};
        this.shipToRequire = {};
        this.gradeData = {};
        this.custDetials = {};
        this.customerBaseDetails = {};
        this.responseChannelData = [];
        this.responseStateData = [];
        this.BUData = [];
        this.monthData = [];
        this.yearData = [];
        this.regionData = [];
        this.dataTypeList = [];
        this.subChannelList = [];
        this.employeeData = [];
        this.getList = [];
        this.targetData = [];
        this.targetList = [];
        this.regionList = [];
        this.branchListData = [];
        this.subAreaData = [];
        this.responseAreaData = [];
        this.customerDetialsData = [];
        this.MonthList = [];
        this.getEmployeeList = [];
        this.shipToPartyCodes = [];
        this.targetType = [];
        this.sucessResponseData = [];
        this.ChannelList = [];
        this.monthFrequencyList = [];
        this.errorMessage = '';
        this.day = '';
        this.month = '';
        this.year = '';
    }
    // DatePicker
    CreateCustomerComponent.prototype.addSoldToPartyCodes = function () {
        this.customerBaseDetails.soldToPartyCodes.push({});
    };
    CreateCustomerComponent.prototype.removeSoldToPartyCodes = function (e) {
        this.customerBaseDetails.soldToPartyCodes.splice(e, 1);
    };
    CreateCustomerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.customerBaseDetails = {
            soldToPartyCodes: [{}]
        };
        this.custDetials = {
            customerDeatils: {},
            customerTargets: [],
            customerRelationship: []
        };
        $("#customerTargets").css('display', 'block');
        $(".form-steps").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-target").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        //this.keyValueData.keyType = "MetaData";//"CHANNEL";
        // this.keyValueData.keyName = "Channel";
        this.keyValueData.keyType = "Channel"; //"CHANNEL";
        this.keyValueData.keyName = "SalesHorizontal";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (datas) {
            if (datas.rsBody.result == 'success') {
                _this.responseChannelData = datas.rsBody.resulats;
                _this.loadState();
            }
        });
    };
    //Get State List
    CreateCustomerComponent.prototype.loadState = function () {
        var _this = this;
        this.keyValueData.keyType = "STATE";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.responseStateData = data.rsBody.resulats;
                _this.customerBaseDetails.status = "ACTIVE";
                _this.getBUList();
            }
        });
    };
    //Get BU List
    CreateCustomerComponent.prototype.getBUList = function () {
        var _this = this;
        this.keyValueData.keyType = "MetaData"; // "OPERATIONAL";
        this.keyValueData.keyName = "businessVertical"; //"BU";
        this.httpService.postRequest(this.keyValueData, '../ManageCustomer/GetBUBGListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.BUData = data.rsBody.resulats;
                _this.getDataTypeList();
            }
        });
    };
    //Get Data Type List
    CreateCustomerComponent.prototype.getDataTypeList = function () {
        var _this = this;
        this.keyValueData.keyType = "MetaData"; //"OPERATIONAL";
        this.keyValueData.keyName = "businessVertical"; //"BU";
        this.httpService.postRequest(this.keyValueData, '../ManageCustomer/GetDataTypeList', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.dataTypeList = data.rsBody.dataTypeList;
                _this.getMonthList();
            }
        });
    };
    //Get Year List
    CreateCustomerComponent.prototype.getYearList = function () {
        var _this = this;
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.yearData = data.rsBody.resulats;
                _this.customerBaseDetails.yearRecorded = _this.yearData[0].keyValue;
                _this.customerBaseDetails.startMonth = "1";
                _this.customerBaseDetails.endMonth = "12";
            }
        });
    };
    //Get Month List
    CreateCustomerComponent.prototype.getMonthList = function () {
        var _this = this;
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetMonthKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.monthData = data.rsBody.resulats;
                _this.monthFrequencyList = data.rsBody.monthList;
                _this.getRegionList();
            }
        });
    };
    //Get REgion List
    CreateCustomerComponent.prototype.getRegionList = function () {
        var _this = this;
        this.keyValueData.keyType = "REGION";
        this.httpService.postRequest(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.regionData = data.rsBody.resulats;
                _this.getYearList();
            }
        });
    };
    // Create Customer Details Submit
    CreateCustomerComponent.prototype.validateCustomerDetailsSubmit = function () {
        var _this = this;
        this.day = this.customerBaseDetails.doaStr.day.toString();
        this.month = this.customerBaseDetails.doaStr.month.toString();
        console.log(this.month.length);
        if (this.month.length == 1) {
            this.month = "0" + this.month;
        }
        this.year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = this.day + '-' + this.month + '-' + this.year;
        this.httpService.postRequest(this.customerBaseDetails, '../ManageCustomer/ValidateCustomerBasicDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.customerDetialsData = data.rsBody.custDetails;
                $("#step-one").css('display', 'none');
                $(".step-one").removeClass('progress-bar-success');
                $(".step-one").addClass('progress-bar-inverse');
                //$(".step-two").addClass('progress-bar-success');
                //$(".step-two").removeClass('progress-bar-inverse');
                //$("#step-twos").css('display', 'block');
                $(".step-three").addClass('progress-bar-success');
                $(".step-three").removeClass('progress-bar-inverse');
                $("#step-three").css('display', 'block');
                $(".step-three").addClass('progress-bar-success');
                $(".step-three").removeClass('progress-bar-inverse');
                $("#step-three").css('display', 'block');
                if (_this.custDetials.customerRelationship.length == 0) {
                    _this.custDetials.customerRelationship = [{
                            startMonth: _this.customerBaseDetails.startMonth,
                            endMonth: _this.customerBaseDetails.endMonth,
                            yearRecorded: _this.customerBaseDetails.yearRecorded,
                        }];
                }
            }
        });
    };
    // Validate Customer Targets
    CreateCustomerComponent.prototype.validateTargetsSubmit = function () {
        var _this = this;
        $("#customerType").addClass('ng-hide');
        this.httpService.postRequest(this.targetList, '../ManageCustomer/ValidateCustomerTargets', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.targetData = data.rsBody.custDetails;
                $("#step-twos").css('display', 'none');
                $(".step-two").removeClass('progress-bar-success');
                $(".step-two").addClass('progress-bar-inverse');
                $(".step-three").addClass('progress-bar-success');
                $(".step-three").removeClass('progress-bar-inverse');
                $("#step-three").css('display', 'block');
                if (_this.custDetials.customerRelationship.length == 0) {
                    _this.custDetials.customerRelationship = [{
                            startMonth: _this.customerBaseDetails.startMonth,
                            endMonth: _this.customerBaseDetails.endMonth,
                            yearRecorded: _this.customerBaseDetails.yearRecorded,
                        }];
                }
            }
        });
    };
    //On selection of Sales Component
    CreateCustomerComponent.prototype.OnSalesComponentChange = function (data) {
        var _this = this;
        this.keyValueData.keyValue = data;
        this.keyValueData.keyType = "MetaData";
        this.httpService.postRequest(this.keyValueData, '../ManageCustomer/onSalesComponentChange', true).subscribe(function (data) {
            if (data.rsBody.result == 'success') {
                _this.monthFrequencyList = data.rsBody.month;
            }
        });
    };
    // Previous for step 2
    CreateCustomerComponent.prototype.previouSubmit = function () {
        $("#step-twos").css('display', 'none');
        $(".step-two").removeClass('progress-bar-success');
        $(".step-two").addClass('progress-bar-inverse');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    };
    // Previous for step 3
    CreateCustomerComponent.prototype.previouSecondSubmit = function () {
        $("#step-three").css('display', 'none');
        $(".step-three").removeClass('progress-bar-success');
        $(".step-three").addClass('progress-bar-inverse');
        // $(".step-two").addClass('progress-bar-success');
        //$(".step-two").removeClass('progress-bar-inverse');
        // $("#step-twos").css('display', 'block');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    };
    // Add Targets For The Customer
    CreateCustomerComponent.prototype.addtargetList = function () {
        $("#remove-role-button").css('display', 'block');
        this.targetList.push({
            buCode: "",
            customerTargets: [
                {
                    month: "1",
                },
                {
                    month: "2",
                },
                {
                    month: "3"
                },
                {
                    month: "4",
                },
                {
                    month: "5",
                },
                {
                    month: "6"
                },
                {
                    month: "7",
                },
                {
                    month: "8",
                },
                {
                    month: "9"
                },
                {
                    month: "10",
                },
                {
                    month: "11",
                },
                {
                    month: "12"
                }
            ]
        });
    };
    // Remove Targets
    CreateCustomerComponent.prototype.removetargetList = function (e) {
        this.targetList.splice(e, 1);
    };
    // Get Channel List for Select Channel
    CreateCustomerComponent.prototype.onChangeGetList = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].subChannelCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].subArea = '';
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.getList = data.rsBody.resulats;
                _this.custDetials.customerRelationship[0].branchDetailsFK = '';
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                // Subchannel
                if (_this.getList.subChannel != null && _this.getList.subChannel.length > 0) {
                    _this.subChannelList = _this.getList.subChannel;
                    $("#customerSubChannel").css('display', 'block');
                }
                else {
                    $("#customerSubChannel").css('display', 'none');
                }
                if (_this.getList.regionList != null && _this.getList.regionList.length > 0) {
                    _this.regionList = _this.getList.regionList;
                    $("#customerRegion").css('display', 'block');
                }
                else {
                    $("#customerRegion").css('display', 'none');
                }
                // Subarea
                if (_this.getList.subArea != null && _this.getList.subArea.length > 0) {
                    _this.subAreaData = _this.getList.subArea;
                    $("#subarea").css('display', 'block');
                }
                else {
                    $("#subarea").css('display', 'none');
                }
                // Branch List
                if (_this.getList.branchList != null && _this.getList.branchList.length > 0) {
                    _this.branchListData = _this.getList.branchList;
                    $("#customerBranch").css('display', 'block');
                }
                else {
                    $("#customerBranch").css('display', 'none');
                }
                if (_this.getList.areaList != null && _this.getList.areaList.length > 0) {
                    _this.responseAreaData = _this.getList.areaList;
                    $("#customerArea").css('display', 'block');
                }
                else {
                    $("#customerArea").css('display', 'none');
                }
                if (_this.getList.employeeBaseDetails != null && _this.getList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#submit-button").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    if (data.rsBody.errorMessage != null) {
                        $("#customerEmpMessage").css('display', 'block');
                        _this.errorMessage = data.rsBody.errorMessage;
                        $("#submit-button").addClass('ng-hide');
                    }
                    else {
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                }
            }
        });
    };
    CreateCustomerComponent.prototype.kamEmployeeList = function () {
        $("#customerArea").css('display', 'none');
    };
    // Get SubChannel List 
    CreateCustomerComponent.prototype.onChangeGetSubChannelList = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].region = '';
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.getList = data.rsBody.resulats;
                _this.custDetials.customerRelationship[0].branchDetailsFK = '';
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                if (_this.getList.areaList != null && _this.getList.areaList.length > 0) {
                    _this.responseAreaData = _this.getList.areaList;
                    $("#customerArea").css('display', 'block');
                }
                else {
                    $("#customerArea").css('display', 'none');
                }
                if (_this.getList.branchList != null && _this.getList.branchList.length > 0) {
                    _this.branchListData = _this.getList.branchList;
                    $("#customerBranch").css('display', 'block');
                }
                else {
                    $("#customerBranch").css('display', 'none');
                }
                if (_this.getList.regionList != null && _this.getList.regionList.length > 0) {
                    _this.regionList = _this.getList.regionList;
                    $("#customerRegion").css('display', 'block');
                }
                else {
                    $("#customerRegion").css('display', 'none');
                }
                if (_this.getList.employeeBaseDetails != null && _this.getList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#submit-button").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    if (data.errorMessage != null && data.errorMessage != "") {
                        $("#customerEmpMessage").css('display', 'block');
                        _this.errorMessage = data.errorMessage;
                        $("#submit-button").addClass('ng-hide');
                    }
                    else {
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                }
                $("#shipTOPartyCode").css('display', 'none');
            }
        });
    };
    // Get Employee List Usin Brnch
    CreateCustomerComponent.prototype.getEmployeeBasedOnSubArea = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                _this.getEmployeeList = data.rsBody.resulats;
                _this.shipToRequire = data.shipToRequire;
                if (_this.getEmployeeList.employeeBaseDetails != null && _this.getEmployeeList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getEmployeeList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#submit-button").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    $("#customerEmpMessage").css('display', 'block');
                    _this.errorMessage = data.errorMessage;
                    $("#submit-button").addClass('ng-hide');
                }
                if (_this.shipToRequire != null) {
                    if (_this.shipToRequire.keyName == "MTSE" && _this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                        //this.shipToPartyCodes = [{}];
                        _this.lengthdata = _this.shipToPartyCodes.length;
                        $("#shipTOPartyCode").css('display', 'block');
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                }
                else {
                    $("#shipTOPartyCode").css('display', 'none');
                }
                $("#customerEmpKAMDetails").css('display', 'none');
            }
        });
    };
    // Get Employee List Usin Brnch
    CreateCustomerComponent.prototype.getEmployeeSelectedArea = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.custDetials.customerRelationship[0].employeeRoleFK = '';
                _this.getEmployeeList = data.rsBody.resulats;
                _this.shipToRequire = data.shipToRequire;
                if (_this.getEmployeeList.employeeBaseDetails != null && _this.getEmployeeList.employeeBaseDetails.length > 0) {
                    _this.employeeData = _this.getEmployeeList.employeeBaseDetails;
                    $("#customerEmpDetails").css('display', 'block');
                    $("#customerEmpMessage").css('display', 'none');
                    $("#submit-button").removeClass('ng-hide');
                }
                else {
                    $("#customerEmpDetails").css('display', 'none');
                    $("#customerEmpMessage").css('display', 'block');
                    _this.errorMessage = data.errorMessage;
                    $("#submit-button").addClass('ng-hide');
                }
                if (_this.shipToRequire != null) {
                    if (_this.shipToRequire.keyName == "MTSE" && _this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                        //this.shipToPartyCodes = [{}];
                        _this.lengthdata = _this.shipToPartyCodes.length;
                        $("#shipTOPartyCode").css('display', 'block');
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                }
                else {
                    $("#shipTOPartyCode").css('display', 'none');
                }
                $("#customerEmpKAMDetails").css('display', 'none');
            }
        });
    };
    // Add Ship To Party Codes
    CreateCustomerComponent.prototype.addShipToCode = function () {
        this.shipToPartyCodes.push({});
        this.lengthdata = this.shipToPartyCodes.length;
    };
    // Remove Ship To party Code
    CreateCustomerComponent.prototype.removeShipToCode = function (e) {
        this.shipToPartyCodes.splice(e, 1);
        this.lengthdata = this.shipToPartyCodes.length;
    };
    // Create New Customer Details
    CreateCustomerComponent.prototype.createCustomerSubmit = function () {
        var _this = this;
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].customerGeoDetailsFK = this.customerBaseDetailsFK;
            this.custDetials.customerRelationship[i].shipToPartyCodes = this.shipToPartyCodes;
            if (this.custDetials.customerRelationship[i].branchDetailsFK == "") {
                this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            }
            if (this.custDetials.customerRelationship[i].employeeRoleFK == "") {
                this.custDetials.customerRelationship[i].employeeRoleFK = 0;
            }
        }
        this.custDetials.customerDeatils = this.customerDetialsData;
        //this.custDetials.customerTargets = this.targetData;
        this.httpService.postRequest(this.custDetials, '../ManageCustomer/CreateCustomerDetails', true).subscribe(function (data) {
            if (data.rsBody.result === 'success') {
                _this.sucessResponseData = data.rsBody.customerDetails;
                _this.router.navigate(['/AddKeyValue']);
                // this.router.onSameUrlNavigation('reload');
                _this.router.onSameUrlNavigation = "reload";
                $("#sucessPopup").modal();
                //Utilities.transitionTo('create-customer', this);
            }
        });
    };
    CreateCustomerComponent.prototype.ngAfterViewInit = function () {
    };
    CreateCustomerComponent.prototype.ngOnDestroy = function () {
    };
    CreateCustomerComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL,
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], CreateCustomerComponent);
    return CreateCustomerComponent;
}());
exports.CreateCustomerComponent = CreateCustomerComponent;
//# sourceMappingURL=createcustomer.component.js.map