"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/http");
var ng_multiselect_dropdown_1 = require("ng-multiselect-dropdown");
var http_service_1 = require("../../service/http.service");
var modelservice_1 = require("../../service/modelservice");
var d3methodbinding_service_1 = require("../../service/d3methodbinding.service");
var createcustomer_routing_module_1 = require("./createcustomer-routing.module");
var createcustomer_component_1 = require("./createcustomer.component");
var ngx_treeview_1 = require("ngx-treeview");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var CreateCustomerModule = /** @class */ (function () {
    function CreateCustomerModule() {
    }
    CreateCustomerModule = __decorate([
        core_1.NgModule({
            declarations: [
                createcustomer_component_1.CreateCustomerComponent
            ],
            imports: [
                common_1.CommonModule,
                createcustomer_routing_module_1.CreateCustomerRoutingModule,
                ng_multiselect_dropdown_1.NgMultiSelectDropDownModule.forRoot(),
                http_1.HttpClientModule,
                forms_1.FormsModule,
                ngx_treeview_1.TreeviewModule.forRoot(),
                http_2.HttpModule,
                ng_bootstrap_1.NgbModule
            ],
            providers: [http_service_1.HttpService, modelservice_1.ModalService, d3methodbinding_service_1.D3BindingsService],
            bootstrap: [createcustomer_component_1.CreateCustomerComponent],
            exports: []
        })
    ], CreateCustomerModule);
    return CreateCustomerModule;
}());
exports.CreateCustomerModule = CreateCustomerModule;
//# sourceMappingURL=createcustomer.module.js.map