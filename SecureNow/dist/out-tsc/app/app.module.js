"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/http");
var app_routing_module_1 = require("./app-routing.module");
var http_service_1 = require("../service/http.service");
var global_service_1 = require("../service/global.service");
var app_component_1 = require("./app.component");
var modelservice_1 = require("../service/modelservice");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var angular2_draggable_1 = require("angular2-draggable");
var animations_1 = require("@angular/platform-browser/animations");
var ngx_treeview_1 = require("ngx-treeview");
var d3methodbinding_service_1 = require("../service/d3methodbinding.service");
//import { tree } from 'd3-hierarchy';
require("nvd3");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                app_routing_module_1.AppRoutingModule,
                http_2.HttpModule,
                angular2_draggable_1.AngularDraggableModule,
                animations_1.BrowserAnimationsModule,
                ngx_treeview_1.TreeviewModule.forRoot(),
                ng_bootstrap_1.NgbModule
            ],
            providers: [http_service_1.HttpService, global_service_1.Global, modelservice_1.ModalService, d3methodbinding_service_1.D3BindingsService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map