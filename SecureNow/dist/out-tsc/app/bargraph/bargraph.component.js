"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var d3 = require("d3");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'BarGraph';
var MultiBarGraph = /** @class */ (function () {
    function MultiBarGraph() {
        this._graphData = {};
        this._graphDesiredData = [];
    }
    Object.defineProperty(MultiBarGraph.prototype, "containerElement", {
        set: function (containerElement) {
            //console.log(graphData);
            this._containerElement = containerElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiBarGraph.prototype, "graphData", {
        set: function (graphData) {
            //console.log(graphData);
            this._graphData = graphData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiBarGraph.prototype, "graphXAxis", {
        set: function (graphXAxis) {
            //console.log(graphXAxis);
            this._graphXAxis = graphXAxis;
        },
        enumerable: true,
        configurable: true
    });
    MultiBarGraph.prototype.DeStructureData = function () {
        //this._graphDesiredData = [];
        if (Object.keys(this._graphData).length > 0) {
            var keys = Object.keys(this._graphData);
            var dataObjKeys = Object.keys(this._graphData[keys[0]]);
            for (var i = 0; i < dataObjKeys.length; i++) {
                var tempObj = {};
                tempObj['key'] = dataObjKeys[i];
                tempObj['values'] = [];
                if (this._graphXAxis.length > 0) {
                    for (var j = 0; j < this._graphXAxis.length; j++) {
                        var innertempobj = {};
                        innertempobj['label'] = this._graphXAxis[j];
                        innertempobj['value'] = this._graphData[keys[j]][dataObjKeys[i]];
                        tempObj['values'].push(innertempobj);
                    }
                }
                this._graphDesiredData.push(tempObj);
            }
        }
        else {
        }
        this.drawGraph();
    };
    MultiBarGraph.prototype.drawGraph = function () {
        var graphData = Object.assign([], this._graphDesiredData);
        var _containerElement = this._containerElement;
        var height = 400;
        var margin = { top: 50, right: 80, bottom: 50, left: 80 };
        nv.addGraph(function () {
            var chart = nv.models.multiBarChart()
                .x(function (d) { return d.label; })
                .y(function (d) { return d.value; })
                .staggerLabels(false)
                .height(height)
                .margin(margin)
                .showControls(false);
            chart.yAxis
                .tickFormat(d3.format(',.2f'));
            d3.select('#' + _containerElement + ' #BarChart svg')
                .datum(graphData)
                .transition().duration(10000).delay(100).ease('elastic')
                .call(chart);
            nv.utils.windowResize(chart.update);
            chart.dispatch.on('renderEnd', function () {
                /******** shadow effect ********/
                /* var defs = d3.select('#BarChart svg').append("defs");
 
                 // create filter with id #drop-shadow
                 // height=130% so that the shadow is not clipped
                 var filter = defs.append("filter")
                     .attr("id", "drop-shadow")
                     .attr("width","100%")
 
                 filter.append("feGaussianBlur")
                     .attr("in", "SourceAlpha")
                     .attr("stdDeviation", 1)
                     .attr("result", "blur");
 
                 filter.append("feOffset")
                     .attr("in", "blur")
                     .attr("dx", -5)
                     .attr("dy", 0)
                     .attr("flood-color", "#ff0000")
                     .attr("flood-opacity","1")
                     .attr("result", "offsetBlur");
 
                 var feMerge = filter.append("feMerge");
 
                 feMerge.append("feMergeNode")
                     .attr("in", "offsetBlur")
                 feMerge.append("feMergeNode")
                     .attr("in", "SourceGraphic");*/
                var xAxis = d3.select('#BarChart svg .nv-x');
                xAxis.attr('transform', 'translate(0,' + (height - (margin.top + margin.bottom) + 20) + ')');
                d3.selectAll('.nv-multibar .nv-group').each(function (group) {
                    var g = d3.select('#BarChart svg .nv-groups');
                    // Remove previous labels if there is any
                    g.selectAll('text').remove();
                    g.selectAll('.nv-bar').each(function (bar) {
                        var b = d3.select(this);
                        var barWidth = b.attr('width');
                        var barHeight = b.attr('height');
                        b.style("stroke", "#348fff");
                        b.style("fill-opacity", "1");
                        g.append('text')
                            // Transforms shift the origin point then the x and y of the bar
                            // is altered by this transform. In order to align the labels
                            // we need to apply this transform to those.
                            .attr('transform', b.attr('transform'))
                            .text(function () {
                            // Two decimals format
                            return parseFloat(bar.value).toFixed(2);
                        })
                            .attr('fill', function () {
                            if (bar.value < 0) {
                                return "#000";
                            }
                            else {
                                return "#000";
                            }
                        })
                            .attr('y', function () {
                            // Center label vertically
                            if (bar.value < 0) {
                                var height = b.attr('height');
                                return (parseFloat(height) + (parseFloat(b.attr('y')) + 15));
                            }
                            else {
                                return parseFloat(b.attr('y')) - 10; // 10 is the label's magin from the bar
                            }
                        })
                            .attr('x', function () {
                            // Center label horizontally
                            var width = this.getBBox().width;
                            return parseFloat(b.attr('x')) + (parseFloat(barWidth) / 2) - (width / 2);
                        })
                            .style('font-weight', 500);
                    });
                });
            });
            return chart;
        });
    };
    MultiBarGraph.prototype.ngOnInit = function () {
        this.DeStructureData();
        //console.log(this._graphData);
    };
    MultiBarGraph.prototype.ngOnDestroy = function () {
        console.log('destroying component..');
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MultiBarGraph.prototype, "containerElement", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MultiBarGraph.prototype, "graphData", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MultiBarGraph.prototype, "graphXAxis", null);
    MultiBarGraph = __decorate([
        core_1.Component({
            selector: 'MultiBarGraph',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [])
    ], MultiBarGraph);
    return MultiBarGraph;
}());
exports.MultiBarGraph = MultiBarGraph;
//# sourceMappingURL=bargraph.component.js.map