"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var global_service_1 = require("../../service/global.service");
var ad_service_1 = require("./ad.service");
var c3 = require("c3");
require("../../../customChart-js/liquidGauge.js");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../SecureNow/Views');
var _templateURL = 'MyDashboard';
var MyDashboardComponent = /** @class */ (function () {
    function MyDashboardComponent(httpService, router, adService, globalVar) {
        this.httpService = httpService;
        this.router = router;
        this.adService = adService;
        this.globalVar = globalVar;
        this.SalesHorizontalList = [];
        this.empDeatilsHistry = [];
        this.tabularList = [];
        this.isSpinner = true;
        this.period = "";
        this.quarter = "";
        this.ytdMonth = "";
        this.ftmMonth = "";
        this.month = "";
        this.fullName = "";
        this.chartDy = "";
        this.radioSelected = "";
        this.empList = {};
        this.adpId = "";
        this.role = "";
        this.keys = [];
        this.graphData = [];
        this.filterData = {};
    }
    MyDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.goGetUserDetails();
        setTimeout(function () {
            _this.goGetUserDashboardData(_this.radioSelected, _this.adpId);
        }, 2000);
    };
    MyDashboardComponent.prototype.goGetUserDetails = function () {
        var _this = this;
        this.httpService.postRequest({ country: "" }, '../Account/GetCurrentLoginUserDetails', this.isSpinner).subscribe(function (data) {
            _this.fullName = data.rsBody.data.fullName;
            _this.empDeatilsHistry = data.rsBody.empList;
            _this.radioSelected = _this.empDeatilsHistry[0].role;
            _this.adpId = _this.empDeatilsHistry[0].adpId;
        });
    };
    MyDashboardComponent.prototype.goGetUserDashboardData = function (role, adpId) {
        var _this = this;
        this.filterData.period = this.period;
        this.filterData.month = this.month;
        this.filterData.role = role;
        this.filterData.adpId = adpId;
        var that = this;
        this.httpService.postRequest(this.filterData, '../MyDashboard/GetDashboardData', this.isSpinner).subscribe(function (data) {
            _this.SalesHorizontalList = data.rsBody.finalData;
            _this.tabularList = _this.SalesHorizontalList.tabularData;
            if (_this.SalesHorizontalList.employee.length > 4) {
                $("#six").css('display', 'block');
                $("#four").css('display', 'none');
                var barChart = c3.generate({
                    bindto: '#barChartSix',
                    data: {
                        columns: [
                            _this.SalesHorizontalList.target,
                            _this.SalesHorizontalList.actual
                        ],
                        onclick: function (d, i) {
                            this.indexData = d.index;
                            that.breadCrums = that.breadCrums + ":" + that.SalesHorizontalList.employeeDataList[this.indexData].adpId;
                            that.goGetUserDashboardData(that.SalesHorizontalList.employeeDataList[this.indexData].role, that.SalesHorizontalList.employeeDataList[this.indexData].adpId);
                        },
                        /*onclick: function (event, d) {
                            this.indexData = d.index;
                          //  this.clickhandler(event, this.indexData)
                        },*/
                        type: 'bar',
                        labels: true,
                        colors: {
                            Target: '#006294',
                            Actual: '#ed5a24'
                        }
                    },
                    bar: {
                        width: {
                            ratio: 0.5 // this makes bar width 50% of length between ticks
                        }
                    },
                    grid: {
                        x: {
                            show: true
                        },
                        y: {
                            show: true
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: _this.SalesHorizontalList.employee
                        }
                    },
                });
                setTimeout(function () {
                    for (var i = 0; i < _this.SalesHorizontalList.achievement.length; i++) {
                        var gaugeChart = c3.generate({
                            bindto: '#gaugeChartSix' + i,
                            data: {
                                columns: [
                                    [_this.SalesHorizontalList.employee[i], _this.SalesHorizontalList.achievement[i]]
                                ],
                                type: 'gauge',
                                colors: {
                                    Target: '#006294'
                                }
                            },
                            gauge: {
                                label: {
                                    format: function (value, ratio) {
                                        return value; //returning here the value and not the ratio
                                    },
                                },
                                min: 50,
                                max: 100,
                                units: '%' //this is only the text for the label
                            }
                        });
                    }
                }, 100);
            }
            else {
                $("#six").css('display', 'none');
                $("#four").css('display', 'block');
                var barChart = c3.generate({
                    bindto: '#barChartFour',
                    data: {
                        columns: [
                            _this.SalesHorizontalList.target,
                            _this.SalesHorizontalList.actual
                        ],
                        onclick: function (d, i) {
                            this.indexData = d.index;
                            that.breadCrums = that.breadCrums + ":" + that.SalesHorizontalList.employeeDataList[this.indexData].adpId;
                            that.goGetUserDashboardData(that.SalesHorizontalList.employeeDataList[this.indexData].role, that.SalesHorizontalList.employeeDataList[this.indexData].adpId);
                        },
                        /*onclick: function (event, d) {
                            this.indexData = d.index;
                          //  this.clickhandler(event, this.indexData)
                        },*/
                        type: 'bar',
                        labels: true,
                        colors: {
                            Target: '#006294',
                            Actual: '#ed5a24'
                        }
                    },
                    bar: {
                        width: {
                            ratio: 0.5 // this makes bar width 50% of length between ticks
                        }
                    },
                    grid: {
                        x: {
                            show: true
                        },
                        y: {
                            show: true
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: _this.SalesHorizontalList.employee
                        }
                    },
                });
                setTimeout(function () {
                    for (var i = 0; i < _this.SalesHorizontalList.achievement.length; i++) {
                        var gaugeChart = c3.generate({
                            bindto: '#gaugeChartFour' + i,
                            data: {
                                columns: [
                                    [_this.SalesHorizontalList.employee[i], _this.SalesHorizontalList.achievement[i]]
                                ],
                                type: 'gauge',
                                colors: {
                                    Target: '#006294'
                                }
                            },
                            gauge: {
                                label: {
                                    format: function (value, ratio) {
                                        return value; //returning here the value and not the ratio
                                    },
                                },
                                min: 50,
                                max: 100,
                                units: '%' //this is only the text for the label
                            }
                        });
                    }
                }, 100);
            }
            if (_this.breadCrums == null || _this.breadCrums == "") {
                _this.breadCrums = _this.SalesHorizontalList.Param;
            }
            _this.radioSelected = data.rsBody.finalData.empParam.role;
            var lineChart = c3.generate({
                bindto: '#lineChart',
                data: {
                    columns: [
                        _this.SalesHorizontalList.actual
                    ],
                    colors: {
                        Sales: '#970747'
                    }
                },
                grid: {
                    x: {
                        show: true
                    },
                    y: {
                        show: true
                    }
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: _this.SalesHorizontalList.employee
                    }
                }
            });
        });
    };
    MyDashboardComponent.prototype.empHistrySubmit = function (adpId, role) {
        this.goGetUserDashboardData(role, adpId);
    };
    //for ytd data                           
    MyDashboardComponent.prototype.YTDData = function () {
        this.period = "YTD";
        this.month = this.ytdMonth;
        this.ngOnInit();
    };
    //for selected  quarter
    MyDashboardComponent.prototype.QtrData = function () {
        this.period = "QTD";
        this.month = this.quarter;
        this.ngOnInit();
    };
    //for ftm
    MyDashboardComponent.prototype.FTMData = function () {
        this.period = "FTM";
        this.month = this.ftmMonth;
        this.ngOnInit();
    };
    MyDashboardComponent.prototype.goGetAllData = function () {
        var _this = this;
        this.httpService.postRequest({ country: "" }, '../MyDashboard/GetDataBasedOnFilters', this.isSpinner).subscribe(function (data) {
            _this.SalesHorizontalList = data.rsBody;
        });
    };
    MyDashboardComponent.prototype.ngAfterViewInit = function () {
    };
    MyDashboardComponent.prototype.ngOnDestroy = function () {
    };
    MyDashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            ad_service_1.AdService,
            global_service_1.Global])
    ], MyDashboardComponent);
    return MyDashboardComponent;
}());
exports.MyDashboardComponent = MyDashboardComponent;
//# sourceMappingURL=mydashboard.component.js.map