"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_service_1 = require("../../service/http.service");
var path = require('path');
var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewActualApprovalList';
var ApproveActualUploadComponent = /** @class */ (function () {
    function ApproveActualUploadComponent(httpService, router, route) {
        this.httpService = httpService;
        this.router = router;
        this.route = route;
        this.dashboardData = {};
        //public keyValueData: Object = {};
        this.keyValueData = {};
        this.shipToRequire = {};
        this.customerDeatils = {};
        this.custDetials = {};
        this.customerTargets = [];
        this.customerRelationship = {};
        this.responseData = [];
        this.getPreviousCustomerList = {};
        this.responseStateData = [];
        this.responseRelationData = {};
        this.yearData = [];
        this.yearRecorded = [];
        this.dataTypeList = [];
        this.BUData = [];
        this.responseChannelData = [];
        this.monthData = [];
        this.regionData = {};
        this.actualData = {};
        this.searchByBranch = {};
        this.customerDetails = {};
        this.customerBaseDetails = [];
        this.soldToPartyCodes = [];
        this.targetList = [];
        this.targetData = [];
        this.secondaryTargetList = {};
        this.sucessResponseData = {};
        this.getList = {};
        this.subChannelList = [];
        this.regionList = {};
        this.branchListData = {};
        this.subAreaData = [];
        this.responseAreaData = [];
        this.employeeKAMData = {};
        this.employeeData = [];
        this.getEmployeeList = {};
        this.shipToPartyCodes = {};
        this.lengthdata = {};
        this.customerBaseDetailsFK = {};
        this.deleteRel = {};
        this.sucessData = {};
        this.inputData = {};
        this.gradeData = {};
        this.doaStr = {};
        this.success = '';
        this.date = '';
        this.employeeActual = {};
        this.searchByName = {};
    }
    ApproveActualUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.custDetials = {
            customerDeatils: {},
            customerRelationship: {},
            customerTargets: []
        };
        this.doaStr =
            {
                year: null,
                month: null,
                day: null
            };
        $("#submit-basic-button").css('display', 'none');
        $(".h3-edit").css('display', 'block');
        $(".h3-create").css('display', 'none');
        $(".viewCustTar").css('display', 'block');
        $(".addNewCustTar").css('display', 'block');
        $("#update-basic-button").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        this.getNextCustomerList = 0;
        this.httpService.postRequest({}, '../ManageUploadData/GetActualRequestList', true).subscribe(function (datas) {
            if (datas.rsBody.showButton == true) {
                setTimeout(function () {
                    for (var i = 0; i < _this.responseData.length; i++) {
                        $("#showButtonTD" + i).css('display', 'block');
                    }
                }, 1000);
                $(".showButtonTH").css('display', 'block');
            }
            else {
                $(".showButtonTH").css('display', 'none');
                setTimeout(function () {
                    for (var j = 0; j < _this.responseData.length; j++) {
                        $("#showButtonTD" + j).css('display', 'none');
                    }
                }, 100);
            }
            if (datas.rsBody.data.length > 0) {
                _this.responseData = datas.rsBody.data;
                $("#row_not_found_id").css('display', 'none');
            }
            else {
                $("#row_not_found_id").css('display', 'block');
            }
            var _loop_1 = function (i) {
                if (_this.responseData[i].status != "WAITING FOR APPROVAL") {
                    setTimeout(function () {
                        $("#showButtonTD" + i).css('display', 'none');
                    }, 1000);
                }
            };
            for (var i = 0; i < _this.responseData.length; i++) {
                _loop_1(i);
            }
        });
    };
    ApproveActualUploadComponent.prototype.approveActualRequest = function (index) {
        var _this = this;
        this.employeeData = this.responseData[index];
        this.httpService.postRequest(this.employeeData, '../ManageUploadData/ApproveActualRequest', true).subscribe(function (datas) {
            _this.ngOnInit();
        });
    };
    ApproveActualUploadComponent.prototype.rejectActualRequest = function (index) {
        var _this = this;
        this.employeeData = this.responseData[index];
        this.httpService.postRequest(this.employeeData, '../ManageUploadData/RejectActualRequest', true).subscribe(function (datas) {
            _this.ngOnInit();
        });
    };
    ApproveActualUploadComponent.prototype.searchData = function () {
        this.httpService.postRequest(this.searchByName, '../ManageUploadData/GetActualRequestList', true).subscribe(function (datas) {
        });
    };
    ApproveActualUploadComponent.prototype.ngAfterViewInit = function () {
    };
    ApproveActualUploadComponent.prototype.ngOnDestroy = function () {
    };
    ApproveActualUploadComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: _templateURL
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ApproveActualUploadComponent);
    return ApproveActualUploadComponent;
}());
exports.ApproveActualUploadComponent = ApproveActualUploadComponent;
//# sourceMappingURL=approveactualupload.component.js.map