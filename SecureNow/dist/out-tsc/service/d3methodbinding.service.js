"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var d3 = require("d3");
var core_1 = require("@angular/core");
var D3BindingsService = /** @class */ (function () {
    function D3BindingsService() {
        this.drawable = [];
    }
    D3BindingsService.prototype.initDraw = function (initializerObj, scope) {
        this.scope = scope;
        for (var i = 0; i < initializerObj.length; i++) {
            this.drawable['path'] = initializerObj[i].path;
            this.drawable['class'] = initializerObj[i].style;
            this.drawable['id'] = initializerObj[i].id;
            this.drawable['title'] = initializerObj[i].title;
            this.drawable['status'] = initializerObj[i].status;
            this.drawable['progress'] = initializerObj[i].performance;
            this.drawable['fillColor'] = initializerObj[i].colorCode;
            //this.angularScope = this.scopeObj;
            this.drawPerformanceArcs(this.drawable);
        }
    };
    D3BindingsService.prototype.drawPerformanceArcs = function (drawable) {
        var svgElem = d3.select("svg");
        var gelem = svgElem.append("g");
        //gelem.append("title").attr("id", "title_" + this.id).text(this.title);
        var pathelem = gelem.append("path").attr("id", drawable.id).attr("class", drawable.class).attr("d", drawable.path).attr("title", drawable.title).attr("fill", drawable.fillColor);
        this.bindMouseMoveIn(pathelem, drawable.title);
        this.bindMouseClick(pathelem, drawable.title);
        this.bindMouseMoveOut(pathelem, drawable.title);
    };
    D3BindingsService.prototype.bindMouseMoveIn = function (elem, title) {
        var sample = elem;
        var text = title;
        elem.on('mousemove', function () {
            var tooltip = document.getElementById("tooltip");
            var content = $('#tooltip .tooltip_content');
            var flag = $('#tooltip .flag-icon');
            flag.removeClass();
            content[0].innerHTML = text;
            var flagclass = 'flag-icon flag-icon-' + this.id.toLowerCase();
            flag.addClass(flagclass);
            tooltip.style.display = "block";
            //tooltip.style.left = (event.pageX - 450) + 'px';
            //tooltip.style.top = (event.pageY - 240) + 'px';
        });
    };
    D3BindingsService.prototype.bindMouseClick = function (elem, title) {
        var sample = elem;
        var name = title;
        elem.on('click', function () {
            return title;
        });
    };
    D3BindingsService.prototype.bindMouseMoveOut = function (elem, title) {
        var sample = elem;
        var text = title;
        elem.on('mouseout', function (event) {
            var tooltip = document.getElementById("tooltip");
            var content = $('#tooltip .tooltip_content');
            var flag = $('#tooltip .flag-icon');
            flag.removeClass();
            content[0].innerHTML = text;
            var flagclass = 'flag-icon flag-icon-' + this.id.toLowerCase();
            flag.addClass(flagclass);
            tooltip.style.display = "none";
            //tooltip.style.left = (event.pageX - 450) + 'px';
            //tooltip.style.top = (event.pageY - 240) + 'px';
        });
    };
    D3BindingsService = __decorate([
        core_1.Injectable()
    ], D3BindingsService);
    return D3BindingsService;
}());
exports.D3BindingsService = D3BindingsService;
//# sourceMappingURL=d3methodbinding.service.js.map