"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dropdown_directive_1 = require("./dropdown.directive");
var DropdownMenuDirective = /** @class */ (function () {
    function DropdownMenuDirective(dropdown) {
        this.dropdown = dropdown;
    }
    DropdownMenuDirective = __decorate([
        core_1.Directive({
            selector: '[ngxDropdownMenu]',
            // tslint:disable-next-line:use-host-property-decorator
            host: {
                '[class.dropdown-menu]': 'true',
                '[class.show]': 'dropdown.isOpen'
            }
        }),
        __metadata("design:paramtypes", [dropdown_directive_1.DropdownDirective])
    ], DropdownMenuDirective);
    return DropdownMenuDirective;
}());
exports.DropdownMenuDirective = DropdownMenuDirective;
//# sourceMappingURL=dropdown-menu.directive.js.map