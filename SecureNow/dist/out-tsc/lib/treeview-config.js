"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TreeviewConfig = /** @class */ (function () {
    function TreeviewConfig() {
        this.hasAllCheckBox = true;
        this.hasFilter = false;
        this.hasCollapseExpand = false;
        this.decoupleChildFromParent = false;
        this.maxHeight = 500;
    }
    TreeviewConfig_1 = TreeviewConfig;
    Object.defineProperty(TreeviewConfig.prototype, "hasDivider", {
        get: function () {
            return this.hasFilter || this.hasAllCheckBox || this.hasCollapseExpand;
        },
        enumerable: true,
        configurable: true
    });
    TreeviewConfig.create = function (fields) {
        var config = new TreeviewConfig_1();
        Object.assign(config, fields);
        return config;
    };
    var TreeviewConfig_1;
    TreeviewConfig = TreeviewConfig_1 = __decorate([
        core_1.Injectable()
    ], TreeviewConfig);
    return TreeviewConfig;
}());
exports.TreeviewConfig = TreeviewConfig;
//# sourceMappingURL=treeview-config.js.map