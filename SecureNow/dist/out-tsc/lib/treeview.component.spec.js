"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var treeview_component_1 = require("./treeview.component");
var treeview_item_component_1 = require("./treeview-item.component");
var treeview_config_1 = require("./treeview-config");
var treeview_i18n_1 = require("./treeview-i18n");
var treeview_event_parser_1 = require("./treeview-event-parser");
var treeview_item_1 = require("./treeview-item");
var testing_2 = require("../testing");
var fakeData = {
    config: undefined,
    items: undefined,
    selectedChange: function (data) { }
};
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        this.config = fakeData.config;
        this.items = fakeData.items;
        this.selectedChange = fakeData.selectedChange;
    }
    TestComponent = __decorate([
        core_1.Component({
            selector: 'ngx-test',
            template: ''
        })
    ], TestComponent);
    return TestComponent;
}());
var createTestComponent = function (html) {
    return testing_2.createGenericTestComponent(html, TestComponent);
};
function queryCheckboxAll(debugElement) {
    return debugElement.query(platform_browser_1.By.css('.treeview-header input[type=checkbox]'));
}
exports.queryCheckboxAll = queryCheckboxAll;
function queryFilterTextBox(debugElement) {
    return debugElement.query(platform_browser_1.By.css('.treeview-header input[type=text]'));
}
exports.queryFilterTextBox = queryFilterTextBox;
function queryCollapseExpandIcon(debugElement) {
    return debugElement.query(platform_browser_1.By.css('.treeview-header i'));
}
exports.queryCollapseExpandIcon = queryCollapseExpandIcon;
function queryDivider(debugElement) {
    return debugElement.query(platform_browser_1.By.css('.treeview-header .dropdown-divider'));
}
exports.queryDivider = queryDivider;
function queryItemCheckboxes(debugElement) {
    return debugElement.queryAll(platform_browser_1.By.css('.treeview-container input[type=checkbox]'));
}
exports.queryItemCheckboxes = queryItemCheckboxes;
function queryItemTexts(debugElement) {
    var treeviewLabels = debugElement.queryAll(platform_browser_1.By.css('.treeview-container .form-check-label'));
    return treeviewLabels.map(function (label) { return label.nativeElement.innerText.trim(); });
}
exports.queryItemTexts = queryItemTexts;
describe('TreeviewComponent', function () {
    var baseTemplate = '<ngx-treeview [items]="items" [config]="config" (selectedChange)="selectedChange($event)"></ngx-treeview>';
    var selectedChangeSpy;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                platform_browser_1.BrowserModule
            ],
            declarations: [
                TestComponent,
                treeview_component_1.TreeviewComponent,
                treeview_item_component_1.TreeviewItemComponent
            ],
            providers: [
                treeview_config_1.TreeviewConfig,
                { provide: treeview_i18n_1.TreeviewI18n, useClass: treeview_i18n_1.TreeviewI18nDefault },
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.DefaultTreeviewEventParser }
            ]
        });
        selectedChangeSpy = spyOn(fakeData, 'selectedChange');
    });
    it('should initialize with default config', function () {
        var defaultConfig = new treeview_config_1.TreeviewConfig();
        var component = testing_1.TestBed.createComponent(treeview_component_1.TreeviewComponent).componentInstance;
        testing_2.expect(component.config).toEqual(defaultConfig);
    });
    describe('raiseSelectedChange', function () {
        var spy;
        beforeEach(testing_1.fakeAsync(function () {
            var fixture = testing_1.TestBed.createComponent(treeview_component_1.TreeviewComponent);
            spy = spyOn(fixture.componentInstance.selectedChange, 'emit');
            fixture.componentInstance.raiseSelectedChange();
            fixture.detectChanges();
            testing_1.tick();
        }));
        it('should raise event selectedChange', function () {
            testing_2.expect(spy.calls.any()).toBeTruthy();
            var args = spy.calls.mostRecent().args;
            testing_2.expect(args[0]).toEqual([]);
        });
    });
    describe('no data binding', function () {
        var fixture;
        beforeEach(testing_1.fakeAsync(function () {
            fixture = createTestComponent('<ngx-treeview></ngx-treeview>');
            fixture.detectChanges();
            testing_1.tick();
        }));
        it('should display "No items found"', function () {
            testing_2.expect(fixture.nativeElement.textContent.trim()).toBe('No items found');
        });
    });
    describe('null data binding', function () {
        var fixture;
        beforeEach(testing_1.fakeAsync(function () {
            fakeData.items = null;
            fixture = createTestComponent('<ngx-treeview [items]="items"></ngx-treeview>');
            fixture.detectChanges();
            testing_1.tick();
        }));
        it('should display "No items found"', function () {
            testing_2.expect(fixture.nativeElement.textContent.trim()).toBe('No items found');
        });
    });
    describe('all items are collapsed but config has hasCollapseExpand=false', function () {
        var fixture;
        beforeEach(testing_1.fakeAsync(function () {
            fakeData.config = treeview_config_1.TreeviewConfig.create({
                hasCollapseExpand: true
            });
            fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1, collapsed: true })];
            fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
            fixture.detectChanges();
            testing_1.tick();
        }));
        it('should show icon on header with collapsed state', function () {
            var collapseExpandIcon = queryCollapseExpandIcon(fixture.debugElement);
            testing_2.expect(collapseExpandIcon.nativeElement).toHaveCssClass('fa-expand');
        });
    });
    describe('config', function () {
        describe('hasAllCheckBox', function () {
            beforeEach(function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
            });
            it('should display checkbox "All" if value is true', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasAllCheckBox: true
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var checkboxAll = queryCheckboxAll(fixture.debugElement);
                testing_2.expect(checkboxAll).not.toBeNull();
            });
            it('should not display checkbox "All" if value is false', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasAllCheckBox: false
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var checkboxAll = queryCheckboxAll(fixture.debugElement);
                testing_2.expect(checkboxAll).toBeNull();
            });
        });
        describe('hasFilter', function () {
            beforeEach(function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
            });
            it('should display checkbox Filter textbox if value is true', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasFilter: true
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var filterTextBox = queryFilterTextBox(fixture.debugElement);
                testing_2.expect(filterTextBox).not.toBeNull();
            });
            it('should not display checkbox Filter textbox if value is false', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasFilter: false
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var filterTextBox = queryFilterTextBox(fixture.debugElement);
                testing_2.expect(filterTextBox).toBeNull();
            });
        });
        describe('hasCollapseExpand', function () {
            beforeEach(function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
            });
            it('should display icon Collapse/Expand if value is true', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasCollapseExpand: true
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var collapseExpandIcon = queryCollapseExpandIcon(fixture.debugElement);
                testing_2.expect(collapseExpandIcon).not.toBeNull();
            });
            it('should not display icon Collapse/Expand if value is false', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasCollapseExpand: false
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var collapseExpandIcon = queryCollapseExpandIcon(fixture.debugElement);
                testing_2.expect(collapseExpandIcon).toBeNull();
            });
        });
        describe('decoupleChildFromParent with false value', function () {
            var fixture;
            var parentCheckbox;
            var childCheckbox;
            beforeEach(testing_1.fakeAsync(function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1, children: [{ text: '11', value: 11 }] })];
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasAllCheckBox: false,
                    hasCollapseExpand: false,
                    hasFilter: false,
                    decoupleChildFromParent: true
                });
                fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                testing_1.tick();
                var checkboxes = queryItemCheckboxes(fixture.debugElement);
                parentCheckbox = checkboxes[0];
                childCheckbox = checkboxes[1];
            }));
            it('should have checked state is true for child item', function () {
                testing_2.expect(childCheckbox.nativeElement.checked).toBeTruthy();
            });
            describe('uncheck parent', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    parentCheckbox.nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should not change checked state of child item', function () {
                    testing_2.expect(childCheckbox.nativeElement.checked).toBeTruthy();
                });
            });
        });
        describe('maxHeight', function () {
            it('should display style correct max-height value', function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    maxHeight: 400
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var container = fixture.debugElement.query(platform_browser_1.By.css('.treeview-container'));
                testing_2.expect(container.nativeElement).toHaveCssStyle({ 'max-height': '400px' });
            });
        });
        describe('divider', function () {
            beforeEach(function () {
                fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
            });
            it('should display divider with default config', function () {
                fakeData.config = new treeview_config_1.TreeviewConfig();
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var divider = queryDivider(fixture.debugElement);
                testing_2.expect(divider).not.toBeNull();
            });
            it('should not display divider when no filter, no All checkbox & no collapse/expand', function () {
                fakeData.config = treeview_config_1.TreeviewConfig.create({
                    hasAllCheckBox: false,
                    hasCollapseExpand: false,
                    hasFilter: false
                });
                var fixture = createTestComponent('<ngx-treeview [config]="config" [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                var divider = queryDivider(fixture.debugElement);
                testing_2.expect(divider).toBeNull();
            });
        });
    });
    describe('template', function () {
        var fixture;
        beforeEach(function () {
            fakeData.items = [new treeview_item_1.TreeviewItem({ text: '1', value: 1 })];
        });
        describe('default template', function () {
            beforeEach(testing_1.fakeAsync(function () {
                fixture = createTestComponent('<ngx-treeview [items]="items"></ngx-treeview>');
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should work', function () {
                var treeviewTemplate = fixture.debugElement.query(platform_browser_1.By.css('.form-check'));
                testing_2.expect(treeviewTemplate).not.toBeNull();
            });
        });
        describe('custom template', function () {
            beforeEach(testing_1.fakeAsync(function () {
                var htmlTemplate = "\n<ng-template #itemTemplate let-item=\"item\"\n    let-onCollapseExpand=\"onCollapseExpand\"\n    let-onCheckedChange=\"onCheckedChange\">\n    <div class=\"treeview-template\">{{item.text}}</div>\n</ng-template>\n<ngx-treeview [items]=\"items\" [itemTemplate]=\"itemTemplate\"></ngx-treeview>";
                fixture = createTestComponent(htmlTemplate);
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should work', function () {
                var treeviewTemplate = fixture.debugElement.query(platform_browser_1.By.css('.treeview-template'));
                testing_2.expect(treeviewTemplate.nativeElement).toHaveText('1');
            });
        });
    });
    describe('items', function () {
        var fixture;
        var allCheckBox;
        var itemCheckBoxes;
        var filterTextBox;
        beforeEach(function () {
            fakeData.config = treeview_config_1.TreeviewConfig.create({
                hasAllCheckBox: true,
                hasCollapseExpand: true,
                hasFilter: true,
                decoupleChildFromParent: false,
                maxHeight: 400
            });
            fakeData.items = [
                new treeview_item_1.TreeviewItem({
                    text: 'Item1',
                    value: 1,
                    children: [{
                            text: 'Item11',
                            value: 11,
                            children: [{
                                    text: 'Item111',
                                    value: 111
                                }, {
                                    text: 'Item112',
                                    value: 112
                                }]
                        }, {
                            text: 'Item12',
                            value: 12
                        }]
                }),
                new treeview_item_1.TreeviewItem({
                    text: 'Item2',
                    value: 2
                })
            ];
        });
        beforeEach(testing_1.fakeAsync(function () {
            selectedChangeSpy.calls.reset();
            fixture = createTestComponent(baseTemplate);
            fixture.detectChanges();
            testing_1.tick();
            allCheckBox = queryCheckboxAll(fixture.debugElement);
            itemCheckBoxes = queryItemCheckboxes(fixture.debugElement);
            filterTextBox = queryFilterTextBox(fixture.debugElement);
        }));
        it('should raise selectedChange when binding items', function () {
            testing_2.expect(selectedChangeSpy.calls.any()).toBeTruthy();
            var args = selectedChangeSpy.calls.mostRecent().args;
            testing_2.expect(args[0]).toEqual([111, 112, 12, 2]);
        });
        it('should have "All" checkbox is checked', function () {
            testing_2.expect(allCheckBox.nativeElement.checked).toBeTruthy();
        });
        it('should have all of items are checked', function () {
            var checkedItems = itemCheckBoxes.filter(function (item) { return item.nativeElement.checked; });
            testing_2.expect(checkedItems.length).toBe(itemCheckBoxes.length);
        });
        describe('uncheck "All"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                selectedChangeSpy.calls.reset();
                allCheckBox.nativeElement.click();
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should uncheck all of items', function () {
                var checkedItems = itemCheckBoxes.filter(function (item) { return item.nativeElement.checked; });
                testing_2.expect(checkedItems.length).toBe(0);
            });
            it('should raise event selectedChange', function () {
                testing_2.expect(selectedChangeSpy.calls.any()).toBeTruthy();
                var args = selectedChangeSpy.calls.mostRecent().args;
                testing_2.expect(args[0]).toEqual([]);
            });
        });
        describe('uncheck "Item1"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                selectedChangeSpy.calls.reset();
                itemCheckBoxes[0].nativeElement.click();
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should have "All" checkbox is unchecked', function () {
                testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
            });
            it('should raise event selectedChange', function () {
                testing_2.expect(selectedChangeSpy.calls.any()).toBeTruthy();
                var args = selectedChangeSpy.calls.mostRecent().args;
                testing_2.expect(args[0]).toEqual([2]);
            });
            describe('filtering "em2"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'em2');
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should not display item "Item1" & its children', function () {
                    var texts = queryItemTexts(fixture.debugElement);
                    testing_2.expect(texts).toEqual(['Item2']);
                });
                it('should have checkbox "All" is checked', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeTruthy();
                });
            });
            describe('filtering "em1"', function () {
                var itemCheckboxes;
                beforeEach(testing_1.fakeAsync(function () {
                    itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                    testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'em1');
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should not display item "Item2"', function () {
                    var texts = queryItemTexts(fixture.debugElement);
                    testing_2.expect(texts).toEqual(['Item1', 'Item11', 'Item111', 'Item112', 'Item12']);
                });
                it('should have checkbox "All" is unchecked', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                });
                it('should have "Item11" is unchecked', function () {
                    testing_2.expect(itemCheckBoxes[1].nativeElement.checked).toBeFalsy();
                });
                describe('check "Item11"', function () {
                    beforeEach(testing_1.fakeAsync(function () {
                        itemCheckBoxes[1].nativeElement.click();
                        fixture.detectChanges();
                        testing_1.tick();
                    }));
                    it('should have "Item1" is unchecked', function () {
                        testing_2.expect(itemCheckBoxes[0].nativeElement.checked).toBeFalsy();
                    });
                    it('should have checkbox "All" is unchecked', function () {
                        testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                    });
                });
            });
            describe('uncheck "Item2"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    selectedChangeSpy.calls.reset();
                    itemCheckBoxes[itemCheckBoxes.length - 1].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should keep "All" checkbox is unchecked', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                });
                it('should raise event selectedChange', function () {
                    testing_2.expect(selectedChangeSpy.calls.any()).toBeTruthy();
                    var args = selectedChangeSpy.calls.mostRecent().args;
                    testing_2.expect(args[0]).toEqual([]);
                });
            });
            describe('check "Item11"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    selectedChangeSpy.calls.reset();
                    itemCheckBoxes[1].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should have "All" checkbox is unchecked', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                });
                it('should have 4 items are checked', function () {
                    var checkedItems = itemCheckBoxes.filter(function (item) { return item.nativeElement.checked; });
                    testing_2.expect(checkedItems.length).toBe(4);
                });
                it('should raise event selectedChange', function () {
                    testing_2.expect(selectedChangeSpy.calls.any()).toBeTruthy();
                    var args = selectedChangeSpy.calls.mostRecent().args;
                    testing_2.expect(args[0]).toEqual([111, 112, 2]);
                });
            });
            describe('check "Item111"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    selectedChangeSpy.calls.reset();
                    itemCheckBoxes[2].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should have "All" checkbox is unchecked', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                });
                it('should have "Item1" is unchecked', function () {
                    testing_2.expect(itemCheckBoxes[0].nativeElement.checked).toBeFalsy();
                });
            });
        });
        describe('collapse/expand icon', function () {
            var collapseExpandIcon;
            beforeEach(function () {
                collapseExpandIcon = queryCollapseExpandIcon(fixture.debugElement);
            });
            it('should have element class "fa-compress"', function () {
                testing_2.expect(collapseExpandIcon.nativeElement).toHaveCssClass('fa-compress');
            });
            it('should display "Item1" & "Item2"', function () {
                var texts = queryItemTexts(fixture.debugElement);
                testing_2.expect(texts).toEqual(['Item1', 'Item11', 'Item111', 'Item112', 'Item12', 'Item2']);
            });
            describe('toggle', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    collapseExpandIcon.nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should have element class "fa-expand"', function () {
                    testing_2.expect(collapseExpandIcon.nativeElement).toHaveCssClass('fa-expand');
                });
                it('should display "Item1" & "Item2"', function () {
                    var texts = queryItemTexts(fixture.debugElement);
                    testing_2.expect(texts).toEqual(['Item1', 'Item2']);
                });
            });
        });
        describe('filtering "em1"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'em1');
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should not display item "Item2"', function () {
                var texts = queryItemTexts(fixture.debugElement);
                testing_2.expect(texts).toEqual(['Item1', 'Item11', 'Item111', 'Item112', 'Item12']);
            });
            it('should have checkbox "All" is checked', function () {
                testing_2.expect(allCheckBox.nativeElement.checked).toBeTruthy();
            });
            describe('uncheck "Item1"', function () {
                var itemCheckboxes;
                beforeEach(testing_1.fakeAsync(function () {
                    itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                    itemCheckboxes[0].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should change checked value of checkbox "All" to false', function () {
                    testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                });
                it('should have checked value of "Item1" && its children are false', function () {
                    itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                    var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                    testing_2.expect(checkedValues).toEqual([false, false, false, false, false]);
                });
                describe('clear filter', function () {
                    beforeEach(testing_1.fakeAsync(function () {
                        itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                        testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, '');
                        fixture.detectChanges();
                        testing_1.tick();
                    }));
                    it('should have checked of "Item1" is false', function () {
                        itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                        var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                        testing_2.expect(checkedValues).toEqual([false, false, false, false, false, true]);
                    });
                    it('should have checked value of "All" checkbox is false', function () {
                        testing_2.expect(allCheckBox.nativeElement.checked).toBeFalsy();
                    });
                    describe('check "All"', function () {
                        beforeEach(testing_1.fakeAsync(function () {
                            allCheckBox.nativeElement.click();
                            fixture.detectChanges();
                            testing_1.tick();
                        }));
                        it('should have checked value of "Item1" is true', function () {
                            itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                            var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                            testing_2.expect(checkedValues).toEqual([true, true, true, true, true, true]);
                        });
                    });
                });
            });
        });
        describe('filtering "em11"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'em11');
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should display only "Item1" & its children', function () {
                var texts = queryItemTexts(fixture.debugElement);
                testing_2.expect(texts).toEqual(['Item1', 'Item11', 'Item111', 'Item112']);
            });
            describe('uncheck "Item11', function () {
                var itemCheckboxes;
                beforeEach(testing_1.fakeAsync(function () {
                    itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                    itemCheckboxes[1].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should have "Item1" is unchecked', function () {
                    testing_2.expect(itemCheckboxes[0].nativeElement.checked).toBeFalsy();
                });
                describe('clear filter', function () {
                    beforeEach(testing_1.fakeAsync(function () {
                        testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, '');
                        fixture.detectChanges();
                        testing_1.tick();
                    }));
                    it('should have "Item12" & "Item2" are checked', function () {
                        itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                        var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                        testing_2.expect(checkedValues).toEqual([false, false, false, false, true, true]);
                    });
                });
                describe('check "Item11"', function () {
                    beforeEach(testing_1.fakeAsync(function () {
                        itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                        itemCheckboxes[1].nativeElement.click();
                        fixture.detectChanges();
                        testing_1.tick();
                    }));
                    it('should have "All" checkbox is checked', function () {
                        testing_2.expect(allCheckBox.nativeElement.checked).toBeTruthy();
                    });
                });
            });
            describe('uncheck "All"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    allCheckBox.nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should uncheck "Item11" & its children', function () {
                    var itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                    var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                    testing_2.expect(checkedValues).toEqual([false, false, false, false]);
                });
                describe('clear filter', function () {
                    beforeEach(testing_1.fakeAsync(function () {
                        testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, '');
                        fixture.detectChanges();
                        testing_1.tick();
                    }));
                    it('should have "Item12" & "Item2" are checked', function () {
                        var itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
                        var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                        testing_2.expect(checkedValues).toEqual([false, false, false, false, true, true]);
                    });
                });
            });
        });
        describe('filtering "Item111"', function () {
            var itemCheckboxes;
            beforeEach(testing_1.fakeAsync(function () {
                testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'Item111');
                fixture.detectChanges();
                testing_1.tick();
                itemCheckboxes = queryItemCheckboxes(fixture.debugElement);
            }));
            it('should display "Item1", "Item11" & "Item111"', function () {
                var texts = queryItemTexts(fixture.debugElement);
                testing_2.expect(texts).toEqual(['Item1', 'Item11', 'Item111']);
            });
            describe('uncheck "Item1"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    itemCheckboxes[0].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should have displayed items are unchecked', function () {
                    var checkedValues = itemCheckboxes.map(function (checkbox) { return checkbox.nativeElement.checked; });
                    testing_2.expect(checkedValues).toEqual([false, false, false]);
                });
            });
        });
        describe('filtering "fake"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                testing_2.eventHelper.raiseInput(filterTextBox.nativeElement, 'fake');
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should display filter', function () {
                var filterInput = fixture.debugElement.query(platform_browser_1.By.css('input[type="text"]'));
                testing_2.expect(filterInput).not.toBeNull();
            });
            it('should not display any items', function () {
                var texts = queryItemTexts(fixture.debugElement);
                testing_2.expect(texts).toEqual([]);
            });
            it('should display a text "No items found"', function () {
                var textElement = fixture.debugElement.query(platform_browser_1.By.css('.treeview-text'));
                testing_2.expect(textElement.nativeElement.textContent.trim()).toBe('No items found');
            });
        });
    });
});
//# sourceMappingURL=treeview.component.spec.js.map