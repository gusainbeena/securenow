"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var testing_2 = require("../testing");
var dropdown_directive_1 = require("./dropdown.directive");
var dropdown_toggle_directive_1 = require("./dropdown-toggle.directive");
var fakeData = {
    open: undefined,
    openChange: function (open) { }
};
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        this.open = fakeData.open;
        this.openChange = function (open) { return fakeData.openChange(open); };
    }
    TestComponent = __decorate([
        core_1.Component({
            selector: 'ngx-test',
            template: ''
        })
    ], TestComponent);
    return TestComponent;
}());
var createTestComponent = function (html) {
    return testing_2.createGenericTestComponent(html, TestComponent);
};
describe('DropdownToggleDirective', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                platform_browser_1.BrowserModule
            ],
            declarations: [
                TestComponent,
                dropdown_toggle_directive_1.DropdownToggleDirective,
                dropdown_directive_1.DropdownDirective
            ]
        });
    });
    it('should not have class "show" by default', function () {
        var fixture = createTestComponent('<div ngxDropdown></div>');
        fixture.detectChanges();
        var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
    });
    describe('open', function () {
        it('should not have class "show" if open is false', function () {
            fakeData.open = false;
            var fixture = createTestComponent('<div ngxDropdown [open]="open"></div>');
            fixture.detectChanges();
            var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
            testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
        });
        it('should have class "show" if open is true', function () {
            fakeData.open = true;
            var fixture = createTestComponent('<div ngxDropdown [open]="open"></div>');
            fixture.detectChanges();
            var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
            testing_2.expect(element.nativeElement).toHaveCssClass('show');
        });
    });
    it('should allow toggling dropdown from outside', function () {
        var template = "\n            <button (click)=\"drop.open(); $event.stopPropagation()\"></button>\n            <button (click)=\"drop.close(); $event.stopPropagation()\"></button>\n            <button (click)=\"drop.toggle(); $event.stopPropagation()\"></button>\n            <div ngxDropdown #drop=\"ngxDropdown\" (openChange)=\"openChange($event)\"></div>\n        ";
        var fixture = createTestComponent(template);
        var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
        var buttons = fixture.debugElement.queryAll(platform_browser_1.By.css('button'));
        var spy = spyOn(fakeData, 'openChange');
        spy.calls.reset();
        buttons[0].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).toHaveCssClass('show');
        testing_2.expect(spy.calls.any()).toBeTruthy();
        var args = spy.calls.mostRecent().args;
        testing_2.expect(args[0]).toBe(true);
        spy.calls.reset();
        buttons[0].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(spy.calls.any()).toBeFalsy();
        spy.calls.reset();
        buttons[1].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
        testing_2.expect(spy.calls.any()).toBeTruthy();
        args = spy.calls.mostRecent().args;
        testing_2.expect(args[0]).toBe(false);
        spy.calls.reset();
        buttons[1].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(spy.calls.any()).toBeFalsy();
        spy.calls.reset();
        buttons[2].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).toHaveCssClass('show');
        testing_2.expect(spy.calls.any()).toBeTruthy();
        args = spy.calls.mostRecent().args;
        testing_2.expect(args[0]).toBe(true);
        spy.calls.reset();
        buttons[2].nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
        testing_2.expect(spy.calls.any()).toBeTruthy();
        args = spy.calls.mostRecent().args;
        testing_2.expect(args[0]).toBe(false);
    });
    it('should close when press Esc', function () {
        fakeData.open = true;
        var fixture = createTestComponent('<div ngxDropdown [open]="open"></div>');
        fixture.detectChanges();
        var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
        element.triggerEventHandler('keyup.esc', {});
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
    });
    it('should close if click outside', function () {
        fakeData.open = true;
        var fixture = createTestComponent('<button>Outside<button><div ngxDropdown [open]="open"></div>');
        fixture.detectChanges();
        var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
        var button = fixture.debugElement.query(platform_browser_1.By.css('button'));
        button.nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
    });
    it('should not close if click on toggle element', function () {
        var template = "\n            <div ngxDropdown [open]=\"open\">\n                <button ngxDropdownToggle>Button</button>\n                <p>Panel</p>\n            </div>\n        ";
        fakeData.open = true;
        var fixture = createTestComponent(template);
        fixture.detectChanges();
        var element = fixture.debugElement.query(platform_browser_1.By.directive(dropdown_directive_1.DropdownDirective));
        var button = fixture.debugElement.query(platform_browser_1.By.css('button'));
        button.nativeElement.click();
        fixture.detectChanges();
        testing_2.expect(element.nativeElement).not.toHaveCssClass('show');
    });
});
//# sourceMappingURL=dropdown.directive.spec.js.map