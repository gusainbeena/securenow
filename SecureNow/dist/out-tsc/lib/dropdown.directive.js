"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lodash_1 = require("lodash");
var DropdownDirective = /** @class */ (function () {
    function DropdownDirective() {
        // tslint:disable-next-line:no-input-rename
        this.internalOpen = false;
        this.openChange = new core_1.EventEmitter();
    }
    Object.defineProperty(DropdownDirective.prototype, "isOpen", {
        get: function () {
            return this.internalOpen;
        },
        enumerable: true,
        configurable: true
    });
    DropdownDirective.prototype.onKeyupEsc = function () {
        this.close();
    };
    DropdownDirective.prototype.onDocumentClick = function (event) {
        if (event.button !== 2 && !this.isEventFromToggle(event)) {
            this.close();
        }
    };
    DropdownDirective.prototype.open = function () {
        if (!this.internalOpen) {
            this.internalOpen = true;
            this.openChange.emit(true);
        }
    };
    DropdownDirective.prototype.close = function () {
        if (this.internalOpen) {
            this.internalOpen = false;
            this.openChange.emit(false);
        }
    };
    DropdownDirective.prototype.toggle = function () {
        if (this.isOpen) {
            this.close();
        }
        else {
            this.open();
        }
    };
    DropdownDirective.prototype.isEventFromToggle = function (event) {
        return !lodash_1.isNil(this.toggleElement) && this.toggleElement.contains(event.target);
    };
    __decorate([
        core_1.Input('open'),
        __metadata("design:type", Object)
    ], DropdownDirective.prototype, "internalOpen", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], DropdownDirective.prototype, "openChange", void 0);
    __decorate([
        core_1.HostBinding('class.show'),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], DropdownDirective.prototype, "isOpen", null);
    __decorate([
        core_1.HostListener('keyup.esc'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], DropdownDirective.prototype, "onKeyupEsc", null);
    __decorate([
        core_1.HostListener('document:click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [MouseEvent]),
        __metadata("design:returntype", void 0)
    ], DropdownDirective.prototype, "onDocumentClick", null);
    DropdownDirective = __decorate([
        core_1.Directive({
            selector: '[ngxDropdown]',
            exportAs: 'ngxDropdown'
        })
    ], DropdownDirective);
    return DropdownDirective;
}());
exports.DropdownDirective = DropdownDirective;
//# sourceMappingURL=dropdown.directive.js.map