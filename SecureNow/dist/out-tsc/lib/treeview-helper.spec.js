"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
var treeview_item_1 = require("./treeview-item");
var treeview_helper_1 = require("./treeview-helper");
var rootNoChildren = new treeview_item_1.TreeviewItem({
    text: '1',
    value: 1
});
var rootHasChildren = new treeview_item_1.TreeviewItem({
    text: '1',
    value: 1,
    checked: false,
    children: [
        {
            text: '2',
            value: 2,
            checked: true
        },
        {
            text: '3',
            value: 3,
            checked: false,
            children: [{
                    text: '4',
                    value: 4,
                    checked: false
                }]
        }
    ]
});
var fakeItem = new treeview_item_1.TreeviewItem({
    text: '1',
    value: 1
});
describe('findItem', function () {
    it('should not find item if root is null or undefined', function () {
        expect(treeview_helper_1.TreeviewHelper.findItem(undefined, 1)).toBeUndefined();
        expect(treeview_helper_1.TreeviewHelper.findItem(null, 1)).toBeUndefined();
    });
    it('should find item', function () {
        expect(treeview_helper_1.TreeviewHelper.findItem(rootNoChildren, 1)).toEqual(rootNoChildren);
        expect(treeview_helper_1.TreeviewHelper.findItem(rootHasChildren, 2)).toEqual(rootHasChildren.children[0]);
    });
    it('should not find item', function () {
        expect(treeview_helper_1.TreeviewHelper.findItem(rootNoChildren, 2)).toBeUndefined();
        expect(treeview_helper_1.TreeviewHelper.findItem(rootHasChildren, 0)).toBeUndefined();
    });
});
describe('findItemInList', function () {
    it('should not find item if list is null or undefined', function () {
        expect(treeview_helper_1.TreeviewHelper.findItemInList(undefined, 1)).toBeUndefined();
        expect(treeview_helper_1.TreeviewHelper.findItemInList(null, 1)).toBeUndefined();
    });
    it('should find item', function () {
        var list = [rootNoChildren, rootHasChildren];
        expect(treeview_helper_1.TreeviewHelper.findItemInList(list, 2)).toEqual(rootHasChildren.children[0]);
    });
    it('should not find item', function () {
        var list = [rootNoChildren, rootHasChildren];
        expect(treeview_helper_1.TreeviewHelper.findItemInList(list, 0)).toBeUndefined();
    });
});
describe('findParent', function () {
    it('should not find parent if root is undefined or root has no children', function () {
        expect(treeview_helper_1.TreeviewHelper.findParent(undefined, fakeItem)).toBeUndefined();
        expect(treeview_helper_1.TreeviewHelper.findParent(rootNoChildren, fakeItem)).toBeUndefined();
    });
    it('should not find parent', function () {
        expect(treeview_helper_1.TreeviewHelper.findParent(rootHasChildren, fakeItem)).toBeUndefined();
    });
    it('should find parent', function () {
        var parent = rootHasChildren;
        var item = rootHasChildren.children[0];
        expect(treeview_helper_1.TreeviewHelper.findParent(rootHasChildren, item)).toBe(parent);
        parent = rootHasChildren.children[1];
        item = rootHasChildren.children[1].children[0];
        expect(treeview_helper_1.TreeviewHelper.findParent(rootHasChildren, item)).toBe(parent);
    });
});
describe('removeParent', function () {
    it('should not remove item if root is undefined or root has no children', function () {
        expect(treeview_helper_1.TreeviewHelper.removeItem(undefined, fakeItem)).toBeFalsy();
        expect(treeview_helper_1.TreeviewHelper.removeItem(rootNoChildren, fakeItem)).toBeFalsy();
    });
    it('should not remove item', function () {
        expect(treeview_helper_1.TreeviewHelper.removeItem(rootHasChildren, fakeItem)).toBeFalsy();
    });
    it('should remove item & has correct checked', function () {
        var root = lodash_1.cloneDeep(rootHasChildren);
        var parent = root;
        var item = root.children[1];
        expect(parent.children.length).toBe(2);
        expect(parent.checked).toBe(undefined);
        expect(treeview_helper_1.TreeviewHelper.removeItem(parent, item)).toBe(true);
        expect(parent.children.length).toBe(1);
        expect(parent.checked).toBe(true);
        item = root.children[0];
        expect(treeview_helper_1.TreeviewHelper.removeItem(parent, item)).toBe(true);
        expect(parent.children).toBeUndefined();
    });
});
//# sourceMappingURL=treeview-helper.spec.js.map