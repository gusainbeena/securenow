"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var treeview_i18n_1 = require("./treeview-i18n");
var treeview_config_1 = require("./treeview-config");
var treeview_component_1 = require("./treeview.component");
var dropdown_directive_1 = require("./dropdown.directive");
var DropdownTreeviewComponent = /** @class */ (function () {
    function DropdownTreeviewComponent(i18n, defaultConfig) {
        this.i18n = i18n;
        this.defaultConfig = defaultConfig;
        this.buttonClass = 'btn-outline-secondary';
        this.selectedChange = new core_1.EventEmitter(true);
        this.filterChange = new core_1.EventEmitter();
        this.config = this.defaultConfig;
    }
    DropdownTreeviewComponent.prototype.getText = function () {
        return this.i18n.getText(this.treeviewComponent.selection);
    };
    DropdownTreeviewComponent.prototype.onSelectedChange = function (values) {
        this.selectedChange.emit(values);
    };
    DropdownTreeviewComponent.prototype.onFilterChange = function (text) {
        this.filterChange.emit(text);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], DropdownTreeviewComponent.prototype, "buttonClass", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], DropdownTreeviewComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], DropdownTreeviewComponent.prototype, "itemTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], DropdownTreeviewComponent.prototype, "items", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", treeview_config_1.TreeviewConfig)
    ], DropdownTreeviewComponent.prototype, "config", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], DropdownTreeviewComponent.prototype, "selectedChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], DropdownTreeviewComponent.prototype, "filterChange", void 0);
    __decorate([
        core_1.ViewChild(treeview_component_1.TreeviewComponent),
        __metadata("design:type", treeview_component_1.TreeviewComponent)
    ], DropdownTreeviewComponent.prototype, "treeviewComponent", void 0);
    __decorate([
        core_1.ViewChild(dropdown_directive_1.DropdownDirective),
        __metadata("design:type", dropdown_directive_1.DropdownDirective)
    ], DropdownTreeviewComponent.prototype, "dropdownDirective", void 0);
    DropdownTreeviewComponent = __decorate([
        core_1.Component({
            selector: 'ngx-dropdown-treeview',
            templateUrl: './dropdown-treeview.component.html',
            styleUrls: ['./dropdown-treeview.component.scss']
        }),
        __metadata("design:paramtypes", [treeview_i18n_1.TreeviewI18n,
            treeview_config_1.TreeviewConfig])
    ], DropdownTreeviewComponent);
    return DropdownTreeviewComponent;
}());
exports.DropdownTreeviewComponent = DropdownTreeviewComponent;
//# sourceMappingURL=dropdown-treeview.component.js.map