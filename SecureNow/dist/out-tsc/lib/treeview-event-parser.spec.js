"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var treeview_item_1 = require("./treeview-item");
var treeview_component_1 = require("./treeview.component");
var treeview_module_1 = require("./treeview.module");
var treeview_event_parser_1 = require("./treeview-event-parser");
var selectionWithUndefinedCheckedItems = {
    checkedItems: undefined,
    uncheckedItems: undefined
};
var selectionWithNullCheckedItems = {
    checkedItems: null,
    uncheckedItems: undefined
};
describe('DefaultTreeviewEventParser', function () {
    var parser;
    var fakeComponent;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                treeview_module_1.TreeviewModule.forRoot()
            ],
            providers: [
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.DefaultTreeviewEventParser }
            ]
        });
        parser = testing_1.TestBed.get(treeview_event_parser_1.TreeviewEventParser);
        fakeComponent = testing_1.TestBed.createComponent(treeview_component_1.TreeviewComponent).componentInstance;
    });
    it('should return empty list if checkedItems is null or undefined', function () {
        fakeComponent.items = undefined;
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        var result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
        fakeComponent.items = undefined;
        fakeComponent.selection = selectionWithNullCheckedItems;
        result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
    });
    it('should return list of value of checked items', function () {
        fakeComponent.items = undefined;
        fakeComponent.selection = {
            checkedItems: [
                new treeview_item_1.TreeviewItem({ text: 'Item1', value: 1 }),
                new treeview_item_1.TreeviewItem({ text: 'Item2', value: 2 })
            ],
            uncheckedItems: undefined
        };
        var result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([1, 2]);
    });
});
describe('DownlineTreeviewEventParser', function () {
    var parser;
    var fakeComponent;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                treeview_module_1.TreeviewModule.forRoot()
            ],
            providers: [
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.DownlineTreeviewEventParser }
            ]
        });
        parser = testing_1.TestBed.get(treeview_event_parser_1.TreeviewEventParser);
        fakeComponent = testing_1.TestBed.createComponent(treeview_component_1.TreeviewComponent).componentInstance;
    });
    it('should return empty list if items is null or undefined', function () {
        fakeComponent.items = undefined;
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        var result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
        fakeComponent.items = null;
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
    });
    it('should return list of checked items with links', function () {
        var item1 = new treeview_item_1.TreeviewItem({ text: 'Item1', value: 1, checked: false });
        var item1Child1 = new treeview_item_1.TreeviewItem({ text: 'Item11', value: 11 });
        var item1Child2 = new treeview_item_1.TreeviewItem({
            text: 'Item12', value: 12, checked: false,
            children: [
                { text: 'Item12', value: 12, checked: false }
            ]
        });
        item1.children = [item1Child1, item1Child2];
        var item2 = new treeview_item_1.TreeviewItem({ text: 'Item2', value: 2 });
        var item3 = new treeview_item_1.TreeviewItem({ text: 'Item3', value: 3, checked: false });
        fakeComponent.items = [item1, item2, item3];
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        var result = parser.getSelectedChange(fakeComponent);
        var expected = [
            {
                item: item1Child1,
                parent: {
                    item: item1,
                    parent: null
                }
            },
            {
                item: item2,
                parent: null
            }
        ];
        expect(result).toEqual(expected);
    });
});
describe('OrderDownlineTreeviewEventParser', function () {
    var parser;
    var fakeComponent;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                treeview_module_1.TreeviewModule.forRoot()
            ],
            providers: [
                { provide: treeview_event_parser_1.TreeviewEventParser, useClass: treeview_event_parser_1.OrderDownlineTreeviewEventParser }
            ]
        });
        parser = testing_1.TestBed.get(treeview_event_parser_1.TreeviewEventParser);
        fakeComponent = testing_1.TestBed.createComponent(treeview_component_1.TreeviewComponent).componentInstance;
    });
    it('should return empty list if items is null or undefined', function () {
        fakeComponent.items = undefined;
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        var result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
        fakeComponent.items = null;
        fakeComponent.selection = selectionWithUndefinedCheckedItems;
        result = parser.getSelectedChange(fakeComponent);
        expect(result).toEqual([]);
    });
    describe('', function () {
        var item1 = new treeview_item_1.TreeviewItem({ text: 'Item1', value: 1, checked: false });
        var item1Child1 = new treeview_item_1.TreeviewItem({ text: 'Item11', value: 11 });
        var item1Child2 = new treeview_item_1.TreeviewItem({
            text: 'Item12', value: 12, checked: false,
            children: [
                { text: 'Item12', value: 12, checked: false }
            ]
        });
        item1.children = [item1Child1, item1Child2];
        var item2 = new treeview_item_1.TreeviewItem({ text: 'Item2', value: 2, checked: false });
        var item3 = new treeview_item_1.TreeviewItem({ text: 'Item3', value: 3 });
        beforeEach(function () {
            fakeComponent.items = [item1, item2, item3];
            fakeComponent.selection = selectionWithUndefinedCheckedItems;
        });
        it('should return list of checked items with links', function () {
            var result = parser.getSelectedChange(fakeComponent);
            var expected = [
                {
                    item: item1Child1,
                    parent: {
                        item: item1,
                        parent: null
                    }
                },
                {
                    item: item3,
                    parent: null
                }
            ];
            expect(result).toEqual(expected);
        });
        it('should return list of checked items with links by order', function () {
            parser.getSelectedChange(fakeComponent);
            item1Child1.checked = false;
            item2.checked = true;
            var result = parser.getSelectedChange(fakeComponent);
            var expected = [
                {
                    item: item3,
                    parent: null
                },
                {
                    item: item2,
                    parent: null
                }
            ];
            expect(result).toEqual(expected);
        });
    });
});
//# sourceMappingURL=treeview-event-parser.spec.js.map