"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var lodash_1 = require("lodash");
var treeview_i18n_1 = require("./treeview-i18n");
var treeview_item_1 = require("./treeview-item");
var treeview_config_1 = require("./treeview-config");
var treeview_event_parser_1 = require("./treeview-event-parser");
var treeview_helper_1 = require("./treeview-helper");
var FilterTreeviewItem = /** @class */ (function (_super) {
    __extends(FilterTreeviewItem, _super);
    function FilterTreeviewItem(item) {
        var _this = _super.call(this, {
            text: item.text,
            value: item.value,
            disabled: item.disabled,
            checked: item.checked,
            collapsed: item.collapsed,
            children: item.children
        }) || this;
        _this.refItem = item;
        return _this;
    }
    FilterTreeviewItem.prototype.updateRefChecked = function () {
        this.children.forEach(function (child) {
            if (child instanceof FilterTreeviewItem) {
                child.updateRefChecked();
            }
        });
        var refChecked = this.checked;
        if (refChecked) {
            for (var _i = 0, _a = this.refItem.children; _i < _a.length; _i++) {
                var refChild = _a[_i];
                if (!refChild.checked) {
                    refChecked = false;
                    break;
                }
            }
        }
        this.refItem.checked = refChecked;
    };
    return FilterTreeviewItem;
}(treeview_item_1.TreeviewItem));
var TreeviewComponent = /** @class */ (function () {
    function TreeviewComponent(i18n, defaultConfig, eventParser) {
        this.i18n = i18n;
        this.defaultConfig = defaultConfig;
        this.eventParser = eventParser;
        this.selectedChange = new core_1.EventEmitter();
        this.filterChange = new core_1.EventEmitter();
        this.filterText = '';
        this.config = this.defaultConfig;
        this.allItem = new treeview_item_1.TreeviewItem({ text: 'All', value: undefined });
        this.createHeaderTemplateContext();
    }
    Object.defineProperty(TreeviewComponent.prototype, "hasFilterItems", {
        get: function () {
            return !lodash_1.isNil(this.filterItems) && this.filterItems.length > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TreeviewComponent.prototype, "maxHeight", {
        get: function () {
            return "" + this.config.maxHeight;
        },
        enumerable: true,
        configurable: true
    });
    TreeviewComponent.prototype.ngOnChanges = function (changes) {
        var itemsSimpleChange = changes['items'];
        if (!lodash_1.isNil(itemsSimpleChange)) {
            if (!lodash_1.isNil(this.items)) {
                this.updateFilterItems();
                this.updateCollapsedOfAll();
                this.raiseSelectedChange();
            }
        }
        this.createHeaderTemplateContext();
    };
    TreeviewComponent.prototype.onAllCollapseExpand = function () {
        var _this = this;
        this.allItem.collapsed = !this.allItem.collapsed;
        this.filterItems.forEach(function (item) { return item.setCollapsedRecursive(_this.allItem.collapsed); });
    };
    TreeviewComponent.prototype.onFilterTextChange = function (text) {
        this.filterText = text;
        this.filterChange.emit(text);
        this.updateFilterItems();
    };
    TreeviewComponent.prototype.onAllCheckedChange = function () {
        var checked = this.allItem.checked;
        this.filterItems.forEach(function (item) {
            item.setCheckedRecursive(checked);
            if (item instanceof FilterTreeviewItem) {
                item.updateRefChecked();
            }
        });
        this.raiseSelectedChange();
    };
    TreeviewComponent.prototype.onItemCheckedChange = function (item, checked) {
        if (item instanceof FilterTreeviewItem) {
            item.updateRefChecked();
        }
        this.updateCheckedOfAll();
        this.raiseSelectedChange();
    };
    TreeviewComponent.prototype.raiseSelectedChange = function () {
        this.generateSelection();
        var values = this.eventParser.getSelectedChange(this);
        this.selectedChange.emit(values);
    };
    TreeviewComponent.prototype.createHeaderTemplateContext = function () {
        var _this = this;
        this.headerTemplateContext = {
            config: this.config,
            item: this.allItem,
            onCheckedChange: function () { return _this.onAllCheckedChange(); },
            onCollapseExpand: function () { return _this.onAllCollapseExpand(); },
            onFilterTextChange: function (text) { return _this.onFilterTextChange(text); }
        };
    };
    TreeviewComponent.prototype.generateSelection = function () {
        var checkedItems = [];
        var uncheckedItems = [];
        if (!lodash_1.isNil(this.items)) {
            var selection = treeview_helper_1.TreeviewHelper.concatSelection(this.items, checkedItems, uncheckedItems);
            checkedItems = selection.checked;
            uncheckedItems = selection.unchecked;
        }
        this.selection = {
            checkedItems: checkedItems,
            uncheckedItems: uncheckedItems
        };
    };
    TreeviewComponent.prototype.updateFilterItems = function () {
        var _this = this;
        if (this.filterText !== '') {
            var filterItems_1 = [];
            var filterText_1 = this.filterText.toLowerCase();
            this.items.forEach(function (item) {
                var newItem = _this.filterItem(item, filterText_1);
                if (!lodash_1.isNil(newItem)) {
                    filterItems_1.push(newItem);
                }
            });
            this.filterItems = filterItems_1;
        }
        else {
            this.filterItems = this.items;
        }
        this.updateCheckedOfAll();
    };
    TreeviewComponent.prototype.filterItem = function (item, filterText) {
        var _this = this;
        var isMatch = lodash_1.includes(item.text.toLowerCase(), filterText);
        if (isMatch) {
            return item;
        }
        else {
            if (!lodash_1.isNil(item.children)) {
                var children_1 = [];
                item.children.forEach(function (child) {
                    var newChild = _this.filterItem(child, filterText);
                    if (!lodash_1.isNil(newChild)) {
                        children_1.push(newChild);
                    }
                });
                if (children_1.length > 0) {
                    var newItem = new FilterTreeviewItem(item);
                    newItem.collapsed = false;
                    newItem.children = children_1;
                    return newItem;
                }
            }
        }
        return undefined;
    };
    TreeviewComponent.prototype.updateCheckedOfAll = function () {
        var itemChecked = null;
        for (var _i = 0, _a = this.filterItems; _i < _a.length; _i++) {
            var filterItem = _a[_i];
            if (itemChecked === null) {
                itemChecked = filterItem.checked;
            }
            else if (itemChecked !== filterItem.checked) {
                itemChecked = undefined;
                break;
            }
        }
        if (itemChecked === null) {
            itemChecked = false;
        }
        this.allItem.checked = itemChecked;
    };
    TreeviewComponent.prototype.updateCollapsedOfAll = function () {
        var hasItemExpanded = false;
        for (var _i = 0, _a = this.filterItems; _i < _a.length; _i++) {
            var filterItem = _a[_i];
            if (!filterItem.collapsed) {
                hasItemExpanded = true;
                break;
            }
        }
        this.allItem.collapsed = !hasItemExpanded;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], TreeviewComponent.prototype, "headerTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", core_1.TemplateRef)
    ], TreeviewComponent.prototype, "itemTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], TreeviewComponent.prototype, "items", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", treeview_config_1.TreeviewConfig)
    ], TreeviewComponent.prototype, "config", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], TreeviewComponent.prototype, "selectedChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], TreeviewComponent.prototype, "filterChange", void 0);
    TreeviewComponent = __decorate([
        core_1.Component({
            selector: 'ngx-treeview',
            templateUrl: './treeview.component.html',
            styleUrls: ['./treeview.component.scss']
        }),
        __metadata("design:paramtypes", [treeview_i18n_1.TreeviewI18n,
            treeview_config_1.TreeviewConfig,
            treeview_event_parser_1.TreeviewEventParser])
    ], TreeviewComponent);
    return TreeviewComponent;
}());
exports.TreeviewComponent = TreeviewComponent;
//# sourceMappingURL=treeview.component.js.map