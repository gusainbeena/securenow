"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var treeview_config_1 = require("./treeview-config");
describe('TreeviewConfig', function () {
    it('should have sensible default values', function () {
        var config = new treeview_config_1.TreeviewConfig();
        expect(config.hasAllCheckBox).toBe(true);
        expect(config.hasFilter).toBe(false);
        expect(config.hasCollapseExpand).toBe(false);
        expect(config.maxHeight).toBe(500);
        expect(config.decoupleChildFromParent).toBe(false);
    });
});
//# sourceMappingURL=treeview-config.spec.js.map