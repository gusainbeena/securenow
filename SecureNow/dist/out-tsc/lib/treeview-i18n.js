"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TreeviewI18n = /** @class */ (function () {
    function TreeviewI18n() {
    }
    TreeviewI18n = __decorate([
        core_1.Injectable()
    ], TreeviewI18n);
    return TreeviewI18n;
}());
exports.TreeviewI18n = TreeviewI18n;
var TreeviewI18nDefault = /** @class */ (function (_super) {
    __extends(TreeviewI18nDefault, _super);
    function TreeviewI18nDefault() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TreeviewI18nDefault.prototype.getText = function (selection) {
        if (selection.uncheckedItems.length === 0) {
            return this.getAllCheckboxText();
        }
        switch (selection.checkedItems.length) {
            case 0:
                return 'Select options';
            case 1:
                return selection.checkedItems[0].text;
            default:
                return selection.checkedItems.length + " options selected";
        }
    };
    TreeviewI18nDefault.prototype.getAllCheckboxText = function () {
        return 'All';
    };
    TreeviewI18nDefault.prototype.getFilterPlaceholder = function () {
        return 'Filter';
    };
    TreeviewI18nDefault.prototype.getFilterNoItemsFoundText = function () {
        return 'No items found';
    };
    TreeviewI18nDefault.prototype.getTooltipCollapseExpandText = function (isCollapse) {
        return isCollapse ? 'Expand' : 'Collapse';
    };
    TreeviewI18nDefault = __decorate([
        core_1.Injectable()
    ], TreeviewI18nDefault);
    return TreeviewI18nDefault;
}(TreeviewI18n));
exports.TreeviewI18nDefault = TreeviewI18nDefault;
//# sourceMappingURL=treeview-i18n.js.map