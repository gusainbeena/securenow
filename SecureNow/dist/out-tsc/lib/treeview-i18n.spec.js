"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var treeview_item_1 = require("./treeview-item");
var treeview_i18n_1 = require("./treeview-i18n");
describe('TreeviewI18nDefault', function () {
    var treeviewI18n;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [
                treeview_i18n_1.TreeviewI18nDefault
            ]
        });
        treeviewI18n = testing_1.TestBed.get(treeview_i18n_1.TreeviewI18nDefault);
    });
    describe('getText', function () {
        it('should return "All" if having no unchecked items', function () {
            var selection = {
                checkedItems: undefined,
                uncheckedItems: []
            };
            expect(treeviewI18n.getText(selection)).toBe('All');
        });
        it('should return "Select options" if list of checked items is empty', function () {
            var selection = {
                checkedItems: [],
                uncheckedItems: [new treeview_item_1.TreeviewItem({ text: 'Item 1', value: undefined })]
            };
            expect(treeviewI18n.getText(selection)).toBe('Select options');
        });
        it('should return text of first checked item if length of checked items is 1', function () {
            var selection = {
                checkedItems: [new treeview_item_1.TreeviewItem({ text: 'Item 1', value: undefined })],
                uncheckedItems: [new treeview_item_1.TreeviewItem({ text: 'Item 2', value: undefined })]
            };
            expect(treeviewI18n.getText(selection)).toBe('Item 1');
        });
        it('should return "2 options selected" if length of checked items is 2', function () {
            var selection = {
                checkedItems: [
                    new treeview_item_1.TreeviewItem({ text: 'Item 1', value: undefined }),
                    new treeview_item_1.TreeviewItem({ text: 'Item 2', value: undefined })
                ],
                uncheckedItems: [new treeview_item_1.TreeviewItem({ text: 'Item 3', value: undefined })]
            };
            expect(treeviewI18n.getText(selection)).toBe('2 options selected');
        });
    });
});
//# sourceMappingURL=treeview-i18n.spec.js.map