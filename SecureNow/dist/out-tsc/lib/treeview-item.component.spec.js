"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var lodash_1 = require("lodash");
var testing_2 = require("../testing");
var treeview_config_1 = require("./treeview-config");
var treeview_item_component_1 = require("./treeview-item.component");
var treeview_item_1 = require("./treeview-item");
var treeview_item_template_spec_1 = require("./treeview-item-template.spec");
var fakeData = {
    item: undefined,
    checkedChange: function (checked) { }
};
var testTemplate = treeview_item_template_spec_1.fakeItemTemplate
    + '<ngx-treeview-item [item]="item" [template]="itemTemplate" (checkedChange)="checkedChange($event)"></ngx-treeview-item>';
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        this.item = fakeData.item;
        this.checkedChange = function (checked) { return fakeData.checkedChange(checked); };
    }
    TestComponent = __decorate([
        core_1.Component({
            selector: 'ngx-test',
            template: '',
        })
    ], TestComponent);
    return TestComponent;
}());
var createTestComponent = function (html) {
    return testing_2.createGenericTestComponent(html, TestComponent);
};
describe('TreeviewItemComponent', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [
                forms_1.FormsModule,
                platform_browser_1.BrowserModule
            ],
            declarations: [
                TestComponent,
                treeview_item_component_1.TreeviewItemComponent
            ],
            providers: [treeview_config_1.TreeviewConfig]
        });
    });
    it('should initialize with default config', function () {
        var defaultConfig = new treeview_config_1.TreeviewConfig();
        var component = testing_1.TestBed.createComponent(treeview_item_component_1.TreeviewItemComponent).componentInstance;
        testing_2.expect(component.config).toEqual(defaultConfig);
    });
    describe('item', function () {
        it('should not have element with class "treeview-item" if no item provided', function () {
            var fixture = createTestComponent('<ngx-treeview-item></ngx-treeview-item>');
            fixture.detectChanges();
            var element = fixture.debugElement.query(platform_browser_1.By.css('.treeview-item'));
            testing_2.expect(element).toBeNull();
        });
        it('should have element with class "treeview-item" if binding item', function () {
            fakeData.item = new treeview_item_1.TreeviewItem({ text: '1', value: 1 });
            var fixture = createTestComponent('<ngx-treeview-item [item]="item"></ngx-treeview-item>');
            fixture.detectChanges();
            var element = fixture.debugElement.query(platform_browser_1.By.css('.treeview-item'));
            testing_2.expect(element).not.toBeNull();
        });
    });
    describe('template', function () {
        var fixture;
        var collapsedElement;
        var parentCheckbox;
        var childrenCheckboxes;
        beforeEach(function () {
            fakeData.item = new treeview_item_1.TreeviewItem({
                text: 'Parent 1',
                value: 1,
                checked: true,
                collapsed: false,
                disabled: false,
                children: [
                    { text: 'Child 1', value: 11 },
                    { text: 'Child 2', value: 12 }
                ]
            });
        });
        beforeEach(testing_1.fakeAsync(function () {
            fixture = createTestComponent(testTemplate);
            fixture.detectChanges();
            testing_1.tick();
            collapsedElement = fixture.debugElement.query(platform_browser_1.By.css('.fa'));
            var checkboxElements = fixture.debugElement.queryAll(platform_browser_1.By.css('.form-check-input'));
            parentCheckbox = checkboxElements[0];
            childrenCheckboxes = lodash_1.slice(checkboxElements, 1);
        }));
        it('should work', function () {
            var textElement = fixture.debugElement.query(platform_browser_1.By.css('.form-check-label'));
            testing_2.expect(textElement.nativeElement.innerText.trim()).toBe('Parent 1');
            testing_2.expect(parentCheckbox.nativeElement.checked).toBeTruthy();
            testing_2.expect(parentCheckbox.nativeElement.disabled).toBeFalsy();
            testing_2.expect(collapsedElement.nativeElement).toHaveCssClass('fa-caret-down');
        });
        it('should render "Parent 1", "Child 1" & "Child 2" with checked', function () {
            testing_2.expect(parentCheckbox.nativeElement.checked).toEqual(true);
            testing_2.expect(childrenCheckboxes.map(function (element) { return element.nativeElement.checked; })).toEqual([true, true]);
        });
        describe('toggle collapse/expand', function () {
            beforeEach(testing_1.fakeAsync(function () {
                collapsedElement.triggerEventHandler('click', {});
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should invoke onCollapseExpand to change value of collapsed', function () {
                testing_2.expect(collapsedElement.nativeElement).toHaveCssClass('fa-caret-right');
            });
            it('should not render children', function () {
                var checkboxElements = fixture.debugElement.queryAll(platform_browser_1.By.css('.form-check-input'));
                testing_2.expect(checkboxElements.length).toBe(1);
            });
        });
        describe('uncheck "Parent 1"', function () {
            var spy;
            beforeEach(testing_1.fakeAsync(function () {
                spy = spyOn(fakeData, 'checkedChange');
                parentCheckbox.nativeElement.click();
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should un-check "Child 1" & "Child 2"', function () {
                testing_2.expect(childrenCheckboxes.map(function (element) { return element.nativeElement.checked; })).toEqual([false, false]);
            });
            it('should raise event checkedChange', function () {
                testing_2.expect(spy.calls.count()).toBe(1);
                var args = spy.calls.mostRecent().args;
                testing_2.expect(args[0]).toBeFalsy();
            });
        });
        describe('un-check "Child 1"', function () {
            var spy;
            beforeEach(testing_1.fakeAsync(function () {
                spy = spyOn(fakeData, 'checkedChange');
                childrenCheckboxes[0].nativeElement.click();
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('should uncheck "Parent 1"', function () {
                testing_2.expect(parentCheckbox.nativeElement.checked).toEqual(false);
            });
            it('should raise event checkedChange', function () {
                testing_2.expect(spy.calls.count()).toBe(1);
                var args = spy.calls.mostRecent().args;
                testing_2.expect(args[0]).toBeFalsy();
            });
            describe('un-check "Child 2"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    spy.calls.reset();
                    childrenCheckboxes[1].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should keep "Parent 1" unchecked', function () {
                    testing_2.expect(parentCheckbox.nativeElement.checked).toEqual(false);
                });
                it('should raise event checkedChange', function () {
                    testing_2.expect(spy.calls.count()).toBe(1);
                    var args = spy.calls.mostRecent().args;
                    testing_2.expect(args[0]).toBeFalsy();
                });
            });
            describe('check "Child 1"', function () {
                beforeEach(testing_1.fakeAsync(function () {
                    spy.calls.reset();
                    childrenCheckboxes[0].nativeElement.click();
                    fixture.detectChanges();
                    testing_1.tick();
                }));
                it('should check "Parent 1"', function () {
                    testing_2.expect(parentCheckbox.nativeElement.checked).toEqual(true);
                });
                it('should raise event checkedChange', function () {
                    testing_2.expect(spy.calls.count()).toBe(1);
                    var args = spy.calls.mostRecent().args;
                    testing_2.expect(args[0]).toBeTruthy();
                });
            });
        });
    });
});
//# sourceMappingURL=treeview-item.component.spec.js.map