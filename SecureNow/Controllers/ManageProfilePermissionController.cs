﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CustomValidator = SecureNow.Validators.CustomValidator;
using TreeNode = SecureNow.Container.TreeNode;

namespace SecureNow.Controllers
{
    public class ManageProfilePermissionController : Controller
    {
        DBModels db = new DBModels();
        public JsonResult GetExistingMetaData()
        {
            Stream req = Request.InputStream;
            SearchMetaDataDTO keyvalue = (SearchMetaDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(SearchMetaDataDTO));

            object jsonsuccess = null;
            List<KeyValueData> attributesList = new List<KeyValueData>();
            Dictionary<string, List<KeyValueData>> data = new Dictionary<string, List<KeyValueData>>();
            List<EmployeeAttributeLink> localList = new List<EmployeeAttributeLink>();
            var size = 15;
            int pageNo = 0;
            

            if (keyvalue != null)
            {
                if (keyvalue.pageNumber != null)
                {
                    pageNo = Convert.ToInt32(keyvalue.pageNumber);

                    if (keyvalue.searchKey != null && !(keyvalue.searchKey.Equals("")))
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab) && e.keyValue.Equals(keyvalue.searchKey)
                                          orderby e.id descending
                                          select e).Skip(pageNo * size).Take(size).ToList();
                    }
                    else
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab)
                                          orderby e.id descending
                                          select e).Skip(pageNo * size).Take(size).ToList();
                    }
                }
                else
                {
                    if (keyvalue.searchKey != null && !(keyvalue.searchKey.Equals("")))
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab) && e.keyValue.Equals(keyvalue.searchKey)
                                          orderby e.id descending
                                          select e).ToList();
                    }
                    else
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab)
                                          orderby e.id descending
                                          select e).ToList();
                    }
                }

            }


            jsonsuccess = new
            {
                rsBody = new
                {
                    existingList = attributesList,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExistingAccessForProfile()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            AccessControlDTO finalData = new AccessControlDTO();

            if (data != null && data.profile != null)
            {
                List<TreeNode> childNodes = GetAllOUList();
                finalData.ouList = childNodes;

                List<int> selectedOU = new List<int>();
                finalData.user = data.user;
                if (data.profile.Count == 1)
                {
                    foreach (string s in data.profile)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && k.keyValue.Equals(s) select k).FirstOrDefault();
                        if (profile != null)
                        {
                            finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                            HashSet<string> ouID = new HashSet<string>();

                            finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                            List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();
                            finalData.selectedFunctions.AddRange((from k in db.keyValueData
                                                                  where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl")
                                                                  select
                                                                  new RolePerformanceListDTO
                                                                  {
                                                                      row = k,
                                                                      selected = false
                                                                  }).ToList());

                            foreach (RolePerformanceListDTO item in finalData.selectedFunctions)
                            {
                                item.selectAll = false;

                                int countPermission = 0;
                                List<RolePerformanceListDTO> tempList = new List<RolePerformanceListDTO>();
                                UserAccessLinking link = (from u in db.userAccessLinkings where u.profileId == profile.id && u.functionId == item.row.id select u).FirstOrDefault();
                                if (link != null)
                                {
                                    string[] selectedOUList = link.OUs.Split(':');
                                    foreach (string itm in selectedOUList)
                                    {
                                        if (itm != "" && itm != null)
                                        {
                                            int id = Convert.ToInt32(itm);
                                            if (!selectedOU.Contains(id))
                                            {
                                                selectedOU.Add(id);
                                            }
                                        }
                                    }
                                }
                                finalData.selectedOUs = selectedOU;
                                if (link != null)
                                {
                                    item.selected = true;
                                    foreach (KeyValueData p in permissions)
                                    {
                                        UserAccessLinking per = (from u in db.userAccessLinkings where u.profileId == profile.id && u.functionId == item.row.id && u.permissionId == p.id select u).FirstOrDefault();

                                        RolePerformanceListDTO tempObj = new RolePerformanceListDTO();
                                        if (per != null)
                                        {
                                            tempObj.selected = true;
                                            tempObj.row = p;
                                            countPermission = countPermission + 1;
                                            if (countPermission == permissions.Count)
                                            {
                                                item.selectAll = true;
                                            }
                                        }
                                        else
                                        {
                                            tempObj.selected = false;
                                            tempObj.row = p;
                                        }
                                        tempList.Add(tempObj);
                                    }

                                }
                                else
                                {
                                    foreach (KeyValueData p in permissions)
                                    {
                                        //UserAccessLinking per = (from u in db.userAccessLinkings where u.profileId == profile.id && u.functionId == item.row.id && u.permissionId == p.id select u).FirstOrDefault();

                                        RolePerformanceListDTO tempObj = new RolePerformanceListDTO();
                                        tempObj.selected = false;
                                        tempObj.row = p;
                                        tempList.Add(tempObj);
                                    }
                                }
                                finalData.selectedPermissions.Add(item.row.keyValue, tempList);
                            }

                        }
                    }
                    finalData.profile = data.profile;
                }
                else
                {
                    List<int> profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k.id).ToList();
                    if (profile != null && profile.Count > 0)
                    {
                        finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                        finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                        List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();
                        finalData.selectedFunctions.AddRange((from k in db.keyValueData
                                                              where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl")
                                                              select
                                                              new RolePerformanceListDTO
                                                              {
                                                                  row = k,
                                                                  selected = false
                                                              }).ToList());
                        foreach (RolePerformanceListDTO item in finalData.selectedFunctions)
                        {

                        }
                    }
                }
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    listData = finalData,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public List<TreeNode> GetAllOUList()
        {
            KeyValueData topOu = (from e in db.keyValueData
                                  where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.preferenceValue == 0
                         && e.keyName.Equals("OperationalUnit")
                                  select e).FirstOrDefault();

            List<TreeNode> childNodes = new List<TreeNode>();
            if (topOu != null)
            {
                List<KeyValueData> topOuList = (from e in db.keyValueData
                                                where e.keyType.Equals(topOu.keyValue) && e.keyName.Equals("OperationalUnit")
                                                select e).ToList();


                if (topOuList != null)
                {
                    foreach (KeyValueData k in topOuList)
                    {
                        TreeNode root = AddNodeToRoot(k, topOu, new TreeNode());
                        root.text = k.keyValue;
                        root.value = k.id;
                        childNodes.Add(root);
                    }
                }
            }
            return childNodes;
        }

        public TreeNode AddNodeToRoot(KeyValueData ouValue, KeyValueData ouLevel, TreeNode nodes)
        {
            TreeNode nodeList = new TreeNode();
            KeyValueData nextOULevel = (from e in db.keyValueData
                                        where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.preferenceValue == ouLevel.preferenceValue + 1
                                        && e.keyName.Equals("OperationalUnit")
                                        select e).FirstOrDefault();

            if (nextOULevel != null)
            {
                List<KeyValueData> OuList = (from e in db.keyValueData
                                             join a in db.attributesLink on e.id equals a.childId
                                             where e.keyType.Equals(nextOULevel.keyValue) && e.keyName.Equals("OperationalUnit")
                                             && a.attributeSequence.Contains(ouValue.id + "") && a.childlabel.Equals(nextOULevel.keyValue)
                                             select e).ToList();
                if (OuList != null)
                {
                    foreach (KeyValueData k in OuList)
                    {
                        TreeNode root = AddNodeToRoot(k, nextOULevel, new TreeNode());
                        root.text = k.keyValue;
                        root.value = k.id;
                        nodeList.children.Add(root);
                    }
                }
            }
            else
            {
                nodeList.text = ouValue.keyValue;
                nodeList.value = ouValue.id;
            }
            return nodeList;
        }

        public JsonResult GetExistingAccessForMultiProfile()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            AccessControlDTO finalData = new AccessControlDTO();
            if (data != null && data.profile != null)
            {
                List<int> selectedOU = new List<int>();
                //Get alll OU List
                List<TreeNode> childNodes = GetAllOUList();
                finalData.ouList = childNodes;
                List<KeyValueData> attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.keyName.Equals("OperationalUnit") select e).ToList();
                finalData.user = data.user;
                if (data.profile.Count > 0)
                {
                    List<Int32> profIds = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k.id).ToList();
                    finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                    HashSet<string> ouID = new HashSet<string>();

                    finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                    List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();

                    List<int> functionsIds = (from u in db.userAccessLinkings where profIds.Contains(u.profileId) select u.functionId).Distinct().ToList();


                    foreach (int u in functionsIds)
                    {
                        RolePerformanceListDTO funcDet = (from k in db.keyValueData
                                                          where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl") && k.id == u
                                                          select
                                                          new RolePerformanceListDTO
                                                          {
                                                              row = k,
                                                              selected = true,
                                                              selectAll = false
                                                          }).FirstOrDefault();

                        UserAccessLinking ous = (from k in db.userAccessLinkings where profIds.Contains(k.profileId) && k.functionId == u select k).FirstOrDefault();

                        if (ous != null)
                        {

                            string[] selectedOUList = ous.OUs.Split(':');
                            foreach (string itm in selectedOUList)
                            {
                                if (itm != "" && itm != null)
                                {
                                    int id = Convert.ToInt32(itm);
                                    if (!selectedOU.Contains(id))
                                    {
                                        selectedOU.Add(id);
                                    }
                                }
                            }
                            finalData.selectedOUs = selectedOU;
                        }

                        List<int> permissionID = (from e in db.userAccessLinkings where e.functionId == u select e.permissionId).ToList();
                        List<int> perIds = (from per in db.userAccessLinkings where profIds.Contains(per.profileId) && per.functionId == u select per.permissionId).ToList();
                        List<RolePerformanceListDTO> tempList = new List<RolePerformanceListDTO>();
                        if (perIds != null)
                        {
                            int countAll = 0;
                            foreach (KeyValueData p in permissions)
                            {
                                RolePerformanceListDTO temp = new RolePerformanceListDTO();
                                if (perIds.Contains(p.id))
                                {
                                    temp.row = p;
                                    temp.selected = true;
                                    countAll = countAll + 1;
                                }
                                else
                                {
                                    temp.row = p;
                                    temp.selected = false;
                                }
                                tempList.Add(temp);
                            }
                            if (countAll == permissions.Count)
                            {
                                funcDet.selectAll = true;
                            }

                            finalData.selectedFunctions.Add(funcDet);
                            if (finalData.selectedPermissions.ContainsKey(funcDet.row.keyValue))
                            {
                                finalData.selectedPermissions[funcDet.row.keyValue].AddRange(tempList);
                            }
                            else
                            {
                                finalData.selectedPermissions.Add(funcDet.row.keyValue, tempList);
                            }
                        }
                    }
                    finalData.selectedRole = data.selectedRole;
                    finalData.profile = data.profile;
                }
                //}
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    listData = finalData,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public void GenerateOUsLinking(MetaDataSetupDTO frontData, KeyValueData generetedKey)
        {
            if (generetedKey != null)
            {
                List<KeyValueData> upperLevels = new List<KeyValueData>();
                AttributesLink newLink = new AttributesLink();
                KeyValueData currentLevel = (from k in db.keyValueData
                                             where k.keyName.Equals(frontData.key) && k.keyType.Equals("MetaData")
                                             && k.keyValue.Equals(frontData.subkey)
                                             select k).FirstOrDefault();
                if (generetedKey.keyName.Equals("OperationalUnit"))
                {
                    List<string> sequence = new List<string>();
                    int parentId = 0;
                    int childId = 0;
                    upperLevels.AddRange((from k in db.keyValueData
                                          where k.keyName.Equals(frontData.key) && k.keyType.Equals("MetaData")
                && k.preferenceValue < currentLevel.preferenceValue
                                          orderby k.preferenceValue ascending
                                          select k).ToList());

                    upperLevels.Add(currentLevel);
                    if (upperLevels.Count > 0)
                    {
                        foreach (KeyValueData item in upperLevels)
                        {
                            /*switch (item.keyValueModel)
                            {
                                case "OU1":
                                    if (currentLevel.keyValueModel.Equals(item.keyValueModel))
                                    {
                                    }
                                    else
                                    {
                                        if (item.preferenceValue == 0)
                                        {
                                            parentId = (from k in db.keyValueData
                                                        where k.keyType.Equals(item.keyValue) && k.keyName.Equals(item.keyName)
                                                        && k.keyValue.Equals(frontData.OU1)
                                                        select k.id).FirstOrDefault();
                                            sequence.Add(parentId + "");
                                        }
                                    }
                                    break;
                                case "OU2":
                                    if (currentLevel.keyValueModel.Equals(item.keyValueModel))
                                    {
                                        childId = generetedKey.id;
                                        sequence.Add(childId + "");

                                        newLink.parentId = parentId;
                                        newLink.childId = childId;
                                        newLink.attributeSequence = String.Join(":", sequence);
                                        newLink.startMonth = generetedKey.startMonth;
                                        newLink.endMonth = generetedKey.endMonth;
                                        newLink.year = generetedKey.year;
                                        newLink.childlabel = item.keyValue;
                                        db.attributesLink.Add(newLink);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        int intermediateId = (from k in db.keyValueData
                                                              where k.keyType.Equals(item.keyValue) && k.keyName.Equals(item.keyName)
                                                              && k.keyValue.Equals(frontData.OU2)
                                                              select k.id).FirstOrDefault();
                                        sequence.Add(intermediateId + "");
                                    }
                                    break;
                                case "OU3":
                                    if (currentLevel.keyValueModel.Equals(item.keyValueModel))
                                    {
                                        childId = generetedKey.id;
                                        sequence.Add(childId + "");

                                        newLink.parentId = parentId;
                                        newLink.childId = childId;
                                        newLink.attributeSequence = String.Join(":", sequence);
                                        newLink.startMonth = generetedKey.startMonth;
                                        newLink.endMonth = generetedKey.endMonth;
                                        newLink.year = generetedKey.year;
                                        newLink.childlabel = item.keyValue;
                                        db.attributesLink.Add(newLink);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        int intermediateId = (from k in db.keyValueData
                                                              where k.keyType.Equals(item.keyValue) && k.keyName.Equals(item.keyName)
                                                              && k.keyValue.Equals(frontData.OU3)
                                                              select k.id).FirstOrDefault();
                                        sequence.Add(intermediateId + "");
                                    }
                                    break;
                                case "OU4":
                                    if (currentLevel.keyValueModel.Equals(item.keyValueModel))
                                    {
                                        childId = generetedKey.id;
                                        sequence.Add(childId + "");

                                        newLink.parentId = parentId;
                                        newLink.childId = childId;
                                        newLink.attributeSequence = String.Join(":", sequence);
                                        newLink.startMonth = generetedKey.startMonth;
                                        newLink.endMonth = generetedKey.endMonth;
                                        newLink.year = generetedKey.year;
                                        newLink.childlabel = item.keyValue;
                                        db.attributesLink.Add(newLink);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        int intermediateId = (from k in db.keyValueData
                                                              where k.keyType.Equals(item.keyValue) && k.keyName.Equals(item.keyName)
                                                              && k.keyValue.Equals(frontData.OU4)
                                                              select k.id).FirstOrDefault();
                                        sequence.Add(intermediateId + "");
                                    }
                                    break;
                                case "OU5":
                                    if (currentLevel.keyValueModel.Equals(item.keyValueModel))
                                    {
                                        childId = generetedKey.id;
                                        sequence.Add(childId + "");

                                        newLink.parentId = parentId;
                                        newLink.childId = childId;
                                        newLink.attributeSequence = String.Join(":", sequence);
                                        newLink.startMonth = generetedKey.startMonth;
                                        newLink.endMonth = generetedKey.endMonth;
                                        newLink.year = generetedKey.year;
                                        newLink.childlabel = item.keyValue;
                                        db.attributesLink.Add(newLink);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        int intermediateId = (from k in db.keyValueData
                                                              where k.keyType.Equals(item.keyValue) && k.keyName.Equals(item.keyName)
                                                              && k.keyValue.Equals(frontData.OU5)
                                                              select k.id).FirstOrDefault();
                                        sequence.Add(intermediateId + "");
                                    }
                                    break;

                                default:
                                    break;
                            }*/
                        }
                    }

                }
                else
                {

                }
            }
        }

        [HttpPost]
        [ActionName("CreateMetaData")]
        public JsonResult CreateMetaData()
        {
            Stream req = Request.InputStream;
            MetaDataSetupDTO metaData = (MetaDataSetupDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataSetupDTO));

            object jsonsuccess = null;
            if (metaData != null)
            {
                ExceptionResponseContainer retVal = CustomValidator.applyValidations(metaData, typeof(ValidateMetaData));
                if (retVal.isValid)
                {
                    KeyValueData createNew = new KeyValueData();
                    switch (metaData.key)
                    {
                        case "SalesHorizontal":
                            if (metaData.subkey != metaData.key)
                            {
                                createNew.keyType = metaData.subkey;
                                createNew.keyValue = metaData.value;
                                createNew.keyName = metaData.key;
                                createNew.description = metaData.key;
                                createNew.startMonth = metaData.startMonth;
                                createNew.endMonth = metaData.endMonth;
                                createNew.relatedKeyType = metaData.key;
                                createNew.labelOnScreen = metaData.labelOnScreen;
                                createNew.preferenceValue = metaData.preferenceValue;
                                createNew.year = metaData.year;
                                db.keyValueData.Add(createNew);
                                db.SaveChanges();

                            }
                            else
                            {
                                List<KeyValueData> key = (from k in db.keyValueData where k.keyType.Equals("MetaData") && k.keyName.Equals("SalesHorizontal") orderby k.id descending select k).ToList();

                                KeyValueData newkey = new KeyValueData();
                                newkey.keyType = "MetaData";
                                newkey.keyName = metaData.key;
                                newkey.keyValue = metaData.value;
                                newkey.description = "This sales boundaries represents the core component while uploading the OP";
                                newkey.startMonth = metaData.startMonth;
                                newkey.endMonth = metaData.endMonth;
                                newkey.labelOnScreen = metaData.value;
                                newkey.preferenceValue = metaData.preferenceValue;
                                newkey.relatedKeyType = "MetaDataType";
                                if (key != null && key.Count > 0)
                                {
                                    newkey.preferenceSerial = key[0].preferenceSerial;
                                    newkey.preferenceValue = key[0].preferenceValue + 1;
                                    //newkey.keyValueModel = "SB" + (key.Count + 1);
                                }
                                else
                                {
                                    newkey.preferenceSerial = 1;
                                    newkey.preferenceValue = 0;
                                    //newkey.keyValueModel = "SB1";
                                }
                                db.keyValueData.Add(newkey);
                                db.SaveChanges();
                            }

                            break;
                        case "OperationalUnit":
                            if (metaData.subkey != metaData.key)
                            {
                                createNew.keyType = metaData.subkey;
                                createNew.keyValue = metaData.value;
                                createNew.keyName = metaData.key;
                                createNew.description = metaData.key;
                                createNew.startMonth = metaData.startMonth;
                                createNew.endMonth = metaData.endMonth;
                                createNew.relatedKeyType = metaData.key;
                                createNew.preferenceValue = metaData.preferenceValue;
                                createNew.labelOnScreen = metaData.labelOnScreen;
                                createNew.year = metaData.year;
                                db.keyValueData.Add(createNew);
                                db.SaveChanges();

                                GenerateOUsLinking(metaData, createNew);
                            }
                            else
                            {
                                List<KeyValueData> key = (from k in db.keyValueData where k.keyType.Equals("MetaData") && k.keyName.Equals("OperationalUnit") orderby k.id descending select k).ToList();

                                KeyValueData newkey = new KeyValueData();
                                newkey.keyType = "MetaData";
                                newkey.keyName = metaData.key;
                                newkey.keyValue = metaData.value;
                                newkey.description = "This is the one of the basic key of metadata.Under this we can define various geological regions";
                                newkey.startMonth = metaData.startMonth;
                                newkey.endMonth = metaData.endMonth;
                                newkey.labelOnScreen = metaData.value;
                                newkey.relatedKeyType = "MetaDataType";
                                if (key != null && key.Count > 0)
                                {
                                    newkey.preferenceSerial = key[0].preferenceSerial;
                                    newkey.preferenceValue = key[0].preferenceValue + 1;
                                    //newkey.keyValueModel = "OU" + (key.Count + 1);
                                }
                                else
                                {
                                    newkey.preferenceSerial = 1;
                                    newkey.preferenceValue = 0;
                                    //newkey.keyValueModel = "OU1";
                                }
                                db.keyValueData.Add(newkey);
                                db.SaveChanges();
                            }

                            break;
                        case "RoleHierarchy":
                            createNew.keyType = "MetaData";
                            createNew.keyValue = metaData.value;
                            createNew.keyName = metaData.key;
                            createNew.description = metaData.key;
                            createNew.startMonth = metaData.startMonth;
                            createNew.endMonth = metaData.endMonth;
                            createNew.relatedKeyType = "MetaDataType";
                            createNew.labelOnScreen = metaData.labelOnScreen;
                            createNew.year = metaData.year;
                            createNew.preferenceSerial = 1;
                            db.keyValueData.Add(createNew);
                            db.SaveChanges();

                            if (metaData.linkedBoundries != null && metaData.linkedBoundries.Count > 0)
                            {
                                TemplateData keyvaluedatalink = new TemplateData();
                                foreach (string per in metaData.linkedBoundries)
                                {
                                    keyvaluedatalink.keyValueFK = createNew.id;
                                    keyvaluedatalink.keyType = "DefaultSalesHorizontalsLinking";
                                    keyvaluedatalink.keyName = createNew.keyName;
                                    keyvaluedatalink.keyValue = per;
                                    keyvaluedatalink.startMonth = metaData.startMonth;
                                    keyvaluedatalink.endMonth = metaData.endMonth;
                                    keyvaluedatalink.labelOnScreen = createNew.labelOnScreen;
                                    keyvaluedatalink.description = createNew.description;
                                    keyvaluedatalink.year = metaData.year;
                                    db.templateData.Add(keyvaluedatalink);
                                    db.SaveChanges();
                                }
                            }

                            break;
                        case "SalesComponent":
                            createNew.keyType = "MetaData";
                            createNew.keyValue = metaData.value;
                            createNew.keyName = metaData.key;
                            createNew.description = metaData.key;
                            createNew.startMonth = metaData.startMonth;
                            createNew.endMonth = metaData.endMonth;
                            createNew.preferenceSerial = 1;
                            createNew.relatedKeyType = "MetaDataType";
                            createNew.labelOnScreen = metaData.value;
                            createNew.year = metaData.year;
                            createNew.labelOnScreen = metaData.labelOnScreen;
                            db.keyValueData.Add(createNew);
                            db.SaveChanges();

                            if (metaData.uploadFrequency != null && metaData.uploadFrequency != "")
                            {
                                TemplateData keyvaluedatalink = new TemplateData();
                                keyvaluedatalink.keyValueFK = createNew.id;
                                keyvaluedatalink.keyType = "DefaultSalesComponentFrequency";
                                keyvaluedatalink.keyName = createNew.keyName;
                                keyvaluedatalink.keyValue = metaData.uploadFrequency;
                                keyvaluedatalink.startMonth = metaData.startMonth;
                                keyvaluedatalink.endMonth = metaData.endMonth;
                                keyvaluedatalink.labelOnScreen = createNew.labelOnScreen;
                                keyvaluedatalink.description = createNew.description;
                                keyvaluedatalink.year = metaData.year;
                                db.templateData.Add(keyvaluedatalink);
                                db.SaveChanges();
                            }

                            break;
                        case "AccessControl":
                            if (metaData.subkey != metaData.key)
                            {
                                createNew.keyType = metaData.subkey;
                                createNew.keyValue = metaData.value;
                                createNew.keyName = metaData.key;
                                createNew.description = "Various roles according access functions";
                                createNew.startMonth = metaData.startMonth;
                                createNew.endMonth = metaData.endMonth;
                                createNew.relatedKeyType = metaData.key;
                                createNew.labelOnScreen = metaData.labelOnScreen;
                                createNew.year = metaData.year;
                                db.keyValueData.Add(createNew);
                                db.SaveChanges();

                            }
                            else
                            {

                            }
                            break;
                        default:
                            createNew.keyType = "SalesTeam";
                            createNew.keyValue = metaData.value;
                            createNew.keyName = metaData.key + " Level";
                            createNew.description = "Sales team defines hierarchy and sales components across department";
                            createNew.startMonth = metaData.startMonth;
                            createNew.endMonth = metaData.endMonth;
                            createNew.relatedKeyType = "MetaDataType";
                            createNew.labelOnScreen = metaData.labelOnScreen;
                            createNew.year = metaData.year;
                            createNew.labelOnScreen = metaData.labelOnScreen;
                            db.keyValueData.Add(createNew);
                            db.SaveChanges();

                            /*if (metaData.performanceComponent != null && metaData.performanceComponent.Count > 0)
                            {
                                KeyValueDataLink keyvaluedatalink = new KeyValueDataLink();
                                foreach (RolePerformanceListDTO per in metaData.performanceComponent)
                                {
                                    if (per.selected)
                                    {
                                        keyvaluedatalink.keyValueFK = createNew.id;
                                        keyvaluedatalink.keyType = per.row.keyType;
                                        keyvaluedatalink.keyName = per.row.keyName;
                                        keyvaluedatalink.keyValue = per.row.keyValue;
                                        keyvaluedatalink.startMonth = metaData.startMonth;
                                        keyvaluedatalink.endMonth = metaData.endMonth;
                                        keyvaluedatalink.labelOnScreen = per.row.labelOnScreen;
                                        keyvaluedatalink.description = per.row.description;
                                        keyvaluedatalink.year = metaData.year;
                                        keyvaluedatalink.uploadFrequency = per.uploadFrequency;

                                        db.keyValueDataLink.Add(keyvaluedatalink);
                                        db.SaveChanges();
                                    }
                                }
                            }*/

                            if (metaData.roleHierarchy != null && metaData.roleHierarchy.Count > 0)
                            {
                                KeyValueDataLink keyvaluedatalink = new KeyValueDataLink();
                                foreach (RolePerformanceListDTO role in metaData.roleHierarchy)
                                {
                                    if (role.selected)
                                    {
                                        foreach (string s in role.selectedBoundries)
                                        {
                                            keyvaluedatalink.keyValueFK = createNew.id;
                                            keyvaluedatalink.keyType = role.row.keyType;
                                            keyvaluedatalink.keyName = role.row.keyName;
                                            keyvaluedatalink.keyValue = role.row.keyValue;
                                            keyvaluedatalink.startMonth = metaData.startMonth;
                                            keyvaluedatalink.endMonth = metaData.endMonth;
                                            keyvaluedatalink.labelOnScreen = role.row.labelOnScreen;
                                            keyvaluedatalink.description = role.row.description;
                                            keyvaluedatalink.optional = role.optional;
                                            keyvaluedatalink.year = metaData.year;
                                            keyvaluedatalink.requiredSalesHorizontals = s;
                                            db.keyValueDataLink.Add(keyvaluedatalink);
                                            db.SaveChanges();
                                        }

                                    }
                                }
                            }
                            break;

                    }
                }
                else
                {
                    return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
                }


            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    msg = "success",
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public string triverseTree(List<TreeViewItem> children, string OUlist)
        {
            foreach (TreeViewItem itm in children)
            {
                if (itm.internalChecked != "False")
                {
                    OUlist = OUlist + ":" + itm.value.ToString();
                    OUlist = triverseTree(itm.internalChildren, OUlist);
                }
            }
            return OUlist;
        }

        public JsonResult CreateAccessPermissions()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            string msg = null;

            //get concate ou list which is selected for particular profile access
            string OUlist = "";
            foreach (TreeViewItem itm in data.treeViewItem)
            {
                if (itm.internalChecked != "False")
                {
                    //run recursion
                    if (OUlist == "")
                    {
                        OUlist = itm.value.ToString();
                    }
                    else
                    {
                        OUlist = OUlist + "::" + itm.value.ToString();
                    }
                    OUlist = triverseTree(itm.internalChildren, OUlist);
                }
            }


            if (data != null && data.selectedFunctions != null)
            {
                StringBuilder sb = new StringBuilder();
                if (data.selectedRole != null && data.selectedRole.Count != 0)
                {
                    if (data.selectedRole.Count > 0)
                    {

                        if (data.profile != null && data.profile.Count > 0)
                        {
                            KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                            List<UserExceptionPermissionFlow> oldData = (from u in db.UserExceptionPermissionFlow where u.profileId == profile.id select u).ToList();
                            db.UserExceptionPermissionFlow.RemoveRange(oldData);
                            db.SaveChanges();

                            foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                            {
                                if (rl.selected == true)
                                {
                                    foreach (string role in data.selectedRole)
                                    {
                                        UserExceptionPermissionFlow newUser = new UserExceptionPermissionFlow();
                                        newUser.profileId = profile.id;
                                        newUser.OUs = OUlist;
                                        newUser.roleId = (from e in db.keyValueData where e.keyName == "RoleHierarchy" && e.labelOnScreen == role select e.id).FirstOrDefault();// 0101;
                                        newUser.empId = 001;
                                    }

                                }
                            }
                            msg = "success";
                        }
                    }
                }
                else if (data.user != null)
                {
                    if (data.profile != null && data.profile.Count > 0)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                        List<UserExceptionPermissionFlow> oldData = (from u in db.UserExceptionPermissionFlow where u.profileId == profile.id select u).ToList();
                        db.UserExceptionPermissionFlow.RemoveRange(oldData);
                        db.SaveChanges();

                        foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                        {
                            if (rl.selected == true)
                            {

                                UserExceptionPermissionFlow newUser = new UserExceptionPermissionFlow();
                                newUser.profileId = profile.id;
                                newUser.OUs = sb.ToString();
                                //newUser.functionId = rl.row.id;
                                newUser.roleId = 0;
                                newUser.empId = Convert.ToInt32(data.user.adpId);
                                newUser.OUs = OUlist;

                            }
                        }
                        msg = "success";
                    }
                }
                else
                {

                    if (data.profile != null && data.profile.Count > 0)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                        List<UserAccessLinking> oldData = (from u in db.userAccessLinkings where u.profileId == profile.id select u).ToList();
                        db.userAccessLinkings.RemoveRange(oldData);
                        db.SaveChanges();

                        foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                        {
                            if (rl.selected == true)
                            {
                                UserAccessLinking newUser = new UserAccessLinking();
                                newUser.profileId = profile.id;
                                newUser.functionId = rl.row.id;
                                newUser.OUs = OUlist;
                                if (data.selectedPermissions.ContainsKey(rl.row.keyValue))
                                {
                                    foreach (RolePerformanceListDTO per in data.selectedPermissions[rl.row.keyValue])
                                    {
                                        if (per.selected == true)
                                        {
                                            newUser.permissionId = per.row.id;
                                        }
                                        db.userAccessLinkings.Add(newUser);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        msg = "success";
                    }
                }

            }
            else
            {
                msg = "error";
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    result = msg,
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ADPIdData()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));

            object jsonsuccess = null;

            MonthDTO monthDeatail = new MonthDTO();
            monthDeatail = CommonController.getMonthList((from k in db.keyValueData where k.keyType.Equals("CURRENTQUARTER") && k.keyName.Equals("CFY") select k.keyValue).FirstOrDefault());
            EmployeeRoleAndAreaDTO empDet = (from e in db.employeeBaseDetails
                                             join r in db.employeeRole on e.adpId equals r.adpId
                                             where e.adpId.Equals(data.user.adpId) &&
                                            ((r.startMonth <= monthDeatail.startMonth && r.endMonth >= monthDeatail.startMonth)
                                            || (r.startMonth >= monthDeatail.startMonth && r.endMonth <= monthDeatail.endMonth)
                                            || (r.startMonth >= monthDeatail.startMonth && r.startMonth <= monthDeatail.endMonth))
                                             select
                                             new EmployeeRoleAndAreaDTO
                                             {
                                                 fullName = e.fullName,
                                                 adpId = e.adpId,
                                                 role = r.role,
                                                 email = e.email

                                             }).FirstOrDefault();
            if (empDet != null)
            {
                jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                        empDet = empDet
                    }
                };
            }
            else
            {
                jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "error",
                    }
                };
            }

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getEmployeeAttributes()
        {
            Stream req = Request.InputStream;
            SearchMetaDataDTO keyvalue = (SearchMetaDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(SearchMetaDataDTO));
            object jsonsuccess = null;

            List<KeyValueData> attributesList = new List<KeyValueData>();
            string currentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();

            switch (keyvalue.currentTab)
            {
                case "TeamSetup":
                    attributesList = (from e in db.keyValueData where e.keyType.Equals("SalesTeam") && e.keyName.Equals(keyvalue.currentTab + " Level") select e).ToList();
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            attributesList = attributesList,
                            CFY = currentYear,
                            result = "success"
                        }
                    };
                    break;               
                default:
                    attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == keyvalue.metaDataLevel && e.keyName.Equals(keyvalue.currentTab) orderby e.preferenceValue ascending select e).ToList();
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            attributesList = attributesList,
                            CFY = currentYear,
                            result = "success"
                        }
                    };
                    break;
            }



            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
    }
}