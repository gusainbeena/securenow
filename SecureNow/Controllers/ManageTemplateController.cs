﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecureNow.Controllers
{
    public class ManageTemplateController : Controller
    {
        DBModels db = new DBModels();
        public JsonResult getActualTemplateAttributes()
        {
            Stream req = Request.InputStream;
            KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));            
            List<string> excelColumn = new List<string>();
            List<string> alphabets = new List<string>();
            List<string> columnId = new List<string>();
            List<string> columnsRow = new List<string>();
            List<string> performance = new List<string>();
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            int currentMonth = DateTime.Now.Month;
            List<KeyValueData> monthsList = new List<KeyValueData>();
            int count = 0;
            int countEnd = 0;
            for (char c = 'A'; c <= 'Z'; c++)
            {
                string columnNo = c.ToString();
                alphabets.Add(columnNo);
            }
            int q = 1;
            foreach (string str in alphabets)
            {
                dictionary.Add(q, str);
                q++;
            }


            KeyValueData cfy = (from fy in db.keyValueData
                                where fy.keyType.Equals("OPERATIONAL")
                                && fy.keyName.Equals("CFY")
                                select fy).FirstOrDefault();
            List<string> currentYear = new List<string>();
            currentYear.Add(cfy.keyValue);
            int year = Convert.ToInt32(cfy.keyValue);
            // Check Incentive Close or Not In Incentive Confirmation Table

            string quarter = (from d in db.incentiveCalculationRequest
                              where d.year.Equals(year)
                              && d.status.Equals(Codes.InStatus.CLOSED.ToString())
                              orderby d.quarter descending
                              select d.quarter).FirstOrDefault();


            if (quarter != null)
            {
                KeyValueData keyData = (from k in db.keyValueData
                                        where k.keyType.Equals("QTR")
                                        && k.keyName.Equals(quarter)
                                        select k).FirstOrDefault();
                int mon = Convert.ToInt32(keyData.relatedKeyValue);
                count = mon + 1;
                countEnd = count + 2;
            }
            else
            {
                count = 1;
                countEnd = 12;
            }

            for (int i = count; i <= countEnd; i++)
            {
                string month = i.ToString();
                KeyValueData monthData = (from d in db.keyValueData
                                          where (d.keyType.Equals("MOY")
                                          && d.relatedKeyType != "NONE"
                                          && d.keyValue.Equals(month))
                                          select (d)).FirstOrDefault();

                monthsList.Add(monthData);
            }



            List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" && e.preferenceSerial == 1 && e.preferenceSerial == 2 select e.keyValue).ToList();
            List<string> salesHorizontal = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" && e.preferenceValue >= 2 select e.keyValue).ToList();
            List<string> actualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue < 2 select e.keyValue).ToList();
            List<string> performanceComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesComponent" select e.keyValue).ToList();
            var ids = new List<int>() { 1, 2 };
            customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" && ids.Contains(e.preferenceSerial) select e.keyValue).ToList();
            string lastActualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue == 2 select e.keyValue).FirstOrDefault();

            // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
            foreach (string str in actualComponent)
            {
                excelColumn.Add(str);

            }

            // Adding All Customer Attributes in excel 
            foreach (string cust in customerComponent)
            {
                excelColumn.Add(cust);

            }


            // creating Column Id Row
            int colInExcel = excelColumn.Count();
            for (int si = 1; si <= colInExcel; si++)
            {
                columnId.Add(si.ToString());
            }


            // Converting Integers to their repective Alphabet
            foreach (string str in columnId)
            {
                int l = Convert.ToInt32(str) - 1;
                string toAlpha = toAlphabetic(l);
                columnsRow.Add(toAlpha);
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    monthsList = monthsList,
                    customerComponent = customerComponent,
                    excelColumn = excelColumn,
                    performanceComponent = performanceComponent,
                    actualComponent = actualComponent,
                    columnsId = columnsRow,
                    salesHorizontal = salesHorizontal
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public static string toAlphabetic(int i)
        {
            if (i < 0)
            {
                return "-" + toAlphabetic(-i - 1);
            }

            int quot = i / 26;
            int rem = i % 26;
            char letter = (char)((int)'A' + rem);
            //letter = (char)((int)'A');
            if (quot == 0)
            {
                return "" + letter;
            }
            else
            {
                return toAlphabetic(quot - 1) + letter;
            }

        }

        public JsonResult removeActualColumn()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            List<string> columnsId = new List<string>();
            int i = data.removeColumnId;
            foreach (string col in data.finalExcel)
            {
                columnsA.Add(col);
            }
            foreach (string col in data.columnsId)
            {
                columnsId.Add(col);
            }
            columnsA.Remove(columnsA[i]);
            int index = columnsId.Count() - 1;
            columnsId.Remove(columnsId[index]);
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columns = columnsA,
                    excelColumn = columnsA,
                    columnsId = columnsId
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult editLabel()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            List<string> columnsId = new List<string>();
            int i = data.removeColumnId;

            string label = columnsA[i];
            //columnsA.Remove(columnsA[i]);
            int index = columnsId.Count() - 1;
            columnsId.Remove(columnsId[index]);
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",

                    label = label,
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult generateActualTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            string message = null;
            string endMonth, startMonth;
            endMonth = data.endMonth.ToString();
            startMonth = data.startMonth.ToString();
            string currentMOY = DateTime.Now.Month.ToString();
            //KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            string cMonth = (from e in db.keyValueData where e.keyType == "MOY" && e.keyValue == currentMOY select e.keyName).FirstOrDefault();
            List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "ACTUAL" && e.keyName == "Template" select e).ToList().Where(t => Convert.ToInt32(t.endMonth) == Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.startMonth) == Convert.ToInt32(startMonth)).ToList();
            //Exist = (from e in db.keyValueData where e.keyType == "ACTUAL" && e.keyName == "Template" select e).ToList().ToList();
            List<string> previousColumnId = new List<string>();
            List<string> previousColumnsRow = new List<string>();
            List<string> previousExcelColumn = new List<string>();
            if (MetaDataValues.startMonth == 0 || MetaDataValues.endMonth == 0)
            {
                message = "Please Select Start Month and End Month";
                var jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                        message = message

                    }
                };
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }
            else
            if (data.performanceSelection.Count == 0)
            {
                message = "Please Select Performance Component";
                var jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                        message = message

                    }
                };
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (MetaDataValues.startMonth > MetaDataValues.endMonth)
                {
                    message = "Month Error";
                    var jsonsuccess = new
                    {
                        rsBody = new
                        {
                            result = "success",
                            message = message

                        }
                    };
                    return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                }
                else if (Exist.Count() != 0)
                {
                    /*
                    foreach (KeyValueData value in Exist)
                    {
                        previousExcelColumn.Add(value.keyValue);
                    }
                    message = "Actual Template for given period is already exist";
                    int colInExcel = Exist.Count();
                    for (int si = 1; si <= colInExcel; si++)
                    {
                        previousColumnId.Add(si.ToString());
                    }
                    foreach (string str in previousColumnId)
                    {
                        int l = Convert.ToInt32(str) - 1;
                        string toAlpha = toAlphabetic(l);
                        previousColumnsRow.Add(toAlpha);
                    }
                    var jsonsuccess = new
                    {
                        rsBody = new
                        {
                            result = "success",
                            message = message,
                            previousExcelColumn = previousExcelColumn,
                            previousColumnId = previousColumnsRow

                        }
                    };
                    return Json(jsonsuccess, JsonRequestBehavior.AllowGet);*/
                    foreach (KeyValueData k in Exist)
                    {
                        db.keyValueData.Remove(k);
                        db.SaveChanges();
                    }

                    List<string> excelColumn = new List<string>();
                    List<string> alphabets = new List<string>();
                    List<string> columnId = new List<string>();
                    List<string> columnsRow = new List<string>();
                    int currentMonth = DateTime.Now.Month;
                    List<KeyValueData> monthsList = new List<KeyValueData>();
                    List<string> performanceSelection = new List<string>();
                    List<string> frequency = new List<string>();
                    List<int> frValue = new List<int>();
                    string monthofYear = (from e in db.keyValueData where e.keyValue == DateTime.Now.Month.ToString() && e.keyType == "MOY" select e.keyName).FirstOrDefault();
                    int i = 0;
                    KeyValueData maxFrequency = (from e in db.keyValueData select e).FirstOrDefault();
                    string val;
                    foreach (string perform in data.performanceSelection)
                    {
                        //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                        int keyFK = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "SalesComponent" && e.keyValue == perform select e.id).FirstOrDefault();
                        //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                        frequency = (from e in db.templateData where e.keyType == "DefaultSalesComponentFrequency" && e.keyName == "salesComponent" && e.keyValueFK == keyFK && e.keyValue != null select e.keyValue).Distinct().ToList();
                        foreach (string fr in frequency)
                        {
                            int frTemp = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.keyValue == fr select e.preferenceValue).FirstOrDefault();
                            frValue.Add(frTemp);

                            int j = 0;
                        }
                        i = frValue.Max();
                        maxFrequency = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.preferenceValue == i select e).FirstOrDefault();
                        switch (maxFrequency.keyValue)
                        {
                            case "Week":
                                val = "Week";
                                int mondays = 0;
                                //DateTime thisMonth = DateTime.Now.Month;
                                int month = DateTime.Now.Month;
                                int year = DateTime.Now.Year;
                                int daysThisMonth = DateTime.DaysInMonth(year, month);
                                DateTime beginingOfThisMonth = new DateTime(year, month, 1);
                                for (int d = 0; d < daysThisMonth; d++)
                                    if (beginingOfThisMonth.AddDays(d).DayOfWeek == DayOfWeek.Monday)
                                        mondays++;
                                int noofWeeks = mondays;




                                for (int j = 1; j <= noofWeeks; j++)
                                {

                                    performanceSelection.Add(perform + " " + val + "" + j + " " + monthofYear + " Total");
                                }
                                frValue.Clear();
                                //performanceSelection.Add(perform + " " + val + "" +  " Total");
                                break;
                            case "Fortnight":
                                val = "Fortnight";

                                for (int j = 1; j <= 2; j++)
                                {
                                    performanceSelection.Add(perform + " " + val + "" + j + " " + monthofYear + " Total");
                                }
                                frValue.Clear();
                                //performanceSelection.Add(perform + " " + val  + " Total");
                                break;
                            case "Month":
                                val = "Month";


                                performanceSelection.Add(perform + " Total");

                                frValue.Clear();
                                break;

                            default:
                                performanceSelection.Add(perform + " Total");
                                frValue.Clear();
                                break;
                        }
                    }
                    /*
                    foreach (string perform in performanceSelection)
                    {
                        performanceSelection.Add(perform + " " + cMonth + " Total");
                    }*/
                    List<KeyValueData> monthsDataw = (from a in db.keyValueData
                                                      where a.keyType == "MOY"
                                                      select a).ToList().Where(t => Convert.ToInt32(t.keyValue) >= MetaDataValues.startMonth).Where(t => Convert.ToInt32(t.keyValue) <= MetaDataValues.endMonth).ToList();

                    List<string> salesHorizontal = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" && e.preferenceValue >= 2 select e.keyValue).ToList();
                    List<string> actualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue < 2 select e.keyValue).ToList();
                    var ids = new List<int>() { 1, 2 };
                    List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" && ids.Contains(e.preferenceSerial) select e.keyValue).ToList();
                    string lastActualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue == 2 select e.keyValue).FirstOrDefault();
                    // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
                    foreach (string str in actualComponent)
                    {
                        excelColumn.Add(str);

                    }
                    foreach (string cust in customerComponent)
                    {
                        excelColumn.Add(cust);

                    }
                    foreach (string perform in performanceSelection)
                    {

                        excelColumn.Add(perform);
                    }

                    foreach (string cust in data.salesList)
                    {
                        excelColumn.Add(cust);

                    }

                    // Adding All Customer Attributes in excel 


                    // creating Column Id Row
                    int colInExcel = excelColumn.Count();
                    for (int si = 1; si <= colInExcel; si++)
                    {
                        columnId.Add(si.ToString());
                    }


                    // Converting Integers to their repective Alphabet
                    foreach (string str in columnId)
                    {
                        int l = Convert.ToInt32(str) - 1;
                        string toAlpha = toAlphabetic(l);
                        columnsRow.Add(toAlpha);
                    }


                    var jsonsuccess = new
                    {
                        rsBody = new
                        {
                            result = "success",
                            customerComponent = customerComponent,
                            columnsId = columnsRow,
                            excelColumn = excelColumn,
                            finalExcel = excelColumn,

                        }
                    };
                    return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<string> excelColumn = new List<string>();
                    List<string> alphabets = new List<string>();
                    List<string> columnId = new List<string>();
                    List<string> columnsRow = new List<string>();
                    int currentMonth = DateTime.Now.Month;
                    List<KeyValueData> monthsList = new List<KeyValueData>();
                    List<string> performanceSelection = new List<string>();
                    List<string> frequency = new List<string>();
                    List<int> frValue = new List<int>();
                    string monthofYear = (from e in db.keyValueData where e.keyValue == DateTime.Now.Month.ToString() && e.keyType == "MOY" select e.keyName).FirstOrDefault();
                    int i = 0;
                    KeyValueData maxFrequency = (from e in db.keyValueData select e).FirstOrDefault();
                    string val;
                    foreach (string perform in data.performanceSelection)
                    {
                        //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                        int keyFK = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "SalesComponent" && e.keyValue == perform select e.id).FirstOrDefault();
                        //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                        frequency = (from e in db.templateData where e.keyType == "DefaultSalesComponentFrequency" && e.keyName == "salesComponent" && e.keyValueFK == keyFK && e.keyValue != null select e.keyValue).Distinct().ToList();
                        foreach (string fr in frequency)
                        {
                            int frTemp = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.keyValue == fr select e.preferenceValue).FirstOrDefault();
                            frValue.Add(frTemp);

                            int j = 0;
                        }
                        i = frValue.Max();
                        maxFrequency = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.preferenceValue == i select e).FirstOrDefault();
                        switch (maxFrequency.keyValue)
                        {
                            case "Week":
                                val = "Week";
                                int mondays = 0;
                                //DateTime thisMonth = DateTime.Now.Month;
                                int month = DateTime.Now.Month;
                                int year = DateTime.Now.Year;
                                int daysThisMonth = DateTime.DaysInMonth(year, month);
                                DateTime beginingOfThisMonth = new DateTime(year, month, 1);
                                for (int d = 0; d < daysThisMonth; d++)
                                    if (beginingOfThisMonth.AddDays(d).DayOfWeek == DayOfWeek.Monday)
                                        mondays++;
                                int noofWeeks = mondays;




                                for (int j = 1; j <= noofWeeks; j++)
                                {

                                    performanceSelection.Add(perform + " " + val + "" + j + " " + monthofYear + " Total");
                                }
                                frValue.Clear();
                                //performanceSelection.Add(perform + " " + val + "" +  " Total");
                                break;
                            case "Fortnight":
                                val = "Fortnight";

                                for (int j = 1; j <= 2; j++)
                                {
                                    //performanceSelection.Add(perform + " " + val + "" + j + " " + monthofYear + " Total");
                                    performanceSelection.Add(perform + " " + val + "" + j + " Total");
                                }
                                frValue.Clear();
                                //performanceSelection.Add(perform + " " + val  + " Total");
                                break;
                            case "Month":
                                val = "Month";


                                //performanceSelection.Add(perform +" "+monthofYear+ " Total");
                                performanceSelection.Add(perform + " Total");

                                frValue.Clear();
                                break;

                            default:
                                performanceSelection.Add(perform + " Total");
                                frValue.Clear();
                                break;
                        }
                    }
                    /*
                    foreach (string perform in performanceSelection)
                    {
                        performanceSelection.Add(perform + " " + cMonth + " Total");
                    }*/
                    List<KeyValueData> monthsDataw = (from a in db.keyValueData
                                                      where a.keyType == "MOY"
                                                      select a).ToList().Where(t => Convert.ToInt32(t.keyValue) >= MetaDataValues.startMonth).Where(t => Convert.ToInt32(t.keyValue) <= MetaDataValues.endMonth).ToList();

                    List<string> salesHorizontal = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" && e.preferenceValue >= 2 select e.keyValue).ToList();
                    List<string> actualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue < 2 select e.keyValue).ToList();
                    var ids = new List<int>() { 1, 2 };
                    List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" && ids.Contains(e.preferenceSerial) select e.keyValue).ToList();
                    string lastActualComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "actualComponent" && e.preferenceValue == 2 select e.keyValue).FirstOrDefault();
                    // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
                    foreach (string str in actualComponent)
                    {
                        excelColumn.Add(str);

                    }
                    foreach (string cust in customerComponent)
                    {
                        excelColumn.Add(cust);

                    }
                    foreach (string perform in performanceSelection)
                    {

                        excelColumn.Add(perform);
                    }

                    foreach (string cust in data.salesList)
                    {
                        excelColumn.Add(cust);

                    }

                    // Adding All Customer Attributes in excel 


                    // creating Column Id Row
                    int colInExcel = excelColumn.Count();
                    for (int si = 1; si <= colInExcel; si++)
                    {
                        columnId.Add(si.ToString());
                    }


                    // Converting Integers to their repective Alphabet
                    foreach (string str in columnId)
                    {
                        int l = Convert.ToInt32(str) - 1;
                        string toAlpha = toAlphabetic(l);
                        columnsRow.Add(toAlpha);
                    }


                    var jsonsuccess = new
                    {
                        rsBody = new
                        {
                            result = "success",
                            customerComponent = customerComponent,
                            columnsId = columnsRow,
                            excelColumn = excelColumn,
                            finalExcel = excelColumn,

                        }
                    };
                    return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                }
            }



        }


        public JsonResult downloadActualTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            string filepath = "";
            string fileName = "";
            string endMonth = data.endMonth.ToString();
            string startMonth = data.startMonth.ToString();
            int Col = 0;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            List<string> columnsA = new List<string>();
            List<string> editHeader = new List<string>();
            List<string> columnsB = new List<string>();
            List<char> characters = new List<char>();
            //List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "Actual" && e.keyName == "Template" select e).ToList();
            List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "Actual" && e.keyName == "Template" && e.relatedKeyValue == user.operationalUnit select e).ToList().Where(t => Convert.ToInt32(t.endMonth) == Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.startMonth) == Convert.ToInt32(startMonth)).ToList();
            if (Exist.Count != 0)
            {
                foreach (KeyValueData j in Exist)
                {
                    db.keyValueData.Remove(j);
                    db.SaveChanges();
                }
            }
            for (int f = 1; f <= data.columnList.Count; f++)
            {
                Col = f;
                string col = "COLUMN " + Col.ToString();
                columnsA.Add(col.ToString());
            }

            int k = columnsA.Count() - 1;
            for (int i = 0; i < data.columnList.Count; i++)
            {
                columnsA[i] = data.columnList[i];
            }
            for (int i = 0; i < columnsA.Count; i++)
            {
                string firstId = columnsA[i].ToString();
                string secondId = "";
                string val = columnsA[i];

            }
            foreach (string header in data.editHeader)
            {
                editHeader.Add(header);
            }
            string AppLocation = "";
            AppLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            AppLocation = AppLocation.Replace("file:\\", "");
            string date = DateTime.Now.ToShortDateString();
            date = date.Replace("/", "_");
            filepath = AppLocation + "\\ExcelFiles\\" + "Template_" + "Actual_" + date + ".xlsx";
            fileName = "Template_" + "Actual_" + date + ".xlsx";
            var workbook = new XLWorkbook();
            workbook.AddWorksheet("sheetName");
            var ws = workbook.Worksheet("sheetName");

            for (int i = 0; i < columnsA.Count; i++)
            {
                string x = columnsA[i];
                ws.Cell(1, i + 1).Value = x;

                ws.Cell(1, i + 1).Style.Fill.BackgroundColor = XLColor.Yellow;
                ws.Cell(1, i + 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Cell(1, i + 1).Style.Font.FontColor = XLColor.Black;
                ws.Cell(1, i + 1).Style.Border.BottomBorderColor = XLColor.Black;
                ws.Cell(1, i + 1).Style.Border.RightBorderColor = XLColor.Black;
                ws.Cell(1, i + 1).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                ws.Cell(1, i + 1).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Column(i + 1).AdjustToContents();
            }

            workbook.SaveAs(filepath);



            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columns = columnsA,
                    filepath = filepath,
                    fileName = fileName,
                    editHeader = editHeader

                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult insertActualTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            KeyValueData newValues = new KeyValueData();
            TemplateData newTemplateValues = new TemplateData();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            bool flag = false;
            int preferenceSerial = 0;
            int Serial = 0;
            string relatedKeyType = "employeeComponent";
            string relatedKeyValue = "";
            int startMonth = data.startMonth;
            int endMonth = data.endMonth;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            string CurrentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            string tags = data.keyValue;
            columnsA = tags.Split(',').ToList();


            foreach (string keys in columnsA)
            {

                if (keys != "")
                {

                    flag = true;
                    if (flag)
                    {

                        newValues.keyName = data.keyName;
                        newValues.keyType = data.keyType;
                        newValues.keyValue = keys;
                        newValues.preferenceValue = Serial;
                        newValues.relatedKeyType = "";
                        if (keys.Contains("Customer"))
                        {
                            newValues.relatedKeyType = "customerComponent";
                        }
                        else if (keys.Contains("Year") || keys.Contains("Month"))
                        {
                            newValues.relatedKeyType = "ActualComponent";
                        }
                        else if (keys.Contains("Region") || keys.Contains("territoryCode") || keys.Contains("SubTerritoryCode"))
                        {
                            newValues.relatedKeyType = "MetaDataComponent";
                        }
                        else
                        {
                            newValues.relatedKeyType = "PerformanceValues";
                        }
                        newValues.relatedKeyValue = user.operationalUnit;
                        newValues.preferenceSerial = preferenceSerial;
                        newValues.startMonth = startMonth;
                        newValues.endMonth = endMonth;
                        newValues.year = Convert.ToInt32(CurrentYear);
                        newValues.description = data.keyType + " " + data.keyName;
                        db.keyValueData.Add(newValues);
                        db.SaveChanges();

                        newTemplateValues.keyName = data.keyName;
                        newTemplateValues.keyType = data.keyType;
                        newTemplateValues.keyValue = keys;
                        newTemplateValues.preferenceValue = Serial;
                        newTemplateValues.relatedKeyType = "";
                        if (keys.Contains("Customer"))
                        {
                            newTemplateValues.relatedKeyType = "customerComponent";
                        }
                        newTemplateValues.preferenceSerial = preferenceSerial;
                        newTemplateValues.startMonth = startMonth;
                        newTemplateValues.endMonth = endMonth;
                        newTemplateValues.year = Convert.ToInt32(CurrentYear);
                        newTemplateValues.description = data.keyType + " " + data.keyName;
                        //db.TemplateDataTable.Add(newTemplateValues);
                        db.SaveChanges();
                    }


                }

                Serial++;
            }


            List<string> y = columnsA.ToList<string>();
            y.RemoveAll(p => string.IsNullOrEmpty(p));
            columnsA = y.ToList();


            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    excelColumn = columnsA
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }


        //-------------------------- Template Configuration Functions Start ---------------------------------
        public JsonResult getTemplateAttributes()
        {
            Stream req = Request.InputStream;
            KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            string message = null;
            List<string> excelColumn = new List<string>();
            List<string> alphabets = new List<string>();
            List<string> columnId = new List<string>();
            List<string> columnsRow = new List<string>();
            List<string> performance = new List<string>();
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            int currentMonth = DateTime.Now.Month;
            List<KeyValueData> monthsList = new List<KeyValueData>();
            int count = 0;
            int countEnd = 0;
            string employeeList = "";
            string customerList = "";

            for (char c = 'A'; c <= 'Z'; c++)
            {
                string columnNo = c.ToString();
                alphabets.Add(columnNo);
            }
            int q = 1;
            foreach (string str in alphabets)
            {
                dictionary.Add(q, str);
                q++;
            }


            KeyValueData cfy = (from fy in db.keyValueData
                                where fy.keyType.Equals("OPERATIONAL")
                                && fy.keyName.Equals("CFY")
                                select fy).FirstOrDefault();
            List<string> currentYear = new List<string>();
            currentYear.Add(cfy.keyValue);
            int year = Convert.ToInt32(cfy.keyValue);
            // Check Incentive Close or Not In Incentive Confirmation Table

            string quarter = (from d in db.incentiveCalculationRequest
                              where d.year.Equals(year)
                              && d.status.Equals(Codes.InStatus.CLOSED.ToString())
                              orderby d.quarter descending
                              select d.quarter).FirstOrDefault();


            if (quarter != null)
            {
                KeyValueData keyData = (from k in db.keyValueData
                                        where k.keyType.Equals("QTR")
                                        && k.keyName.Equals(quarter)
                                        select k).FirstOrDefault();
                int mon = Convert.ToInt32(keyData.relatedKeyValue);
                count = mon + 1;
                countEnd = count + 2;
            }
            else
            {
                count = 1;
                countEnd = 12;
            }

            //int count = Convert.ToInt32(keyData.relatedKeyType);
            for (int i = count; i <= countEnd; i++)
            {
                string month = i.ToString();
                KeyValueData monthData = (from d in db.keyValueData
                                          where (d.keyType.Equals("MOY")
                                          && d.relatedKeyType != "NONE"
                                          && d.keyValue.Equals(month))
                                          select (d)).FirstOrDefault();

                monthsList.Add(monthData);
            }


            List<string> employeeComponent = (from e in db.keyValueData where e.keyType == "EMPLOYEE" && e.keyName == "COMPONENT" select e.keyValue).ToList();
            List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" select e.keyValue).ToList();
            foreach (string str in employeeComponent)
            {

                employeeList = employeeList + " " + str + "-";

            }
            employeeList.TrimEnd('-');
            foreach (string str in customerComponent)
            {

                customerList = customerList + " " + str + "-";
                customerList.TrimEnd('-');
            }

            List<string> metas = (from e in db.keyValueData where e.keyName == "MetaDataType" && e.keyValue != "RoleHierarchy" select e.keyValue).ToList();
            List<string> months = (from e in db.keyValueData where e.keyType == "MOY" select e.keyName).ToList();
            List<string> targetType = (from e in db.keyValueData where e.relatedKeyType == "targetType" select e.keyName).Distinct().ToList();

            List<KeyValueData> key = null;
            List<string> salesHorizontals = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" select e.keyValue).ToList();
            List<string> performanceComponent = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesComponent" select e.keyValue).ToList();
            List<string> businessVertical = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "businessVertical" select e.keyValue).ToList();


            // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
            foreach (string str in salesHorizontals)
            {
                excelColumn.Add(str);
                foreach (string emp in employeeComponent)
                {
                    excelColumn.Add(emp);
                }
            }

            // Adding All Customer Attributes in excel 
            foreach (string cust in customerComponent)
            {
                excelColumn.Add(cust);

            }

            // Collecting all performance data based on BG
            if (businessVertical.Count != 0)
            {
                foreach (string perform in performanceComponent)
                {
                    foreach (string BV in businessVertical)
                    {
                        foreach (string mon in months)
                        {
                            performance.Add(BV + " " + perform + " " + mon + " " + year.ToString());

                        }

                    }

                }
            }
            else
            {
                foreach (string perform in performanceComponent)
                {

                    foreach (string mon in months)
                    {
                        performance.Add(perform + " " + mon + " " + year.ToString());

                    }

                }
            }

            //Adding all performance data in excel
            foreach (string perform in performance)
            {
                excelColumn.Add(perform);

            }



            // creating Column Id Row
            int colInExcel = excelColumn.Count();
            for (int si = 1; si <= colInExcel; si++)
            {
                columnId.Add(si.ToString());
            }


            // Converting Integers to their repective Alphabet
            foreach (string str in columnId)
            {
                int l = Convert.ToInt32(str) - 1;
                string toAlpha = toAlphabetic(l);
                columnsRow.Add(toAlpha);
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    metas = metas,
                    months = months,
                    targetType = targetType,

                    monthsList = monthsList,
                    employeeComponent = employeeComponent,
                    customerComponent = customerComponent,
                    employeeList = employeeList,
                    customerList = customerList,
                    excelColumn = excelColumn,
                    performanceComponent = performanceComponent,
                    columnsId = columnsRow,
                    currentYear = currentYear,
                    businessVertical = businessVertical,
                    salesHorizontals = salesHorizontals
                    //aa = cc

                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public JsonResult addPerformance()
        {
            Stream req = Request.InputStream;

            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> BGperformance = new List<string>();
            List<string> BUperformance = new List<string>();
            List<string> columnList = new List<string>();
            foreach (string performanceValue in data.performanceList)
            {
                foreach (string target in data.BGtargetList)
                {
                    foreach (string month in data.monthList)
                    {
                        BGperformance.Add(performanceValue + " " + target + " " + month + " 2018");
                    }
                }
            }
            foreach (string performanceValue in data.performanceList)
            {
                foreach (string target in data.BUtargetList)
                {
                    foreach (string month in data.monthList)
                    {
                        BUperformance.Add(performanceValue + " " + target + " " + month + " 2018");
                    }
                }
            }
            foreach (string datum in BGperformance)
            {
                columnList.Add(datum);
            }
            foreach (string datum in BUperformance)
            {
                columnList.Add(datum);
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columnList = columnList
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMetaDataFields()
        {
            Stream req = Request.InputStream;
            KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            int page = 0;
            string message = null;
            List<string> fields = (from e in db.keyValueData where e.keyName == MetaDataValues.keyName && e.keyType == "MetaData" select e.keyValue).ToList();
            List<KeyValueData> key = null;
            var pageSize = 20;
            var skip = pageSize * page;

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    fields = fields,
                    id = MetaDataValues.id

                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult removeColumn()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            List<string> columnsId = new List<string>();
            int i = data.removeColumnId;
            foreach (string col in data.finalExcel)
            {
                columnsA.Add(col);
            }
            foreach (string col in data.columnsId)
            {
                columnsId.Add(col);
            }
            columnsA.Remove(columnsA[i]);
            int index = columnsId.Count() - 1;
            columnsId.Remove(columnsId[index]);
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columns = columnsA,
                    excelColumn = columnsA,
                    columnsId = columnsId
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult createTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            List<string> columnsA = new List<string>();
            List<string> finalExcel = new List<string>();
            List<string> roleList = new List<string>();
            List<string> roleList2 = new List<string>();
            List<string> mainRoleList = new List<string>();
            List<int> preference = new List<int>();
            List<KeyValueData> roleKeys = new List<KeyValueData>();
            string preferRoles = "";
            string C = "";
            List<KeyValueData> employeeComponent = (from e in db.keyValueData where e.keyType == "EMPLOYEE" && e.keyName == "COMPONENT" select e).ToList();
            List<KeyValueData> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" select e).ToList();
            string message = "";
            int Col = 0;
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));

            Dictionary<int, string> dictionary = new Dictionary<int, string>();

            Dictionary<string, string> relatedKeyValue = new Dictionary<string, string>();
            foreach (string bound in data.salesList)
            {
                string values = "";
                finalExcel.Add(bound);
                foreach (KeyValueData kd in employeeComponent)
                {
                    finalExcel.Add(kd.keyValue);
                    values = kd.keyValue + ",";
                    values.TrimEnd(',');
                }

                relatedKeyValue.Add(bound, values);
            }
            if (data.isChecked == 'Y')
            {

                foreach (KeyValueData kd in customerComponent)
                {
                    finalExcel.Add(kd.keyValue);
                }

            }

            foreach (string performance in data.performance)
            {
                finalExcel.Add(performance);
            }

            if (data == null)
            {
                message = "error";
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columns = columnsA,
                    finalExcel = finalExcel,
                    message = message,
                    relatedKeyValue = relatedKeyValue
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult generateOpTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            string endMonth, startMonth;
            endMonth = data.endMonth.ToString();
            startMonth = data.startMonth.ToString();
            string message = null;
            string userName = HttpContext.User.Identity.Name;
            //KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            if (endMonth == null || startMonth == null || endMonth == "" || startMonth == "" || startMonth == "0" || endMonth == "0")
            {
                message = "Please Select Start Month and End Month";
                var jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                        message = message

                    }
                };
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "OP" && e.keyName == "Template" select e).ToList().Where(t => Convert.ToInt32(t.endMonth) == Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.startMonth) == Convert.ToInt32(startMonth)).ToList();
                //Exist = (from e in db.keyValueData where e.keyType == "OP" && e.keyName == "Template" select e).ToList().ToList();
                List<string> previousColumnId = new List<string>();
                List<string> previousColumnsRow = new List<string>();
                List<string> previousExcelColumn = new List<string>();


                if (data.startMonth == 0 || data.endMonth == 0)
                {
                    message = "Please Select Start Month and End Month";
                    var jsonsuccess = new
                    {
                        rsBody = new
                        {
                            result = "success",
                            message = message

                        }
                    };
                    return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (data.startMonth > data.endMonth)
                    {
                        message = "Month Error";
                        var jsonsuccess = new
                        {
                            rsBody = new
                            {
                                result = "success",
                                message = message

                            }
                        };
                        return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                    }
                    else if (Exist.Count() != 0)
                    {
                        foreach (KeyValueData value in Exist)
                        {
                            db.keyValueData.Remove(value);
                            db.SaveChanges();
                        }
                        /*
                        foreach (KeyValueData value in Exist)
                        {
                            previousExcelColumn.Add(value.keyValue);
                        }
                        message = "OP Template for given period is already exist";
                        int colInExcel = Exist.Count();
                        for (int si = 1; si <= colInExcel; si++)
                        {
                            previousColumnId.Add(si.ToString());
                        }
                        foreach (string str in previousColumnId)
                        {
                            int l = Convert.ToInt32(str) - 1;
                            string toAlpha = toAlphabetic(l);
                            previousColumnsRow.Add(toAlpha);
                        }
                        var jsonsuccess = new
                        {
                            rsBody = new
                            {
                                result = "success",
                                message = message,
                                previousExcelColumn = previousExcelColumn,
                                previousColumnId = previousColumnsRow

                            }
                        };
                        return Json(jsonsuccess, JsonRequestBehavior.AllowGet);*/

                        List<string> excelColumn = new List<string>();
                        List<string> alphabets = new List<string>();
                        List<string> columnId = new List<string>();
                        List<string> columnsRow = new List<string>();
                        List<string> performance = new List<string>();


                        List<string> performanceComponent = new List<string>();

                        List<KeyValueData> monthsData3 = new List<KeyValueData>();
                        Dictionary<int, string> dictionary = new Dictionary<int, string>();
                        int currentMonth = DateTime.Now.Month;
                        List<KeyValueData> monthsList = new List<KeyValueData>();


                        List<KeyValueData> monthsData = (from a in db.keyValueData
                                                         where a.keyType == "MOY"
                                                         select a).ToList().Where(t => Convert.ToInt32(t.keyValue) <= Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.keyValue) >= Convert.ToInt32(startMonth)).ToList();


                        KeyValueData cfy = (from fy in db.keyValueData
                                            where fy.keyType.Equals("OPERATIONAL")
                                            && fy.keyName.Equals("CFY")
                                            select fy).FirstOrDefault();
                        int year = Convert.ToInt32(cfy.keyValue);

                        // Check Incentive Close or Not In Incentive Confirmation Table

                        string quarter = (from d in db.incentiveCalculationRequest
                                          where d.year.Equals(year)
                                          && d.status.Equals(Codes.InStatus.CLOSED.ToString())
                                          orderby d.quarter descending
                                          select d.quarter).FirstOrDefault();


                        if (quarter != null)
                        {
                            KeyValueData keyData = (from k in db.keyValueData
                                                    where k.keyType.Equals("QTR")
                                                    && k.keyName.Equals(quarter)
                                                    select k).FirstOrDefault();
                            int mon = Convert.ToInt32(keyData.relatedKeyValue);

                        }
                        else
                        {

                        }
                        List<string> performanceSelection = new List<string>();
                        List<string> frequency = new List<string>();
                        List<int> frValue = new List<int>();

                        int i = 0;
                        KeyValueData maxFrequency = (from e in db.keyValueData select e).FirstOrDefault();
                        string val;
                        foreach (string perform in data.performanceList)
                        {
                            int keyFK = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "SalesComponent" && e.keyValue == perform select e.id).FirstOrDefault();
                            //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                            frequency = (from e in db.templateData where e.keyType == "DefaultSalesComponentFrequency" && e.keyName == "salesComponent" && e.keyValueFK == keyFK && e.keyValue != null select e.keyValue).Distinct().ToList();

                            foreach (string fr in frequency)
                            {
                                int frTemp = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.keyValue == fr select e.preferenceValue).FirstOrDefault();
                                frValue.Add(frTemp);

                                int j = 0;
                            }
                            i = frValue.Max();
                            maxFrequency = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.preferenceValue == i select e).FirstOrDefault();
                            switch (maxFrequency.keyValue)
                            {
                                case "Week":
                                    val = "Week";
                                    int mondays = 0;
                                    //DateTime thisMonth = DateTime.Now.Month;
                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        int month = Convert.ToInt32(mon.keyValue);// DateTime.Now.Month;
                                        int annual = DateTime.Now.Year;
                                        int daysThisMonth = DateTime.DaysInMonth(annual, month);
                                        DateTime beginingOfThisMonth = new DateTime(annual, month, 1);
                                        for (int d = 0; d < daysThisMonth; d++)
                                            if (beginingOfThisMonth.AddDays(d).DayOfWeek == DayOfWeek.Monday)
                                                mondays++;
                                        int noofWeeks = mondays;
                                        for (int j = 1; j <= noofWeeks; j++)
                                        {

                                            performanceSelection.Add(perform + " " + val + "" + j + " " + mon.keyName + " " + year.ToString());
                                        }
                                        frValue.Clear();
                                        mondays = 0;
                                    }

                                    break;
                                case "Fortnight":
                                    val = "Fortnight";
                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        for (int j = 1; j <= 2; j++)
                                        {
                                            performanceSelection.Add(perform + " " + val + "" + j + " " + mon.keyName + " " + year.ToString());
                                        }
                                    }
                                    frValue.Clear();
                                    break;
                                case "Month":
                                    val = "Month";

                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        performanceSelection.Add(perform + " " + mon.keyName + " " + year.ToString());

                                    }

                                    frValue.Clear();
                                    break;

                                default:
                                    performanceSelection.Add(perform);
                                    frValue.Clear();
                                    break;
                            }
                        }
                        //performanceComponent.Clear();
                        foreach (string perform in performanceSelection)
                        {
                            performanceComponent.Add(perform);
                        }


                        // List<string> salesHorizontals = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" select e.keyValue).ToList();
                        List<string> salesHorizontals = new List<string>();
                        List<string> employeeComponent = (from e in db.keyValueData where e.keyType == "EMPLOYEE" && e.keyName == "COMPONENT" select e.keyValue).ToList();
                        List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" select e.keyValue).ToList();
                        List<string> businessVertical = new List<string>();
                        foreach (string BV in data.businessVerticalList)
                        {
                            businessVertical.Add(BV);
                        }

                        // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
                        foreach (string str in data.salesHorizontals)
                        {
                            excelColumn.Add(str);
                            foreach (string emp in employeeComponent)
                            {
                                excelColumn.Add(emp);
                            }
                        }

                        // Adding All Customer Attributes in excel 
                        foreach (string cust in customerComponent)
                        {
                            excelColumn.Add(cust);

                        }

                        // Collecting all performance data based on BG
                        foreach (string perform in performanceComponent)
                        {
                            foreach (string bv in businessVertical)
                            {
                                /* foreach (KeyValueData mon in monthsData)
                                 {
                                     performance.Add(bv + " " + perform + " " + year.ToString());

                                 }*/
                                performance.Add(bv + " " + perform);
                            }

                        }


                        //Adding all performance data in excel
                        foreach (string perform in performance)
                        {
                            excelColumn.Add(perform);

                        }

                        // creating Column Id Row
                        int colInExcel = excelColumn.Count();
                        for (int si = 1; si <= colInExcel; si++)
                        {
                            columnId.Add(si.ToString());
                        }


                        // Converting Integers to their repective Alphabet
                        foreach (string str in columnId)
                        {
                            int l = Convert.ToInt32(str) - 1;
                            string toAlpha = toAlphabetic(l);
                            columnsRow.Add(toAlpha);
                        }


                        var jsonsuccess = new
                        {
                            rsBody = new
                            {
                                result = "success",
                                //BUType = BUType,
                                monthsList = monthsList,
                                employeeComponent = employeeComponent,
                                customerComponent = customerComponent,
                                columnsId = columnsRow,
                                excelColumn = excelColumn,
                                finalExcel = excelColumn,

                            }
                        };
                        return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {


                        List<string> excelColumn = new List<string>();
                        List<string> alphabets = new List<string>();
                        List<string> columnId = new List<string>();
                        List<string> columnsRow = new List<string>();
                        List<string> performance = new List<string>();


                        List<string> performanceComponent = new List<string>();

                        List<KeyValueData> monthsData3 = new List<KeyValueData>();
                        Dictionary<int, string> dictionary = new Dictionary<int, string>();
                        int currentMonth = DateTime.Now.Month;
                        List<KeyValueData> monthsList = new List<KeyValueData>();


                        List<KeyValueData> monthsData = (from a in db.keyValueData
                                                         where a.keyType == "MOY"
                                                         select a).ToList().Where(t => Convert.ToInt32(t.keyValue) <= Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.keyValue) >= Convert.ToInt32(startMonth)).ToList();


                        KeyValueData cfy = (from fy in db.keyValueData
                                            where fy.keyType.Equals("OPERATIONAL")
                                            && fy.keyName.Equals("CFY")
                                            select fy).FirstOrDefault();
                        int year = Convert.ToInt32(cfy.keyValue);

                        // Check Incentive Close or Not In Incentive Confirmation Table
                        /*
                        string quarter = (from d in db.incentiveCalculationRequest
                                          where d.year.Equals(year)
                                          && d.status.Equals(Codes.InStatus.CLOSED.ToString())
                                          orderby d.quarter descending
                                          select d.quarter).FirstOrDefault();


                        if (quarter != null)
                        {
                            KeyValueData keyData = (from k in db.keyValueData
                                                    where k.keyType.Equals("QTR")
                                                    && k.keyName.Equals(quarter)
                                                    select k).FirstOrDefault();
                            int mon = Convert.ToInt32(keyData.relatedKeyValue);

                        }
                        else
                        {

                        }*/
                        List<string> performanceSelection = new List<string>();
                        List<string> frequency = new List<string>();
                        List<int> frValue = new List<int>();

                        int i = 0;
                        KeyValueData maxFrequency = (from e in db.keyValueData select e).FirstOrDefault();
                        string val;
                        foreach (string perform in data.performanceList)
                        {
                            //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                            int keyFK = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "SalesComponent" && e.keyValue == perform select e.id).FirstOrDefault();
                            //frequency = (from e in db.keyValueDataLink where e.keyName == "salesComponent" && e.keyValue == perform && e.uploadFrequency != null select e.uploadFrequency).Distinct().ToList();
                            frequency = (from e in db.templateData where e.keyType == "DefaultSalesComponentFrequency" && e.keyName == "salesComponent" && e.keyValueFK == keyFK && e.keyValue != null select e.keyValue).Distinct().ToList();

                            foreach (string fr in frequency)
                            {
                                int frTemp = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.keyValue == fr select e.preferenceValue).FirstOrDefault();
                                frValue.Add(frTemp);

                                int j = 0;
                            }
                            i = frValue.Max();
                            maxFrequency = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.preferenceValue == i select e).FirstOrDefault();
                            switch (maxFrequency.keyValue)
                            {
                                case "Week":
                                    val = "Week";
                                    int mondays = 0;
                                    //DateTime thisMonth = DateTime.Now.Month;
                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        int month = Convert.ToInt32(mon.keyValue);// DateTime.Now.Month;
                                        int annual = DateTime.Now.Year;
                                        int daysThisMonth = DateTime.DaysInMonth(annual, month);
                                        DateTime beginingOfThisMonth = new DateTime(annual, month, 1);
                                        for (int d = 0; d < daysThisMonth; d++)
                                            if (beginingOfThisMonth.AddDays(d).DayOfWeek == DayOfWeek.Monday)
                                                mondays++;
                                        int noofWeeks = mondays;
                                        for (int j = 1; j <= noofWeeks; j++)
                                        {

                                            performanceSelection.Add(perform + " " + val + "" + j + " " + mon.keyName + " " + year.ToString());
                                        }
                                        frValue.Clear();
                                        mondays = 0;
                                    }

                                    break;
                                case "Fortnight":
                                    val = "Fortnight";
                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        for (int j = 1; j <= 2; j++)
                                        {
                                            performanceSelection.Add(perform + " " + val + "" + j + " " + mon.keyName + " " + year.ToString());
                                        }
                                    }
                                    frValue.Clear();
                                    break;
                                case "Month":
                                    val = "Month";

                                    foreach (KeyValueData mon in monthsData)
                                    {
                                        performanceSelection.Add(perform + " " + mon.keyName + " " + year.ToString());

                                    }

                                    frValue.Clear();
                                    break;

                                default:
                                    performanceSelection.Add(perform);
                                    frValue.Clear();
                                    break;
                            }
                        }
                        //performanceComponent.Clear();
                        foreach (string perform in performanceSelection)
                        {
                            performanceComponent.Add(perform);
                        }


                        // List<string> salesHorizontals = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" select e.keyValue).ToList();
                        List<string> salesHorizontals = new List<string>();
                        List<string> employeeComponent = (from e in db.keyValueData where e.keyType == "EMPLOYEE" && e.keyName == "COMPONENT" select e.keyValue).ToList();
                        List<string> customerComponent = (from e in db.keyValueData where e.keyType == "CUSTOMER" && e.keyName == "COMPONENT" select e.keyValue).ToList();
                        List<string> businessVertical = new List<string>();
                        foreach (string BV in data.businessVerticalList)
                        {
                            businessVertical.Add(BV);
                        }

                        // Adding All Sales Hoizontals along with their respective Employee Attributes in excel 
                        foreach (string str in data.salesHorizontals)
                        {
                            excelColumn.Add(str);
                            foreach (string emp in employeeComponent)
                            {
                                excelColumn.Add(emp);
                            }
                        }

                        // Adding All Customer Attributes in excel 
                        foreach (string cust in customerComponent)
                        {
                            excelColumn.Add(cust);

                        }

                        // Collecting all performance data based on BG
                        foreach (string perform in performanceComponent)
                        {
                            foreach (string bv in businessVertical)
                            {
                                /* foreach (KeyValueData mon in monthsData)
                                 {
                                     performance.Add(bv + " " + perform + " " + year.ToString());

                                 }*/
                                performance.Add(bv + " " + perform);
                            }

                        }


                        //Adding all performance data in excel
                        foreach (string perform in performance)
                        {
                            excelColumn.Add(perform);

                        }

                        // creating Column Id Row
                        int colInExcel = excelColumn.Count();
                        for (int si = 1; si <= colInExcel; si++)
                        {
                            columnId.Add(si.ToString());
                        }


                        // Converting Integers to their repective Alphabet
                        foreach (string str in columnId)
                        {
                            int l = Convert.ToInt32(str) - 1;
                            string toAlpha = toAlphabetic(l);
                            columnsRow.Add(toAlpha);
                        }


                        var jsonsuccess = new
                        {
                            rsBody = new
                            {
                                result = "success",
                                //BUType = BUType,
                                monthsList = monthsList,
                                employeeComponent = employeeComponent,
                                customerComponent = customerComponent,
                                columnsId = columnsRow,
                                excelColumn = excelColumn,
                                finalExcel = excelColumn,

                            }
                        };
                        return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
                    }
                }
            }


        }

        public JsonResult downloadTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            string filepath = "";
            string fileName = "";
            string C = "COLUMN";
            string CX = "COLUMN";
            string message = "";
            string msg = "";
            int Col = 0;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            string startMonth = data.startMonth.ToString();
            string endMonth = data.endMonth.ToString();
            List<string> columnsA = new List<string>();
            List<string> columnsB = new List<string>();
            List<char> characters = new List<char>();
            string TemplatePath = (from e in db.dataUploadBatchParameters where e.uploadType == "Template" select e.fileUploadFolder).FirstOrDefault();
            KeyValueData quarter = (from e in db.keyValueData where e.keyType == "QTR" && e.keyName == "Q4" select e).FirstOrDefault();
            //List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "OP" && e.keyName == "Template" select e).ToList();
            List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "OP" && e.keyName == "Template" && e.relatedKeyValue == user.operationalUnit select e).ToList().Where(t => Convert.ToInt32(t.endMonth) == Convert.ToInt32(endMonth)).Where(t => Convert.ToInt32(t.startMonth) == Convert.ToInt32(startMonth)).ToList();
            //List<KeyValueData> Exist = (from e in db.keyValueData where e.keyType == "OP" && e.keyName == "Template" select e).ToList().Where(t => Convert.ToInt32(t.endMonth) == Convert.ToInt32(quarter.relatedKeyType)).Where(t => Convert.ToInt32(t.startMonth) == Convert.ToInt32(quarter.relatedKeyValue)).ToList();
            if (Exist.Count != 0)
            {
                foreach (KeyValueData j in Exist)
                {
                    db.keyValueData.Remove(j);
                    db.SaveChanges();
                }
            }
            for (int f = 1; f <= data.columnList.Count; f++)
            {
                Col = f;
                string col = "COLUMN " + Col.ToString();
                columnsA.Add(col.ToString());
            }

            int k = columnsA.Count() - 1;
            for (int i = 0; i < data.columnList.Count; i++)
            {
                columnsA[i] = data.columnList[i];
            }
            for (int i = 0; i < columnsA.Count; i++)
            {
                string firstId = columnsA[i].ToString();
                string secondId = "";
                string val = columnsA[i];

            }
            if (msg == "duplicate")
            {
                message = message;
                msg = msg;
            }
            else
            {

                string AppLocation = "";
                AppLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                AppLocation = AppLocation.Replace("file:\\", "");
                string date = DateTime.Now.ToShortDateString();
                date = date.Replace("/", "_");
                //filepath = AppLocation + "\\ExcelFiles\\" + "Template_" + "OP_" + date + "_" + user.operationalUnit.ToUpper() + ".xlsx";
                filepath = TemplatePath + "\\ExcelFiles\\" + "Template_" + "OP_" + date + ".xlsx";
                fileName = "Template_" + "OP_" + date + ".xlsx";
                var workbook = new XLWorkbook();
                workbook.AddWorksheet("sheetName");
                var ws = workbook.Worksheet("sheetName");

                for (int i = 0; i < columnsA.Count; i++)
                {
                    string x = columnsA[i];
                    ws.Cell(1, i + 1).Value = x;

                    ws.Cell(1, i + 1).Style.Fill.BackgroundColor = XLColor.Yellow;
                    ws.Cell(1, i + 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    ws.Cell(1, i + 1).Style.Font.FontColor = XLColor.Black;
                    ws.Cell(1, i + 1).Style.Border.BottomBorderColor = XLColor.Black;
                    ws.Cell(1, i + 1).Style.Border.RightBorderColor = XLColor.Black;
                    ws.Cell(1, i + 1).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    ws.Cell(1, i + 1).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    ws.Column(i + 1).AdjustToContents();
                }

                workbook.SaveAs(filepath);


            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    columns = columnsA,
                    message = message,
                    msg = msg,
                    filepath = filepath,
                    fileName = fileName

                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult insertOPTemplate()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            string employeeValues = null;// "Name" + "," + "Email ID" + "," + "Adp ID" + "," + "Role" + "," + "Grade";
            List<string> empComp = (from e in db.keyValueData where e.keyType == "Employee" && e.keyName == "Component" select e.keyValue).ToList();
            foreach (string s in empComp)
            {
                employeeValues = employeeValues + "," + s;
            }
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            List<string> columnsA = new List<string>();
            List<string> salesBoundaries = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == "salesHorizontal" select e.keyValue).ToList();
            KeyValueData newValues = new KeyValueData();
            TemplateData newTemplateValues = new TemplateData();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string CurrentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            bool flag = false;
            int preferenceSerial = 0;
            int Serial = 0;
            //string relatedKeyType = "employeeComponent";
            string relatedKeyValue = "";
            int startMonth = data.startMonth;
            int endMonth = data.endMonth;
            //Important Don't delete
            /*
             * foreach (var item in data.relatedKeyValue)
            {
                string v = item.Value;
                string k = item.Name;
                dictionary.Add(k,v);
            }*/

            string tags = data.keyValue;
            columnsA = tags.Split(',').ToList();


            foreach (string keys in columnsA)
            {

                if (keys != "")
                {
                    if (salesBoundaries.Contains(keys))
                    {
                        flag = true;
                        if (flag)
                        {
                            preferenceSerial = (from e in db.keyValueData
                                                where e.keyType == "MetaData"
                                                && e.keyName == "salesHorizontal"
                                                && e.keyValue == keys
                                                select e.preferenceValue).FirstOrDefault();
                            newValues.keyName = data.keyName;
                            newValues.keyType = data.keyType;
                            newValues.keyValue = keys;
                            newValues.preferenceValue = Serial;
                            newValues.relatedKeyType = "MetaDataComponent";
                            newValues.relatedKeyValue = user.operationalUnit;
                            newValues.preferenceSerial = preferenceSerial;
                            newValues.startMonth = startMonth;
                            newValues.endMonth = endMonth;
                            newValues.description = data.keyType + " " + data.keyName;
                            newValues.year = Convert.ToInt32(CurrentYear);
                            db.keyValueData.Add(newValues);
                            db.SaveChanges();

                            newTemplateValues.keyName = data.keyName;
                            newTemplateValues.keyType = data.keyType;
                            newTemplateValues.keyValue = keys;
                            newTemplateValues.preferenceValue = Serial;
                            newTemplateValues.relatedKeyType = "MetaDataComponent";
                            newTemplateValues.preferenceSerial = preferenceSerial;
                            newTemplateValues.startMonth = startMonth;
                            newTemplateValues.endMonth = endMonth;
                            newTemplateValues.description = data.keyType + " " + data.keyName;
                            newTemplateValues.year = Convert.ToInt32(CurrentYear);
                            //db.TemplateDataTable.Add(newTemplateValues);
                            db.SaveChanges();
                        }


                    }
                    else
                    {

                        if (flag)
                        {

                            newValues.keyName = data.keyName;
                            newValues.keyType = data.keyType;
                            newValues.keyValue = keys;
                            newValues.preferenceSerial = preferenceSerial;
                            if (employeeValues.Contains(keys))
                            {
                                newValues.relatedKeyType = "employeeComponent";// relatedKeyType;
                            }
                            if (keys.Contains("Customer"))
                            {
                                newValues.preferenceSerial = 0;
                                flag = false;
                                preferenceSerial = 0;
                                newValues.relatedKeyType = "customerComponent";
                            }
                            newValues.startMonth = startMonth;
                            newValues.endMonth = endMonth;
                            newValues.preferenceValue = Serial;
                            newValues.year = Convert.ToInt32(CurrentYear);
                            newValues.description = data.keyType + " " + data.keyName;
                            db.keyValueData.Add(newValues);
                            db.SaveChanges();

                            newTemplateValues.keyName = data.keyName;
                            newTemplateValues.keyType = data.keyType;
                            newTemplateValues.keyValue = keys;
                            newTemplateValues.preferenceSerial = preferenceSerial;
                            if (employeeValues.Contains(keys))
                            {
                                newTemplateValues.relatedKeyType = "employeeComponent";// relatedKeyType;
                            }
                            if (keys.Contains("Customer"))
                            {
                                newTemplateValues.preferenceSerial = 0;
                                flag = false;
                                preferenceSerial = 0;
                                newTemplateValues.relatedKeyType = "customerComponent";
                            }
                            newTemplateValues.startMonth = startMonth;
                            newTemplateValues.endMonth = endMonth;
                            newTemplateValues.year = Convert.ToInt32(CurrentYear);
                            newTemplateValues.preferenceValue = Serial;
                            newTemplateValues.description = data.keyType + " " + data.keyName;
                            //db.TemplateDataTable.Add(newTemplateValues);
                            db.SaveChanges();


                        }
                        else
                        {
                            flag = false;
                            newValues.keyName = data.keyName;
                            newValues.keyType = data.keyType;
                            newValues.keyValue = keys;
                            newValues.preferenceSerial = preferenceSerial;
                            if (keys.Contains("Customer"))
                            {
                                newValues.relatedKeyType = "customerComponent";
                            }
                            else { newValues.relatedKeyType = "PerformanceValues"; }
                            newValues.startMonth = startMonth;
                            newValues.endMonth = endMonth;
                            newValues.preferenceValue = Serial;
                            newValues.year = Convert.ToInt32(CurrentYear);
                            newValues.description = data.keyType + " " + data.keyName;
                            db.keyValueData.Add(newValues);
                            db.SaveChanges();

                            newTemplateValues.keyName = data.keyName;
                            newTemplateValues.keyType = data.keyType;
                            newTemplateValues.keyValue = keys;
                            newTemplateValues.preferenceSerial = preferenceSerial;
                            if (keys.Contains("Customer"))
                            {
                                newTemplateValues.relatedKeyType = "customerComponent";
                            }
                            else { newTemplateValues.relatedKeyType = "PerformanceValues"; }
                            newTemplateValues.startMonth = startMonth;
                            newTemplateValues.endMonth = endMonth;
                            newTemplateValues.preferenceValue = Serial;
                            newTemplateValues.year = Convert.ToInt32(CurrentYear);
                            newTemplateValues.description = data.keyType + " " + data.keyName;
                            //db.TemplateDataTable.Add(newTemplateValues);
                            db.SaveChanges();
                        }
                    }
                    Serial++;
                }
            }

            List<string> y = columnsA.ToList<string>();
            y.RemoveAll(p => string.IsNullOrEmpty(p));
            columnsA = y.ToList();


            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    excelColumn = columnsA
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult addemployeeComponent()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            KeyValueData empComponent = new KeyValueData();
            string emp = data;
            string employeeList = "";
            string keyType = "EMPLOYEE";
            string keyName = "COMPONENT";
            empComponent.keyName = keyName;
            empComponent.keyType = keyType;
            empComponent.keyValue = emp;
            empComponent.preferenceValue = 0;
            empComponent.preferenceSerial = 0;
            empComponent.startMonth = 0;
            empComponent.endMonth = 0;
            empComponent.description = "Attribute of employee";
            db.keyValueData.Add(empComponent);
            db.SaveChanges();
            List<string> employeeComponent = (from e in db.keyValueData where e.keyName == keyName && e.keyType == keyType select e.keyValue).ToList();
            foreach (string str in employeeComponent)
            {

                employeeList = employeeList + " " + str + "-";
                employeeList.TrimEnd('-');
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    //columns = columnsA,
                    //excelColumn = columnsAe
                    employeeComponent = employeeComponent,
                    employeeList = employeeList
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }


        public JsonResult addcustomerComponent()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            List<string> columnsA = new List<string>();
            KeyValueData custometComponent = new KeyValueData();
            string emp = data;
            string customerList = "";
            string keyType = "CUSTOMER";
            string keyName = "COMPONENT";
            custometComponent.keyName = keyName;
            custometComponent.keyType = keyType;
            custometComponent.keyValue = emp;
            custometComponent.description = "Attribute of customer";
            db.keyValueData.Add(custometComponent);
            db.SaveChanges();
            List<string> customerComponent = (from e in db.keyValueData where e.keyName == keyName && e.keyType == keyType select e.keyValue).ToList();
            foreach (string str in customerComponent)
            {

                customerList = customerList + " " + str + "-";
                customerList.TrimEnd('-');
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    customerComponent = customerComponent,
                    customerList = customerList
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }
    }
}