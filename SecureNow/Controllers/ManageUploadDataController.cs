﻿using SecureNow.AuthFilter;
using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Mvc;


namespace SecureNow.Controllers
{
    [UserAuthenticationFilter]
    public class ManageUploadDataController : Controller
    {
        public static DBModels db = new DBModels();

        public JsonResult SubmitActualRequest()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            ActualRequest actualData = new ActualRequest();
            actualData.amount =  actualRequestDetails.amount;
            actualData.month = Convert.ToInt32(actualRequestDetails.month);
            actualData.bg = actualRequestDetails.bg;
            actualData.year = actualRequestDetails.year;
            actualData.dateTime = DateTime.Now.Ticks;
            actualData.status = Codes.status.WAITING_FOR_APPROVAL.ToString();         
            if(actualRequestDetails.type=="self")
            {
                actualData.requestRaisedByAdpId = user.adpId;
                actualData.requestRaisedForAdpId = user.adpId;
            }
            else
            {
                actualData.requestRaisedByAdpId = user.adpId;
                actualData.requestRaisedForAdpId = actualRequestDetails.adpId;
            }
            actualData.type = actualRequestDetails.type;
            db.actualRequest.Add(actualData);
            db.SaveChanges();

                      
            ////Now get  approver of actual request data////   
            EmployeeRole requiredParam = (from u in db.employeeRole where u.adpId == user.adpId select u).FirstOrDefault();

            int id = (from i in db.keyValueData
                    where i.keyType == Codes.keyValue.TeamSetup.ToString()
                    && i.keyName == Codes.keyValue.MetaData.ToString() && i.keyValue == requiredParam.RP1
                    select i.id).FirstOrDefault();

            List<string> salesHorizontalList = (from i in db.keyValueData
                                                where i.keyType == Codes.keyValue.MetaData.ToString()
                                                 && i.keyName == Codes.keyValue.SalesHorizontal.ToString()
                                                orderby i.preferenceValue descending
                                                select i.keyValue).ToList();

            //get existing role sequence///
            int getSequence = (from u in db.keyValueDataLink
                                             where u.keyValueFK == id && u.keyValue == requiredParam.role
                                             select u.attrSequence).FirstOrDefault();

            ///now getting required saleshorizontal for approver///

            KeyValueDataLink approverD = (from u in db.keyValueDataLink
                               where u.keyValueFK == id && u.attrSequence == getSequence-1
                               select u).FirstOrDefault();

           
             string adpId = (from u in db.employeeRole
                                    where u.role == approverD.keyValue &&
                                     u.RP1 == requiredParam.RP1
                                     && u.RP2 == requiredParam.RP2
                                    select u.adpId).FirstOrDefault();
               
                ActualApprovalRequest approvalReq = new ActualApprovalRequest();
                approvalReq.actualRequestIdFK = actualData.id;
                approvalReq.approverAdpId = adpId;
                approvalReq.dateTime = DateTime.Now.Ticks;
                approvalReq.status = Codes.status.WAITING_FOR_APPROVAL.ToString();
                db.actualApprovalRequest.Add(approvalReq);
                db.SaveChanges();

           


            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = Codes.message.Success.ToString()
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActualRequestList()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));

            string userName = HttpContext.User.Identity.Name;

            Boolean search = false;
            EmployeeBaseDetails user = new EmployeeBaseDetails();

            EmployeeRole userRole = new EmployeeRole();
            string approverId = "";
            if(actualRequestDetails.adpId==null)
            {
                user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
                userRole = (from e in db.employeeRole where e.adpId == user.adpId select e).FirstOrDefault();
            }
            else
            {
                user.adpId = actualRequestDetails.adpId;
                approverId =  (from e in db.employeeBaseDetails where e.email == userName select e.adpId).FirstOrDefault();
                search = true;
            }

            List<UploadActualDTO> listData = new List<UploadActualDTO>();
            if (search == false)
            {

                if (userRole.role == "SE")
                {
                    listData = (from u in db.actualRequest
                                join e in db.actualApprovalRequest
                                on u.id equals e.actualRequestIdFK
                                where u.requestRaisedByAdpId == user.adpId
                                select new UploadActualDTO
                                {
                                    year = u.year,
                                    id = u.id,
                                    month = u.month.ToString(),
                                    amount = u.amount,
                                    bg = u.bg,
                                    status = u.status,
                                    approverAdpId = e.approverAdpId,
                                    requestRaisedByAdpId = u.requestRaisedByAdpId,
                                    requestRaisedForAdpId = u.requestRaisedForAdpId

                                }).ToList();
                }
                else
                {
                    listData = (from u in db.actualRequest
                                join e in db.actualApprovalRequest
                                on u.id equals e.actualRequestIdFK
                                where e.approverAdpId == user.adpId
                                select new UploadActualDTO
                                {
                                    year = u.year,
                                    id = u.id,
                                    month = u.month.ToString(),
                                    amount = u.amount,
                                    bg = u.bg,
                                    status = u.status,
                                    approverAdpId = e.approverAdpId,
                                    requestRaisedByAdpId = u.requestRaisedByAdpId,
                                    requestRaisedForAdpId = u.requestRaisedForAdpId

                                }).ToList();
                }
            }
            else
            {
                if (userRole.role == "SE")
                {
                    listData = (from u in db.actualRequest
                                join e in db.actualApprovalRequest
                                on u.id equals e.actualRequestIdFK
                                where u.requestRaisedByAdpId.Contains(user.adpId)
                                select new UploadActualDTO
                                {
                                    year = u.year,
                                    id = u.id,
                                    month = u.month.ToString(),
                                    amount = u.amount,
                                    bg = u.bg,
                                    status = u.status,
                                    approverAdpId = e.approverAdpId,
                                    requestRaisedByAdpId = u.requestRaisedByAdpId,
                                    requestRaisedForAdpId = u.requestRaisedForAdpId

                                }).ToList();
                }
                else
                {
                    listData = (from u in db.actualRequest
                                join e in db.actualApprovalRequest
                                on u.id equals e.actualRequestIdFK
                                where (u.requestRaisedByAdpId.Contains(user.adpId) && e.approverAdpId == approverId) 
                                select new UploadActualDTO
                                {
                                    year = u.year,
                                    id = u.id,
                                    month = u.month.ToString(),
                                    amount = u.amount,
                                    bg = u.bg,
                                    status = u.status,
                                    approverAdpId = e.approverAdpId,
                                    requestRaisedByAdpId = u.requestRaisedByAdpId,
                                    requestRaisedForAdpId = u.requestRaisedForAdpId

                                }).ToList();
                }
            }

            List<UploadActualDTO> finaData = new List<UploadActualDTO>();


            foreach (UploadActualDTO u in listData)
            {

                u.requestRaisedByName = (from i in db.employeeBaseDetails
                                   where i.adpId == u.requestRaisedByAdpId
                                   select i.firstName).FirstOrDefault();
                u.requestRaisedForName = (from i in db.employeeBaseDetails
                                         where i.adpId == u.requestRaisedForAdpId
                                         select i.firstName).FirstOrDefault();
                u.approverName = (from i in db.employeeBaseDetails
                                          where i.adpId == u.approverAdpId
                                          select i.firstName).FirstOrDefault();

                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                u.status = u.status.Replace('_',' ');
                u.status = textInfo.ToTitleCase(u.status);
                u.month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(u.month));
                finaData.Add(u);
            }
            Boolean showButton = false;
            if (userRole.role != "SE")
            {
                showButton = true;
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = finaData,
                    showButton = showButton

                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApproveActualRequest()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));

            string userName = HttpContext.User.Identity.Name;

            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();

            ActualRequest data = (from i in db.actualRequest
                                  where i.id == actualRequestDetails.id
                                  select i).FirstOrDefault();

            data.status = Codes.status.APPROVED.ToString();
            db.SaveChanges();

            ActualApprovalRequest d = (from o in db.actualApprovalRequest where o.actualRequestIdFK == data.id select o).FirstOrDefault();
            d.status = Codes.status.APPROVED.ToString();
            db.SaveChanges();

            //save actuals in actual data
            EmployeeRole userParam = (from e in db.employeeRole where e.adpId == data.requestRaisedForAdpId select e).FirstOrDefault();

            KeyValueData keyData = (from i in db.keyValueData where i.keyType == Codes.keyValue.TeamSetup.ToString()
                                    && i.keyValue == userParam.RP1 select i).FirstOrDefault();

           var horizontalCount = (from i in db.keyValueDataLink where i.keyValueFK == keyData.id && i.keyValue == userParam.role
                                         select i).Count();
           
            YTDActualData actualData = new YTDActualData();
            actualData.month = data.month;
            actualData.bg = data.bg;
            actualData.value = data.amount;
            actualData.year = data.year;
            switch(horizontalCount)
            {
                case 1:
                    actualData.code = userParam.RP1;
                    break;
                case 2:
                    actualData.code = userParam.RP2;
                    break;
                case 3:
                    actualData.code = userParam.RP3;
                    break;
                case 4:
                    actualData.code = userParam.RP4;
                    break;
                case 5:
                    actualData.code = userParam.RP5;
                    break;


            }
            db.YTDActualData.Add(actualData);
            db.SaveChanges();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = Codes.status.SUCCESS.ToString()
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RejectActualRequest()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));

            string userName = HttpContext.User.Identity.Name;

            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();

            ActualRequest data = (from i in db.actualRequest where i.id == actualRequestDetails.id
                                  select i).FirstOrDefault();

            data.status = Codes.status.REJECTED.ToString();
            db.SaveChanges();

            ActualApprovalRequest d = (from o in db.actualApprovalRequest where o.actualRequestIdFK == data.id select o).FirstOrDefault();
            d.status = Codes.status.REJECTED.ToString();
            db.SaveChanges();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = Codes.status.SUCCESS.ToString()
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetEmployeeList()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));
            string userName = HttpContext.User.Identity.Name;
            List<EmployeeBaseDetails> userList = (from e in db.employeeBaseDetails select e).ToList();
            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = userList
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductList()
        {
            Stream req = Request.InputStream;
            KeyValueData keyType = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));


            List<string> GetBusinessVerticalList = (from u in db.keyValueData
                                                    where u.keyType == Codes.keyValue.MetaData.ToString()
                                                    && u.keyName == Codes.keyValue.BusinessVertical.ToString()
                                                    select u.keyValue).ToList();

            List<KeyValueData> productList = new List<KeyValueData>();
            foreach(string u in GetBusinessVerticalList)
            {
                 productList = (from i in db.keyValueData
                            where i.keyType == u
                            && i.keyName == Codes.keyValue.BusinessVertical.ToString()
                            select i).ToList();
            }

            
            
            
            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = productList
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CancelUploadRequest()
        {
            Stream req = Request.InputStream;
            EmployeeMasterUploadDTO employeeMasterList = (EmployeeMasterUploadDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeMasterUploadDTO));
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
          
            dynamic jsonsuccess = null;
            if (employeeMasterList != null)
            {
                DataUploadBatchRequests request = (from d in db.dataUploadBatchRequests where d.id == employeeMasterList.requestId && d.requestType.Equals(employeeMasterList.requestType) select d).FirstOrDefault();
                if (request != null)
                {
                    if (request.status.Equals(Codes.status.SUCCESS.ToString()) || request.status.Equals(Codes.status.PROCESSING.ToString()))
                    {
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                msg = "Request can not be cancelled",
                                key = "error"
                            }
                        };
                    }
                    else
                    {
                        request.status = Codes.status.CANCELLED.ToString();
                        db.SaveChanges();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                msg = "Request cancelled successfully",
                                key = "success"
                            }
                        };
                    }
                }
            }

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue

            };
        }

    }
}




