﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TreeNode = SecureNow.Container.TreeNode;

namespace SecureNow.Controllers
{
    public class ManageMetaDataController : Controller
    {
        //public static DBModels db = DBModels.dynamicDB;
        public DBModels db = new DBModels();

        public JsonResult getEmployeeAttributes()
        {

            IEnumerable<CustomerBaseDetails> getDetail = (from e in db.customerBaseDetails select e).ToList();
            IQueryable<CustomerBaseDetails> getData = (from e in db.customerBaseDetails select e);


            Stream req = Request.InputStream;
            SearchMetaDataDTO keyvalue = (SearchMetaDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(SearchMetaDataDTO));
            object jsonsuccess = null;
            List<KeyValueData> attributesList = new List<KeyValueData>();
            string currentYear = (from e in db.keyValueData where e.keyType == "OPERATIONAL" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();

           
           // data1(getUpperList, count, getDataInHeirarchical);

            switch (keyvalue.currentTab)
            {
                case "TeamSetup":
                    attributesList = (from e in db.keyValueData where e.keyType.Equals("SalesTeam") && e.keyName.Equals(keyvalue.currentTab + " Level") select e).ToList();
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            attributesList = attributesList,
                            CFY = currentYear,
                            result = "success"
                        }
                    };
                    break;
                case "SalesHorizontal":
                case "OperationalUnit":
                case "BusinessVertical":


                    //Get OU List
                    KeyValueData getOUKey = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.keyName.Equals("OperationalUnit") && e.preferenceValue == 0 select e).FirstOrDefault();
                    List<KeyValueData> getOUList = (from e in db.keyValueData where e.keyType == getOUKey.keyValue && e.keyName == "OperationalUnit" select e).ToList();

                    //get parent child list
                    ParentChildNode children = new ParentChildNode();
                    List<ParentChildNode> data1 = new List<ParentChildNode>();
                    List<ParentChildNode> getDataInHeirarchical = new List<ParentChildNode>();
                    KeyValueData getSalesHorizontalList = new KeyValueData();
                    getSalesHorizontalList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == keyvalue.metaDataLevel && e.keyName.Equals(keyvalue.currentTab) select e).FirstOrDefault();
                    List<KeyValueData> getUpperList = new List<KeyValueData>();
                    if (getSalesHorizontalList != null)
                    {
                        getUpperList = (from e in db.keyValueData where e.keyType == getSalesHorizontalList.keyValue && e.keyName == keyvalue.currentTab select e).ToList();

                        int count = keyvalue.metaDataLevel;
                        children.name = getSalesHorizontalList.keyValue;
                        attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == keyvalue.metaDataLevel && e.keyName.Equals(keyvalue.currentTab) select e).ToList();

                      
                        ParentChildNode data11 = new ParentChildNode();
                        data11.preferenceValue = attributesList[0].preferenceValue;
                        data11.name = attributesList[0].keyValue;
                        data11.key = attributesList[0].keyName;
                        data11.children = getUpperHorizontals(attributesList[0]);
                        data1.Add(data11);
                        foreach (KeyValueData itm in getUpperList)
                        {
                            ParentChildNode data = new ParentChildNode();
                            string period = "(" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(itm.startMonth).Substring(0, 3) + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(itm.endMonth).Substring(0, 3) + "," + itm.year + ")";

                            data.name = itm.keyValue;
                            data.key = itm.keyType;
                            data.id = itm.id;
                            if(itm.startMonth != 1 || itm.endMonth != 12)
                            {
                                data.period = period;
                            }
                            else
                            {
                                data.period = "";
                            }
                            getSalesHorizontalList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceValue == itm.preferenceValue + 1 && e.keyName.Equals(itm.keyName) select e).FirstOrDefault();
                           
                            string dasds = itm.id.ToString();
                            List<AttributesLink> getList = (from e in db.attributesLink where e.rootId == itm.id && e.attributeSequence == dasds select e).ToList();
                            if (getSalesHorizontalList != null)
                            {
                                data.children = ParentChildSequence(getSalesHorizontalList, getList, itm, dasds, true);
                            }
                            else
                            {
                                data.children = new List<ParentChildNode>();
                            }
                            getDataInHeirarchical.Add(data);
                           
                        }
                    }
                    children.children = getDataInHeirarchical;
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            attributesList = attributesList,
                            CFY = currentYear,
                            result = "success",
                            metaDataHeirarchy = children,
                            heirarchy = data1,
                            ouDataList = getOUList
                        }
                    };
                    break;                
                default:                    
                    attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == keyvalue.metaDataLevel && e.keyName.Equals(keyvalue.currentTab) select e).ToList();
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            attributesList = attributesList,
                            CFY = currentYear,
                            result = "success"
                        }
                    };
                    break;
            }

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReAssignMetaData()
        {
            Stream req = Request.InputStream;
            MetaDataConfigureDTO reAssignMetaData = (MetaDataConfigureDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataConfigureDTO));


            //Close first assign atrribute link

            AttributesLink attributeEnd = (from e in db.attributesLink where e.attributeSequence == reAssignMetaData.attributeSequence && e.childId == reAssignMetaData.childNodeDetail.id select e).FirstOrDefault();
            attributeEnd.endMonth = reAssignMetaData.startMonth - 1;
            db.SaveChanges();

            //Create new Attribute Link
            string attributeSequesnce = "";

            for (int i = 0; i < reAssignMetaData.preferenceValue; i++)
            {

                switch (i)
                {
                    case 0:
                        if (reAssignMetaData.level_0 != null && reAssignMetaData.level_0 != "")
                        {
                            attributeSequesnce = reAssignMetaData.level_0;
                        }
                        break;
                    case 1:
                        if (reAssignMetaData.level_1 != null && reAssignMetaData.level_1 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_1;
                        }
                        break;
                    case 2:
                        if (reAssignMetaData.level_2 != null && reAssignMetaData.level_2 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_2;
                        }
                        break;
                    case 3:
                        if (reAssignMetaData.level_3 != null && reAssignMetaData.level_3 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_3;
                        }
                        break;
                    case 4:
                        if (reAssignMetaData.level_4 != null && reAssignMetaData.level_4 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_4;
                        }
                        break;
                    case 5:
                        if (reAssignMetaData.level_5 != null && reAssignMetaData.level_5 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_5;
                        }
                        break;
                    case 6:
                        if (reAssignMetaData.level_6 != null && reAssignMetaData.level_6 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_6;
                        }
                        break;
                    case 7:
                        if (reAssignMetaData.level_7 != null && reAssignMetaData.level_7 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_7;
                        }
                        break;
                    case 8:
                        if (reAssignMetaData.level_8 != null && reAssignMetaData.level_8 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_8;
                        }
                        break;
                    case 9:
                        if (reAssignMetaData.level_9 != null && reAssignMetaData.level_9 != "")
                        {
                            attributeSequesnce = attributeSequesnce + ":" + reAssignMetaData.level_9;
                        }
                        break;
                }
            }

            List<string> attributeId = attributeSequesnce.Split(':').ToList();
            AttributesLink insertData = new AttributesLink();
            insertData.rootId = Convert.ToInt32(reAssignMetaData.level_0);
            insertData.parentId = Convert.ToInt32(attributeId[attributeId.Count() - 1]);
            insertData.startMonth = reAssignMetaData.startMonth;
            insertData.endMonth = reAssignMetaData.endMonth;
            insertData.year = reAssignMetaData.year;
            insertData.attributeSequence = attributeSequesnce;
            insertData.childId = reAssignMetaData.childNodeDetail.id;
            insertData.childlabel = reAssignMetaData.childNodeDetail.key;
            db.attributesLink.Add(insertData);
            db.SaveChanges();
            if (reAssignMetaData.heirarchyAssign == true)
            {
                //End All Hierarchy Endmonth
                string attributeSequence = reAssignMetaData.attributeSequence + ":" + reAssignMetaData.heirarchyNode.id;
                updateEndMonthOfChild(reAssignMetaData.heirarchyNode.children, attributeSequence, reAssignMetaData);

                //Insert New Assign Data
                attributeSequesnce = attributeSequesnce + ":" + insertData.childId;
                insertNewData(reAssignMetaData.heirarchyNode.children, attributeSequesnce, reAssignMetaData);
            }

            var jsonResult = new
            {
                rsBosy = new
                {
                    message = "success"
                }
            };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }


        public string insertNewData(List<ParentChildNode> childList, string sequence, MetaDataConfigureDTO childParentData)
        {
            string attSequence = "";
            foreach (ParentChildNode itm in childList)
            {
                //insert record
                List<string> attributeId = sequence.Split(':').ToList();
                AttributesLink insertData = new AttributesLink();
                insertData.rootId = Convert.ToInt32(childParentData.level_0);
                insertData.parentId = Convert.ToInt32(attributeId[attributeId.Count() - 1]);
                insertData.startMonth = childParentData.startMonth;
                insertData.endMonth = childParentData.endMonth;
                insertData.year = childParentData.year;
                insertData.attributeSequence = sequence;
                insertData.childId = itm.id;
                insertData.childlabel = itm.key;
                db.attributesLink.Add(insertData);
                db.SaveChanges();
                attSequence = sequence + ":" + itm.id;
                insertNewData(itm.children, attSequence, childParentData);
            }

            return attSequence;
        }


        public string updateEndMonthOfChild(List<ParentChildNode> childList,string sequence, MetaDataConfigureDTO childParentData)
        {
            string attSequence = "";
            foreach (ParentChildNode itm in childList)
            {
                 attSequence = sequence + ":" + itm.id;
                AttributesLink getData = (from e in db.attributesLink where e.attributeSequence == sequence && e.childId == itm.id select e).FirstOrDefault();
                getData.endMonth = childParentData.startMonth - 1;
                db.SaveChanges();
                updateEndMonthOfChild(itm.children,attSequence, childParentData);
            }

            return attSequence;
        }

        public JsonResult RemoveMetaDataKey()
        {
            Stream req = Request.InputStream;
            MetaDataConfigureDTO metaData = (MetaDataConfigureDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataConfigureDTO));
            KeyValueData getCurrentMetaData = (from e in db.keyValueData
                                               where e.keyType == "MetaData" && e.keyName == metaData.key
                                                && e.keyValue == metaData.keyName
                                               select e).FirstOrDefault();

            //Remove All Data
            List<KeyValueData> getAllBelowMetaData = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == metaData.key && e.preferenceValue > getCurrentMetaData.preferenceValue select e).ToList();
            db.keyValueData.RemoveRange(getAllBelowMetaData);
            db.keyValueData.Remove(getCurrentMetaData);
            db.SaveChanges();

            var jsonResult = new
            {
                rsBody = new
                {
                    message = "success"
                }
            };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public List<ParentChildNode> getUpperHorizontals(KeyValueData keyData)
        {
           List<ParentChildNode> getAllData = new List<ParentChildNode>();
           
            List<KeyValueData> attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == keyData.preferenceSerial && e.keyName.Equals(keyData.keyName)
                                                 && e.preferenceValue == keyData.preferenceValue + 1  select e).ToList();


            foreach(KeyValueData key in attributesList)
            {
                ParentChildNode dataa = new ParentChildNode();
                dataa.name = key.keyValue;
                dataa.preferenceValue = key.preferenceValue;
                dataa.children = getUpperHorizontals(attributesList[0]);
                getAllData.Add(dataa);
            }          

            return getAllData;
        }

        public List<ParentChildNode> ParentChildSequence(KeyValueData itmData, List<AttributesLink> getList,KeyValueData channelData, string attaributeSequence, Boolean flag)
        {
            List<ParentChildNode> data = new List<ParentChildNode>();
            if (flag != false)
            {
                foreach (AttributesLink itm in getList)
                {
                    string period = "(" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(itm.startMonth).Substring(0, 3) + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(itm.endMonth).Substring(0, 3) + "," + itm.year + ")";
                    string[] attributeSequenceList = attaributeSequence.Split(':');
                    List<string> attributeSequeceIdList = new List<string>();
                    int id = 0;
                    foreach (string itmdata in attributeSequenceList)
                    {
                        attributeSequeceIdList.Add(itmdata);
                        id = Convert.ToInt32(itmdata);
                    }
                    string childLabel = (from e in db.attributesLink where e.childId == id select e.childlabel).FirstOrDefault();
                    string childValue = (from e in db.attributesLink where e.childId == itm.childId select e.childlabel).FirstOrDefault();
                    if (childLabel == childValue)
                    {
                        attaributeSequence = "";
                        for (int i = 0; i < attributeSequeceIdList.Count() - 1; i++)
                        {
                            if (attaributeSequence == "")
                            {
                                attaributeSequence = attributeSequeceIdList[i].ToString();
                            }
                            else
                            {
                                attaributeSequence = attaributeSequence + ":" + attributeSequeceIdList[i].ToString();
                            }
                        }
                    }

                    attaributeSequence = attaributeSequence + ":" + itm.childId;
                    ParentChildNode childNode = new ParentChildNode();
                    string naem = (from e in db.keyValueData where e.id == itm.childId select e.keyValue).FirstOrDefault();
                    childNode.name = naem;
                    if (itm.startMonth != 1 || itm.endMonth != 12)
                    {
                        childNode.period = period;
                    }
                    else
                    {
                        childNode.period = "";
                    }
                    childNode.id = itm.childId;
                    if (itmData != null)
                    {
                        KeyValueData getSalesHorizontalList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceValue == itmData.preferenceValue + 1 && e.keyName.Equals(itmData.keyName) select e).FirstOrDefault();
                        childNode.preferenceValue = itmData.preferenceValue;
                        childNode.key = itm.childlabel;
                        List<AttributesLink> getListData = (from e in db.attributesLink where e.rootId == channelData.id && e.attributeSequence == attaributeSequence select e).ToList();
                        flag = true;
                        if (getSalesHorizontalList != null)
                        {
                            childNode.children = ParentChildSequence(getSalesHorizontalList, getListData, channelData, attaributeSequence, flag);
                        }
                        //New Code For Getting Customers and Employee//
                        if (childNode.children.Count == 0)
                        {
                            flag = false;
                            if (getSalesHorizontalList != null)
                            {
                                childNode.children = ParentChildSequence(getSalesHorizontalList, getListData, channelData, attaributeSequence, flag);
                            }
                            
                        }

                        //New Code End//
                        data.Add(childNode);
                    }

                }
            }
            else
            {
                if (attaributeSequence != null)
                {
                    string[] attributeSequenceList = attaributeSequence.Split(':');

                    ParentChildNode childNodee = new ParentChildNode();

                    List<EmployeeRole> getEmpRoleDetail = new List<EmployeeRole>();
                    var query = from e in db.employeeRole
                                select e;
                    for (int i = 0; i < attributeSequenceList.Count(); i++)
                    {
                        int id = Convert.ToInt32(attributeSequenceList[i]);

                        string ids = (from u in db.keyValueData
                                      where u.id == id
                                      select u.keyValue).FirstOrDefault();
                        switch (i)
                        {
                            case 0:
                                query = query.Where(x => x.RP1 == ids);
                                break;
                            case 1:
                                query = query.Where(x => x.RP2 == ids);
                                break;
                            case 2:
                                query = query.Where(x => x.RP3 == ids);
                                break;
                            case 3:
                                query = query.Where(x => x.RP4 == ids);
                                break;
                            case 4:
                                query = query.Where(x => x.RP5 == ids);
                                break;
                            case 5:
                                query = query.Where(x => x.RP6 == ids);
                                break;
                            case 6:
                                query = query.Where(x => x.RP7 == ids);
                                break;
                        }
                    }

                    List<CustomerBaseDetails> customerDetails = new List<CustomerBaseDetails>();
                    List<int> newCustDetails = new List<int>();
                    foreach (EmployeeRole empR in query)
                    {
                        List<int> custDetails = new List<int>();
                        custDetails = (from u in db.customerRelationshipDetails where u.employeeRoleFK == empR.id select u.customerBaseDetailsFK).ToList();

                        foreach (int cust in custDetails)
                        {
                            newCustDetails.Add(cust);
                        }


                    }
                    customerDetails = (from i in db.customerBaseDetails where newCustDetails.Contains(i.id) select i).ToList();

                    foreach (CustomerBaseDetails c in customerDetails)
                    {
                        childNodee = new ParentChildNode();
                        attaributeSequence = null;


                        childNodee.name = c.customerName;
                        childNodee.id = c.id;

                        KeyValueData getSalesHorizontalList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceValue == itmData.preferenceValue + 1 && e.keyName.Equals(itmData.keyName) select e).FirstOrDefault();

                        childNodee.key = "";
                        List<AttributesLink> getListData = (from e in db.attributesLink where e.rootId == channelData.id && e.attributeSequence == attaributeSequence select e).ToList();
                        flag = false;
                        if (getSalesHorizontalList != null)
                        {
                            childNodee.children = ParentChildSequence(getSalesHorizontalList, getListData, channelData, attaributeSequence, flag);
                        }

                        if (childNodee.children.Count == 0)
                        {
                            ParentChildNode asd = new ParentChildNode();

                            //add employee on customer//
                            List<ParentChildNode> paae = new List<ParentChildNode>();

                            List<int> customerDetila = (from u in db.customerRelationshipDetails
                                                        where u.customerBaseDetailsFK == c.id
                                                        select u.employeeRoleFK).ToList();

                            foreach (int i in customerDetila)
                            {
                                List<string> empt = (from t in db.employeeRole where t.id == i select t.adpId).ToList();

                                foreach (string em in empt)
                                {
                                    string names = (from u in db.employeeBaseDetails where u.adpId == em select u.firstName).FirstOrDefault();
                                    asd.name = names;
                                    paae.Add(asd);
                                    childNodee.children = paae;

                                }
                            }
                            data.Add(childNodee);
                        }
                    }

                }
            }
            return data;
        }

        public JsonResult GetSalesHorizontalValue()
        {
            Stream req = Request.InputStream;
            ParentChildNode metaData = (ParentChildNode)Utility.mapRequestToClass(Request.InputStream, typeof(ParentChildNode));

            AttributeLinkDTO attributeData = new AttributeLinkDTO();
            attributeData.endLoop = false;
            ParentChildNode getCurrentKey = new ParentChildNode();
            getCurrentKey.key = metaData.currentItem.key;
            getCurrentKey.name = metaData.currentItem.name;
            getCurrentKey.id = metaData.currentItem.id;
            attributeData = getAttributeLinkRow(metaData, getCurrentKey, attributeData);

            KeyValueData currentKey = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == metaData.key && e.keyValue == attributeData.childLabel select e).FirstOrDefault();
            List<KeyValueData> nextHorizontal = new List<KeyValueData>();
            if (attributeData.childLabel == null)
            {
                nextHorizontal = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == metaData.key && e.preferenceValue >= 0 orderby e.preferenceValue ascending select e).Distinct().ToList();

            }
            else
            {
                nextHorizontal = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == metaData.key && e.preferenceValue > currentKey.preferenceValue orderby e.preferenceValue ascending select e).Distinct().ToList();

            }
                      

            var jsonResult = new
            {
                rsBody = new
                {
                    subTabList = nextHorizontal
                }
            };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

      

        public JsonResult DeactivateSalesHorizontalSubmit()
        {
            Stream req = Request.InputStream;

            ParentChildNode metaData = (ParentChildNode)Utility.mapRequestToClass(Request.InputStream, typeof(ParentChildNode));

            try
            {
                string attributeSequence = metaData.newNode.attributeSequence;

                string key = metaData.currentItem.key;

                int currentSalesHorizonatlPreferenceValue = (from u in db.keyValueData
                                                             where u.keyType == Codes.keyValue.MetaData.ToString()
                                                             && u.keyName == Codes.keyValue.SalesHorizontal.ToString()
                                                             && u.keyValue == key
                                                             select u.preferenceValue).FirstOrDefault();

                List<string> lowerLevelSalesHorizontal = (from u in db.keyValueData
                                                          where u.keyType == Codes.keyValue.MetaData.ToString() &
                                                          u.keyName == Codes.keyValue.SalesHorizontal.ToString()
                                                           & u.preferenceValue >= currentSalesHorizonatlPreferenceValue
                                                          select u.keyValue).ToList();

                List<AttributesLink> attrLiist = (from r in db.attributesLink
                                                  where r.attributeSequence.Contains(attributeSequence) && lowerLevelSalesHorizontal.Contains(r.childlabel)
                                                  select r).ToList();


                AttributesLink ttrLiist = (from r in db.attributesLink
                                           where r.childId == metaData.currentItem.id
                                           select r).FirstOrDefault();



                ttrLiist.endMonth = metaData.endMonth;
                db.SaveChanges();

                foreach (AttributesLink attr in attrLiist)
                {
                    attr.endMonth = metaData.endMonth;
                    db.SaveChanges();
                }

                var jsonResult = new
                {
                    rsBody = new
                    {
                        data = Codes.message.Success.ToString()

                    }
                };
                return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                var jsonResult = new
                {
                    rsBody = new
                    {
                        data = Codes.message.Failed.ToString()

                    }
                };
                return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }   
        }

        public List<ParentChildNode> ChildSequence(KeyValueData itmData, List<AttributesLink> getList, KeyValueData channelData, string attaributeSequence)
        {
            List<ParentChildNode> data = new List<ParentChildNode>();
            foreach (AttributesLink itm in getList)
            {
                string[] attributeSequenceList = attaributeSequence.Split(':');
                List<string> attributeSequeceIdList = new List<string>();
                int id = 0;
                foreach(string itmdata in attributeSequenceList)
                {
                    attributeSequeceIdList.Add(itmdata);
                    id = Convert.ToInt32(itmdata);
                }
                string childLabel = (from e in db.attributesLink where e.childId == id select e.childlabel).FirstOrDefault();
                string childValue = (from e in db.attributesLink where e.childId == itm.childId select e.childlabel).FirstOrDefault();
                if (childLabel == childValue)
                {
                    attaributeSequence = "";
                    for (int i = 0; i < attributeSequeceIdList.Count() - 1; i++)
                    {
                        if (attaributeSequence == "")
                        {
                            attaributeSequence = attributeSequeceIdList[i].ToString();
                        }
                        else
                        {
                            attaributeSequence = attaributeSequence + ":" + attributeSequeceIdList[i].ToString();
                        }
                    }
                }

                attaributeSequence = attaributeSequence + ":" + itm.childId;
                ParentChildNode childNode = new ParentChildNode();
                string naem = (from e in db.keyValueData where e.id == itm.childId select e.keyValue).FirstOrDefault();
                childNode.name = naem;
                childNode.id = itm.childId;
                if (itmData != null)
                {
                    KeyValueData getSalesHorizontalList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceValue == itmData.preferenceValue + 1 && e.keyName.Equals(itmData.keyName) select e).FirstOrDefault();

                    childNode.key = itm.childlabel;
                    List<AttributesLink> getListData = (from e in db.attributesLink where e.rootId == channelData.id && e.attributeSequence == attaributeSequence select e).ToList();
                    if (getSalesHorizontalList != null)
                    {
                        childNode.children = ParentChildSequence(getSalesHorizontalList, getListData, channelData, attaributeSequence, true);
                    }
                    data.Add(childNode);
                }
            }
            return data;
        }


        public static List<AttributesLink> GetAllBelowChildOfSalesHorizontal(List<AttributesLink> childList, List<AttributesLink> finalAttributeData)
        {
            List<AttributesLink> attrData = new List<AttributesLink>();
            List<AttributesLink> attrata = new List<AttributesLink>();
            List<AttributesLink> chld = new List<AttributesLink>();

            using (DBModels db = new DBModels())
            {
                foreach (AttributesLink chldLst in childList)
                {
                    attrData = (from u in db.attributesLink
                                where u.parentId == chldLst.parentId
                                select u).ToList();

                    foreach (AttributesLink atrData in attrData)
                    {
                        finalAttributeData.Add(atrData);
                    }
                }
                return finalAttributeData;
            }
        }      
      
        [HttpPost]
        public JsonResult SaveMetaDataTreeValue()
        {
            Stream req = Request.InputStream;
            ParentChildNode metaData = (ParentChildNode)Utility.mapRequestToClass(Request.InputStream, typeof(ParentChildNode));

            KeyValueData getValueExit = (from e in db.keyValueData
                                         where e.keyType == metaData.newNode.keyName && e.keyName == metaData.newNode.key
                                         && e.keyValue == metaData.newNode.keyValue select e).FirstOrDefault();

            //Create Simple Value In KeyValueTable
            if (getValueExit == null)
            {
                getValueExit = new KeyValueData();
                getValueExit.keyType = metaData.newNode.keyName;
                getValueExit.keyName = metaData.newNode.key;
                getValueExit.keyValue = metaData.newNode.keyValue;
                getValueExit.description = "Value of " + metaData.newNode.keyName;
                getValueExit.relatedKeyType = metaData.newNode.key;
                getValueExit.preferenceSerial = 0;
                getValueExit.preferenceValue = 0;
                getValueExit.startMonth = metaData.newNode.startMonth;
                getValueExit.endMonth = metaData.newNode.endMonth;
                getValueExit.year = metaData.newNode.year;
                getValueExit.labelOnScreen = metaData.newNode.labelOnScreen;
                db.keyValueData.Add(getValueExit);
                db.SaveChanges();
            }

            //Create Relationship Between The Value And Heirarchy
            if (metaData.newNode.attributeSequence != "" && metaData.newNode.attributeSequence != null)
            {
                List<string> attribute = metaData.newNode.attributeSequence.Split(':').ToList();
                AttributesLink insertAttributeLink = new AttributesLink();
                insertAttributeLink.rootId = Convert.ToInt32(attribute[0]);
                insertAttributeLink.parentId = Convert.ToInt32(attribute[attribute.Count() - 1]);
                insertAttributeLink.childId = getValueExit.id;
                insertAttributeLink.attributeSequence = metaData.newNode.attributeSequence;
                insertAttributeLink.startMonth = metaData.newNode.startMonth;
                insertAttributeLink.endMonth = metaData.newNode.endMonth;
                insertAttributeLink.childlabel = getValueExit.keyType;
                insertAttributeLink.year = metaData.newNode.year;
                db.attributesLink.Add(insertAttributeLink);
                db.SaveChanges();
            }

            var jsonResult = new
            {
                rsBody = new
                {
                    message = "success"
                }
            };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public AttributeLinkDTO getAttributeLinkRow(ParentChildNode metaData, ParentChildNode getCurrentKey, AttributeLinkDTO attributeData)
        {           
                          
            foreach (ParentChildNode itmData in metaData.children)
            {
                if (attributeData.endLoop == true)
                {
                    break;
                }
                if (itmData.id != getCurrentKey.id)
                {
                    if (attributeData.attributeSequence != "" && attributeData.attributeSequence != null)
                    {
                        attributeData.attributeSequence = attributeData.attributeSequence + ":" + itmData.id.ToString();                           
                    }
                    else
                    {
                        attributeData.attributeSequence = itmData.id.ToString();
                    }
                }
                else
                {
                    if (attributeData.attributeSequence == null || attributeData.attributeSequence == "")
                    {
                        attributeData.attributeSequence = itmData.id.ToString();
                    }
                    else
                    {
                        attributeData.attributeSequence = attributeData.attributeSequence + ":" + itmData.id.ToString();
                    }
                    attributeData.childId = itmData.id;
                    attributeData.childLabel = itmData.key;
                    attributeData.endLoop = true;
                }                

                attributeData = getAttributeLinkRow(itmData, getCurrentKey, attributeData);
               
            }
            return attributeData;
        }

        [HttpPost]       
        public JsonResult CreateMetaData()
        {
            Stream req = Request.InputStream;
            MetaDataConfigureDTO metaData = (MetaDataConfigureDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataConfigureDTO));

            object jsonsuccess = null;
            if (metaData != null)
            {
                string value = metaData.keyType.Replace(" ", String.Empty);                
                KeyValueData insertKey = new KeyValueData();
                insertKey.keyType = "MetaData";
                insertKey.keyName = value;
                insertKey.keyValue = metaData.keyName;
                insertKey.description = "This sales boundaries represents the core component while uploading the OP";
                insertKey.relatedKeyType = "MetaDataType";
                insertKey.preferenceSerial = 1;
                insertKey.startMonth = metaData.startMonth;
                insertKey.endMonth = metaData.endMonth;
                insertKey.labelOnScreen = metaData.labelOnScreen;
                insertKey.year = metaData.year;
                insertKey.preferenceValue = metaData.preferenceValue;
                db.keyValueData.Add(insertKey);
                db.SaveChanges();
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExistingMetaData()
        {
            Stream req = Request.InputStream;
            SearchMetaDataDTO keyvalue = (SearchMetaDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(SearchMetaDataDTO));

            object jsonsuccess = null;
            List<KeyValueData> attributesList = new List<KeyValueData>();
            Dictionary<string, List<KeyValueData>> data = new Dictionary<string, List<KeyValueData>>();
            List<EmployeeAttributeLink> localList = new List<EmployeeAttributeLink>();
            var size = 15;
            int pageNo = 0;
            if (keyvalue != null)
            {
                if (keyvalue.pageNumber != null)
                {
                    pageNo = Convert.ToInt32(keyvalue.pageNumber);

                    if (keyvalue.searchKey != null && !(keyvalue.searchKey.Equals("")))
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab) && e.keyValue.Equals(keyvalue.searchKey)
                                          orderby e.id descending
                                          select e).Skip(pageNo * size).Take(size).ToList();
                    }
                    else
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab)
                                          orderby e.id descending
                                          select e).Skip(pageNo * size).Take(size).ToList();
                    }
                }
                else
                {
                    if (keyvalue.searchKey != null && !(keyvalue.searchKey.Equals("")))
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab) && e.keyValue.Equals(keyvalue.searchKey)
                                          orderby e.id descending
                                          select e).ToList();
                    }
                    else
                    {
                        attributesList = (from e in db.keyValueData
                                          where e.keyType.Equals(keyvalue.subTab) && e.keyName.Equals(keyvalue.currentTab)
                                          orderby e.id descending
                                          select e).ToList();
                    }
                }
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    existingList = attributesList,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult getAllAccessControlAttributes()
        {
            Stream req = Request.InputStream;
            SearchMetaDataDTO keyvalue = (SearchMetaDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(SearchMetaDataDTO));
            object jsonsuccess = null;
            var listData = new Dictionary<string, List<KeyValueData>>();


            if (keyvalue != null)
            {
                string currentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
                List<KeyValueData> data = (from e in db.keyValueData where e.keyType.Equals("AccessControl") select e).ToList();
                listData = data.GroupBy(item => item.keyName).ToDictionary(grp => grp.Key, grp => grp.ToList());

            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    listData = listData,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExistingAccessForProfile()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            AccessControlDTO finalData = new AccessControlDTO();

            if(data != null && data.profile != null)
            {
                List<TreeNode> childNodes = GetAllOUList();
                finalData.ouList = childNodes;
                   
                List<int> selectedOU = new List<int>();
                finalData.user = data.user;
                if (data.profile.Count == 1)
                {
                    foreach (string s in data.profile)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && k.keyValue.Equals(s) select k).FirstOrDefault();
                        if (profile != null)
                        {
                            finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                            HashSet<string> ouID = new HashSet<string>();
                            
                            finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                            List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();
                            finalData.selectedFunctions.AddRange((from k in db.keyValueData
                                                                  where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl")
                                                                  select
                                                                  new RolePerformanceListDTO
                                                                  {
                                                                      row = k,
                                                                      selected = false
                                                                  }).ToList());

                            List<KeyValueData> getList = (from e in db.keyValueData where e.keyType == "SalesHorizontal" select e).ToList();

                            foreach(KeyValueData key in getList)
                            {
                                KeyValueData itmD = new KeyValueData();
                                AttributesLink getItemData = (from e in db.attributesLink where e.rootId == key.id select e).FirstOrDefault();

                                itmD = key;
                                db.keyValueData.Add(itmD);
                                db.SaveChanges();
                            }


                           
                            foreach (RolePerformanceListDTO item in finalData.selectedFunctions)
                            {
                                item.selectAll = false;
                                
                                int countPermission = 0;
                                List<RolePerformanceListDTO> tempList = new List<RolePerformanceListDTO>();
                                UserAccessLinking link = (from u in db.userAccessLinkings where u.profileId == profile.id && u.functionId == item.row.id select u).FirstOrDefault();
                                if (link != null)
                                {
                                    string[] selectedOUList = link.OUs.Split(':');
                                    foreach (string itm in selectedOUList)
                                    {
                                        if (itm != "" && itm != null)
                                        {
                                            int id = Convert.ToInt32(itm);
                                            if (!selectedOU.Contains(id))
                                            {
                                                selectedOU.Add(id);
                                            }
                                        }
                                    }
                                }
                                finalData.selectedOUs = selectedOU;                              
                                if (link != null)
                                {
                                    item.selected = true;
                                    foreach (KeyValueData p in permissions)
                                    {
                                        UserAccessLinking per = (from u in db.userAccessLinkings where u.profileId == profile.id && u.functionId == item.row.id && u.permissionId == p.id select u).FirstOrDefault();

                                        RolePerformanceListDTO tempObj = new RolePerformanceListDTO();
                                        if (per != null)
                                        {
                                            tempObj.selected = true;
                                            tempObj.row = p;
                                            countPermission = countPermission + 1;
                                            if (countPermission == permissions.Count)
                                            {
                                                item.selectAll = true;
                                            }
                                        }
                                        else
                                        {
                                            tempObj.selected = false;
                                            tempObj.row = p;
                                        }
                                        tempList.Add(tempObj);
                                    }

                                }
                                else
                                {
                                    foreach (KeyValueData p in permissions)
                                    {
                                        RolePerformanceListDTO tempObj = new RolePerformanceListDTO();
                                        tempObj.selected = false;
                                        tempObj.row = p;
                                        tempList.Add(tempObj);
                                    }
                                }
                                finalData.selectedPermissions.Add(item.row.keyValue, tempList);
                            }
                           
                        }
                    }
                    finalData.profile = data.profile;
                }
                else
                {
                    List<int> profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k.id).ToList();
                    if (profile != null && profile.Count > 0)
                    {
                        finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                        finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                        List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();
                        finalData.selectedFunctions.AddRange((from k in db.keyValueData
                                                              where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl")
                                                              select
                                                              new RolePerformanceListDTO
                                                              {
                                                                  row = k,
                                                                  selected = false
                                                              }).ToList());
                        foreach (RolePerformanceListDTO item in finalData.selectedFunctions)
                        {
                            
                        }
                    }
                }
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    listData = finalData,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public List<TreeNode> GetAllOUList()
        {
            KeyValueData topOu = (from e in db.keyValueData
                                  where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.preferenceValue == 0
                         && e.keyName.Equals("OperationalUnit")
                                  select e).FirstOrDefault();

            List<TreeNode> childNodes = new List<TreeNode>();
            if (topOu != null)
            {
                List<KeyValueData> topOuList = (from e in db.keyValueData
                                                where e.keyType.Equals(topOu.keyValue) && e.keyName.Equals("OperationalUnit")
                                                select e).ToList();


                if (topOuList != null)
                {
                    foreach (KeyValueData k in topOuList)
                    {
                        TreeNode root = AddNodeToRoot(k, topOu, new TreeNode());
                        root.text = k.keyValue;
                        root.value = k.id;
                        childNodes.Add(root);
                    }
                }
            }
            return childNodes;
        }

        public TreeNode AddNodeToRoot(KeyValueData ouValue, KeyValueData ouLevel, TreeNode nodes)
        {
            TreeNode nodeList = new TreeNode();
            KeyValueData nextOULevel = (from e in db.keyValueData
                                        where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.preferenceValue == ouLevel.preferenceValue + 1
                                        && e.keyName.Equals("OperationalUnit")
                                        select e).FirstOrDefault();

            if (nextOULevel != null)
            {
                List<KeyValueData> OuList = (from e in db.keyValueData
                                             join a in db.attributesLink on e.id equals a.childId
                                             where e.keyType.Equals(nextOULevel.keyValue) && e.keyName.Equals("OperationalUnit")
                                             && a.attributeSequence.Contains(ouValue.id + "") && a.childlabel.Equals(nextOULevel.keyValue)
                                             select e).ToList();
                if (OuList != null)
                {
                    foreach (KeyValueData k in OuList)
                    {
                        TreeNode root = AddNodeToRoot(k, nextOULevel, new TreeNode());
                        root.text = k.keyValue;
                        root.value = k.id;
                        nodeList.children.Add(root);
                    }
                }
            }
            else
            {
                nodeList.text = ouValue.keyValue;
                nodeList.value = ouValue.id;
            }
            return nodeList;
        }

        public JsonResult GetExistingAccessForMultiProfile()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            AccessControlDTO finalData = new AccessControlDTO();
            if (data != null && data.profile != null)
            {
                List<int> selectedOU = new List<int>();
                //Get alll OU List
                List<TreeNode> childNodes = GetAllOUList();
                finalData.ouList = childNodes;
                List<KeyValueData> attributesList = (from e in db.keyValueData where e.keyType.Equals("MetaData") && e.preferenceSerial == 1 && e.keyName.Equals("OperationalUnit") select e).ToList();
                finalData.user = data.user;
                if (data.profile.Count > 0)
                {
                    List<Int32> profIds = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k.id).ToList();
                    finalData.selectedFunctions = new List<RolePerformanceListDTO>();
                    HashSet<string> ouID = new HashSet<string>();

                    finalData.selectedPermissions = new Dictionary<string, List<RolePerformanceListDTO>>();
                    List<KeyValueData> permissions = (from k in db.keyValueData where k.keyType.Equals("Permission") && k.keyName.Equals("AccessControl") orderby k.id descending select k).ToList();

                    List<int> functionsIds = (from u in db.userAccessLinkings where profIds.Contains(u.profileId) select u.functionId).Distinct().ToList();


                    foreach (int u in functionsIds)
                    {
                        RolePerformanceListDTO funcDet = (from k in db.keyValueData
                                                          where k.keyType.Equals("Function") && k.keyName.Equals("AccessControl") && k.id == u
                                                          select
                                                          new RolePerformanceListDTO
                                                          {
                                                              row = k,
                                                              selected = true,
                                                              selectAll = false
                                                          }).FirstOrDefault();
                        UserAccessLinking ous = (from k in db.userAccessLinkings where profIds.Contains(k.profileId) && k.functionId == u select k).FirstOrDefault();
                        if (ous != null)
                        {

                            string[] selectedOUList = ous.OUs.Split(':');
                            foreach (string itm in selectedOUList)
                            {
                                if (itm != "" && itm != null)
                                {
                                    int id = Convert.ToInt32(itm);
                                    if (!selectedOU.Contains(id))
                                    {
                                        selectedOU.Add(id);
                                    }
                                }
                            }
                            finalData.selectedOUs = selectedOU;
                        }

                        List<int> permissionID = (from e in db.userAccessLinkings where e.functionId == u select e.permissionId).ToList();
                        List<int> perIds = (from per in db.userAccessLinkings where profIds.Contains(per.profileId) && per.functionId == u select per.permissionId).ToList();
                        List<RolePerformanceListDTO> tempList = new List<RolePerformanceListDTO>();
                        if (perIds != null)
                        {
                            int countAll = 0;
                            foreach (KeyValueData p in permissions)
                            {
                                RolePerformanceListDTO temp = new RolePerformanceListDTO();
                                if (perIds.Contains(p.id))
                                {
                                    temp.row = p;
                                    temp.selected = true;
                                    countAll = countAll + 1;
                                }
                                else
                                {
                                    temp.row = p;
                                    temp.selected = false;
                                }
                                tempList.Add(temp);
                            }
                            if (countAll == permissions.Count)
                            {
                                funcDet.selectAll = true;
                            }

                            finalData.selectedFunctions.Add(funcDet);
                            if (finalData.selectedPermissions.ContainsKey(funcDet.row.keyValue))
                            {
                                finalData.selectedPermissions[funcDet.row.keyValue].AddRange(tempList);
                            }
                            else
                            {
                                finalData.selectedPermissions.Add(funcDet.row.keyValue, tempList);
                            }
                        }
                    }
                    finalData.selectedRole = data.selectedRole;
                    finalData.profile = data.profile;
                }
            }
            jsonsuccess = new
            {
                rsBody = new
                {
                    listData = finalData,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public string triverseTree(List<TreeViewItem> children, string OUlist)
        {
            foreach (TreeViewItem itm in children)
            {
                if (itm.internalChecked != "False")
                {
                    OUlist = OUlist + ":" + itm.value.ToString();
                    OUlist = triverseTree(itm.internalChildren, OUlist);
                }
            }
            return OUlist;
        }

        public JsonResult CreateAccessPermissions()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));
            object jsonsuccess = null;
            string msg = null;

            //get concate ou list which is selected for particular profile access
            string OUlist = "";
            foreach (TreeViewItem itm in data.treeViewItem)
            {
                if (itm.internalChecked != "False")
                {
                    //run recursion
                    if (OUlist == "")
                    {
                        OUlist = itm.value.ToString();
                    }
                    else
                    {
                        OUlist = OUlist + "::" + itm.value.ToString();
                    }
                    OUlist = triverseTree(itm.internalChildren, OUlist);
                }
            }


            if (data != null && data.selectedFunctions != null)
            {
                StringBuilder sb = new StringBuilder();
                if (data.selectedRole != null && data.selectedRole.Count != 0)
                {
                    if (data.selectedRole.Count > 0)
                    {

                        if (data.profile != null && data.profile.Count > 0)
                        {
                            KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                            List<UserExceptionPermissionFlow> oldData = (from u in db.UserExceptionPermissionFlow where u.profileId == profile.id select u).ToList();
                            db.UserExceptionPermissionFlow.RemoveRange(oldData);
                            db.SaveChanges();

                            foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                            {
                                if (rl.selected == true)
                                {
                                    foreach (string role in data.selectedRole)
                                    {
                                        UserExceptionPermissionFlow newUser = new UserExceptionPermissionFlow();
                                        newUser.profileId = profile.id;
                                        newUser.OUs = OUlist;
                                        newUser.roleId = (from e in db.keyValueData where e.keyName == "RoleHierarchy" && e.labelOnScreen == role select e.id).FirstOrDefault();// 0101;
                                        newUser.empId = 001;
                                    }

                                }
                            }
                            msg = "success";
                        }
                    }
                }
                else if (data.user != null)
                {
                    if (data.profile != null && data.profile.Count > 0)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                        List<UserExceptionPermissionFlow> oldData = (from u in db.UserExceptionPermissionFlow where u.profileId == profile.id select u).ToList();
                        db.UserExceptionPermissionFlow.RemoveRange(oldData);
                        db.SaveChanges();

                        foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                        {
                            if (rl.selected == true)
                            {

                                UserExceptionPermissionFlow newUser = new UserExceptionPermissionFlow();
                                newUser.profileId = profile.id;
                                newUser.OUs = sb.ToString();
                                newUser.roleId = 0;
                                newUser.empId = Convert.ToInt32(data.user.adpId);
                                newUser.OUs = OUlist;

                            }
                        }
                        msg = "success";
                    }
                }
                else
                {

                    if (data.profile != null && data.profile.Count > 0)
                    {
                        KeyValueData profile = (from k in db.keyValueData where k.keyType.Equals("ROLE") && k.keyName.Equals("AccessControl") && data.profile.Contains(k.keyValue) select k).FirstOrDefault();
                        List<UserAccessLinking> oldData = (from u in db.userAccessLinkings where u.profileId == profile.id select u).ToList();
                        db.userAccessLinkings.RemoveRange(oldData);
                        db.SaveChanges();

                        foreach (RolePerformanceListDTO rl in data.selectedFunctions)
                        {
                            if (rl.selected == true)
                            {
                                UserAccessLinking newUser = new UserAccessLinking();
                                newUser.profileId = profile.id;
                                newUser.functionId = rl.row.id;
                                newUser.OUs = OUlist;
                                if (data.selectedPermissions.ContainsKey(rl.row.keyValue))
                                {
                                    foreach (RolePerformanceListDTO per in data.selectedPermissions[rl.row.keyValue])
                                    {
                                        if (per.selected == true)
                                        {
                                            newUser.permissionId = per.row.id;
                                        }
                                        db.userAccessLinkings.Add(newUser);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        msg = "success";
                    }
                }

            }
            else
            {
                msg = "error";
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    result = msg,
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ADPIdData()
        {
            Stream req = Request.InputStream;
            AccessControlDTO data = (AccessControlDTO)Utility.mapRequestToClass(Request.InputStream, typeof(AccessControlDTO));

            object jsonsuccess = null;

            MonthDTO monthDeatail = new MonthDTO();
            monthDeatail = CommonController.getMonthList((from k in db.keyValueData where k.keyType.Equals("CURRENTQUARTER") && k.keyName.Equals("CFY") select k.keyValue).FirstOrDefault());
            EmployeeRoleAndAreaDTO empDet = (from e in db.employeeBaseDetails
                                             join r in db.employeeRole on e.adpId equals r.adpId
                                             where e.adpId.Equals(data.user.adpId) &&
                                            ((r.startMonth <= monthDeatail.startMonth && r.endMonth >= monthDeatail.startMonth)
                                            || (r.startMonth >= monthDeatail.startMonth && r.endMonth <= monthDeatail.endMonth)
                                            || (r.startMonth >= monthDeatail.startMonth && r.startMonth <= monthDeatail.endMonth))
                                             select
                                             new EmployeeRoleAndAreaDTO
                                             {
                                                 fullName = e.fullName,
                                                 adpId = e.adpId,
                                                 role = r.role,
                                                 email = e.email

                                             }).FirstOrDefault();
            if (empDet != null)
            {
                jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                        empDet = empDet
                    }
                };
            }
            else
            {
                jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "error",
                    }
                };
            }

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSalesHorizontalHierarchy()
        {
            Stream req = Request.InputStream;
            MetaDataConfigureDTO keyvalueData = (MetaDataConfigureDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataConfigureDTO));
            object jsonsuccess = null;
            List<KeyValueData> finalList = new List<KeyValueData>();
            var query = from r in db.attributesLink select r;
            string attributeSequesnce = "";
            for (int i = 0; i < keyvalueData.upperPreferenceValue; i++)
            {
                
                switch(i)
                {
                    case 0:
                        if (keyvalueData.preferenceValue != keyvalueData.upperPreferenceValue)
                        {
                            attributeSequesnce = keyvalueData.level_0;
                        }
                        break;
                    case 1:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_1;
                        break;
                    case 2:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_2;
                        break;
                    case 3:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_3;
                        break;
                    case 4:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_4;
                        break;
                    case 5:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_5;
                        break;
                    case 6:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_6;
                        break;
                    case 7:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_7;
                        break;
                    case 8:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_8;
                        break;
                    case 9:
                        attributeSequesnce = attributeSequesnce + ":" + keyvalueData.level_9;
                        break;  
                }   
            }

            if (keyvalueData.preferenceValue == keyvalueData.upperPreferenceValue)
            {
                KeyValueData topLevel = (from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(keyvalueData.key) && key.preferenceValue == keyvalueData.preferenceValue select key).FirstOrDefault();
                finalList.AddRange((from k in db.keyValueData where k.keyType.Equals(topLevel.keyValue) && k.keyName.Equals(keyvalueData.key) select k).ToList());
            }
            else
            {
                query = query.Where(i => i.attributeSequence == attributeSequesnce);
                List<AttributesLink> temp = new List<AttributesLink>();
                foreach (AttributesLink itm in query)
                {
                    string sequence = attributeSequesnce + ":" + itm.childId;
                    if(sequence != keyvalueData.attributeSequence)
                    {
                        temp.Add(itm);
                    }
                }
              
                List<AttributesLink> newattr = new List<AttributesLink>();
                foreach (AttributesLink a in temp)
                {
                    KeyValueData mapping = (from k in db.keyValueData where k.id == a.childId select k).FirstOrDefault();
                    finalList.Add(mapping);
                }
            }
            jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    list = finalList
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUpperHeirarchyList()
        {
            Stream req = Request.InputStream;
            MetaDataSetupDTO data = (MetaDataSetupDTO)Utility.mapRequestToClass(Request.InputStream, typeof(MetaDataSetupDTO));

            KeyValueData getHorizontalDetail = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == data.key && e.keyValue == data.subkey select e).FirstOrDefault();

            List<heirarchyLevelList> heirarchy = new List<heirarchyLevelList>();
            List<KeyValueDataDTO> getHeirarchyData = (from e in db.keyValueData where e.keyType == "MetaData" && e.keyName == data.key && e.preferenceValue < getHorizontalDetail.preferenceValue
                                                      select new KeyValueDataDTO
                                                      {
                                                          keyType = e.keyType,
                                                          keyName = e.keyName,
                                                          keyValue = e.keyValue,
                                                          labelOnScreen = e.labelOnScreen,
                                                          description = e.description,
                                                          relatedKeyType = e.relatedKeyType,
                                                          relatedKeyValue = e.relatedKeyValue,
                                                          preferenceValue = e.preferenceValue,
                                                          preferenceSerial = e.preferenceSerial,
                                                          startMonth = e.startMonth,
                                                          endMonth = e.endMonth,
                                                          year = e.year,
                                                          modalKey = "level_" + e.preferenceValue
                                                       }).ToList();

            

            if(getHeirarchyData.Count() == 0)
            {
                KeyValueDataDTO setData = new KeyValueDataDTO();
                setData.keyType = getHorizontalDetail.keyType;
                setData.keyName = getHorizontalDetail.keyName;
                setData.keyValue = getHorizontalDetail.keyValue;
                setData.labelOnScreen = getHorizontalDetail.labelOnScreen;
                setData.description = getHorizontalDetail.description;
                setData.relatedKeyType = getHorizontalDetail.relatedKeyType;
                setData.relatedKeyValue = getHorizontalDetail.relatedKeyValue;
                setData.preferenceValue = getHorizontalDetail.preferenceValue;
                setData.preferenceSerial = getHorizontalDetail.preferenceSerial;
                setData.startMonth = getHorizontalDetail.startMonth;
                setData.endMonth = getHorizontalDetail.endMonth;
                setData.year = getHorizontalDetail.year;
                setData.modalKey = "level_" + getHorizontalDetail.preferenceValue;
                getHeirarchyData.Add(setData);
            }
            for (int i = 0; i < getHorizontalDetail.preferenceValue; i++)
            {
                switch (i)
                {
                    case 0:
                        string getType = getHeirarchyData[0].keyValue.ToString();
                        heirarchyLevelList heirarchyList = new heirarchyLevelList();
                        heirarchyList.keyName = data.value;
                        heirarchyList.value = (from e in db.keyValueData where e.keyType == getType && e.keyName == data.key select e).ToList();
                        heirarchy.Add(heirarchyList);
                        break;
                    case 1:
                        getType = getHeirarchyData[1].keyValue.ToString();
                        heirarchyList = new heirarchyLevelList();
                        heirarchyList.keyName = data.value;
                        heirarchyList.value = new List<KeyValueData>();
                        heirarchy.Add(heirarchyList);
                        break;
                    case 2:
                        getType = getHeirarchyData[2].keyValue.ToString();
                        heirarchyList = new heirarchyLevelList();
                        heirarchyList.keyName = data.value;
                        heirarchyList.value = new List<KeyValueData>();
                        heirarchy.Add(heirarchyList);
                        break;
                    case 3:
                        getType = getHeirarchyData[3].keyValue.ToString();
                        heirarchyList = new heirarchyLevelList();
                        heirarchyList.keyName = data.value;
                        heirarchyList.value = new List<KeyValueData>();
                        heirarchy.Add(heirarchyList);
                        break;
                }
            }
            
            
           
            var jsonResult = new
            {
                rsBody = new
                {
                    result = getHeirarchyData,
                    level = heirarchy,
                    message = "Success"
                }
            };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

    }
}