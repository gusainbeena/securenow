﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecureNow.Controllers
{
    public class ManageDataController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
       
        public ActionResult LeftSidebar()
        {
            return View();
        }
        public ActionResult AccessControl()
        {
            return View();
        }
        public ActionResult BatchProcess()
        {
            return View();
        }
        public ActionResult MyDashboard()
        {
            return View();
        }
        public ActionResult BarGraph()
        {
            return View();
        }
        public ActionResult PieChart()
        {
            return View();
        }
        public ActionResult CreateEmployee()
        {
            return View();
        }

        public ActionResult ViewEmployee()
        {
            return View();
        }
        public ActionResult OpUpload()
        {
            return View();
        }
        public ActionResult ActualUpload()
        {
            return View();
        }
        public ActionResult OverdueUpload()
        {
            return View();
        }
        public ActionResult MassUpload()
        {
            return View();
        }
        public ActionResult ConfigureOpTemplate()
        {
            return View();
        }
        public ActionResult ConfigureActualTemplate()
        {
            return View();
        }
        public ActionResult ConfigureMetaData()
        {
            return View();
        }
        public ActionResult CreateCustomer()
        {
            return View();
        }
        public ActionResult ViewCustomer()
        {
            return View();
        }
        public ActionResult CreateIncentive()
        {
            return View();
        }
        public ActionResult Viewincentive()
        {
            return View();
        }
        public ActionResult PaySlip()
        {
            return View();
        }

        public ActionResult GlobalDashboard()
        {
            return View();
        }        
        public ActionResult ViewActualApprovalList()
        {
            return View();
        }
    }
}