﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SecureNow.Controllers
{
    public class CommonController : Controller
    {
        DBModels db = new DBModels();
        // GET: Comman
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetMonthKeyValueData()
        {
           
            int currentMonth = DateTime.Now.Month;
            List<KeyValueData> resulats = new List<KeyValueData>();

            KeyValueData cfy = (from fy in db.keyValueData
                                where fy.keyType.Equals("OPERATIONAL")
                                && fy.keyName.Equals("CFY")
                                select fy).FirstOrDefault();
            int year = Convert.ToInt32(cfy.keyValue);

            string quarter = (from d in db.keyValueData
                              where d.keyType.Equals("OPERATIONAL")
                              && d.keyName.Equals("CFQ")
                              select d.keyValue).FirstOrDefault();

            int count = 0;
            int countEnd = 0;
            if (quarter != null)
            {
                KeyValueData keyData = (from k in db.keyValueData
                                        where k.keyType.Equals("QTR")
                                        && k.keyName.Equals(quarter)
                                        select k).FirstOrDefault();
                int mon = Convert.ToInt32(keyData.relatedKeyValue);
                count = mon + 1;
                countEnd = count + 2;
            }
            else
            {
                count = 1;
                countEnd = 12;
            }

            //int count = Convert.ToInt32(keyData.relatedKeyType);
            for (int i = count; i <= countEnd; i++)
            {
                string month = i.ToString();
                KeyValueData monthData = (from d in db.keyValueData
                                          where (d.keyType.Equals("MOY")
                                          && d.relatedKeyType != "NONE"
                                          && d.keyValue.Equals(month))
                                          select (d)).FirstOrDefault();

                resulats.Add(monthData);
            }
            


            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = resulats,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetKeyValueData()
        {
            Stream req = Request.InputStream;

            KeyValueData customerDetails = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));

            IncentiveParameter incentiveDetails = (IncentiveParameter)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveParameter));

            int page = 0;
            string message = null;

            List<IncentiveParameter> incentive = null;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;  

            List<KeyValueData> keyValueData = (from d in db.keyValueData
                                               select d).ToList();            

            List<KeyValueData> resulats = new List<KeyValueData>();
            List<String> keyTypes = new List<String>();
            using (DBModels dbData = new DBModels())
            {
                object jsonsuccess = null;
                if (customerDetails.keyType != null)
                {
                    if (customerDetails.keyType == "YEAR")
                    {
                        KeyValueData cFyYear = (from y in dbData.keyValueData
                                                where y.keyType.Equals("OPERATIONAL")
                                                && y.keyName.Equals("CFY")
                                                select y).FirstOrDefault();

                        KeyValueData cAyYear = (from y in dbData.keyValueData
                                                where y.keyType.Equals("OPERATIONAL")
                                                && y.keyName.Equals("CAY")
                                                select y).FirstOrDefault();
                        if (cFyYear.keyValue == cAyYear.keyValue)
                        {
                            resulats.Add(cFyYear);
                        }
                        else
                        {
                            resulats.Add(cAyYear);
                            resulats.Add(cFyYear);

                        }
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "QUARTER")
                    {
                        resulats = (from d in dbData.keyValueData
                                    where (d.keyType.Equals("QTR")
                                    && d.relatedKeyType != "NONE"
                                    && d.keyName != "H1"
                                    && d.keyName != "H2"
                                    && d.keyName != "FY")
                                    orderby d.id ascending
                                    select (d)).ToList();

                        KeyValueData cq = (from k in db.keyValueData where k.keyType.Equals("CURRENTQUARTER") && k.keyName.Equals("CFY") select k).FirstOrDefault();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                currentQuarter = cq,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "STATE")
                    {
                        resulats = (from d in dbData.keyValueData
                                    where (d.keyType.Equals(customerDetails.keyType)
                                    )
                                    orderby d.keyValue ascending
                                    select (d)).ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "TARGETKEY")
                    {
                        resulats = (from d in db.keyValueData
                                    where (d.keyType.Equals("MetaData")
                                    && d.keyName == "SalesComponent") && d.relatedKeyType != "Incentive"
                                    orderby d.id ascending
                                    select (d)).ToList();

                        KeyValueData anotherComponenet = (from e in dbData.keyValueData where e.keyType == "TARGETDIVIDE" && e.keyName == "FORTNIGHT" select e).FirstOrDefault();

                        resulats.Add(anotherComponenet);

                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "KeyType")
                    {
                        keyTypes = (from k in dbData.keyValueData
                                    select k.keyType).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                keyTypes = keyTypes,
                                result = "success"
                            }
                        };


                    }
                    else if (customerDetails.keyType == "CHANNEL")
                    {
                        resulats = (from k in dbData.keyValueData
                                    where k.keyType.Equals("CHANNELLIST") && k.keyName.Equals(customerDetails.keyType)
                                    select k).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };


                    }
                    else if (customerDetails.keyType == "MOY")
                    {
                        resulats = (from k in dbData.keyValueData
                                    where k.keyType.Equals(customerDetails.keyType)
                                    select k).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };


                    }
                    else if (customerDetails.keyType == "TARGETTYPE")
                    {
                        List<KeyValueData> keyTypeData = (from k in dbData.keyValueData
                                                          where k.keyType.Equals(customerDetails.keyType)
                                && k.keyName.Equals(customerDetails.keyType)
                                                          select k).Distinct().ToList();
                    }
                    else
                    {
                        if (customerDetails.keyName == "CHANNEL")
                        {
                            resulats = (from d in dbData.keyValueData
                                        where (d.keyType.Equals(customerDetails.keyType)
                                        && d.keyName != "ALL"
                                        && d.keyName != "CFY")
                                        orderby d.id ascending
                                        select (d)).ToList();

                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    resulats = resulats,
                                    result = "success"
                                }
                            };
                        }
                        else
                        {
                            resulats = (from d in dbData.keyValueData
                                        where d.keyType.Equals(customerDetails.keyType)
                                        select d).Distinct().ToList();

                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    resulats = resulats,
                                    result = "success"
                                }
                            };
                        }

                    }
                }
                else
                {

                    keyTypes = (from k in dbData.keyValueData
                                select k.keyType).Distinct().ToList();
                    List<String> channelList = new List<String>();
                    List<String> subChannelList = new List<String>();
                    List<String> roleList = new List<String>();
                    List<String> actualType = new List<String>();
                    List<String> keyName = new List<String>();
                    List<String> booster = new List<String>();
                    subChannelList = (from k in dbData.keyValueData
                                      where k.keyName == "SUBCHANNEL"
                                      select k.keyValue).Distinct().ToList();
                    roleList = (from k in dbData.keyValueData
                                where k.keyName == "ROLE"
                                select k.keyValue).Distinct().ToList();
                    channelList = (from k in dbData.keyValueData
                                   where k.keyType == "CHANNELLIST"
                                   select k.keyValue).Distinct().ToList();
                    actualType = (from k in dbData.IncentiveParameter
                                  select k.actualType).Distinct().ToList();
                    keyName = (from k in dbData.IncentiveParameter
                               select k.keyName).Distinct().ToList();
                    booster = (from k in dbData.IncentiveParameter
                               select k.keyType).Distinct().ToList();
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            channelList = channelList,
                            incentive = incentive,
                            result = "success"
                        }
                    };

                }
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }

        }
        //Download Operation Plan Error Template
        public void DownloadOpErrorReport()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "OperationalPlanDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.errorFileFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Sales Source Template
        public void DownloadOdSourceReport()
        {
            string filePath = Request.QueryString["name"];
            // string uploadType = Request.QueryString["uploadType"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "SalesDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Sales Error Template
        public void DownloadOdErrorReport()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "SalesDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.errorFileFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Over Due Source Template
        public void DownloadOverdSourceReport()
        {
            string filePath = Request.QueryString["name"];
            // string uploadType = Request.QueryString["uploadType"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "OverDueDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();

        }

        //Download Sales Error Template
        public void DownloadOverdErrorReport()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "OverDueDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.errorFileFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Employee Source Template
        public void DownloadEmpSourceReport()
        {
            string filePath = Request.QueryString["name"];
            // string uploadType = Request.QueryString["uploadType"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "EmployeeMasterDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Employee Error Template
        public void DownloadEmpErrorReport()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "EmployeeMasterDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.errorFileFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Operational plan Source Template
        public void DownloadOpSourceReport()
        {
            string filePath = Request.QueryString["name"];
            // string uploadType = Request.QueryString["uploadType"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "OperationalPlanDataPort"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Sales Error Template
        public void DownloadSeconderyOperationalPlanDataPortErrorReport()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.uploadType.SecondaryOperationalPlanDataPort.ToString()
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.errorFileFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        //Download Upload Template
        public void DownloadTemplate()
        {
            string filePath = Request.QueryString["name"];

            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == "Template"
                                                   select p).FirstOrDefault();

            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();


        }

        public static MonthDTO getMonthList(string period)
        {
            MonthDTO month = new MonthDTO();
            switch (period)
            {

                case "Q1":
                    month.startMonth = 1;
                    month.endMonth = 3;
                    break;

                case "Q2":
                    month.startMonth = 4;
                    month.endMonth = 6;
                    break;

                case "Q3":
                    month.startMonth = 7;
                    month.endMonth = 9;
                    break;

                case "Q4":
                    month.startMonth = 10;
                    month.endMonth = 12;
                    break;

                case "H1":
                    month.startMonth = 1;
                    month.endMonth = 6;
                    break;

                case "H2":
                    month.startMonth = 7;
                    month.endMonth = 12;
                    break;

                case "FY":
                    month.startMonth = 1;
                    month.endMonth = 12;
                    break;

                case "YTD-Jan":
                case "YTD-January":
                    month.startMonth = 1;
                    month.endMonth = 1;
                    break;

                case "YTD-Feb":
                case "YTD-Febuary":
                    month.startMonth = 1;
                    month.endMonth = 2;
                    break;

                case "YTD-Mar":
                case "YTD-March":
                    month.startMonth = 1;
                    month.endMonth = 3;
                    break;

                case "YTD-Apr":
                case "YTD-April":
                    month.startMonth = 1;
                    month.endMonth = 4;
                    break;

                case "YTD-May":
                    month.startMonth = 1;
                    month.endMonth = 5;
                    break;

                case "YTD-Jun":
                case "YTD-June":
                    month.startMonth = 1;
                    month.endMonth = 6;
                    break;

                case "YTD-Jul":
                case "YTD-July":
                    month.startMonth = 1;
                    month.endMonth = 7;
                    break;

                case "YTD-Aug":
                case "YTD-August":
                    month.startMonth = 1;
                    month.endMonth = 8;
                    break;

                case "YTD-Sep":
                case "YTD-September":
                    month.startMonth = 1;
                    month.endMonth = 9;
                    break;

                case "YTD-Oct":
                case "YTD-October":
                    month.startMonth = 1;
                    month.endMonth = 10;
                    break;

                case "YTD-Nov":
                case "YTD-November":
                    month.startMonth = 1;
                    month.endMonth = 11;
                    break;

                case "YTD-Dec":
                case "YTD-December":
                    month.startMonth = 1;
                    month.endMonth = 12;
                    break;

                case "Jan":
                    month.startMonth = 1;
                    month.endMonth = 1;
                    break;

                case "Feb":
                    month.startMonth = 2;
                    month.endMonth = 2;
                    break;

                case "Mar":
                    month.startMonth = 3;
                    month.endMonth = 3;
                    break;

                case "Apr":
                    month.startMonth = 4;
                    month.endMonth = 4;
                    break;

                case "May":
                    month.startMonth = 5;
                    month.endMonth = 5;
                    break;

                case "Jun":
                    month.startMonth = 6;
                    month.endMonth = 6;
                    break;

                case "Jul":
                    month.startMonth = 7;
                    month.endMonth = 7;
                    break;

                case "Aug":
                    month.startMonth = 8;
                    month.endMonth = 8;
                    break;

                case "Sep":
                    month.startMonth = 9;
                    month.endMonth = 9;
                    break;

                case "Oct":
                    month.startMonth = 10;
                    month.endMonth = 10;
                    break;

                case "Nov":
                    month.startMonth = 11;
                    month.endMonth = 11;
                    break;

                case "Dec":
                    month.startMonth = 12;
                    month.endMonth = 12;
                    break;
            }

            return month;
        }

        public static MonthDTO getMonthListFortnight(string period)
        {
            DBModels db = new DBModels();
            List<string> quarter = (from e in db.keyValueData where e.keyType == "QTR" select e.keyValue).ToList();
            if (!quarter.Contains(period))
            {
                string getCurrentQtr = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
                period = getCurrentQtr;
            }

            MonthDTO month = new MonthDTO();
            month.quarter = period;
            switch (period)
            {
                case "Q1":
                    month.startMonth = 1;
                    month.endMonth = 3;
                    break;

                case "Q2":
                    month.startMonth = 4;
                    month.endMonth = 6;
                    break;

                case "Q3":
                    month.startMonth = 7;
                    month.endMonth = 9;
                    break;

                case "Q4":
                    month.startMonth = 10;
                    month.endMonth = 12;
                    break;
                case "FY":
                    month.startMonth = 1;
                    month.endMonth = 12;
                    break;
            }

            return month;
        }
        //Get quarterList
        public static List<string> quarterListData(List<string> quarterList, int minRoleMon, int maxRoleMon)
        {
            for (int i = minRoleMon; i <= maxRoleMon; i++)
            {
                string qtr = "";
                switch (i)
                {
                    case 1:
                    case 2:
                    case 3:
                        qtr = "Q1";
                        if (!quarterList.Contains(qtr))
                        {
                            quarterList.Add(qtr);
                        }
                        break;

                    case 4:
                    case 5:
                    case 6:
                        qtr = "Q2";
                        if (!quarterList.Contains(qtr))
                        {
                            quarterList.Add(qtr);
                        }
                        break;

                    case 7:
                    case 8:
                    case 9:
                        qtr = "Q3";
                        if (!quarterList.Contains(qtr))
                        {
                            quarterList.Add(qtr);
                        }
                        break;

                    case 10:
                    case 11:
                        qtr = "Q4";
                        if (!quarterList.Contains(qtr))
                        {
                            quarterList.Add(qtr);
                        }

                        break;
                    case 12:
                        qtr = "Q4";
                        if (!quarterList.Contains(qtr))
                        {
                            quarterList.Add(qtr);
                        }
                        if (!quarterList.Contains("FY"))
                        {
                            quarterList.Add("FY");
                        }
                        break;
                }

            }
            return quarterList;
        }

    }
}