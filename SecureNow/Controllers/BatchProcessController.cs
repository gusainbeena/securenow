﻿using SecureNow.Container;
using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using NodaTime.Text;
using NodaTime;
using System.Web.Mvc;
using SecureNow.utilities;

namespace SecureNow.Controllers
{
    public class BatchProcessController : Controller
    {
        public static DBModels db = new DBModels();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BatchProcess()
        {
            return View();
        }
        public JsonResult GetUserDetails()
        {
            string userName = HttpContext.User.Identity.Name;
            int page = 0;
            long date = DateTime.Now.Ticks;
            DateTime nowDate = new DateTime(date);
            DateTime yesterday = DateTime.Today.AddDays(-1);
            string tempDate = nowDate.ToString("yyyy-MM-dd");
            string[] batchDate = tempDate.Split('-');
            long ticks = yesterday.Ticks;
            string previousDay = ticks.ToString();
            List<string> OperationalUnits = new List<string>();
            int parentOU = 0;
            List<string> ou = new List<string>();
            var pageSize = 8;
            var skip = pageSize * page;
            List<AttributesLink> childOU = new List<AttributesLink>();
            KeyValueData TopLevel = (from e in db.keyValueData /*where e.keyValueModel == "OU1"*/ select e).FirstOrDefault();
            List<string> ListedTopOU = new List<string>();
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            if (user.operationalUnit == "EARTH")
            {
                ListedTopOU = (from e in db.keyValueData where e.keyType == TopLevel.keyValue 
                               && e.keyName == TopLevel.keyName select e.labelOnScreen).ToList();
                parentOU = (from e in db.keyValueData where e.keyValue == "INDIA" && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add("INDIA");
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                
                //var skip = pageSize * page;
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            else
            {
                parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit 
                            && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(user.operationalUnit);
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                
                //var skip = pageSize * page;
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            
            
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }
            List<BatchRunningProcess> bprocess = new List<BatchRunningProcess>();
            List<int> sequence = (from e in db.batchRunningProcess where e.date == previousDay select e.sequence).Distinct().ToList();
            foreach(int i in sequence)
            {
                int maxId = (from e in db.batchRunningProcess where e.sequence == i && e.date == previousDay select e.id).Max();
                BatchRunningProcess brp = (from e in db.batchRunningProcess where e.date == previousDay && e.sequence == i && e.id == maxId select e).FirstOrDefault();
                bprocess.Add(brp);
            }
            //List<BatchRunningProcess> bprocess = (from e in db.batchRunningProcesses where e.date == previousDay select e).Distinct().ToList();
            List<DataUploadBatchRequests> batchRequests = (from e in db.dataUploadBatchRequests where ouString.Contains(e.operationalUnit) && e.status != "ERROR" orderby e.id descending select e).Skip(skip).Take(pageSize).ToList();
            List<DataUploadBatchParameters> batchParameters = (from e in db.dataUploadBatchParameters where e.uploadType != "masterBatch" && e.status != null orderby e.uploadType ascending select e).Skip(skip).Take(pageSize).ToList();
            List<DataUploadBatchRequestDTO> batchRequestDTO = new List<DataUploadBatchRequestDTO>();
            DataUploadBatchRequestDTO batchDTO = new DataUploadBatchRequestDTO();
            foreach (DataUploadBatchRequests requests in batchRequests)
            {
                batchDTO.id = requests.id;
                batchDTO.batchName = requests.batchName;
                DateTime dt = new DateTime(requests.timeOfRequest);
                batchDTO.timeOfRequest = dt.ToShortDateString();
                batchDTO.status = requests.status;
                string UTCTime = requests.scheduleTime;
                batchDTO.requestorAdpId = (from e in db.employeeBaseDetails where e.adpId == requests.requestorAdpId select e.fullName).FirstOrDefault();
                if (requests.scheduleTime != null)
                {
                    string[] h = UTCTime.Split(':');
                    int hour = Convert.ToInt32(h[0]);
                    string ampm = null;
                    if (hour > 12)
                    {
                        hour = hour - 12;
                        ampm = "PM";
                    }
                    if (hour == 00)
                    {
                        hour = hour + 12;
                        ampm = "AM";
                    }
                    var length = h.Length;
                    var left = h[length - 1];
                    UTCTime = hour.ToString() + ":" + left + " " + ampm;
                    batchDTO.scheduleTime = UTCTime;
                }
                else
                {
                    batchDTO.scheduleTime = dt.ToShortTimeString();
                }

                batchDTO.operationalUnit = requests.operationalUnit.ToUpper();
                batchRequestDTO.Add(batchDTO);
                batchDTO = new DataUploadBatchRequestDTO();

            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    user = user,
                    //batchRequests = batchRequestDTO,
                    batchDate = batchDate,
                    //batchParameters = batchParameters,
                    bprocess = bprocess,
                    currentOU = currentOU,
                    allOU = ListedTopOU,
                    selectedOU = "INDIA"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RescheduleBatch()
        {
            Stream req = Request.InputStream;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            KeyValueData parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e).FirstOrDefault();
            string timeZone = parentOU.relatedKeyValue;
            string UTCTime = null;
            DataUploadBatchRequests batchRequests = (DataUploadBatchRequests)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadBatchRequests));
            DataUploadBatchRequests request = (from e in db.dataUploadBatchRequests where e.id == batchRequests.id select e).FirstOrDefault();
            DataUploadBatchRequestDTO requestDTO = new DataUploadBatchRequestDTO();
            requestDTO.id = request.id;
            DateTime dt = new DateTime(request.timeOfRequest);
            UTCTime = request.scheduleTime;
            if (timeZone != "Asia/Kolkata")
            {
                DateTime date = DateTime.ParseExact(UTCTime, "h:mm tt", CultureInfo.InvariantCulture);            //string utc = dt.ToFileTimeUtc().ToString();
                UTCTime = LocalTimeToUTC(timeZone, date.ToString("HH:mm:ss"));
                int hour = Convert.ToInt32(UTCTime.Substring(0, 2));
                string ampm = null;
                if (hour > 12)
                {
                    hour = hour - 12;
                    ampm = "PM";
                }
                if (hour == 00)
                {
                    hour = hour + 12;
                    ampm = "AM";
                }
                var left = UTCTime.Substring(2, 5);
                UTCTime = hour.ToString() + left + " " + ampm;
            }
            requestDTO.timeOfRequest = dt.ToShortDateString();
            if (request.scheduleTime != null)
            {
                string[] h = UTCTime.Split(':');
                int hour = Convert.ToInt32(h[0]);
                string ampm = null;
                if (hour > 12)
                {
                    hour = hour - 12;
                    ampm = "PM";
                }
                if (hour == 00)
                {
                    hour = hour + 12;
                    ampm = "AM";
                }
                var length = h.Length;
                var left = h[length - 1];
                UTCTime = hour.ToString() + ":" + left + " " + ampm;
                requestDTO.scheduleTime = UTCTime;
            }
            else
            {
                requestDTO.scheduleTime = dt.ToString("HH:mm:ss");
            }
            requestDTO.batchName = request.batchName.ToUpper();
            requestDTO.requestorAdpId = ((from e in db.employeeBaseDetails where e.adpId == request.requestorAdpId select e.fullName).FirstOrDefault());
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    request = requestDTO
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResetTime()
        {
            Stream req = Request.InputStream;
            DBModels db = new DBModels();
            DataUploadBatchRequests batchRequests = (DataUploadBatchRequests)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadBatchRequests));
            DataUploadBatchRequests request = (from e in db.dataUploadBatchRequests where e.id == batchRequests.id select e).FirstOrDefault();
            DataUploadBatchRequestDTO requestDTO = new DataUploadBatchRequestDTO();
            DataUploadBatchRequests changeRrequest = new DataUploadBatchRequests();
            requestDTO.id = request.id;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            KeyValueData parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e).FirstOrDefault();
            string timeZone = parentOU.relatedKeyValue;
            DateTime dt = new DateTime(request.timeOfRequest);
            string UTCTime = batchRequests.scheduleTime;
            if (timeZone != "Asia/Kolkata")
            {
                DateTime date = DateTime.ParseExact(UTCTime, "h:mm tt", CultureInfo.InvariantCulture);
                UTCTime = LocalTimeToUTC(timeZone, date.ToString("HH:mm:ss"));
            }

            requestDTO.timeOfRequest = dt.ToShortDateString();

            changeRrequest.batchName = request.batchName;
            requestDTO.batchName = request.batchName;
            changeRrequest.requestorAdpId = ((from e in db.employeeBaseDetails where e.adpId == request.requestorAdpId select e.adpId).FirstOrDefault());
            requestDTO.requestorAdpId = ((from e in db.employeeBaseDetails where e.adpId == request.requestorAdpId select e.adpId).FirstOrDefault());
            if (request.scheduleTime != null)
            {
                string[] h = UTCTime.Split(':');
                int hour = Convert.ToInt32(h[0]);
                string ampm = null;
                if (hour > 12)
                {
                    hour = hour - 12;
                    ampm = "PM";
                }
                if (hour == 00)
                {
                    hour = hour + 12;
                    ampm = "AM";
                }
                var length = h.Length;
                var left = h[length - 1];
                UTCTime = hour.ToString() + ":" + left + " " + ampm;
                changeRrequest.scheduleTime = UTCTime;
                requestDTO.scheduleTime = UTCTime.Trim();
                request.scheduleTime = UTCTime.Trim();
            }
            else
            {
                changeRrequest.scheduleTime = "";
                requestDTO.scheduleTime = "";
            }
            db.SaveChanges();
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    request = requestDTO
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BatchDataList()
        {/*
            Stream req = Request.InputStream;
            List<DataUploadBatchRequestDTO> employeeMasterList = new List<DataUploadBatchRequestDTO>();
            //get the userId from the session
            IQueryable<DataUploadBatchRequestDTO> results = null;
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            string requestorAdpId = (from d in db.employeeBaseDetails where d.email.Equals(userName) select d.adpId).FirstOrDefault();
            string operationalUnit = (from d in db.employeeBaseDetails where d.email.Equals(userName) select d.operationalUnit).FirstOrDefault();
            int parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
            List<string> ou = new List<string>();
            ou.Add(user.operationalUnit);
            DataUploadBatchRequestDTO reportDetails = (DataUploadBatchRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadBatchRequestDTO));
            int page = reportDetails.page;
            DateTime dt = new DateTime();
            string requestType = reportDetails.requestType;
            if (page != null)
            {
                page = page;
            }
            else
            {
                page = 0;
            }
            var pageSize = 8;

            var skip = pageSize * page;
            List<AttributesLink> childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }

            results = (from d in db.dataUploadBatchRequests
                       where d.status != "ERROR" &&
                       ouString.Contains(d.operationalUnit)
                       orderby d.id descending
                       select new DataUploadBatchRequestDTO
                       {
                           id = d.id,
                           status = d.status,
                           timeOfRequest = d.timeOfRequest.ToString(),
                           operationalUnit = d.operationalUnit,
                           batchName = d.batchName,
                           requestorAdpId = d.requestorAdpId,// ((from e in db.employeeBaseDetails where e.adpId == requestorAdpId select e.fullName).FirstOrDefault()),
                           scheduleTime = d.scheduleTime
                       }).Skip(skip).Take(pageSize);

            foreach (DataUploadBatchRequestDTO item in results)
            {
                DataUploadBatchRequestDTO sendBack = new DataUploadBatchRequestDTO();
                sendBack.status = item.status;
                long requestTime = Convert.ToInt64(item.timeOfRequest);
                dt = new DateTime(requestTime);
                sendBack.id = item.id;
                sendBack.batchName = item.batchName;
                sendBack.timeOfRequest = dt.ToShortDateString();
                string UTCTime = item.scheduleTime;
                if (item.scheduleTime != null)
                {
                    //int hour = 0; //Convert.ToInt32(UTCTime.Substring(0, 2));
                    var h = UTCTime.Split(':');
                    int hour = Convert.ToInt32(h[0]);
                    string ampm = null;
                    if (hour > 12)
                    {
                        hour = hour - 12;
                        ampm = "PM";
                    }
                    if (hour == 00)
                    {
                        hour = hour + 12;
                        ampm = "AM";
                    }
                    var length = h.Length;//UTCTime.Substring(h[0], 6);
                    var left = h[length - 1];
                    UTCTime = hour.ToString() + ":" + left + " " + ampm;
                    sendBack.scheduleTime = UTCTime;
                }
                else
                {
                    sendBack.scheduleTime = dt.ToShortTimeString();
                }

                sendBack.operationalUnit = item.operationalUnit.ToUpper();
                sendBack.requestorAdpId = ((from e in db.employeeBaseDetails where e.adpId == item.requestorAdpId select e.fullName).FirstOrDefault());
                employeeMasterList.Add(sendBack);
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    data = employeeMasterList
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);*/
            Stream req = Request.InputStream;
            DBModels db = new DBModels();
            string userName = HttpContext.User.Identity.Name;
            var pageSize = 8;
            //string selectedOU = data.selectOU;
            int page = 0;
            int parentOU = 0;
            List<string> ou = new List<string>();
            List<AttributesLink> childOU = new List<AttributesLink>();
            var skip = pageSize * page;
            BatchRunningProcessDTO bpr = (BatchRunningProcessDTO)Utility.mapRequestToClass(Request.InputStream, typeof(BatchRunningProcessDTO));
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            
            
            if (user.operationalUnit == "EARTH")
            {
                parentOU = (from e in db.keyValueData where e.keyValue == bpr.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(bpr.operationalUnit);
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            else
            {
                parentOU = (from e in db.keyValueData where e.keyValue == bpr.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(user.operationalUnit);
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }
            int prev = 0;
            List<BatchRunningProcessDTO> bprocessDTO = new List<BatchRunningProcessDTO>();
            BatchRunningProcessDTO batchProcessDTO = new BatchRunningProcessDTO();
            List<BatchRunningProcess> brp = new List<BatchRunningProcess>();
            string batchName = (from e in db.batchRunningProcess where e.id == bpr.id select e.batchName).FirstOrDefault();
            List<BatchRunningProcess> bprocess = (from e in db.batchRunningProcess where ouString.Contains(e.operationalUnit) && e.batchName == batchName orderby e.date descending select e).ToList();
            foreach (BatchRunningProcess process in bprocess)
            {
                int maxId = (from e in db.batchRunningProcess where e.batchName == process.batchName && e.date == process.date select e.id).Max();
                BatchRunningProcess batchProcess = (from e in db.batchRunningProcess where e.id == maxId select e).FirstOrDefault();
                batchProcessDTO.id = batchProcess.id;
                batchProcessDTO.batchName = batchProcess.batchName;
                batchProcessDTO.startTime = batchProcess.startTime;
                batchProcessDTO.endTime = batchProcess.endTime;
                batchProcessDTO.operationalUnit = batchProcess.operationalUnit;
                long tempDate = Convert.ToInt64(batchProcess.date);
                DateTime nowDate = new DateTime(tempDate);
                batchProcessDTO.date = nowDate.ToString("yyyy-MM-dd");
                if (batchProcess.executionTime != null)
                {
                    string[] duration = batchProcess.executionTime.Split(':');
                    string executionTime = duration[0] + " Hrs " + duration[1] + " Mins";
                    batchProcessDTO.executionTime = executionTime;
                }
                else
                {
                    string executionTime = "00 Hrs 00 Mins";
                    batchProcessDTO.executionTime = executionTime;
                }
                batchProcessDTO.status = batchProcess.status;
                if (bprocessDTO.Count != 0)
                {
                    prev = bprocessDTO[bprocessDTO.Count - 1].id;
                }
                else
                {
                    prev = bprocessDTO.Count;
                }
                if (batchProcessDTO.id != prev)
                {
                    bprocessDTO.Add(batchProcessDTO);
                }



                batchProcessDTO = new BatchRunningProcessDTO();
            }

            bprocessDTO = bprocessDTO.Distinct().ToList();
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    bprocessDTO = bprocessDTO
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        
    }
        public JsonResult BatchLogs()
        {
            Stream req = Request.InputStream;
            DBModels db = new DBModels();
            string logsAvail = null;
            DataUploadBatchRequests batchRequests = (DataUploadBatchRequests)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadBatchRequests));
            DataUploadBatchRequests request = (from e in db.dataUploadBatchRequests where e.id == batchRequests.id select e).FirstOrDefault();
            if (request.errorLogFileName != null || request.errorLogFileName != "")
            {

            }
            else
            {
                logsAvail = "error";
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    request = request,
                    logsAvail = logsAvail
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        static string LocalTimeToUTC(string timeZone, string localDateTime)
        {
            var pattern = LocalDateTimePattern.CreateWithInvariantCulture("HH:mm:ss");
            LocalDateTime ldt = pattern.Parse(localDateTime).Value;
            ZonedDateTime zdt = ldt.InZoneLeniently(DateTimeZoneProviders.Tzdb[timeZone]);
            Instant instant = zdt.ToInstant();
            //instant = SystemClock.Instance.GetCurrentInstant();
            ZonedDateTime utc = instant.InUtc();
            string output = utc.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
            return output;

        }
        public JsonResult BatchParameterList()
        {
            string userName = HttpContext.User.Identity.Name;
            int page = 0;
            long date = DateTime.Now.Ticks;
            DateTime nowDate = new DateTime(date);
            string tempDate = nowDate.ToString("yyyy-MM-dd");
            string[] batchDate = tempDate.Split('-');
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            DataUploadRequestParameterDTO reportDetails = (DataUploadRequestParameterDTO)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadRequestParameterDTO));
            page = reportDetails.page;
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            if (page != null)
            {
                page = page;
            }
            else
            {
                page = 0;
            }
            var pageSize = 8;

            var skip = pageSize * page;


            List<DataUploadBatchParameters> batchParameters = (from e in db.dataUploadBatchParameters where e.uploadType != "masterBatch" && e.status != null orderby e.uploadType ascending select e).Skip(skip).Take(pageSize).ToList();
            DataUploadRequestParameterDTO batchDTO = new DataUploadRequestParameterDTO();
            foreach (DataUploadBatchParameters requests in batchParameters)
            {
                batchDTO.id = requests.id;
                batchDTO.uploadType = requests.uploadType;
                batchDTO.status = requests.status;
                batchDTO = new DataUploadRequestParameterDTO();
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    user = user,
                    batchDate = batchDate,
                    batchParameters = batchParameters,
                    currentOU = currentOU
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListedBatch()
        {
            string userName = HttpContext.User.Identity.Name;
            int page = 0;
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            DataUploadBatchParameters parameters = (DataUploadBatchParameters)Utility.mapRequestToClass(Request.InputStream, typeof(DataUploadBatchParameters));
            int parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
            List<string> ou = new List<string>();
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            ou.Add(user.operationalUnit);
            if (page != null)
            {
                page = page;
            }
            else
            {
                page = 0;
            }
            var pageSize = 8;

            var skip = pageSize * page;
            List<AttributesLink> childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }
            string uploadType = (from e in db.dataUploadBatchParameters where e.id == parameters.id select e.uploadType).FirstOrDefault();
            List<DataUploadBatchRequests> batchRequests = (from e in db.dataUploadBatchRequests where ouString.Contains(e.operationalUnit) && e.requestType == uploadType && e.status != "ERROR" orderby e.id descending select e).Skip(skip).Take(pageSize).ToList();
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    batchRequests = batchRequests
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchData()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody));
            string gDate = data.batchDate;
            DateTime dateTime = DateTime.Parse(gDate);
            long gTicks = dateTime.Ticks;
            string userName = HttpContext.User.Identity.Name;
            string selectedOU = data.selectOU;
            int page = 0;
            int parentOU = 0;
            List<string> ou = new List<string>();
            List<AttributesLink> childOU = new List<AttributesLink>();
            //long date = DateTime.Now.Ticks;
            //DateTime nowDate = new DateTime(date);
            //DateTime yesterday = DateTime.Today.AddDays(-1);
            //string tempDate = nowDate.ToString("yyyy-MM-dd");
            //string[] batchDate = tempDate.Split('-');
            //long ticks = yesterday.Ticks;
            string previousDay = gTicks.ToString();
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            if(user.operationalUnit == "EARTH")
            {
                parentOU = (from e in db.keyValueData where e.keyValue == selectedOU && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(selectedOU);
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                var pageSize = 8;

                var skip = pageSize * page;
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            else
            {
                parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(user.operationalUnit);
                if (page != null)
                {
                    page = page;
                }
                else
                {
                    page = 0;
                }
                var pageSize = 8;

                var skip = pageSize * page;
                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
             
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }
            List<BatchRunningProcess> bprocess = new List<BatchRunningProcess>();
            List<int> sequence = (from e in db.batchRunningProcess where e.date == previousDay && ouString.Contains(e.operationalUnit) select e.sequence ).Distinct().ToList();
            foreach (int i in sequence)
            {
                int maxId = (from e in db.batchRunningProcess where e.sequence == i && e.date == previousDay && ouString.Contains(e.operationalUnit) select e.id).Max();
                BatchRunningProcess brp = (from e in db.batchRunningProcess where e.date == previousDay &&  ouString.Contains(e.operationalUnit) && e.sequence == i && e.id == maxId select e).FirstOrDefault();
                bprocess.Add(brp);
            }
            //List<BatchRunningProcess> bprocess = (from e in db.batchRunningProcesses where e.date == previousDay select e).Distinct().ToList();
            //List<DataUploadBatchRequests> batchRequests = (from e in db.dataUploadBatchRequests where ouString.Contains(e.operationalUnit) && e.status != "ERROR" orderby e.id descending select e).Skip(skip).Take(pageSize).ToList();
            //List<DataUploadBatchParameters> batchParameters = (from e in db.dataUploadBatchParameters where e.uploadType != "masterBatch" && e.status != null orderby e.uploadType ascending select e).Skip(skip).Take(pageSize).ToList();
            //List<DataUploadBatchRequestDTO> batchRequestDTO = new List<DataUploadBatchRequestDTO>();
            //DataUploadBatchRequestDTO batchDTO = new DataUploadBatchRequestDTO();
            /*
            foreach (DataUploadBatchRequests requests in batchRequests)
            {
                batchDTO.id = requests.id;
                batchDTO.batchName = requests.batchName;
                DateTime dt = new DateTime(requests.timeOfRequest);
                batchDTO.timeOfRequest = dt.ToShortDateString();
                batchDTO.status = requests.status;
                string UTCTime = requests.scheduleTime;
                batchDTO.requestorAdpId = (from e in db.employeeBaseDetails where e.adpId == requests.requestorAdpId select e.fullName).FirstOrDefault();
                if (requests.scheduleTime != null)
                {
                    string[] h = UTCTime.Split(':');
                    int hour = Convert.ToInt32(h[0]);
                    string ampm = null;
                    if (hour > 12)
                    {
                        hour = hour - 12;
                        ampm = "PM";
                    }
                    if (hour == 00)
                    {
                        hour = hour + 12;
                        ampm = "AM";
                    }
                    var length = h.Length;
                    var left = h[length - 1];
                    UTCTime = hour.ToString() + ":" + left + " " + ampm;
                    batchDTO.scheduleTime = UTCTime;
                }
                else
                {
                    batchDTO.scheduleTime = dt.ToShortTimeString();
                }

                batchDTO.operationalUnit = requests.operationalUnit.ToUpper();
                batchRequestDTO.Add(batchDTO);
                batchDTO = new DataUploadBatchRequestDTO();

            }
            */
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    user = user,
                    //batchRequests = batchRequestDTO,
                    //batchDate = batchDate,
                    //batchParameters = batchParameters,
                    bprocess = bprocess,
                    currentOU = currentOU,
                    selectedOU = selectedOU
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Explore()
        {
            Stream req = Request.InputStream;
            DBModels db = new DBModels();
            string userName = HttpContext.User.Identity.Name;
            int page = 0;
            BatchRunningProcessDTO bpr = (BatchRunningProcessDTO)Utility.mapRequestToClass(Request.InputStream, typeof(BatchRunningProcessDTO));
            EmployeeBaseDetails user = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            int parentOU = (from e in db.keyValueData where e.keyValue == user.operationalUnit && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
            List<string> ou = new List<string>();
            List<AttributesLink> childOU = new List<AttributesLink>();
            if (user.operationalUnit == "EARTH")
            {
                parentOU = (from e in db.keyValueData where e.keyValue == bpr.selectedOU && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(bpr.selectedOU);
                

                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            else
            {
                parentOU = (from e in db.keyValueData where e.keyValue == bpr.selectedOU && e.keyName == "OperationalUnit" select e.id).FirstOrDefault();
                ou.Add(bpr.selectedOU);
               

                childOU = (from e in db.attributesLink where e.parentId == parentOU select e).ToList();
            }
            string currentOU = (from e in db.employeeBaseDetails where e.email == user.email select e.operationalUnit).FirstOrDefault();
            ou.Add(user.operationalUnit);
            if (bpr.page != null)
            {
                page = bpr.page;
            }
            else
            {
                page = 0;
            }
            var pageSize = 10;

            var skip = pageSize * page;
            
            foreach (AttributesLink link in childOU)
            {
                KeyValueData s = (from e in db.keyValueData where e.id == link.childId select e).FirstOrDefault();
                ou.Add(s.keyValue);

            }
            string ouString = null;
            foreach (string s in ou)
            {
                ouString = ouString + " " + s;
            }
            int prev = 0;
            List<BatchRunningProcessDTO> bprocessDTO = new List<BatchRunningProcessDTO>();
            BatchRunningProcessDTO batchProcessDTO = new BatchRunningProcessDTO();
            List<BatchRunningProcess> brp = new List<BatchRunningProcess>();
            string batchName = (from e in db.batchRunningProcess where e.id == bpr.id select e.batchName).FirstOrDefault();
            List<BatchRunningProcess> bprocess = (from e in db.batchRunningProcess where ouString.Contains(e.operationalUnit) && e.batchName == batchName orderby e.date descending select e).ToList();
            foreach(BatchRunningProcess process in bprocess)
            {
                int maxId = (from e in db.batchRunningProcess where e.batchName == process.batchName && e.date == process.date && ouString.Contains(e.operationalUnit) select e.id).Max();
                BatchRunningProcess batchProcess = (from e in db.batchRunningProcess where e.id == maxId select e).FirstOrDefault();
                batchProcessDTO.id = batchProcess.id;
                batchProcessDTO.batchName = batchProcess.batchName;
                batchProcessDTO.startTime = batchProcess.startTime;
                batchProcessDTO.operationalUnit = batchProcess.operationalUnit;
                batchProcessDTO.endTime = batchProcess.endTime;
                long tempDate = Convert.ToInt64(batchProcess.date);
                DateTime nowDate = new DateTime(tempDate);
                batchProcessDTO.date = nowDate.ToString("yyyy-MM-dd");
                if (batchProcess.executionTime != null)
                {
                    string[] duration = batchProcess.executionTime.Split(':');
                    string executionTime = duration[0] + " Hrs " + duration[1] + " Mins";
                    batchProcessDTO.executionTime = executionTime;
                }
                else
                {
                    string executionTime ="00 Hrs 00 Mins";
                    batchProcessDTO.executionTime = executionTime;
                }
                batchProcessDTO.status = batchProcess.status;
                if (bprocessDTO.Count != 0)
                {
                    prev = bprocessDTO[bprocessDTO.Count - 1].id;
                }
                else
                {
                    prev =bprocessDTO.Count;
                }
                if(batchProcessDTO.id != prev) {
                    bprocessDTO.Add(batchProcessDTO);
                }
                
                
                
                batchProcessDTO = new BatchRunningProcessDTO();
            }

            bprocessDTO = bprocessDTO.Distinct().ToList();
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = Codes.message.Success.ToString(),
                    bprocessDTO = bprocessDTO
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

    }
}