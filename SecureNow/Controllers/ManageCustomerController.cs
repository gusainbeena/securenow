﻿using SecureNow.Container;
using PHIncentiveApp.CustomFilters;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace SecureNow.Controllers
{
    public class ManageCustomerController : Controller
    {
       public static DBModels db = new DBModels();
        // Get BU List
        // Get List For Region, State, Channel,TT,NB,FIN,MOY,QTR,MOD,L&I,
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetBUBGListData()
        {
            Stream req = Request.InputStream;
            KeyValueData customerDetails = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));

            List<KeyValueData> resulats = null;
            if (customerDetails.keyType != null)
            {
                resulats = (from d in db.keyValueData
                            where (d.keyType.Equals(customerDetails.keyType)
                            && d.keyName.Equals(customerDetails.keyName))
                            orderby d.id ascending
                            select (d)).ToList();
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = resulats,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetDataTypeList()
        {
            List<string> dataTypeList = (from i in db.keyValueData
                                         where i.keyType == "TARGETKEY"
                                         && i.keyName == "TARGETTYPE"
                                         select i.keyValue).Distinct().ToList();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    dataTypeList = dataTypeList,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        // Check Validation For Employee Basic Details
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateCustomerBasicDetails()
        {
            Stream req = Request.InputStream;
            CustomerBaseDetailsDTO customerBasicDetails = (CustomerBaseDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerBaseDetailsDTO));

            CustomerBaseDetailsDTO custDetails = null;
            // Check Employee Deatils Validate
            ExceptionResponseContainer retVal = CustomValidator.applyValidations(customerBasicDetails, typeof(CustomerDetailsValidate));

            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                custDetails = customerBasicDetails;
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    custDetails = custDetails,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        // Check Validation For Employee Basic Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateCustomerTargets()
        {
            Stream req = Request.InputStream;
            List<YearlyCustomerTargetsDTO> custTargets = (List<YearlyCustomerTargetsDTO>)Utility.mapRequestToClass(Request.InputStream, typeof(List<YearlyCustomerTargetsDTO>));

            List<YearlyCustomerTargetsDTO> custDetails = null;
            ExceptionResponseContainer retVal = null;

            // Check Employee Deatils Validate            
            retVal = CustomValidator.applyValidations(custTargets, typeof(CustomerTargetsValidate));
            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {

                custDetails = custTargets;
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    custDetails = custDetails,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult onSalesComponentChange()
        {
            Stream req = Request.InputStream;
            KeyValueData MetaDataValues = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));
            List<string> frequency = (from e in db.keyValueDataLink where e.uploadFrequency != null && e.keyValue == MetaDataValues.keyValue select e.uploadFrequency).ToList();
            KeyValueData maxFrequency = (from e in db.keyValueData select e).FirstOrDefault();
            List<KeyValueData> monthsName = (from e in db.keyValueData where e.keyType == "MOY" select e).ToList();
            List<int> frValue = new List<int>();
            List<string> month = new List<string>();
            string val = "";
            foreach (string fr in frequency)
            {
                int frTemp = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.keyValue == fr select e.preferenceValue).FirstOrDefault();
                frValue.Add(frTemp);

                int j = 0;
            }
            int i = frValue.Max();
            maxFrequency = (from e in db.keyValueData where e.keyName == "UploadFrequency" && e.preferenceValue == i select e).FirstOrDefault();
            switch (maxFrequency.keyValue)
            {
                case "Week":
                    val = "Week";
                    int mondays = 0;
                    //DateTime thisMonth = DateTime.Now.Month;
                    int currentMonth = DateTime.Now.Month;
                    int year = DateTime.Now.Year;
                    DateTime beginingOfThisMonth = new DateTime(year, currentMonth, 1);
                    foreach (KeyValueData months in monthsName)
                    {
                        int daysThisMonth = DateTime.DaysInMonth(year, Convert.ToInt32(months.keyValue));
                        beginingOfThisMonth = new DateTime(year, Convert.ToInt32(months.keyValue), 1);
                        for (int d = 0; d < daysThisMonth; d++)
                            if (beginingOfThisMonth.AddDays(d).DayOfWeek == DayOfWeek.Monday)
                                mondays++;
                        int noofWeeks = mondays;
                        for (int j = 1; j <= noofWeeks; j++)
                        {
                            month.Add(months.keyName + " " + val + "" + j);
                        }
                        mondays = 0;
                    }
                    frValue.Clear();

                    break;

                case "Fortnight":
                    val = "Fortnight";
                    foreach (KeyValueData months in monthsName)
                    {

                        for (int j = 1; j <= 2; j++)
                        {
                            month.Add(months.keyName + " " + val + "" + j);
                        }
                    }
                    frValue.Clear();
                    break;

                case "Month":
                    val = "Month";
                    foreach (KeyValueData months in monthsName)
                    {

                        for (int j = 1; j <= 2; j++)
                        {
                            month.Add(months.keyName);
                        }
                    }
                    frValue.Clear();
                    break;

                default:
                    foreach (KeyValueData months in monthsName)
                    {


                        month.Add(months.keyName);

                    }
                    frValue.Clear();
                    break;
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    month = month
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }


        public static List<KeyValueData> getKeyValueData(string channel, string requiredParameter, string otherParameter)
        {
            DBModels db = new DBModels();
            return (from i in db.keyValueData
                    where i.keyType == channel
                    && i.keyName == requiredParameter
                    && i.relatedKeyType == otherParameter
                    select i).ToList();
        }


        // Get Customer Area Employee And SubChannel List
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCustomerListData()
        {
            Stream req = Request.InputStream;
            List<CustomerRelationshipDTO> customerDetail = (List<CustomerRelationshipDTO>)Utility.mapRequestToClass(Request.InputStream, typeof(List<CustomerRelationshipDTO>));

            List<KeyValueData> subChannel = null;
            List<KeyValueData> subArea = null;
            List<KeyValueData> areaList = null;
            List<KeyValueData> regionList = null;
            string errorMessage = null;
            List<EmployeeDetailsDTO> employeeBaseDetails = null;
            if (customerDetail != null)
            {
                foreach (CustomerRelationshipDTO item in customerDetail)
                {
                    if (item.RP1 != null && item.RP2 != "")
                    {


                        string roleName = (from i in db.keyValueData
                                           where i.keyType == item.RP1
                                           && i.keyName == "ROLE"
                                           orderby i.preferenceSerial descending
                                           select i.keyValue).FirstOrDefault();


                        List<string> requiredParameter = (from i in db.keyValueData
                                                          where i.keyType == item.RP1
                                                          && i.keyName == roleName
                                                          && i.relatedKeyType == "REQUIRED"
                                                          orderby i.preferenceSerial descending
                                                          select i.keyValue).ToList();

                        foreach (string r in requiredParameter)
                        {
                            switch (r)
                            {


                                case "AREACODEREQUIRED":
                                    areaList = getKeyValueData(item.RP1, "AREACODE", item.RP2);
                                    employeeBaseDetails = (from e in db.employeeBaseDetails
                                                           join rt in db.employeeRole on e.adpId equals rt.adpId
                                                           where (rt.RP1.Equals(item.RP1)
                                                           && e.toBeConsidered == true
                                                           && e.status.Equals("ACTIVE")
                                                           && e.yearRecorded.Equals(item.yearRecorded)
                                                           && rt.RP2.Equals(item.RP2))
                                                           select new EmployeeDetailsDTO
                                                           {
                                                               id = rt.id,
                                                               adpId = e.adpId,
                                                               fullName = e.fullName
                                                           }).ToList();
                                    break;


                                case "SUBAREACODEREQUIRED":
                                    subArea = getKeyValueData(item.RP1, "SUBAREACODE", item.RP4);
                                    employeeBaseDetails = (from e in db.employeeBaseDetails
                                                           join rt in db.employeeRole on e.adpId equals rt.adpId
                                                           where (rt.RP1.Equals(item.RP1)
                                                           && e.toBeConsidered == true
                                                           && e.status.Equals("ACTIVE")
                                                           && e.yearRecorded.Equals(item.yearRecorded)
                                                           && rt.RP2.Equals(item.RP2)
                                                           && rt.RP6.Equals(item.RP4)
                                                           && rt.RP7.Equals(item.RP5))
                                                           select new EmployeeDetailsDTO
                                                           {
                                                               id = rt.id,
                                                               adpId = e.adpId,
                                                               fullName = e.fullName
                                                           }).ToList();
                                    break;


                                case "REGIONREQUIRED":
                                    regionList = getKeyValueData(item.RP1, "REGIONCODE", item.RP3);
                                    employeeBaseDetails = (from e in db.employeeBaseDetails
                                                           join er in db.employeeRole on e.adpId equals er.adpId
                                                           where (er.RP1.Equals(item.RP1)
                                                           && e.toBeConsidered == true
                                                           && e.status.Equals("ACTIVE")
                                                           && e.yearRecorded.Equals(item.yearRecorded)
                                                           && er.RP2.Equals(item.RP2))
                                                           select new EmployeeDetailsDTO
                                                           {
                                                               id = er.id,
                                                               adpId = e.adpId,
                                                               fullName = e.fullName
                                                           }).ToList();
                                    break;


                            }
                        }



                        if (employeeBaseDetails.Count == 0)
                        {
                            errorMessage = ExceptionMessage.getExceptionMessage("ErrorMessage.Unique");
                        }



                    }
                    else
                    {


                        string roleName = (from i in db.keyValueData
                                           where i.keyType == item.RP1
                                           && i.keyName == "ROLE"
                                           orderby i.preferenceSerial descending
                                           select i.keyValue).FirstOrDefault();


                        List<string> requiredParameter = (from i in db.keyValueData
                                                          where i.keyType == item.RP1
                                                          && i.keyName == roleName
                                                          && i.relatedKeyType == "REQUIRED"
                                                          orderby i.preferenceSerial descending
                                                          select i.keyValue).ToList();

                        foreach (string r in requiredParameter)
                        {
                            switch (r)
                            {

                                case "SUBCHANNELREQUIRED":
                                    subChannel = getKeyValueDataList(item.RP1, "SUBCHANNEL");
                                    break;

                                case "AREACODEREQUIRED":
                                    areaList = getKeyValueDataList(item.RP1, "AREACODE");
                                    break;


                                case "SUBAREACODEREQUIRED":
                                    subArea = getKeyValueDataList(item.RP1, "SUBAREACODE");
                                    break;


                                case "REGIONREQUIRED":
                                    regionList = getKeyValueDataList(item.RP1, "REGIONCODE");
                                    break;


                            }
                        }

                        employeeBaseDetails = (from e in db.employeeBaseDetails
                                               join r in db.employeeRole on e.adpId equals r.adpId
                                               where (r.RP1.Equals(item.RP1)
                                               && e.toBeConsidered == true
                                               && e.status.Equals("ACTIVE")
                                               && e.yearRecorded.Equals(item.yearRecorded))
                                               select new EmployeeDetailsDTO
                                               {
                                                   id = r.id,
                                                   adpId = e.adpId,
                                                   fullName = e.fullName
                                               }).ToList();

                        if (employeeBaseDetails.Count == 0)
                        {
                            errorMessage = ExceptionMessage.getExceptionMessage("ErrorMessage.Unique");
                        }



                    }
                }

            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    employeeBaseDetails = employeeBaseDetails,
                    areaList = areaList,
                    subChannel = subChannel,
                    regionList = regionList,
                    subArea = subArea,
                    errorMessage = errorMessage,

                    result = "success",
                    resulats = new
                    {
                        employeeBaseDetails,
                        areaList,
                        subChannel,
                        // branchList,
                        regionList,
                        subArea,
                        errorMessage
                    }
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        // Customer Target Insert Bu Wise
        // Create Customer Details
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CustomerRelationSubmit()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO customerDetails = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));
            EmployeeRequestDTO retData = new EmployeeRequestDTO();
            retData.customerRelationship = new List<CustomerBasicRelationDTO>();
            ExceptionResponseContainer retVal = null;
            List<CustomerBasicRelationDTO> customerRel = null;

            foreach (CustomerBasicRelationDTO cr in customerDetails.customerRelationship)
            {
                cr.customerBaseDetailsFK = customerDetails.customerDeatils.id;
                cr.yearRecorded = customerDetails.customerDeatils.yearRecorded;
                retVal = CustomValidator.applyValidations(cr, typeof(CustomerRelationShipValidate));
            }

            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (customerDetails.customerRelationship.Count != 0)
                {
                    foreach (CustomerBasicRelationDTO item in customerDetails.customerRelationship)
                    {
                        List<CustomerBasicRelationDTO> resultsDb1 = (from e in db.customerRelationshipDetails
                                                                     where e.customerBaseDetailsFK.Equals(item.customerBaseDetailsFK)
                                                                     && e.endMonth >= item.endMonth
                                                                     && e.yearRecorded.Equals(item.yearRecorded)
                                                                     select new CustomerBasicRelationDTO
                                                                     {
                                                                         employeeRoleFK = e.employeeRoleFK,

                                                                     }).ToList();

                        CustomerRelationshipDetails customerGeo = new CustomerRelationshipDetails();
                        customerGeo.customerBaseDetailsFK = customerDetails.customerDeatils.id;
                        customerGeo.endMonth = item.endMonth;
                        //customerGeo.endMonthsDescription = item.endMonth.ToString();
                        //customerGeo.subArea = item.subArea;
                        customerGeo.startMonth = item.startMonth;
                        customerGeo.employeeRoleFK = item.employeeRoleFK;
                        //customerGeo.startMonthDescription = item.startMonth.ToString();
                        customerGeo.yearRecorded = customerDetails.customerDeatils.yearRecorded;
                        db.customerRelationshipDetails.Add(customerGeo);
                        db.SaveChanges();
                    }
                }

                customerRel = (from d in db.customerRelationshipDetails
                               join e in db.employeeRole on d.employeeRoleFK equals e.id
                               where d.customerBaseDetailsFK.Equals(customerDetails.customerDeatils.id)
                               select new CustomerBasicRelationDTO
                               {
                                   id = d.id,
                                   RP1 = e.RP1,
                                   RP2 = e.RP2,
                                   RP3 = e.RP3,
                                   RP4 = e.RP4,
                                   RP5 = e.RP5,
                                   RP6 = e.RP6,
                                   RP7 = e.RP7,
                                   startMonth = d.startMonth,
                                   endMonth = d.endMonth,
                                   employeeRoleFK = 0,
                                   employeeRole = "",
                                   branch = "",
                                   yearRecorded = d.yearRecorded,
                               }).ToList();

            }

            List<KeyValueData> keyValueData = (from d in db.keyValueData
                                               select d).ToList();

            foreach (CustomerBasicRelationDTO item3 in customerRel)
            {
                foreach (KeyValueData keyV in keyValueData)
                {
                    if (keyV.keyValue == item3.startMonthDescription && keyV.keyType == "MOY")
                    {
                        item3.startMonthDescription = keyV.description;
                    }
                    if (keyV.keyValue == item3.endMonthsDescription && keyV.keyType == "MOY")
                    {
                        item3.endMonthsDescription = keyV.description;
                    }
                }
                retData.customerRelationship.Add(item3);
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    retData = retData,
                    result = "success"
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteCustomerRelationship()
        {

            Stream req = Request.InputStream;
            CustomerRelationshipDetails custRel = (CustomerRelationshipDetails)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerRelationshipDetails));

            if (custRel.id != 0)
            {
                CustomerRelationshipDetails resultsDb = (from c in db.customerRelationshipDetails
                                                         where (c.id.Equals(custRel.id))
                                                         select c
                                                         ).FirstOrDefault();

                if (resultsDb != null)
                {
                    db.customerRelationshipDetails.Remove(resultsDb);
                    db.SaveChanges();
                }
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "sucess"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public static List<KeyValueData> getKeyValueDataList(string key, string required)
        {
           
            return (from s in db.keyValueData
                    where (s.keyType.Equals(key)
                    && s.keyName.Equals(required))
                    orderby s.id ascending
                    select (s)).ToList();
        }

        // Create New Customer 
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateCustomerDetails()
        {
            Stream req = Request.InputStream;
            CustomerDetailsDTO customerDetails = (CustomerDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerDetailsDTO));

            DateTime dateFormate = DateTime.ParseExact(customerDetails.customerDeatils.doaStr, "dd-mm-yyyy", null);
            long dateOfJoining = dateFormate.ToUniversalTime().Ticks;
            ExceptionResponseContainer retVal = null;

            retVal = CustomValidator.applyValidations(customerDetails.customerDeatils, typeof(CustomerDetailsValidate));
            if (retVal.isValid == true)
            {
                foreach (CustomerBasicRelationDTO cr in customerDetails.customerRelationship)
                {
                    retVal = CustomValidator.applyValidations(cr, typeof(CustomerRelationShipValidate));
                }
            }

            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {

                CustomerBaseDetails customerBaseDetails = new CustomerBaseDetails();

                if (customerDetails.customerDeatils.customerName != null)
                {
                    customerBaseDetails.customerName = customerDetails.customerDeatils.customerName;
                    customerBaseDetails.soldToCode = customerDetails.customerDeatils.soldToCode;
                    customerBaseDetails.groupName = customerDetails.customerDeatils.groupName;
                    customerBaseDetails.districtOrTown = customerDetails.customerDeatils.districtOrTown;
                    customerBaseDetails.stateCode = customerDetails.customerDeatils.stateCode;
                    customerBaseDetails.doa = dateOfJoining;
                    customerBaseDetails.doaStr = customerDetails.customerDeatils.doaStr;
                    customerBaseDetails.areaOfOperation = customerDetails.customerDeatils.areaOfOperation;
                    customerBaseDetails.status = customerDetails.customerDeatils.status;
                    customerBaseDetails.yearRecorded = customerDetails.customerDeatils.yearRecorded;
                    db.customerBaseDetails.Add(customerBaseDetails);
                    db.SaveChanges();
                }

                if (customerDetails.customerTargets != null && customerDetails.customerTargets.Count != 0)
                {
                    if (customerDetails.customerRelationship.Count != 0)
                    {
                        foreach (CustomerBasicRelationDTO item in customerDetails.customerRelationship)
                        {
                            CustomerRelationshipDetails customerGeo = new CustomerRelationshipDetails();
                            customerGeo.customerBaseDetailsFK = customerBaseDetails.id;
                            customerGeo.endMonth = item.endMonth;
                            //customerGeo.endMonthsDescription = item.endMonth.ToString();
                            customerGeo.startMonth = item.startMonth;
                            //customerGeo.subArea = item.subArea;
                            //customerGeo.startMonthDescription = item.startMonth.ToString();
                            customerGeo.yearRecorded = customerDetails.customerDeatils.yearRecorded;
                            customerGeo.employeeRoleFK = item.employeeRoleFK;
                            db.customerRelationshipDetails.Add(customerGeo);
                            db.SaveChanges();

                            foreach (YearlyCustomerTargetsDTO itemm in customerDetails.customerTargets)
                            {
                                foreach (YearlyTargetsDTO item1 in itemm.customerTargets)
                                {
                                    YearlyCustomerTargets yearlyTarget = new YearlyCustomerTargets();
                                    yearlyTarget.customerBaseDetailsFK = customerBaseDetails.id;
                                    yearlyTarget.buCode = itemm.buCode;
                                    yearlyTarget.month = item1.month;
                                    yearlyTarget.targetAmt = item1.targetAmt;
                                    yearlyTarget.dataType = itemm.dataType;
                                    yearlyTarget.yearRecorded = customerDetails.customerDeatils.yearRecorded;
                                    db.yearlyCustomerTargets.Add(yearlyTarget);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    customerDetails = customerDetails
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Get Customer Details List
        // get employee list
        // [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCustomerDetailsList()
        {
            Stream req = Request.InputStream;
            CustomerBaseDetails customerDetails = (CustomerBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerBaseDetails));

            int page = 0;
            string message = null;

            List<CustomerBaseDetails> resulats = null;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;

            if (customerDetails.customerName != null)
            {
                resulats = (from d in db.customerBaseDetails
                            where (d.customerName.ToLower().Contains(customerDetails.customerName)
                            )
                            orderby d.id descending
                            select d).Skip(skip).Take(pageSize).ToList();
                if (resulats.Count == 0)
                {
                    resulats = (from d in db.customerBaseDetails
                                where (d.soldToCode.ToLower().Contains(customerDetails.customerName)
                                )
                                orderby d.id descending
                                select d).Skip(skip).Take(pageSize).ToList();
                }
            }
            else
            {
                resulats = (from d in db.customerBaseDetails
                            orderby d.id descending
                            select d).Skip(skip).Take(pageSize).ToList();
            }

            List<KeyValueData> keyValueData = (from d in db.keyValueData
                                               select d).ToList();

            foreach (CustomerBaseDetails item1 in resulats)
            {
                foreach (KeyValueData item in keyValueData)
                {
                    if (item.keyValue == item1.stateCode && item.keyType == "STATE")
                    {
                        item1.stateCode = item.description;
                    }

                }
            }

            if (resulats.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = resulats,
                    sucess = message,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Next And Privious For Employee Basic Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NextAndPriviousCustomerDetails()
        {
            Stream req = Request.InputStream;
            NextAndPriviousDTO nextAndPrivious = (NextAndPriviousDTO)Utility.mapRequestToClass(Request.InputStream, typeof(NextAndPriviousDTO));

            int page = 0;
            if (nextAndPrivious.pageNumber != null)
            {
                page = Convert.ToInt32(nextAndPrivious.pageNumber);
            }
            string message = null;
            int currentMonth = DateTime.Now.Month;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;
            List<CustomerBaseDetails> results = null;

            results = (from d in db.customerBaseDetails
                       orderby d.id descending
                       select d).Skip(skip).Take(pageSize).ToList();

            List<KeyValueData> keyValueData = (from d in db.keyValueData
                                               select d).ToList();

            foreach (CustomerBaseDetails item1 in results)
            {
                foreach (KeyValueData item in keyValueData)
                {
                    if (item.keyValue == item1.stateCode && item.keyType == "STATE")
                    {
                        item1.stateCode = item.description;
                    }

                }
            }

            if (results.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = results,
                    sucess = message,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Edit Customer Details
        // Edit Branch List
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditCustomerDetails()
        {

            Stream req = Request.InputStream;
            CustomerBaseDetails customerdetails = (CustomerBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerBaseDetails));

            EmployeeRequestDTO retVal = new EmployeeRequestDTO();
            retVal.customerDeatils = new CustomerBaseDetailsDTO();
            retVal.customerRelationship = new List<CustomerBasicRelationDTO>();
            retVal.customerTargets = new List<YearlyCustomerTargetsDTO>();
            retVal.secondaryCustomerTargets = new List<YearlyCustomerTargetsDTO>();
            List<CustomerBasicRelationDTO> customerRel = null;

            if (customerdetails.id != 0)
            {
                retVal.customerDeatils = (from cu in db.customerBaseDetails
                                          where cu.id.Equals(customerdetails.id)
                                          select new CustomerBaseDetailsDTO
                                          {
                                              id = cu.id,
                                              customerName = cu.customerName,
                                              soldToCode = cu.soldToCode,
                                              groupName = cu.groupName,
                                              stateCode = cu.stateCode,
                                              districtOrTown = cu.districtOrTown,
                                              doa = cu.doa,
                                              doaStr = cu.doaStr,
                                              docStr = cu.docStr,
                                              areaOfOperation = cu.areaOfOperation,
                                              status = cu.status,
                                              yearRecorded = cu.yearRecorded
                                          }).FirstOrDefault();
                //var date = DateTime.ParseExact(retVal.customerDeatils.doaStr, "ddMMMyy", CultureInfo.InvariantCulture);
                //retVal.customerDeatils.doaStr = date.ToString();
                if (retVal.customerDeatils != null)
                {
                    customerRel = (from d in db.customerRelationshipDetails
                                   join e in db.employeeRole on d.employeeRoleFK equals e.id
                                   where d.customerBaseDetailsFK.Equals(retVal.customerDeatils.id)
                                   select new CustomerBasicRelationDTO
                                   {
                                       id = d.id,
                                       RP1 = e.RP1,
                                       RP2 = e.RP2,
                                       RP3 = e.RP3,
                                       //subArea = d.subArea,
                                       startMonth = d.startMonth,
                                       endMonth = d.endMonth,
                                       employeeRoleFK = d.employeeRoleFK,//0,
                                       RP4 = "",
                                       employeeRole = "",
                                       branch = "",
                                       yearRecorded = d.yearRecorded,
                                       //startMonthDescription = d.startMonthDescription,
                                       //endMonthsDescription = d.endMonthsDescription,
                                   }).ToList();
                    /*
                    customerRel = (from d in db.customerRelationshipDetails
                                   join e in db.employeeRole on d.employeeRoleFK equals e.id
                                   where d.customerBaseDetailsFK.Equals(retVal.customerDeatils.id)
                                   select new CustomerBasicRelationDTO
                                   {
                                       id = d.id,
                                       channelCode = e.channelCode,
                                       subChannelCode = e.subChannelCode,
                                       region = e.region,
                                       subArea = d.subArea,
                                       startMonth = d.startMonth,
                                       endMonth = d.endMonth,
                                       employeeRoleFK = d.employeeRoleFK,//0,
                                       areaCode = "",
                                       employeeRole = "",
                                       branch = "",
                                       yearRecorded = d.yearRecorded,
                                       startMonthDescription = d.startMonthDescription,
                                       endMonthsDescription = d.endMonthsDescription,
                                   }).ToList();
                    */
                    if (customerRel.Count != 0)
                    {
                        foreach (CustomerBasicRelationDTO geo in customerRel)
                        {

                            /*CustomerAreaLink areaLink = (from a in db.customerAreaLink
                                                         where a.customerRelationshipDetailsFK.Equals(geo.id)
                                                         select a).FirstOrDefault();*/
                            CustomerRelationshipDetails empLink = (from a in db.customerRelationshipDetails
                                                                   where a.id.Equals(geo.id)
                                                                   select a).FirstOrDefault();


                            if (empLink != null)
                            {
                                EmployeeRole empRole = (from r in db.employeeRole
                                                        where r.id.Equals(empLink.employeeRoleFK)
                                                        select r).FirstOrDefault();
                                if (empRole != null)
                                {
                                    EmployeeBaseDetails empDetails = (from emp in db.employeeBaseDetails
                                                                      where emp.adpId.Equals(empRole.adpId)
                                                                      select emp).FirstOrDefault();
                                    if (empDetails != null)
                                    {
                                        geo.employeeRole = empDetails.fullName + '-' + empDetails.adpId;
                                    }

                                    geo.employeeRoleFK = empLink.employeeRoleFK;
                                }
                            }


                        }

                    }

                    List<KeyValueData> keyValueData = (from d in db.keyValueData
                                                       select d).ToList();

                    foreach (CustomerBasicRelationDTO item3 in customerRel)
                    {
                        foreach (KeyValueData keyV in keyValueData)
                        {
                            if (keyV.keyValue == item3.RP1 && keyV.keyType != "CUSTOMERACTUALSMAPPING")
                            {
                                item3.RP1 = keyV.keyValue;
                            }
                            if (keyV.keyValue == item3.RP2)
                            {
                                item3.RP2 = keyV.keyValue;
                            }
                            if (keyV.keyValue == item3.RP3)
                            {
                                //item3.region = keyV.description;
                                item3.RP3 = keyV.keyValue;
                            }
                            if (keyV.keyValue == item3.startMonth.ToString() && keyV.keyType == "MOY")
                            {
                                item3.startMonthDescription = keyV.description;
                            }
                            if (keyV.keyValue == item3.endMonth.ToString() && keyV.keyType == "MOY")
                            {
                                item3.endMonthsDescription = keyV.description;
                            }


                        }
                        retVal.customerRelationship.Add(item3);
                    }

                }
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    retVal = retVal,
                    result = "success"
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Update Customer Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateCustomerDetails()
        {
            Stream req = Request.InputStream;
            CustomerBaseDetailsDTO customerDetails = (CustomerBaseDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(CustomerBaseDetailsDTO));

            DateTime dateFormate = DateTime.ParseExact(customerDetails.doaStr, "dd-mm-yyyy", null);

            long dateOfJoining = dateFormate.ToUniversalTime().Ticks;

            long docStr = 0;
            if (customerDetails.docStr != null)
            {
                DateTime dateOfClosing = DateTime.ParseExact(customerDetails.docStr, "dd-MM-yyyy", null);
                docStr = dateOfClosing.ToUniversalTime().Ticks;
            }
            ExceptionResponseContainer retVal = null;

            retVal = CustomValidator.applyValidations(customerDetails, typeof(CustomerDetailsValidate));


            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {


                if (customerDetails.customerName != null)
                {
                    CustomerBaseDetails custBaseDetails = (from cbd in db.customerBaseDetails
                                                           where cbd.id.Equals(customerDetails.id)
                                                           select cbd).FirstOrDefault();
                    if (custBaseDetails != null)
                    {
                        custBaseDetails.doa = dateOfJoining;
                        custBaseDetails.doaStr = customerDetails.doaStr;
                        custBaseDetails.areaOfOperation = customerDetails.areaOfOperation;
                        custBaseDetails.customerName = customerDetails.customerName;
                        custBaseDetails.soldToCode = customerDetails.soldToCode;
                        custBaseDetails.stateCode = customerDetails.stateCode;
                        custBaseDetails.status = customerDetails.status;
                        if (customerDetails.status == "CLOSED")
                        {
                            custBaseDetails.doc = docStr;
                            custBaseDetails.docStr = customerDetails.docStr;
                        }
                        custBaseDetails.districtOrTown = customerDetails.districtOrTown;
                        custBaseDetails.groupName = customerDetails.groupName;
                        custBaseDetails.yearRecorded = customerDetails.yearRecorded;
                        db.SaveChanges();
                    }

                    if (customerDetails.status == "CLOSED")
                    {
                        CloseCustomerDetails(custBaseDetails);
                    }

                }

            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    customerDetails = customerDetails,
                    sucess = "Sucess",
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        // Close Customer Details
        public void CloseCustomerDetails(CustomerBaseDetails inputData)
        {

            int docMonth = Convert.ToInt32(inputData.docStr.Split('-')[1]);
            if (docMonth == 12)
            {
                return;
            }
            CustomerRelationshipDetails custRetShip = (from crs in db.customerRelationshipDetails
                                                       where crs.customerBaseDetailsFK == inputData.id
                                                       select crs).FirstOrDefault();
            if (custRetShip != null)
            {
                custRetShip.endMonth = docMonth;
                db.SaveChanges();
            }
        }
    }

}