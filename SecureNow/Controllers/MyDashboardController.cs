﻿using Newtonsoft.Json;
using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecureNow.Controllers
{
    public class MyDashboardController : Controller
    {
        DBModels db = new DBModels();
        

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SetSlaveDB()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            string country = jsonDeserialize.rqBody.country; 
            object jsonsuccess = null;
            if(country != null && country != "")
            {

                KeyValueData getDb = (from k in db.keyValueData where k.keyType.Equals("COUNTRY") && k.keyName.Equals(country) select k).FirstOrDefault();
                if (getDb != null)
                {
                    //DBModels.dynamicDB = "LightingDB_" + getDb.keyValue.ToUpper();
                    //DBModels.dynamicDB = new DBModels("LightingDB_" + getDb.keyValue.ToUpper());
                    //dynamicDb = DBModels.dynamicDB;
                }
                jsonsuccess = new
                {
                    rsBody = new
                    {
                        result = "success",
                    }
                };
            }

            

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FilterOptionsList()
        {
            object jsonsuccess = null;

            List<KeyValueData> filterOptions = new List<KeyValueData>();
            string userName = HttpContext.User.Identity.Name;
            int currentMonth = Convert.ToInt32((from e in db.keyValueData where e.keyType == "CCM" && e.keyName == "Calender" select e.keyValue).FirstOrDefault());

            string currentQuarter = "";
            string period = "";
            currentQuarter = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" select e.keyValue).FirstOrDefault();
            period = currentQuarter;

            MonthDTO getMonth = CommonController.getMonthList(currentQuarter);
            string currentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            string previousYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "PSY" select e.keyValue).FirstOrDefault();
            int currentYearData = Convert.ToInt32(currentYear);

            filterOptions.AddRange((from k in db.keyValueData where k.keyType.Equals("MetaData") && k.keyName.Equals("SalesHorizontal") orderby k.preferenceValue ascending select k).ToList());

            jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    filterOptions = filterOptions,
                    quarter = currentQuarter,
                    period = period,
                    currentYear = currentYear,
                    previousYear = previousYear,

                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getPeriodFilter()
        {
            List<string> quarterList = CommonController.quarterListData(new List<string>(), 1, 12);
            List<KeyValueData> monthsList = (from k in db.keyValueData where k.keyType.Equals("MOY") orderby k.preferenceSerial select k).ToList();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    quarterList = quarterList,
                    monthsList = monthsList
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetSalesHorizontalFilterHierarchy()
        {
            Stream req = Request.InputStream;
            DashboardFilters keyvalue = (DashboardFilters)Utility.mapRequestToClass(Request.InputStream, typeof(DashboardFilters));
            object jsonsuccess = null;
            List<RolePerformanceListDTO> finalList = new List<RolePerformanceListDTO>();
            if (keyvalue != null)
            {
                KeyValueData currentSBSeq = (from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(keyvalue.data.currentTab) && key.preferenceValue == keyvalue.data.metaDataLevel select key).FirstOrDefault();
                List<KeyValueData> upperSB = (from k in db.keyValueData
                                              where k.keyType.Equals("MetaData") && k.keyName.Equals(keyvalue.data.currentTab) &&
                                              k.preferenceValue < currentSBSeq.preferenceValue
                                              select k).ToList();

                var query = from r in db.attributesLink select r;

                upperSB.Add(currentSBSeq);

                if (upperSB != null)
                {
                    Boolean flag = false;
                    List<int> rootId = new List<int>();
                    List<int> parentId = new List<int>();
                    List<int> childId = new List<int>();
                    foreach (KeyValueData salesBoundry in upperSB)
                    {
                        switch (salesBoundry.preferenceValue)
                        {
                           
                            case 0:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    finalList.AddRange((from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.keyName.Equals(keyvalue.data.currentTab) select new RolePerformanceListDTO {
                                        row = k,
                                        selected = true
                                    }).ToList());
                                    flag = true;
                                }
                                else
                                {
                                    parentId = (from k in db.keyValueData
                                                where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_1.Contains(k.keyValue) && k.keyName.Equals(keyvalue.data.currentTab)
                                                select k.id).ToList();
                                    rootId = parentId;
                                    query = query.Where(i => rootId.Contains(i.rootId));
                                }
                                break;
                            case 1:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                            
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_2.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 2:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_3.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 3:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_4.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 4:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_5.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 5:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_6.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 6:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_7.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 7:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_8.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 8:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_9.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 9:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_10.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;
                            case 10:
                                if (currentSBSeq.preferenceValue == salesBoundry.preferenceValue)
                                {
                                    query = query.Where(i => i.childlabel == currentSBSeq.keyValue);
                                    List<AttributesLink> data = query.ToList();
                                    foreach (AttributesLink q in data)
                                    {
                                        RolePerformanceListDTO mapping = (from k in db.keyValueData where k.keyType.Equals(currentSBSeq.keyValue) && k.id == q.childId select new RolePerformanceListDTO {
                                            row = k,
                                            selected = true,
                                        }).FirstOrDefault();
                                        finalList.Add(mapping);
                                    }
                                }
                                else
                                {
                                    childId = (from k in db.keyValueData
                                               where k.keyType.Equals(salesBoundry.keyValue) && keyvalue.level_11.Contains(k.keyValue) &&
                                               k.keyName.Equals(keyvalue.data.currentTab)
                                               select k.id).ToList();
                                    query = query.Where(i => rootId.Contains(i.rootId) && childId.Contains(i.parentId));
                                    List<AttributesLink> temp = query.ToList();
                                    List<AttributesLink> newattr = new List<AttributesLink>();
                                    foreach (AttributesLink a in temp)
                                    {
                                        foreach (int chId in childId)
                                        {
                                            if (a.attributeSequence.Split(':').ToList().Contains(chId + ""))
                                            {
                                                newattr.Add(a);
                                            }
                                        }
                                    }

                                    query = newattr.AsQueryable();
                                }
                                break;

                            default:
                                break;
                        }
                        if (flag)
                        {
                            break;
                        }
                    }
                }
            }

            jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    list = finalList,
                    level = keyvalue.data.metaDataLevel
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
       
        //get filter wise data for showing graph        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getFilterSB1List()
        {
            Stream req = Request.InputStream;
            TabularDataDTO customerDetails = (TabularDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(TabularDataDTO));
            List<RolePerformanceListDTO> getChannelList = new List<RolePerformanceListDTO>();
            List<string> getSubChannelList = new List<string>();
            List<string> getRegionList = new List<string>();
            List<string> getBranchList = new List<string>();
            string userName = HttpContext.User.Identity.Name;
            //dynamicDb = new DBModels("LightingDB_IN");
            int currentMonth = Convert.ToInt32((from e in db.keyValueData where e.keyType == "CCM" && e.keyName == "Calender" select e.keyValue).FirstOrDefault());

            List<string> getBgList = (from e in db.keyValueData where e.keyType == "OPERATIONAL" && e.keyName == "BG" orderby e.preferenceValue select e.keyValue).Distinct().ToList();


            string currentQuarter = "";
            string period = "";
            if (customerDetails.period != null && customerDetails.period != "")
            {
                currentQuarter = customerDetails.period;
                period = customerDetails.period;
            }
            else
            {
                currentQuarter = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" select e.keyValue).FirstOrDefault();
                period = currentQuarter;
            }

            MonthDTO getMonth = CommonController.getMonthList(currentQuarter);
            string currentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            string previousYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "PSY" select e.keyValue).FirstOrDefault();
            int currentYearData = Convert.ToInt32(currentYear);
            string hideCalculator = "";
            List<EmployeeDetailsDTO> results = (from d in db.employeeBaseDetails
                                                join r in db.employeeRole on d.adpId equals r.adpId
                                                where d.email.Equals(userName)
                                                 && d.status.Equals("ACTIVE") &&
                                                ((r.startMonth <= getMonth.startMonth && r.endMonth >= getMonth.startMonth)
                                                || (r.startMonth >= getMonth.startMonth && r.endMonth <= getMonth.endMonth)
                                                || (r.startMonth >= getMonth.startMonth && r.startMonth <= getMonth.endMonth))
                                                 && r.yearRecorded == currentYearData
                                                select new EmployeeDetailsDTO
                                                {
                                                    role = r.role,
                                                    adpId = r.adpId,
                                                    RP1 = r.RP1,
                                                    RP2 = r.RP2,
                                                    RP3 = r.RP3,
                                                    RP4 = r.RP4,
                                                    RP5 = r.RP5,
                                                    RP6 = r.RP6,
                                                    RP7 = r.RP7
                                                }).ToList();



            foreach (EmployeeDetailsDTO item in results)
            {
                //hideCalculator = (from e in dynamicDb.keyValueData where e.keyType == item.role && e.keyName == "INCENTIVECALCULATOR" select e.keyValue).FirstOrDefault();
                if (item.RP1.Equals("AllChannels"))
                {
                    getChannelList = (
                                        from k in db.keyValueData
                                        where
                                        k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals("SalesHorizontal") /*&& key.keyValueModel.Equals("SB1")*/ select key.keyValue).FirstOrDefault())
                                        && k.keyName.Equals("SalesHorizontal")
                                        select new RolePerformanceListDTO
                                        {
                                            row = k,
                                            selected = true
                                        }).ToList();
                }
                else
                {
                    getChannelList.Add((
                                        from k in db.keyValueData
                                        where
                                        k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals("SalesHorizontal") /*&& key.keyValueModel.Equals("SB1")*/ select key.keyValue).FirstOrDefault())
                                        && k.keyName.Equals("SalesHorizontal")
                                        select new RolePerformanceListDTO
                                        {
                                            row = k,
                                            selected = true
                                        }).FirstOrDefault());
                }


            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    channelList = getChannelList,
                    quarter = currentQuarter,
                    period = period,
                    currentYear = currentYear,
                    previousYear = previousYear,
                    hideIncentive = hideCalculator,
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        //Get subchannel list
        //get filter wise data for showing graph        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getFilterSB2List()
        {
            Stream req = Request.InputStream;
            TabularDataDTO customerDetails = (TabularDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(TabularDataDTO));
            List<string> getSubChannelList = new List<string>();
           // dynamicDb = new DBModels("LightingDB_IN");
            string userName = HttpContext.User.Identity.Name;
            string currentQuarter = "";
            string period = "";
            AttributeLinkingDTO sb2Linking = new AttributeLinkingDTO();
            if (customerDetails.period != null && customerDetails.period != "")
            {
                period = customerDetails.period;
            }
            else
            {
                currentQuarter = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" select e.keyValue).FirstOrDefault();
                period = currentQuarter;
            }

            MonthDTO getMonth = CommonController.getMonthList(period);
            string currentYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            string previousYear = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "PSY" select e.keyValue).FirstOrDefault();
            int currentYearData = Convert.ToInt32(currentYear);

            List<EmployeeDetailsDTO> results = (from d in db.employeeBaseDetails
                                                join r in db.employeeRole on d.adpId equals r.adpId
                                                where d.email.Equals(userName)
                                                 && d.status.Equals("ACTIVE") &&
                                                ((r.startMonth <= getMonth.startMonth && r.endMonth >= getMonth.startMonth)
                                                || (r.startMonth >= getMonth.startMonth && r.endMonth <= getMonth.endMonth)
                                                || (r.startMonth >= getMonth.startMonth && r.startMonth <= getMonth.endMonth))
                                                 && r.yearRecorded == currentYearData
                                                select new EmployeeDetailsDTO
                                                {
                                                    role = r.role,
                                                    adpId = r.adpId,
                                                    RP1 = r.RP1,
                                                    RP2 = r.RP2,
                                                    RP3 = r.RP3,
                                                    RP4 = r.RP4,
                                                    RP5 = r.RP5,
                                                    RP6 = r.RP6,
                                                    RP7 = r.RP7
                                                }).ToList();



            foreach (EmployeeDetailsDTO item in results)
            {
                if (item.RP1.Equals("AllChannels"))
                {
                    List<int> sb1IDS = (from k in db.keyValueData
                                        where k.keyType.Equals((from key in db.keyValueData
                                                                where key.keyType.Equals("MetaData") && key.keyName.Equals("SalesHorizontal")
                                                                /*&& key.keyValueModel.Equals("SB1")*/
                                                                select key.keyValue).FirstOrDefault())
                                        && k.keyName.Equals("SalesHorizontal") && customerDetails.SB1.Contains(k.keyValue)
                                        select k.id).ToList();

                    KeyValueData sb2 = (from key in db.keyValueData
                                        where key.keyType.Equals("MetaData") && key.keyName.Equals("SalesHorizontal")
                                        /*&& key.keyValueModel.Equals(customerDetails.key)*/
                                        select key).FirstOrDefault();
                    if (sb2 != null)
                    {
                        sb2Linking = GetAttributeLinking(sb2, sb1IDS, customerDetails.attrSequence);
                    }
                }
                else
                {

                }


            }


            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    sb2Linking = sb2Linking
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public AttributeLinkingDTO GetAttributeLinking(KeyValueData sbLevel, List<int> sb1IDS, List<string> attrSequence)
        {
            AttributeLinkingDTO getLinking = new AttributeLinkingDTO();          
            if (attrSequence != null)
            {
                foreach (string s in attrSequence)
                {
                    getLinking.attrDetail.AddRange(
                                (from k in db.keyValueData
                                 join attr in db.attributesLink on k.id equals attr.childId
                                 where sb1IDS.Contains(attr.rootId) && attr.childlabel.Equals(sbLevel.keyValue) && attr.attributeSequence.Contains(s)
                                 select new RolePerformanceListDTO
                                 {
                                     row = k,
                                     selected = true,
                                     attrSequence = attr.attributeSequence
                                 }).ToList());
                }
            }
            else
            {
                getLinking.attrDetail.AddRange(
                                (from k in db.keyValueData
                                 join attr in db.attributesLink on k.id equals attr.childId
                                 where sb1IDS.Contains(attr.rootId) && attr.childlabel.Equals(sbLevel.keyValue)
                                 select new RolePerformanceListDTO
                                 {
                                     row = k,
                                     selected = true,
                                     attrSequence = attr.attributeSequence
                                 }).ToList());
            }

            foreach (RolePerformanceListDTO item in getLinking.attrDetail)
            {
                getLinking.attrSequence.Add(item.attrSequence);
            }
            return getLinking;
        }


        public JsonResult AddCountry()
        {
            Stream req = Request.InputStream;
            GlobeAttributes keyvalue = (GlobeAttributes)Utility.mapRequestToClass(Request.InputStream, typeof(GlobeAttributes));
            //A reader to store your values
            SqlDataReader sqlDataReader;
            //Your Connection
            string dbName = "LightingDB_" + keyvalue.id;
            using (SqlConnection sqlConnection = new SqlConnection("data source =.; initial catalog = LightingDB; integrated security = true"))
            {
                //Create a command to execute your Stored Procedure
                SqlCommand sqlCommand = new SqlCommand("CreateDataBase", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;


                //Add any necessary parameters to your Procedure
                sqlCommand.Parameters.AddWithValue("@pDBName", dbName);

                //Open your connection
                sqlConnection.Open();
                //Execute your Stored Procedure
                sqlDataReader = sqlCommand.ExecuteReader();
                //Close the connection
                sqlConnection.Close();

                //Add user name password on DB
                /*sqlCommand = new SqlCommand("CreateLoginAndUser", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                //Add any necessary parameters to your Procedure
                sqlCommand.Parameters.AddWithValue("@db", dbName);
                sqlCommand.Parameters.AddWithValue("@login", "Beena");
                sqlCommand.Parameters.AddWithValue("@password", "Beena123");

                //Open your connection
                sqlConnection.Open();
                //Execute your Stored Procedure
                sqlDataReader = sqlCommand.ExecuteReader();
                //Close the connection
                sqlConnection.Close();*/


                sqlCommand = new SqlCommand("create_Table", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;


                //Add any necessary parameters to your Procedure
                sqlCommand.Parameters.AddWithValue("@pDBName", dbName);

                //Open your connection
                sqlConnection.Open();
                //Execute your Stored Procedure
                sqlDataReader = sqlCommand.ExecuteReader();
                //Close the connection
                sqlConnection.Close();
            }


            //Add in keyValue
            KeyValueData countryKey = new KeyValueData();
            countryKey.keyType = "COUNTRY";
            countryKey.keyName = keyvalue.title;
            countryKey.description = keyvalue.title;
            countryKey.keyValue = keyvalue.id;
            countryKey.relatedKeyType = "CURRENCY";
            countryKey.relatedKeyValue = keyvalue.currency;
            countryKey.preferenceSerial = 0;
            countryKey.preferenceValue = 0;
            db.keyValueData.Add(countryKey);
            db.SaveChanges();


            object jsonsuccess = null;

            jsonsuccess = new
            {
                rsBody = new
                {
                        result = "success",
                        message = "success"
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getGlobeData()
        {
            Stream req = Request.InputStream;
            EmployeeDetailsDTO employeeDetail = (EmployeeDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeDetailsDTO));

            List<string> activeCntr = new List<string>();
            Dictionary<string, EmployeeActualsAndTargetsDTO> performance = new Dictionary<string, EmployeeActualsAndTargetsDTO>();
            
            if (employeeDetail.operationalUnits.Equals("ALL"))
            {
                activeCntr = (from k in db.keyValueData where k.keyType.Equals("COUNTRY") select k.keyName.ToUpper()).ToList();
            }
            else
            {
                activeCntr = employeeDetail.operationalUnits.Split(',').ToList();
            }

            List<string> countryList = (from e in db.keyValueData where e.keyType == "COUNTRY" select e.keyName).ToList();
            performance = (from e in db.EmployeeActualAndTargets
                           where activeCntr.Contains(e.operationalUnit)
                           group e by new
                           {
                               e.operationalUnit
                           } into g
                           select new EmployeeActualsAndTargetsDTO
                           {
                               drivenFrom = g.Key.operationalUnit,
                               targetAmt = g.Sum(x => x.targetAmt),
                               netActualAmt = g.Sum(x => x.netActualAmt),
                               grossActualAmt = g.Sum(x => x.grossActualAmt),
                               performance = (g.Sum(x => x.grossActualAmt)/ g.Sum(x => x.targetAmt))*100
                           }).ToDictionary(p => p.drivenFrom);
            
            object jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    countryList = activeCntr,
                    performance = performance,
                    List = countryList
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getGraphTypes()
        {
            Stream req = Request.InputStream;
            TabularDataDTO frontEndData = (TabularDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(TabularDataDTO));

            MonthDTO getMonth = CommonController.getMonthListFortnight(frontEndData.period);

            string userName = HttpContext.User.Identity.Name;
            List<string> adpIds = new List<string>();
            adpIds.Add("ALL");
            List<DashboardGraphDataDTO> graphData = new List<DashboardGraphDataDTO>();
            string empAdpIdData = (from e in db.employeeBaseDetails where e.email.Equals(userName) && e.status.Equals("ACTIVE") select e.adpId).FirstOrDefault();
            if (empAdpIdData!= null && empAdpIdData != "")
            {
                adpIds.Add(empAdpIdData);
            }
            List<DashboardGraphs> graphs = (from g in db.dashboardGraphs where adpIds.Contains(g.adpId) select g).ToList();
            if (graphs != null && graphs.Count>0)
            {
                graphData = FetchGraphData(graphs,frontEndData);
            }
            object jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    graphData = graphData
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public List<DashboardGraphDataDTO> FetchGraphData(List<DashboardGraphs> graphs,TabularDataDTO frontData)
        {
            List<DashboardGraphDataDTO> data = new List<DashboardGraphDataDTO>();

            foreach (DashboardGraphs d in graphs)
            {
                DashboardGraphDataDTO temp = new DashboardGraphDataDTO();
                temp.graphDetail = d;
                temp.graphData = new Dictionary<string, GraphValueDTO>();
                List<string> xAxisLabels = new List<string>();
                List<string> yAxisLabels = new List<string>();
                // fetch x-axis labels 
                switch (temp.graphDetail.xAxis)
                {
                    case "SalesHorizontal":
                        if (temp.graphDetail.xAxisSubKey.Equals("ALL"))
                        {
                            xAxisLabels = (from k in db.keyValueData where 
                                           k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(temp.graphDetail.xAxis) /*&& key.keyValueModel.Equals("SB1")*/ select key.labelOnScreen).FirstOrDefault()) 
                                           && k.keyName.Equals(temp.graphDetail.xAxis) select k.labelOnScreen).ToList();
                        }
                        else
                        {
                            xAxisLabels = (from k in db.keyValueData where 
                                           k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(temp.graphDetail.xAxis) /*&& key.keyValueModel.Equals(temp.graphDetail.xAxisSubKey)*/ select key.labelOnScreen).FirstOrDefault()) 
                                           && k.keyName.Equals(temp.graphDetail.xAxis) select k.labelOnScreen).ToList();
                        }
                        break;
                    case "SalesComponent":
                        break;
                    default:
                        break;
                }

                //y-axis labels
                switch (temp.graphDetail.yAxis)
                {
                    case "SalesHorizontal":
                        if (temp.graphDetail.yAxisSubKey.Equals("ALL"))
                        {
                            yAxisLabels = (from k in db.keyValueData where 
                                           k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(temp.graphDetail.xAxis) /*&& key.keyValueModel.Equals("SB1")*/ select key.labelOnScreen).FirstOrDefault()) 
                                           && k.keyName.Equals(temp.graphDetail.xAxis) select k.labelOnScreen).ToList();
                        }
                        else
                        {
                            yAxisLabels = (from k in db.keyValueData where 
                                           k.keyType.Equals((from key in db.keyValueData where key.keyType.Equals("MetaData") && key.keyName.Equals(temp.graphDetail.xAxis)/* && key.keyValueModel.Equals(temp.graphDetail.xAxisSubKey)*/ select key.labelOnScreen).FirstOrDefault()) 
                                           && k.keyName.Equals(temp.graphDetail.xAxis) select k.labelOnScreen).ToList();
                        }
                        break;
                    case "SalesComponent":
                        if (temp.graphDetail.yAxisSubKey.Equals("ALL"))
                        {
                            yAxisLabels = (from k in db.keyValueData where k.keyType.Equals("MetaData") 
                                            && k.keyName.Equals(temp.graphDetail.yAxis) select k.labelOnScreen).ToList();
                        }
                        else
                        {
                            yAxisLabels = (from k in db.keyValueData where k.keyType.Equals("MetaData") 
                                            && k.keyName.Equals(temp.graphDetail.yAxis) && k.keyValue.Equals(temp.graphDetail.yAxisSubKey) select k.labelOnScreen).ToList();
                        }
                        break;
                    default:
                        break;
                }

               
                // prepare data dictionary
                if(xAxisLabels != null && xAxisLabels.Count > 0)
                {
                    temp.graphXAxis = xAxisLabels;
                    foreach (string s in xAxisLabels)
                    {
                        if (temp.graphData.ContainsKey(s))
                        {
                            //temp.graphData[s].
                        }
                        else
                        {
                            GraphValueDTO tempDataValues = db.EmployeeActualAndTargets
                                                   .Where(e => e.RP1 == s)
                                                   .GroupBy(e => e.RP1)
                                                   .Select(e => new GraphValueDTO
                                                   {
                                                       target = e.Sum(i => i.targetAmt),
                                                       actual = e.Sum(i => i.netActualAmt),
                                                       
                                                       //performance = (e.Sum(i => i.netActualAmt)/ e.Sum(i => i.targetAmt))*100
                                                   }).FirstOrDefault();
                            if (tempDataValues == null)
                            {
                                temp.graphData.Add(s,new GraphValueDTO());
                            }
                            else{
                                // to make empty entres and avoid null values
                                temp.graphData.Add(s, tempDataValues);
                            }
                        }
                    }
                }

                data.Add(temp);
            }
            return data;
        }

        public UserDashboardDTO monthData(string startMonth, string endMonth, string period)
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);

            UserDashboardDTO month = new UserDashboardDTO();           

            switch (period)
            {
                case "YTD":
                    if (startMonth == "YTD-Jan")
                    {
                        month.startMonth = 1;
                        month.endMonth = 1;
                    }
                    if (startMonth == "YTD-Feb")
                    {
                        month.startMonth = 1;
                        month.endMonth = 2;
                    }
                    if (startMonth == "YTD-Mar")
                    {
                        month.startMonth = 1;
                        month.endMonth = 3;
                    }
                    if (startMonth == "YTD-Apr")
                    {
                        month.startMonth = 1;
                        month.endMonth = 4;
                    }
                    if (startMonth == "YTD-May")
                    {
                        month.startMonth = 1;
                        month.endMonth = 5;
                    }
                    if (startMonth == "YTD-Jun")
                    {
                        month.startMonth = 1;
                        month.endMonth = 6;
                    }
                    if (startMonth == "YTD-Jul")
                    {
                        month.startMonth = 1;
                        month.endMonth = 7;
                    }
                    if (startMonth == "YTD-Aug")
                    {
                        month.startMonth = 1;
                        month.endMonth = 8;
                    }
                    if (startMonth == "YTD-Sep")
                    {
                        month.startMonth = 1;
                        month.endMonth = 9;
                    }
                    if (startMonth == "YTD-Oct")
                    {
                        month.startMonth = 1;
                        month.endMonth = 10;
                    }
                    if (startMonth == "YTD-Nov")
                    {
                        month.startMonth = 1;
                        month.endMonth = 11;
                    }
                    if (startMonth == "YTD-Dec")
                    {
                        month.startMonth = 1;
                        month.endMonth = 12;
                    }
                    break;

                case "QTD":
                    if(startMonth == "Q1")
                    {
                        month.startMonth = 1;
                        month.endMonth = 3;
                    }
                    if (startMonth == "Q2")
                    {
                        month.startMonth = 1;
                        month.endMonth = 3;
                    }
                    if (startMonth == "Q3")
                    {
                        month.startMonth = 1;
                        month.endMonth = 3;
                    }
                    if (startMonth == "Q4")
                    {
                        month.startMonth = 10;
                        month.endMonth = 12;
                    }
                    if (startMonth == "H1")
                    {
                        month.startMonth = 1;
                        month.endMonth = 6;
                    }
                    if(startMonth == "H2")
                    {
                        month.startMonth = 7;
                        month.endMonth = 12;
                    }
                    if(startMonth == "FY")
                    {
                        month.startMonth = 1;
                        month.endMonth = 12;
                    }
                     break;

                case "FTM":
                    if (startMonth.ToLower() == "jan")
                    {
                        month.startMonth = 1;
                        month.endMonth = 1;
                    }
                    if (startMonth.ToLower() == "feb")
                    {
                        month.startMonth = 2;
                        month.endMonth = 2;
                    }
                    if (startMonth.ToLower() == "mar")
                    {
                        month.startMonth = 3;
                        month.endMonth = 3;
                    }
                    if (startMonth.ToLower() == "apr")
                    {
                        month.startMonth = 4;
                        month.endMonth = 4;
                    }
                    if (startMonth.ToLower() == "may")
                    {
                        month.startMonth = 5;
                        month.endMonth = 5;
                    }
                    if (startMonth.ToLower() == "jun")
                    {
                        month.startMonth = 6;
                        month.endMonth = 6;
                    }
                    if (startMonth.ToLower() == "jul")
                    {
                        month.startMonth = 7;
                        month.endMonth = 7;
                    }
                    if (startMonth.ToLower() == "aug")
                    {
                        month.startMonth = 8;
                        month.endMonth = 8;
                    }
                    if (startMonth == "sep")
                    {
                        month.startMonth = 9;
                        month.endMonth = 9;
                    }
                    if (startMonth == "oct")
                    {
                        month.startMonth = 10;
                        month.endMonth = 10;
                    }
                    if (startMonth == "nov")
                    {
                        month.startMonth = 11;
                        month.endMonth = 11;
                    }
                    if (startMonth == "dec")
                    {
                        month.startMonth = 12;
                        month.endMonth = 12;
                    }
                    break;
   
                        case "default":
                    break;

            }
            return month;
        }


        ///////////get current calender month///////////
        public static int GetCurrentMonth()
        {
            DBModels db = new DBModels();
            return Convert.ToInt32((from c in db.keyValueData
                                    where c.keyType == "OPERATIONAL"
                                    && c.keyName == "CAM"
                                    select c.keyValue).FirstOrDefault());

        }

        public JsonResult GetDashboardData()
        {
            Stream req = Request.InputStream;
            UploadActualDTO actualRequestDetails = (UploadActualDTO)Utility.mapRequestToClass(Request.InputStream, typeof(UploadActualDTO));

            
            EmployeeRole reqParam = (from u in db.employeeRole
                                     where u.adpId == actualRequestDetails.adpId
                                     && u.role == actualRequestDetails.role
                                     select u).FirstOrDefault();


            //get user requiredParam
            KeyValueData getFK = (from e in db.keyValueData where e.keyType == "TeamSetup" && e.keyName == "MetaData" && e.keyValue == reqParam.RP1 select e).FirstOrDefault();

           int getRoleSequence = (from e in db.keyValueDataLink
                                                where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                                e.keyValue == reqParam.role select e.attrSequence).FirstOrDefault();

            List<string> getNewRole = (from e in db.keyValueDataLink where e.keyValueFK == getFK.id && e.keyName == "Role" && e.attrSequence == getRoleSequence + 1 select e.keyValue).Distinct().ToList();
            List<string> roleRequiredParam = (from e in db.keyValueDataLink
                                              where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                               e.attrSequence == getRoleSequence + 1
                                              select e.requiredSalesHorizontals).Distinct().ToList();
            if (getNewRole.Count() == 0)
            {
                getNewRole = (from e in db.keyValueDataLink where e.keyValueFK == getFK.id && e.keyName == "Role" && e.attrSequence == getRoleSequence select e.keyValue).Distinct().ToList();
                roleRequiredParam = (from e in db.keyValueDataLink
                                     where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                      e.attrSequence == getRoleSequence
                                     select e.requiredSalesHorizontals).Distinct().ToList();
            }

           

            List<string> selfRequiredParam = (from e in db.keyValueDataLink
                                              where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                               e.attrSequence == getRoleSequence 
                                              select e.requiredSalesHorizontals).Distinct().ToList();


            MyDashboard graphData = new MyDashboard();
            //Dictionary<string, MyDashboard> getData = new Dictionary<string, MyDashboard>();
           
         
            UserDashboardDTO month = new UserDashboardDTO();           
            if (actualRequestDetails.period != "" && actualRequestDetails.month!="")
            {
                month = monthData(actualRequestDetails.month, actualRequestDetails.month, actualRequestDetails.period);
            }
            else
            {
                month.startMonth = 1;
                int lastDataUpload = GetCurrentMonth();
                month.endMonth = lastDataUpload;
            }

            //check head role or not 
            KeyValueData managementRole = (from e in db.keyValueData where e.keyType == reqParam.RP1 && e.keyName == "Channel" select e).FirstOrDefault();
            List<string> RP1 = new List<string>();
            if(managementRole != null)
            {
                RP1 = (from e in db.keyValueData where e.keyType == "Channel" select e.keyValue).Distinct().ToList();
            }
            else
            {
                RP1.Add(reqParam.RP1);
            }

            List<MyDashboard> tabularList = new List<MyDashboard>();
            foreach (string itmRole in getNewRole)
            {                
                IEnumerable<EmployeeRole> EmpList = from p in db.employeeRole where p.role == itmRole select p;
                for(int i = 0; i< selfRequiredParam.Count();i++)
                {
                    switch(i)
                    {
                        case 0:
                            EmpList = EmpList.Where(x => RP1.Contains(x.RP1));
                            break;
                        case 1:
                            EmpList = EmpList.Where(x => x.RP2 == reqParam.RP2);
                            break;
                        case 2:
                            EmpList = EmpList.Where(x => x.RP3 == reqParam.RP3);
                            break;
                        case 3:
                            EmpList = EmpList.Where(x => x.RP4 == reqParam.RP4);
                            break;
                        case 4:
                            EmpList = EmpList.Where(x => x.RP5 == reqParam.RP5);
                            break;
                    }
                }
                graphData = new MyDashboard();
                graphData.target.Add("Target");
                graphData.actual.Add("Actual");
                foreach (EmployeeRole itmData in EmpList)
                {
                    graphData.employeeData = itmData;
                    var achievmentData = from e in db.EmployeeActualAndTargets select e;
                    for(int i = 0; i < roleRequiredParam.Count(); i++)
                    {
                        switch(i)
                        {
                            case 0:                                
                                achievmentData = achievmentData.Where(x => x.RP1 == itmData.RP1);
                                break;
                            case 1:
                                achievmentData = achievmentData.Where(x => x.RP2 == itmData.RP2);
                                break;
                            case 2:
                                achievmentData = achievmentData.Where(x => x.RP3 == itmData.RP3);
                                break;
                            case 3:
                                achievmentData = achievmentData.Where(x => x.RP4 == itmData.RP4);
                                break;
                            case 4:
                                achievmentData = achievmentData.Where(x => x.RP5 == itmData.RP5);
                                break;                                                      
                        }
                    }
                    decimal target = 0;
                    decimal actual = 0;

                    //Add in tabular
                    MyDashboard tabularMonthlyData = new MyDashboard();
                    tabularMonthlyData = GetTabularData(achievmentData);
                    tabularMonthlyData.employeeData = itmData;
                    tabularList.Add(tabularMonthlyData);
                      

                    foreach (var data in achievmentData)
                    {
                        target = target + data.targetAmt;
                        actual = actual + data.netActualAmt;
                    }
                    graphData.target.Add(target.ToString());
                    graphData.actual.Add(actual.ToString());
                    if (target > 0)
                    {
                        graphData.achievement.Add(((actual / target) * 100).ToString());
                    }
                    else
                    {
                        graphData.achievement.Add("0");
                    }
                    graphData.employee.Add(itmData.adpId);
                    graphData.employeeDataList.Add(itmData);
                    graphData.Param = itmData.RP1 + ":" + itmData.RP2;                    
                }
            }
            graphData.empParam = reqParam;
            graphData.tabularData = tabularList;
            var jsonsuccess = new
            {
                rsBody = new
                {
                    finalData = graphData
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public MyDashboard GetTabularData(IQueryable<EmployeeActualsAndTargets> achievmentData)
        {
                Dictionary<int, decimal> targetData = new Dictionary<int, decimal>();
                Dictionary<int, decimal> actualData = new Dictionary<int, decimal>();
                for (int i = 1; i <= 12; i++)
                {
                    foreach (var data in achievmentData)
                    {
                        if (!targetData.ContainsKey(i))
                        {
                            targetData.Add(i, data.targetAmt);
                        }
                        else
                        {
                            targetData[i] = targetData[i] + data.targetAmt;
                        }
                        if (!actualData.ContainsKey(i))
                        {
                            actualData.Add(i, data.netActualAmt);
                        }
                        else
                        {
                            actualData[i] = actualData[i] + data.netActualAmt;
                        }
                    }
                }

             MyDashboard tabularSortData = new MyDashboard();
            decimal q1Actual = 0M;
            decimal q1Target = 0M;
            decimal q2Actual = 0M;
            decimal q2Target = 0M;
            decimal q3Actual = 0M;
            decimal q3Target = 0M;
            decimal q4Actual = 0M;
            decimal q4Target = 0M;
            decimal FYActual = 0M;
            decimal FYTarget = 0M;

            foreach (KeyValuePair<int,decimal> itm in targetData)
            {
                tabularSortData.target.Add(Math.Round(itm.Value,2).ToString());
                tabularSortData.actual.Add(Math.Round(actualData[itm.Key],2).ToString());
                if(itm.Value > 0)
                {
                    tabularSortData.achievement.Add(Math.Round(actualData[itm.Key],2).ToString() + "%");
                }
                else
                {
                    tabularSortData.achievement.Add("0");
                }

                switch(itm.Key)
                {
                    case 1:
                        q1Actual = actualData[itm.Key];
                        q1Target = itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 2:
                        q1Actual = q1Actual + actualData[itm.Key];
                        q1Target = q1Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 3:
                        q1Actual = q1Actual + actualData[itm.Key];
                        q1Target = q1Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        tabularSortData.target.Add(Math.Round(q1Target,2).ToString());
                        tabularSortData.actual.Add(Math.Round(q1Actual,2).ToString());
                        if (q1Target > 0)
                        {
                            tabularSortData.achievement.Add(Math.Round(((q1Actual / q1Target)*100),2).ToString() + "%");
                        }
                        else
                        {
                            tabularSortData.achievement.Add("0");
                        }
                        break;

                    case 4:
                        q2Actual = actualData[itm.Key];
                        q2Target = itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 5:
                        q2Actual = q2Actual + actualData[itm.Key];
                        q2Target = q2Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 6:
                        q2Actual = q2Actual + actualData[itm.Key];
                        q2Target = q2Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        tabularSortData.target.Add(Math.Round(q2Target,2).ToString());
                        tabularSortData.actual.Add(Math.Round(q2Actual,2).ToString());
                        if (q2Target > 0)
                        {
                            tabularSortData.achievement.Add(Math.Round(((q2Actual / q2Target)*100),2).ToString() + "%");
                        }
                        else
                        {
                            tabularSortData.achievement.Add("0");
                        }
                        break;
                    case 7:
                        q3Actual = actualData[itm.Key];
                        q3Target = itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 8:
                        q3Actual = q3Actual + actualData[itm.Key];
                        q3Target = q3Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 9:
                        q3Actual = q3Actual + actualData[itm.Key];
                        q3Target = q3Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        tabularSortData.target.Add(Math.Round(q3Target,2).ToString());
                        tabularSortData.actual.Add(Math.Round(q3Actual,2).ToString());
                        if (q2Target > 0)
                        {
                            tabularSortData.achievement.Add(Math.Round(((q3Actual / q3Target)*100),2).ToString() + "%");
                        }
                        else
                        {
                            tabularSortData.achievement.Add("0");
                        }
                        break;
                    case 10:
                        q4Actual = q4Actual + actualData[itm.Key];
                        q4Target = q4Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 11:
                        q4Actual = q4Actual + actualData[itm.Key];
                        q4Target = q4Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        break;

                    case 12:
                        q4Actual = q4Actual + actualData[itm.Key];
                        q4Target = q4Target + itm.Value;
                        FYActual = FYActual + actualData[itm.Key];
                        FYTarget = FYTarget + itm.Value;
                        tabularSortData.target.Add(Math.Round(q4Target,2).ToString());
                        tabularSortData.actual.Add(Math.Round(q4Actual,2).ToString());
                        if (q4Target > 0)
                        {
                            tabularSortData.achievement.Add(Math.Round(((q4Actual / q4Target)*100),2).ToString() + "%");
                        }
                        else
                        {
                            tabularSortData.achievement.Add("0");
                        }
                        //FY
                        tabularSortData.target.Add(Math.Round(FYTarget,2).ToString());
                        tabularSortData.actual.Add(Math.Round(FYActual,2).ToString());
                        if (FYTarget > 0)
                        {
                            tabularSortData.achievement.Add(Math.Round(((FYActual / FYTarget)*100),2).ToString() + "%");
                        }
                        else
                        {
                            tabularSortData.achievement.Add("0");
                        }
                        break;
                }
            }

            return tabularSortData;
        }
    }
}