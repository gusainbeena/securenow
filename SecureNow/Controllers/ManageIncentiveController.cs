﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SecureNow.AuthFilter;
using SecureNow.Container;
using PHIncentiveApp.CustomFilters;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace SecureNow.Controllers
{
    [UserAuthenticationFilter]
    public class ManageIncentiveController : Controller
    {
        public static DBModels db = new DBModels();
        public static List<KeyValueData> getKeyValueData(string channel, string requiredParameter, string otherParameter)
        {
            return (from i in db.keyValueData
                    where i.keyType == channel
                    && i.keyName == requiredParameter
                    && i.relatedKeyType == otherParameter
                    select i).ToList();
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetIncentiveList()
        {
            Stream req = Request.InputStream;
            IncentiveCalculationRequest incentiveList = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));

            int page = 0;
            string message = null;
            List<IncentiveCalculationRequest> resulats = null;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;
            int currentMonth = DateTime.Now.Month;
            if (incentiveList.incentiveName != null)
            {
                resulats = (from d in db.incentiveCalculationRequest
                            where (d.incentiveName.ToLower().Contains(incentiveList.incentiveName)
                            )
                            orderby d.id descending
                            select d).Skip(skip).Take(pageSize).Distinct().ToList();
            }
            else
            {
                resulats = (from d in db.incentiveCalculationRequest
                            orderby d.id descending
                            select d).Skip(skip).Take(pageSize).ToList();
            }

            foreach (IncentiveCalculationRequest item in resulats)
            {
                item.status = item.status.Replace("_", " ");
                //item.incentiveName = item.incentiveName.Replace("_", " ");
            }

            if (resulats.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = resulats,
                    sucess = message,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NextAndPriviousIncentive()
        {
            Stream req = Request.InputStream;
            NextAndPriviousDTO nextAndPrivious = (NextAndPriviousDTO)Utility.mapRequestToClass(Request.InputStream, typeof(NextAndPriviousDTO));

            int page = 0;
            if (nextAndPrivious.pageNumber != null)
            {
                page = Convert.ToInt32(nextAndPrivious.pageNumber);
            }
            string message = null;

            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;
            List<IncentiveCalculationRequest> results = null;

            results = (from d in db.incentiveCalculationRequest
                       orderby d.id descending
                       select d).Skip(skip).Take(pageSize).ToList();

            if (results.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }
            foreach (IncentiveCalculationRequest item in results)
            {
                item.status = item.status.Replace("_", " ");
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = results,
                    sucess = message
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetViewIncentive()
        {
            Stream req = Request.InputStream;
            IncentiveCalculationRequest incentiveList = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));

            EmployeeRequestDTO retval = new EmployeeRequestDTO();
            retval.incentiveCofirmation = new List<IncentiveDTO>();
            retval.incentiveRequest = new List<IncentiveCalculationRequest>();
            string userName = HttpContext.User.Identity.Name.ToString();
            string role = (from e in db.employeeBaseDetails
                           join d in db.employeeRole on e.adpId equals d.adpId
                           where e.email.Equals(userName)
                           select d.role).FirstOrDefault();

            int currentMonth = DateTime.Now.Month;
            IncentiveCalculationRequest resulats = null;
            List<IncentiveDTO> incentiveCof = null;
            if (incentiveList.id != 0)
            {
                resulats = (from d in db.incentiveCalculationRequest
                            where (d.id.Equals(incentiveList.id)
                            )
                            select d).FirstOrDefault();
                retval.incentiveRequest.Add(resulats);

                if (resulats != null)
                {
                    incentiveCof = (from ic in db.incentiveConfirmations
                                    where ic.incentiveRequestId.Equals(resulats.id)
                                    select new IncentiveDTO
                                    {
                                        id = ic.id,
                                        adpId = ic.adpId,
                                        incentiveRequestId = ic.incentiveRequestId,
                                        channelCode = ic.channelCode,
                                        role = ic.role,
                                        branchDetailsFK = ic.branchDetailsFK,
                                        status = ic.status,
                                        comments = ic.comments,
                                        actionTakenDate = ic.actionTakenDate,
                                        pathToFile = ic.pathToFile,

                                    }).ToList();

                }

                foreach (IncentiveDTO item in incentiveCof)
                {
                    EmployeeRoleAndAreaDTO empDetails = (from emp in db.employeeBaseDetails
                                                         join r in db.employeeRole on emp.adpId equals r.adpId
                                                         where emp.adpId.Equals(item.adpId)
                                                         select new EmployeeRoleAndAreaDTO
                                                         {
                                                             fullName = emp.fullName,
                                                             // region = r.region
                                                         }).FirstOrDefault();


                    item.fullName = empDetails.fullName;
                    item.region = empDetails.region;
                    int commentId = (from e in db.incentiveConfirmations where e.mainIdPartial == item.id select e.id).FirstOrDefault();
                    if (commentId > 0)
                    {
                        string getComment = (from e in db.incentivePartiallyApproved where e.incentiveConfirmationFK == commentId select e.comment).FirstOrDefault();
                        item.comments = getComment;
                    }
                    List<KeyValueData> keyValueData = (from d in db.keyValueData
                                                       select d).ToList();

                    foreach (KeyValueData key in keyValueData)
                    {
                        if (key.keyValue == item.channelCode && key.keyType != "CUSTOMERACTUALSMAPPING")
                        {
                            item.channelCode = key.description;
                        }
                        if (key.keyValue == item.role && key.keyName == "ROLE")
                        {
                            item.role = key.description;
                        }
                    }

                    item.status = item.status.Replace("_", " ");


                    /////////////For Showing button in admin screen//////////////
                    string adpIdV = "VACANT";
                    if (item.adpId.ToLower().Contains(adpIdV.ToLower()))
                    {
                        item.showApproval = true;
                    }
                    else
                    {
                        item.showApproval = false;
                    }

                    retval.incentiveCofirmation.Add(item);
                }

            }
            retval.role = role;


            var jsonsuccess = new
            {
                rsBody = new
                {
                    retval = retval,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ApproveAndRejectIncentiveSheet()
        {
            Stream req = Request.InputStream;
            IncentiveConfirmationsDTO incentiveList = (IncentiveConfirmationsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveConfirmationsDTO));

            IncentiveConfirmations incentiveStatus = (from p in db.incentiveConfirmations
                                                      where p.id.Equals(incentiveList.id)
                                                      select p).FirstOrDefault();

            var jsonData = JsonConvert.SerializeObject(incentiveList.incentiveError);
            string incentiveError = jsonData.ToString();
            // Today Date
            DateTime today = DateTime.Today.ToUniversalTime();
            long sToday = DateTime.Now.Ticks;
            var tDate = new DateTime(sToday);
            string currentDate = tDate.ToString("dd-MM-yyyy");

            if (incentiveStatus != null)
            {
                incentiveStatus.status = incentiveList.status;
                incentiveStatus.comments = incentiveList.comments;
                incentiveStatus.incentiveError = incentiveError;
                if (incentiveStatus.status == Codes.InStatus.APPROVED.ToString())
                {
                    IncentiveCalculationRequest incentiveRequests = (from d in db.incentiveCalculationRequest
                                                                     where d.id.Equals(incentiveStatus.incentiveRequestId)
                                                                     select d).FirstOrDefault();
                    incentiveStatus.quarter = incentiveRequests.quarter;
                    incentiveStatus.yearRecorded = incentiveRequests.year;

                }
                db.SaveChanges();

                EmployeeRoleAndAreaDTO approveOrRejectBy = (from e in db.employeeBaseDetails
                                                            join r in db.employeeRole on e.adpId equals r.adpId
                                                            where r.adpId.Equals(incentiveStatus.adpId)
                                                            select new EmployeeRoleAndAreaDTO
                                                            {
                                                                adpId = e.adpId,
                                                                fullName = e.firstName,
                                                                email = e.email
                                                            }).FirstOrDefault();

                IncentiveCalculationRequest incentiveRequest = (from ic in db.incentiveCalculationRequest
                                                                where ic.id.Equals(incentiveStatus.incentiveRequestId)
                                                                select ic
                                                ).FirstOrDefault();

                List<EmployeeRoleAndAreaDTO> employeeDetails = new List<EmployeeRoleAndAreaDTO>();
                EmployeeRoleAndAreaDTO empData = null;
                if (incentiveRequest != null)
                {
                    empData = (from e in db.employeeBaseDetails
                               join r in db.employeeRole on e.adpId equals r.adpId
                               where r.adpId.Equals(incentiveRequest.adpId)
                               select new EmployeeRoleAndAreaDTO
                               {
                                   adpId = e.adpId,
                                   fullName = e.firstName,
                                   email = e.email
                               }).FirstOrDefault();

                    EmployeeRoleAndAreaDTO fcDetails = (from e in db.employeeBaseDetails
                                                        join r in db.employeeRole on e.adpId equals r.adpId
                                                        where r.role.Equals("FC") && e.status == "ACTIVE"
                                                        select new EmployeeRoleAndAreaDTO
                                                        {
                                                            adpId = e.adpId,
                                                            fullName = e.firstName,
                                                            email = e.email
                                                        }).FirstOrDefault();

                    employeeDetails.Add(fcDetails);
                }
                // Email For Admin & Finanacial Controller
                List<KeyValueData> keyValueData = (from k in db.keyValueData
                                                   where k.keyType.Equals("IncentiveEmail")
                                                   select k).ToList();

                foreach (EmployeeRoleAndAreaDTO item in employeeDetails)
                {
                    //////after creating change request send mail to the first Approver///////
                    string emailId = null;
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(Server.MapPath("~/views/Templates/EmailIncentiveApproval.cshtml")))
                    {
                        body = reader.ReadToEnd();
                    }
                    //get initiator details


                    emailId = item.email;
                    //emailId = "msharma@knowledgeops.co.in";
                    body = body.Replace("{employeeName}", item.fullName);
                    body = body.Replace("{name}", approveOrRejectBy.fullName);
                    body = body.Replace("{email}", approveOrRejectBy.email);
                    body = body.Replace("{date}", currentDate);
                    body = body.Replace("{status}", incentiveStatus.status);
                    string subject = "Incentive Request";
                    Utility.sendMaillTOFC("mmjadon@knowledgeops.co.in", body, subject, empData.email);
                }
            }

            var jsonsuccess = new
            {
                rsBody = "sucess"
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateIncentive()
        {
            Stream req = Request.InputStream;
            IncentiveCalculationRequest incentiveList = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));

            string message = null;
            IncentiveCalculationRequest resulats = null;
            if (incentiveList.id != 0)
            {
                // Check Incentive Exist or Not particular ID
                resulats = (from d in db.incentiveCalculationRequest
                            where (d.id.Equals(incentiveList.id)
                            )
                            select d).FirstOrDefault();
                if (resulats != null)
                {
                    if (incentiveList.status == Codes.InStatus.CLOSED.ToString())
                    {
                        // If Status is CLosed Check All Confirmation DISBURSE Or Not For Particular ID
                        List<IncentiveConfirmations> incentiveDetails = (from d in db.incentiveConfirmations
                                                                         where d.incentiveRequestId.Equals(resulats.id)
                                                                         && d.status != Codes.InStatus.DISBURSE.ToString()
                                                                         select d).ToList();
                        if (incentiveDetails.Count == 0)
                        {
                            // Update Status Closed
                            resulats.status = incentiveList.status;
                            resulats.comments = incentiveList.comments;
                            db.SaveChanges();

                            // Sucess Message for Closed Incentive
                            message = ExceptionMessage.getExceptionMessage("Message.Sucess");
                        }
                        else
                        {
                            // Error Message For Closed Incentive
                            message = ExceptionMessage.getExceptionMessage("IncentiveRequest.Close.Unique");
                        }

                    }
                    else
                    {
                        // Update Status Canncel
                        resulats.status = incentiveList.status;
                        resulats.comments = incentiveList.comments;
                        db.SaveChanges();
                        // Message
                        message = ExceptionMessage.getExceptionMessage("Message.Sucess");
                    }

                }
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    message = message
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DownloadPerformanceSheet()
        {
            Stream req = Request.InputStream;
            IncentiveCalculationRequest incentiveList = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));
            string path = null;
            path = (from p in db.incentiveConfirmations
                    where p.id.Equals(incentiveList.id)
                    select p.pathToFile).FirstOrDefault();
            string getStatus = (from e in db.incentiveConfirmations where e.id == incentiveList.id select e.status).FirstOrDefault();
            if (getStatus == "PARTIALLY_APPROVED" || getStatus == "PARTIALLY_QUEUED_FOR_PAYOUT_CARD" || getStatus == "PARTIALLY_PAYOUT_CARD_GENERATED")
            {
                string fileName = (from e in db.incentiveConfirmations
                                   join f in db.incentivePartiallyApproved on e.id equals f.incentiveConfirmationFK
                                   where e.mainIdPartial == incentiveList.id && f.action == Codes.status.PARTIALLY_APPROVED.ToString()
                                   select f.fileName).FirstOrDefault();
                if (fileName != null && fileName != "")
                {
                    path = fileName;
                }
            }

            if (path == null)
            {
                path = "noExist";
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    path = path,
                    result = "success"
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }       
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReGenerateIncentiveSheet()
        {
            Stream req = Request.InputStream;
            IncentiveConfirmations incetoveDetails = (IncentiveConfirmations)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveConfirmations));


            IncentiveConfirmations results = null;

            results = (from d in db.incentiveConfirmations
                       where d.id.Equals(incetoveDetails.id)
                       select d).FirstOrDefault();

            if (results != null)
            {
                results.status = Codes.InStatus.QUEUED_FOR_DATA.ToString();
                db.SaveChanges();
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetEmployeeIncentiveDetails()
        {
            Stream req = Request.InputStream;
            IncentiveConfirmations incentiveList = (IncentiveConfirmations)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveConfirmations));

            IncentiveConfirmations IncentiveDetails = (from p in db.incentiveConfirmations
                                                       where p.id.Equals(incentiveList.id)
                                                       select p).FirstOrDefault();

            var jsonsuccess = new
            {
                rsBody = IncentiveDetails
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckForCloser()
        {
            Stream req = Request.InputStream;
            IncentiveCalculationRequest incentiveList = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));

            EmployeeRequestDTO retval = new EmployeeRequestDTO();
            retval.incentiveCofirmation = new List<IncentiveDTO>();
            retval.incentiveRequest = new List<IncentiveCalculationRequest>();

            int currentMonth = DateTime.Now.Month;
            IncentiveCalculationRequest resulats = null;
            List<IncentiveDTO> incentiveCof = null;
            string message = "";
            if (incentiveList.id != 0)
            {
                resulats = (from d in db.incentiveCalculationRequest
                            where (d.id.Equals(incentiveList.id)
                            )
                            select d).FirstOrDefault();
                retval.incentiveRequest.Add(resulats);

                if (resulats != null)
                {
                    incentiveCof = (from ic in db.incentiveConfirmations
                                    where ic.incentiveRequestId.Equals(resulats.id) && !(ic.status.Equals(Codes.InStatus.DISBURSE.ToString()) || ic.status.Equals("PARTIAL_DISBURSE"))
                                    select new IncentiveDTO
                                    {
                                        id = ic.id,
                                        adpId = ic.adpId,
                                        incentiveRequestId = ic.incentiveRequestId,
                                        channelCode = ic.channelCode,
                                        role = ic.role,
                                        branchDetailsFK = ic.branchDetailsFK,
                                        status = ic.status,
                                        comments = ic.comments,
                                        actionTakenDate = ic.actionTakenDate,
                                        pathToFile = ic.pathToFile,

                                    }).ToList();

                }

                if (incentiveCof != null && incentiveCof.Count > 0)
                {
                    message = "error";
                }
                else
                {
                    message = "success";
                }

            }



            var jsonsuccess = new
            {
                rsBody = new
                {
                    message = message,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GenerateIncentiveRequest()
        {
            Stream req = Request.InputStream;
            db = new DBModels();
            IncentiveCalculationRequest incentiveBaseDetails = (IncentiveCalculationRequest)Utility.mapRequestToClass(Request.InputStream, typeof(IncentiveCalculationRequest));

            ExceptionResponseContainer retVal = CustomValidator.applyValidations(incentiveBaseDetails, typeof(IncentiveCalculationValidate));
            int currentMonth = DateTime.Now.Month;
            //string currentQuarter = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" select e.keyValue).FirstOrDefault();
            MonthDTO getMonth = CommonController.getMonthList(incentiveBaseDetails.quarter);

            List<EmployeeRoleAndAreaDTO> employeeList = new List<EmployeeRoleAndAreaDTO>();
            string userName = HttpContext.User.Identity.Name;
            string adpId = (from d in db.employeeBaseDetails
                            where d.email.Equals(userName)
                            select d.adpId).FirstOrDefault();
            string message = null;
            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }

            else
            {
                IncentiveCalculationRequest incentiveRequest = null;
                incentiveRequest = (from inr in db.incentiveCalculationRequest
                                    where inr.incentiveName.Equals(incentiveBaseDetails.incentiveName)
                                    && inr.year.Equals(incentiveBaseDetails.year)
                                    && inr.quarter.Equals(incentiveBaseDetails.quarter)
                                    select inr).FirstOrDefault();
                if (incentiveRequest == null)
                {
                    incentiveRequest = (from inr in db.incentiveCalculationRequest
                                        where inr.status != Codes.InStatus.CLOSED.ToString()
                                        select inr).FirstOrDefault();
                }
                if (incentiveRequest == null)
                {

                    incentiveBaseDetails.status = Codes.InStatus.OPEN.ToString();
                    incentiveBaseDetails.adpId = adpId;

                    db.incentiveCalculationRequest.Add(incentiveBaseDetails);
                    db.SaveChanges();

                    List<KeyValueData> crRaiseList = (from key in db.keyValueData
                                                      where key.keyName.Equals("INCENTIVELIST")
                                                      select key).ToList();

                    foreach (KeyValueData item in crRaiseList)
                    {

                        // Get Employee Role Details
                        List<EmployeeRoleAndAreaDTO> emprole = (from d in db.employeeRole
                                                                join emp in db.employeeBaseDetails on d.adpId equals emp.adpId
                                                                join e in db.employeeGrade on d.id equals e.employeeRoleFK
                                                                where d.role.Equals(item.keyValue)
                                                                && d.RP1.Equals(item.keyType)
                                                                && d.startMonth <= getMonth.endMonth
                                                                && d.endMonth >= getMonth.startMonth
                                                                && e.currentlyAssigned == true
                                                                && emp.status.Equals("ACTIVE")
                                                                select new EmployeeRoleAndAreaDTO
                                                                {
                                                                    id = d.id,
                                                                    fullName = emp.fullName,
                                                                    adpId = d.adpId,
                                                                    role = d.role,
                                                                    RP1 = d.RP1,
                                                                    RP2 = d.RP2,
                                                                    RP3 = d.RP3,
                                                                    RP4 = d.RP4,
                                                                    RP5 = d.RP5,
                                                                    RP6 = d.RP6,
                                                                    RP7 = d.RP7,
                                                                    startMonth = d.startMonth,
                                                                    endMonth = d.endMonth,
                                                                    grade = e.grade,
                                                                    reportsToAdpId = d.reportsToAdpId,
                                                                    doj = emp.doj,
                                                                    yearRecorded = d.yearRecorded
                                                                }).ToList();
                    }

                    foreach (EmployeeRoleAndAreaDTO empList in employeeList)
                    {
                        IncentiveConfirmations incentiveCon = new IncentiveConfirmations();
                        incentiveCon.incentiveRequestId = incentiveBaseDetails.id;
                        incentiveCon.adpId = empList.adpId;
                        incentiveCon.channelCode = empList.channelCode;
                        incentiveCon.role = empList.role;
                        incentiveCon.region = empList.region;
                        incentiveCon.status = Codes.InStatus.QUEUED_FOR_DATA.ToString();
                        incentiveCon.yearRecorded = 0;

                        IncentiveConfirmations incCon = (from i in db.incentiveConfirmations where i.adpId.Equals(incentiveCon.adpId) && i.channelCode.Equals(incentiveCon.channelCode) && i.incentiveRequestId == incentiveCon.incentiveRequestId select i).FirstOrDefault();
                        if (incCon == null)
                        {
                            db.incentiveConfirmations.Add(incentiveCon);
                            db.SaveChanges();
                        }
                    }
                    message = ExceptionMessage.getExceptionMessage("Message.Sucess");
                }
                else
                {
                    message = ExceptionMessage.getExceptionMessage("IncentiveRequest.Incentive.Unique");
                }
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    message = message,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
    }

}