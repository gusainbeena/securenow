﻿using SecureNow.AuthFilter;
using SecureNow.Container;
using PHIncentiveApp.CustomFilters;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SecureNow.Controllers
{
    [UserAuthenticationFilter]
    public class ManageEmployeeController : Controller
    {
        DBModels db = new DBModels();

        // GET: EmployeeCreationDeletion
       
        public ActionResult Index()
        {
            return View();
        }

        public string encodePassword()
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[6];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < 6; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            string encPassword = "";
            string empPassword = new string(chars);
            var pass = empPassword;
            string EncryptionKey = "incentive_application";
            byte[] clearBytes = Encoding.Unicode.GetBytes(pass);
            using (TripleDES encryptor = TripleDES.Create())
            {

                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(24);
                encryptor.IV = pdb.GetBytes(8);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encPassword = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encPassword;
        }

        public string decodePassword(string encodePassword)
        {
            string newpassword = null;
            byte[] cipherBytes = Convert.FromBase64String(encodePassword);
            string EncryptionKey = "incentive_application";
            using (TripleDES encryptor = TripleDES.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(24);
                encryptor.IV = pdb.GetBytes(8);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    newpassword = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return newpassword;
        }
        // Create New Employee  Details
        //CreateEmployeeDetails
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateEmployee()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO empRquest = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));

           

            long dateOfJoining = 0;
            if (empRquest.type.ToUpper() == "CREATE")
            {
                DateTime dateFormate = DateTime.ParseExact(empRquest.employeeBaseDetail.dojStr, "yyyy-MM-d", CultureInfo.InvariantCulture);
                dateOfJoining = dateFormate.ToUniversalTime().Ticks;
                empRquest.employeeBaseDetail.password = encodePassword();
            }

            string body = string.Empty;
            ExceptionResponseContainer retVal = new ExceptionResponseContainer();
            string message = null;
            if (empRquest.type.ToUpper() == "CREATE")
            {
                retVal = CustomValidator.applyValidations(empRquest.employeeBaseDetail, typeof(EmployeeBaseDetailsValidate));
            }
            else
            {
                retVal.isValid = true;
            }
            if (retVal.isValid == true)
            {
                foreach (EmployeeRoleAndAreaDTO item in empRquest.employeeRole)
                {
                    retVal = CustomValidator.applyValidations(item, typeof(EmployeeRoleValidate));
                }
            }
            List<EmployeeRoleAndAreaDTO> resultsDb = new List<EmployeeRoleAndAreaDTO>();
            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {

                if (retVal.isValid)
                {
                    if (empRquest.type.ToUpper() == "CREATE")
                    {
                        // Insert Employee Base Details
                        empRquest.employeeBaseDetail.id = 0;
                        empRquest.employeeBaseDetail.fullName = empRquest.employeeBaseDetail.firstName + ' ' + empRquest.employeeBaseDetail.middleName + ' ' + empRquest.employeeBaseDetail.lastName;
                        empRquest.employeeBaseDetail.toBeConsidered = true;
                        empRquest.employeeBaseDetail.doj = dateOfJoining;
                        db.employeeBaseDetails.Add(empRquest.employeeBaseDetail);
                        db.SaveChanges();
                    }

                    if (empRquest.employeeRole.Count != 0)
                    {

                        foreach (EmployeeRoleAndAreaDTO item in empRquest.employeeRole)
                        {

                            //Insert new emprole and employeeGrade in db
                            EmployeeRole empRole = new EmployeeRole();
                            EmployeeGrade empGrade = new EmployeeGrade();
                            if (empRquest.type.ToUpper() == "CREATE")
                            {
                                empRole.adpId = empRquest.employeeBaseDetail.adpId;
                                item.yearRecorded = empRquest.employeeBaseDetail.yearRecorded;
                                item.adpId = empRole.adpId;
                            }
                            else
                            {
                                empRole.adpId = item.adpId;
                            }
                            empRole.RP1 = item.RP1;
                            empRole.RP2 = item.RP2;
                            empRole.RP3 = item.RP3;
                            empRole.RP4 = item.RP4;
                            empRole.RP5 = item.RP5;
                            empRole.RP6 = item.RP6;
                            empRole.endMonth = item.endMonth;
                            empRole.reportsToAdpId = item.reportsToAdpId;
                            empRole.role = item.role;
                            empRole.startMonth = item.startMonth;

                            /*List<string> requiredParameterData = (from e in db.keyValueData
                                                                  where e.keyType == item.channelCode && e.relatedKeyType == "REQUIRED"
                                                                  && e.keyName == item.role
                                                                  select e.keyValue).ToList();*/
                            empRole.yearRecorded = item.yearRecorded;
                            empRole.customCalculationStringOP = item.customCalculationStringOP;
                            empRole.customCalculationStringActuals = item.customCalculationStringActuals;

                            empGrade.currentlyAssigned = true;
                            empGrade.yearRecorded = empRole.yearRecorded;
                            empGrade.startMonth = empRole.startMonth;
                            empGrade.endMonth = empRole.endMonth;

                            db.employeeRole.Add(empRole);
                            db.SaveChanges();

                            empGrade.employeeRoleFK = empRole.id;
                            db.employeeGrade.Add(empGrade);
                            db.SaveChanges();

                            // get employee whose is already in this role and then end month update
                            Dictionary<string, List<string>> requiredParameter = endAllRole(item, empRole);                            
                        }
                    }


                    if (empRquest.type.ToUpper() == "CREATE")
                    {
                        string newpassword = decodePassword(empRquest.employeeBaseDetail.password);
                        body = "<p style='font-size: 14px; font-weight: 600; font-style: normal; line-height: 20px; letter-spacing: normal; '>Dear " + empRquest.employeeBaseDetail.firstName + ",</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>Welcome to the Incentiwiser Application</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>Your password for signing into Incentiwiser application is: <b> " + newpassword + "</b>. Your username would be your Signify email ID (for example: yy.zz@signify.com).</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>You may log in to the Incentiwiser Application by signing <a href='https://incentiwiser.com/'>https://incentiwiser.com</a></p><br>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '> <b>This is an automatically generated Email, Please do not reply</b><br /><b>  For your queries please email us at support@incentiwiser.com </ b ></p><br>" +
                              "<p style='font-size: 14px; font-weight: 600; font-style: normal; padding-top: 20px; line-height: 5px; letter-spacing: normal; color: #1f497d;'>Thanks & Regards</p>" +
                              "<p style='font-weight: 400; line-height: 0px; font-style: normal; letter-spacing: normal; color: #1f497d;'>Incentiwiser Application Support Team</p>";

                        string subject = "Welcome Incentiwiser Application";
                        string emailId = empRquest.employeeBaseDetail.email;
                        try
                        {
                            Utility.sendMaill(emailId, body, subject);
                        }
                        catch (Exception e)
                        {

                        }
                        message = ExceptionMessage.getExceptionMessage("Message.Sucess");
                    }
                }

            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resultsDb = resultsDb,
                    message = message
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        public static Dictionary<string, List<string>> endAllRole(EmployeeRoleAndAreaDTO item, EmployeeRole empRole)
        {
            DBModels db = new DBModels();
            //get required paramert and end all active role for that particular month
            Dictionary<string, List<string>> returnData = new Dictionary<string, List<string>>();
            List<string> requiredParameter = (from e in db.keyValueData
                                              where e.keyType == item.channelCode && e.relatedKeyType == "REQUIRED"
                                              && e.keyName == item.role
                                              select e.keyValue).ToList();

            List<EmployeeRole> getEmpRoleDetail = new List<EmployeeRole>();
            var query = from e in db.employeeRole where e.adpId != item.adpId && e.role == item.role
                          && e.endMonth >= item.startMonth && e.startMonth <= item.startMonth select e;
            for (int i = 0; i < requiredParameter.Count(); i++)
            {
                switch (i)
                {
                    case 0:
                        query = query.Where(x => x.RP1 == item.RP1);
                        break;
                    case 1:
                        query = query.Where(x => x.RP2 == item.RP2);
                        break;
                    case 2:
                        query = query.Where(x => x.RP3 == item.RP3);
                        break;
                    case 3:
                        query = query.Where(x => x.RP4 == item.RP4);
                        break;
                    case 4:
                        query = query.Where(x => x.RP5 == item.RP5);
                        break;
                    case 5:
                        query = query.Where(x => x.RP6 == item.RP6);
                        break;
                    case 6:
                        query = query.Where(x => x.RP7 == item.RP7);
                        break;
                }
            }


            if (getEmpRoleDetail.Count == 0 && requiredParameter.Contains("SUBAREACODEREQUIRED"))
            {
                //Make a relationship against this new employee
                string customerCode = item.seCode + "_Dummy";
                CustomerBaseDetails getCustomerDetail = (from e in db.customerBaseDetails where e.soldToCode == customerCode select e).FirstOrDefault();
                //insert relationship
                CustomerRelationshipDetails relationshipDetail = new CustomerRelationshipDetails();
                relationshipDetail.customerBaseDetailsFK = getCustomerDetail.id;
                relationshipDetail.endMonth = item.endMonth;
               // relationshipDetail.endMonthsDescription = item.endMonth.ToString();
                relationshipDetail.startMonth = item.startMonth;
                //relationshipDetail.startMonthDescription = item.startMonth.ToString();
                //relationshipDetail.subArea = "";
                relationshipDetail.yearRecorded = empRole.yearRecorded;
                relationshipDetail.employeeRoleFK = empRole.id;
                using (DBModels dbData = new DBModels())
                {
                    dbData.customerRelationshipDetails.Add(relationshipDetail);
                    dbData.SaveChanges();
                }

                List<string> customerdetail = new List<string>();
                customerdetail.Add(getCustomerDetail.soldToCode);
                returnData.Add("CUSTOMER", customerdetail);
            }

            foreach (EmployeeRole itm in getEmpRoleDetail)
            {
                using (DBModels dbData = new DBModels())
                {
                    //Stop emp role
                    EmployeeRole employeerole = (from e in dbData.employeeRole
                                                 where e.id == itm.id
                                                 select e).FirstOrDefault();

                    employeerole.endMonth = item.startMonth - 1;
                    dbData.SaveChanges();

                    EmployeeGrade getGrade = (from e in dbData.employeeGrade where e.employeeRoleFK == employeerole.id && e.currentlyAssigned == true select e).FirstOrDefault();
                    if (getGrade != null)
                    {
                        getGrade.endMonth = item.startMonth - 1;
                        dbData.SaveChanges();
                    }

                    //based on required data
                    CustomerRelationshipDetails getCustomerFK = new CustomerRelationshipDetails();

                    //getcustomerFacingRole Or Not
                    List<string> getFacingRole = (from e in db.keyValueData where e.keyType == itm.RP1 && e.keyName == "CUSTOMEREMPLOYEEMAP" select e.keyValue).ToList();
                    if (getFacingRole.Contains(itm.role))
                    {
                        //End previous role relationship
                        getCustomerFK = (from e in dbData.customerRelationshipDetails where e.employeeRoleFK == itm.id select e).FirstOrDefault();
                        getCustomerFK.endMonth = item.startMonth - 1;
                       // getCustomerFK.endMonthsDescription = (item.startMonth - 1).ToString();
                        dbData.SaveChanges();

                        CustomerRelationshipDetails insertNewRecord = new CustomerRelationshipDetails();
                        using (db = new DBModels())
                        {
                            insertNewRecord.customerBaseDetailsFK = getCustomerFK.customerBaseDetailsFK;
                            insertNewRecord.startMonth = item.startMonth;
                            //insertNewRecord.startMonthDescription = item.startMonth.ToString();
                            insertNewRecord.endMonth = item.endMonth;
                            //insertNewRecord.endMonthsDescription = item.endMonth.ToString();
                            insertNewRecord.yearRecorded = item.yearRecorded;
                            //insertNewRecord.subArea = "";
                            db.customerRelationshipDetails.Add(insertNewRecord);
                            db.SaveChanges();
                        }
                    }

                    //delete vacant position for same period
                    if (employeerole.adpId.ToLower().Contains("vacant") && employeerole.startMonth == item.startMonth)
                    {
                        //if vacant then delete all
                        if (employeerole != null)
                        {
                            dbData.employeeRole.Remove(employeerole);
                        }
                        if (getGrade != null)
                        {
                            dbData.employeeGrade.Remove(getGrade);
                        }

                        //Remove relationship
                        if (getFacingRole.Contains(itm.role))
                        {
                            if (getCustomerFK != null)
                            {
                                dbData.customerRelationshipDetails.Remove(getCustomerFK);
                            }
                        }
                        dbData.SaveChanges();
                    }
                }
            }


            returnData.Add("REQUIRED", requiredParameter);
            return returnData;

        }


        // End Employee Role Or Create New Employee
        // Create New Employee  Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EndExistingRoleAndCreateEmployeeDetails()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO empRquest = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));
            
            DateTime dateFormate = DateTime.Parse(empRquest.employeeBaseDetail.dojStr);
            long dateOfJoining = dateFormate.ToUniversalTime().Ticks;
            empRquest.employeeBaseDetail.password = encodePassword();
            string body = string.Empty;
            ExceptionResponseContainer retVal = null;

            retVal = CustomValidator.applyValidations(empRquest.employeeBaseDetail, typeof(EmployeeBaseDetailsValidate));

            if (retVal.isValid == true)
            {
                foreach (EmployeeRoleAndAreaDTO item in empRquest.employeeRole)
                {
                    retVal = CustomValidator.applyValidations(item, typeof(EmployeeRoleValidate));
                }
            }
            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // End Existing Employee Role 
                List<EmployeeRoleAndAreaDTO> resultsDb = new List<EmployeeRoleAndAreaDTO>();
                foreach (EmployeeRoleAndAreaDTO item in empRquest.employeeRole)
                {
                    List<string> requiredParameters = null;
                    List<EmployeeRoleAndAreaDTO> resultsDbData = new List<EmployeeRoleAndAreaDTO>();
                    requiredParameters = (from d in db.keyValueData
                                          where d.keyType == item.RP1
                                          &&
                                          d.keyName == item.role
                                          &&
                                          d.relatedKeyType == "REQUIRED"
                                          select d.keyValue).ToList();



                    List<EmployeeRoleAndAreaDTO> query = resultsDb = (from e in db.employeeRole
                                                                      join g in db.employeeGrade on e.id equals g.employeeRoleFK
                                                                      where e.role.Equals(item.role)
                                                                      && !e.adpId.ToLower().Contains("vacant")
                                                                      && e.startMonth <= item.startMonth
                                                                      && e.endMonth >= item.startMonth
                                                                      && g.startMonth <= item.startMonth
                                                                      && g.endMonth >= item.startMonth
                                                                      && e.yearRecorded.Equals(item.yearRecorded)
                                                                      select new EmployeeRoleAndAreaDTO
                                                                      {
                                                                          adpId = e.adpId,
                                                                          RP1 = e.RP1,
                                                                          RP2 = e.RP2,
                                                                          RP3 = e.RP3,
                                                                          RP4 = e.RP4,
                                                                          RP5 = e.RP5,
                                                                          role = e.role,
                                                                          startMonth = e.startMonth,
                                                                          endMonth = e.endMonth,
                                                                          grade = g.grade,
                                                                          yearRecorded = e.yearRecorded
                                                                      }).ToList();

                    List<EmployeeRoleAndAreaDTO> queryVacant = resultsDb = (from e in db.employeeRole
                                                                            join g in db.employeeGrade on e.id equals g.employeeRoleFK
                                                                            where e.role.Equals(item.role)
                                                                            && e.startMonth <= item.startMonth
                                                                            && e.endMonth >= item.startMonth
                                                                            && g.startMonth <= item.startMonth
                                                                            && g.endMonth >= item.startMonth
                                                                            && e.yearRecorded.Equals(item.yearRecorded)
                                                                            select new EmployeeRoleAndAreaDTO
                                                                            {
                                                                                adpId = e.adpId,
                                                                                RP1 = e.RP1,
                                                                                RP2 = e.RP2,
                                                                                RP3 = e.RP3,
                                                                                RP4 = e.RP4,
                                                                                RP5 = e.RP5,
                                                                                role = e.role,
                                                                                startMonth = e.startMonth,
                                                                                endMonth = e.endMonth,
                                                                                grade = g.grade,
                                                                                yearRecorded = e.yearRecorded

                                                                            }).ToList();

                    for (int i = 0; i < requiredParameters.Count(); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                query = query.Where(x => x.RP1 == item.RP1).ToList();
                                queryVacant = queryVacant.Where(x => x.RP1 == item.RP1).ToList();
                                break;
                            case 1:
                                query = query.Where(x => x.RP2 == item.RP2).ToList();
                                queryVacant = queryVacant.Where(x => x.RP2 == item.RP2).ToList();
                                break;
                            case 2:
                                query = query.Where(x => x.RP3 == item.RP3).ToList();
                                queryVacant = queryVacant.Where(x => x.RP3 == item.RP3).ToList();
                                break;
                            case 3:
                                query = query.Where(x => x.RP4 == item.RP4).ToList();
                                queryVacant = queryVacant.Where(x => x.RP4 == item.RP4).ToList();
                                break;
                            case 4:
                                query = query.Where(x => x.RP5 == item.RP5).ToList();
                                queryVacant = queryVacant.Where(x => x.RP5 == item.RP5).ToList();
                                break;
                            case 5:
                                query = query.Where(x => x.RP6 == item.RP6).ToList();
                                queryVacant = queryVacant.Where(x => x.RP6 == item.RP6).ToList();
                                break;
                            case 6:
                                query = query.Where(x => x.RP7 == item.RP7).ToList();
                                queryVacant = queryVacant.Where(x => x.RP7 == item.RP7).ToList();
                                break;
                        }
                    }

                    if (queryVacant.Count > 0)
                    {
                        foreach (EmployeeRoleAndAreaDTO itm in queryVacant)
                        {
                            if (itm.adpId.ToLower().Contains("vacant"))
                            {
                                //Delete Vacant
                                EmployeeRole getRole = (from e in db.employeeRole where e.id == itm.id select e).FirstOrDefault();
                                EmployeeGrade getGrade = (from e in db.employeeGrade where e.employeeRoleFK == getRole.id select e).FirstOrDefault();
                                // EmployeeAreaLink getarea = (from e in db.employeeAreaLink where e.employeeRoleFK == getRole.id && itm.areaCode == itm.areaCode select e).FirstOrDefault();
                                EmployeeBaseDetails getBaseDetails = (from e in db.employeeBaseDetails where e.adpId == getRole.adpId select e).FirstOrDefault();
                                db.employeeRole.Remove(getRole);
                                //db.employeeAreaLink.Remove(getarea);
                                db.employeeGrade.Remove(getGrade);
                                db.employeeBaseDetails.Remove(getBaseDetails);
                                db.SaveChanges();
                            }
                        }
                    }


                    // Insert Employee Base Details
                    empRquest.employeeBaseDetail.fullName = empRquest.employeeBaseDetail.firstName + ' ' + empRquest.employeeBaseDetail.middleName + ' ' + empRquest.employeeBaseDetail.lastName;
                    empRquest.employeeBaseDetail.toBeConsidered = true;
                    empRquest.employeeBaseDetail.doj = dateOfJoining;
                    db.employeeBaseDetails.Add(empRquest.employeeBaseDetail);
                    db.SaveChanges();


                    // Insert Employee Role Details
                    EmployeeRole empRole = new EmployeeRole();
                    empRole.adpId = empRquest.employeeBaseDetail.adpId;
                    empRole.RP1 = item.RP1;
                    empRole.RP2 = item.RP2;
                    empRole.RP3 = item.RP3;
                    empRole.RP4 = item.RP4;
                    empRole.RP5 = item.RP5;
                    empRole.RP6 = item.RP6;
                    empRole.RP7 = item.RP7;
                    empRole.endMonth = item.endMonth;
                    empRole.reportsToAdpId = item.reportsToAdpId;
                    empRole.role = item.role;
                    empRole.startMonth = item.startMonth;
                    empRole.yearRecorded = empRquest.employeeBaseDetail.yearRecorded;
                    db.employeeRole.Add(empRole);
                    db.SaveChanges();

                    // string EncryptionKey = "incentive_application";
                    string newpassword = decodePassword(empRquest.employeeBaseDetail.password);                   
                    body = "<p style='font-size: 14px; font-weight: 600; font-style: normal; line-height: 20px; letter-spacing: normal; '>Dear " + empRquest.employeeBaseDetail.firstName + ",</p>" +
                            "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>Welcome to the Numero PH Application</p><br>" +
                            "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>Your password for signing into PH application is: <b> " + newpassword + "</b>. Your username would be your Signify email ID (for example: yy.zz@signify.com).</p>" +
                            "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>You may log in to the Numero PH Application by signing <a href='https://incentiwiser.com/'>https://incentiwiser.com/</a></p><br>" +
                            "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '> <b>This is an automatically generated Email, Please do not reply</b><br /><b>  For your queries please email us at support@incentiwiser.com </ b ></p><br>" +
                            "<p style='font-size: 14px; font-weight: 600; font-style: normal; padding-top: 20px; line-height: 5px; letter-spacing: normal; color: #1f497d;'>Thanks & Regards</p>" +
                            "<p style='font-weight: 400; line-height: 0px; font-style: normal; letter-spacing: normal; color: #1f497d;'>Numero PH Application Support Team</p>";

                    string subject = "Welcome Numero PH Application";
                    string emailId = empRquest.employeeBaseDetail.email;
                    Utility.sendMaill(emailId, body, subject);
                }
            }
            var jsonsuccess = new
            {
                rsBody = "sucess"
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }

        // Create New Employee Role  Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateEmployeeRoleDetails()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO empRoleDetails = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));

            List<EmployeeRoleAndAreaDTO> empoyeeRoleData = new List<EmployeeRoleAndAreaDTO>();
            string adpId = null;
            List<CustomerDetailsDTO> retData = new List<CustomerDetailsDTO>();
            // Insert New Employee Role 
            if (empRoleDetails.employeeRole.Count != 0)
            {
                foreach (EmployeeRoleAndAreaDTO item in empRoleDetails.employeeRole)
                {
                    ExceptionResponseContainer retVal = CustomValidator.applyValidations(item, typeof(EmployeeRoleValidate));

                    if (retVal.isValid == false)
                    {
                        return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string endMonthDescription = item.endMonth.ToString();
                        string startMonthDescription = item.startMonth.ToString();
                        adpId = item.adpId;
                        EmployeeRole empRole = new EmployeeRole();
                        EmployeeGrade empgrade = new EmployeeGrade();
                        empRole.adpId = item.adpId;
                        empRole.RP1 = item.RP1;
                        empRole.RP2 = item.RP2;
                        empRole.RP3 = item.RP3;
                        empRole.RP4 = item.RP4;
                        empRole.RP5 = item.RP5;
                        empRole.RP6 = item.RP6;
                        empRole.RP7 = item.RP7;
                        empRole.endMonth = item.endMonth;
                        empRole.reportsToAdpId = item.reportsToAdpId;
                        empRole.role = item.role;
                        empRole.customCalculationStringActuals = item.customCalculationStringActuals;
                        empRole.customCalculationStringOP = item.customCalculationStringOP;
                        empRole.startMonth = item.startMonth;
                        empRole.yearRecorded = item.yearRecorded;
                        db.employeeRole.Add(empRole);
                        db.SaveChanges();
                        empgrade.currentlyAssigned = true;
                        empgrade.employeeRoleFK = empRole.id;
                        empgrade.startMonth = item.startMonth;
                        empgrade.endMonth = item.endMonth;
                        empgrade.grade = item.grade;
                        empgrade.yearRecorded = item.yearRecorded;
                        db.employeeGrade.Add(empgrade);
                        db.SaveChanges();
                    }
                }
            }

            //Get Employee Role List
            // Get Employee List

            List<EmployeeRoleAndAreaDTO> empRoleList = (from d in db.employeeRole
                                                        join gr in db.employeeGrade on d.id equals gr.employeeRoleFK
                                                        join e in db.employeeBaseDetails on d.adpId equals e.adpId
                                                        where d.adpId.Equals(adpId)
                                                        select new EmployeeRoleAndAreaDTO
                                                        {
                                                            id = d.id,
                                                            adpId = d.adpId,
                                                            role = d.role,
                                                            RP1 = d.RP1,
                                                            RP2 = d.RP2,
                                                            RP3 = d.RP3,
                                                            RP4 = d.RP4,
                                                            RP5 = d.RP5,
                                                            RP6 = d.RP6,
                                                            RP7 = d.RP7,
                                                            startMonth = d.startMonth,
                                                            endMonth = d.endMonth,
                                                            grade = gr.grade,
                                                            reportsToAdpId = d.reportsToAdpId,
                                                            branchDetailsFK = "",
                                                            doj = e.doj,
                                                            yearRecorded = d.yearRecorded

                                                        }).ToList();

            // Employee Area And Employee Branch Details
            foreach (EmployeeRoleAndAreaDTO data in empRoleList)
            {
                data.grades = (from gr in db.employeeGrade where gr.employeeRoleFK == data.id select gr).ToList();
                empoyeeRoleData.Add(data);
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    sucess = empoyeeRoleData,
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Update Employee Role 
        // Create New Employee Role  Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateEmployeeRoleDetails()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO empRoleDetails = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));

            List<EmployeeRoleAndAreaDTO> empoyeeRoleData = new List<EmployeeRoleAndAreaDTO>();
            string adpId = null;
            string errorMessage = null;
            List<CustomerDetailsDTO> retData = new List<CustomerDetailsDTO>();

            // Update Employee Exist Role End Role 
            if (empRoleDetails.responseRoleData.Count != 0)
            {

                foreach (EmployeeRoleAndAreaDTO roleData in empRoleDetails.responseRoleData)
                {
                    EmployeeRole empRole = (from e in db.employeeRole
                                            where e.id.Equals(roleData.id)
                                            select e).FirstOrDefault();
                    // int currentMonth = DateTime.Now.Month;

                    List<CustomerRelationshipDetails> custEmpLink = (from e in db.customerRelationshipDetails
                                                                     where e.employeeRoleFK.Equals(empRole.id)
                                                              && e.endMonth > roleData.endMonth
                                                              && e.yearRecorded.Equals(empRole.yearRecorded)
                                                                     select e).ToList();
                    adpId = empRole.adpId;
                    if (custEmpLink.Count == 0)
                    {
                        if (empRole != null)
                        {
                            if (roleData.endMonth != empRole.endMonth)
                            {
                                // Update Employee Role End Date
                                empRole.endMonth = roleData.endMonth;
                                db.SaveChanges();
                            }

                        }
                    }
                    else
                    {
                        if (custEmpLink.Count != 0)
                        {

                            foreach (CustomerRelationshipDetails item in custEmpLink)
                            {

                                EmployeeRole empRoles = (from d in db.employeeRole
                                                         where d.id.Equals(item.employeeRoleFK)
                                                         select d).FirstOrDefault();

                                CustomerDetailsDTO customerDetails = (from c in db.customerBaseDetails
                                                                      join r in db.customerRelationshipDetails
                                                                      on c.id equals r.customerBaseDetailsFK
                                                                      join e in db.employeeRole on r.employeeRoleFK equals e.id
                                                                      where r.id.Equals(item.id)
                                                                      && c.yearRecorded.Equals(item.yearRecorded)
                                                                      select new CustomerDetailsDTO
                                                                      {
                                                                          id = c.id,
                                                                          customerName = c.customerName,
                                                                          soldToCode = c.soldToCode,
                                                                          RP1 = e.RP1,
                                                                          RP2 = e.RP2,
                                                                          RP3 = e.RP3,
                                                                          RP4 = e.RP4,
                                                                          RP5 = e.RP5,
                                                                          RP6 = e.RP6,
                                                                          RP7 = e.RP7,
                                                                          stateCode = c.stateCode,
                                                                          districtOrTown = c.districtOrTown,
                                                                          startMonth = r.startMonth,
                                                                          endMonth = r.endMonth,
                                                                          yearRecorded = c.yearRecorded
                                                                      }).FirstOrDefault();
                                retData.Add(customerDetails);
                            }

                            List<KeyValueData> keyData = (from k in db.keyValueData
                                                          select k).ToList();

                            foreach (CustomerDetailsDTO item in retData)
                            {
                                foreach (KeyValueData keyV in keyData)
                                {
                                    if (item.endMonth.ToString() == keyV.keyValue && keyV.keyType == "MOY")
                                    {
                                        item.endMonthsDescription = keyV.description;
                                    }
                                    if (item.startMonth.ToString() == keyV.keyValue && keyV.keyType == "MOY")
                                    {
                                        item.startMonthDescription = keyV.description;
                                    }
                                    if (item.stateCode == keyV.keyValue && keyV.keyType == "STATE")
                                    {
                                        item.stateCode = keyV.description;
                                    }
                                }
                            }
                        }
                        else
                        {
                            errorMessage = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueCr");
                        }

                    }
                }
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    retData = retData
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Edit Employee Details
        //[CustomAuthorize("ADMINISTRATOR")]
        [HttpPost]
        public JsonResult EditEmployeeDetails()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetails employeeDetails = (EmployeeBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetails));
            EmployeeRequestDTO retVal = new EmployeeRequestDTO();
            retVal.employeeRole = new List<EmployeeRoleAndAreaDTO>();
            retVal.employeeBaseDetail = new EmployeeBaseDetails();

            //Get Employee Base Details
            retVal.employeeBaseDetail = (from s in db.employeeBaseDetails
                                         where (s.id.Equals(employeeDetails.id))
                                         select s).FirstOrDefault();

            // Get Employee Role Details
            List<EmployeeRoleAndAreaDTO> empRoleList = (from d in db.employeeRole
                                                        join gr in db.employeeGrade on d.id equals gr.employeeRoleFK
                                                        join e in db.employeeBaseDetails on d.adpId equals e.adpId
                                                        where d.adpId == retVal.employeeBaseDetail.adpId && gr.currentlyAssigned == true
                                                        select new EmployeeRoleAndAreaDTO
                                                        {
                                                            id = d.id,
                                                            adpId = d.adpId,
                                                            role = d.role,
                                                            RP1 = d.RP1,
                                                            RP2 = d.RP2,
                                                            RP3 = d.RP3,
                                                            RP4 = d.RP4,
                                                            RP5 = d.RP5,
                                                            RP6 = d.RP6,
                                                            RP7 = d.RP7,
                                                            startMonth = d.startMonth,
                                                            endMonth = d.endMonth,
                                                            grade = gr.grade,
                                                            reportsToAdpId = d.reportsToAdpId,
                                                            branchDetailsFK = "",
                                                            doj = e.doj,
                                                            yearRecorded = d.yearRecorded
                                                        }).ToList();


            foreach (EmployeeRoleAndAreaDTO item1 in empRoleList)
            {
                item1.requiredData = (from e in db.keyValueData
                                      where e.relatedKeyType.Equals("REQUIRED") &&
             e.keyType.Equals(item1.channelCode) && e.keyName.Equals(item1.role)
                                      select e.keyValue).ToList();


                List<KeyValueData> resulats = new List<KeyValueData>();
                string quarter = (from d in db.incentiveCalculationRequest
                                  where d.year.Equals(item1.yearRecorded)
                                  && d.status.Equals(Codes.InStatus.CLOSED.ToString())
                                  orderby d.quarter descending
                                  select d.quarter).FirstOrDefault();

                int count = 0;
                int countEnd = 0;
                if (quarter != null)
                {
                    KeyValueData keyData = (from k in db.keyValueData
                                            where k.keyType.Equals("QTR")
                                            && k.keyName.Equals(quarter)
                                            select k).FirstOrDefault();
                    int mon = Convert.ToInt32(keyData.relatedKeyValue);
                    count = mon + 1;
                    countEnd = count + 2;
                }
                else
                {
                    count = 1;
                    countEnd = 12;
                }
                item1.getMonthList = new List<KeyValueData>();
                for (int i = count; i <= countEnd; i++)
                {
                    string month = i.ToString();
                    KeyValueData monthData = (from d in db.keyValueData
                                              where (d.keyType.Equals("MOY")
                                              && d.relatedKeyType != "NONE"
                                              && d.keyValue.Equals(month))
                                              select (d)).FirstOrDefault();

                    resulats.Add(monthData);
                }
                item1.getMonthList = resulats;
                if (item1.endMonth < count || item1.endMonth > countEnd)
                {
                    string month = item1.endMonth.ToString();
                    KeyValueData monthData = (from d in db.keyValueData
                                              where (d.keyType.Equals("MOY")
                                              && d.relatedKeyType != "NONE"
                                              && d.keyValue.Equals(month))
                                              select (d)).FirstOrDefault();
                    item1.getMonthList.Add(monthData);
                }

                retVal.employeeRole.Add(item1);
            }
            var jsonsuccess = new
            {
                rsBody = retVal
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        //Update Employee Details
        // Update Customer Details
        //[CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateEmployeeBaseDetails()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetailsDTO empDetails = (EmployeeBaseDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetailsDTO));

            long dateOfActivation = 0;
            List<EmployeeRoleAndAreaDTO> empoyeeRoleData = new List<EmployeeRoleAndAreaDTO>();
            ExceptionResponseContainer retVal = CustomValidator.applyValidations(empDetails, typeof(EmployeeBaseDetailsDTOValidate));
            if (empDetails.dolStr != null)
            {
                DateTime dateFormate = DateTime.ParseExact(empDetails.dolStr, "yyyy-MM-d", null);
                dateOfActivation = dateFormate.ToUniversalTime().Ticks;
            }


            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                dateOfActivation = 0;

                EmployeeBaseDetails empBaseDetails = (from d in db.employeeBaseDetails
                                                      where d.adpId.Equals(empDetails.adpId)
                                                      && d.email.Equals(empDetails.email)
                                                      && d.id.Equals(empDetails.id)
                                                      select d).FirstOrDefault();
                if (empDetails.dolStr != null)
                {
                    DateTime dateFormate = DateTime.ParseExact(empDetails.dolStr, "yyyy-MM-d", null);
                    dateOfActivation = dateFormate.ToUniversalTime().Ticks;
                }
                if (empBaseDetails != null)
                {
                    empBaseDetails.firstName = empDetails.firstName;
                    empBaseDetails.middleName = empDetails.middleName;
                    empBaseDetails.lastName = empDetails.lastName;
                    empBaseDetails.fullName = empDetails.firstName + ' ' + empDetails.middleName + ' ' + empDetails.lastName;
                    empBaseDetails.status = empDetails.status;
                    empBaseDetails.phoneNumber = empDetails.phoneNumber;
                    if (empDetails.status == "RESIGNED")
                    {
                        empBaseDetails.dol = dateOfActivation;
                        empBaseDetails.dolStr = empDetails.dolStr;
                    }
                    db.SaveChanges();
                }

                string currentQuarter = (from k in db.keyValueData where k.keyType.Equals("OPERATIONAL") && k.keyName.Equals("CQ") select k.keyValue).FirstOrDefault();
                KeyValueData currentQuarterEndMonth = (from k in db.keyValueData where k.keyType.Equals("QTR") && k.keyName.Equals(currentQuarter) select k).FirstOrDefault();


                if (empDetails.status == "RESIGNED" && empDetails.endMonth < Convert.ToInt32(currentQuarterEndMonth.relatedKeyValue))
                {
                    SeprationEmployeeRole(empDetails, Convert.ToInt32(currentQuarterEndMonth.relatedKeyValue));
                }

                var jsonsuccess = new
                {
                    rsBody = "success"

                };
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }

        }

        public void SeprationEmployeeRole(EmployeeBaseDetailsDTO empBaseDetails, int curQtrEndMonth)
        {

            int dolMonth = empBaseDetails.endMonth;
            if (dolMonth == 12)
            {
                return;
            }

            List<EmployeeRole> employeeRoles = (from d in db.employeeRole
                                                where (d.endMonth >= dolMonth && d.adpId == empBaseDetails.adpId
                        && d.yearRecorded.Equals(empBaseDetails.yearRecorded))
                                                select d).ToList();

            foreach (EmployeeRole empRole in employeeRoles)
            {
                //string startMonthDescription = (currentMonth + 1).ToString();
                //Data insert into employeeBase details
                StringBuilder vacantAdpId = new StringBuilder();
                vacantAdpId.Append("Vacant");
                List<string> requiredParam = (from e in db.keyValueData where e.relatedKeyType.Equals("REQUIRED") && e.keyType.Equals(empRole.RP1) && e.keyName.Equals(empRole.role) select e.keyValue).ToList();

                for (int i = 0; i < requiredParam.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            vacantAdpId.Append("_" + empRole.RP1 + "_" + empRole.role);
                            break;
                        case 1:
                            vacantAdpId.Append("_" + empRole.RP2);
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP2;
                            break;
                        case 2:
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP3;
                            vacantAdpId.Append("_" + empRole.RP3);
                            break;
                        case 3:
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP4;
                            vacantAdpId.Append("_" + empRole.RP4);
                            break;
                        case 4:
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP5;
                            vacantAdpId.Append("_" + empRole.RP5);
                            break;
                        case 5:
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP6;
                            vacantAdpId.Append("_" + empRole.RP6);
                            break;
                        case 6:
                            //vacantAdpId = vacantAdpId + "_" + empRole.RP7;
                            vacantAdpId.Append("_" + empRole.RP7);
                            break;
                       
                    }
                }

                EmployeeBaseDetails vacantUser = new EmployeeBaseDetails();
                vacantUser.adpId = vacantAdpId.ToString();
                vacantUser.fullName = vacantAdpId.ToString();
                vacantUser.firstName = vacantAdpId.ToString();
                vacantUser.lastName = vacantAdpId.ToString();
                vacantUser.email = empBaseDetails.firstName + "_VACANT@gmail.com";
                vacantUser.password = "1g1FVRLDC09kWBceYTi/ttaQ8CVCmn0f";
                vacantUser.status = "ACTIVE";
                vacantUser.yearRecorded = empRole.yearRecorded;
                db.employeeBaseDetails.Add(vacantUser);
                db.SaveChanges();

                EmployeeRole vacantRole = new EmployeeRole();
                vacantRole.adpId = vacantUser.adpId;
                vacantRole.RP1 = empRole.RP1;
                vacantRole.RP2 = empRole.RP2;
                vacantRole.RP3 = empRole.RP3;
                vacantRole.RP4 = empRole.RP4;
                vacantRole.RP5 = empRole.RP5;
                vacantRole.RP6 = empRole.RP6;
                vacantRole.RP7 = empRole.RP7;
                vacantRole.endMonth = curQtrEndMonth;
                vacantRole.reportsToAdpId = empRole.reportsToAdpId;
                vacantRole.role = empRole.role;
                vacantRole.startMonth = dolMonth + 1;
                vacantRole.yearRecorded = empRole.yearRecorded;
                empRole.endMonth = dolMonth;
                db.employeeRole.Add(vacantRole);
                db.SaveChanges();

                EmployeeGrade vacantGrade = new EmployeeGrade();
                EmployeeGrade existingGrade = (from gr in db.employeeGrade where gr.currentlyAssigned == true && gr.employeeRoleFK == empRole.id select gr).FirstOrDefault();
                vacantGrade.currentlyAssigned = true;
                vacantGrade.startMonth = dolMonth + 1;
                vacantGrade.yearRecorded = empRole.yearRecorded;
                vacantGrade.grade = existingGrade.grade;
                vacantGrade.employeeRoleFK = vacantRole.id;
                vacantGrade.endMonth = curQtrEndMonth;
                if (existingGrade != null)
                {
                    existingGrade.endMonth = dolMonth;
                }
                db.employeeGrade.Add(vacantGrade);
                db.SaveChanges();

                //now diassociate the old role from the customer if any and associate the customer with a 
                //new VACANT role
                List<CustomerRelationshipDetails> customerEmployeeLinkList = (from d in db.customerRelationshipDetails
                                                                              where (
                                                                              d.employeeRoleFK == empRole.id
                                                                              &&
                                                                              d.endMonth > dolMonth)
                                                                              select d).ToList();

                //close this record and create a new one with the vacant role like this one
                foreach (CustomerRelationshipDetails item in customerEmployeeLinkList)
                {

                    //get relationship details

                    CustomerRelationshipDetails customerRelationshipDetails = (from d in db.customerRelationshipDetails
                                                                               where
                                                                               d.id == item.id
                                                                               select d).FirstOrDefault();




                    CustomerRelationshipDetails vacantCustomerRelationshipDetails = new CustomerRelationshipDetails();
                    vacantCustomerRelationshipDetails.customerBaseDetailsFK = customerRelationshipDetails.customerBaseDetailsFK;
                    vacantCustomerRelationshipDetails.startMonth = dolMonth + 1;
                    //vacantCustomerRelationshipDetails.startMonthDescription = Convert.ToString(dolMonth + 1);
                    vacantCustomerRelationshipDetails.endMonth = customerRelationshipDetails.endMonth;
                    //vacantCustomerRelationshipDetails.endMonthsDescription = Convert.ToString(customerRelationshipDetails.endMonth);
                    vacantCustomerRelationshipDetails.yearRecorded = customerRelationshipDetails.yearRecorded;
                    //vacantCustomerRelationshipDetails.subArea = customerRelationshipDetails.subArea;
                    vacantCustomerRelationshipDetails.employeeRoleFK = vacantRole.id;

                    db.customerRelationshipDetails.Add(vacantCustomerRelationshipDetails);
                    db.SaveChanges();

                    //  save End Month For Employee Branch Details
                    customerRelationshipDetails.endMonth = dolMonth;
                    //customerRelationshipDetails.endMonthsDescription = dolMonth.ToString();
                    db.SaveChanges();

                }

                empRole.endMonth = dolMonth;
                EmployeeGrade getGrade = (from e in db.employeeGrade where e.employeeRoleFK == empRole.id && e.currentlyAssigned == true select e).FirstOrDefault();
                if (getGrade != null)
                {
                    getGrade.endMonth = dolMonth;
                }
                db.SaveChanges();
            }
        }


        // Delete Employee Role
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteEmployeeRole()
        {

            Stream req = Request.InputStream;
            EmployeeRole empRole = (EmployeeRole)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRole));

            //EmployeeAreaLink empAreaLink = null;
            List<EmployeeGrade> empGrade = null;
            string errorMessage = null;
            List<CustomerDetailsDTO> retVal = new List<CustomerDetailsDTO>();
            //Delete employeeRole
            if (empRole.id != 0)
            {
                //Delete grade role
                empGrade = (from gr in db.employeeGrade where gr.employeeRoleFK == empRole.id select gr).ToList();
                if (empGrade != null)
                {
                    db.employeeGrade.RemoveRange(empGrade);
                    db.SaveChanges();
                }
            }
            EmployeeRole getEmpData = (from e in db.employeeRole where e.id == empRole.id select e).FirstOrDefault();
            db.employeeRole.Remove(getEmpData);
            db.SaveChanges();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    retVal = retVal,
                    errorMessage = errorMessage
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }



        // Next And Privious For Employee Basic Details
        //[CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NextAndPriviousEmployeeDetails()
        {
            Stream req = Request.InputStream;
            NextAndPriviousDTO nextAndPrivious = (NextAndPriviousDTO)Utility.mapRequestToClass(Request.InputStream, typeof(NextAndPriviousDTO));

            int page = 0;
            if (nextAndPrivious.pageNumber != null)
            {
                page = Convert.ToInt32(nextAndPrivious.pageNumber);
            }
            string message = null;
            int currentMonth = DateTime.Now.Month;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;
            List<EmployeeDetailsDTO> retVal = null;

            retVal = (from d in db.employeeBaseDetails
                      join e in db.employeeRole on d.adpId equals e.adpId
                      orderby d.id
                      select new EmployeeDetailsDTO
                      {
                          id = d.id,
                          fullName = d.fullName,
                          email = d.email,
                          adpId = d.adpId,
                          phoneNumber = d.phoneNumber,
                          dojStr = d.dojStr,
                          RP1 = e.RP1,
                          RP2 = e.RP2,
                          RP3 = e.RP3,
                          RP4 = e.RP4,
                          RP5 = e.RP5,
                          RP6 = e.RP6,
                          RP7 = e.RP7,
                          role = e.role,
                          status = d.status
                      }).Skip(skip).Take(pageSize).ToList();

            List<KeyValueData> keyData = (from k in db.keyValueData
                                          select k).ToList();
            if (retVal.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = retVal,
                    sucess = message
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        public static IEnumerable<EmployeeRoleAndAreaDTO> getActiveRoles(List<EmployeeRoleAndAreaDTO> empRquest)
        {
            DBModels db = new DBModels();
            IEnumerable<EmployeeRoleAndAreaDTO> getEmpRoleDetail = null;
            foreach (EmployeeRoleAndAreaDTO item in empRquest)
            {
                List<string> requiredParameter = (from e in db.keyValueData
                                                  where e.keyType == item.channelCode && e.relatedKeyType == "REQUIRED"
                                                  && e.keyName == item.role
                                                  select e.keyValue).ToList();

                getEmpRoleDetail = (from e in db.employeeRole
                                    where e.adpId != item.adpId && e.role.Equals(item.role)
                                    && ((e.startMonth <= item.startMonth || e.endMonth >= item.startMonth) || (e.endMonth >= item.endMonth && e.startMonth <= item.endMonth))
                                    select new EmployeeRoleAndAreaDTO
                                    {
                                        adpId = e.adpId,
                                        startMonth = e.startMonth,
                                        endMonth = e.endMonth,
                                        RP1 = e.RP1,
                                        RP2 = e.RP2,
                                        RP3 = e.RP3,
                                        RP4 = e.RP4,
                                        RP5 = e.RP5,
                                        RP6 = e.RP6,
                                        RP7 = e.RP7,
                                        role = e.role
                                    }).ToList();

                for (int i = 0; i < requiredParameter.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP1 == item.RP1).ToList();
                            break;
                        case 1:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP2 == item.RP2).ToList();
                            break;
                        case 2:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP3 == item.RP3).ToList();
                            break;
                        case 3:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP4 == item.RP4).ToList();
                            break;
                        case 4:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP5 == item.RP5).ToList();
                            break;
                        case 5:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP6 == item.RP6).ToList();
                            break;
                        case 6:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP7 == item.RP7).ToList();
                            break;
                        case 7:
                            getEmpRoleDetail = getEmpRoleDetail.Where(x => x.RP8 == item.RP8).ToList();
                            break;
                    }
                }
            }

            return getEmpRoleDetail;
        }




        // Craete Vacant Role For Update Employee Role End Month
        // Delete Employee Role
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateVacantRole()
        {
            Stream req = Request.InputStream;
            EmployeeRequestDTO empRoleDetails = (EmployeeRequestDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeRequestDTO));


            // Update Employee Exist Role End Role 
            if (empRoleDetails.responseRoleData.Count != 0)
            {

                foreach (EmployeeRoleAndAreaDTO roleData in empRoleDetails.responseRoleData)
                {
                    EmployeeRole empRole = (from e in db.employeeRole
                                            where e.id.Equals(roleData.id)
                                            select e).FirstOrDefault();
                    // int currentMonth = DateTime.Now.Month;

                    List<CustomerRelationshipDetails> custEmpLink = (from e in db.customerRelationshipDetails
                                                                     where e.employeeRoleFK.Equals(empRole.id)
                                                                     && e.endMonth > roleData.endMonth
                                                                     && e.yearRecorded.Equals(empRole.yearRecorded)
                                                                     select e).ToList();
                    if (empRole != null)
                    {
                        if (roleData.endMonth != empRole.endMonth)
                        {
                            VacantEmployeeRole(empRole, roleData);
                        }
                    }
                }
            }
            var jsonsuccess = new
            {
                rsBody = ""
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Employee Role Vacant Postion Create For Update Employee Endmonth 
        public void VacantEmployeeRole(EmployeeRole empRole, EmployeeRoleAndAreaDTO roleData)
        {

            if (roleData.endMonth == 12)
            {
                return;
            }

            //string startMonthDescription = (currentMonth + 1).ToString();
            EmployeeRole vacantRole = new EmployeeRole();

            //create apdId of vacant
            StringBuilder vacantAdpId = new StringBuilder();
            vacantAdpId.Append("Vacant");
            List<string> requiredParam = (from e in db.keyValueData where e.relatedKeyType.Equals("REQUIRED") && e.keyType.Equals(empRole.RP1) && e.keyName.Equals(empRole.role) select e.keyValue).ToList();
            for (int i = 0; i < requiredParam.Count(); i++)
            {
                switch (i)
                {
                    case 0:
                        vacantAdpId.Append("_" + empRole.RP1 + "_" + empRole.role);
                        break;
                    case 1:
                       // vacantAdpId = vacantAdpId + "_" + empRole.RP2;
                        vacantAdpId.Append("_" + empRole.RP2);
                        break;
                    case 2:
                        //vacantAdpId = vacantAdpId + "_" + empRole.RP3;
                        vacantAdpId.Append("_" + empRole.RP3);
                        break;
                    case 3:
                        //vacantAdpId = vacantAdpId + "_" + empRole.RP4;
                        vacantAdpId.Append("_" + empRole.RP4);
                        break;
                    case 4:
                        //vacantAdpId = vacantAdpId + "_" + empRole.RP5;
                        vacantAdpId.Append("_" + empRole.RP5);
                        break;
                    case 5:
                        //vacantAdpId = vacantAdpId + "_" + empRole.RP6;
                        vacantAdpId.Append("_" + empRole.RP6);
                        break;
                    case 6:
                        //vacantAdpId = vacantAdpId + "_" + empRole.RP7;
                        vacantAdpId.Append("_" + empRole.RP7);
                        break;                   
                }
            }

            vacantRole.adpId = vacantAdpId.ToString();
            vacantRole.RP1 = empRole.RP1;
            vacantRole.RP2 = empRole.RP2;
            vacantRole.RP3 = empRole.RP3;
            vacantRole.RP4 = empRole.RP4;
            vacantRole.RP5 = empRole.RP5;
            vacantRole.RP6 = empRole.RP6;
            vacantRole.RP7 = empRole.RP7;
            vacantRole.endMonth = empRole.endMonth;
            vacantRole.reportsToAdpId = empRole.reportsToAdpId;
            vacantRole.role = empRole.role;
            vacantRole.startMonth = roleData.endMonth + 1;
            vacantRole.yearRecorded = empRole.yearRecorded;
            db.employeeRole.Add(vacantRole);
            db.SaveChanges();

            // Update Employee Role End Date
            empRole.endMonth = roleData.endMonth;
            EmployeeGrade endGarde = (from e in db.employeeGrade where e.employeeRoleFK == empRole.id && e.currentlyAssigned == true select e).FirstOrDefault();
            if (endGarde != null)
            {
                endGarde.endMonth = roleData.endMonth;
                db.SaveChanges();
            }
            db.SaveChanges();

            //now diassociate the old role from the customer if any and associate the customer with a 
            //new VACANT role
            List<CustomerRelationshipDetails> customerEmployeeLinkList = (from d in db.customerRelationshipDetails
                                                                          where (
                                                                          d.employeeRoleFK == empRole.id
                                                                          &&
                                                                          d.endMonth > empRole.endMonth)
                                                                          select d).ToList();

            //close this record and create a new one with the vacant role like this one
            foreach (CustomerRelationshipDetails item in customerEmployeeLinkList)
            {



                //get relationship details

                CustomerRelationshipDetails customerRelationshipDetails = (from d in db.customerRelationshipDetails
                                                                           where
                                                                           d.id == item.id
                                                                           select d).FirstOrDefault();




                CustomerRelationshipDetails vacantCustomerRelationshipDetails = new CustomerRelationshipDetails();
                vacantCustomerRelationshipDetails.customerBaseDetailsFK = customerRelationshipDetails.customerBaseDetailsFK;
                vacantCustomerRelationshipDetails.startMonth = empRole.endMonth + 1;
                //vacantCustomerRelationshipDetails.startMonthDescription = Convert.ToString(empRole.endMonth + 1);
                vacantCustomerRelationshipDetails.endMonth = customerRelationshipDetails.endMonth;
                //vacantCustomerRelationshipDetails.endMonthsDescription = customerRelationshipDetails.endMonth.ToString();
                vacantCustomerRelationshipDetails.yearRecorded = customerRelationshipDetails.yearRecorded;
                vacantCustomerRelationshipDetails.employeeRoleFK = vacantRole.id;
                //vacantCustomerRelationshipDetails.subArea = customerRelationshipDetails.subArea;

                db.customerRelationshipDetails.Add(vacantCustomerRelationshipDetails);
                db.SaveChanges();

                customerRelationshipDetails.endMonth = empRole.endMonth;
                //customerRelationshipDetails.endMonthsDescription = empRole.endMonth.ToString();
                db.SaveChanges();

                item.endMonth = empRole.endMonth;
                db.SaveChanges();
            }

            EmployeeBaseDetails employeeDeatils = (from d in db.employeeBaseDetails
                                                   where d.adpId.Equals(empRole.adpId)
                                                   select d).FirstOrDefault();

            EmployeeBaseDetails emplDetail = (from d in db.employeeBaseDetails
                                              where d.adpId.Equals(vacantRole.adpId)
                                              select d).FirstOrDefault();
            if (emplDetail == null)
            {
                EmployeeBaseDetails vacantEmployee = new EmployeeBaseDetails();

                vacantEmployee.adpId = vacantRole.adpId;
                vacantEmployee.firstName = vacantRole.adpId;
                vacantEmployee.lastName = vacantRole.adpId;
                vacantEmployee.fullName = vacantRole.adpId;
                vacantEmployee.status = "ACTIVE";
                vacantEmployee.toBeConsidered = false;
                vacantEmployee.doj = employeeDeatils.doj;
                vacantEmployee.dojStr = employeeDeatils.dojStr;
                vacantEmployee.yearRecorded = employeeDeatils.yearRecorded;
                vacantEmployee.password = employeeDeatils.password;
                db.employeeBaseDetails.Add(vacantEmployee);
                db.SaveChanges();
            }
        }

        // Check Validation For Employee Basic Details
        [CustomAuthorize("ADMINISTRATOR")]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateEmployeeBasicDetails()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetails employeeBasicDetails = (EmployeeBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetails));
            DateTime dateFormat = DateTime.ParseExact(employeeBasicDetails.dojStr, "yyyy-MM-d", CultureInfo.InvariantCulture);

            long dateOfJoining = dateFormat.ToUniversalTime().Ticks;
            employeeBasicDetails.password = "abcde";
            EmployeeBaseDetails empDetails = null;
            string body = string.Empty;

            // Check Employee Deatils Validate
            ExceptionResponseContainer retVal = CustomValidator.applyValidations(employeeBasicDetails, typeof(EmployeeBaseDetailsValidate));

            if (retVal.isValid == false)
            {
                return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                empDetails = employeeBasicDetails;
            }

            var jsonsuccess = new
            {
                rsBody = empDetails
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetKeyValueDataEmpCreate()
        {
            Stream req = Request.InputStream;

            KeyValueData customerDetails = (KeyValueData)Utility.mapRequestToClass(Request.InputStream, typeof(KeyValueData));

            List<KeyValueData> resulats = new List<KeyValueData>();
            List<String> keyTypes = new List<String>();
            using (DBModels dbData = new DBModels())
            {
                object jsonsuccess = null;
                if (customerDetails.keyType != null)
                {
                    if (customerDetails.keyType == "YEAR")
                    {
                        KeyValueData cFyYear = (from y in dbData.keyValueData
                                                where y.keyType.Equals("OPERATIONAL")
                                                && y.keyName.Equals("CFY")
                                                select y).FirstOrDefault();

                        KeyValueData cAyYear = (from y in dbData.keyValueData
                                                where y.keyType.Equals("OPERATIONAL")
                                                && y.keyName.Equals("CAY")
                                                select y).FirstOrDefault();
                        if (cFyYear.keyValue == cAyYear.keyValue)
                        {
                            resulats.Add(cFyYear);
                        }
                        else
                        {
                            resulats.Add(cAyYear);
                            resulats.Add(cFyYear);

                        }
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }

                        };
                    }
                    else if (customerDetails.keyType == "QUATER")
                    {
                        resulats = (from d in dbData.keyValueData
                                    where (d.keyType.Equals("QTR")
                                    && d.relatedKeyType != "NONE"
                                    && d.keyName != "H1"
                                    && d.keyName != "H2"
                                    && d.keyName != "FY")
                                    orderby d.id ascending
                                    select (d)).ToList();

                        KeyValueData cq = (from k in db.keyValueData where k.keyType.Equals("CURRENTQUARTER") && k.keyName.Equals("CFY") select k).FirstOrDefault();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                currentQuarter = cq,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "STATE")
                    {
                        resulats = (from d in dbData.keyValueData
                                    where (d.keyType.Equals(customerDetails.keyType)
                                    )
                                    orderby d.keyValue ascending
                                    select (d)).ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };
                    }
                    else if (customerDetails.keyType == "TARGETKEY")
                    {
                        resulats = (from d in dbData.keyValueData
                                    where (d.keyType.Equals("TARGETKEY")
                                    && d.keyName == "TARGETTYPE") && d.relatedKeyType != "Incentive"
                                    orderby d.id ascending
                                    select (d)).ToList();

                        KeyValueData anotherComponenet = (from e in dbData.keyValueData where e.keyType == "TARGETDIVIDE" && e.keyName == "FORTNIGHT" select e).FirstOrDefault();

                        resulats.Add(anotherComponenet);

                        jsonsuccess = new
                        {
                            rsBody = resulats
                        };
                    }
                    else if (customerDetails.keyType == "KeyType")
                    {
                        keyTypes = (from k in dbData.keyValueData
                                    select k.keyType).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                keyTypes = keyTypes,
                                result = "success"
                            }
                        };


                    }
                    else if (customerDetails.keyType == "CHANNEL")
                    {
                        resulats = (from k in dbData.keyValueData
                                    where k.keyType.Equals("CHANNELLIST") && k.keyName.Equals(customerDetails.keyType)
                                    select k).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };


                    }

                    else if (customerDetails.keyType == "MOY")
                    {
                        resulats = (from k in dbData.keyValueData
                                    where k.keyType.Equals(customerDetails.keyType)
                                    select k).Distinct().ToList();
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                resulats = resulats,
                                result = "success"
                            }
                        };


                    }

                    else if (customerDetails.keyType == "TARGETTYPE")
                    {
                        List<KeyValueData> keyTypeData = (from k in dbData.keyValueData
                                                          where k.keyType.Equals(customerDetails.keyType)
                                && k.keyName.Equals(customerDetails.keyType)
                                                          select k).Distinct().ToList();
                    }
                    else
                    {
                        if (customerDetails.keyName == "CHANNEL")
                        {
                            resulats = (from d in dbData.keyValueData
                                        where (d.keyType.Equals(customerDetails.keyType)
                                        && d.keyName != "ALL"
                                        && d.keyName != "CFY")
                                        orderby d.id ascending
                                        select (d)).ToList();

                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    resulats = resulats,
                                    result = "success"
                                }
                            };
                        }
                        else
                        {
                            resulats = (from d in dbData.keyValueData
                                        where d.keyType.Equals(customerDetails.keyType)
                                        select d).Distinct().ToList();

                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    resulats = resulats,
                                    result = "success"
                                }
                            };
                        }

                    }
                }
                return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all required parameter for user.
        public JsonResult GetRelatedParam()
        {
            Stream req = Request.InputStream;
            List<string> result = new List<string>();
            RequiredValue getRequiredValue = new RequiredValue();
            List<EmployeeRoleAndAreaDTO> employeeRoleData = (List<EmployeeRoleAndAreaDTO>)Utility.mapRequestToClass(Request.InputStream, typeof(List<EmployeeRoleAndAreaDTO>));
            string type = "";
            foreach (EmployeeRoleAndAreaDTO itm in employeeRoleData)
            {

                List<string> requiredParam = (from e in db.keyValueData
                                              where e.keyType == itm.channelCode && e.keyName == itm.role
                    && e.relatedKeyType == "REQUIRED"
                                              select e.keyValue).ToList();

                result.AddRange((from d in db.keyValueData
                                 where (d.keyType.Equals(itm.channelCode)
                                 && d.keyName.Equals(itm.role)
                                 && d.relatedKeyType == "REQUIRED")
                                 orderby d.id ascending
                                 select (d.keyValue)).ToList());

                KeyValueData getData = (from e in db.keyValueData where e.keyType == "CHANNELLIST" && e.keyName == "CHANNEL" && e.keyValue == itm.channelCode select e).FirstOrDefault();

                if (itm.role != "" || itm.role != null)//&&
                {
                    getRequiredValue.subChannelList = (from e in db.employeeAttributeLink
                                                       where e.channelFk == getData.id && e.keyName == "SUBCHANNELREQUIRED"
                    && ((e.startMonth <= itm.startMonth && e.endMonth >= itm.startMonth) ||
                    (e.startMonth >= itm.startMonth && e.startMonth <= itm.endMonth))
                                                       select e.keyValue).ToList();
                }
                type = itm.type;
                switch (itm.type)
                {
                    case "ROLE":
                        result.AddRange((from d in db.keyValueData
                                         where (d.keyType.Equals(itm.channelCode)
                                         && d.keyName.Equals("ROLE")
                                         && d.relatedKeyType != null)
                                         orderby d.id ascending
                                         select (d.keyValue)).ToList());
                        break;

                    case "SUBCHANNELREQUIRED":
                        KeyValueData getNextRequired = (from e in db.keyValueData
                                                        where e.keyValue == "SUBCHANNELREQUIRED" && e.keyType == itm.channelCode
                                                        && e.keyName.Equals(itm.role)
                                                        select e).FirstOrDefault();
                        string required = (from e in db.keyValueData
                                           where e.relatedKeyType == "REQUIRED" && e.keyType == itm.channelCode
                                              && e.keyName.Equals(itm.role) && e.preferenceSerial == getNextRequired.preferenceSerial + 1
                                           select e.keyValue).FirstOrDefault();

                        List<string> getDataRequire = (from e in db.employeeAttributeLink
                                                       where e.channelFk == getData.id && e.relatedKeyName2 == "SUBCHANNELREQUIRED" &&
                                                        ((e.startMonth <= itm.startMonth && e.endMonth >= itm.startMonth) ||
                                                       (e.startMonth >= itm.startMonth && e.startMonth <= itm.endMonth))
                                                        && e.relatedKeyValue2 == itm.subChannelCode && e.keyName == required
                                                       select e.keyValue).ToList();
                        getRequiredValue.areaList = new List<string>();
                        getRequiredValue.regionList = new List<string>();
                        getRequiredValue.subAreaList = new List<string>();
                        if (required == "REGIONREQUIRED")
                        {
                            getRequiredValue.regionList = getDataRequire;
                        }
                        else
                        {
                            getRequiredValue.areaList = getDataRequire;
                        }
                        break;

                    case "REGIONREQUIRED":
                        getNextRequired = (from e in db.keyValueData
                                           where e.keyValue == "REGIONREQUIRED" && e.keyType == itm.channelCode
                                           && e.keyName.Equals(itm.role)
                                           select e).FirstOrDefault();
                        required = (from e in db.keyValueData
                                    where e.relatedKeyType == "REQUIRED" && e.keyType == itm.channelCode
                                       && e.keyName.Equals(itm.role) && e.preferenceSerial == getNextRequired.preferenceSerial + 1
                                    select e.keyValue).FirstOrDefault();

                        getRequiredValue.areaList = new List<string>();
                        getRequiredValue.subAreaList = new List<string>();
                        if (requiredParam.Contains("SUBCHANNELREQUIRED"))
                        {
                            getRequiredValue.areaList = (from e in db.employeeAttributeLink
                                                         where e.channelFk == getData.id && e.relatedKeyName2 == "REGIONREQUIRED"
                                                         && e.relatedKeyName1 == "SUBCHANNELREQUIRED" && e.relatedKeyValue1 == itm.subChannelCode
                                                         && ((e.startMonth <= itm.startMonth && e.endMonth >= itm.startMonth) ||
                                                       (e.startMonth >= itm.startMonth && e.startMonth <= itm.endMonth))
                                                         && e.relatedKeyValue2 == itm.region
                                                         && e.keyName == required
                                                         select e.keyValue).ToList();
                        }
                        else
                        {
                            getRequiredValue.areaList = (from e in db.employeeAttributeLink
                                                         where e.channelFk == getData.id && e.relatedKeyName2 == "REGIONREQUIRED"
                                                         && e.relatedKeyValue2 == itm.region
                                                         && ((e.startMonth <= itm.startMonth && e.endMonth >= itm.startMonth) ||
                                                       (e.startMonth >= itm.startMonth && e.startMonth <= itm.endMonth))
                                                         && e.keyName == required
                                                         select e.keyValue).ToList();
                        }
                        break;

                    case "AREACODEREQUIRED":
                        getNextRequired = (from e in db.keyValueData
                                           where e.keyValue == "AREACODEREQUIRED" && e.keyType == itm.channelCode
                                           && e.keyName.Equals(itm.role)
                                           select e).FirstOrDefault();
                        required = (from e in db.keyValueData
                                    where e.relatedKeyType == "REQUIRED" && e.keyType == itm.channelCode
                                       && e.keyName.Equals(itm.role) && e.preferenceSerial == getNextRequired.preferenceSerial + 1
                                    select e.keyValue).FirstOrDefault();

                        getRequiredValue.subAreaList = (from e in db.employeeAttributeLink
                                                        where e.channelFk == getData.id && e.relatedKeyName2 == "AREACODEREQUIRED"
                                                        && e.relatedKeyValue2 == itm.area
                                                        && ((e.startMonth <= itm.startMonth && e.endMonth >= itm.startMonth) ||
                                                        (e.startMonth >= itm.startMonth && e.startMonth <= itm.endMonth))
                                                        && e.keyName == required
                                                        select e.keyValue).ToList();
                        break;


                }
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    type = type,
                    result = result,
                    paramValue = getRequiredValue
                }
            };

            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [UserAuthenticationFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetEmployeeDetailsList()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetails employeeBaseDetails = (EmployeeBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetails));

            int page = 0;
            string message = null;

            List<EmployeeBaseDetails> resulats = null;
            var pageSize = 20; // set your page size, which is number of records per page
            var skip = pageSize * page;
            int currentMonth = DateTime.Now.Month;
            if (employeeBaseDetails.fullName != null)
            {
                resulats = (from d in db.employeeBaseDetails
                            where (d.fullName.ToLower().Contains(employeeBaseDetails.fullName)
                            )
                            orderby d.id descending
                            select d).Skip(skip).Take(pageSize).Distinct().ToList();
                if (resulats.Count == 0)
                {
                    resulats = (from d in db.employeeBaseDetails
                                where (d.adpId.ToLower().Contains(employeeBaseDetails.fullName)
                                )
                                orderby d.id descending
                                select d).Skip(skip).Take(pageSize).ToList();
                }
                if (resulats.Count == 0)
                {
                    resulats = (from d in db.employeeBaseDetails
                                where (d.status.ToLower().Contains(employeeBaseDetails.fullName)
                                )
                                orderby d.id descending
                                select d).Skip(skip).Take(pageSize).ToList();
                }
            }
            else
            {
                resulats = (from d in db.employeeBaseDetails
                            orderby d.id
                            select d).Skip(skip).Take(pageSize).ToList();
            }

            if (resulats.Count == 0)
            {
                message = ExceptionMessage.getExceptionMessage("ErrorMessage.UniqueNoData");
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    resulats = resulats,
                    sucess = message,
                    result = "success"
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

    }
}