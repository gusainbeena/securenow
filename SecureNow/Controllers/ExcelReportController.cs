﻿using Newtonsoft.Json;
using SecureNow.Container;
using PHIncentiveApp.CustomFilters;
using SecureNow.Models;
using SecureNow.utilities;
using SecureNow.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SecureNow.Models;

namespace PHIncentiveApp.Controllers
{
    //[CustomAuthorize("admin")]
    public class ExcelReportController : Controller
    {
        DBModels db = new DBModels();
        public static List<string> typeOfData = new List<string>();
        public ActionResult Index()
        {
            //code for function index
            //check if there is a department name in the request parameter
            //if not and the system role of the employee is Management 
            //default the department as Sales
            //populate the model with the path to the cshtml for the department
            string scriptPathName = "~/Views/AdminDepartment/Script.cshtml";
            //return ActionResult(model);
            //Get Current Employee Logdin 

            ViewBag.departmentScriptFilepath = scriptPathName;
            return View();
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCurrentLoginUserDetails()
        {
            string userName = HttpContext.User.Identity.Name;

            string results = (from d in db.employeeBaseDetails
                              where d.email.Equals(userName)
                              select d.fullName).FirstOrDefault();

            var jsonsuccess = new
            {
                rsBody = results
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateReportRequest(DataUploadBatchRequests dataUploadBatchRequests)
        {
            Stream req = Request.InputStream;
            EmployeeMasterUploadDTO reportDetails = (EmployeeMasterUploadDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeMasterUploadDTO));

            //get the userId from the session
            string userName = HttpContext.User.Identity.Name;
            string requestorAdpId = (from d in db.employeeBaseDetails where d.email.Equals(userName) select d.adpId).FirstOrDefault();
            dataUploadBatchRequests.requestType = reportDetails.requestType;
            dataUploadBatchRequests.requestorAdpId = requestorAdpId;
            dataUploadBatchRequests.downloadReportType = reportDetails.downloadReportType;
            dataUploadBatchRequests.status = Codes.status.QUEUED.ToString();
            dataUploadBatchRequests.timeOfRequest = DateTime.Now.Ticks;
            dataUploadBatchRequests.startMonth = 0;
            dataUploadBatchRequests.endMonth = 0;
            dataUploadBatchRequests.year = 2016;
            dataUploadBatchRequests.quarter = "Q4";
            dataUploadBatchRequests.fullUpload = false;
            dataUploadBatchRequests.recordsProcessed = 0;
            dataUploadBatchRequests.totalRecords = 0;

            db.dataUploadBatchRequests.Add(dataUploadBatchRequests);
            db.SaveChanges();
            int id = dataUploadBatchRequests.id;
            var jsonsuccess = new
            {
                rsBody = new
                {
                    RequestId = id
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckReportRequest()
        {
            Stream req = Request.InputStream;
            EmployeeMasterUploadDTO reportDetails = (EmployeeMasterUploadDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeMasterUploadDTO));
            string filename = null;
            int id = reportDetails.requestId;
            DataUploadBatchRequests checkReportStatus = (from rp in db.dataUploadBatchRequests
                                                         where (rp.id == id)
                                                         select
                                                        rp).FirstOrDefault();

            if (checkReportStatus.status == Codes.status.SUCCESS.ToString())
            {
                filename = checkReportStatus.dataSourceFileName;
            }
            else
            {
                filename = "";
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    filename = filename
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckFileExistOrNot()
        {
            string filePath = Request.QueryString["name"];
            if (filePath == null || filePath == "")
            {
                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string json = new StreamReader(req).ReadToEnd();
                dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
                filePath = jsonDeserialize.rqBody;
            }
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.DownloadReports.ToString()
                                                   select p).FirstOrDefault();


            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            if (System.IO.File.Exists(fileName))
            {

                var jsonsuccessss = new
                {
                    rsBody = new
                    {
                        Exist = "Exist",
                        result = "success"
                    }

                };
                return Json(jsonsuccessss, JsonRequestBehavior.AllowGet);

            }
            else
            {
                parameter = (from p in db.dataUploadBatchParameters
                             where p.uploadType == Codes.downloadType.PDFPath.ToString()
                             select p).FirstOrDefault();


                fileName = Path.Combine(parameter.fileUploadFolder, "payslipPDF/" + filePath);

                if (System.IO.File.Exists(fileName))
                {

                    var jsonsuccessss = new
                    {
                        rsBody = new
                        {
                            Exist = "Exist",
                            result = "success"
                        }

                    };
                    return Json(jsonsuccessss, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    var jsonsuccesss = new
                    {
                        rsBody = new
                        {
                            Exist = "NotExist",
                            result = "success"
                        }

                    };
                    return Json(jsonsuccesss, JsonRequestBehavior.AllowGet);
                }
            }


        }
        public void pptexport()
        {
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.PDFPath.ToString()
                                                   select p).FirstOrDefault();
            var fileName = Path.Combine(parameter.fileUploadFolder, filePath);
            FileStream MyFileStream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
            long FileSize = MyFileStream.Length;
            Byte[] buffer = new Byte[FileSize];


            MyFileStream.Read(buffer, 0, Convert.ToInt32(FileSize));
            MyFileStream.Close();

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.ContentType = "application/vnd.ms-powerpoint";
            Response.Buffer = true;
            Response.OutputStream.Write(buffer, 0, Convert.ToInt32(FileSize));
            Response.End();
        }
        public void ExportExcel()
        {
            /*
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.DownloadReports.ToString()
                                                   select p).FirstOrDefault();


            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            FileStream MyFileStream = new FileStream(fileName, FileMode.Open);
            long FileSize = MyFileStream.Length;
            Byte[] buffer = new Byte[FileSize];


            MyFileStream.Read(buffer, 0, Convert.ToInt32(FileSize));
            MyFileStream.Close();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + filePath);
            Response.OutputStream.Write(buffer, 0, Convert.ToInt32(FileSize));
            Response.Flush();
            Response.Close();*/
            string filePath = Request.QueryString["path"];
            string filename = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.DownloadReports.ToString()
                                                   select p).FirstOrDefault();


            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);
            //fileName = Path.Combine(filePath,filename);
            //fileName = "Template_OP_30-Jan-19.xlsx";
            FileStream MyFileStream = new FileStream(fileName, FileMode.Open);
            long FileSize = MyFileStream.Length;
            Byte[] buffer = new Byte[FileSize];


            MyFileStream.Read(buffer, 0, Convert.ToInt32(FileSize));
            MyFileStream.Close();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.OutputStream.Write(buffer, 0, Convert.ToInt32(FileSize));
            Response.Flush();
            Response.Close();

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetYearList()
        {
            List<KeyValueData> yearList = new List<KeyValueData>();
            //yearList = Utility.GetYearList();
            string userName = HttpContext.User.Identity.Name;
            string adpId = (from e in db.employeeBaseDetails where e.email == userName select e.adpId).FirstOrDefault();
            List<KeyValueData> key = (from d in db.keyValueData where d.keyType.Equals("OPERATIONAL") && (d.keyName.Equals("CFY")) select d).ToList();

            string role = (from e in db.employeeRole where e.adpId == adpId select e.role).FirstOrDefault();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    key = key,
                    adpId = adpId,
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PptDownload()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);

            string channel = jsonDeserialize.rqBody.channel;
            int year = jsonDeserialize.rqBody.year;
            string filePath = "";
            filePath = (from fileP in db.incentivePolicy
                        where (fileP.channel.Equals(channel) && fileP.year == year)
                        select (fileP.policyName)).FirstOrDefault();

            string type = (from fileP in db.incentivePolicy
                           where (fileP.channel.Equals(channel) && fileP.year == year)
                           select (fileP.type)).FirstOrDefault();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    fileName = filePath,
                    type = type,
                    result="success"
                }

            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);


        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetYearAndChannelList()
        {
            List<KeyValueData> yearList = new List<KeyValueData>();
            //yearList = Utility.GetYearList();
            int month = DateTime.Now.Month;
            List<KeyValueData> key = (from d in db.keyValueData where d.keyType.Equals("OPERATIONAL") && (d.keyName.Equals("CFY")) select d).ToList();
            List<string> quarter = (from d in db.keyValueData where d.keyType.Equals("QTR") && (!d.keyName.Equals("H1") && !d.keyName.Equals("H2") && !d.keyName.Equals("FY")) select d.keyValue).ToList();
            string name = HttpContext.User.Identity.Name;
            List<string> getChannelList = (from e in db.employeeRole
                                           join f in db.employeeBaseDetails on e.adpId equals f.adpId
                                           where f.email == name
                                           select e.RP1).Distinct().ToList();
            string userName = HttpContext.User.Identity.Name;
            string adpId = (from e in db.employeeBaseDetails where e.email == userName select e.adpId).FirstOrDefault();

            var jsonsuccess = new
            {
                rsBody = new
                {
                    yearList = key,
                    channel = getChannelList,
                    adpId = adpId,
                    quarter = quarter,
                    result="success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public void downloadPDF()
        {
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.PDFPath.ToString()
                                                   select p).FirstOrDefault();


            string path = parameter.fileUploadFolder;
            string fileName = Path.Combine(path, filePath);

            //string FilePath = Server.MapPath("javascript1-sample.pdf");
            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();

        }
        public void AOPVsActual()
        {
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.PDFPath.ToString()
                                                   select p).FirstOrDefault();


            string path = parameter.fileUploadFolder;
            //string fileName = Path.Combine(path, filePath);
            string fileName = "D:\\securenow\\Password_SecureNow_2019.xlsx";
            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();

        }
        public void downloadPDFPaySlip()
        {
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.PDFPath.ToString()
                                                   select p).FirstOrDefault();


            string path = parameter.fileUploadFolder;
            string fileName = Path.Combine(path, "payslipPDF/" + filePath);

            //string FilePath = Server.MapPath("javascript1-sample.pdf");
            WebClient User = new WebClient();

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);
            Response.TransmitFile(fileName);
            Response.End();

        }

        //Performance Sheet
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getPerformanceSheet(DataUploadBatchRequests dataUploadBatchRequests)
        {

            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            string year = jsonDeserialize.rqBody.year;
            int yearData = Convert.ToInt32(year);
            var month = jsonDeserialize.rqBody.month;
            string userName = HttpContext.User.Identity.Name;
            
            string empAdpId = (from d in db.employeeBaseDetails
                               where d.email.Equals(userName)
                               select d.adpId).FirstOrDefault();
            List<int> getMonth = new List<int>();
            foreach (var i in month)
            {
                int monthData = Convert.ToInt32(i);
                getMonth.Add(monthData);
            }

            int maxmonth = getMonth.Max();
            int minMonth = getMonth.Min();
            string fileName = "";
            string genFilePath = "";
            fileName = DateTime.Now.Ticks + "_" + year + ".xlsx";
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.DownloadReports.ToString()
                                                   select p).FirstOrDefault();



            List<string> getEmpData = new List<string>();
            //Get EmployeeRole
            List<EmployeeRole> role = (from e in db.employeeRole
                                 where e.adpId == empAdpId
                                 select e).Distinct().ToList();
           

            //Write header
            string path = parameter.fileUploadFolder;
            genFilePath = Path.Combine(path, fileName);

            FileStream fs = new FileStream(genFilePath, FileMode.OpenOrCreate, FileAccess.Write);
            fs.Close();
            int count = 1;
            var workBook = new XLWorkbook();
            var workSheet = workBook.Worksheets.Add("Sheet1");
            workSheet.SetShowGridLines(false);
            //create the header buffer
            workSheet.Cell(1, count).Value = "ADP ID";
            count++;
            workSheet.Cell(1, count).Value = "EMP NAME";
            count++;
            workSheet.Cell(1, count).Value = "ROLE START MONTH";
            count++;
            workSheet.Cell(1, count).Value = "ROLE END MONTH";
            count++;
            workSheet.Cell(1, count).Value = "ROLE";
            count++;
            workSheet.Cell(1, count).Value = "CHANNEL";
            count++;
            workSheet.Cell(1, count).Value = "ZONE";
            count++;
            workSheet.Cell(1, count).Value = "AREA";
            count++;
            workSheet.Cell(1, count).Value = "TM CODE";
            count++;
            workSheet.Cell(1, count).Value = "RECORDED YEAR";
            count++;
            workSheet.Cell(1, count).Value = "ACTUAL";
            count++;
            workSheet.Cell(1, count).Value = "TARGET";
            count++;
            workSheet.Cell(1, count).Value = "ACHIEVEMENT";

            workSheet.Columns(1, count).Width = 16;
            setCellBorder((workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(1, count))));
            var row = 1;


            //get user requiredParam
            foreach (EmployeeRole reqParam in role)
            {
                KeyValueData getFK = (from e in db.keyValueData where e.keyType == "TeamSetup" && e.keyName == "MetaData" && e.keyValue == reqParam.RP1 select e).FirstOrDefault();

                int getRoleSequence = (from e in db.keyValueDataLink
                                       where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                       e.keyValue == reqParam.role
                                       select e.attrSequence).FirstOrDefault();

                List<string> getNewRole = (from e in db.keyValueDataLink where e.keyValueFK == getFK.id && e.keyName == "Role" && e.attrSequence > getRoleSequence orderby e.attrSequence select e.keyValue).Distinct().ToList();
                List<string> roleRequiredParam = new List<string>();
                if (getNewRole.Count() == 0)
                {
                    getNewRole = (from e in db.keyValueDataLink where e.keyValueFK == getFK.id && e.keyName == "Role" && e.attrSequence == getRoleSequence select e.keyValue).Distinct().ToList();
                    roleRequiredParam = (from e in db.keyValueDataLink
                                         where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                          e.attrSequence == getRoleSequence
                                         select e.requiredSalesHorizontals).Distinct().ToList();
                }

                List<string> selfRequiredParam = (from e in db.keyValueDataLink
                                                  where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                                   e.attrSequence == getRoleSequence
                                                  select e.requiredSalesHorizontals).Distinct().ToList();
                List<string> RP1 = new List<string>();
                KeyValueData managementRole = (from e in db.keyValueData where e.keyType == reqParam.RP1 && e.keyName == "Channel" select e).FirstOrDefault();
                if (managementRole != null)
                {
                    RP1 = (from e in db.keyValueData where e.keyType == "Channel" select e.keyValue).Distinct().ToList();
                }
                else
                {
                    RP1.Add(reqParam.RP1);
                }

                List<MyDashboard> tabularList = new List<MyDashboard>();
                int countData = getRoleSequence;
                foreach (string itmRole in getNewRole)
                {
                    countData = countData + 1;
                    roleRequiredParam = (from e in db.keyValueDataLink
                                                      where e.keyValueFK == getFK.id && e.keyName == "Role" &&
                                                       e.attrSequence == countData
                                                      select e.requiredSalesHorizontals).Distinct().ToList();

                    IEnumerable<EmployeeRole> EmpList = from p in db.employeeRole where p.role == itmRole select p;
                    for (int i = 0; i < selfRequiredParam.Count(); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                EmpList = EmpList.Where(x => RP1.Contains(x.RP1));
                                break;
                            case 1:
                                EmpList = EmpList.Where(x => x.RP2 == reqParam.RP2);
                                break;
                            case 2:
                                EmpList = EmpList.Where(x => x.RP3 == reqParam.RP3);
                                break;
                            case 3:
                                EmpList = EmpList.Where(x => x.RP4 == reqParam.RP4);
                                break;
                            case 4:
                                EmpList = EmpList.Where(x => x.RP5 == reqParam.RP5);
                                break;
                        }
                    }
                   
                    foreach (EmployeeRole itmData in EmpList)
                    {
                        row++;
                        count = 1;
                        //get name of the person who initiated it
                        workSheet.Cell(row, count).Value = itmData.adpId;
                        count++;
                        string name = (from e in db.employeeBaseDetails where e.adpId == itmData.adpId select e.fullName).FirstOrDefault();
                        workSheet.Cell(row, count).Value = name;
                        count++;
                        if (itmData.startMonth <= minMonth)
                        {
                            workSheet.Cell(row, count).Value = minMonth;
                        }
                        else
                        {
                            workSheet.Cell(row, count).Value = itmData.startMonth;
                        }
                        count++;
                        int gradeEndMonth = 0;
                        if (itmData.endMonth >= maxmonth)
                        {
                            workSheet.Cell(row, count).Value = maxmonth;
                            gradeEndMonth = maxmonth;
                        }
                        else
                        {
                            workSheet.Cell(row, count).Value = itmData.endMonth;
                            gradeEndMonth = itmData.endMonth;
                        }

                       
                        workSheet.Cell(row, count).Value = itmData.role;
                        count++;
                        workSheet.Cell(row, count).Value = itmData.RP1;
                        count++;
                        workSheet.Cell(row, count).Value = itmData.RP2;
                        count++;
                        workSheet.Cell(row, count).Value = itmData.RP3;
                        count++;
                        workSheet.Cell(row, count).Value = itmData.RP4;
                        count++;

                        var achievmentData = from e in db.EmployeeActualAndTargets select e;
                        for (int i = 0; i < roleRequiredParam.Count(); i++)
                        {
                            switch (i)
                            {
                                case 0:
                                    achievmentData = achievmentData.Where(x => x.RP1 == itmData.RP1);
                                    break;
                                case 1:
                                    achievmentData = achievmentData.Where(x => x.RP2 == itmData.RP2);
                                    break;
                                case 2:
                                    achievmentData = achievmentData.Where(x => x.RP3 == itmData.RP3);
                                    break;
                                case 3:
                                    achievmentData = achievmentData.Where(x => x.RP4 == itmData.RP4);
                                    break;
                                case 4:
                                    achievmentData = achievmentData.Where(x => x.RP5 == itmData.RP5);
                                    break;
                            }
                        }

                        decimal target = 0;
                        decimal actual = 0;
                        foreach (var perf in achievmentData)
                        {
                            target = target + perf.targetAmt;
                            actual = actual + perf.netActualAmt;
                        }

                        workSheet.Cell(row, count).Value = Math.Round(target,2).ToString();
                        count++;
                        workSheet.Cell(row, count).Value = Math.Round(actual, 2).ToString();
                        count++;
                        if (target > 0)
                        {
                            workSheet.Cell(row, count).Value = Math.Round((actual / target) * 100).ToString() + "%";
                        }
                        else
                        {
                            workSheet.Cell(row, count).Value = "0.00%";
                        }
                    }
                }
            }
            workSheet.Columns(1, count).Width = 16;
            setCellBorder((workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(row, count))));
            if (genFilePath != null)
            {
                System.IO.File.Delete(genFilePath);
            }
            workBook.SaveAs(genFilePath);

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    fileName = fileName
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DownLoadReport()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = jsonDeserialize.rqBody;
            string adpId = data.adpId;
            string region = data.region;
            string channel = data.channelCode;
            string role = data.role;
            int userBranch = data.branchDetailsFK;
            string currentQuarter = data.quarter;
            string NameForPayslip = "";
            if (role == "RM" || role == "Regional Sales Manager")
            {
                NameForPayslip = region;
            }
            else if (role == "CH")
            {
                NameForPayslip = channel;
            }
            else if (role == "NKAM")
            {
                NameForPayslip = channel;
            }
            else
            {
                NameForPayslip = channel;
            }
            IncentiveCalculationRequest getRequest = (from e in db.incentiveCalculationRequest where e.status != "DISBURSE" && e.quarter == currentQuarter select e).FirstOrDefault();
            string filepath = adpId + "_" + NameForPayslip + "_" + getRequest.quarter + "_" + getRequest.year + ".pdf";

            var jsonsuccess = new
            {
                rsBody = new
                {
                    filepath = filepath,
                    result="success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PaySlipSend()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            var data = jsonDeserialize.rqBody;
            string NameForPayslip = "";
            Boolean flagpartialMail = false;
            string bmChadpId = "";
            string bmChRole = "";
            foreach (var itm in data)
            {
                string region = itm.region;
                string channel = itm.channelCode;

                int userBranch = itm.branchDetailsFK;
                string role = itm.role;
                Boolean flag = itm.Selected;
                string adpId = itm.adpId;

                IncentiveCalculationRequest getRequest = (from e in db.incentiveCalculationRequest where e.status != "DISBURSE" select e).FirstOrDefault();
              
                string emailRMCH = "";
                if (flag == true)
                {

                    IncentiveCalculationRequest getIncentiveQuarter = (from e in db.incentiveCalculationRequest where e.status.Equals(Codes.status.APPROVED.ToString()) select e).FirstOrDefault();


                    if (role == "RM" || role == "Regional Sales Manager")
                    {
                        NameForPayslip = region;
                    }
                    else if (role == "CH")
                    {
                        NameForPayslip = channel;
                    }
                    else if (role == "NKAM")
                    {
                        NameForPayslip = channel;
                    }
                    else
                    {
                        if (userBranch == 0)
                        {
                            NameForPayslip = channel;
                            emailRMCH = (from e in db.employeeRole
                                         join c in db.employeeBaseDetails on e.adpId equals c.adpId
                                         where e.role == "CH" && e.RP1 == channel && c.status == "Active"
                                         && e.yearRecorded == getRequest.year
                                         select c.email).FirstOrDefault();
                        }
                      
                    }

                    string emailId = (from e in db.employeeBaseDetails where e.adpId == adpId select e.email).FirstOrDefault();

                    DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                           where p.uploadType == Codes.downloadType.PDFPath.ToString()
                                                           select p).FirstOrDefault();


                    string path = parameter.fileUploadFolder;
                    string filepath = Path.Combine(path, "payslipPDF/" + adpId + "_" + NameForPayslip + "_" + getRequest.quarter + "_" + getRequest.year + ".pdf");
                    if (System.IO.File.Exists(filepath))
                    {

                        try
                        {
                            if (emailId != "")
                            {
                                string body = string.Empty;
                                using (StreamReader readers = new StreamReader(path + "Templates/PaySlipGenerated.cshtml"))
                                {
                                    body = readers.ReadToEnd();
                                }
                                string empName = (from e in db.employeeBaseDetails where e.adpId == adpId select e.fullName).FirstOrDefault();
                                //string adp_id = (from db.employee)
                                body = body.Replace("{employeeName}", empName);
                                body = body.Replace("{quarter}", getRequest.quarter);
                                body = body.Replace("{year}", getRequest.year.ToString());
                                string subject = "Incentive working for " + getRequest.quarter + ", " + getRequest.year.ToString();
                                //Utility.sendPayslipMaill(emailId, body, subject, filepath, emailBM, emailRMCH);
                                Utility.sendPayslipMaill("mmjadon@knowledgeops.co.in", body, subject, filepath, "mmjadon@knowledgeops.co.in", "mmjadon@knowledgeops.co.in");
                            }
                        }
                        catch
                        {

                        }
                    }


                    if (role == "RM" || role == "Regional Sales Manager" || role == "NKAM" || role == "CH")
                    {
                        IncentiveConfirmations getDataIncentive = (from e in db.incentiveConfirmations where e.adpId == adpId select e).FirstOrDefault();
                        if (getDataIncentive != null && (role == "RM" || role == "Regional Sales Manager"))
                        {
                            getDataIncentive.status = "DISBURSE";
                            db.SaveChanges();
                        }

                        bmChadpId = adpId;
                        bmChRole = role;
                    }

                    DisbursmentData getDetail = (from e in db.disbursmentdata
                                                 where e.adpId == adpId && e.yearRecorded == getRequest.year
                    && e.quarter == getRequest.quarter && e.role == role
                                                 select e).FirstOrDefault();
                    if (getDetail != null)
                    {
                        getDetail.finalStatus = "DISBURSE";
                        db.SaveChanges();
                    }
                }
                else
                {
                    DisbursmentData getDetail = (from e in db.disbursmentdata
                                                 where e.adpId == adpId && e.yearRecorded == getRequest.year
                   && e.quarter == getRequest.quarter && e.role == role && e.finalStatus != "DISBURSE"
                                                 select e).FirstOrDefault();
                    if (getDetail == null)
                    {
                        flagpartialMail = true;

                    }
                }
            }

            IncentiveConfirmations getData = (from e in db.incentiveConfirmations where e.adpId == bmChadpId select e).FirstOrDefault();
            if (getData != null)
            {
                if (flagpartialMail == true)
                {
                    getData.status = "PARTIAL_DISBURSE";
                    db.SaveChanges();
                }
                else
                {
                    getData.status = "DISBURSE";
                    db.SaveChanges();
                }
            }


            var jsonsuccess = new
            {
                rsBody = "success"
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);

        }
        private static void setCellBorder(IXLRange cellReference)
        {
            cellReference.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            cellReference.Style.Border.BottomBorderColor = XLColor.LightGray;
            cellReference.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            cellReference.Style.Border.TopBorderColor = XLColor.LightGray;
            cellReference.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            cellReference.Style.Border.LeftBorderColor = XLColor.LightGray;
            cellReference.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            cellReference.Style.Border.RightBorderColor = XLColor.LightGray;
        }
        private static void setBackGroundColor(IXLCell cellReference, string colorClass)
        {
            switch (colorClass)
            {
                case "green-BG":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Green;
                    cellReference.Style.Font.FontColor = XLColor.White;
                    break;

                case "yellow-BG":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Yellow;
                    cellReference.Style.Font.FontColor = XLColor.Black;
                    break;

                case "red-BG":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Red;
                    cellReference.Style.Font.FontColor = XLColor.White;
                    break;
            }
        }
        private static void setArrowColor(IXLCell cellReference, string colorClass)
        {
            switch (colorClass)
            {
                case "fa-arrow-up":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Green;
                    cellReference.Style.Font.FontColor = XLColor.White;
                    break;

                case "fa-arrow-right":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Yellow;
                    break;

                case "fa-arrow-down":
                    cellReference.Style.Fill.BackgroundColor = XLColor.Red;
                    cellReference.Style.Font.FontColor = XLColor.White;
                    break;
            }
        }
        private static void setCellTextAlignment(IXLCell cellReference)
        {
            cellReference.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateReportRequestForTabular(ReportRequest reportRequest)
        {
            Stream req = Request.InputStream;
            TabularDataDTO reportDetails = (TabularDataDTO)Utility.mapRequestToClass(Request.InputStream, typeof(TabularDataDTO));

            //get the userId from the session
            string userName = HttpContext.User.Identity.Name;
            EmployeeBaseDetails requestorData = (from d in db.employeeBaseDetails where d.email.Equals(userName) select d).FirstOrDefault();
            reportRequest.adpId = requestorData.adpId;
            reportDetails.userName = userName;
            reportRequest.status = "DTG";
            reportRequest.requestedDate = DateTime.Now.Ticks;
            reportRequest.requestedFilename = reportDetails.requestedFileName;
            reportRequest.requestParameters = JsonConvert.SerializeObject(reportDetails);
            db.reportRequest.Add(reportRequest);
            db.SaveChanges();
            int id = reportRequest.id;
            var jsonsuccess = new
            {
                rsBody = new
                {
                    RequestId = id
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckReportRequestForTabular()
        {
            Stream req = Request.InputStream;
            EmployeeMasterUploadDTO reportDetails = (EmployeeMasterUploadDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeMasterUploadDTO));
            string filename = null;
            string success = null;
            int id = reportDetails.requestId;
            ReportRequest checkReportStatus = (from rp in db.reportRequest
                                               where (rp.id == id)
                                               select
                                              rp).FirstOrDefault();

            if (checkReportStatus.status == Codes.status.SUCCESS.ToString())
            {
                filename = checkReportStatus.generatedFilename;
                success = "dataUploaded";

            }
            else
            {
                filename = "";
                success = "";
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    filename = filename
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }
        public void ExportExcelTabular()
        {
            string filePath = Request.QueryString["name"];
            DataUploadBatchParameters parameter = (from p in db.dataUploadBatchParameters
                                                   where p.uploadType == Codes.downloadType.TabularData.ToString()
                                                   select p).FirstOrDefault();


            string fileName = Path.Combine(parameter.fileUploadFolder, filePath);

            FileStream MyFileStream = new FileStream(fileName, FileMode.Open);
            long FileSize = MyFileStream.Length;
            Byte[] buffer = new Byte[FileSize];


            MyFileStream.Read(buffer, 0, Convert.ToInt32(FileSize));
            MyFileStream.Close();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=" + filePath);
            Response.OutputStream.Write(buffer, 0, Convert.ToInt32(FileSize));
            Response.Flush();
            Response.Close();

        }
    }
}