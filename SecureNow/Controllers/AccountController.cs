﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SecureNow.Models;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Web.Security;
using SecureNow.Container;
using SecureNow.Validators;
using System.Collections.Generic;
using System.Web.Configuration;
using SecureNow.Controllers;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using CaptchaMvc.HtmlHelpers;
using SecureNow.utilities;

namespace SecureNow.Controllers
{    
    public class AccountController : Controller
    {
        DBModels db = new DBModels();

        [Authorize]
        [AllowAnonymous]

        public ActionResult ForgotPassword()
        {

            return View();
        }
        public ActionResult PasswordReset(string value1, string tl, string nod, string lp)
        {
            ViewBag.Value1 = value1;// "admin@signify.com";
            ViewBag.tl = tl;// "admin@signify.com";
            ViewBag.lp = lp;// "admin@signify.com";
            ViewBag.nod = nod;// "admin@signify.com";
            return View();
        }

        public ActionResult Login()
        {
            // string cookieName = FormsAuthentication.FormsCookieName;
            // HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies[cookieName];
            string email = HttpContext.User.Identity.Name;
            if (email != "")
            {
                string userName = HttpContext.User.Identity.Name;
                long lastPassword = (from e in db.employeeBaseDetails where e.email == userName select e.lastPasswordDate).FirstOrDefault();
                DateTime currentDate = System.DateTime.Now;
                DateTime lastPass = new DateTime(Convert.ToInt64(lastPassword));
                TimeSpan time = DateTime.Now.TimeOfDay;
                DateTime oDate = Convert.ToDateTime(lastPass);
                TimeSpan ts = currentDate - lastPass;
                DateTime tt = System.DateTime.Now.ToLocalTime();
                int numberOfDays = ts.Days;
                TimeSpan tenMin = new TimeSpan(0, 10, 0);
                TimeSpan timeLimit = time.Add(tenMin);
                if (numberOfDays >= 30)
                {
                    return RedirectToRoute("Account", new { value1 = userName, value2 = currentDate, lp = lastPass, tl = timeLimit, nod = ts });
                }                

                else
                {
                    EmployeeDetailsDTO resultsDb = null;
                    resultsDb = (from e in db.employeeBaseDetails
                                 join d in db.employeeRole on e.adpId equals d.adpId
                                 where (e.email.Equals(email))
                                 select new EmployeeDetailsDTO
                                 {
                                     role = d.role,
                                     rememberMe = e.rememberMe
                                 }).FirstOrDefault();

                    if (resultsDb != null && resultsDb.rememberMe == true)
                    {
                        //based on the department route the index of that controller
                        if (resultsDb.role == "ADMINISTRATOR" || resultsDb.role == "NSM-CL" || resultsDb.role == "AM" || resultsDb.role == "TM")
                        {

                            //code for function index
                            //check if there is a department name in the request parameter
                            //if not and the system role of the employee is Managment 
                            //default the department as Sales
                            //populate the model with the path to the cshtml for the department
                            // salesDepartment/SalesDepartmentScripts.cshtml
                            //return ActionResult(model);
                            return RedirectToAction("Index", "AdminDepartment");
                        }
                        else
                        {
                            return RedirectToAction("Index", "EmployeeDepartment");
                        }
                    }
                }
            }

            return View();
        }



        public static long ConvertDateTimeToTicks(DateTime dtInput)
        {
            long ticks = 0;
            ticks = dtInput.Ticks;
            return ticks;
        }

        public ActionResult ResetPassword(PasswordReset model)
        {
            //string email = Request.QueryString["value1"];
            string email = ViewBag.Value1;            
            string PasswordReset = model.newPassword;
            string oldPassword = model.password;
            string name = TempData["value1"].ToString();
            string days = TempData["nod"].ToString();
            string tl = TempData["tl"].ToString();
            email = TempData["value1"].ToString();
            //string c= TempData["lp"].ToString();
            TimeSpan expiryTime = TimeSpan.Parse(tl);
            TimeSpan currentTime = DateTime.Now.TimeOfDay;
            //DateTime myDate = DateTime.Parse(c);
            string Password = model.password;
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
            if (PasswordReset != null)
            {
                if (!hasLowerChar.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should contain At least one lower case letter");

                }
                else if (!hasUpperChar.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should contain At least one upper case letter");


                }
                else if (!hasMiniMaxChars.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should not be less than 8 or greater than 15 characters");


                }
                else if (!hasNumber.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should contain At least one numeric value");


                }

                else if (!hasSymbols.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should contain At least one special case characters");


                }
                else
                {
                    // Lets first check if the Model is valid or not
                    if (email != null)
                    {

                        string userName = Request.QueryString["Value1"];
                        DateTime currentDate = System.DateTime.Now;
                        string lastPass = Request.QueryString["lp"];
                        string timeLimit = Request.QueryString["tl"];
                        //string days = Request.QueryString["nod"];

                        // Check time alloted for changing password expired or not
                        //TimeSpan expiryTime = TimeSpan.Parse(timeLimit);
                        // TimeSpan currentTime = DateTime.Now.TimeOfDay;
                        if (currentTime > expiryTime)
                        {
                            ModelState.AddModelError("", "Time limit of password updation is exceeded");
                        }
                        else
                        {
                            bool userValid = db.employeeBaseDetails.Any(user => user.email == userName && user.status == "ACTIVE");

                            // User found in the database
                            if (userValid)
                            {

                                EmployeeBaseDetails resultsDb = null;
                                resultsDb = (from e in db.employeeBaseDetails
                                             where e.email.Equals(email)
                                             && e.status.Equals("ACTIVE")
                                             select e).FirstOrDefault();

                                string newPassword = model.newPassword;
                                if (resultsDb != null)
                                {
                                    string body = string.Empty;
                                    string EncryptionKey = "incentive_application";
                                    //string oldPassword = null;
                                    byte[] cipherBytes = Convert.FromBase64String(resultsDb.password);
                                    using (TripleDES encryptor = TripleDES.Create())
                                    {
                                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                                        encryptor.Key = pdb.GetBytes(24);
                                        encryptor.IV = pdb.GetBytes(8);
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                                            {
                                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                                cs.Close();
                                            }
                                            oldPassword = Encoding.Unicode.GetString(ms.ToArray());
                                        }
                                    }
                                    if (oldPassword == newPassword)
                                    {

                                        ModelState.AddModelError("", "New password should not be same as old password");
                                    }
                                    else
                                    {
                                        //byte[] clearBytes = Convert.FromBase64String(newPassword);
                                        byte[] clearBytes = Encoding.Unicode.GetBytes(newPassword);
                                        using (TripleDES encryptor = TripleDES.Create())
                                        {

                                            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                                            encryptor.Key = pdb.GetBytes(24);
                                            encryptor.IV = pdb.GetBytes(8);
                                            using (MemoryStream ms = new MemoryStream())
                                            {
                                                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                                                {
                                                    cs.Write(clearBytes, 0, clearBytes.Length);
                                                    cs.Close();
                                                }
                                                newPassword = Convert.ToBase64String(ms.ToArray());

                                            }
                                        }
                                        DateTime lastPassword = System.DateTime.Now.Date;
                                        long newPasswordDate = ConvertDateTimeToTicks(lastPassword);
                                        string user = email;
                                        using (var ctx = new DBModels())
                                        {
                                            int noOfRowUpdated = ctx.Database.ExecuteSqlCommand("Update EmployeeBaseDetails set password = '" + newPassword + "', lastPasswordDate ='" + newPasswordDate + "'  where email = '" + user + "' ");
                                            db.SaveChanges();
                                        }
                                        // ModelState.AddModelError("", "Please check your email.");
                                        //return View("Login","Account");
                                        return RedirectToAction("Login", "Account");
                                    }
                                }
                                else
                                {

                                    ModelState.AddModelError("", "A registered email Id does not exist with the same email Id.");
                                }

                            }

                            else
                            {
                                ModelState.AddModelError("", "A registered email Id does not exist with the same email Id.");

                            }

                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Enter registered email Id.");
                    }
                    // If we got this far, something failed, redisplay form
                }
            }
            else
            {
                ModelState.AddModelError("", "Please Enter Password");
            }
            return View("PasswordReset");
        }
        
        //POST: /Account/Login
        [Authorize]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(EmployeeBaseDetails model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            string userName = model.email;
            string password = model.password;
            string encPassword = null;
            string rememberMe = Convert.ToString(model.rememberMe);
            long lastPassword = (from e in db.employeeBaseDetails where e.email == userName select e.lastPasswordDate).FirstOrDefault();
            EmployeeBaseDetails emp = (from e in db.employeeBaseDetails where e.email == userName select e).FirstOrDefault();
            
            DateTime currentDate = System.DateTime.Now;
            DateTime lastPass = new DateTime(Convert.ToInt64(lastPassword));
            TimeSpan time = DateTime.Now.TimeOfDay;
            DateTime oDate = Convert.ToDateTime(lastPass);
            TimeSpan ts = currentDate - lastPass;
            DateTime tt = System.DateTime.Now.ToLocalTime();
            int numberOfDays = ts.Days;
            TimeSpan tenMin = new TimeSpan(0, 10, 0);
            TimeSpan timeLimit = time.Add(tenMin);
            /*if (numberOfDays >= 30)
            {
                //return (RedirectToAction("PasswordReset", "Account", new { value1 = userName, value2 = currentDate, lp = lastPass, tl = timeLimit, nod = ts }));                
            }*/
           // else
            //{
                // Now if our password was enctypted or hashed we would have done the
                // same operation on the user entered password here, But for now
                // since the password is in plain text lets just authenticate directly
                var isValidCaptcha = this.IsCaptchaValid("Captcha is not valid");
                
                if (!isValidCaptcha)
                {
                    ViewBag.ErrMessage = "Captcha string not matching";
                }
                else
                {
                  
                    if (model.email != null && model.password != null)
                    {
                        var pass = password;
                        string EncryptionKey = "incentive_application";
                        byte[] clearBytes = Encoding.Unicode.GetBytes(pass);
                        using (TripleDES encryptor = TripleDES.Create())
                        {

                            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                            encryptor.Key = pdb.GetBytes(24);
                            encryptor.IV = pdb.GetBytes(8);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(clearBytes, 0, clearBytes.Length);
                                    cs.Close();
                                }
                                encPassword = Convert.ToBase64String(ms.ToArray());

                            }
                        }

                        ////////////////for checking password /////////////////
                        string body = string.Empty;
                       // string EncryptionKey = "incentive_application";
                        string newpassword = null;
                        byte[] cipherBytes = Convert.FromBase64String("SYnMTLMK5hrmC2XswklbYe8usDW8twHc");
                        using (TripleDES encryptor = TripleDES.Create())
                        {
                            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                            encryptor.Key = pdb.GetBytes(24);
                            encryptor.IV = pdb.GetBytes(8);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                                    cs.Close();
                                }
                                newpassword = Encoding.Unicode.GetString(ms.ToArray());
                            }
                        }
                        ////////////for checking password/////////////
                        bool userValid = db.employeeBaseDetails.Any(user => user.email == userName && user.password == encPassword && user.status == "ACTIVE");
                        // User found in the database
                        if (userValid)
                        {
                            EmployeeDetailsDTO resultsDb = null;
                            resultsDb = (from e in db.employeeBaseDetails
                                         join d in db.employeeRole on e.adpId equals d.adpId
                                         where (e.email.Equals(userName))
                                         select new EmployeeDetailsDTO
                                         {
                                             role = d.role,
                                         }).FirstOrDefault();

                            FormsAuthentication.SetAuthCookie(userName, model.rememberMe);
                            Session["UserID"] = Guid.NewGuid();
                            if (rememberMe == "True")
                            {
                                EmployeeBaseDetails result = null;
                                result = (from e in db.employeeBaseDetails
                                          where (e.email.Equals(userName)
                                          && e.status.Equals("ACTIVE"))
                                          select e).FirstOrDefault();

                                result.rememberMe = model.rememberMe;
                                db.SaveChanges();
                            }
                            if (resultsDb != null)
                            {                                                                 
                                    return RedirectToAction("Index", "ManageData");                                
                            }
                            else
                            {
                                ModelState.AddModelError("", "The role is not define for register employee.");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "The user name or password provided is incorrect.");
                        }
                    }
                }
          //  }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // Forgot Password For Employee
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPassword model)
        {
            if (model.email != null)
            {    // Lets first check if the Model is valid or not
                string userName = model.email;

                // Now if our password was enctypted or hashed we would have done the

                bool userValid = db.employeeBaseDetails.Any(user => user.email == userName && user.status == "ACTIVE");

                // User found in the database
                if (userValid)
                {
                    EmployeeBaseDetails resultsDb = null;
                    resultsDb = (from e in db.employeeBaseDetails
                                 where e.email.Equals(userName)
                                 && e.status.Equals("ACTIVE")
                                 select e).FirstOrDefault();

                    if (resultsDb != null)
                    {
                        string body = string.Empty;
                        string EncryptionKey = "incentive_application";
                        string newpassword = null;
                        byte[] cipherBytes = Convert.FromBase64String(resultsDb.password);
                        using (TripleDES encryptor = TripleDES.Create())
                        {
                            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                            encryptor.Key = pdb.GetBytes(24);
                            encryptor.IV = pdb.GetBytes(8);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                                    cs.Close();
                                }
                                newpassword = Encoding.Unicode.GetString(ms.ToArray());
                            }
                        }
                        Console.Write(newpassword);
                        body = "<p style='font-size: 14px; font-weight: 600; font-style: normal; line-height: 20px; letter-spacing: normal; '>Dear " + resultsDb.firstName + ",</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>We've raised request for 'Forget Password'.</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>Your password for signing into Incentiwiser application is: <b> " + newpassword + "</b>.</p>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '>You may log in to the Incentiwiser Application by signing <a href='https://incentiwiser.com/'>https://incentiwiser.com/</a></p><br>" +
                              "<p style='font-size: 14px; font-weight: 400; font-style: normal; line-height: 20px; letter-spacing: normal; '> <b>This is an automatically generated Email, Please do not reply</b><br /><b>  For your queries please email us at support@incentiwiser.com </ b ></p><br>" +
                              "<p style='font-size: 14px; font-weight: 600; font-style: normal; padding-top: 20px; line-height: 5px; letter-spacing: normal; color: #1f497d;'>Thanks & Regards</p>" +
                              "<p style='font-weight: 400; line-height: 0px; font-style: normal; letter-spacing: normal; color: #1f497d;'>Incentiwiser Application Support Team</p>";

                        string subject = "Forgot Password";
                        string emailId = resultsDb.email;
                        Utility.sendMaill(emailId, body, subject);
                        ModelState.AddModelError("", "Please check your email.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "A registered email Id does not exist with the same email Id.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "A registered email Id does not exist with the same email Id.");
                }

            }
            else
            {
                ModelState.AddModelError("", "Enter registered email Id.");
            }
            // If we got this far, something failed, redisplay form
            return View();
        }

        //
        [Authorize]
        public ActionResult LogOff()
        {
            Session.Clear();
            Session.Abandon();          
           
            return RedirectToAction("Login");
        }

        //Get Current User Login Details
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCurrentLoginUserDetails()
        {
            string userName = HttpContext.User.Identity.Name;
            string year = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            int currentYear = Convert.ToInt32(year);
            MonthDTO monthDeatail = new MonthDTO();
            monthDeatail = CommonController.getMonthList((from e in db.keyValueData where e.keyType == "CURRENTQUARTER" select e.keyValue).FirstOrDefault());
            
            EmployeeDetailsDTO results = (from d in db.employeeBaseDetails join e in db.employeeRole on d.adpId equals e.adpId where d.email.Equals(userName) 
                                          select new EmployeeDetailsDTO
                                           {
                                               email = d.email,
                                               adpId = d.adpId,
                                               fullName = d.fullName,
                                               country = d.operationalUnit,
                                               operationalUnits = d.operationalUnit, 
                                               role = e.role
                                           }).FirstOrDefault();
            if (results != null)
            {
                results.roles = new List<EmployeeRole>();
                foreach (string ou in results.operationalUnits.Split(','))
                {
                    KeyValueData getDb = (from k in db.keyValueData where k.keyType.Equals("COUNTRY") && k.keyName.Equals(ou) select k).FirstOrDefault();
                    if (getDb != null)
                    {
                       // dynamicDB = new DBModels("LightingDB_" + getDb.keyValue.ToUpper());

                        List<EmployeeRole> role = (from r in db.employeeRole
                                                   where r.adpId.Equals(results.adpId) && ((r.startMonth <= monthDeatail.startMonth && r.endMonth >= monthDeatail.startMonth)
                                                   || (r.startMonth >= monthDeatail.startMonth && r.endMonth <= monthDeatail.endMonth)
                                                   || (r.startMonth >= monthDeatail.startMonth && r.startMonth <= monthDeatail.endMonth))
                                                   && r.yearRecorded == currentYear
                                                   select r).ToList();
                        if (role != null && role.Count > 0)
                        {
                            results.roles.AddRange(role);
                        }
                    }
                    else
                    {
                        List<EmployeeRole> role = (from r in db.employeeRole
                                                   where r.adpId.Equals(results.adpId) && ((r.startMonth <= monthDeatail.startMonth && r.endMonth >= monthDeatail.startMonth)
                                                   || (r.startMonth >= monthDeatail.startMonth && r.endMonth <= monthDeatail.endMonth)
                                                   || (r.startMonth >= monthDeatail.startMonth && r.startMonth <= monthDeatail.endMonth))
                                                   && r.yearRecorded == currentYear
                                                   select r).ToList();
                        if (role != null && role.Count > 0)
                        {
                            results.roles.AddRange(role);
                        }
                    }

                }
            }
            List<EmployeeRole> empList = (from u in db.employeeRole
                                    where u.adpId == results.adpId
                                    select u).ToList();

            var jsonsuccess = new
            {
                rsBody = new {
                    result = "success",
                    data = results,
                    empList = empList
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        //Update Employee Password
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateEmployeePassword()
        {
            Stream req = Request.InputStream;
            EmployeeDetailsDTO employeeBasicDetails = (EmployeeDetailsDTO)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeDetailsDTO));
            DateTime date = System.DateTime.Now;
            long today = ConvertDateTimeToTicks(date);
            ExceptionResponseContainer retVal = CustomValidator.applyValidations(employeeBasicDetails, typeof(EmployeePasswordValidate));
            string userName = HttpContext.User.Identity.Name;
            string message = null;
            string PasswordReset = employeeBasicDetails.newPassword;
            string oldPassword = employeeBasicDetails.password;// (from e in db.employeeBaseDetails where e.email == employeeBasicDetails.email select e.password).FirstOrDefault();

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
            if (PasswordReset != null)
            {
                if (!hasLowerChar.IsMatch(PasswordReset))
                {

                    ModelState.AddModelError("", "Password should contain At least one lower case letter");
                    message = "error";
                }
                else if (!hasUpperChar.IsMatch(PasswordReset))
                {
                    ModelState.AddModelError("", "Password should contain At least one upper case letter");
                    message = "error";
                }
                else if (!hasMiniMaxChars.IsMatch(PasswordReset))
                {
                    ModelState.AddModelError("", "Password should not be less than 8 or greater than 15 characters");
                    message = "error";
                }
                else if (!hasNumber.IsMatch(PasswordReset))
                {
                    ModelState.AddModelError("", "Password should contain At least one numeric value");
                    message = "error";
                }
                else if (!hasSymbols.IsMatch(PasswordReset))
                {
                    ModelState.AddModelError("", "Password should contain At least one special case characters");
                    message = "error";
                }
                else
                {
                    if (retVal.isValid == false)
                    {
                        message = "error";
                        return Json(retVal.jsonResponse, JsonRequestBehavior.AllowGet);
                        
                    }
                    else
                    {
                        string encPassword = null;
                        var pass = employeeBasicDetails.newPassword;
                        string EncryptionKey = "incentive_application";
                        byte[] clearBytes = Encoding.Unicode.GetBytes(pass);
                        using (TripleDES encryptor = TripleDES.Create())
                        {

                            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                            encryptor.Key = pdb.GetBytes(24);
                            encryptor.IV = pdb.GetBytes(8);
                            using (System.IO.MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(clearBytes, 0, clearBytes.Length);
                                    cs.Close();
                                }
                                encPassword = Convert.ToBase64String(ms.ToArray());

                            }
                        }
                        if (oldPassword == encPassword)
                        {
                            ModelState.AddModelError("", "New password should not be same as old password");
                            message = "error";
                        }
                        else
                        {
                            EmployeeBaseDetails empDetails = (from d in db.employeeBaseDetails
                                                              where d.id.Equals(employeeBasicDetails.id)
                                                              select d).FirstOrDefault();
                            if (empDetails != null)
                            {
                                empDetails.password = encPassword;
                                empDetails.lastPasswordDate = today;
                                empDetails.phoneNumber = employeeBasicDetails.phoneNumber;
                                db.SaveChanges();
                            }
                            message = "success";
                        }
                    }
                }
            }
            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success",
                    message = message
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        //Get Current User Login Details
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetEmployeeDetails()
        {
            string userName = HttpContext.User.Identity.Name;
            int currentMonth = DateTime.Now.Month;
            List<EmployeeDetailsDTO> results = (from d in db.employeeBaseDetails
                                                join r in db.employeeRole on d.adpId equals r.adpId
                                                where d.email.Equals(userName)
                                                select new EmployeeDetailsDTO
                                                {
                                                    id = d.id,
                                                    fullName = d.fullName,
                                                    firstName = d.firstName,
                                                    lastName = d.lastName,
                                                    middleName = d.middleName,
                                                    role = r.role,
                                                    RP1= r.RP1,
                                                    RP2 = r.RP2,
                                                    RP3 = r.RP3,
                                                    RP4 = r.RP4,
                                                    RP5 = r.RP5,
                                                    RP6 = r.RP6,
                                                    adpId = d.adpId,
                                                    email = d.email,
                                                    yearRecorded = d.yearRecorded,
                                                    status = d.status,
                                                    dojStr = d.dojStr,
                                                    phoneNumber = d.phoneNumber
                                                }).ToList();

            List<KeyValueData> keyData = (from k in db.keyValueData
                                          select k).ToList();
            foreach (EmployeeDetailsDTO item in results)
            {
                foreach (KeyValueData keyV in keyData)
                {
                  
                    if (keyV.keyValue == item.role && keyV.keyName == "ROLE")
                    {
                        item.role = keyV.description;
                    }
                }
            }

            var jsonsuccess = new
            {
                
                rsBody = new
                {
                    result = "success",
                    results = results
                }
            };


            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        // Update Profile 
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateProfile()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetails empDetails = (EmployeeBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetails));


            EmployeeBaseDetails empBaseDetails = (from d in db.employeeBaseDetails
                                                  where d.adpId.Equals(empDetails.adpId)
                                                  && d.email.Equals(empDetails.email)
                                                  && d.id.Equals(empDetails.id)
                                                  select d).FirstOrDefault();

            if (empBaseDetails != null)
            {
                empBaseDetails.phoneNumber = empDetails.phoneNumber;
                db.SaveChanges();
            }

            var jsonsuccess = new
            {
                rsBody = new
                {
                    result = "success"
                }
            };
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet);
        }

        //POST: /Account/Login
        [Authorize]
        [HttpPost]
        [AllowAnonymous]
        public JsonResult mLogin()
        {
            Stream req = Request.InputStream;
            EmployeeBaseDetails model = (EmployeeBaseDetails)Utility.mapRequestToClass(Request.InputStream, typeof(EmployeeBaseDetails));
            object jsonsuccess = null;
            if (model.email != null && model.password != null)
            {    // Lets first check if the Model is valid or not
                string userName = model.email;
                string password = model.password;
                string encPassword = null;
                string rememberMe = Convert.ToString(model.rememberMe);

                // Now if our password was enctypted or hashed we would have done the
                // same operation on the user entered password here, But for now
                // since the password is in plain text lets just authenticate directly
                var pass = password;
                string EncryptionKey = "incentive_application";
                byte[] clearBytes = Encoding.Unicode.GetBytes(pass);
                using (TripleDES encryptor = TripleDES.Create())
                {

                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(24);
                    encryptor.IV = pdb.GetBytes(8);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encPassword = Convert.ToBase64String(ms.ToArray());

                    }
                }

                bool userValid = db.employeeBaseDetails.Any(user => user.email == userName && user.password == encPassword && user.status == "ACTIVE");

                // User found in the database
                if (userValid)
                {
                    EmployeeDetailsDTO resultsDb = null;
                    resultsDb = (from e in db.employeeBaseDetails
                                 join d in db.employeeRole on e.adpId equals d.adpId
                                 where (e.email.Equals(userName))
                                 select new EmployeeDetailsDTO
                                 {
                                     role = d.role,
                                 }).FirstOrDefault();

                    FormsAuthentication.SetAuthCookie(userName, model.rememberMe);

                    if (rememberMe == "True")
                    {
                        EmployeeBaseDetails result = null;
                        result = (from e in db.employeeBaseDetails
                                  where (e.email.Equals(userName)
                                  && e.status.Equals("ACTIVE"))
                                  select e
                       ).FirstOrDefault();

                        result.rememberMe = model.rememberMe;
                        db.SaveChanges();

                    }
                    if (resultsDb != null)
                    {
                        if (resultsDb.role == "ADMINISTRATOR" || resultsDb.role == "SUPERADMIN")
                        {
                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    msg = "success",
                                    role = resultsDb.role
                                }
                            };
                        }
                        else
                        {
                            jsonsuccess = new
                            {
                                rsBody = new
                                {
                                    msg = "success",
                                    role = "Employee"
                                }
                            };
                        }

                    }
                    else
                    {
                        jsonsuccess = new
                        {
                            rsBody = new
                            {
                                msg = "The role is not define for register employee.",
                                role = ""
                            }
                        };
                    }


                }
                else
                {
                    jsonsuccess = new
                    {
                        rsBody = new
                        {
                            msg = "The user name or password provided is incorrect.",
                            role = ""
                        }
                    };
                }

            }
            // If we got this far, something failed, redisplay form
            return Json(jsonsuccess, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

    }
}