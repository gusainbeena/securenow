﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class SecondarySalesDTO
    {
       
        public int year { get; set; }
        public string adpId { get; set; }
        public int channel { get; set; }
        public string distributionName { get; set; }
        public string distributionCode { get; set; }
        public string bg { get; set; }
        public string bu { get; set; }
        public decimal value { get; set; }
        public string invoiceStatus { get; set; }
        public Boolean nonDMSFlag { get; set; }
        public string status { get; set; }
    }
}