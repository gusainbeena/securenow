﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class UploadActualDTO
    {
        public int id { get; set; }
        public int year { set; get; }
        public string month { get; set; }
        public string period { get; set; }
        public decimal amount { get; set; }
        public string bg { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string adpId { get; set; }
        public string role { get; set; }
        public string requestRaisedByAdpId { get; set; }
        public long dateTime { get; set; }
        public string approverAdpId { get; set; }
        public string approverName { get; set; }
        public string requestRaisedForAdpId { get; set; }
        public string requestRaisedByName { get; set; }
        public string requestRaisedForName { get; set; }

    }

}