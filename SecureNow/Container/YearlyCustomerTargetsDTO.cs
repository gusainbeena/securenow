﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class YearlyCustomerTargetsDTO
    {
        public List<YearlyTargetsDTO> customerTargets { get; set; }
        [Required]
        public string buCode { get; set; }
        public string empName { get; set; }
        public string adpId { get; set; }
        public string dataType { get; set; }
        public int month { get; set; }
       
        public decimal targetAmt { get; set; }
        public int yearRecorded { get; set; }
      
        public int customerEmployeeLinkFK { set; get; }
        public string frequencyType { get; set; }
    }

    public class FortNightTargetDTO
    {

        public int id { get; set; }
        public int yearlyTargetFK { get; set; }
        public decimal target { get; set; }
        public string targetType { get; set; }
        public int month { set; get; }

        public Dictionary<string, List<YearlyCustomerTargets>> OPData { set; get; }
        public Dictionary<string, List<FortNightTargetDTO>> fortnightData { set; get; }
    }
    public class YearlyTargetsDTO
    {

        public int id { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public int month { get; set; }
        public string buCode { get; set; }
        public decimal targetAmt { get; set; }
        public int yearRecorded { get; set; }
        public string dataType { get; set; }
        public int customerEmployeeLinkFK { set; get; }
        public string frequencyType { get; set; }
    }
    public class CustomerTargetsDTO
    {
        public List<YearlyCustomerTargets> customerTargets { get; set; }
        public string buCode { get; set; }
    }

    public class TargetSplitDetailsDTO
    {
        public List<SplitCustomerRecordsDTO> addCustomer { get; set; }
        public string bgCode { get; set; }
    }
}