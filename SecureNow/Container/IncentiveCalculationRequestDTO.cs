﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{   
    public class IncentiveCalculationRequestDTO
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public int year { get; set; }
        public string quarter { get; set; }
        public string incentiveName { get; set; }
        public string status { get; set; }
        public string incentiveSheetPath { get; set; }
        public bool outOfTarn { get; set; }
        public string comments { get; set; }
        public string disbursementSheet { get; set; }
    }
    public class IncentiveCalculatorDTO
    {

        public string key { get; set; }
        public decimal value { get; set; }
        public decimal target { get; set; }
        public int linearCount { get; set; }
        public int reLinearCount { get; set; }
        public decimal btg { get; set; }
        public decimal actual { get; set; }
        public decimal actualAll { get; set; }
        public decimal targetAll { get; set; }
        public decimal toBeActual { get; set; }

    }

    public class IncentiveConfirmationsDTO
    {
        public int id { get; set; }

        public int incentiveRequestId { get; set; }
        public string adpId { get; set; }
        public string channelCode { get; set; }
        public string role { get; set; }
        public string areaCode { get; set; }
        public int branchDetailsFK { get; set; }
        public string status { get; set; }
        public long actionTakenDate { get; set; }
        public string comments { get; set; }
        public string pathToFile { get; set; }
        public List<string> incentiveError { get; set; }

    }
    public class IncentiveDTO
    {
        public int id { get; set; }
        public int incentiveRequestId { get; set; }
        public string adpId { get; set; }
        public string channelCode { get; set; }
        public string role { get; set; }
        public string areaCode { get; set; }
        public int branchDetailsFK { get; set; }
        public string subChannel { get; set; }
        public string status { get; set; }
        public long actionTakenDate { get; set; }
        public string comments { get; set; }
        public string pathToFile { get; set; }
        public string branchCode { get; set; }
        public string fullName { get; set; }
        public string region { get; set; }
        public int year { get; set; }
        public string quarter { get; set; }
        public string incentiveName { get; set; }
        public int incentiveStartMonth { get; set; }
        public int incentiveEndMonth { get; set; }
        public string incentiveSheetPath { get; set; }
        public string finalStatus { get; set; }
        public Boolean showApproval { get; set; }
        public Boolean approvalData { get; set; }
    }

    public class IncentiveExceptionDTO
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public string status { get; set; }
        public string channelCode { get; set; }
        public string role { get; set; }
        public string areaCode { get; set; }
        public int branchDetailsFK { get; set; }
        public string region { get; set; }
        public long initiatedDate { get; set; }
        public string raisedOnAdpId { get; set; }
        public string raisedOnRole { get; set; }
        public string comments { get; set; }
        public string startMonth { get; set; }
        public string endMonth { get; set; }
        public int incentiveRequestFK { get; set; }
        public int yearRecorded { get; set; }
        public string assignAdpId { get; set; }
        public string actionTaken { get; set; }
        public long actionDate { get; set; }
        public bool assignTo { get; set; }
        public int approvalSequence { get; set; }
        public int incentiveExceptionFK { get; set; }
        public string initiatedDateStr { get; set; }
        public string assignEmployeeName { get; set; }
        public string rasedEmployeeName { get; set; }
        public string branchCode { get; set; }
        public string name { get; set; }
        public decimal disbursedAmt { get; set; }

    }

    public class InitiateIncentiveOverrideDTO
    {
        public string adpId { get; set; }
        public int incentiveFK { get; set; }
        public string comment { get; set; }
        public int yearRecorded { get; set; }
    }

}