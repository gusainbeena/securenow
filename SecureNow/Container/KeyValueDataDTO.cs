﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{

    public class AttributeLinkDTO
    {
        public string attributeSequence {get;set;}
        public string childLabel { get; set; }
        public int childId { get; set; }
        public Boolean endLoop { get; set; }
    }

    public class KeyValueDataDTO
    {
        public int id { get; set; }
        public string keyType { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public string keyDescription { get; set; }
        public string relatedKeyType { get; set; }
        public string relatedKeyValue { get; set; }
        public int preferenceValue { get; set; }
        public int preferenceSerial { get; set; }
        public Boolean keyType_Flag { set; get; }
        public Boolean keyName_Flag { set; get; }
        public long baseValue { set; get; }
        public string region { set; get; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { set; get; }
        public string modalKey { set; get; }
        public string labelOnScreen { get; set; }
        public string description { get; set; }
    }

    public class SalesHorizontalDeactivation
    {
        public string name { get; set; }
        public string key { get; set; }
        public int id { get; set; }
        public List<ParentChildNode> children { get; set; }
        public ChildNode currentItem { get; set; }
        public MetaDataConfigureDTO newNode { get; set; }
        public int preferenceValue { get; set; }
        public SalesHorizontalDeactivation()
        {
            children = new List<ParentChildNode>();
            newNode = new MetaDataConfigureDTO();
            currentItem = new ChildNode();
        }
    }

    public class ParentChildNode
    {
        public string name { get; set; }
        public string key { get; set; }
        public string period { get; set; }
        public int id { get; set; }
        public List<ParentChildNode> children { get; set; }
        public ChildNode currentItem { get; set; }
        public MetaDataConfigureDTO newNode { get; set; }
        public int preferenceValue { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        //parentChildNode currentItem { get; set; }
        public ParentChildNode()
        {
            children = new List<ParentChildNode>();
            newNode = new MetaDataConfigureDTO();
            currentItem = new ChildNode();
        }
    }
    public class ChildNode
    {
        public string name { get; set; }
        public string key { get; set; }
        public int id { get; set; }
    }

    public class RequiredValue
    {
        public List<string> subChannelList { get; set; }
        public List<string> regionList { get; set; }
        public List<string> areaList { get; set; }
        public List<string> subAreaList { get; set; }

    }

    public class MetaDataSetupDTO
    {
        public int id { set; get; }
        public string key { get; set; }
        public string subkey { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { set; get; }
        public int preferenceValue { set; get; }
        public string value { set; get; }
        public string labelOnScreen { set; get; }
        public List<string> linkedBoundries { set; get; }
        public string uploadFrequency { set; get; }
        public List<RolePerformanceListDTO> roleHierarchy { set;get;}
        public List<RolePerformanceListDTO> performanceComponent { set;get;}
    }

    public class RolePerformanceListDTO
    {
        public KeyValueData row { set; get; }
        public List<string> selectedBoundries { set; get; }
        public Boolean selected { set; get; }
        public string labelOnScreen { set; get; }
        public int sequence { get; set; }
        public string uploadFrequency { get; set; }
        public Boolean optional { set; get; }
        public Boolean customerFacing { set; get; }
        public Boolean selectAll { set; get; }
        public string attrSequence { set; get; }
    }

    public class AttributeLinkingDTO
    {
        public List<RolePerformanceListDTO> attrDetail { set; get; }
        public List<string> attrSequence { set; get; }

        public AttributeLinkingDTO()
        {
            this.attrDetail = new List<RolePerformanceListDTO>();
            this.attrSequence = new List<string>();
        }
    }

    public class EmployeeAttributesDTO
    {
        public int id { set; get; }
        public string key { get; set; }

        public string channel { get; set; }
        public string subChannel { get; set; }
        public string region { get; set; }
        public string area { get; set; }
        public string subArea { get; set; }

        public string SB1 { get; set; }
        public string SB2 { get; set; }
        public string SB3 { get; set; }
        public string SB4 { get; set; }
        public string SB5 { get; set; }
        public string SB6 { get; set; }
        public string SB7 { get; set; }
        public string SB8 { get; set; }
        public string SB9 { get; set; }
        public string SB10 { get; set; }
        public string SB11 { get; set; }
        public string SB12 { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
     //   public KeyValueDataDTO keyValue { set; get; }
        public List<KeyValueData> keyValueList { set; get; }
        public string value { set; get; }
        //only for rendering purpose
        public List<EmployeeAttributeLink> subChannelList { set; get; }
        public List<EmployeeAttributeLink> areaList { set; get; }
    }
    public class MonthDataDTO
    {

        public int id { get; set; }
        public List<string> colorClass { get; set; }
        public string buName { get; set; }
        public string bgName { get; set; }
        public List<decimal> actual { get; set; }
        public List<string> dataRofo { get; set; }
        public List<Decimal> target { get; set; }
        public List<Decimal> performance { get; set; }
        public List<string> employeeNames { get; set; }
        public List<string> bgNames { get; set; }


        public MonthDataDTO()
        {
            actual = new List<decimal>();
            target = new List<decimal>();
            performance = new List<decimal>();
            bgNames = new List<string>();
            colorClass = new List<string>();
        }

    }

    public class NameData
    {
        public NameData()
        {
            data = new List<decimal>();
        }


        public string name { get; set; }
        public List<decimal> data { get; set; }
    }
    public class MonthDTO
    {
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string quarter { get; set; }

    }

    public class MetaDataConfigureDTO
    {
        public string keyType { get; set; }
        public Boolean heirarchyAssign { get; set; }
        public string attributeSequence { get; set; }
        public string tab { get; set; }
        public string keyName { get; set; }
        public string key { get; set; }
        public string keyValue { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string labelOnScreen { get; set; }
        public int year { get; set; }
        public int preferenceValue { get; set; }
        public int upperPreferenceValue { get; set; }
        public AttributesLink value { get; set; }
        public List<string> linkedBoundries { get; set; }
        public ChildNode childNodeDetail { get; set; }
        public ParentChildNode heirarchyNode { get; set; }
        public string description { get; set; }
        public string relatedKeyType { get; set; }
        public string level_0 { get; set; }
        public string level_1 { get; set; }
        public string level_2 { get; set; }
        public string level_3 { get; set; }
        public string level_4 { get; set; }
        public string level_5 { get; set; }
        public string level_6 { get; set; }
        public string level_7 { get; set; }
        public string level_8 { get; set; }
        public string level_9 { get; set; }
        public string level_10 { get; set; }
        public string level_11 { get; set; }
        public string level_12 { get; set; }
        public string level_13 { get; set; }
        public string level_14 { get; set; }
        public string level_15 { get; set; }
    }
    
    public class heirarchyLevelList
    {
        public string keyName { get; set; }
        public List<KeyValueData> value { get; set; }
    }


    public class DeactivateSalesHorizontalDTO
    {
        public List<KeyValueData> hierarchyList = new List<KeyValueData>();
        public string message { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public int keyId { get; set; }
        public string attributeSequence { get; set; }
        public int childId { get; set; }
        public int parentId { get; set; }
        public int rootId { get; set; }
    }

    public class KeyValueDataLinkDTO
    {
        public int id { set; get; }
        public int keyValueFK { get; set; }
        public string keyType { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public string labelOnScreen { get; set; }
        public string description { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { get; set; }
     //   public string requiredSalesHorizontals { set; get; }
        public Boolean optional { set; get; }
        public string uploadFrequency { set; get; }
        public int countSalesHorizontal { get; set; }
    }
}