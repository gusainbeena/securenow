﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class ExceptionResponseContainer
    {
        public Boolean isValid { get; set; }
        public dynamic jsonResponse { get; set; }
    }
}