﻿using SecureNow.Container;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    public class TypeaheadDTO
    {

        public string id { get; set; }
        public string text { get; set; }
        public string role { get; set; }
        public string adpId { get; set; }
        public Dictionary<string, List<TypeaheadDTO>> lowerLevel { set; get; }
        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        public string RP8 { get; set; }
        public string channelCode { set; get; }
        public string subChannelCode { set; get; }
        public string seCode { set; get; }
        public string searchKeyRequired { get; set; }
        public int roleId { set; get; }

    }

    public class GlobeAttributes
    {
        public string id { set; get; }
        public string style { set; get; }
        public string path { set; get; }
        public string title { set; get; }
        public string color { set; get; }
        public string currency { set; get; }
        public decimal performance { set; get; }
        public string status { set; get; }
    }
    public class UserDashboardDTO
    {

        public int id { get; set; }
        public string name { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public List<Decimal> primaryAchievement { get; set; }
        public List<Decimal> secondaryAchievement { get; set; }
        public List<Decimal> achievementData { get; set; }
        public List<Decimal> primaryActual { get; set; }
        public List<Decimal> secondaryActual { get; set; }
        public List<Decimal> targets { get; set; }
        public List<Decimal> actuals { get; set; }
        public List<Decimal> performance { get; set; }
        public List<String> bgNames { get; set; }
        public List<String> monthName { get; set; }
        public List<decimal> data { get; set; }
        public List<UserDashboardDTO> dataList { get; set; }
        public int activeEmployee { get; set; }
        public int resignedEmployee { get; set; }
        public int activeCustomer { get; set; }
    }

    public class GraphValueDTO
    {
        public decimal target { set; get; }
        public decimal actual { set; get; }
        
        //public decimal performance { set; get; }
        public GraphValueDTO()
        {
            this.target = 0;
            this.actual = 0;
            
            //this.performance = 0;
        }
    }

    public class MyDashboard
    {
        public List<string> target { get; set; }
        public List<string> actual { get; set; }
        public List<string> employee { get; set; }
        public List<string> achievement { get; set; }
        public EmployeeRole employeeData { get; set; }
        public List<EmployeeRole> employeeDataList { get; set; }
        public string Param { get; set; }
        public EmployeeRole empParam { get; set; }
        public List<MyDashboard> tabularData { get; set; }
        public MyDashboard()
        {
            tabularData = new List<MyDashboard>();
            target = new List<string>();
            actual = new List<string>();
            employee = new List<string>();
            employeeDataList = new List<EmployeeRole>();
            employeeData = new EmployeeRole();
            empParam = new EmployeeRole();
            achievement = new List<string>();

        }
    }
    public class DashboardGraphDataDTO
    {
        public DashboardGraphs graphDetail { set; get; }
        public string xAxisLabelString { set; get; }
        public List<string> graphXAxis { set; get; }
        public string yAxisLabelString { set; get; }
        public Dictionary<string, GraphValueDTO> graphData { set; get; }

    }

    public class TerritoryDetailsDTO
    {
        public int id { get; set; }
        public string adpId { get; set; }
        public string channelCodeFrom { get; set; }
        public string subChannelCodeFrom { get; set; }
        public string areaCodeFrom { get; set; }
        public string territoryCodeFrom { get; set; }
        public string role { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string region { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string status { get; set; }
        public string dojStr { get; set; }
        public int yearRecorded { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public bool rememberMe { get; set; }
        public int roleId { set; get; }
        public int startMonth { set; get; }
        public int endMonth { set; get; }
        public string channelCodeTo { get; set; }
        public string subChannelCodeTo { get; set; }
        public string areaCodeTo { get; set; }
        public string territoryCodeTo { get; set; }
        public string customerNameFrom { get; set; }
        public string customerCodeFrom { get; set; }
        public string customerNameTo { get; set; }
        public string customerCodeTo { get; set; }
        public int startMonthFrom { get; set; }
        public int endMonthFrom { get; set; }
        public int startMonthTo { get; set; }
        public int endMonthTo { get; set; }
        public int customerBaseDetailFk { get; set; }
        public TerritoryDetailsDTO customerDetailsFrom { get; set; }
        public TerritoryDetailsDTO customerDetailsTo { get; set; }
    }

    public class TabularDataDTO
    {
        public string key { get; set; }
        public List<string> region { get; set; }
        public List<string> channel { get; set; }
        public List<string> subChannel { get; set; }
        public List<string> seCode { get; set; }
        public List<string> areaList { get; set; }
        public List<string> empName { get; set; }
        public List<string> attrSequence { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string soldToCode { get; set; }
        public string allEmpData { get; set; }
        public string type { get; set; }
        public string reportType { get; set; }
        public string period { get; set; }
        public string branch { get; set; }
        public int pageNumber { get; set; }
        public List<decimal> primaryActualdata { get; set; }
        public List<decimal> opData { get; set; }
        public List<decimal> actualData { get; set; }
        public List<decimal> previousYearActual { get; set; }
        public List<decimal> secondaryActual { get; set; }
        public List<decimal> growth { get; set; }
        public List<decimal> achievement { get; set; }
        public decimal actuals { get; set; }
        public decimal targets { get; set; }
        public string userName { set; get; }
        public string requestedFileName { set; get; }



        public List<string> SB1 { get; set; }
        public List<string> SB2 { get; set; }
        public List<string> SB3 { get; set; }
        public List<string> SB4 { get; set; }
        public List<string> SB5 { get; set; }
        public List<string> SB6 { get; set; }
        public List<string> SB7 { get; set; }
        public List<string> SB8 { get; set; }
        public List<string> SB9 { get; set; }
        public List<string> SB10 { get; set; }
    }
    public class DashboardFilters
    {
        public string keyType { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string labelOnScreen { get; set; }
        public int year { get; set; }
        public int preferenceValue { get; set; }
        public AttributesLink value { get; set; }
        public SearchMetaDataDTO data { set; get; }
        public List<string> level_1 { get; set; }
        public List<string> level_2 { get; set; }
        public List<string> level_3 { get; set; }
        public List<string> level_4 { get; set; }
        public List<string> level_5 { get; set; }
        public List<string> level_6 { get; set; }
        public List<string> level_7 { get; set; }
        public List<string> level_8 { get; set; }
        public List<string> level_9 { get; set; }
        public List<string> level_10 { get; set; }
        public List<string> level_11 { get; set; }
        public List<string> level_12 { get; set; }
        public List<string> level_13 { get; set; }
        public List<string> level_14 { get; set; }
        public List<string> level_15 { get; set; }
    }
    public class TabularDataDTOnew
    {

        public string key { get; set; }
        public string region { get; set; }
        public List<string> channel { get; set; }
        public List<string> subChannel { get; set; }
        public List<string> empName { get; set; }
        public int month { get; set; }
        public string soldToCode { get; set; }
        public string allEmpData { get; set; }
        public string type { get; set; }
        public string reportType { get; set; }
        public string period { get; set; }
        public string branch { get; set; }
        public int pageNumber { get; set; }
        public List<decimal> primaryActualdata { get; set; }
        public List<decimal> opData { get; set; }
        public List<decimal> actualData { get; set; }
        public List<decimal> previousYearActual { get; set; }
        public List<decimal> secondaryActual { get; set; }
        public List<decimal> growth { get; set; }
        public List<decimal> achievement { get; set; }
        public decimal actuals { get; set; }
        public decimal targets { get; set; }
    }
}