﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class BatchRunningProcessDTO
    {

        public int id { get; set; }
        public int syncId { set; get; }
        public int sequence { set; get; }
        public int page { set; get; }
        public string date { set; get; }
        public string batchName { set; get; }
        public string description { set; get; }
        public string startTime { set; get; }
        public string endTime { set; get; }
        public string executionTime { set; get; }
        public string status { set; get; }
        public string operationalUnit { set; get; }
        public string selectedOU { set; get; }
    }
}