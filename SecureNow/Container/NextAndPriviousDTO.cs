﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    public class NextAndPriviousDTO
    {

        public string pageNumber { get; set; }
        public List<string> searchKey { get; set; }
        public string year { get; set; }
        public string type { get; set; }
        public string status { get; set; }
        public List<string> channel { get; set; }
        public List<string> subChannel { get; set; }
        public List<string> roleArray { get; set; }

    }
}