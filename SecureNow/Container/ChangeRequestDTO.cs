﻿using SecureNow.Models;
using SecureNow.Container;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    public class ChangeRequestCustomerRelnDTO
    {
        public List<CustomerDetailsDTO> cusdetails { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public CustomerDetailsDTO customerBaseDetails { get; set; }
        public CustomerDetailsDTO customerDetails { get; set; }

        public string errorMessage { get; set; }

        public List<CustomerRelationshipDetailsDTO> customerRelationship { get; set; }

        public List<CustomerTargetsDTO> taregts { get; set; }

        public CustomerDetailsDTO customertarget { get; set; }

        public List<NewCustomerRelationShipDetailsDTO> customerRelation { get; set; }
    }

    public class ChangeRequestTargetModDTO
    {
        public CustomerBaseDetails customerBaseDetails { get; set; }

        public List<CustomerTargetsDTO> targetList { get; set; }

    }
    public class ChangeRequestTargetSplitDTO
    {
        public SplitCustomerFromRecordsDTO customerBaseDetails { get; set; }

        public List<TargetSplitDetailsDTO> customerDetails { get; set; }

    }
    public class ChangeRequestDTO
    {
        public List<CustomerDetailsDTO> cusdetails { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public string errorMessage { get; set; }
        public List<CustomerDetailsDTO> customerRelationship { get; set; }
        public List<string> buData { get; set; }
        public List<YearlyCustomerTargetsDTO> taregts { get; set; }
        public List<YearlyCustomerTargetsDTO> secondaryTaregts { get; set; }
        public List<CustomerDetailsDTO> customerRelation { get; set; }
        public int disabledMonth { get; set; }
        public ChangeRequestDTO()
        {
            cusdetails = new List<CustomerDetailsDTO>();
            customerRelationship = new List<CustomerDetailsDTO>();
            taregts = new List<YearlyCustomerTargetsDTO>();
            buData = new List<string>();
            secondaryTaregts = new List<YearlyCustomerTargetsDTO>();
        }
    }

    public class CrViewRequestDTO
    {

        public CustomerBaseDetails customerBaseDetails { get; set; }
        public List<YearlyCustomerTargetsDTO> targetList { get; set; }
        public CustomerDetailsDTO customertarget { get; set; }

    }

    public class CrRequestDTO
    {
        public int id { get; set; }
        public string adpId { get; set; }
        public string type { get; set; }
        public long initiatedDate { get; set; }
        public string initiatedDateStr { get; set; }
        public string status { get; set; }
        public string assignAdpId { get; set; }
        public string assignEmployeeName { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public string comments { get; set; }
        public string relatedKeyType { get; set; }
        public int crViewRequestFK { get; set; }
        public string rasedEmployeeName { get; set; }
        public string jsonData { get; set; }
        public int yearRecorded { get; set; }
    }
}