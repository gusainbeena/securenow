﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{   
    public class DataUploadBatchRequestDTO
    {

        public int id { get; set; }
        public string requestType { get; set; }
        public string requestorAdpId { get; set; }
        public string status { get; set; }
        public string timeOfRequest { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { get; set; }
        public string quarter { get; set; }
        public bool fullUpload { get; set; }
        public int recordsProcessed { get; set; }
        public int totalRecords { get; set; }
        public string scheduleTime { get; set; }
        public string dataSourceFileName { get; set; }
        public string errorLogFileName { get; set; }
        public string downloadReportType { get; set; }
        public string additionalParameters { get; set; }
        public string operationalUnit { get; set; }
        public string batchName { get; set; }
        public int page { get; set; }
    }

    public class DataUploadRequestParameterDTO
    {
        public int id { get; set; }
        public string uploadType { get; set; }
        public string fileUploadFolder { get; set; }
        public string errorFileFolder { get; set; }
        public string ProgramFilePath { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public int page { get; set; }
        public int sequence { get; set; }
        public int syncId { get; set; }
    }

   
}