﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class ErrorReportingDTO
    {        
        public int lineNo { get; set; }
        public List<string> data = new List<string>();
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();

    }
}