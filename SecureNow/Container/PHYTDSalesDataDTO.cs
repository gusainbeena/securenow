﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{   
    public class PHYTDSalesDataDTO
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int yearOfLineItem { get; set; }
        public int monthOfLineItem { get; set; }
        public decimal value { get; set; }
        public string soldToParty { get; set; }
        public string referenceType { get; set; }
        public string dataType { get; set; }
        public string seCode { get; set; }
        public string seCodeAm { get; set; }
        public string kamCode { get; set; }
        public Nullable<DateTime> billDate { get; set; }
        public string channelCode { get; set; }
        public string region { get; set; }
        public string dateStr { get; set; }
    }

    public class PrimarySalesDataDTO
    {
        public List<string> bgGroup { get; set; }
        public int region { get; set; }
        public string branch { get; set; }
        public string customerCode { get; set; }
        public List<decimal> PrimarySalesData { get; set; }
        public List<decimal> SecondarySalesData { get; set; }
    }

    public class PrimarySalesDTO
    {
        [Required]
        public int yearOfLineItem { get; set; }
        public int monthOfLineItem { get; set; }
        public string billNumber { get; set; }
        public string itemNumber { get; set; }
        public string billCategory { get; set; }
        public string billT { get; set; }
        public int qty { get; set; }
        public decimal value { get; set; }
        public string currency { get; set; }
        public string productHeirarchy { get; set; }
        public string bg1 { get; set; }
        public string materialCode { get; set; }
        public string sag { get; set; }
        public string bu { get; set; }
        public string bg { get; set; }
        public string soldToParty { get; set; }
        public string channel { get; set; }
        public string stateCode { get; set; }
        public int returnFromYear { get; set; }
    }
}