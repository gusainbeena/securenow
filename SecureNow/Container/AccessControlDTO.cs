﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class AccessControlDTO
    {
        public List<string> profile { set; get; }
        public Dictionary<string,List<RolePerformanceListDTO>> selectedPermissions { set; get; }
        public List<RolePerformanceListDTO> selectedFunctions { set; get; }
        public List<TreeNode> ouList { set; get; }
        public List<TreeViewItem> treeViewItem { set; get; }
        public List<int> selectedOUs { get; set; }
        public KeyValueData dataList { set; get; }
        public EmployeeRoleAndAreaDTO user { set; get; }
        public List<string> selectedRole { set; get; }        
    }

    public class Node
    {
        public string ouLevel { set; get; }
        public KeyValueData nodeVal { get; set; }
        public KeyValueData parentNode { get; set; }
        public List<Node> childNodes { set; get; }
        public Boolean selected { set; get; }
        public Boolean allChildSelected { set; get; }

        public Node()
        {
            childNodes = new List<Node>();
        }
    }

    public class TreeNode
    {
        public string text { set; get; }
        public int value { get; set; }
        public List<TreeNode> children { set; get; }
        public TreeNode()
        {
            children = new List<TreeNode>();
        }
    }

    public class TreeViewItem
    {
        public string text { set; get; }
        public int value { get; set; }
        public string internalChecked { get; set; }
        public List<TreeViewItem> internalChildren { set; get; }

        public TreeViewItem()
        {
            internalChildren = new List<TreeViewItem>();
        }
    }
}