﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class SearchDataCorrectionDTO
    {
        public string dataType { set; get; }
        public string customerCode { set; get; }
        public string additionalParameters { set; get; }
        public int startMonth { set; get; }
        public int endMonth { set; get; }
        public int year { set; get; }
        public string pageNumber { get; set; }
        public string searchKey { get; set; }
        public string filterKey { get; set; }
        public string quarter { set; get; }
        public List<string> selectedMonths { get; set; }
    }
    public class SearchMetaDataDTO
    {
        public string currentTab { set; get; }
        public string subTab { set; get; }
        public string pageNumber { get; set; }
        public string searchKey { get; set; }
        public int metaDataLevel { set; get; }
    }
}