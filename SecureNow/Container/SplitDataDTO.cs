﻿using SecureNow.Container;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    public class SplitDataDTO
    {
        public List<string> RP1List { get; set; }
        public List<string> RP2List { get; set; }
        public List<string> RP3List { get; set; }
        public List<string> RP4List { get; set; }
        public List<string> RP5List { get; set; }
        public List<string> roles { get; set; }
        public List<string> adpId { get; set; }
        public List<EmployeeRoleAndAreaDTO> employeeRoleList { set; get; }
    }
    public class SplitTargetDTO
    {

        public CustomerDetailsDTO customerDetails { get; set; }
        public List<SplitCustomerTargetsDTO> SplitCustomerTarget { get; set; }

    }

    public class SplitCustomerRecordsDTO
    {
        public int id { get; set; }
        public decimal splitValue { get; set; }
        public string customerName { get; set; }
        public string soldToCode { get; set; }
        public string shipToCode { get; set; }
        public string stateCode { get; set; }
    }
    public class SplitTargetValueDTO
    {
        public string buName { get; set; }
        public decimal splitValue { get; set; }
    }

    public class SplitCustomerFromRecordsDTO
    {
        public int id { get; set; }
        public string customerName { get; set; }
        public string customerBaseDetailsFK { get; set; }
        public string branchDetailsFK { get; set; }
        public string soldToCode { get; set; }
        public string shipToCode { get; set; }
        public string groupName { get; set; }
        public string areaCode { get; set; }
        public string districtOrTown { get; set; }
        public string stateCode { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string type { get; set; }
        public int employeeRoleFK { get; set; }
        public string status { get; set; }
        public string doa { get; set; }
        public string doc { get; set; }
        public string doaStr { get; set; }
        public string docStr { get; set; }
        public int yearRecorded { get; set; }
    }
}