﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class EmployeeDetailsDTO
    {
        public int id { get; set; }
        public string adpId { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }
        public string role { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string region { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string status { get; set; }
        public string dojStr { get; set; }
        public int yearRecorded { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public bool rememberMe { get; set; }
        public int roleId { set; get; }
        public int startMonth { set; get; }
        public int endMonth { set; get; }
        public string country { get; set; }
        public string operationalUnits { get; set; }
        public List<EmployeeRole> roles { set; get; }

        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        public string RP8 { get; set; }
        public string RP9 { get; set; }
        public string RP10 { get; set;}

        public EmployeeDetailsDTO()
        {
            roles = new List<EmployeeRole>();
        }
    }

    public class EmployeeBaseDetailsDTO
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string adpId { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }
        [Required]
        public string status { get; set; }
        public string fullName { get; set; }
        [Required]
        public string firstName { get; set; }
        public string middleName { get; set; }
        [Required]
        public string lastName { get; set; }
        public string phoneNumber { get; set; }
        [Required]
        public long doj { get; set; }
        public long dol { get; set; }
        public string dojStr { get; set; }
        public string dolStr { get; set; }
        public bool toBeConsidered { get; set; }
        public int yearRecorded { get; set; }
        [Display(Name = "Remember me?")]
        public bool rememberMe { get; set; }
        public int endMonth { get; set; }
    }

    public class EmployeeAreaLinkDTO
    {

        public string areaCode { get; set; }
        public string employeeName { get; set; }

    }
    public class EmployeeDetailsUpdateDTO
    {
        public EmployeeBaseDetailsDTO empDet { set; get; }
        public List<CustomerBasicRelationDTO> custEmpRel { set; get; }

    }

    public class EmployeeRequestDTO
    {
        public EmployeeBaseDetails employeeBaseDetail { get; set; }
        public InitiateIncentiveOverrideDTO initiateIncetive { get; set; }
        public List<EmployeeRoleAndAreaDTO> employeeRole { get; set; }
        public List<EmployeeRoleAndAreaDTO> responseRoleData { get; set; }
        public List<IncentiveCalculationRequest> incentiveRequest { get; set; }
        public List<IncentiveDTO> incentiveCofirmation { get; set; }
        public CustomerBaseDetailsDTO customerDeatils { get; set; }
        public List<YearlyCustomerTargetsDTO> customerTargets { get; set; }
        public List<YearlyCustomerTargetsDTO> secondaryCustomerTargets { get; set; }
        public List<CustomerBasicRelationDTO> customerRelationship { get; set; }
        public List<CrRequestDTO> CrRequestDetails { get; set; }
        public string role { get; set; }
        public string type { get; set; }
    }

    public class EmployeeRoleAndAreaDTO
    {
        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        public string RP8 { get; set; }

        public int id { get; set; }
        public string adpId { get; set; }
        public string email { get; set; }
        public string type { get; set; }
        public List<string> areaCode { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }
        public string role { get; set; }
        public string seCode { get; set; }
        public string region { get; set; }
        public int startMonth { get; set; }
        public string status { get; set; }
        public long doj { get; set; }
        public int endMonth { get; set; }
        public string grade { get; set; }
        public int yearRecorded { get; set; }
        public string branchDetailsFK { get; set; } 
        public string reportsToAdpId { get; set; }
        public string startMonthDescription { get; set; }
        public string toConsidered { get; set; }
        public string endMonthsDescription { get; set; }
        public string fullName { get; set; }
        public string area { get; set; }
        public List<EmployeeGrade> grades { set; get; }
        public string subArea { get; set; }
        public List<string> subAreaCode { get; set; }
        public string customCalculationStringOP { get; set; }
        public string customCalculationStringActuals { get; set; }
        public EmployeeRoleAndAreaDTO()
        {
            grades = new List<EmployeeGrade>();
        }

        public List<string> requiredData { get; set; }
        public List<KeyValueData> getMonthList { get; set; }

    }

    public class EmployeeActualsAndTargetsDTO
    {
        public int id { get; set; }
        public int monthCode { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }


        public decimal accruedActualAmt { get; set; }

        public string bgCode { get; set; }
        public string buCode { get; set; }
        public string region { get; set; }
        public string areaCode { get; set; }
        public string subAreaCode { get; set; }
        public decimal targetAmt { get; set; }
        public string branchCode { get; set; }
        public string branchName { get; set; }

        public decimal grossActualAmt { get; set; }
        public decimal netActualAmt { get; set; }
        public decimal performance { set; get; }
        public int yearRecorded { get; set; }
        public string drivenFrom { get; set; }
        public string soldToCode { get; set; }
        public string opType { set; get; }
    }

    public class PasswordReset
    {
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Password")]
        public string password { get; set; }
        [Display(Name = "newPassword")]
        public string newPassword { get; set; }

    }

    public class ReporteesDTO
    {
        public List<TypeaheadDTO> employeeList { get; set; }
        public List<KeyValueData> monthsList { get; set; }
        public List<string> quarterList { get; set; }
        public List<string> areaList { get; set; }
        public List<string> seCode { get; set; }
        public string hideIncentive { get; set; }
        public List<string> channel { get; set; }
        public List<string> subChannel { get; set; }
    }
}