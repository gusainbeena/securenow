﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class EmployeeMasterUploadDTO
    {
       

        public string requestType { get; set; }
        public string requestorAdpId { get; set; }
        public string status { get; set; }
        public long timeOfRequest { get; set; }
        public string RequestTime { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string quater { get; set; }
        public bool fullUpload { get; set; }
        public int recordsProcessed { get; set; }
        public int totalRecords { get; set; }
        public string dataSourceFileName { get; set; }
        public string date { get; set; }
        public string errorLogFileName { get; set; }
        public int page { get; set; }
        public string localFileName { get; set; }
        public string uploadType { get; set; }
        public string adpId { get; set; }
        public int requestId { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string requestedFilename { get; set; }
        public string startMonth_str { get; set; }
        public string endMonth_str { get; set; }
        public string downloadReportType { get; set; }
        public string batchName { get; set; }
        public string scheduleTime { get; set; }

        //Tabular DTO
        public string key { get; set; }
        public string region { get; set; }
        public string channel { get; set; }
        public string empName { get; set; }
        public string soldToCode { get; set; }
        public string allEmpData { get; set; }
        public string type { get; set; }
        public string reportType { get; set; }
        public string period { get; set; }
        public string branch { get; set; }
        public int pageNumber { get; set; }
        public decimal actuals { get; set; }
        public decimal targets { get; set; }
        //Tabular DTO
    }
}