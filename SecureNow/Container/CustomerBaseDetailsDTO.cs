﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Container
{
    public class CustomerBaseDetailsDTO
    {
        public int id { get; set; }
        public string customerName { get; set; }
        public string soldToCode { get; set; }
        public string type { get; set; }
        public string areaOfOperation { get; set; }
        public string groupName { get; set; }
        public string districtOrTown { get; set; }
        public string stateCode { get; set; }
        public string status { get; set; }
        public long doa { get; set; }
        public long doc { get; set; }
        public string doaStr { get; set; }
        public string docStr { get; set; }
        public int yearRecorded { get; set; }

    }

    public class SplitCustomerTargetsDTO
    {

        public int id { get; set; }
        public string customerName { get; set; }
        public string soldToCode { get; set; }
        public List<SplitTargetValueDTO> buCodeList { get; set; }

    }

    public class CustomerBasicRelationDTO
    {
        public int id { get; set; }
        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        public string RP8 { get; set; }
        public string subArea { get; set; }
        public int branchDetailsFK { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int yearRecorded { get; set; }
        public int employeeRoleFK { get; set; }
        public int customerRelationshipDetailsFK { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public string employeeRole { get; set; }
        public string adpId { get; set; }
        public string fullName { get; set; }
        public string startMonthDescription { get; set; }
        public string endMonthsDescription { get; set; }
        public string branch { get; set; }
        public string soldToCode { get; set; }
        public string relatedKeyType { get; set; }
        public List<CustomerBasicRelationDTO> otherEmployeesList { set; get; }
        public Boolean multiEmployeeRelation { set; get; }
        public Boolean toBeMergeOrVacent { get; set; }
        public int shareToBeMerge { set; get; }
    }


    public class CustomerRelationDTO
    {
        public List<CustomerBasicRelationDTO> customerRelation { get; set; }
        public List<CustomerBasicRelationDTO> responseRelationData { get; set; }
        public int customerBaseDetailsFK { get; set; }
    }

    public class CustomerDetailsDTO
    {

        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }

        public int id { get; set; }
        public string customerName { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public int branchDetailsFK { get; set; }
        public string soldToCode { get; set; }
        public string groupName { get; set; }
        public string fullName { get; set; }
        public string region { get; set; }
        public string areaCode { get; set; }
        public string districtOrTown { get; set; }
        public string areaOfOperation { get; set; }
        public string stateCode { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string type { get; set; }
        public string comments { get; set; }
        public string startMonthDescription { get; set; }
        public string endMonthsDescription { get; set; }
        public int employeeRoleFK { get; set; }
        public string status { get; set; }
        public long doa { get; set; }
        public long doc { get; set; }
        public string doaStr { get; set; }
        public string docStr { get; set; }
        public string branchCode { get; set; }
        public string employeeCode { get; set; }
        public int yearRecorded { get; set; }
        public string subArea { get; set; }
        public CustomerBaseDetailsDTO customerDeatils { get; set; }
        public List<CustomerBasicRelationDTO> customerRelationship { get; set; }
        public List<YearlyCustomerTargetsDTO> customerTargets { get; set; }

    }

    public class CustomerRelationshipDTO
    { 
        public int id { get; set; }
        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        public string RP8 { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public int branchDetailsFK { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int yearRecorded { get; set; }
        public string startMonthDescription { get; set; }
        public string endMonthsDescription { get; set; }
    }

    public class CustomerRequestDTO
    {
        public List<KeyValueData> areaList { get; set; }
        public List<string> EmployeeRegionList { get; set; }
        public List<KeyValueData> subAreaList { get; set; }
        public List<KeyValueData> regionList { get; set; }
        public List<EmployeeDetailsDTO> employeeList { get; set; }
        public string errorMessage { get; set; }

    }

    public class NewCustomerRelationShipDetailsDTO
    {

        public int id { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }
        public int branchDetailsFK { get; set; }
        public string region { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int yearRecorded { get; set; }
        public int employeeRoleFK { get; set; }
        public int customerrelationshipDetailsFK { get; set; }
        public string areaCode { get; set; }
        public string employeeRole { get; set; }
        public string adpId { get; set; }
        public string fullName { get; set; }
        public string startMonthDescription { get; set; }
        public string endMonthsDescription { get; set; }
        public string branch { get; set; }
    }
    public class CustomerRelationshipDetailsDTO
    {
        public int id { get; set; }
        public string customerName { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public int branchDetailsFK { get; set; }
        public string soldToCode { get; set; }
        public string shipToCode { get; set; }
        public string groupName { get; set; }
        public string region { get; set; }
        public string areaCode { get; set; }
        public string districtOrTown { get; set; }
        public string stateCode { get; set; }
        public string channelCode { get; set; }
        public string subChannelCode { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string type { get; set; }
        public string startMonthDescription { get; set; }
        public string endMonthsDescription { get; set; }
        public int employeeRoleFK { get; set; }
        public string status { get; set; }
        public long doa { get; set; }
        public long doc { get; set; }
        public string doaStr { get; set; }
        public string docStr { get; set; }
        public string branchCode { get; set; }
        public string employeeCode { get; set; }
        public int yearRecorded { get; set; }
    }

    public class DataCorrectionDTO
    {
        public int customerBaseDetailsFK { get; set; }
        public int customerEmployeeLinkFK { get; set; }
        public string customerName { get; set; }
        public int month { get; set; }
        public string buCode { get; set; }
        public decimal targetAmt { get; set; }
        public int yearRecorded { get; set; }
        public string dataType { set; get; }

        public int yearOfLineItem { get; set; }
        public int monthOfLineItem { get; set; }
        public decimal value { get; set; }
        public string soldToParty { get; set; }
        public string referenceType { get; set; }
        public string seCode { get; set; }
        public string seCodeAm { get; set; }
        public string kamCode { get; set; }
        public string billDate { get; set; }
        public string channel { get; set; }
    }
}