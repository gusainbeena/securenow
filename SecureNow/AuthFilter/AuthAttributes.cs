﻿using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Linq;
using SecureNow.utilities;

namespace SecureNow.AuthFilter
{
    public class UserAuthenticationFilter : ActionFilterAttribute, IAuthenticationFilter
    {

        private bool _authenticate;
        public static DBModels db = new DBModels();
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            //get method name 
            string methodName = filterContext.ActionDescriptor.ActionName;
            string userEmail = filterContext.HttpContext.User.Identity.Name;
            List<EmployeeRole> getEmpId = (from e in db.employeeBaseDetails
                                           join f in db.employeeRole on e.adpId equals f.adpId
                                           where e.email == userEmail
                                           select f).ToList();
            _authenticate = false;
            foreach (EmployeeRole itm in getEmpId)
            {
                List<UserExceptionPermissionFlow> getAssignServices = (from e in db.UserExceptionPermissionFlow where e.empId == itm.id select e).ToList();
                if(getAssignServices.Count() == 0)
                {
                    //get role from keyValue
                    int getRoleID = (from e in db.keyValueData where e.keyType == Codes.keyValue.MetaData.ToString()
                                     && e.keyName == Codes.keyValue.RoleHierarchy.ToString() && e.keyValue == itm.role select e.id).FirstOrDefault();
                    getAssignServices = (from e in db.UserExceptionPermissionFlow where e.roleId == getRoleID select e).ToList();
                }
                foreach(UserExceptionPermissionFlow usesrBasedService in getAssignServices)
                {
                    List<int> getServiceID = (from e in db.userAccessLinkings where e.id == usesrBasedService.profileId select e.functionId).Distinct().ToList();
                    foreach(int id in getServiceID)
                    {
                        KeyValueData getServiceName = (from e in db.keyValueData where e.id == id select e).FirstOrDefault();
                        if(getServiceName != null)
                        {
                            if(getServiceName.keyValue == methodName)
                            {
                                if (_authenticate == false)
                                {
                                    _authenticate = true;
                                }
                            }
                        }
                    }
                }
            }            
        }


        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var user = filterContext.HttpContext.User;
            //user == null || !user.Identity.IsAuthenticated
           /* if (_authenticate == false)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }*/
        }

    }
}