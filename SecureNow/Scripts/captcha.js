﻿// captcha script
$(function () {
	$(".captcha-refresh").click(function (e) {
		e.preventDefault();
		var cmd = $(this);
		var img = cmd.parent().find("img").first();
		var url = img.attr("src").replace(/(.*?)\?r=\d+\.\d+$/, "$1");
		img.attr('src', url + "?r=" + Math.random());
		
		return false;
	});
});