﻿var viewRequest = {
    "initiatorData": [
      {
          "sapId": "",
          "ottl": [
            {
                "bg_name": "PCMS",
                "region": "South",
                "amount": 16597451.5,
                "amountStr": "165,97,451.50",
                "month_code": "May",
                "category": "Private",
                "employeeHierarchy": {
                    "FSE": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Sreejith B Nair",
                        "adp_id": "40000313",
                        "role_name": null,
                        "sap_code": "2050159",
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": "South",
                        "category": "Private",
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    },
                    "ASM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Praveen Pinto",
                        "adp_id": "40009518",
                        "role_name": null,
                        "sap_code": "1060864",
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": "South",
                        "category": "Private",
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    },
                    "RSM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Vinay Oommen Thomas",
                        "adp_id": "40009090",
                        "role_name": null,
                        "sap_code": "1060836",
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": "South",
                        "category": "Private",
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    },
                    "RBM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Prasad P P",
                        "adp_id": "40004015",
                        "role_name": null,
                        "sap_code": null,
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": null,
                        "category": null,
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    }
                }
            }
          ],
          "department": "",
          "sapCodeTo": "2050159"
      },
      {
          "sapId": "",
          "ottl": [
            {
                "bg_name": "PCMS",
                "region": "South Deccan",
                "amount": 16597451.5,
                "amountStr": "165,97,451.50",
                "month_code": "May",
                "category": "Private",
                "employeeHierarchy": {
                    "ASM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "R Harish",
                        "adp_id": "40000303",
                        "role_name": null,
                        "sap_code": "2050259",
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": "South Deccan",
                        "category": "Private",
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    },
                    "RSM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Dipu K Bose",
                        "adp_id": "40007029",
                        "role_name": null,
                        "sap_code": "2050282",
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": "South Deccan",
                        "category": "Private",
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    },
                    "RBM": {
                        "reports_to_adp_id": null,
                        "reports_to_name": null,
                        "reports_to_role": null,
                        "name": "Srinivasa Prasad Bodi",
                        "adp_id": "40008269",
                        "role_name": null,
                        "sap_code": null,
                        "role_start_month": 0,
                        "role_end_month": 0,
                        "bg_name": null,
                        "region": null,
                        "category": null,
                        "system_role": null,
                        "amount_from_system_monthly": 0,
                        "amount_from_system_comparable": 0,
                        "email_id": null,
                        "location_name": null,
                        "department": null,
                        "year": null,
                        "password": null
                    }
                }
            }
          ],
          "department": "",
          "sapCodeTo": "2050259"
      }
    ],
    "allemp": [
      {
          "id": 149637,
          "status": null,
          "status_key": null,
          "year": null,
          "month_code": 5,
          "quarter": null,
          "company_code": null,
          "sales_order_type": null,
          "sales_order_number": "6302875884",
          "sales_order_item": null,
          "sales_order_header_creation_date": null,
          "sales_order_item_creation_date": null,
          "bg_name": null,
          "bg_internal": "PCMS",
          "bu_name": null,
          "mag": null,
          "mag_name": null,
          "region": "South Deccan",
          "adp_id": null,
          "emp_name": null,
          "role_name": null,
          "category": "Private",
          "region_for_mis_purpose": null,
          "country": null,
          "sold_to_party_code": null,
          "sold_to_party_name": "ASTER DM HEALTHCARE PRIVATE LIMITED",
          "sold_to_location": "BANGALORE",
          "customer_category_sold_to": null,
          "quantity_from_system": null,
          "currency": null,
          "quantity": null,
          "amount_from_system_monthly": 0,
          "amount_from_system_comparable": 0,
          "amount_from_system_comparable_str": null,
          "amount_from_system_aop_rate": 33194903,
          "amount_from_system_aop_rate_str": "331,94,903.00",
          "commission_weightage": null,
          "state_sold_to": null,
          "state_sold_to_region": null,
          "employee_concatenated_string": [
            [
              "RSM",
              "40007029",
              "Dipu K Bose"
            ],
            [
              "ASM",
              "40000303",
              "R Harish"
            ],
            [
              "TSM",
              "0",
              "0"
            ],
            [
              "FSE",
              "40005515",
              "Manjunath Br"
            ],
            [
              "RBM",
              "40008269",
              "Srinivasa Prasad Bodi"
            ]
          ],
          "reversal_flag": false,
          "reverse_on_date": 0,
          "change_request_id": 0,
          "group_id": 0,
          "previous_change_request_id": 0,
          "record_creation_date": 0,
          "current_change_request_date": 0,
          "previous_change_request_date": 0,
          "order_first_flag": false,
          "view_data": null,
          "month_code_str": "May"
      }
    ],
    "order": {
        "month": [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11
        ],
        "department": "Private",
        "status": "oit",
        "ChangeRequestByRegion": "ChangeRequestByRegion",
        "orderNumber": "6302875884"
    },
    "comment": {
        "resion": "50% sharing between South & Deccan"
    },
    "splitMethod": {
        "split": "equal",
        "ppp": {}
    }
};

var rowWise = [];
var arrayLen = viewRequest.initiatorData.length;
for (var count = 0; count < arrayLen; count++)
{
    var item = viewRequest.initiatorData[count].ottl[0];

    //get the following attributes and check whether the BG is available already or not

    var rowiseLen = rowWise.length;
    var found = false;
    for(var countRows = 0; countRows < rowiseLen; countRows++ )
    {
        var rowItem = rowWise[countRows];

        if(rowItem.bg_name == item.bg_name)
        {
            //add the sum 
            rowItem.amount =  rowItem.amount + item.amount;

            //add the name of the FSE if not there 

            var empName = "";
            var adpId = "";

            if (item.FSE != undefined) {
                empName = item.FSE.name;
                adpId = item.FSE.adp_id;

            }
            else if (item.TSM != undefined) {
                empName = item.TSM.name;
                adpId = item.TSM.adp_id;

            }
            else if (item.ASM != undefined) {
                empName = item.ASM.name;
                adpId = item.ASM.adp_id;

            }
            var foundEmp = false;

            for (var countEmp = 0; countEmp < rowItem.empDet.length; countEmp++)
            {
                if(empName == rowItem.empDet[countEmp].name)
                {
                    foundEmp = true;
                    break;
                }
            }


            if (!foundEmp)
            {
                rowItem.empDet.push({});
                rowItem.empDet[rowItem.empDet.length - 1].name = empName;
                rowItem.empDet[rowItem.empDet.length - 1].adp_id = adpId;
            }
        found= true;
        break;
        }
     
    if(!found)
    {
   
            var newItem = {};
        rowWise.push(newItem);

        newItem.bg_name = item.bg_name;
        newItem.amount = item.amount;
        newItem.empDet = [];
        newItem.empDet.push({});

        if(item.FSE !=  undefined)
        {
            newItem.name[0].name = item.FSE.name;
            newItem.name[0].adp_id = item.FSE.adp_id;

        }
        else if(item.TSM !=  undefined)
        {
            newItem.name[0].name = item.TSM.name;
            newItem.name[0].adp_id = item.TSM.adp_id;

        }
        else if(item.ASM !=  undefined)
        {
            newItem.name[0].name = item.ASM.name;
            newItem.name[0].adp_id = item.ASM.adp_id;

        }
           
    
    }
    }
   
}