﻿import { Injectable } from "@angular/core";

//some dataService, which store your needed data.
@Injectable()
export class Global {

    lowerEmp: object = {};
    breadcrumbLinks: any = [];
    masterBatchStatus: boolean = false;
    currentUser: string = "";
    countryName: string = "";
    periodFilterName: string = "";
    countryMap: any = [];
    currentUserData: any = {};
    sideBarShow: boolean;
    pinSideBar: boolean;
    constructor() { }
}