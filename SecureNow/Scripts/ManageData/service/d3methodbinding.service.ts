﻿import * as d3 from 'd3';
import { Injectable } from '@angular/core';
declare var $: any;

@Injectable()
export class D3BindingsService {

     private drawable: any = [];
     public scope: any;
     public initDraw(initializerObj,scope) {
         this.scope = scope;

         for (var i = 0; i < initializerObj.length; i++) {
             this.drawable['path'] = initializerObj[i].path;
             this.drawable['class'] = initializerObj[i].style;
             this.drawable['id'] = initializerObj[i].id;
             this.drawable['title'] = initializerObj[i].title;
             this.drawable['status'] = initializerObj[i].status;
             this.drawable['progress'] = initializerObj[i].performance;
             this.drawable['fillColor'] = initializerObj[i].colorCode;

             //this.angularScope = this.scopeObj;
             this.drawPerformanceArcs(this.drawable);
         }
     }

     drawPerformanceArcs(drawable) {

         var svgElem = d3.select("svg");
         var gelem = svgElem.append("g");
         //gelem.append("title").attr("id", "title_" + this.id).text(this.title);
         var pathelem = gelem.append("path").attr("id", drawable.id).attr("class", drawable.class).attr("d", drawable.path).attr("title", drawable.title).attr("fill", drawable.fillColor);

         this.bindMouseMoveIn(pathelem, drawable.title);
         this.bindMouseClick(pathelem, drawable.title);
         this.bindMouseMoveOut(pathelem, drawable.title);
     }


     bindMouseMoveIn(elem, title) {
         var sample = elem;
         var text = title;
         

         elem.on('mousemove', function () {
             let tooltip = document.getElementById("tooltip");
             let content = $('#tooltip .tooltip_content');
             let flag = $('#tooltip .flag-icon');
             flag.removeClass();
     
             content[0].innerHTML = text;
             var flagclass = 'flag-icon flag-icon-' + this.id.toLowerCase();
             flag.addClass(flagclass);
             tooltip.style.display = "block";
			 //tooltip.style.left = (event.pageX - 450) + 'px';
             //tooltip.style.top = (event.pageY - 240) + 'px';
         });

     }

     bindMouseClick(elem, title) {
         var sample = elem;
         var name = title;
         elem.on('click', function () {
             return title;
         });

     }

     bindMouseMoveOut(elem, title) {
         var sample = elem;
         var text = title;
         elem.on('mouseout', function (event) {

             let tooltip = document.getElementById("tooltip");
             let content = $('#tooltip .tooltip_content');
             let flag = $('#tooltip .flag-icon');
             flag.removeClass();

             content[0].innerHTML = text;
             var flagclass = 'flag-icon flag-icon-' + this.id.toLowerCase();
             flag.addClass(flagclass);
             tooltip.style.display = "none";
             //tooltip.style.left = (event.pageX - 450) + 'px';
             //tooltip.style.top = (event.pageY - 240) + 'px';
         });

     }
}