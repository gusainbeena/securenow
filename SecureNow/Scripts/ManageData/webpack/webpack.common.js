var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
var helpers = require('./helpers');
const subpath = './Scripts/ManageData';
const lastpath = 'ManageData';

module.exports = {
  entry: {
   'polyfills': subpath+'/polyfills.ts',
   'main': subpath+'/main.ts',
   'styles':subpath+'/styles.css'
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
           {
            loader: 'awesome-typescript-loader',
            options: { configFileName: helpers.root(subpath, 'tsconfig.app.json') }
            },
            'angular2-template-loader'
        ]
        },
        {
            test: /\.(ts|js)$/,
            loaders: [
                'angular-router-loader'
            ]
        },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/images/[name].[hash].[ext]'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader"
        ]
      },
      {
        test: /\.css$/,
        include: helpers.root('src', 'app'),
        loader: 'raw-loader'
        },
        {
            test: /\.scss$/,
            exclude: /node_modules/,
            loaders: ['raw-loader', 'sass-loader'] // sass-loader not scss-loader
        }
    ]
  },

  plugins: [
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)@angular/,
      helpers.root('./AngularSetup/'+subpath), // location of your src
      {} // a map of your routes
    ),

    new CleanWebpackPlugin([
  lastpath
], {
  root:     helpers.root('Script' , 'build/dist')
}),

  /*  new webpack.optimize.CommonsChunkPlugin({
      name: ['companyapp','studentappp' ,'vendor', 'polyfills']
    }),*/

    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default'],
        
      })

    /*new HtmlWebpackPlugin({
      template: './public/index.html'
    })*/
    ],
    optimization: {
        minimize: false // <---- disables uglify.
        // minimizer: [new UglifyJsPlugin()] if you want to customize it.
    }
};
