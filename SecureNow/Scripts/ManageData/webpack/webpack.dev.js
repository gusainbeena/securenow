var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common.js');
var  ManifestPlugin = require('webpack-manifest-plugin');
var helpers = require('./helpers');
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
 const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const subpath = '/Scripts/build/dist/ManageData/';
const lastpath = 'ManageData';

const useVersioning = false;

module.exports = webpackMerge(commonConfig, {
 devtool: 'inline-source-map',


  output: {
    path: helpers.root('' , subpath),
    publicPath: subpath,
    filename: useVersioning ? '[name].[hash:6].js' : '[name].js',
    chunkFilename: '[id].chunk.js'
  },
plugins: [
 new MiniCssExtractPlugin({
  filename: useVersioning ? '[name].[hash:6].bundle.css' :  '[name].bundle.css',
  allChunks: true
 }),
 new ManifestPlugin({
            // always dump manifest
            fileName: 'rev-manifest.json',
            writeToFileEmit: true
        })
  ]
});
