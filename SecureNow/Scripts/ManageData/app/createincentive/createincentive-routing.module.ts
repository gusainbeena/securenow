import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateIncentiveComponent } from './createincentive.component';

const routes: Routes = [
    { path: '', component: CreateIncentiveComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CreateIncentiveRoutingModule { }
