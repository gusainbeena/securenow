﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { ElementAst } from '@angular/compiler';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateIncentive';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})



export class CreateIncentiveComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    //public keyValueData: Object = {};
    public keyValueData: any = {};
    public gradeData: Object = {};


    public IncentiveCalculationRequest: any = {};
    public yearData: any = [];
    public qtrList: any = [];
    public ChannelList: any = [];
    public channel: string = '';
    public subChannelList: string = '';
    public roleList: string = '';
    public actualTypeList: string = '';
    public weightageList: string = '';
    public keyType: string = '';
    public keyName: string = '';
    public subChannel: string = '';
    public actualType: string = '';
    public incentive: any = [];
    public getNextEmployeeList: any;
    public pageNumber: any;
    public incentivList: any = {};
    public id: any;
    public flag: boolean = false;
    public incentiveEmpList: string = "";


    ngOnInit() {



        $('.form-steps').css('display', 'block');
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<CreateIncentiveComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {

                    this.yearData = datas.rsBody.resulats;
                    this.IncentiveCalculationRequest.year = this.yearData[0].keyValue;
                    this.IncentiveCalculationRequest.outOfTarn = false;
                    this.getQTR();

                }
            });

    }


    getQTR() {
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest<CreateIncentiveComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.qtrList = data.rsBody.resulats;
                    this.IncentiveCalculationRequest.quarter = data.rsBody.currentQuarter;
                    this.IncentiveCalculationRequest.quarter = data.rsBody.currentQuarter.keyValue;
                }
            });
    }

    getEmployeeForIncentive() {

        this.httpService.postRequest<CreateIncentiveComponent>(this.IncentiveCalculationRequest, '../ManageIncentive/GenerateIncentiveRequest', true).subscribe(
            data => {

                if (data.rsBody.result === 'success') {
                    if (data.rsBody.message == 'An active incentive request for this or some other quarter is still pending closure.A new incentive request cannot be raised.') {
                        this.incentiveEmpList = data.rsBody.message;
                        $("#responseDataPopupnew").modal();
                    }
                    else {
                        $("#sucessPopupnew").modal();
                    }


                }
            });
    }
    // Incentive Selection
    
    customIncentive() {

    if (this.flag == false) {
        this.flag = true;
        this.getQTR();
        this.IncentiveCalculationRequest.quarter = '';
        this.IncentiveCalculationRequest.incentiveStartMonth = '';
        this.IncentiveCalculationRequest.incentiveEndMonth = '';
        this.IncentiveCalculationRequest.incentiveName = '';
        this.IncentiveCalculationRequest.outOfTarn = true;
        }
        else {
        this.getQTR();
        this.flag = false;
        this.IncentiveCalculationRequest.incentiveName = '';
        this.IncentiveCalculationRequest.outOfTarn = false;
        }

    }

    autoMonthSelect() {
        this.keyValueData.keyType = "QUARTER";
        $("#chkbox").removeClass("ng-hide");
        $('#incentiveName').removeClass("ng-hide");

        this.IncentiveCalculationRequest.incentiveName = 'Incentive_' + this.IncentiveCalculationRequest.quarter + '_' + this.IncentiveCalculationRequest.year;
        /*this.httpService.postRequest<CreateIncentiveComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData').subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.qtrList = data.rsBody;

                }
            });*/
    }
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
