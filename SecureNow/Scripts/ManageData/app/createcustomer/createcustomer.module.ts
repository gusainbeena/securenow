import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { CreateCustomerRoutingModule } from './createcustomer-routing.module';
import { CreateCustomerComponent } from './createcustomer.component';
import { TreeviewModule } from 'ngx-treeview';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
    declarations: [
        CreateCustomerComponent
    ],
    imports: [
        CommonModule,
        CreateCustomerRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        TreeviewModule.forRoot(),
        HttpModule,
        NgbModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [CreateCustomerComponent],
    exports: []
})
export class CreateCustomerModule { }
