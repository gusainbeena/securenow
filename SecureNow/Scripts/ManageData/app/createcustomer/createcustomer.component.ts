﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';



declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateCustomer';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL,
})
export class CreateCustomerComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    //public keyValueData: Object = {};
    public keyValueData: any = {};
    public shipToRequire: any = {};
    public gradeData: Object = {};


    public custDetials: any = {};
    public customerBaseDetails: any = {};
    public responseChannelData: any = [];
    public responseStateData: any = [];
    public BUData: any = [];
    public monthData: any = [];
    public yearData: any = [];
    public regionData: any = [];
    public dataTypeList: any = [];
    public subChannelList: any = [];
    public employeeData: any = [];
    public getList: any = [];
    public targetData: any = [];
    public targetList: any = [];
    public regionList: any = [];
    public branchListData: any = [];
    public subAreaData: any = [];
    public responseAreaData: any = [];
    public customerDetialsData: any = [];
    public MonthList: any = [];
    public getEmployeeList: any = [];
    public shipToPartyCodes: any = [];
    public targetType: any = [];
    public sucessResponseData: any = [];
    public ChannelList: any = [];
    public monthFrequencyList: any = [];
    public errorMessage: string = '';
    public day: string = '';
    public month: string = '';
    public year: string = '';
    public id: any;
    public lengthdata: any;
    public customerBaseDetailsFK: any;
    public employeeBaseDetails: any;
    public areaCode: any;
    public subChannel: any;
    public branchList: any;
    public subArea: any;
    public dataType: any;

    // DatePicker
    addSoldToPartyCodes() {
        this.customerBaseDetails.soldToPartyCodes.push({});
    }

    removeSoldToPartyCodes(e) {
        this.customerBaseDetails.soldToPartyCodes.splice(e, 1);
    }

    ngOnInit() {
        this.customerBaseDetails = {
            soldToPartyCodes: [{}]
        };

        this.custDetials = {
            customerDeatils: {},
            customerTargets: [],
            customerRelationship: []
            
        };
        $("#customerTargets").css('display', 'block');
        $(".form-steps").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-target").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        //this.keyValueData.keyType = "MetaData";//"CHANNEL";
       // this.keyValueData.keyName = "Channel";
        this.keyValueData.keyType = "Channel";//"CHANNEL";
        this.keyValueData.keyName = "SalesHorizontal";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.responseChannelData = datas.rsBody.resulats;
                    this.loadState();
                }
            });

    }

    //Get State List
    loadState() {
        this.keyValueData.keyType = "STATE";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.responseStateData = data.rsBody.resulats;
                    this.customerBaseDetails.status = "ACTIVE";
                    this.getBUList();

                }
            });
    }

    //Get BU List
    getBUList() {
        this.keyValueData.keyType = "MetaData";// "OPERATIONAL";
        this.keyValueData.keyName = "businessVertical";//"BU";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../ManageCustomer/GetBUBGListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.BUData = data.rsBody.resulats;
                    this.getDataTypeList();

                }
            });
    }

    //Get Data Type List
    getDataTypeList() {
        this.keyValueData.keyType = "MetaData";//"OPERATIONAL";
        this.keyValueData.keyName = "businessVertical";//"BU";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../ManageCustomer/GetDataTypeList', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.dataTypeList = data.rsBody.dataTypeList;
                    this.getMonthList();

                }
            });
    }

    //Get Year List
    getYearList() {
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.yearData = data.rsBody.resulats;
                    this.customerBaseDetails.yearRecorded = this.yearData[0].keyValue;
                    this.customerBaseDetails.startMonth = "1";
                    this.customerBaseDetails.endMonth = "12";

                }
            });
    }

    //Get Month List
    getMonthList() {
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../Comman/GetMonthKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.monthData = data.rsBody.resulats;
                    this.monthFrequencyList = data.rsBody.monthList;
                    this.getRegionList();
                }
            });
    }

    //Get REgion List
    getRegionList() {
        this.keyValueData.keyType = "REGION";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.regionData = data.rsBody.resulats;
                    this.getYearList();

                }
            });
    }


    // Create Customer Details Submit
    validateCustomerDetailsSubmit() {
        
        this.day = this.customerBaseDetails.doaStr.day.toString();
        this.month = this.customerBaseDetails.doaStr.month.toString();
        console.log(this.month.length);
        if (this.month.length == 1) {
            this.month = "0" + this.month;
        }
        this.year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = this.day + '-' + this.month + '-' + this.year;
        this.httpService.postRequest<CreateCustomerComponent>(this.customerBaseDetails, '../ManageCustomer/ValidateCustomerBasicDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.customerDetialsData = data.rsBody.custDetails;
                    $("#step-one").css('display', 'none');
                    $(".step-one").removeClass('progress-bar-success');
                    $(".step-one").addClass('progress-bar-inverse');
                    //$(".step-two").addClass('progress-bar-success');
                    //$(".step-two").removeClass('progress-bar-inverse');
                    //$("#step-twos").css('display', 'block');
                    $(".step-three").addClass('progress-bar-success');
                    $(".step-three").removeClass('progress-bar-inverse');
                    $("#step-three").css('display', 'block');
                    $(".step-three").addClass('progress-bar-success');
                    $(".step-three").removeClass('progress-bar-inverse');
                    $("#step-three").css('display', 'block');
                    if (this.custDetials.customerRelationship.length == 0) {
                        this.custDetials.customerRelationship = [{
                            startMonth: this.customerBaseDetails.startMonth,
                            endMonth: this.customerBaseDetails.endMonth,
                            yearRecorded: this.customerBaseDetails.yearRecorded,

                        }];
                    }                 
                }
            });
    }

    // Validate Customer Targets
    validateTargetsSubmit() {
        $("#customerType").addClass('ng-hide');
        this.httpService.postRequest<CreateCustomerComponent>(this.targetList, '../ManageCustomer/ValidateCustomerTargets', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.targetData = data.rsBody.custDetails;
                    $("#step-twos").css('display', 'none');
                    $(".step-two").removeClass('progress-bar-success');
                    $(".step-two").addClass('progress-bar-inverse');
                    $(".step-three").addClass('progress-bar-success');
                    $(".step-three").removeClass('progress-bar-inverse');
                    $("#step-three").css('display', 'block');
                    if (this.custDetials.customerRelationship.length == 0) {
                        this.custDetials.customerRelationship = [{
                            startMonth: this.customerBaseDetails.startMonth,
                            endMonth: this.customerBaseDetails.endMonth,
                            yearRecorded: this.customerBaseDetails.yearRecorded,

                        }];
                    }


                }
            });
    }


    //On selection of Sales Component
    OnSalesComponentChange (data) {
       
        this.keyValueData.keyValue = data;
        this.keyValueData.keyType = "MetaData";
        this.httpService.postRequest<CreateCustomerComponent>(this.keyValueData, '../ManageCustomer/onSalesComponentChange', true).subscribe(
            data => {
                if (data.rsBody.result == 'success') {
                    this.monthFrequencyList = data.rsBody.month;
                }
            });
    }

    // Previous for step 2
    previouSubmit() {
        $("#step-twos").css('display', 'none');
        $(".step-two").removeClass('progress-bar-success');
        $(".step-two").addClass('progress-bar-inverse');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    }

    // Previous for step 3
    previouSecondSubmit() {
        $("#step-three").css('display', 'none');
        $(".step-three").removeClass('progress-bar-success');
        $(".step-three").addClass('progress-bar-inverse');
       // $(".step-two").addClass('progress-bar-success');
        //$(".step-two").removeClass('progress-bar-inverse');
       // $("#step-twos").css('display', 'block');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    }



    // Add Targets For The Customer
    addtargetList() {
        $("#remove-role-button").css('display', 'block');
        this.targetList.push(
            {
                buCode: "",
                customerTargets: [

                    {
                        month: "1",
                    },
                    {
                        month: "2",
                    },
                    {
                        month: "3"
                    },
                    {
                        month: "4",
                    },
                    {
                        month: "5",
                    },
                    {
                        month: "6"
                    },
                    {
                        month: "7",
                    },
                    {
                        month: "8",
                    },
                    {
                        month: "9"
                    },
                    {
                        month: "10",
                    },
                    {
                        month: "11",
                    },
                    {
                        month: "12"
                    }


                ]
            }
        );
    }

    // Remove Targets
    removetargetList(e) {
        this.targetList.splice(e, 1);
    }


    // Get Channel List for Select Channel
    onChangeGetList() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].subChannelCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].subArea = '';
        }
        this.httpService.postRequest<CreateCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.getList = data.rsBody.resulats;
                    this.custDetials.customerRelationship[0].branchDetailsFK = '';
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    // Subchannel
                    if (this.getList.subChannel != null && this.getList.subChannel.length > 0) {
                        this.subChannelList = this.getList.subChannel;
                        $("#customerSubChannel").css('display', 'block');
                    }
                    else {
                        $("#customerSubChannel").css('display', 'none');
                    }
                    if (this.getList.regionList != null && this.getList.regionList.length > 0) {
                        this.regionList = this.getList.regionList;
                        $("#customerRegion").css('display', 'block');
                    }
                    else {
                        $("#customerRegion").css('display', 'none');
                    }
                    // Subarea
                    if (this.getList.subArea != null && this.getList.subArea.length > 0) {
                        this.subAreaData = this.getList.subArea;
                        $("#subarea").css('display', 'block');
                    }
                    else {
                        $("#subarea").css('display', 'none');
                    }
                    // Branch List
                    if (this.getList.branchList != null && this.getList.branchList.length > 0) {
                        this.branchListData = this.getList.branchList;
                        $("#customerBranch").css('display', 'block');
                    }
                    else {
                        $("#customerBranch").css('display', 'none');
                    }
                    if (this.getList.areaList != null && this.getList.areaList.length > 0) {
                        this.responseAreaData = this.getList.areaList;
                        $("#customerArea").css('display', 'block');
                    }
                    else {
                        $("#customerArea").css('display', 'none');
                    }
                    if (this.getList.employeeBaseDetails != null && this.getList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        if (data.rsBody.errorMessage != null) {
                            $("#customerEmpMessage").css('display', 'block');
                            this.errorMessage = data.rsBody.errorMessage;
                            $("#submit-button").addClass('ng-hide');
                        }
                        else {
                            $("#customerEmpMessage").css('display', 'none');
                            $("#submit-button").removeClass('ng-hide');
                        }
                    }


                }
            });
    }
    
    kamEmployeeList() {
        $("#customerArea").css('display', 'none');
    }

    // Get SubChannel List 
    
    onChangeGetSubChannelList() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].region = '';
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
        }
        this.httpService.postRequest<CreateCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.getList = data.rsBody.resulats;
                    this.custDetials.customerRelationship[0].branchDetailsFK = '';
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    if (this.getList.areaList != null && this.getList.areaList.length > 0) {
                        this.responseAreaData = this.getList.areaList;
                        $("#customerArea").css('display', 'block');
                    }
                    else {
                        $("#customerArea").css('display', 'none');
                    }
                    if (this.getList.branchList != null && this.getList.branchList.length > 0) {
                        this.branchListData = this.getList.branchList;
                        $("#customerBranch").css('display', 'block');
                    }
                    else {
                        $("#customerBranch").css('display', 'none');
                    }
                    if (this.getList.regionList != null && this.getList.regionList.length > 0) {
                        this.regionList = this.getList.regionList;
                        $("#customerRegion").css('display', 'block');
                    }
                    else {
                        $("#customerRegion").css('display', 'none');
                    }
                    if (this.getList.employeeBaseDetails != null && this.getList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        if (data.errorMessage != null && data.errorMessage != "") {
                            $("#customerEmpMessage").css('display', 'block');
                            this.errorMessage = data.errorMessage;
                            $("#submit-button").addClass('ng-hide');
                        }
                        else {
                            $("#customerEmpMessage").css('display', 'none');
                            $("#submit-button").removeClass('ng-hide');
                        }
                    }
                    $("#shipTOPartyCode").css('display', 'none');

                }
            });
    }

    
    // Get Employee List Usin Brnch
    
    getEmployeeBasedOnSubArea() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {

            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest<CreateCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    this.getEmployeeList = data.rsBody.resulats;
                    this.shipToRequire = data.shipToRequire;
                    if (this.getEmployeeList.employeeBaseDetails != null && this.getEmployeeList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getEmployeeList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        $("#customerEmpMessage").css('display', 'block');
                        this.errorMessage = data.errorMessage;
                        $("#submit-button").addClass('ng-hide');
                    }
                    if (this.shipToRequire != null) {
                        if (this.shipToRequire.keyName == "MTSE" && this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                            //this.shipToPartyCodes = [{}];
                            this.lengthdata = this.shipToPartyCodes.length;
                            $("#shipTOPartyCode").css('display', 'block');
                        }
                        else {
                            $("#shipTOPartyCode").css('display', 'none');
                        }
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                    $("#customerEmpKAMDetails").css('display', 'none');

                }
            });
    }
    
    // Get Employee List Usin Brnch
   
    getEmployeeSelectedArea() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest<CreateCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    this.getEmployeeList = data.rsBody.resulats;
                    this.shipToRequire = data.shipToRequire;
                    if (this.getEmployeeList.employeeBaseDetails != null && this.getEmployeeList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getEmployeeList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#submit-button").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        $("#customerEmpMessage").css('display', 'block');
                        this.errorMessage = data.errorMessage;
                        $("#submit-button").addClass('ng-hide');
                    }
                    if (this.shipToRequire != null) {
                        if (this.shipToRequire.keyName == "MTSE" && this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                            //this.shipToPartyCodes = [{}];
                            this.lengthdata = this.shipToPartyCodes.length;
                            $("#shipTOPartyCode").css('display', 'block');
                        }
                        else {
                            $("#shipTOPartyCode").css('display', 'none');
                        }
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                    $("#customerEmpKAMDetails").css('display', 'none');

                }
            });
    }
   
    // Add Ship To Party Codes
    addShipToCode() {
        this.shipToPartyCodes.push({});
        this.lengthdata = this.shipToPartyCodes.length;
    }

    // Remove Ship To party Code
    removeShipToCode(e) {
        this.shipToPartyCodes.splice(e, 1);
        this.lengthdata = this.shipToPartyCodes.length;
    }


    // Create New Customer Details
  
    createCustomerSubmit() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].customerGeoDetailsFK = this.customerBaseDetailsFK;
            this.custDetials.customerRelationship[i].shipToPartyCodes = this.shipToPartyCodes;
            if (this.custDetials.customerRelationship[i].branchDetailsFK == "") {
                this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            }
            if (this.custDetials.customerRelationship[i].employeeRoleFK == "") {
                this.custDetials.customerRelationship[i].employeeRoleFK = 0;
            }
        }
        this.custDetials.customerDeatils = this.customerDetialsData;
        //this.custDetials.customerTargets = this.targetData;
        this.httpService.postRequest<CreateCustomerComponent>(this.custDetials, '../ManageCustomer/CreateCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.sucessResponseData = data.rsBody.customerDetails;

                   
                    this.router.navigate(['/AddKeyValue']);
                    // this.router.onSameUrlNavigation('reload');
                    this.router.onSameUrlNavigation ="reload";
                    $("#sucessPopup").modal();
                    //Utilities.transitionTo('create-customer', this);

                }
            });
    }

   
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
