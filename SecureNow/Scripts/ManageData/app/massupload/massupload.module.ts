import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { setTimeout } from 'timers';
import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { MassUploadComponent } from './massupload.component';
import { MassUploadRoutingModule } from './massupload-routing.module';
import { FileSelectDirective } from 'ng2-file-upload';
@NgModule({
    declarations: [
         MassUploadComponent, FileSelectDirective
    ],
    imports: [
        CommonModule,
        MassUploadRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,        
        FormsModule,         
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [MassUploadRoutingModule],
    exports: []
})
export class MassUploadModule { }
