﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { FileUploader } from 'ng2-file-upload';
import { ModalService } from '../../service/modelservice';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'MassUpload';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class MassUploadComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private modal: ModalService,
        private route: ActivatedRoute) {
    }
    keyValueData: any = {};
    requestData: any = {};
    targetType: any = {};
    employeeMaster: any = {};
    incentiveData: any = {};
    ChannelList: any = [];
    year: string = '';
    dataSourceFileName: string = '';
    subChannelList: string = '';
    roleList: string = '';
    actualTypeList: string = '';
    weightageList: string = '';
    uploader: FileUploader = new FileUploader({ url: '../ManageUploadData/UploadMasterSheet' });
    incentive: any = [];
    getNextEmployeeList: any;
    pageNumber: any;
    incentivList: any = {};
    listData: any = [];
    id: any;
    getPage: any;
    yearData: any = [];
    monthData: any = [];
    quarterData: any = [];
    responseData: any = {};
    downloadFile: any = {};
    error: any = {};
    filename: any;
    result: any;


    ngOnInit() {
        this.getNextEmployeeList = 0;
        this.getPage = 0;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest<MassUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {

                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });


        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<MassUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.yearData = data.rsBody.resulats;
            });

        //getMonth List			
        this.getMonthList();

    }

    getMonthList() {
        this.keyValueData = {};
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest<MassUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.quarterData = data.rsBody.resulats;
            });

        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<MassUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                this.monthData = data.rsBody.resulats;
            });

        this.keyValueData.keyType = "TARGETKEY";
        this.httpService.postRequest<MassUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                this.targetType = data.rsBody.resulats;
            });
    }

    ngOnEmployeeMasterSubmit() {
        this.employeeMaster.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest<MassUploadComponent>(this.employeeMaster, '../ManageUploadData/UploadMasterData', true).subscribe(
            data => {
                if (data == undefined) {
                    var id = data.rsBody.data;
                }
                else {
                    var id = data.rsBody.data;
                }
                this.responseData.id = id;
                this.ngOnCheckStatus(this.responseData);
            });
    }

    ngOnCheckStatus(inputData)
    {
        this.httpService.postRequest<MassUploadComponent>(inputData, '../ManageUploadData/GetUploadStatus', true).subscribe(
            data => {

                this.responseData = data.rsBody;
                if (this.responseData != "" && this.responseData != null) {
                    this.employeeMaster = {};
                    this.router.navigate(['mass-upload']);
                    this.modal.open('sucessPopup')
                    this.ngOnInit();
                }
                else {                   
                    this.ngOnCheckStatus(inputData);
                }
            });
    }

    ngOnDeleteRequest(target)
    {
        this.employeeMaster = this.listData[target];
        this.employeeMaster.requestType = 'MassUploadForSoldToCode';
        this.httpService.postRequest<MassUploadComponent>(this.employeeMaster, '../ManageUploadData/CancelUploadRequest', true).subscribe(
            data => {
                this.error = data.rsBody;
                this.modal.open('error');
                this.ngOnInit();
            });
    }

    ngOnGetNext() {
        this.getPage = this.getPage + 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest<MassUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {                
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    ngOnGetPrevious() {
        this.getPage = this.getPage - 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "MassUploadForSoldToCode";
        this.httpService.postRequest<MassUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    modalclose(id) {
        this.modal.close(id);
    }

    ngAfterViewInit() {

    }

    fileSelect()
    {
        $('#imageUpload').click();
    }			

    imageUpload()
    {
        this.uploader.uploadAll();
        //Get response
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) =>
        {
	        var responsePath = JSON.parse(response);
	        {
                this.employeeMaster.dataSourceFileName = responsePath.rsBody;
		    };
	    }
    }

    ngOnTemplateDownload() {

        var triggerClickk = setTimeout(function () {
            clearTimeout(triggerClickk);
            $('#downloadReport')[0].click();
        });
    }

    ngOnDownloadSourceReport(data) {
        this.downloadFile = data;
        $('#DownloadSourcePath').attr('href', '../Comman/DownloadOpSourceReport?name=' + this.downloadFile);
        $('#DownloadSourcePath')[0].click();
    }

    ngOnDownloadErrorReport(getData) {
        this.downloadFile = getData;
        this.httpService.postRequest<MassUploadComponent>(this.downloadFile, '../Comman/CheckFileExistOrNot', true).subscribe(
            data => {
                this.result = data;
                if (this.result == "NotExist") {
                    this.modal.open('notFound');
                    this.filename = "";
                }

                else {
                    $('#DownloadErrorPath').attr('href', '../Comman/DownloadOpErrorReport?name=' + this.downloadFile);
                    $('#DownloadErrorPath')[0].click();
                }
            });   
    }

    ngOnDestroy() {
    }


}
