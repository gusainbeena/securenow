import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MassUploadComponent } from './massupload.component';

const routes: Routes = [   
    { path: '', component: MassUploadComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MassUploadRoutingModule { }
