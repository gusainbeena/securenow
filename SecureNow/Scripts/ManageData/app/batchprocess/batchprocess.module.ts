import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { BatchProcessRoutingModule } from './batchprocess-routing.module';
import { BatchProcessComponent } from './batchprocess.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
    declarations: [
        BatchProcessComponent, FileSelectDirective
    ],
    imports: [
        CommonModule,
        BatchProcessRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,        
        FormsModule,         
        HttpModule,
        NgbModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [BatchProcessComponent],
    exports: []
})
export class BatchProcessModule { }
