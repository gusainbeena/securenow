import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BatchProcessComponent } from './batchprocess.component';

const routes: Routes = [
    { path: '', component: BatchProcessComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BatchProcessRoutingModule { }
