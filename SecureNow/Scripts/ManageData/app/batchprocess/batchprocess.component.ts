﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { FileUploader } from 'ng2-file-upload';
import { ModalService } from '../../service/modelservice';
import { parse } from 'url';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'BatchProcess';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class BatchProcessComponent {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    time = { hour: 13, minute: 30 };
    meridian = true;
    public dashboardData: Object = {};
    public request: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private modal: ModalService,
        private route: ActivatedRoute) {
    }
    id: any;
    getPage: any;
    batchId: any;
    index: any;
    public day: string = '';
    public month: string = '';
    public year: string = '';
    currentOU: any;
    selectOU: any;
    responseData: any = [];
    batchRequests: any = [];
    batchParameters: any = [];
    bprocess: any = [];
    listData: any = [];
    uploadedBatch: any = {};
    requestData: any = {};
    batchDate: any = {};
    bprocessDTO: any = [];
    allOU: any = [];
    timer = {
        hour: <string>null,
        minute: <string>null,
        second: <string>null,
    };
    public hour: any = 0;
    ngOnInit() {
        this.getPage = 0;
        this.httpService.postRequest<BatchProcessComponent>({}, '/BatchProcess/GetUserDetails', true).subscribe(
            data => {
                if (data.rsBody.result = "success") {
                    if (data.rsBody.bprocess.length == 0) {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display');
                        this.batchRequests = data.rsBody.batchRequests;
                    }
                    
                    // next and previous 
                    if (this.batchRequests.length < 5) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getPage > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getPage == 0) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide")
                    }
                    this.batchDate.year = data.rsBody.batchDate[0];
                    this.batchDate.month = data.rsBody.batchDate[1];
                    this.batchDate.day = data.rsBody.batchDate[2];
                    //this.batchParameters = data.rsBody.batchParameters;
                    this.bprocess = data.rsBody.bprocess;
                    this.currentOU = data.rsBody.currentOU;
                    this.allOU = data.rsBody.allOU;
                    if (this.allOU.length != 0) {
                        $("#ou").removeClass("ng-hide");
                    }
                    this.selectOU = data.rsBody.selectedOU;
                }

            });


    }

    selectOUfunction(item) {
        this.selectOU = item;
    }
    go() {
        this.year = this.batchDate.year.toString();
        this.month = this.batchDate.month.toString();
        this.day = this.batchDate.day.toString();
        if (this.month.length == 1) {
            this.month = "0" + this.month;
        }
        if (this.day.length == 1) {
            this.day = "0" + this.day;
        }
        if (this.selectOU == null || this.selectOU == undefined) {
            this.selectOU = "INDIA";
            this.uploadedBatch.selectOU = this.selectOU;
        }
        else {
            this.uploadedBatch.selectOU = this.selectOU;
        }
        
        this.uploadedBatch.batchDate = this.year + "-" + this.month + "-" + this.day;
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '/BatchProcess/FetchData', true).subscribe(
            data => {
                if (data.rsBody.result = "success") {
                    this.bprocess = data.rsBody.bprocess;
                    if (this.bprocess.length== 0) {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display', 'block');
                    }
                }

            });

    }
    
    rescheduleBatch(e) {
        this.index = e;
        this.batchId = this.batchRequests[e].id;
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '/BatchProcess/RescheduleBatch',true).subscribe(
         data => {
             if (data.rsBody.result = "success") {
                 this.request = data.rsBody.request;
                 var time = this.request['scheduleTime'];
                 //time = this.convert(time);
                 var ampm = time.slice(5);
                 this.hour = +(time.slice(0, -6));
                 var minute = time.slice(2, -3);
                 if (ampm == "PM") {
                     this.hour = this.hour + 12;
                 }
                 this.timer.hour = this.hour.toString();
                 this.timer.minute = minute;
                 this.timer.second = "0";
                 var id = this.request['id'];
                 this.batchRequests[this.index].scheduleTime = this.timer;
                 $("#scheduleTimePopup").modal();
                 
             }

            });

    }
    resetRunTime() {
        var hour = this.time.hour;
        var ampm = "AM";
        if (hour > 12) {
            hour = hour - 12;
            ampm = "PM";
        }
        if (hour == 0) {
            hour = 12;
        }
        this.time.hour = hour;
        this.uploadedBatch.scheduleTime = this.time.hour + ":" + this.time.minute + " " + ampm;
        //this.uploadedBatch.scheduleTime = this.time;
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '/BatchProcess/ResetTime', true).subscribe(
            data => {
                if (data.rsBody.result = "success") {
                    this.request = data.rsBody.request;
                    var time = this.request['scheduleTime'];
                    //time = this.convert(time);
                    var ampm = time.slice(5);
                    this.hour = (time.slice(0, -6));
                    var minute = time.slice(2, -3);
                    if (ampm == "PM") {
                        this.hour = this.hour + 12;
                    }
                    this.timer.hour = this.hour.toString();
                    this.timer.minute = minute;
                    this.timer.second = "0";
                    var id = this.request['id'];
                    this.batchRequests[this.index].scheduleTime = this.timer;
                    //this.batchRequests[this.index].scheduleTime = this.request['scheduleTime'];


                    //$("#scheduleTimePopup").modal();
                    this.ngOnInit();
                }

            });
    }
    getNextBatch() {
        this.getPage = this.getPage + 1
        this.uploadedBatch.page = this.getPage;
        //this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '../BatchProcess/BatchDataList', true).subscribe(
            data => {
                this.bprocessDTO = data.rsBody.bprocessDTO;
                $("#mainRow").css('display', 'none');
                $("#backRow").css('display', 'flex');
                if (this.bprocessDTO == null) {
                    $("#table_found_id").css('display', 'none');
                    $("#table_not_found_id").css('display', 'block');
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#table_found_id").css('display', 'block');
                    $("#table_not_found_id").css('display', 'none');
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'none');
                }
                // next and previous 
                if (this.bprocessDTO.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });
    }

    getPreviousBatch() {
        if (this.getPage == 0) {
            $('#example1_previous').prop('disabled', true);
        }
        this.getPage = this.getPage - 1
        this.uploadedBatch.page = this.getPage;
        this.uploadedBatch.id = this.batchId;
        //this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '../BatchProcess/BatchDataList', true).subscribe(
            data => {
                this.bprocessDTO = data.rsBody.bprocessDTO;
                $("#mainRow").css('display', 'none');
                $("#backRow").css('display', 'flex');
                if (this.bprocessDTO == null) {
                    $("#table_found_id").css('display', 'none');
                    $("#table_not_found_id").css('display', 'block');
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#table_found_id").css('display', 'block');
                    $("#table_not_found_id").css('display', 'none');
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'none');
                }
                // next and previous 
                if (this.bprocessDTO.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });
    }

    batchLogs(e) {
        this.batchId = this.batchRequests[e].id;
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '../BatchProcess/BatchLogs', true).subscribe(
            data => {
                if (data.rsBody.result == "success") {
                    if (data.rsBody.logsAvail == "error") {
                        $("#noLogFound").modal();
                    }
                }
            });
       

    } 

    runBatch(e) {
        this.batchId = this.batchRequests[e].id;
    }
    back() {
        $("#mainRow").css('display', 'flex');
        $("#backRow").css('display', 'none');
        $("#table_found_id").css('display', 'none');
        $("#row_not_found_id").css('display', 'none');
        $("#row_table_id").css('display', 'block');
    }

    Explore(e) {
        this.batchId = this.bprocess[e].id;
        this.uploadedBatch.id = this.batchId;
        if (this.selectOU == null || this.selectOU == undefined) {
            this.selectOU = "INDIA";
            this.uploadedBatch.selectedOU = this.selectOU;
        }
        else {
            this.uploadedBatch.selectedOU = this.selectOU;
        }
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '../BatchProcess/Explore', true).subscribe(
            data => {
                if (data.rsBody.result == "success")
                {
                    $("#mainRow").css('display', 'none');
                    $("#backRow").css('display', 'flex');
                    this.bprocessDTO = data.rsBody.bprocessDTO;
                    if (this.bprocessDTO.count != 0) {
                        $("#table_found_id").css('display', 'block');
                        $("#table_not_found_id").css('display', 'none');
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display', 'none');
                    }
                    else
                    {
                        $("#row_not_found_id").css('display', 'none');
                        $("#table_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                        $("#table_found_id").css('display', 'none');
                    }

                    // next and previous 
                    if (this.bprocessDTO.length <5 ){
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getPage > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getPage == 0) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide")
                    }
                }
                
            });
    }
    viewPreviousBatches(e) {
        this.batchId = this.batchParameters[e].id;
        var id = e;
        $("#innerTable").removeClass('d-none');
        this.uploadedBatch.id = this.batchId;
        this.httpService.postRequest<BatchProcessComponent>(this.uploadedBatch, '../BatchProcess/ListedBatch', true).subscribe(
            data => {
                if (data.rsBody.result == "success") {
                    this.batchRequests = data.rsBody.batchRequests;
                }
    });
    }
    ViewErros(e) {
        this.batchId = this.batchParameters[e].id;
        var id = e;
    }
    getNextBatchParameters() {
        this.getPage = this.getPage + 1
        this.requestData.page = this.getPage;
        
        this.httpService.postRequest<BatchProcessComponent>(this.requestData, '../BatchProcess/BatchParameterList', true).subscribe(
            data => {
               // this.batchRequests = data.rsBody.data;
                this.batchParameters = data.rsBody.batchParameters;
                this.currentOU = data.rsBody.currentOU;
                if (this.batchParameters == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.batchParameters.length < 8) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });
    }

    getPreviousBatchParameters() {
        if (this.getPage == 0) {
            $('#example1_previous').prop('disabled', true);
        }
        this.getPage = this.getPage - 1
        this.requestData.page = this.getPage;
        
        this.httpService.postRequest<BatchProcessComponent>(this.requestData, '../BatchProcess/BatchParameterList', true).subscribe(
            data => {
                this.batchParameters = data.rsBody.batchParameters;
                if (this.batchParameters == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.batchParameters.length < 8) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });
    }
}

