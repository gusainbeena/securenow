import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';


const routes: Routes = [      
    {
          path: 'access-control',
        loadChildren: './accesscontrol/accesscontrol.module#AccessControlModule'
    },
    {
        path: 'create-employee',
        loadChildren: './createemployee/createemployee.module#CreateEmployeeModule'
    },
    {
        path: 'view-employee',
        loadChildren: './viewemployee/viewemployee.module#ViewEmployeeModule'
    },
    {
        path: 'create-customer',
        loadChildren: './createcustomer/createcustomer.module#CreateCustomerModule'
    },
    {
        path: 'view-customer',
        loadChildren: './viewcustomer/viewcustomer.module#ViewCustomerModule'
    },
    {
        path: 'target-upload',
        loadChildren: './opupload/opupload.module#OpUploadModule'
    },
    {
        path: 'actual-upload',
        loadChildren: './actualupload/actualupload.module#ActualUploadModule'
    },
    {
        path: 'approve-actual-upload',
        loadChildren: './approveactualupload/approveactualupload.module#ApproveActualUploadModule'
    },
    {
        path: 'create-incentive',
        loadChildren: './createincentive/createincentive.module#CreateIncentiveModule'
    },
    {
        path: 'view-incentive',
        loadChildren: './viewincentive/viewincentive.module#ViewIncentiveModule'
    },
    {
        path: 'pay-slip',
        loadChildren: './payslip/payslip.module#PaySlipModule'
    },
    {
        path: 'overdue-upload',
        loadChildren: './overdueupload/overdueupload.module#OverdueUploadModule'
    },
    {
        path: 'mass-upload',
        loadChildren: './massupload/massupload.module#MassUploadModule'
    },
    {
        path: 'configure-actual-template',
        loadChildren: './configureactualtemplate/configureactualtemplate.module#ConfigureActualTemplateModule'
    },
    {
        path: 'configure-op-template',
        loadChildren: './configureoptemplate/configureoptemplate.module#ConfigureOpTemplateModule'
    },
    {
        path: 'configure-metadata',
        loadChildren: './configuremetadata/configuremetadata.module#ConfigureMetaDataModule'
    }, 
    {
        path: 'configure-meta-data',
        loadChildren: './configuremetadata/configuremetadata.module#ConfigureMetaDataModule'
    },
    {
        path: 'global-dashboard',
        loadChildren: './globaldashboard/globaldashboard.module#GlobalDashboardModule'
    },
    {
        path: 'my-dashboard',
        loadChildren: './mydashboard/mydashboard.module#MyDashboardModule'
    },
    {
        path: 'batch-process',
        loadChildren: './batchprocess/batchprocess.module#BatchProcessModule'
    },	
    {
        path: '',
        loadChildren: './globaldashboard/globaldashboard.module#GlobalDashboardModule'
    },    
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: "reload" })],
    exports: [
        RouterModule,
    ],
    declarations: []
})
export class AppRoutingModule { }
