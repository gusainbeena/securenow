﻿import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonModule } from "@angular/common";
import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { BaseContainer } from '../../BaseContainer';
import * as d3 from 'd3';
import { Http } from '@angular/http';
declare var $: any;


var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewEmployee';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class ViewEmployeeComponent implements OnInit, OnDestroy, AfterViewInit {   
    rsBody: any;
    responseData: any;
    searchByBranch: any = {};
    getNextEmployeeList: any;
    keyValueData: any = {};
    keyvalue: any = {};
    responseChannelData: any = [];
    monthData: any = [];
    updatedmonthData: any;
    gradeData: any = [];
    yearData: any = [];
    yearRecorded: any;
    pageDetail: any = {};
    employeeDetials: any = {
        employeeRole: [{
            yearRecorded: "", startMonth: "", endMonth: "", grade: "", channelCode: "", role: "", subChannelCode: "", region: "", area: "", subArea:""
        }]
    };
    getList: any = {};
    roleList: any = [];
    subChannelList: any = [];
    selectID: any;
    employeeDetails: any = [];
    employeeBaseDetails: any = [];
    responseDeleteRoleData: any = [];
    selectVal: any = {};
    regionList: any = [];
    areaList: any = [];
    subAreaList: any = [];
    requiredFlag: boolean = false;
    employeeRole: any = {};
    seprationRole: any = {};
    seprationData: any = {};
    selectedId: any = {};
    empRole: any = {};
    errorMessage: any = {};
    sucessResponseData: any = [];
    lastIndex: any = {};
    employeeGrade: any = { grade: "", startMonth: "", endMonth: "" };
    response: any = {};

    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute,
        private cdref: ChangeDetectorRef,
        private modal: ModalService) {
    }  

    ngOnInit() {
        $("#edit-h-id").css('display', 'block');
        $("#endMonth_id").css("display", "none");
        this.getNextEmployeeList = 0;

        this.httpService.postRequest<ViewEmployeeComponent>({}, '../ManageEmployee/GetEmployeeDetailsList',false).subscribe(
            data =>{
                this.responseData = data.rsBody.resulats;                
                this.ngOnGetChannel();
            }); 
    }

    //get channel list
    ngOnGetChannel()
    {
        //get month data
        this.keyValueData.keyType = "CHANNEL";
        this.keyValueData.keyName = "CHANNEL";
        this.httpService.postRequest<ViewEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
            data => {
                this.responseChannelData = data.rsBody.resulats;   
                this.ngOnMonth();
            });
    }

    //get month list
    ngOnMonth() {
        //get month data
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<ViewEmployeeComponent>(this.keyValueData, '../Comman/GetMonthKeyValueData',false).subscribe(
            data => {
                this.monthData = data.rsBody.resulats;
                this.updatedmonthData = Object.assign([],this.monthData);
                this.ngOnGrade();
            });
    }

    //get grade list
    ngOnGrade()
    {           
        //get grade data
        this.keyValueData.keyType = "GRADE";
        this.httpService.postRequest<ViewEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
            data => {
                this.gradeData = data.rsBody.resulats;    
                this.ngOnYear();
        });
    }

    //get year list
    ngOnYear()
    {
        //get year data
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<ViewEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
            data => {
                this.yearData = data.rsBody.resulats;
                this.yearRecorded = this.yearData[0].keyValue;
            });
    }   

    //Add box for add new grade
    ngOnAddnewGrade(targetIndex)
    {
        this.employeeGrade = { grade: "", startMonth: "", endMonth: "" };
        $("#gradeUpdateNew_" + targetIndex).css("display", "block");
        $("#addGradeButton_" + targetIndex).css("display", "none");
        $(".removeGradeButton_" + targetIndex).css("display", "block");

    }
    //remove box for grade
    ngOnRemovenewGrade(targetIndex) {
        $("#gradeUpdateNew_" + targetIndex).css("display", "none");
        $("#addGradeButton_" + targetIndex).css("display", "block");
        $(".removeGradeButton_" + targetIndex).css("display", "none");
    }

    ngOnSearchBranch() {
        this.httpService.postRequest<ViewEmployeeComponent>(this.searchByBranch, '../ManageEmployee/GetEmployeeDetailsList',true).subscribe(
            data => {
                this.responseData = data.rsBody.resulats;
                this.ngOnGetChannel();
            });
    }

    ngOnGetNextEmployee() {
        this.getNextEmployeeList = this.getNextEmployeeList + 1;
        this.pageDetail.pageNumber = this.getNextEmployeeList;
        this.httpService.postRequest<ViewEmployeeComponent>(this.pageDetail, '../ManageEmployee/NextAndPriviousEmployeeDetails',false).subscribe(
            data => {
                this.responseData = data.rsBody.resulats;
                this.ngOnGetChannel();
            });
    }

    ngOnGetPreviousEmployee() {
        this.getNextEmployeeList = this.getNextEmployeeList - 1;
        this.pageDetail.pageNumber = this.getNextEmployeeList;
        this.httpService.postRequest<ViewEmployeeComponent>(this.pageDetail, '../ManageEmployee/NextAndPriviousEmployeeDetails',false).subscribe(
            data => {
                this.responseData = data.rsBody.resulats;
                this.ngOnGetChannel();
            });
    }

    ngOnChangeGetList() {

        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].role = '';
            this.employeeDetials.employeeRole[i].region = '';
            this.employeeDetials.employeeRole[i].branchCode = '';
            this.employeeDetials.employeeRole[i].subChannelCode = '';
        }
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials.employeeRole, '../ManageEmployee/GetRoleAreaAndBranchData',false).subscribe(
        data => {
            this.getList = data.rsBody;
            this.roleList = this.getList.resulats;

            if (this.getList.subChannel.length > 0) {
                this.subChannelList = this.getList.subChannel;
                $("#employeeSubChannel").css('display', 'block');
            }
            else {
                $("#employeeSubChannel").css('display', 'none');
            }
            if (this.roleList != null) {
                $("#employeeRole").css('display', 'block');
            }
            else {
                $("#employeeRole").css('display', 'none');
            }
            $("#employeeBranch").css('display', 'none');
            $("#employeeRegion").css('display', 'none');
            $("#employeeArea").css('display', 'none');
        });
    }

    ngOnChangeLineOfBusinessGetList() {
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].role = '';
            this.employeeDetials.employeeRole[i].region = '';
            this.employeeDetials.employeeRole[i].branchDetailsFK = '';
        }
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials.employeeRole, '../ManageEmployee/GetRoleAreaAndBranchData',false).subscribe(
            data => {
                this.getList = data.rsBody;
                this.roleList = this.getList.resulats;
                $("#employeeRole").css('display', 'block');
            });
    }


    ngOnEditEmployeeDetailsSubmit(targetIndex) {
        this.selectID = this.responseData[targetIndex].id;
        this.ngOnGetEmployeeDetailsSubmit();
    }

    ngOnGetEmployeeDetailsSubmit() {
        this.employeeDetails = {};
        this.employeeDetails.id = this.selectID;
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetails, '../ManageEmployee/EditEmployeeDetails',true).subscribe(
            data => {
                this.employeeBaseDetails = data.rsBody.employeeBaseDetail;
                this.employeeDetials.responseRoleData = data.rsBody.employeeRole;
                if (this.employeeBaseDetails.status == "RESIGNED") {
                    $(".enable-disable").prop('disabled', true);
                    $(".endMonth-enable").prop('disabled', true);
                    $("#update-basic-button").css('display', 'none');
                    $("#submit-basic-button").css('display', 'none');
                    $("#leaving-id").css('display', 'block');
                    $("#leaving-id").prop('disabled', 'true');
                    $("#endMonth-id").css('display', 'block');
                    $("#endMonth-id").prop('disabled', true);
                    $("#add-role-button").css('display', 'none');
                    this.employeeDetials.responseRoleData.status = "RESIGNED";
                    if (this.employeeBaseDetails.dolStr != null && this.employeeBaseDetails.dolStr != "") {
                        var strAry = this.employeeBaseDetails.dolStr.split("-");
                        this.employeeBaseDetails.endMonth = strAry[1].charAt(strAry[1].length - 1);
                    }
                }
                else {
                    $(".enable-disable").prop('disabled', false);
                    $(".endMonth-enable").prop('disabled', false);
                    $("#update-basic-button").css('display', 'block');
                    $("#submit-basic-button").css('display', 'none');
                    $("#leaving-id").css('display', 'block');
                    $("#endMonth-id").css('display', 'block');
                    $("#leaving-id").prop('disabled', 'false');
                    $("#endMonth-id").prop('disabled', 'false');
                    $("#add-role-button").css('display', 'block');
                    this.employeeDetials.responseRoleData.status = "ACTIVE";
                }

                $("#view-details-id").css('display', 'none');
                $("#edit-details-id").css('display', 'block');
                $("#role-display-details-id").css('display', 'block');             

                for (var i = 0; i < this.employeeDetials.responseRoleData.length; i++) {
                    if (this.employeeDetials.responseRoleData[i].requiredData > 0) {
                        if (this.employeeDetials.responseRoleData[i].requiredData.indexOf("SUBCHANNELREQUIRED") > -1) {
                            $("#employeeSubChannel" + i).css('display', 'block');
                        }
                        else {
                            $("#employeeSubChannel" + i).css('display', 'none');
                        }

                        if (this.employeeDetials.responseRoleData[i].requiredData.indexOf("REGIONREQUIRED") > -1) {
                            $("#employeeRegion" + i).css('display', 'block');
                        }
                        else {
                            $("#employeeRegion" + i).css('display', 'none');
                        }
                        if (this.employeeDetials.responseRoleData[i].requiredData.indexOf("AREACODEREQUIRED") > -1) {
                            $("#employeeArea" + i).css('display', 'block');
                        }
                        else {
                            $("#employeeArea" + i).css('display', 'none');
                        }
                        if (this.employeeDetials.responseRoleData[i].requiredData.indexOf("SUBAREACODEREQUIRED") > -1) {
                            $("#employeeSubArea" + i).css('display', 'block');
                        }
                        else {
                            $("#employeeSubArea" + i).css('display', 'none');
                        }
                    }
                }
            });
    }

    ngOnAddRoleSubmit() {
        this.ngOnEmployeeRoleDetailsSubmit();
    }

    ngOnEmployeeRoleDetailsSubmit() {
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].adpId = this.employeeBaseDetails.adpId;
            this.employeeDetials.employeeRole[i].doj = this.employeeBaseDetails.doj;
            this.employeeDetials.employeeRole[i].yearRecorded = this.employeeBaseDetails.yearRecorded;
        }
        this.employeeDetials.type = "update";
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials, '../ManageEmployee/CreateEmployeeDetails',true).subscribe(
            data => {
                this.employeeDetials.responseRoleData = data.rsBody.sucess;
                this.modal.open('updateSucessPopup');
                //$scope.employeeDetials.employeeRole = {};
                this.employeeDetials.employeeRole = [{}];
                $("#createRoleDetails").css('display', 'none');
                $("#add-role-button").css('display', 'block');
                $("#remove-role-button").css('display', 'none');
                this.employeeDetials.responseRoleData.status = "ACTIVE";
            });
    }

    ngOnUpdateRoleSubmit() {

        this.modal.open("responseDataPopup");
        this.ngOnUpdateRoleDetailsSubmit('refresh');
    }
    ngOnUpdateRoleDetailsSubmit(argArr) {
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials, '../ManageEmployee/UpdateEmployeeRoleDetails',true).subscribe(
            data => {
                this.responseDeleteRoleData = data.rsBody.retData;
                if (this.responseDeleteRoleData.length > 0) {
                    this.modal.open('customerDetailsForUpdateMonth1');

                }
                else {
                    this.modal.open('sucessPopup');
                    this.employeeDetials.responseRoleData.status = "RESIGNED";
                }
            });
    }

    ngOnGetRequiredField(type) {

        this.ngOnChangeGetListInfo(type);
    }

    ngOnChangeGetListInfo(type) {

        this.selectVal = type;
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].type = type;
        }
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials.employeeRole, '../ManageEmployee/GetRelatedParam',true).subscribe(
            data => {
                if (data.rsBody.type == "ROLE") {
                    this.roleList = data.rsBody.result;
                    if (this.roleList != null) {
                        $("#employeeRole").css('display', 'block');
                    }
                    else {
                        $("#employeeRole").css('display', 'none');
                    }
                }
                $("#teritoryCodeData").css('display', 'none');
                $("#employeeSubChannel").css('display', 'none');
                $("#employeeRegion").css('display', 'none');
                $("#employeeArea").css('display', 'none');
                $("#employeeSubArea").css('display', 'none');
                this.requiredFlag = false;
                if (data.rsBody.result.length > 0) {
                    if (data.rsBody.result.indexOf("SUBCHANNELREQUIRED") > -1) {
                        $("#employeeSubChannel").css('display', 'block');
                        this.subChannelList = data.rsBody.paramValue.subChannelList;
                        if (this.selectVal == "") {
                            this.regionList = [];
                            this.areaList = [];
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("REGIONREQUIRED") > -1) {
                        $("#employeeRegion").css('display', 'block');
                        if (this.selectVal == "SUBCHANNELREQUIRED") {
                            this.regionList = data.rsBody.paramValue.regionList;
                            this.areaList = [];
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("AREACODEREQUIRED") > -1) {
                        $("#employeeArea").css('display', 'block');
                        $("#teritoryCodeData").css('display', 'block');
                        if (this.selectVal == "REGIONREQUIRED" || this.selectVal == "SUBCHANNELREQUIRED") {
                            this.areaList = data.rsBody.paramValue.areaList;
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("SUBAREACODEREQUIRED") > -1) {
                        $("#employeeSubArea").css('display', 'block');
                        $("#teritoryCodeData").css('display', 'block');
                        if (this.selectVal == "AREACODEREQUIRED") {
                            this.subAreaList = data.rsBody.paramValue.subAreaList;
                        }
                    }
                }
            });
    }

    ngOnAddEmployeeRoleDetails() {
        $("#remove-role-button").css('display', 'block');
        $("#createRoleDetails").css('display', 'block');
        $("#add-role-button").css('display', 'none');
        $("#addNewRole").css('display', 'block');
        $("#role-list-id").css('display', 'none');
        $("#employeeBranchs").css('display', 'none');
        $("#employeeRegions").css('display', 'none');
        $("#employeeAreas").css('display', 'none');
        $("#employeeRoles").css('display', 'none');
        // Create Array On Click
        this.employeeDetials.employeeRole = {};
        this.employeeDetials.employeeRole = [{
            yearRecorded: this.yearRecorded,
        }];
        $("#remove-role-button").css('display', 'block');
        $('html, body').animate({ scrollTop: 550 }, 'slow');
        return false;
    }

    ngOnRemoveEmployeeRoleDetails() {
        $("#remove-role-button").css('display', 'none');
        $("#add-role-button").css('display', 'block');
        $("#createRoleDetails").css('display', 'none');
        this.employeeDetials.employeeRole = [];
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }

    ngOnUpdateEmployeeBasicDetailsSubmit() {
        this.requiredFlag = true;
        this.httpService.postRequest<ViewEmployeeComponent>((this.employeeBaseDetails), '../ManageEmployee/UpdateEmployeeBaseDetails',true).subscribe(
            data => {
                var response = data.rsBody.exceptionBlock.msg.validationException;
                if (response.email == "invalid email address") {
                   // $('#errorPopup').bPopup();
                    this.modal.open('updateErrorPopup')
                    
                }
                else if (response.password == "value is required") {
                    this.modal.open('updateErrorPopup')
                }
                else if (response == "success") {
                    if (this.employeeBaseDetails.status != "ACTIVE") {
                       
                    }
                    this.modal.open('updateSucessPopup');
                }
            });
    }

    ngOnSeprationYesSubmit() {
        this.selectID = this.employeeRole;
        this.ngOnSeprationEmployeeRole('refresh');
    }

    ngOnSeprationNoSubmit() {
        this.selectID = this.employeeRole;
        this.ngOnSeprationEmployeeRole('refresh');
    }
    

    ngOnSeprationEmployeeRole(argArr) {
        this.modal.open("showCustRelationship1");
        this.seprationRole = this.selectID;
        this.httpService.postRequest<ViewEmployeeComponent>(this.seprationRole, '../ManageEmployee/SeprationEmployeeRole',true).subscribe(
            data => {
                this.seprationData = data.rsBody;
                $("#update-basic-button").css('display', 'none');
                $("#sucess-span").css('display', 'block');
                $(".endMonth-enable").prop('disabled', true);
            });
    }

    ngOnClickRoleSubmit(targetIndex) {
        this.selectedId = this.employeeDetials.responseRoleData[targetIndex].id;
        this.modal.open('deleteEmployeeRolePopup1');
    }

    ngOnDeleteSubmit() {
        this.selectID = this.selectedId;
        this.modal.open("deleteEmployeeRolePopup1");
        this.ngOnDeleteEmployeeRole('refresh');
    }

    ngOnDeleteEmployeeRole(argArr) {
        this.empRole.id = this.selectID;
        this.httpService.postRequest<ViewEmployeeComponent>(this.empRole, '../ManageEmployee/DeleteEmployeeRole',true).subscribe(
            data => {
                this.responseDeleteRoleData = data.rsBody.retVal;
                if (this.responseDeleteRoleData.length > 0) {
                    this.modal.open('customerDetails1');

                }
                else if (data.rsBody.errorMessage != null) {
                    this.errorMessage = data.rsBody.errorMessage;
                    this.modal.open('crSucessPopup');
                }
                else {
                    for (var i = 0; i < this.employeeDetials.responseRoleData.length; i++) {
                        if (this.employeeDetials.responseRoleData[i].id == this.selectID) {
                            this.employeeDetials.responseRoleData.splice(i, 1);
                            break;
                        }
                    }

                }
            });
    }

    ngOnReginedSubmit() {
        var inputData = this.employeeBaseDetails.status;
        if (inputData == "RESIGNED") {
            $("#add-role-button").css('display', 'none');
            $("#role-display-details-id").css('display', 'none');
            $(".enable-disable").prop('disabled', true);
            $(".role-endMonth-enable").prop('disabled', true);
            $("#endMonth_id").css("display", "block");
        }
        else {
            $("#add-role-button").css('display', 'block');
            $("#role-display-details-id").css('display', 'block');
            $(".enable-disable").prop('disabled', false);
            $("#endMonth_id").css("display", "none");
        }
    }

    ngOnCreateVacantRoleSubmit() {
        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeDetials, '../ManageEmployee/CreateVacantRole',true).subscribe(
            data => {
                this.sucessResponseData = data;
                this.modal.open('updateSucessPopup');

                $("#Employeebasicdetail").removeClass('active');
                $("#AddRole").removeClass('active');
                $("#BasicDiv").removeClass('active');
                $("#NewRoleDiv").removeClass('active');

                $("#Role").addClass('active');
                $("#RoleDiv").addClass('active');

                this.ngOnGetEmployeeDetailsSubmit();
            });
    }

    ngOnEmployeeGradeSubmit(index) {
        this.lastIndex = index;
        this.employeeGrade.employeeRoleFK = this.employeeDetials.responseRoleData[index].id;
        this.employeeGrade.endMonth = this.employeeDetials.responseRoleData[index].endMonth;

        this.httpService.postRequest<ViewEmployeeComponent>(this.employeeGrade, '../ManageEmployee/UpdateEmployeeGrades',true).subscribe(
            data => {
                this.response = data.rsBody;
                this.employeeGrade = [];
                this.ngOnGetEmployeeDetailsSubmit();
            });

    }

    ngOnUpdateEmployeeRoleSubmit()
    {
        if (this.employeeDetials.responseRoleData.length > 0) {
            this.modal.open('responseDataPopup');
        }
    }    

    ngOnModalClose(id) {
        this.modal.close(id);
    }
    ngOnEmailMeSubmit() {

    }

    ngOnToggleDiv() {

    }

    ngAfterViewInit()
    {
        
    }

    ngOnDestroy() {

    }
}
