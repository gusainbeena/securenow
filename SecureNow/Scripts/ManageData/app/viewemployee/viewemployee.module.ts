import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ViewEmployeeRoutingModule } from './viewemployee-routing.module';
import { ViewEmployeeComponent } from './viewemployee.component';
import { TreeviewModule } from 'ngx-treeview';
@NgModule({
    declarations: [
        ViewEmployeeComponent
    ],
    imports: [
        CommonModule,
        ViewEmployeeRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        TreeviewModule.forRoot(),
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ViewEmployeeComponent],
    exports: []
})
export class ViewEmployeeModule { }
