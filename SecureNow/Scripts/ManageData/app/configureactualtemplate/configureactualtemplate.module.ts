import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ConfigureActualTemplateRoutingModule } from './configureactualtemplate-routing.module';
import { ConfigureActualTemplateComponent } from './configureactualtemplate.component';
import { TreeviewModule } from 'ngx-treeview';
@NgModule({
    declarations: [
        ConfigureActualTemplateComponent
    ],
    imports: [
        CommonModule,
        ConfigureActualTemplateRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        TreeviewModule.forRoot(),
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ConfigureActualTemplateComponent],
    exports: []
})
export class ConfigureActualTemplateModule
{ }
