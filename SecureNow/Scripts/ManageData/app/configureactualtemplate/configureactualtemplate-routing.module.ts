import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigureActualTemplateComponent } from './configureactualtemplate.component';

const routes: Routes = [
    { path: '', component: ConfigureActualTemplateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigureActualTemplateRoutingModule { }
