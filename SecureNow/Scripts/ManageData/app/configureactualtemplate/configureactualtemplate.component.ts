﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { forEach } from '@angular/router/src/utils/collection';




declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureActualTemplate';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class ConfigureActualTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    public isSpinner: boolean = true;
    public keyValueData: any = {};
    public metas: any = [];
    public meta: any = [];
    public fields: any = [];
    public field: any = [];
    public performance: any = [];
    public column: any = [];
    public id: any;
    public startMonth: any;
    public endMonth: any;
    public removeColumnId: any;
    public filepath: any;
    public fileName: any;
    public relatedKeyValue: any;
    public performanceSelection: any;
    public title: any;
    public newLabel: any;
    public selectColumn: any;
    public actualComponent: any = [];
    public performanceComponent: any = [];
    public salesHorizontal: any = [];
    public months: any = [];
    public columnData: any = {};
    public excelColumn: any = [];
    public columnsId: any = [];
    public finalExcel: any = [];
    public message: string;
    public columnList: any = [];
    public monthsList: any = [];
    public salesList: any = [];
    public performanceList: any = [];
    public roleHeirarchyCount: any;
    public _secureFileService: any = [];
    public currrentYear: any = [];
    public previousExcelColumn: any = [];
    public previousColumnsId: any = [];
    private editHeader: any = [{}];
    public dropdownSettings: any = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        enableCheckAll: false
    };
    ngOnInit() {
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelData').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');

        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.keyValueData, '../ManageTemplate/getActualTemplateAttributes', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.monthsList = datas.rsBody.monthsList;
                    this.actualComponent = datas.rsBody.actualComponent;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;     
                    this.performanceComponent = datas.rsBody.performanceComponent;
                    this.finalExcel = datas.rsBody.excelColumn;
                    this.salesHorizontal = datas.rsBody.salesHorizontal;
                    this.editHeader = datas.rsBody.excelColumn;

                }
            });

    }
    
    removeActualColumn(i) {

        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.columnData, '../ManageTemplate/removeActualColumn', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.columnData.excelColumn = datas.rsBody.excelColumn;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnData.finalExcel = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;
                }
            });

    }


    editLabel(i) {

        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        //this.columnData.finalExcel = this.excelColumn;
        //this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.columnData, '../ManageTemplate/editLabel', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.newLabel = datas.rsBody.label;
                    $("#editLabelPopup").modal();
                    //this.columnData.excelColumn = datas.rsBody.excelColumn;
                    //this.excelColumn = datas.rsBody.excelColumn;
                    //this.columnData.finalExcel = datas.rsBody.excelColumn;
                    //this.columnsId = datas.rsBody.columnsId;
                }
            });

    }


    addLabel(label,i) {

        this.removeColumnId = i;
        this.newLabel = label;
        //this.columnData.finalExcel = this.excelColumn;
        //this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.columnData, '../ManageTemplate/addLabel', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.newLabel = datas.rsBody.label;
                    $("#editLabelPopup").modal();
                    //this.columnData.excelColumn = datas.rsBody.excelColumn;
                    //this.excelColumn = datas.rsBody.excelColumn;
                    //this.columnData.finalExcel = datas.rsBody.excelColumn;
                    //this.columnsId = datas.rsBody.columnsId;
                }
            });

    }
    onItemSelectSales(item: any) {
        this.isSpinner = false;
    }

    onSelectAllSales(items: any) {

        this.isSpinner = false;
    }

    onDeSelectAllSales(items: any) {

        this.isSpinner = false;
    }

    onItemDeSelectSales(item: any) {
        this.isSpinner = false;
        var i = this.salesList.indexOf(item); // RETURNS 1.
        this.salesList.splice(i, 1);

    }

    onItemSelectPerformance(item: any) {
        this.isSpinner = false;
    }

    onSelectAllPerformance(items: any) {

        this.isSpinner = false;
    }

    onDeSelectAllPerformance(items: any) {

        this.isSpinner = false;
    }

    onItemDeSelectPerformance(item: any) {
        this.isSpinner = false;
        var i = this.performance.indexOf(item); // RETURNS 1.
        this.performance.splice(i, 1);

    }





    OnStartMonthSelect(i) {

        var start = i;
        this.startMonth = i;

    }
    OnEndMonthSelect(i) {

        var end = i;
        this.endMonth = i;

    }
    OnPerformanceSelect(i) {

        var end = i;
        this.performanceSelection = i;

    }
    editActualHeader() {
        $("#headerRow").css('display', 'contents');
    }
   
  
    generateTemplate() {
        this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
        this.columnData.startMonth = this.startMonth;
        this.columnData.endMonth = this.endMonth;
        this.columnData.performanceSelection = this.performance;
        this.columnData.salesList = this.salesList;
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.columnData, '../ManageTemplate/generateActualTemplate', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.message == "Month Error") {
                        this.message = "End month Should be greater than start month. Please Select Carefully";
                        this.title = "Select Month error";
                        
                        $("#errorPopup").modal()
                    }
                    if (datas.rsBody.message == "Actual Template for given period is already exist") {
                        this.title = "Actual Template Already Exist";
                        this.message = "Actual Template for given period is already exist";
                        this.previousExcelColumn = datas.rsBody.previousExcelColumn;
                        this.previousColumnsId = datas.rsBody.previousColumnId;
                        $("#previousTemplatePopup").modal();
                    }
                    if (datas.rsBody.message == "Please Select Start Month and End Month") {
                        this.title = "Select Month error";
                        this.message = "Please Select Start Month and End Month";
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "Please Select Performance Component") {
                        this.title = "Performance error";
                        this.message = "Please Select Performance Component";
                        $("#errorPopup").modal();
                        
                    }
                    if (datas.rsBody.message == "Empty") {
                        this.title = "Value Error";
                        this.message = "Please Fill Values";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    else {
                        this.excelColumn = datas.rsBody.excelColumn;
                        this.editHeader = Object.assign([], datas.rsBody.excelColumn);
                        this.columnData.finalExcel = datas.rsBody.excelColumn;
                        this.columnData.columnsId = datas.rsBody.columnsId;
                        this.finalExcel == datas.rsBody.excelColumn;
                        this.columnsId = datas.rsBody.columnsId;
                        $("#sampleTemplate").css('display', 'block');
                        $("#downTempBtn").css('display', 'block');                        
                        $("#addNewColumnListBtn").addClass("ng-hide");
                    }
                }
            });
    }


    DownloadTemplate() {
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }
        this.columnData.editHeader = Object.assign([], this.editHeader);
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.columnData, '../ManageTemplate/downloadActualTemplate', true).subscribe(
            datas => {

                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.msg == "duplicate") {
                        this.message = datas.rsBody.message;
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();

                    }
                    else {
                        this.columnData.filepath = datas.rsBody.filepath;
                        this.filepath = datas.rsBody.filepath;
                        this.fileName = datas.rsBody.fileName;
                        $("#successPopup").modal();
                    }

                }
            });
    }

    selectExcelColumn () {
        $('#columnSeletionPopup').modal();
    }

   

    ExportDataSetToExcel() {
        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        this.keyValueData.keyType = "ACTUAL";
        this.keyValueData.keyName = "Template";
        this.keyValueData.startMonth = this.startMonth;
        this.keyValueData.endMonth = this.endMonth;
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        this.keyValueData.editHeader = Object.assign([], this.editHeader);
        $('#ExportExcelDataBtn').css('display', 'none');

        if (this.startMonth != null || this.startMonth == "") {
            this.keyValueData.startMonth = this.startMonth;
        }
        else {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "") {
            this.keyValueData.endMonth = this.endMonth;
        }
        else {
            this.keyValueData.endMonth = 12;
        }
        this.httpService.postRequest<ConfigureActualTemplateComponent>(this.keyValueData, '../ManageTemplate/insertActualTemplate', true).subscribe(
            datas => {

                if (datas.rsBody.result == 'success') {


                    var path = this.filepath;
                    
                    $('#alertbottomleft').addClass('ng-hide');
                    $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName + '&path=' + this.filepath);
                    $('#ExportExcelData')[0].click();
                    $('#text').html('-Select Month-');
                    $("#collapse1").collapse('hide');
                    $('#collapse1 ul li input[type=checkbox]').attr('checked', false);

                    this.excelColumn = datas.rsBody.excelColumn;
                    $('#ExportExcelData').css('display', 'none');
                    $('#downTempBtn').css('display', 'none');
                    $('#ExportExcelDataBtn').css('display', 'none');
                    $('#addNewColumnBtn').css('display', 'none');
                    $('#ExportExcelData').attr('disabled', true);
                    $('#addNewColumnBtn').attr('disabled', true);
                    $('#afterDownload').css('display', 'table'); 
                    $('#beforeDownload').css('display', 'none'); 
                }
            });
    }

   
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
