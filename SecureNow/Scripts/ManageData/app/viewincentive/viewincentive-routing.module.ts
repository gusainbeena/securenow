import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewIncentiveComponent } from './viewincentive.component';

const routes: Routes = [
    { path: '', component: ViewIncentiveComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ViewIncentiveRoutingModule { }
