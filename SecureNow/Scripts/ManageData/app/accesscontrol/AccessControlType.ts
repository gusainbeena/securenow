﻿export interface IAccessControl {
    selectedOperationalUnits: Array<string>;
    selectedPermissions: Array<string>;
    selectedFunctions: Array<string>;
    profile: Array<AccessControl>;
    dataList: any;
    forAllOrNot: boolean;
    user: any;
    userOrRoleBase: boolean;
    userLevelAccess: string;
}

export class AccessControl implements IAccessControl {
    selectedOperationalUnits: string[];
    selectedPermissions: string[];
    selectedFunctions: string[];
    dataList: any;
    profile: Array<AccessControl>;
    forAllOrNot: boolean;
    userOrRoleBase: boolean;
    userLevelAccess: string;
    user: any;
    constructor() {}
}