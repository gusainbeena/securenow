﻿import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { CommonModule } from "@angular/common";
import { HttpService } from '../../service/http.service';
import { ValidationHandler } from '../ValidationHandler';
declare var $: any;
import { IAccessControl, AccessControl } from './AccessControlType';
import { setTimeout } from 'timers';
import { forEach } from '@angular/router/src/utils/collection';
import { TreeviewItem, TreeviewConfig } from '../../lib';
//import { BookService } from './book.service';

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'AccessControl';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class AccessControlComponent implements OnInit, OnDestroy, AfterViewInit {
    rsBody: any;
    navigationSubscription;

    constructor(private httpservice: HttpService, private routes: Router, private activatedroute: ActivatedRoute, private validation: ValidationHandler, private cdref: ChangeDetectorRef) {
        this.navigationSubscription = this.routes.events.subscribe((e: any) => {
            this.finalDataList = { selectedRole: [] };
            this.newDataList = {};
            this.pageNo = 0;
            this.roleStr = [];
            this.servicesStr = [];
            this.OUsStr = [];
            this.permissionsList = [];
            this.monthData = [];
            this.yearList = [];
            this.roleData = {};
            this.newrole = false;
            this.userLevelAccess = 'adpID';

            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                // this.items = this.getBooks();
                this.getRoutesInitialStateWithRoleBase();
            }
        });
    }
    public dropdownSettings: any = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        enableCheckAll: false
    };
    public items: Array<TreeviewItem> = [];
    //public selectedProfileOUs: Array<TreeviewItem> = [];
    public finalDataList: any = { selectedRole: [] };
    public newDataList: any = {};
    public pageNo: number;
    public roleStr: Array<string> = [];
    public servicesStr: Array<string> = [];
    public OUsStr: any = [];
    public OUsKeys: Array<string> = [];
    public operationalUnit: any = [];
    //public operationalUnit: Array<string> = [];

    public permissionsList: Array<string> = [];
    public currentTab: string;
    public subTab: string;
    public monthData: any = [];
    public yearList: any = [];
    public roleData: any = {};
    public newrole: boolean = false;
    public roleBase: boolean = true;
    public CFY: number;
    public userLevelAccess: string;
    public roleHierarchyList: any = [];
    public roleHierarchyStrList: any = [];

    ngOnInit(): void {

        /*$('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
        this.getAllRoles();*/
    }

    getAllRoles(): void {
        this.currentTab = "AccessControl";
        this.subTab = "Role";
        this.roleStr = [];
        this.httpservice.postRequest<AccessControlComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageProfilePermission/GetExistingMetaData', true).subscribe(datas => {
            if (datas.rsBody.result === "success") {

                for (let i = 0; i < datas.rsBody.existingList.length; i++) {
                    this.roleStr.push(datas.rsBody.existingList[i]);
                }

            }
        });
        this.getAllPermissions();

    }

    trivarseTree(e): void {
        if (e.children != undefined) {
            e.children.map((itemList) => {
                var indexData = this.finalDataList.selectedOUs.indexOf(itemList.value);
                if (indexData != -1) {
                    itemList.checked = true;
                }
                else {
                    itemList.checked = false;
                }
                this.trivarseTree(itemList);
            })
        }
    }

    trivarseTreeDisable(e): void {
        if (e.children != undefined) {
            e.children.map((itemList) => {
                var indexData = this.finalDataList.selectedOUs.indexOf(itemList.value);
                if (indexData != -1) {
                    itemList.checked = true;
                    itemList.disabled = true;
                }
                else {
                    itemList.checked = false;
                    itemList.disabled = true;
                }
                this.trivarseTree(itemList);
            })
        }
    }

    getExistingAccess(e): void {

        if (!this.newrole && this.roleBase) {
            if (e.target.id != "") {
                this.finalDataList["profile"] = [e.target.id];
            } else {
                $('.rolelink').parent().removeClass('active');
                $(e.target.parentElement).addClass('active');
                this.finalDataList["profile"] = [e.target.parentElement.id];
            }

            this.httpservice.postRequest<AccessControlComponent>(this.finalDataList, '../ManageProfilePermission/GetExistingAccessForProfile', true).subscribe(datas => {
                if (datas.rsBody.result === "success") {
                    this.finalDataList = Object.assign({}, datas.rsBody.listData);

                    this.items = [];
                    this.finalDataList.ouList.map((item) => {
                        let treeNode = new TreeviewItem(item);
                        this.items.push(treeNode);
                    })
                    this.items.map((itemList) => {
                        var index = this.finalDataList.selectedOUs.indexOf(itemList.value);
                        if (index != -1) {
                            itemList.checked = true;
                        }
                        else {
                            itemList.checked = false;
                        }
                        this.trivarseTree(itemList);
                    })
                    $('#permissionList').removeClass('d-none');
                    $('#serviceList').removeClass('d-none');
                    $('#ouList').removeClass('d-none');
                }
            });

        } else {
            if (Object.keys(this.finalDataList).indexOf('profile') == -1) {
                //this.finalDataList["profile"] = [e.target.id];
                this.finalDataList.profile = [e.target.id];
            } else {
                if (this.finalDataList["profile"].indexOf(e.target.id) == -1) {
                    this.finalDataList["profile"].push(e.target.id);
                } else {
                    this.finalDataList["profile"].splice(this.finalDataList["profile"].indexOf(e.target.id), 1);
                }
            }

            this.httpservice.postRequest<AccessControlComponent>(this.finalDataList, '../ManageProfilePermission/GetExistingAccessForMultiProfile', true).subscribe(datas => {
                if (datas.rsBody.result === "success") {
                    this.finalDataList = Object.assign({}, datas.rsBody.listData);

                    this.items = [];
                    this.finalDataList.ouList.map((item) => {
                        let treeNode = new TreeviewItem(item);
                        this.items.push(treeNode);
                    })
                    this.items.map((itemList) => {
                        var index = this.finalDataList.selectedOUs.indexOf(itemList.value);
                        if (index != -1) {
                            itemList.checked = true;
                            itemList.disabled = true;
                        }
                        else {
                            itemList.checked = false;
                            itemList.disabled = true;
                        }
                        this.trivarseTreeDisable(itemList);
                    })

                    $('#permissionList').removeClass('d-none');
                    $('#serviceList').removeClass('d-none');
                    $('#ouList').removeClass('d-none');
                }
            });
        }

    }

    getRoutesInitialStateWithRoleBase(): void {
        $('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
        this.getAllRoles();
    }

    getRoutesInitialStateWithUserBase(): void {
        $('#permissionList').addClass('d-none');
        $('#serviceList').addClass('d-none');
        $('#ouList').addClass('d-none');
    }

    getAllPermissions(): void {
        this.currentTab = "AccessControl";
        this.subTab = "Permission";
        let parentRef = this;
        this.httpservice.postRequest<AccessControlComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageProfilePermission/GetExistingMetaData', true).subscribe(datas => {
            if (datas.rsBody.result === "success") {
                this.permissionsList = [];
                for (var item of datas.rsBody.existingList) {
                    this.permissionsList.push(item.labelOnScreen);

                }

            }
        })
        //this.getAllOUs();
    }

    getServiceWisePermissions(e, i): void {
        let classes = [];
        classes = Object.assign([], e.target.classList);
        if (classes.indexOf('selectallservice' + i) != -1) {
            if ($('input[type="checkbox"][class*="selectallservice' + i + '"]:checked').length == $('input[type="checkbox"][class*="selectallservice' + i + '"]').length) {
                $('input[type="checkbox"][id="selectallservice' + i + '"]').prop('checked', true);
            } else {
                $('input[type="checkbox"][id="selectallservice' + i + '"]').prop('checked', false);
            }
            let elem = $('input[type="checkbox"][class*="selectallservice' + i + '"]');

        }

        if (e.target.checked === true) {
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('disabled', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('checked', true);
            $('#chkboxrow' + i).attr('checkMandatory', true);
            this.finalDataList["selectedFunctions"][i].selected = true;
            this.finalDataList["selectedFunctions"][i].selectAll = true;
            for (let i = 0; i < this.finalDataList["selectedPermissions"][e.target.id].length; i++) {
                this.finalDataList["selectedPermissions"][e.target.id][i].selected = true;
            }
        } else {
            $('#chkboxrow' + i).attr('checkMandatory', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('checked', false);
            $('#chkboxrow' + i + ' input[type="checkbox"]').prop('disabled', true);
            this.finalDataList["selectedFunctions"][i].selected = false;
            this.finalDataList["selectedFunctions"][i].selectAll = false;
            for (let i = 0; i < this.finalDataList["selectedPermissions"][e.target.id].length; i++) {
                this.finalDataList["selectedPermissions"][e.target.id][i].selected = false;
            }
        }
    }

    addNewRole(): void {
        this.newrole = true;
        this.getYearList();
    }

    saveNewRole(): void {
        this.roleData['key'] = "AccessControl";
        this.roleData['subkey'] = "Role";
        let status = this.validation.validateDOM('newrole');
        if (!status) {
            this.httpservice.postRequest<AccessControlComponent>(this.roleData, '../ManageProfilePermission/CreateMetaData', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        $("#success").modal();
                        this.routes.navigate(["access-control"]);
                    } else {
                        if (datas.rsBody.exceptionBlock != null) {
                            $('html,body').animate({ scrollTop: $('#newrole').offset().top }, 'slow');
                            this.validation.displayErrors(datas.rsBody.exceptionBlock.msg, 'newrole', '');
                        }
                    }
                });
        } else {
            $('html,body').animate({ scrollTop: $('#newrole').offset().top }, 'slow');
        }
    }

    getYearList(): void {

        this.httpservice.postRequest<AccessControlComponent>({ keyType: "YEAR" }, '../Comman/GetKeyValueData', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.yearList = datas.rsBody.resulats;
                    this.roleData.year = this.CFY;
                    this.getMonthList();

                }
            });
    }

    getMonthList(): void {

        this.httpservice.postRequest<AccessControlComponent>({}, '../Comman/GetMonthKeyValueData', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.monthData = datas.rsBody.resulats;
                    this.roleData['startMonth'] = this.monthData[0].keyValue;
                    this.roleData['endMonth'] = this.monthData[this.monthData.length - 1].keyValue;

                }
            });

    }

    grantAccess(id): void {
        this.roleData['key'] = "AccessControl";
        this.roleData['subkey'] = "Role";
        this.finalDataList.treeViewItem = this.items;
        let status = this.validation.validateDOM(id);
        if (!status) {
            this.httpservice.postRequest<AccessControlComponent>(this.finalDataList, '../ManageProfilePermission/CreateAccessPermissions', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        $("#success").modal();
                        this.routes.navigate(["access-control"]);
                    } else {
                        if (datas.rsBody.exceptionBlock != null) {
                            $('html,body').animate({ scrollTop: $('#' + id).offset().top }, 'slow');
                            this.validation.displayErrors(datas.rsBody.exceptionBlock.msg, id, '');
                        }
                    }
                });
        } else {
            $('html,body').animate({ scrollTop: $('#' + id).offset().top }, 'slow');
        }
    }

    changeToUserBase(type): void {
        if (type === 'user') {
            this.roleBase = false;
        } else {
            this.roleBase = true;
        }

        this.routes.navigate(["access-control"]);
    }


    getAdpIdData(id): void {
        var text = $('#' + id);
        if ($('#' + id)[0].value != undefined && $('#' + id)[0].value != "") {
            this.finalDataList["user"] = {};
            this.finalDataList["user"]["adpId"] = $('#' + id)[0].value;
            this.httpservice.postRequest<AccessControlComponent>(this.finalDataList, '../ManageProfilePermission/ADPIdData', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.finalDataList["user"] = Object.assign({}, datas.rsBody.empDet);
                        $('#roleList').removeClass('d-none');
                        $('#empDataPanel').removeClass('d-none');
                        this.finalDataList.user.adpId = datas.rsBody.empDet.adpId;
                    } else {
                        this.finalDataList["user"] = {};
                        $('#roleList').addClass('d-none');
                    }
                });
        } else {
            alert("please enter a valid employee id");
        }

    }

    onRoleItemSelect(e): void {
        if (this.finalDataList.selectedRole != undefined && this.finalDataList.selectedRole.length > 0) {
            $('#roleList').removeClass('d-none');
        } else {
            $('#roleList').addClass('d-none');
        }
    }
    closePanel(): void {
        this.userLevelAccess == "";
        $('#empDataPanel').addClass('d-none');
    }
    setUserLevelAccess(e): void {
        this.userLevelAccess = e.target.id;
        this.currentTab = "RoleHierarchy";
        this.subTab = "RoleHierarchy";
        this.finalDataList["user"] = undefined;
        if (e.target.id === 'reportingRole') {
            this.httpservice.postRequest<AccessControlComponent>({ pageNumber: this.pageNo, metaDataLevel: 1, currentTab: this.currentTab }, '../ManageProfilePermission/getEmployeeAttributes', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.roleHierarchyStrList = []
                        for (let i = 0; i < datas.rsBody.attributesList.length; i++) {
                            this.roleHierarchyStrList.push(datas.rsBody.attributesList[i].labelOnScreen);
                        }
                    }
                });
        }
    }

    selectAll(e, i) {
        let id = e.target.id;
        if ($('input[type="checkbox"][id="' + id + '"]').is(':checked')) {
            $('.' + id).prop('checked', true);

        }
        else {
            $('.' + id).prop('checked', false);

        }
        let val = id.substr(id.length - 1);
        let val2 = this.finalDataList["selectedFunctions"][val].row.keyValue;
        if (e.target.checked === true) {
            this.finalDataList["selectedFunctions"][i].selectAll = true;
            for (let i = 0; i < this.finalDataList["selectedPermissions"][val2].length; i++) {
                //this.finalDataList["selectedPermissions"][e.target.id][i].selected = true;
                this.finalDataList["selectedPermissions"][val2][i].selected = true;
            }
        } else {
            this.finalDataList["selectedFunctions"][i].selectAll = false;
            for (let i = 0; i < this.finalDataList["selectedPermissions"][val2].length; i++) {
                //this.finalDataList["selectedPermissions"][e.target.id][i].selected = false;
                this.finalDataList["selectedPermissions"][val2][i].selected = false;
            }
        }
    }

    checkboxMentain(e, i, j) {
        let classes = [];
        classes = Object.assign([], e.target.classList);
        let id = e.target.id.split('_')[0];
        if (classes.indexOf('selectallpermission' + i) != -1) {
            if ($('input[type="checkbox"][class*="selectallpermission' + i + '"]:checked').length == $('input[type="checkbox"][class*="selectallpermission' + i + '"]').length) {
                $('input[type="checkbox"][id="selectallpermission' + i + '"]').prop('checked', true);
            } else {
                $('input[type="checkbox"][id="selectallpermission' + i + '"]').prop('checked', false);
            }
            let elem = $('input[type="checkbox"][class*="selectallpermission' + i + '"]');

        }

        if (e.target.checked === true) {
            //this.finalDataList["selectedFunctions"][i].selectAll = true;
            this.finalDataList["selectedPermissions"][id][j].selected = true;

        } else {
            //this.finalDataList["selectedFunctions"][i].selectAll = false;
            this.finalDataList["selectedPermissions"][id][j].selected = false;

        }
    }

    setAllOUs(e, key, i): void {

    }

    setOUs(e, key, i, j): void {

        if (e.target.checked === true) {
            this.finalDataList["selectedOperationalUnits"][key][j].selected = true;

        } else {
            this.finalDataList["selectedOperationalUnits"][key][j].selected = false;

        }
    }

    handleCollaps(item, e, list): void {
        $(e.target).toggleClass("fa-caret-down fa-caret-right");
    }

    handleOUSelection(item, e): void {

        item.selected = e.target.checked;
        var ary = e.target.className.split(' ')[1];


        if (e.target.checked) {

            if ($('input[type="checkbox"][class*="' + ary + '"]:checked').length == $('input[type="checkbox"][class*="' + ary + '"]').length) {
                $('#' + ary).css("")
                $('#' + ary).trigger('click');
            } else {
                //$('input[type="checkbox"][id="allSalesComponentSelect"]').prop('checked', false);
            }

            if (item.selected == true) {
                $('#' + item.parentNode.keyValue + '_chkbox').trigger('click');
            }
            else {
            }
            if (item.childNodes.length > 0) {
                item.childNodes.map((node) => {
                    if (e.target.checked != node.selected) {
                        node.selected = true;
                        $('#' + node.nodeVal.keyValue + '_chkbox').trigger('click');
                    }
                });
            } else {
                item.selected = true;
            }
        } else {
            if ($('input[type="checkbox"][class*="' + ary + '"]:checked').length == $('input[type="checkbox"][class*="' + ary + '"]').length) {
                $('#' + ary).trigger('click');
            } else {
                // $('#' + ary).trigger('click');
            }

            if (item.childNodes.length > 0) {
                item.childNodes.map((node) => {
                    if (e.target.checked != node.selected) {
                        node.selected = false;
                        $('#' + node.nodeVal.keyValue + '_chkbox').trigger('click');
                    }
                });
            } else {
                item.selected = false;
            }
        }

        var v = this.operationalUnit;
    }

    hideErrors(id) {
        this.validation.hideErrors(id);
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {

    }

}