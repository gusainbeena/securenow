import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccessControlComponent } from './accesscontrol.component';

const routes: Routes = [
    { path: '', component: AccessControlComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccessControlRoutingModule { }
