﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { FileUploader } from 'ng2-file-upload';
import { ModalService } from '../../service/modelservice';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'OverdueUpload';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class OverdueUploadComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private modal: ModalService,
        private route: ActivatedRoute) {
    }
    keyValueData: any = {};
    requestData: any = {};
    opData: any = {};
    overDueData: any = {};
    incentiveData: any = {};
    ChannelList: any = [];
    year: string = '';
    dataSourceFileName: string = '';
    subChannelList: string = '';
    roleList: string = '';
    actualTypeList: string = '';
    weightageList: string = '';
    uploader: FileUploader = new FileUploader({ url: '../ManageUploadData/UploadMasterSheet' });
    incentive: any = [];
    getNextEmployeeList: any;
    pageNumber: any;
    incentivList: any = {};
    listData: any = [];
    id: any;
    getPage: any;
    yearData: any = [];
    monthData: any = [];
    responseData: any = {};
    downloadFile: any = {};
    error: any = {};
    filename: any;
    result: any;
    File: any = {};
    lines2: any = [];
    errorList: any = [];
    warningList: any = [];
    reason: any;
    success: any;
    lines: any[] = [];

    ngOnInit() {
        this.getNextEmployeeList = 0;
        this.getPage = 0;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest<OverdueUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {

                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });


        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<OverdueUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.yearData = data.rsBody.resulats;
            });

        //getMonth List			
        this.getMonthList();

    }

    getMonthList() {
        this.keyValueData = {};
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest<OverdueUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.monthData = data.rsBody.resulats;
            });

        this.keyValueData.keyType = "TARGETKEY";
        this.httpService.postRequest<OverdueUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                this.opData = data.rsBody.resulats;
            });
    }

    ngOnOverDueDataSubmit() {
        this.overDueData.requestType = "EmployeeMasterDataPort";
        this.httpService.postRequest<OverdueUploadComponent>(this.overDueData, '../ManageUploadData/UploadMasterData', true).subscribe(
            data => {
                if (data == undefined) {
                    var id = data.rsBody.data;
                }
                else {
                    var id = data.rsBody.data;
                }
                this.responseData.id = id;
                this.responseData.id = data.rsBody.data;
                this.responseData.fileName = data.rsBody.fileName;
                this.responseData.filePath = data.rsBody.filePath;
                this.responseData.uploadType = data.rsBody.uploadType;
                this.ngOnCheckStatus(this.responseData);
                if (this.responseData.fileName != null) {
                    this.errorHandler(this.responseData);
                }
            });
    }

    ngOnCheckStatus(inputData)
    {
        this.httpService.postRequest<OverdueUploadComponent>(inputData, '../ManageUploadData/GetUploadStatus', true).subscribe(
            data => {

                this.responseData = data.rsBody;
                if (this.responseData != "" && this.responseData != null) {
                    this.overDueData = {};
                    this.router.navigate(['overdue-upload']);
                    this.modal.open('sucessPopup')
                    this.ngOnInit();
                }
                else {                   
                    this.ngOnCheckStatus(inputData);
                }
            });
    }

    ngOnDeleteRequest(target)
    {
        this.overDueData = this.listData[target];
        this.overDueData.requestType = 'OverDueDataPort';
        this.httpService.postRequest<OverdueUploadComponent>(this.overDueData, '../ManageUploadData/CancelUploadRequest', true).subscribe(
            data => {
                this.error = data.rsBody;
                this.modal.open('error');
                this.ngOnInit();
            });
    }

    ngOnGetNext() {
        this.getPage = this.getPage + 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest<OverdueUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {                
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    ngOnGetPrevious() {
        this.getPage = this.getPage - 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OverDueDataPort";
        this.httpService.postRequest<OverdueUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    modalclose(id) {
        this.modal.close(id);
    }

    ngAfterViewInit() {

    }

    fileSelect()
    {
        $('#imageUpload').click();
    }			

    imageUpload()
    {
        this.uploader.uploadAll();
        //Get response
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) =>
        {
	        var responsePath = JSON.parse(response);
	        {
                this.overDueData.dataSourceFileName = responsePath.rsBody;
		    };
	    }
    }

    ngOnTemplateDownload() {

        var triggerClickk = setTimeout(function () {
            clearTimeout(triggerClickk);
            $('#downloadReport')[0].click();
        });
    }

    ngOnDownloadSourceReport(data) {
        this.downloadFile = data;
        $('#DownloadSourcePath').attr('href', '../Comman/DownloadOpSourceReport?name=' + this.downloadFile);
        $('#DownloadSourcePath')[0].click();
    }

    ngOnDownloadErrorReport(getData) {
        this.downloadFile = getData;
        this.httpService.postRequest<OverdueUploadComponent>(this.downloadFile, '../Comman/CheckFileExistOrNot', true).subscribe(
            data => {
                this.result = data.rsBody;
                if (this.result == "NotExist") {
                    this.modal.open('notFound');
                    this.filename = "";
                }

                else {
                    $('#DownloadErrorPath').attr('href', '../Comman/DownloadOpErrorReport?name=' + this.downloadFile);
                    $('#DownloadErrorPath')[0].click();
                }
            });   
    }

    ngOnDestroy() {
    }

    errorHandler(parameter) {
        this.File.fileName = parameter.fileName;
        this.File.filePath = parameter.filePath;
        this.File.id = parameter.id;
        this.File.uploadType = parameter.uploadType;
        this.File.quarter = parameter.quarter;
        this.httpService.postRequest<OverdueUploadComponent>(this.File, '../ManageUploadData/Errorhandler', true).subscribe(
            data => {

                if (data.rsBody.result == 'success') {
                    $('#consolePanel').removeClass('d-none');
                    //this.lines = null;
                    if (data.rsBody.error == null) {
                        this.lines2 = data.rsBody.lines;
                        this.errorList = data.rsBody.errorList;
                        if (this.errorList.length == 0) {
                            this.success = data.rsBody.success;
                            $('#successBlock').removeClass('d-none');
                        }
                        for (var i = 0; i <= this.lines2.length; i++) {
                            this.lines.push(this.lines2[i]);
                            //this.sub = Observable.interval(10000)
                            //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                        }
                        this.warningList = data.rsBody.warningList;
                    }
                    else {
                        //this.lines = null;
                        this.lines2 = data.rsBody.lines;
                        for (var i = 0; i <= this.lines2.length; i++) {
                            this.lines.push(this.lines2[i]);
                            //this.sub = Observable.interval(10000)
                            //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                        }
                        this.errorList = data.rsBody.errorList;
                        if (this.errorList.length > 200) {
                            this.errorList = [];
                            this.errorList.push("Number of error are too high to handle please first correct your data sheet than proceed");
                        }
                        this.warningList = data.rsBody.warningList;
                        this.reason = data.rsBody.error;
                    }


                }
            });
    }
}
