import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverdueUploadComponent } from './overdueupload.component';

const routes: Routes = [
    { path: '', component: OverdueUploadComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OverdueUploadRoutingModule { }
