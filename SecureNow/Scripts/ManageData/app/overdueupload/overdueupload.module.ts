import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { OverdueUploadRoutingModule } from './overdueupload-routing.module';
import { OverdueUploadComponent } from './overdueupload.component';
import { FileSelectDirective } from 'ng2-file-upload';
@NgModule({
    declarations: [
        OverdueUploadComponent, FileSelectDirective
    ],
    imports: [
        CommonModule,
        OverdueUploadRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,        
        FormsModule,         
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [OverdueUploadComponent],
    exports: []
})
export class OverdueUploadModule { }
