import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GlobalDashboardComponent } from './globaldashboard.component';

const routes: Routes = [
    { path: '', component: GlobalDashboardComponent },
    { path: 'global-dashboard', component: GlobalDashboardComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GlobalDashboardRoutingModule { }
