import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { HttpService } from '../../service/http.service';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { GlobalDashboardRoutingModule } from './globaldashboard-routing.module';
import { GlobalDashboardComponent } from './globaldashboard.component';
import { ModalComponent } from '../../_directives/modal.service';
@NgModule({
    declarations: [
        GlobalDashboardComponent
    ],
    imports: [
        CommonModule,
        GlobalDashboardRoutingModule,
        HttpClientModule,
        FormsModule,        
        HttpModule
    ],
    providers: [HttpService, D3BindingsService, ModalComponent],
    bootstrap: [GlobalDashboardComponent],
    exports: []
})
export class GlobalDashboardModule { }
