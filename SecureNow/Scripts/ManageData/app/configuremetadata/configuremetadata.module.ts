import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ConfigureMetaDataRoutingModule } from './configuremetadata-routing.module';
import { ConfigureMetaDataComponent } from './configuremetadata.component';
import { TreeviewModule } from 'ngx-treeview';
import { SortablejsModule } from 'angular-sortablejs';
@NgModule({
    declarations: [
        ConfigureMetaDataComponent,
    ],
    imports: [
        CommonModule,
        ConfigureMetaDataRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        SortablejsModule.forRoot({ animation: 150 }),
        TreeviewModule.forRoot(),
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ConfigureMetaDataComponent],
    exports: []
})
export class ConfigureMetaDataModule { }
