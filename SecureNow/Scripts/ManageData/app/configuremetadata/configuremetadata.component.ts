﻿import { Component, OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { ValidationHandler } from '../ValidationHandler';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { forEach } from '@angular/router/src/utils/collection';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { SortablejsOptions } from 'angular-sortablejs';
import { NvD3Module } from 'ng2-nvd3';
import * as d3 from 'd3';
import * as d3hierarchy from 'd3-hierarchy';
import { HighlightDelayBarrier } from 'blocking-proxy/built/lib/highlight_delay_barrier';
//import "../../../customchart-js/scriping.js";
//import "../../../customchart-js/bootstrap.min.js";


declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureMetaData';

@Component({
    selector: 'app-root',
    template: _templateURL  
})

export class ConfigureMetaDataComponent implements OnInit, OnDestroy, AfterViewInit {
    
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public list = [];
    public node = {
        title: 'root',
        level: 0,
        value: {},
        children: []
    };
    public dropdownSettings: any = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        enableCheckAll: false
    };
    valuesOptions: SortablejsOptions;
    keysOptions: SortablejsOptions;

    public d3Tree: any;
    public currentElement: any = {};
    public performanceComponentVisible: boolean = true;
    public roleHierarchyVisible: boolean = true;
    public newKeyContainer: boolean = false;
    public finalData: any = { commonForAll: true, };
    public finalDataa: any = { commonForAll: true, };
    public roleHierarchyList: any = [];
    public performanceComponentList: any = [];
    public operationalUnitList: any = [];
    public teamsList: any = [];
    public currenLevelData: any = {};
    public metaDataValue: any = {};
    public parentList: any = {};
    public dataObj: any = {};
    public teamName: any = [];
    public heirarchyTree: any = {};
    public activeTabData: any = {};
    public dataObjOUs: any = {};
    public salesBoundriesList: any = [];
    public salesVerticalList: any = [];
    public salesBoundriesListStr: any = [];
    public roleHierarchySelected: any = [];
    public ouList: any[];
    public performanceComponentSelected: any = [];
    public currentTab: string;
    public subTab: string;
    public subTabList: any = {};
    public horizontalChildList: any = {};
    public metaheirachy: any = {};
    public subTabDisplayValue: any = {};
    public currentTabDisplay: string;
    public subTabDisplay: string;  
    public pageNo: number;
    public treeId: string = "";
    public empAttributes: any = {};
    public attributesList: any = [];
    public monthData: any = [];
    public upperSalesHorizontal: any = [];
    public upperOUsControls: any = [];
    public CFY: string = "";
    public heirarchyStr: string = "";
    public attributesListCopy: any = [];
    public tabContentData: any = [];
    public controlsArray: any = [];
    public yearList: any = [];
    public seqCount: number = 0;
    public isLoadingFirst: boolean = true;
    public colors: any = ["themeblue", "themegreen", "orange", "danger", "secondary"];
    public filterBreadcrumb: any = [];
    //public currenLevelData: any = {};
    public hierarchyList: any = { };
    public leftCount: number = 0;
    public rightCount: number = 0;
    public colorCode: any = [];
    public shColor: any = ['colorVal1', 'colorVal2', 'colorVal3', 'colorVal4', 'colorVal5', 'colorVal6'];
    public salesBoundriesHierarchyLevelData: any = {
        data: {}, dataList: {}, level_0: undefined, level_1: undefined, level_2: undefined, level_3: undefined, level_4: undefined,
        level_5: undefined, level_6: undefined, level_7: undefined, level_8: undefined, level_9: undefined, level_10: undefined
    };
    public salesVerticalsHierarchyLevelData: any = {
        data: {}, dataList: {}, level_0: undefined, level_1: undefined, level_2: undefined, level_3: undefined, level_4: undefined,
        level_5: undefined, level_6: undefined, level_7: undefined, level_8: undefined, level_9: undefined, level_10: undefined
    };
    public keys: Array<string> = [];
    public timeZone: any = [
        {
            "offset": "GMT-12:00",
            "name": "Etc/GMT-12"
        },
        {
            "offset": "GMT-11:00",
            "name": "Etc/GMT-11"
        },
        {
            "offset": "GMT-11:00",
            "name": "Pacific/Midway"
        },
        {
            "offset": "GMT-10:00",
            "name": "America/Adak"
        },
        {
            "offset": "GMT-09:00",
            "name": "America/Anchorage"
        },
        {
            "offset": "GMT-09:00",
            "name": "Pacific/Gambier"
        },
        {
            "offset": "GMT-08:00",
            "name": "America/Dawson_Creek"
        },
        {
            "offset": "GMT-08:00",
            "name": "America/Ensenada"
        },
        {
            "offset": "GMT-08:00",
            "name": "America/Los_Angeles"
        },
        {
            "offset": "GMT-07:00",
            "name": "America/Chihuahua"
        },
        {
            "offset": "GMT-07:00",
            "name": "America/Denver"
        },
        {
            "offset": "GMT-06:00",
            "name": "America/Belize"
        },
        {
            "offset": "GMT-06:00",
            "name": "America/Cancun"
        },
        {
            "offset": "GMT-06:00",
            "name": "America/Chicago"
        },
        {
            "offset": "GMT-06:00",
            "name": "Chile/EasterIsland"
        },
        {
            "offset": "GMT-05:00",
            "name": "America/Bogota"
        },
        {
            "offset": "GMT-05:00",
            "name": "America/Havana"
        },
        {
            "offset": "GMT-05:00",
            "name": "America/New_York"
        },
        {
            "offset": "GMT-04:30",
            "name": "America/Caracas"
        },
        {
            "offset": "GMT-04:00",
            "name": "America/Campo_Grande"
        },
        {
            "offset": "GMT-04:00",
            "name": "America/Glace_Bay"
        },
        {
            "offset": "GMT-04:00",
            "name": "America/Goose_Bay"
        },
        {
            "offset": "GMT-04:00",
            "name": "America/Santiago"
        },
        {
            "offset": "GMT-04:00",
            "name": "America/La_Paz"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Argentina/Buenos_Aires"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Montevideo"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Araguaina"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Godthab"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Miquelon"
        },
        {
            "offset": "GMT-03:00",
            "name": "America/Sao_Paulo"
        },
        {
            "offset": "GMT-03:30",
            "name": "America/St_Johns"
        },
        {
            "offset": "GMT-02:00",
            "name": "America/Noronha"
        },
        {
            "offset": "GMT-01:00",
            "name": "Atlantic/Cape_Verde"
        },
        {
            "offset": "GMT",
            "name": "Europe/Belfast"
        },
        {
            "offset": "GMT",
            "name": "Africa/Abidjan"
        },
        {
            "offset": "GMT",
            "name": "Europe/Dublin"
        },
        {
            "offset": "GMT",
            "name": "Europe/Lisbon"
        },
        {
            "offset": "GMT",
            "name": "Europe/London"
        },
        {
            "offset": "UTC",
            "name": "UTC"
        },
        {
            "offset": "GMT+01:00",
            "name": "Africa/Algiers"
        },
        {
            "offset": "GMT+01:00",
            "name": "Africa/Windhoek"
        },
        {
            "offset": "GMT+01:00",
            "name": "Atlantic/Azores"
        },
        {
            "offset": "GMT+01:00",
            "name": "Atlantic/Stanley"
        },
        {
            "offset": "GMT+01:00",
            "name": "Europe/Amsterdam"
        },
        {
            "offset": "GMT+01:00",
            "name": "Europe/Belgrade"
        },
        {
            "offset": "GMT+01:00",
            "name": "Europe/Brussels"
        },
        {
            "offset": "GMT+02:00",
            "name": "Africa/Cairo"
        },
        {
            "offset": "GMT+02:00",
            "name": "Africa/Blantyre"
        },
        {
            "offset": "GMT+02:00",
            "name": "Asia/Beirut"
        },
        {
            "offset": "GMT+02:00",
            "name": "Asia/Damascus"
        },
        {
            "offset": "GMT+02:00",
            "name": "Asia/Gaza"
        },
        {
            "offset": "GMT+02:00",
            "name": "Asia/Jerusalem"
        },
        {
            "offset": "GMT+03:00",
            "name": "Africa/Addis_Ababa"
        },
        {
            "offset": "GMT+03:00",
            "name": "Asia/Riyadh89"
        },
        {
            "offset": "GMT+03:00",
            "name": "Europe/Minsk"
        },
        {
            "offset": "GMT+03:30",
            "name": "Asia/Tehran"
        },
        {
            "offset": "GMT+04:00",
            "name": "Asia/Dubai"
        },
        {
            "offset": "GMT+04:00",
            "name": "Asia/Yerevan"
        },
        {
            "offset": "GMT+04:00",
            "name": "Europe/Moscow"
        },
        {
            "offset": "GMT+04:30",
            "name": "Asia/Kabul"
        },
        {
            "offset": "GMT+05:00",
            "name": "Asia/Tashkent"
        },
        {
            "offset": "GMT+05:30",
            "name": "Asia/Kolkata"
        },
        {
            "offset": "GMT+05:45",
            "name": "Asia/Katmandu"
        },
        {
            "offset": "GMT+06:00",
            "name": "Asia/Dhaka"
        },
        {
            "offset": "GMT+06:00",
            "name": "Asia/Yekaterinburg"
        },
        {
            "offset": "GMT+06:30",
            "name": "Asia/Rangoon"
        },
        {
            "offset": "GMT+07:00",
            "name": "Asia/Bangkok"
        },
        {
            "offset": "GMT+07:00",
            "name": "Asia/Novosibirsk"
        },
        {
            "offset": "GMT+08:00",
            "name": "Etc/GMT+8"
        },
        {
            "offset": "GMT+08:00",
            "name": "Asia/Hong_Kong"
        },
        {
            "offset": "GMT+08:00",
            "name": "Asia/Krasnoyarsk"
        },
        {
            "offset": "GMT+08:00",
            "name": "Australia/Perth"
        },
        {
            "offset": "GMT+08:45",
            "name": "Australia/Eucla"
        },
        {
            "offset": "GMT+09:00",
            "name": "Asia/Irkutsk"
        },
        {
            "offset": "GMT+09:00",
            "name": "Asia/Seoul"
        },
        {
            "offset": "GMT+09:00",
            "name": "Asia/Tokyo"
        },
        {
            "offset": "GMT+09:30",
            "name": "Australia/Adelaide"
        },
        {
            "offset": "GMT+09:30",
            "name": "Australia/Darwin"
        },
        {
            "offset": "GMT+09:30",
            "name": "Pacific/Marquesas"
        },
        {
            "offset": "GMT+10:00",
            "name": "Etc/GMT+10"
        },
        {
            "offset": "GMT+10:00",
            "name": "Australia/Brisbane"
        },
        {
            "offset": "GMT+10:00",
            "name": "Australia/Hobart"
        },
        {
            "offset": "GMT+10:00",
            "name": "Asia/Yakutsk"
        },
        {
            "offset": "GMT+10:30",
            "name": "Australia/Lord_Howe"
        },
        {
            "offset": "GMT+11:00",
            "name": "Asia/Vladivostok"
        },
        {
            "offset": "GMT+11:30",
            "name": "Pacific/Norfolk"
        },
        {
            "offset": "GMT+12:00",
            "name": "Etc/GMT+12"
        },
        {
            "offset": "GMT+12:00",
            "name": "Asia/Anadyr"
        },
        {
            "offset": "GMT+12:00",
            "name": "Asia/Magadan"
        },
        {
            "offset": "GMT+12:00",
            "name": "Pacific/Auckland"
        },
        {
            "offset": "GMT+12:45",
            "name": "Pacific/Chatham"
        },
        {
            "offset": "GMT+13:00",
            "name": "Pacific/Tongatapu"
        },
        {
            "offset": "GMT+14:00",
            "name": "Pacific/Kiritimati"
        }
    ]
    public currency: any = [
        {
            "symbol": "$",
            "name": "US Dollar",
            "symbol_native": "$",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "USD",
            "name_plural": "US dollars"
        },
        {
            "symbol": "CA$",
            "name": "Canadian Dollar",
            "symbol_native": "$",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "CAD",
            "name_plural": "Canadian dollars"
        },
        {
            "symbol": "€",
            "name": "Euro",
            "symbol_native": "€",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "EUR",
            "name_plural": "euros"
        },
        {
            "symbol": "AED",
            "name": "United Arab Emirates Dirham",
            "symbol_native": "د.إ.‏",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "AED",
            "name_plural": "UAE dirhams"
        },
        {
            "symbol": "AU$",
            "name": "Australian Dollar",
            "symbol_native": "$",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "AUD",
            "name_plural": "Australian dollars"
        },
        {
            "symbol": "CN¥",
            "name": "Chinese Yuan",
            "symbol_native": "CN¥",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "CNY",
            "name_plural": "Chinese yuan"
        },
        {
            "symbol": "£",
            "name": "British Pound Sterling",
            "symbol_native": "£",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "GBP",
            "name_plural": "British pounds sterling"
        },
        {
            "symbol": "Rs",
            "name": "Indian Rupee",
            "symbol_native": "টকা",
            "decimal_digits": 2,
            "rounding": 0,
            "code": "INR",
            "name_plural": "Indian rupees"
        },
        {
            "symbol": "¥",
            "name": "Japanese Yen",
            "symbol_native": "￥",
            "decimal_digits": 0,
            "rounding": 0,
            "code": "JPY",
            "name_plural": "Japanese yen"
        }];

    constructor(private httpService: HttpService,
        private router: Router,
        private validation: ValidationHandler,
        private route: ActivatedRoute) {

        // slider logic end
        this.keysOptions = {

            onUpdate: (event: any) => {
                console.log(this.tabContentData);
                console.log(event);

            }
        };
        this.valuesOptions = {
            group: 'test',
            onAdd: (event: any) => {
                console.log(this.tabContentData);
                console.log(event);
            }
        };

        this.list.push(this.node);
    }

    ngOnInit(): void {

        this.pageNo = 0;
        this.currentTab = "MetaDataType";
        this.seqCount = 0;
        this.empAttributes = [{ key: this.currentTab, pageNumber: this.pageNo, metaDataLevel: 0 }];
        this.httpService.postRequest<ConfigureMetaDataComponent>({ pageNumber: this.pageNo, metaDataLevel: 0, currentTab: this.currentTab, subTab: this.currentTab }, '../ManageMetaData/getEmployeeAttributes', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.attributesList = datas.rsBody.attributesList;
                    this.currentTab = this.attributesList[0].keyValue;
                    this.ouList = datas.rsBody.attributeList;
                    this.currentTabDisplay = this.attributesList[0].labelOnScreen;
                    this.CFY = datas.rsBody.CFY;
                    this.ouList = datas.rsBody.ouList;
                    this.getYearList();
                }
            });
    }
    getOccurrence(array, value) {
        return array.filter((v) => (v.keyValue === value)).length;
    }

    d3TreeFunction(treeData1, colorCode, treeId) {

        var self = this;
        var treeData = treeData1;
        interface INode { };
        // Set the dimensions and margins of the diagram
        var margin = { top: 20, right: 90, bottom: 30, left: 60 },
            width = 2000 - margin.left - margin.right,
            height = 550 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        $("#" + treeId).empty();
        var svg = d3.select("#" + treeId).append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate("
                + margin.left + "," + margin.top + ")");        
        var i = 0,
            duration = 750,
            root;

        // declares a tree layout and assigns the size
        var treemap = d3hierarchy.tree().size([height, width]);

        // Assigns parent, children, height, depth
        root = d3hierarchy.hierarchy<any>(treeData, function (d) { return d.children; });
        root.x0 = height / 2;
        root.y0 = 0;

        // Collapse after the second level
        if (root.children != undefined) {
            root.children.forEach(collapse);
        }
        update(self, root);

        // Collapse the node and all it's children
        function collapse(d) {
            if (d.children) {
                d._children = d.children
                d._children.forEach(collapse)
                d.children = null
            }
        }

        function update(self, source) {
            // self = this;
            //self.addClick('g');
            // Assigns the x and y position for the nodes
            var treeData = treemap(root);

            // Compute the new tree layout.
            var nodes = treeData.descendants(),
                links = treeData.descendants().slice(1);

            // Normalize for fixed-depth.
            nodes.forEach(function (d) { d.y = d.depth * 200 });

            // ****************** Nodes section ***************************

            // Update the nodes...
            var node = svg.selectAll('g.node')
                .data<INode>(nodes, function (d) { return (d['id'] || (d['id'] = ++i)); });
            //console.log(node);

            // Enter any new modes at the parent's previous position.
            var nodeEnter = node.enter().append('g')
                .attr('class', 'node')
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })



            //Add Value
            nodeEnter.append("circle").on('click', function (d) { self.addValueClick(d); })
                .attr("r", 9)
                .attr("fill", "#555")
                .attr("cx", 0)
                .attr("cy", -1);
            nodeEnter.append("image").on('click', function (d) { self.addValueClick(d); })
                .attr("xlink:href", "../../Images/add1.png")
                .attr("x", -7)
                .attr("y", -8)
                .attr("cursor", "pointer")
                .attr("width", 14)
                .attr("height", 14);


            //Re-Assign Value
            nodeEnter.append("circle").on('click', function (d) { self.reAssignValueClick(d); })
                .attr("r", 9)
                .attr("fill", "#555")
                .attr("cx", 25)
                .attr("cy", -1);
            nodeEnter.append("image").on('click', function (d) { self.reAssignValueClick(d); })
                .attr("xlink:href", "../../Images/edit1.png")
                .attr("x", 18)
                .attr("y", -8)
                .attr("cursor", "pointer")
                .attr("width", 14)
                .attr("height", 14);


            //Delete Value
            nodeEnter.append("circle").on('click', function (d) { self.minusValueClick(d); })
                .attr("r", 9)
                .attr("fill", "#555")
                .attr("cx", 50)
                .attr("cy", -1);  
            nodeEnter.append("image").on('click', function (d) { self.minusValueClick(d); })
                .attr("xlink:href", "../../Images/delete1.png")
                .attr("x", 43)
                .attr("y", -8)
                .attr("cursor", "pointer")
                .attr("width", 14)
                .attr("height", 14);

         
            // Add labels for the nodes
            nodeEnter.append('text')
                .on('click', click).attr("dy", ".3em")
                .style('fill', '#374649')
                .text(function (d) {
                    if (d['data'].name == null) {
                        console.log(d['data']);
                    }
                    if (d['data'].name.length > 5)
                        return d['data'].name.substring(0, 5) + '...';
                    else
                        return d['data'].name;
                })
                .attr("x", function (d) {
                    return d['children'] || d['_children'] ? 65 : 65;
                })
                .attr("y", function (d) {
                    return d['children'] || d['_children'] ? 0 : 0;
                })
                .attr("text-anchor", function (d) {
                    return d['children'] || d['_children'] ? "center" : "center";
                })
                .style("fill", function (d) {
                    return d['_children'] ? "#374649" : "gray";
                })
                .style("cursor", function (d) {
                    return d['_children'] ? "pointer" : "unset";
                })
                .append("svg:title")
                .text(function (d) { return d['data'].name; });


            // Add period for the nodes
            nodeEnter.append('text')
                .on('click', click).attr("dy", ".3em")
                .style('cursor', 'pointer')
                .style('fill', 'gray')
                .text(function (d) {                   
                        return d['data'].period;
                })
                .attr("x", function (d) {
                    return d['children'] || d['_children'] ? 0 : 0;
                })
                .attr("y", function (d) {
                    return d['children'] || d['_children'] ? 20 : 20;
                })
                .style('font-size', '10px')
                .attr("text-anchor", function (d) {
                    return d['children'] || d['_children'] ? "center" : "center";
                })
                .append("svg:title")
                .text(function (d) { return d['data'].period; });

            // UPDATE
            var nodeUpdate = nodeEnter['merge'](node);

            // Transition to the proper position for the node
            nodeUpdate.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            // Update the node attributes and style
            nodeUpdate.selectAll('circle')
                .style('stroke', '#6b6f828c')
                .attr('r', 12)
                .attr("class", function (d) {

                    if (d._children) {
                        for (var j = 0; j < colorCode.length; j++) {
                            if (d.data.key != null && (d.data.key.toLowerCase() == colorCode[j]['key'].toLowerCase())) {
                                return colorCode[j]['color'];
                            }
                            else if (d.data.key == null) {
                                return '#374649';
                            }
                        }
                    }
                    else {
                        for (var j = 0; j < colorCode.length; j++) {
                            if (d.data.key != null && (d.data.key.toLowerCase() == colorCode[j]['key'].toLowerCase())) {
                                return colorCode[j]['color'];
                            }
                            else if (d.data.key == null) {
                                return '#374649';
                            }
                        }
                    }
                })
                .attr('cursor', 'pointer');



            // Remove any exiting nodes
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            // On exit reduce the node circles size to 0
            nodeExit.select('circle')
                .attr('r', 1e-6);

            // On exit reduce the opacity of text labels
            nodeExit.select('text')
                .style('fill-opacity', 1e-6);

            // ****************** links section ***************************

            // Update the links...
            var link = svg.selectAll('path.link')
                .data(links, function (d) { return d['id']; });

            // Enter any new links at the parent's previous position.
            var linkEnter = link.enter().insert('path', "g")
                .attr("class", "link")
                .attr('d', function (d) {
                    var o = { x: source.x0, y: source.y0 }
                    return diagonal(o, o)
                });



            // UPDATE
            var linkUpdate = linkEnter['merge'](link);

            // Transition back to the parent element position
            linkUpdate.transition()
                .duration(duration)
                .attr('d', function (d) { return diagonal(d, d.parent) });

            // Remove any exiting links
            var linkExit = link.exit().transition()
                .duration(duration)
                .attr('d', function (d) {
                    var o = { x: source.x, y: source.y }
                    return diagonal(o, o)
                })
                .remove();

            // Store the old positions for transition.
            nodes.forEach(function (d) {
                d['x0'] = d['x'];
                d['y0'] = d['y'];
            });
            // Creates a curved (diagonal) path from parent to the child nodes
            function diagonal(s, d) {

                path = `M ${s.y} ${s.x}
							C ${(s.y + d.y) / 2 + 50} ${s.x},
							  ${(s.y + d.y) / 2 + 50} ${d.x},
							  ${d.y + 130} ${d.x}`

                return path
            }

            // Toggle children on click.
            function click(d) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else {
                    d.children = d._children;
                    d._children = null;
                }
                update(self, d);
            }

        }

        /* Graph Tree */
    }



    ngParentHeirarchy(data, heirarchyStr) {
        if (data.data.id != 0) {
            if (heirarchyStr != "" && heirarchyStr != null) {
                heirarchyStr = data.data.id + ":" + heirarchyStr;
            }
            else {
                heirarchyStr = data.data.id;
            }
        }
        if (data.parent != null) {

            heirarchyStr = this.ngParentHeirarchy(data.parent, heirarchyStr);
        }
        return heirarchyStr;
    }
    addValueClick(d) {

        this.currentElement = d;
        if (d.data.id != 0) {
            this.heirarchyStr = d.data.id;
        }
        if (d.parent != null)
        {
            this.heirarchyStr = this.ngParentHeirarchy(d.parent, this.heirarchyStr);
        }
        this.parentList.data = this.heirarchyTree;
        this.parentList.data.currentItem = {};
        this.parentList.data.currentItem.key = this.currentElement.data.key;
        this.parentList.data.currentItem.name = this.currentElement.data.name;
        this.parentList.data.currentItem.id = this.currentElement.data.id;
        this.metaDataValue = this.parentList.data;
        this.metaDataValue.key = this.currentTab;
        this.httpService.postRequest<ConfigureMetaDataComponent>(this.metaDataValue, '../ManageMetaData/GetSalesHorizontalValue', false).subscribe(
            datas => {
                this.subTabList = datas.rsBody.subTabList;
                this.finalData.tab = "MetaDataValue";
                $('#addNewAttributeValue').modal();
            });
    }

    reAssignValueClick(d)
    {
        //setting prefrence value for newly created metadata  
        this.heirarchyStr = "";
        this.currentElement = d;        
        if (d.parent != null) {
            this.heirarchyStr = this.ngParentHeirarchy(d.parent, this.heirarchyStr);
        }
        this.finalData.childNodeDetail = {};
        this.finalData.childNodeDetail.key = d.data.key;
        this.finalData.preferenceValue = d.data.preferenceValue;
        this.finalData.childNodeDetail.name = d.data.name;
        this.finalData.childNodeDetail.id = d.data.id;

        this.finalData['key'] = this.currentTab;
        this.finalData['subKey'] = d.data.key;
        this.salesBoundriesList = [];
        this.controlsArray = [];
        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperHeirarchyList', true).subscribe(
            datas => {
                if (datas.rsBody.message == 'Success') {
                    this.salesBoundriesList = datas.rsBody.result;
                    for (let i = 0; i < this.salesBoundriesList.length; i++) {
                        this.controlsArray.push(this.salesBoundriesList[i]);
                        this.finalData[this.salesBoundriesList[i].modalKey] = "";
                    }
                    this.dataObj = datas.rsBody.level;
                    $('.controlArray').prop('disabled', 'disabled');
                    $('#reAssignAttribute').modal();
                }
            });
    }

    ngReAssignMetaData()
    {        


        

        $('#reAssignAttribute').modal('hide');
        this.metaDataValue = this.currentElement.data;
        this.finalData.heirarchyNode = this.metaDataValue;
        if (this.metaDataValue.children.length === 0) {
            this.MetaDataReAssign();
        }
        else {
            this.horizontalChildList = this.metaDataValue;
            this.treeId = "D3_TreeReAssign";
            this.d3TreeFunction(this.horizontalChildList, this.colorCode, this.treeId);
            $('#ReAssignExceptionPopup').modal();            
        }

       
    }

    AssignFlag(obj) {

        if (obj == "Heirarchy") {
            this.finalData.heirarchyAssign = true;
        }
        else
        {
            this.finalData.heirarchyAssign = false;
        }
    }

    MetaDataReAssign() {

        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/ReAssignMetaData', true).subscribe(
            datas => {
                if (datas.rsBody.message == 'Success') {
                    this.salesBoundriesList = datas.rsBody.result;
                    for (let i = 0; i < this.salesBoundriesList.length; i++) {
                        this.controlsArray.push(this.salesBoundriesList[i]);
                        this.finalData[this.salesBoundriesList[i].modalKey] = "";
                    }
                    this.dataObj = datas.rsBody.level;
                    $('.controlArray').prop('disabled', 'disabled');
                    $('#reAssignAttribute').modal();
                }
            });
    }

    minusValueClick(d) {

        this.currentElement = d;
        if (d.data.id != 0) {
            this.heirarchyStr = d.data.id;
        }
        if (d.parent != null) {
            this.heirarchyStr = this.ngParentHeirarchy(d.parent, this.heirarchyStr);
        }
        this.parentList.data = this.heirarchyTree;
        this.parentList.data.currentItem = {};
        this.parentList.data.currentItem.key = this.currentElement.data.key;
        this.parentList.data.currentItem.name = this.currentElement.data.name;
        this.parentList.data.currentItem.id = this.currentElement.data.id;
        this.metaDataValue = this.parentList.data;
        this.metaDataValue.key = this.currentTab;
        $('#deactiveAttributePopup').modal();



    }

    deactivateSalesHorizontal(d) {
        if (d != undefined) {
            this.currentElement = d;
            this.parentList.data = this.heirarchyTree;
            this.parentList.data.currentItem = {};
            this.parentList.data.currentItem.key = this.currentElement.keyType;
            this.parentList.data.currentItem.name = this.currentElement.keyName;
            this.parentList.data.currentItem.id = this.currentElement.sid;
            this.metaDataValue = this.parentList.data;
            this.metaDataValue.key = this.currentTab;


            this.currentElement = d;
            this.parentList.data = this.heirarchyTree;
            this.parentList.data.currentItem = {};
            this.parentList.data.currentItem.key = this.currentElement.keyType;
            this.parentList.data.currentItem.name = this.currentElement.keyName;
            this.parentList.data.currentItem.id = this.currentElement.sid;
          
        }
        

        $('#deactiveAttributePopup').modal('hide');
        this.metaDataValue = this.currentElement.data;
        this.horizontalChildList = this.metaDataValue;
        this.treeId = "D3_TreeDeactivate";
        this.d3TreeFunction(this.horizontalChildList, this.colorCode, this.treeId);
        $('#deactivateExceptionPopup').modal();
        this.setTab(this.activeTabData);
       


    }
    deactivateSalesHorizontalFinalSubmit(d) {
        this.heirarchyStr = "";

        if (this.currentElement.data.id != 0) {
            this.heirarchyStr = this.currentElement.data.id;
        }
        if (this.currentElement.parent != null) {
            this.heirarchyStr = this.ngParentHeirarchy(this.currentElement.parent, this.heirarchyStr);
        }

        this.parentList.data = this.heirarchyTree;
        this.parentList.data.currentItem = {};
        this.parentList.data.currentItem.key = this.currentElement.data.key;
        this.parentList.data.currentItem.name = this.currentElement.data.name;
        this.parentList.data.currentItem.id = this.currentElement.data.id;
        this.metaDataValue = this.parentList.data;
        this.metaDataValue.key = this.currentTab;
        this.metaDataValue.newNode.attributeSequence = this.heirarchyStr;
        this.metaDataValue.endMonth = this.finalDataa.endMonth;
        this.httpService.postRequest<ConfigureMetaDataComponent>(this.metaDataValue, '../ManageMetaData/DeactivateSalesHorizontalSubmit', true).subscribe(
            datas => {

                if (datas.rsBody.message == 'Success')
                {
                  
                    
                    
                }
            });

    }

        getYearList() {
            this.httpService.postRequest<ConfigureMetaDataComponent>({ keyType: "YEAR" }, '../Common/GetKeyValueData', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.yearList = datas.rsBody.resulats;
                        this.finalData.year = this.CFY;
                        this.getMonthList();
                    }
                });
        }

        getMonthList() {

            this.httpService.postRequest<ConfigureMetaDataComponent>({}, '../Common/GetMonthKeyValueData', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.monthData = datas.rsBody.resulats;
                        this.finalData['startMonth'] = this.monthData[0].keyValue;
                        this.finalData['endMonth'] = this.monthData[this.monthData.length - 1].keyValue;
                        this.finalDataa['startMonth'] = this.monthData[0].keyValue;
                        this.finalDataa['endMonth'] = this.monthData[this.monthData.length - 1].keyValue;
                        if (this.attributesList.length > 0) {
                            this.setTab(this.attributesList[0]);
                        }

                    }
                });

        }

        restructureData(dataList) {
            if (this.currentTab === 'RoleHierarchy') {
                this.roleHierarchySelected = [];
                for (let i = 0; i < dataList.length; i++) {
                    this.roleHierarchySelected.push({ row: dataList[i] });
                    this.roleHierarchySelected[i]['selectedBoundries'] = [];
                    this.roleHierarchySelected[i]['selected'] = false;
                    this.roleHierarchySelected[i]['optional'] = false;
                    this.roleHierarchySelected[i]['customerFacing'] = false;
                    this.roleHierarchySelected[i]['sequence'] = 0;
                    this.roleHierarchySelected[i]['labelOnScreen'] = dataList[i].keyValue;
                }
            } else {
                this.performanceComponentSelected = [];
                for (let i = 0; i < dataList.length; i++) {
                    this.performanceComponentSelected.push({ row: dataList[i] });
                    this.performanceComponentSelected[i]['selected'] = false;
                    this.performanceComponentSelected[i]['sequence'] = 0;
                    this.performanceComponentSelected[i]['uploadFrequency'] = "";
                    this.performanceComponentSelected[i]['labelOnScreen'] = dataList[i].keyValue;
                }
            }
        }

        setTab(tabData) {
            this.activeTabData = tabData;
            this.currentTab = tabData.keyValue;
            this.subTab = tabData.keyValue;
            this.currentTabDisplay = tabData.labelOnScreen;
            this.subTabDisplay = tabData.labelOnScreen;
            this.subTabDisplayValue = tabData.keyValue;
            this.newKeyContainer = false;
            this.controlsArray = [];
            this.upperOUsControls = [];
            this.finalData = {

                startMonth: this.monthData[0].keyValue, endMonth: this.monthData[this.monthData.length - 1].keyValue, year: this.CFY, labelOnScreen: "", value: "", linkedBoundries: [],
            }
            this.httpService.postRequest<ConfigureMetaDataComponent>({ pageNumber: this.pageNo, metaDataLevel: 1, currentTab: this.currentTab, subTab: this.subTab }, '../ManageMetaData/getEmployeeAttributes', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.colorCode = [];
                        switch (tabData.keyValue) {                            
                            case "SalesHorizontal":
                            case "OperationalUnit":
                            case "BusinessVertical":
                                this.salesBoundriesListStr = [];
                                this.tabContentData = [];
                                this.salesBoundriesList = Object.assign([], datas.rsBody.attributesList);
                                this.tabContentData = Object.assign([], datas.rsBody.attributesList);
                                for (let i = 0; i < this.salesBoundriesList.length; i++) {
                                    this.tabContentData[i]['list'] = [];
                                    this.salesBoundriesListStr.push(this.salesBoundriesList[i].keyValue);
                                   // this.showRequiredAttributes(this.tabContentData[i]['keyValue'], i);
                                    //set the color corresponding to the sales horizontal values
                                    this.colorCode.push({ "key": this.tabContentData[i]['keyValue'], "color": this.shColor[i]});
                                }
                                this.heirarchyTree = [];
                                this.metaheirachy = [];
                                this.heirarchyTree = datas.rsBody.metaDataHeirarchy;
                                this.metaheirachy = datas.rsBody.heirarchy;
                                if (this.metaheirachy.length == 0) {
                                    $('#floatbtn').show();
                                }
                                this.ouList = datas.rsBody.ouDataList;
                                this.treeId = 'D3_Tree' + "_" + tabData.keyValue;
                                this.d3TreeFunction(this.heirarchyTree, this.colorCode, this.treeId);
                                break;

                            case "RoleHierarchy":
                                this.roleHierarchyList = Object.assign([], datas.rsBody.attributesList);
                                this.tabContentData = Object.assign([], datas.rsBody.attributesList);
                                this.restructureData(this.roleHierarchyList);
                                if (this.isLoadingFirst) {
                                    for (let i = 0; i < this.salesBoundriesList.length; i++) {
                                        if (this.attributesList[i].keyValue === "SalesComponent") {
                                            this.setTab(this.attributesList[i]);
                                            i = this.attributesList.length;
                                        }
                                    }

                                }
                                break;
                            case "SalesComponent":
                                this.performanceComponentList = Object.assign([], datas.rsBody.attributesList);
                                this.tabContentData = Object.assign([], datas.rsBody.attributesList);
                                this.restructureData(this.performanceComponentList);
                                if (this.isLoadingFirst) {
                                    for (let i = 0; i < this.attributesList.length; i++) {
                                        if (this.attributesList[i].keyValue === "BusinessVertical") {
                                            //this.setTab(this.attributesList[i]);
                                            i = this.attributesList.length;
                                        }
                                    }

                                }
                                break;                         
                                
                            default:
                                this.teamsList = Object.assign([], datas.rsBody.attributesList);
                                this.tabContentData = Object.assign([], datas.rsBody.attributesList);
                                this.getLowerLevelHierarchy("SalesHorizontal", this.salesBoundriesList[0], 0);
                                //this.getLowerLevelSalesVerticalHierarchy("SalesVertical", this.salesVerticalList[0], 0);
                                break;
                        }

                    }
                });
        }

        /*setSalesHorizontalCurrentLevel(data, i) {
            //i = i + 1;
            this.salesBoundriesHierarchyLevelData['level_' + i] = data.id;
            this.currenLevelData = Object.assign({}, data);

            this.httpService.postRequest<ConfigureMetaDataComponent>({}, '../ManageMetaData/GetDefaultRoleTemplate', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        for (var item of this.roleHierarchySelected) {
                            item.selectedBoundries = Object.assign([], datas.rsBody.linkedSB[item.labelOnScreen]);
                            item.selected = true;
                        }
                    }
                });
        }
        setSalesVerticalCurrentLevel(data, i) {
            //i = i + 1;
            this.salesVerticalsHierarchyLevelData['level_' + i] = data.keyValue;
            this.currenLevelData = Object.assign({}, data);
        }*/
        getLowerLevelHierarchy(type, upperObj, i) {
            this.salesBoundriesHierarchyLevelData.key = type;
            if (i == 0) {
                this.salesBoundriesHierarchyLevelData.upperPreferenceValue = i;
                this.salesBoundriesHierarchyLevelData.preferenceValue = i;
            } else {
                this.salesBoundriesHierarchyLevelData.upperPreferenceValue = i - 1;
                this.salesBoundriesHierarchyLevelData.preferenceValue = i;
            }

            this.httpService.postRequest<ConfigureMetaDataComponent>(this.salesBoundriesHierarchyLevelData, '../ManageMetaData/getSalesHorizontalHierarchy', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.salesBoundriesHierarchyLevelData['dataList']['level_' + i + 1 + '_list'] = datas.rsBody.list;
                        this.keys = Object.keys(this.salesBoundriesHierarchyLevelData['dataList']);
                    }
                });
        }
        /*getLowerLevelSalesVerticalHierarchy(type, upperObj, i) {
            this.salesVerticalsHierarchyLevelData.key = type;
            if (i == 0) {
                this.salesVerticalsHierarchyLevelData.upperPreferenceValue = i;
                this.salesVerticalsHierarchyLevelData.preferenceValue = i;
            } else {
                this.salesVerticalsHierarchyLevelData.upperPreferenceValue = i - 1;
                this.salesVerticalsHierarchyLevelData.preferenceValue = i;
            }
            this.httpService.postRequest<ConfigureMetaDataComponent>(this.salesVerticalsHierarchyLevelData, '../ManageMetaData/getSalesHorizontalHierarchy', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        this.salesVerticalsHierarchyLevelData['dataList']['level_' + i + 1 + '_list'] = datas.rsBody.list;
                        this.keys = Object.keys(this.salesVerticalsHierarchyLevelData['dataList']);
                    }
                });
        }*/
        setTreeNode(obj) {
            this.roleHierarchySelected.map((item) => {
                if (item.row.keyValue === obj.keyValue) {
                    if (item.selected) {
                        item.selected = !item.selected;
                    } else {
                        item.selected = !item.selected;
                    }
                }
            });
        }

       /* showRequiredAttributes(lv, index) {

            this.subTab = lv;
            this.pageNo = 0;
            /*this.finalData = {
                startMonth: "0", endMonth: "0", labelOnScreen: "", value: ""
            }
            switch (this.currentTab) {
                case 'SalesHorizontal':

                    this.httpService.postRequest<ConfigureMetaDataComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageMetaData/GetExistingMetaData', true).subscribe(
                        datas => {
                            if (datas.rsBody.result == 'success') {
                                this.tabContentData[index]['list'] = datas.rsBody.existingList;
                            }
                        });
                    break;
                case 'SalesVertical':

                    this.httpService.postRequest<ConfigureMetaDataComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageMetaData/GetExistingMetaData', true).subscribe(
                        datas => {
                            if (datas.rsBody.result == 'success') {
                                this.tabContentData[index]['list'] = datas.rsBody.existingList;
                            }
                        });
                    break;
                case 'OperationalUnit':

                    this.httpService.postRequest<ConfigureMetaDataComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageMetaData/GetExistingMetaData', true).subscribe(
                        datas => {
                            if (datas.rsBody.result == 'success') {
                                this.tabContentData[index]['list'] = datas.rsBody.existingList;
                            }
                        });
                    break;
                case 'SalesComponent':
                    break;
                case 'RoleHierarchy':
                    break;
                default:
                    break;
            }

        }*/

        AddMetaDataKey(keyValue) {
            $('#addNewAttribute').modal();
            this.newKeyContainer = true;

            // setting prefrence value for newly created metadata 
            if (this.tabContentData.length > 0) {
                this.finalData['preferenceValue'] = keyValue.preferenceValue + 1;
            } else {
                this.finalData['preferenceValue'] = 0;
            }

            /*switch (this.currentTab) {
                case 'TeamSetup':
                    this.httpService.postRequest<ConfigureMetaDataComponent>({}, '../ManageMetaData/GetDefaultRoleTemplate', true).subscribe(
                        datas => {
                            if (datas.rsBody.result == 'success') {
                                for (var item of this.roleHierarchySelected) {
                                    item.selectedBoundries = Object.assign([], datas.rsBody.linkedSB[item.labelOnScreen]);
                                }
                            }
                        });
                    break;
                case 'SalesHorizontal':

                    break;
                case 'OperationalUnit':
                    if (this.currentTab != this.subTab) {
                        this.httpService.postRequest<ConfigureMetaDataComponent>({ currentTab: this.currentTab, subTab: this.subTab, pageNumber: this.pageNo }, '../ManageMetaData/GetLinkedOUs', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.upperOUsControls = datas.rsBody.upperOUs;
                                   // this.getUpperOUsList(this.upperOUsControls[0]);
                                }
                            });
                    }
                    break;
                default:

                    break;

            }*/

        }

        RemoveMetaDataKey(keyValue)
        {
            this.finalData.key = this.currentTab;
            this.finalData.keyName = keyValue.name;
            this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/RemoveMetaDataKey', true).subscribe(
                datas => {
                    this.setTab(this.activeTabData);
                });
        }

        addNewValueContainer(keyValueData) {

            // setting prefrence value for newly created metadata       
            this.finalData['preferenceValue'] = keyValueData.preferenceValue;
            this.finalData['key'] = this.currentTab;
            this.finalData['subKey'] = keyValueData.keyValue;
            this.subTabDisplay = keyValueData.keyValue;
            this.salesBoundriesList = [];
            this.controlsArray = [];

            //Get upper heirarchy of current heirarchy
            this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperHeirarchyList', true).subscribe(
                datas => {
                    if (datas.rsBody.message == 'Success') {
                        this.salesBoundriesList = datas.rsBody.result;
                        for (let i = 0; i < this.salesBoundriesList.length; i++) {
                            this.controlsArray.push(this.salesBoundriesList[i]);
                            this.finalData[this.salesBoundriesList[i].modalKey] = "";
                        }
                        this.dataObj = datas.rsBody.level;
                        $('.controlArray').prop('disabled', 'disabled');
                        $('#addNewAttributeValue').modal();
                    }
                });
        }

        clearModelData() {
            //$('#addNewAttribute').modal().dispose();
            $('#addNewAttribute').modal('hide').data('bs.modal', null);
        }

       /* getUpperOUsList(firstSB) {
            if (firstSB != undefined) {
                switch (firstSB.keyValueModel) {
                    case "OU1":
                        this.finalData['key'] = firstSB.keyValue;

                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    case "OU2":
                        this.finalData['key'] = firstSB.keyValue;

                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    case "OU3":
                        this.finalData['key'] = firstSB.keyValue;
                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    case "OU4":
                        this.finalData['key'] = firstSB.keyValue;
                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    case "OU5":
                        this.finalData['key'] = firstSB.keyValue;
                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    case "OU6":
                        this.finalData['key'] = firstSB.keyValue;
                        this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetUpperOUsList', true).subscribe(
                            datas => {
                                if (datas.rsBody.result == 'success') {
                                    this.dataObjOUs[firstSB.keyValue] = datas.rsBody.attributesList;
                                    $('#' + firstSB.keyValue).removeAttr('disabled');
                                }
                            });
                        break;
                    default:
                        break;
                }
            }
        }   */

    getRequiredAttributesList(serialNumber) {

        this.finalData.upperPreferenceValue = 0;
        this.finalData.attributeSequence = this.heirarchyStr;
        this.finalData["upperPreferenceValue"] = serialNumber.preferenceValue;
            if (this.finalData["upperPreferenceValue"] != this.finalData["referenceValue"]) {                
                    this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/GetSalesHorizontalHierarchy', true).subscribe(
                        datas => {
                            if (datas.rsBody.result == 'success') {
                                this.dataObj[this.finalData["upperPreferenceValue"]].value = datas.rsBody.list;
                            }
                        })
            }
        }

        ngCreateMetaDataKey() {
            this.finalData.keyType = this.subTabDisplayValue;
            this.httpService.postRequest<ConfigureMetaDataComponent>(this.finalData, '../ManageMetaData/CreateMetaData', true).subscribe(
                datas => {
                    if (datas.rsBody.result == 'success') {
                        $('#addNewAttribute').modal('hide');
                        this.setTab(this.activeTabData);
                        $('#success').modal();
                    }
                })
        }

        ngCreateMetaDataValue() {

            this.finalData.key = this.currentTab;
            this.metaDataValue.newNode = this.finalData;
            this.metaDataValue.newNode.attributeSequence = this.heirarchyStr;
            this.httpService.postRequest<ConfigureMetaDataComponent>(this.metaDataValue, '../ManageMetaData/SaveMetaDataTreeValue', false).subscribe(
                datas => {
                    $('#addNewAttributeValue').modal('hide').data('bs.modal', null);
                    this.setTab(this.activeTabData);
                });
        }

        ngAfterViewInit(): void {

        }

        ngOnDestroy(): void {

        }
        leftSlider() {
            if (this.leftCount != 0) {
                $('#li-' + this.attributesList[this.leftCount - 1].keyValue + "-tab").removeClass('d-none');
                this.leftCount -= 1;
            }

        }
        rightSlider() {

            if ((this.attributesList.length - this.leftCount) <= 6) {

            }
            else {
                $('#li-' + this.attributesList[this.leftCount].keyValue + "-tab").addClass('d-none');
                this.leftCount += 1;
            }
        }




}