import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigureMetaDataComponent } from './configuremetadata.component';

const routes: Routes = [
    { path: '', component: ConfigureMetaDataComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigureMetaDataRoutingModule { }
