﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { FileUploader } from 'ng2-file-upload';
import { ModalService } from '../../service/modelservice';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'OpUpload';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class OpUploadComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private modal: ModalService,
        private route: ActivatedRoute) {
    }
    keyValueData: any = {};
    requestData: any = {};
    opData: any = {};
    employeeMaster: any = {};
    incentiveData: any = {};
    ChannelList: any = [];
    year: string = '';
    dataSourceFileName: string = '';
    subChannelList: string = '';
    roleList: string = '';
    actualTypeList: string = '';
    weightageList: string = '';
    uploader: FileUploader = new FileUploader({ url: '../ManageUploadData/UploadMasterSheet' });
    incentive: any = [];
    getNextEmployeeList: any;
    pageNumber: any;
    incentivList: any = {};
    listData: any = [];
    id: any;
    getPage: any;
    yearData: any = [];
    monthData: any = [];
    responseData: any = {};
    downloadFile: any = {};
    error: any = {};
    File: any = {};
    parameters: any = {};
    filename: any;
    success: any;
    result: any;
    lines: any[] = [];
    lines2: any = [];
    errorList: any = [];
    warningList: any = [];
    reason: any;
    ngOnInit() {
        this.getNextEmployeeList = 0;
        this.getPage = 0;
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<OpUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {

                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });


        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<OpUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.yearData = data.rsBody.resulats;
            });

        //getMonth List			
        this.getMonthList();

    }

    getMonthList() {
        this.keyValueData = {};
        this.keyValueData.keyType = "QUARTER";
        this.httpService.postRequest<OpUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {

                this.monthData = data.rsBody.resulats;
            });

        this.keyValueData.keyType = "TARGETKEY";
        this.httpService.postRequest<OpUploadComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                this.opData = data.rsBody.resulats;
            });
    }

    ngOnEmployeeMasterSubmit() {
        this.employeeMaster.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<OpUploadComponent>(this.employeeMaster, '../ManageUploadData/UploadMasterData', true).subscribe(
            data => {

                if (data == undefined) {
                    var id = data.rsBody.data;
                }
                else if (data.rsBody.error != null) {
                    $('#consolePanel').removeClass('d-none');
                    this.errorList.push(data.rsBody.error);
                }
                else
                {
                    this.parameters.id = data.rsBody.data;
                    this.parameters.fileName = data.rsBody.fileName;
                    this.parameters.filePath = data.rsBody.filePath;
                    this.parameters.uploadType = data.rsBody.uploadType;
                    if (data.rsBody.fileName.search("OP")) {
                        this.parameters.quarter = data.rsBody.quarter;
                    }
                    else {
                        this.parameters.quarter = "fullYear";
                    }
                    if (this.parameters.fileName != null) {
                        this.errorHandler(this.parameters);
                    }
                }
                this.responseData.id = id;              
            });
    }

    ngOnCheckStatus(inputData)
    {
        this.httpService.postRequest<OpUploadComponent>(inputData, '../ManageUploadData/GetUploadStatus', true).subscribe(
            data => {
                this.responseData = data.rsBody;
                if (this.responseData != "" && this.responseData != null) {
                    this.employeeMaster = {};
                    this.router.navigate(['target-upload']);
                    this.modal.open('sucessPopup')
                    this.ngOnInit();
                }
                else {                   
                    this.ngOnCheckStatus(inputData);
                }
            });
    }

    ngOnDeleteRequest(target)
    {
        this.employeeMaster = this.listData[target];
        this.employeeMaster.requestType = 'OperationalPlanDataPort';
        this.httpService.postRequest<OpUploadComponent>(this.employeeMaster, '../ManageUploadData/CancelUploadRequest', true).subscribe(
            data => {
                this.error = data.rsBody;
                this.modal.open('error');
                this.ngOnInit();
            });
    }

    ngOnGetNext() {
        this.getPage = this.getPage + 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<OpUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {                
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    ngOnGetPrevious() {
        if (this.getPage == 0) {
            $('#example1_previous').prop('disabled', true);
        }
        this.getPage = this.getPage - 1
        this.requestData.page = this.getPage;
        this.requestData.requestType = "OperationalPlanDataPort";
        this.httpService.postRequest<OpUploadComponent>(this.requestData, '../ManageUploadData/ListMasterUploadData', true).subscribe(
            data => {
                this.listData = data.rsBody.data;
                if (this.listData == null) {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                // next and previous 
                if (this.listData.length < 5) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getPage > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getPage == 0) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide")
                }
            });   
    }

    modalclose(id) {
        this.modal.close(id);
    }

    ngAfterViewInit() {

    }

    fileSelect()
    {
        $('#imageUpload').click();
    }			

    imageUpload()
    {
        this.uploader.uploadAll();
        //Get response
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) =>
        {
	        var responsePath = JSON.parse(response);
	        {
			        this.employeeMaster.dataSourceFileName = responsePath.rsBody;
		    };
	    }
    }

    ngOnTemplateDownload() {

        var triggerClickk = setTimeout(function () {
            clearTimeout(triggerClickk);
            $('#downloadReport')[0].click();
        });
    }

    ngOnDownloadSourceReport(data) {
        this.downloadFile = data;
        $('#DownloadSourcePath').attr('href', '../Comman/DownloadOpSourceReport?name=' + this.downloadFile);
        $('#DownloadSourcePath')[0].click();
    }

    ngOnDownloadErrorReport(getData) {
        this.downloadFile = getData;
        this.httpService.postRequest<OpUploadComponent>(this.downloadFile, '../ManageUploadData/CheckFileExistOrNot', true).subscribe(
            data => {
                this.result = data;
                if (this.result == "NotExist") {
                    this.modal.open('notFound');
                    this.filename = "";
                }

                else {
                    $('#DownloadErrorPath').attr('href', '../Comman/DownloadOpErrorReport?name=' + this.downloadFile);
                    $('#DownloadErrorPath')[0].click();
                }
            });   
    }

    ngOnDestroy() {
    }

    closePanel(): void {

        $('#consolePanel').addClass('d-none');
    }
    errorHandler(parameter) {
        this.File.fileName = parameter.fileName;
        this.File.filePath = parameter.filePath;
        this.File.id = parameter.id;
        this.File.uploadType = parameter.uploadType;
        this.File.quarter = parameter.quarter;
        this.httpService.postRequest<OpUploadComponent>(this.File, '../UploadData/Errorhandler', true).subscribe(
            data => {

                if (data.rsBody.result == 'success') {
                    $('#consolePanel').removeClass('d-none');
                    //this.lines = null;
                    if (data.rsBody.error == null) {
                        this.lines2 = data.rsBody.lines;
                        this.errorList = data.rsBody.errorList;
                        if (this.errorList.length == 0) {
                            this.success = data.rsBody.success;
                            $('#successBlock').removeClass('d-none');
                        }
                        for (var i = 0; i <= this.lines2.length; i++) {
                            this.lines.push(this.lines2[i]);
                            //this.sub = Observable.interval(10000)
                            //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                        }
                        this.warningList = data.rsBody.warningList;
                    }
                    else {
                        //this.lines = null;
                        this.lines2 = data.rsBody.lines;
                        for (var i = 0; i <= this.lines2.length; i++) {
                            this.lines.push(this.lines2[i]);
                            //this.sub = Observable.interval(10000)
                            //  .subscribe((val) => { this.lines.push(this.lines2[i]) });
                        }
                        this.errorList = data.rsBody.errorList;
                        if(this.errorList.length > 200)
                        {
                            this.errorList = [];
                            this.errorList.push("Number of error are too high to handle please first correct your data sheet than proceed");
                        }
                        this.warningList = data.rsBody.warningList;
                        this.reason = data.rsBody.error;
                    }


                }
            });
    }

}
