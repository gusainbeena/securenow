import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpUploadComponent } from './opupload.component';

const routes: Routes = [
    { path: '', component: OpUploadComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OpUploadRoutingModule { }
