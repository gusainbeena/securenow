import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyDashboardComponent } from './mydashboard.component';

const routes: Routes = [
    { path: '', component: MyDashboardComponent },
    { path: 'my-dashboard', component: MyDashboardComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MyDashboardRoutingModule { }
