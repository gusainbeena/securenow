import { Injectable } from '@angular/core';

import { MultiBarGraph } from '../bargraph/bargraph.component';
//import { LineGraphComponent } from '../linegraph/linegraph.component';
import { PieChartComponent } from '../piechart/piechart.component';
import { AdItem } from './ad-item';

@Injectable()
export class AdService {
  getAds(type: string, data:any) {
      switch (type) {
          case "bar":
              return new AdItem(MultiBarGraph, data);
          case "pie":
              return new AdItem(PieChartComponent, data);
          case "line":
              return new AdItem(MultiBarGraph, data);
          case "BTG":
              return new AdItem(MultiBarGraph, data);
          case "gauge":
              return new AdItem(MultiBarGraph, data);
          default:
              break;

      }
   
  }
}
