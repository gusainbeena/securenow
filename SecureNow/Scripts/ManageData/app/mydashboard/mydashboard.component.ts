﻿import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef, Inject, ViewContainerRef, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonModule } from "@angular/common";
import { HttpService } from '../../service/http.service';
import { Global } from '../../service/global.service';
import { AdService } from './ad.service';
import { AdItem } from './ad-item';
import { AdBannerComponent } from './ad-banner.component';
import { NvD3Module } from 'ng2-nvd3';
import * as d3 from 'd3';
import * as c3 from 'c3';
import * as d3hierarchy from 'd3-hierarchy';

declare var $: any;
declare var liquidFillGaugeDefaultSettings: any;
declare var loadLiquidFillGauge: any;
import '../../../customChart-js/liquidGauge.js';
import { Console } from '@angular/core/src/console';

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../SecureNow/Views');
var _templateURL = 'MyDashboard';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class MyDashboardComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    

    public SalesHorizontalList: any = [];
    public empDeatilsHistry: any = [];
    public tabularList: any = [];
    public isSpinner: boolean = true;  
    public breadCrums: any;
    public subArea: any;
    public period: string = "";
    public quarter: string = "";
    public ytdMonth: string = "";
    public ftmMonth: string = "";
    public month: string = "";
    public fullName: string = "";
    public id: any;
    public chartDy: string = "";
    public indexData: any;
    public emp: any;
    public radioSelected: string = "";
    public empList: any = {};
    public adpId: string = "";
    public role: string = "";
    constructor(private httpService: HttpService,
        private router: Router,
        private adService: AdService,
        private globalVar: Global) {


    }

    public keys: Array<string> = [];
    public graphData: any = [];
    public filterData: any = {};
    ngOnInit() {
        
        this.goGetUserDetails();
        
      
        setTimeout(() => {
            this.goGetUserDashboardData(this.radioSelected,this.adpId);
        }, 2000);
      
    }
    goGetUserDetails() {
        this.httpService.postRequest<MyDashboardComponent>({ country: "" }, '../Account/GetCurrentLoginUserDetails', this.isSpinner).subscribe(
            data => {
                this.fullName = data.rsBody.data.fullName;
                this.empDeatilsHistry = data.rsBody.empList;
                this.radioSelected = this.empDeatilsHistry[0].role;
                this.adpId = this.empDeatilsHistry[0].adpId;

            });
    }
    goGetUserDashboardData(role,adpId) {
        this.filterData.period = this.period;
        this.filterData.month = this.month;
        
        this.filterData.role = role;
        this.filterData.adpId = adpId;
        var that = this;
       
        this.httpService.postRequest<MyDashboardComponent>(this.filterData, '../MyDashboard/GetDashboardData', this.isSpinner).subscribe(
            data => {
                this.SalesHorizontalList = data.rsBody.finalData;
               this.tabularList =  this.SalesHorizontalList.tabularData;
                if (this.SalesHorizontalList.employee.length > 4) {
                    $("#six").css('display', 'block');
                    $("#four").css('display', 'none');
                    let barChart = c3.generate({
                        bindto: '#barChartSix',
                        data: {
                            columns: [
                                this.SalesHorizontalList.target,
                                this.SalesHorizontalList.actual
                            ],
                            onclick: function (d, i) {

                                this.indexData = d.index;
                                that.breadCrums = that.breadCrums + ":" + that.SalesHorizontalList.employeeDataList[this.indexData].adpId;
                                that.goGetUserDashboardData(that.SalesHorizontalList.employeeDataList[this.indexData].role, that.SalesHorizontalList.employeeDataList[this.indexData].adpId);
                            },
                            /*onclick: function (event, d) {
                                this.indexData = d.index;
                              //  this.clickhandler(event, this.indexData)
                            },*/
                            type: 'bar',
                            labels: true,
                            colors: {
                                Target: '#006294',
                                Actual: '#ed5a24'
                            }
                        },
                        bar: {
                            width: {
                                ratio: 0.5 // this makes bar width 50% of length between ticks
                            }
                        },
                        grid: {
                            x: {
                                show: true
                            },
                            y: {
                                show: true
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: this.SalesHorizontalList.employee
                            }
                        },
                    });

                    setTimeout(() => {
                        for (let i = 0; i < this.SalesHorizontalList.achievement.length; i++) {

                            var gaugeChart = c3.generate({
                                bindto: '#gaugeChartSix' + i,
                                data: {
                                    columns: [
                                        [this.SalesHorizontalList.employee[i], this.SalesHorizontalList.achievement[i]]
                                    ],
                                    type: 'gauge',
                                    colors: {
                                        Target: '#006294'
                                    }
                                },
                                gauge: {
                                    label: {
                                        format: function (value, ratio) {
                                            return value; //returning here the value and not the ratio
                                        },
                                    },
                                    min: 50,
                                    max: 100,
                                    units: '%' //this is only the text for the label
                                }

                            });
                        }
                    }, 100);
                }
                else {
                    $("#six").css('display', 'none');
                    $("#four").css('display', 'block');
                    let barChart = c3.generate({
                        bindto: '#barChartFour',
                        data: {
                            columns: [
                                this.SalesHorizontalList.target,
                                this.SalesHorizontalList.actual
                            ],
                            onclick: function (d, i) {

                                this.indexData = d.index;
                                that.breadCrums = that.breadCrums + ":" + that.SalesHorizontalList.employeeDataList[this.indexData].adpId;
                                that.goGetUserDashboardData(that.SalesHorizontalList.employeeDataList[this.indexData].role, that.SalesHorizontalList.employeeDataList[this.indexData].adpId);
                            },
                            /*onclick: function (event, d) {
                                this.indexData = d.index;
                              //  this.clickhandler(event, this.indexData)
                            },*/
                            type: 'bar',
                            labels: true,
                            colors: {
                                Target: '#006294',
                                Actual: '#ed5a24'
                            }
                        },
                        bar: {
                            width: {
                                ratio: 0.5 // this makes bar width 50% of length between ticks
                            }
                        },
                        grid: {
                            x: {
                                show: true
                            },
                            y: {
                                show: true
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: this.SalesHorizontalList.employee
                            }
                        },
                    });

                    setTimeout(() => {
                        for (let i = 0; i < this.SalesHorizontalList.achievement.length; i++) {

                            var gaugeChart = c3.generate({
                                bindto: '#gaugeChartFour' + i,
                                data: {
                                    columns: [
                                        [this.SalesHorizontalList.employee[i], this.SalesHorizontalList.achievement[i]]
                                    ],
                                    type: 'gauge',
                                    colors: {
                                        Target: '#006294'
                                    }
                                },
                                gauge: {
                                    label: {
                                        format: function (value, ratio) {
                                            return value; //returning here the value and not the ratio
                                        },
                                    },
                                    min: 50,
                                    max: 100,
                                    units: '%' //this is only the text for the label
                                }

                            });
                        }
                    }, 100);
                }
                if (this.breadCrums == null || this.breadCrums == "") {
                    this.breadCrums = this.SalesHorizontalList.Param;
                }
                this.radioSelected = data.rsBody.finalData.empParam.role;
                
                

                let lineChart = c3.generate({
                    bindto: '#lineChart',
                    data: {
                        columns: [
                            this.SalesHorizontalList.actual
                        ],
                        colors: {
                            Sales: '#970747'
                        }
                    },
                    grid: {
                        x: {
                            show: true
                        },
                        y: {
                            show: true
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: this.SalesHorizontalList.employee
                        }
                    }
                });

            });

    }
    
    empHistrySubmit(adpId, role) {

        this.goGetUserDashboardData(role,adpId);
    }
  
    
    //for ytd data                           
    YTDData() {

        this.period = "YTD"
        this.month = this.ytdMonth;
        this.ngOnInit();
    }
    //for selected  quarter
    QtrData() {

        this.period = "QTD"
        this.month = this.quarter;
        this.ngOnInit();
       
    }
    //for ftm
    FTMData() {

        this.period = "FTM"
        this.month = this.ftmMonth;
        this.ngOnInit();
    }

    goGetAllData() {

        this.httpService.postRequest<MyDashboardComponent>({ country: "" }, '../MyDashboard/GetDataBasedOnFilters', this.isSpinner).subscribe(
            data => {
                this.SalesHorizontalList = data.rsBody;
                
            });
    }
  
     
    ngAfterViewInit() {

      
       
    }

    ngOnDestroy() {
    }
}