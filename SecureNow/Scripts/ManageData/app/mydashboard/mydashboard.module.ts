import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { MyDashboardRoutingModule } from './mydashboard-routing.module';
import { MyDashboardComponent } from './mydashboard.component';
import { AdBannerComponent } from './ad-banner.component';
import { AdDirective } from './ad.directive';
import { AdService } from './ad.service';
import { MultiBarGraph } from './../bargraph/bargraph.component';
import { PieChartComponent } from './../piechart/piechart.component';
//import { LineGraphComponent } from './../linegraph/linegraph.component';
@NgModule({
    declarations: [
        MyDashboardComponent, AdBannerComponent, AdDirective, MultiBarGraph, PieChartComponent
    ],
    imports: [
        CommonModule,
        MyDashboardRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule,        
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService, AdService],
    bootstrap: [MyDashboardComponent],
    entryComponents: [MultiBarGraph, PieChartComponent],
    exports: []
})
export class MyDashboardModule { }
