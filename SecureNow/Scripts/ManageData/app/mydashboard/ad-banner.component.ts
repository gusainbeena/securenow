import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, OnDestroy } from '@angular/core';

import { AdDirective } from './ad.directive';
import { AdItem }      from './ad-item';
import { AdComponent } from './ad.component';

@Component({
  selector: 'app-ad-banner',
  template: `
              <ng-template ad-host></ng-template>
            `
})
export class AdBannerComponent implements OnInit, OnDestroy {
  @Input() ads: AdItem;
  currentAdIndex = -1;
  @ViewChild(AdDirective) adHost: AdDirective;
  interval: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
    //this.getAds();
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  loadComponent() {
    let adItem = this.ads;

    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(adItem.component);
      
    let viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

      let componentRef = viewContainerRef.createComponent(componentFactory);
      (<AdComponent>componentRef.instance).containerElement = adItem.data.containerElement;
      (<AdComponent>componentRef.instance).graphData = adItem.data.graphData;
      (<AdComponent>componentRef.instance).graphXAxis = adItem.data.graphXAxis;
  }

  getAds() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }
}
