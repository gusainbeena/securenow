import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApproveActualUploadComponent } from './approveactualupload.component';

const routes: Routes = [
    { path: '', component: ApproveActualUploadComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApproveActualUploadRoutingModule { }
