﻿import { Component, OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';


declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewActualApprovalList';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})



export class ApproveActualUploadComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    //public keyValueData: Object = {};
    public keyValueData: any = {};
    public shipToRequire: any = {};
    public customerDeatils: any = {};
    public custDetials: any = {};
    public customerTargets: any = [];
    public customerRelationship: any = {};
    public responseData: any = [];
    public getNextCustomerList: any;
    public getPreviousCustomerList: any = {};
    public responseStateData: any = [];
    public responseRelationData: any = {};
    public yearData: any = [];
    public yearRecorded: any =[];
    public dataTypeList: any = [];
    public BUData: any = [];
    public responseChannelData: any = [];
    public monthData: any = [];
    public regionData: any = {};
    public actualData: any = {};
    public searchByBranch: any = {};
    public customerDetails: any = {};
    public customerBaseDetails: any = [];
    public soldToPartyCodes: any = [];
    public targetList: any = [];
    public targetData: any = [];
    public secondaryTargetList: any = {};
    public sucessResponseData: any = {};
    public getList: any = {};
    public subChannelList: any = [];
    public regionList: any = {};
    public branchListData: any = {};
    public subAreaData: any = [];
    public responseAreaData: any = [];
    public employeeKAMData: any = {};
    public employeeData: any = [];
    public getEmployeeList: any = {};
    public shipToPartyCodes: any = {};
    public lengthdata: any = {};
    public customerBaseDetailsFK: any = {};
    public deleteRel: any = {};
    public sucessData: any = {};
    public inputData: any = {};
    public errorMessage: any;
    public gradeData: Object = {};
    public doaStr: any = {};
    public success: string = '';
    public id: any;
    public customerId: any;
    public status: any;
    public date: string = ''; 
    public employeeActual: any = {};
    public searchByName: any = {};
    
   
    ngOnInit() {
       
            this.custDetials = {
            customerDeatils: {},
            customerRelationship: {},
            customerTargets: []

        };
        this.doaStr =
         {
           
            year: <string>null,
            month: <string>null,
            day: <string>null
         };

        
        $("#submit-basic-button").css('display', 'none');
        $(".h3-edit").css('display', 'block');
        $(".h3-create").css('display', 'none');       
        $(".viewCustTar").css('display', 'block');
        $(".addNewCustTar").css('display', 'block');
        $("#update-basic-button").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        this.getNextCustomerList = 0;
       
            this.httpService.postRequest<ApproveActualUploadComponent>({}, '../ManageUploadData/GetActualRequestList', true).subscribe(
                datas => {
                    
                    if (datas.rsBody.showButton == true) {
                        setTimeout(() => {
                            for (let i = 0; i < this.responseData.length; i++) {
                                $("#showButtonTD" + i).css('display', 'block');
                            }
                        }, 1000);
                        $(".showButtonTH").css('display', 'block');
                    }
                    else {

                        
                        $(".showButtonTH").css('display', 'none');
                        setTimeout(() => {
                            for (let j = 0; j < this.responseData.length; j++) {
                                $("#showButtonTD" + j).css('display', 'none');
                            }
                        }, 100);
                    }

                    if (datas.rsBody.data.length > 0) {
                        this.responseData = datas.rsBody.data;
                        $("#row_not_found_id").css('display', 'none');

                    }
                    else {
                        $("#row_not_found_id").css('display', 'block');
                    }
                    for (let i = 0; i < this.responseData.length; i++) {

                        if (this.responseData[i].status != "WAITING FOR APPROVAL") {
                            setTimeout(() => {
                               
                                    $("#showButtonTD" + i).css('display', 'none');
                                
                            }, 1000);
                        }
                    }

                });

    }   
    

    approveActualRequest(index) {
        this.employeeData =  this.responseData[index];
        this.httpService.postRequest<ApproveActualUploadComponent>(this.employeeData, '../ManageUploadData/ApproveActualRequest', true).subscribe(
            datas => {
               this.ngOnInit();

            });
    }

    rejectActualRequest(index) {
        this.employeeData = this.responseData[index];
        this.httpService.postRequest<ApproveActualUploadComponent>(this.employeeData, '../ManageUploadData/RejectActualRequest', true).subscribe(
            datas => {
                this.ngOnInit();

            });
    }

    searchData() {
        this.httpService.postRequest<ApproveActualUploadComponent>(this.searchByName, '../ManageUploadData/GetActualRequestList', true).subscribe(
            datas => {
            });
    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
