import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ApproveActualUploadRoutingModule } from './approveactualupload-routing.module';
import { ApproveActualUploadComponent } from './approveactualupload.component';
import { TreeviewModule } from 'ngx-treeview';
@NgModule({
    declarations: [
        ApproveActualUploadComponent
    ],
    imports: [
        CommonModule,
        ApproveActualUploadRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        TreeviewModule.forRoot(),
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ApproveActualUploadComponent],
    exports: []
})
export class ApproveActualUploadModule { }
