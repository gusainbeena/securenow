﻿import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonModule } from "@angular/common";
import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { BaseContainer } from '../../BaseContainer';
import * as d3 from 'd3';
import { Http } from '@angular/http';
declare var $: any;


var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'CreateEmployee';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class CreateEmployeeComponent implements OnInit, OnDestroy, AfterViewInit {   
    rsBody: any;
    responseChannelData: any;
    monthData: any = [];
    gradeData: any = [];
    yearData: any = [];
    employeeBaseDetails: any = {};
    roleYear: any = [];
    keyValueData: any = {};
    requiredFlag: boolean;
    roleList: any = [];
    subChannelList: any = [];
    selectVal: string = "";
    regionList: any = [];
    areaList: any = [];
    subAreaList: any = [];
    employeeDetials: any = {};
    employeeData: any = {};
    responseEmpData: any;
    customerData: any;    
    channelCode: any= [];
    responseEmpsData: any;
    keyvalue: any = {};

    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute,
        private cdref: ChangeDetectorRef,
        private modal: ModalService) {
    }  

    ngOnInit() {
        //Remove CSS
       // $("#create-h-id").css('display', 'block');
        $(".form-steps").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        
        //get channel data
        this.keyValueData.keyType = "CHANNEL";
        this.keyValueData.CHANNEL = "CHANNEL";
        this.httpService.postRequest<CreateEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
            data =>{
                this.responseChannelData = data.rsBody.resulats;                
                this.ngOnMonth();
            }); 
    }

    ngOnMonth()
    {
        //get month data
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<CreateEmployeeComponent>(this.keyValueData, '../Comman/GetMonthKeyValueData',false).subscribe(
            data => {
                this.monthData = data.rsBody.resulats;   
                this.ngOnGrade();
            });
    }

    ngOnGrade()
    {
           
            //get grade data
            this.keyValueData.keyType = "GRADE";
        this.httpService.postRequest<CreateEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
                data => {
                    this.gradeData = data.rsBody.resulats;    
                    this.ngOnYear();
            });
    }

    ngOnYear()
    {
        //get year data
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<CreateEmployeeComponent>(this.keyValueData, '../ManageEmployee/GetKeyValueDataEmpCreate',false).subscribe(
            data => {
                this.yearData = data.rsBody.resulats;
                this.employeeBaseDetails.yearRecorded = this.yearData[0].keyValue;
                this.employeeBaseDetails.status = "ACTIVE";
                this.roleYear = this.yearData[0].keyValue;
                $(".remove-disable").prop('disabled', false);
            });
    }

    ngOnGetRequiredField(type, targetIndex) {
        this.ngOnChangeGetListInfo(type);
    }

    ngOnChangeGetListInfo(type) {
        //get year data
        this.selectVal = type;	
        for (var i = 0; i < this.employeeDetials.employeeRole.length; i++) {
            this.employeeDetials.employeeRole[i].type = type;
        }
        this.httpService.postRequest<CreateEmployeeComponent>(this.employeeDetials.employeeRole, '../ManageEmployee/GetRelatedParam',true).subscribe(
            data => {
                if (data.rsBody.type == "ROLE") {
                    this.roleList = data.rsBody.result;
                    if (this.roleList != null) {
                        $("#employeeRole").css('display', 'block');
                    }
                    else {
                        $("#employeeRole").css('display', 'none');
                    }
                }
                $("#teritoryCodeData").css('display', 'none');
                $("#employeeSubChannel").css('display', 'none');
                $("#employeeRegion").css('display', 'none');
                $("#employeeArea").css('display', 'none');
                $("#employeeSubArea").css('display', 'none');
                this.requiredFlag = false;
                if (data.rsBody.result.length > 0) {
                    if (data.rsBody.result.indexOf("SUBCHANNELREQUIRED") > -1) {
                        $("#employeeSubChannel").css('display', 'block');
                        this.subChannelList = data.rsBody.paramValue.subChannelList;
                        if (this.selectVal == "") {
                            this.regionList = [];
                            this.areaList = [];
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("REGIONREQUIRED") > -1) {
                        $("#employeeRegion").css('display', 'block');
                        if (this.selectVal == "SUBCHANNELREQUIRED") {
                            this.regionList = data.rsBody.paramValue.regionList;
                            this.areaList = [];
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("AREACODEREQUIRED") > -1) {
                        $("#employeeArea").css('display', 'block');
                        $("#teritoryCodeData").css('display', 'block');
                        if (this.selectVal == "REGIONREQUIRED" || this.selectVal == "SUBCHANNELREQUIRED") {
                            this.areaList = data.rsBody.paramValue.areaList;
                            this.subAreaList = [];
                        }
                    }
                    if (data.rsBody.result.indexOf("SUBAREACODEREQUIRED") > -1) {
                        $("#employeeSubArea").css('display', 'block');
                        $("#teritoryCodeData").css('display', 'block');
                        if (this.selectVal == "AREACODEREQUIRED") {
                            this.subAreaList = data.rsBody.paramValue.subAreaList;
                        }
                    }
                }
            });
    }   

    ngOnemployeeBasicDetailsSubmit() {
        this.httpService.postRequest<CreateEmployeeComponent>(this.employeeBaseDetails, '../ManageEmployee/ValidateEmployeeBasicDetails',true).subscribe(
            data => {
                this.employeeData = data.rsBody;
                $("#step-one").css('display', 'none');
                $(".step-one").removeClass('progress-bar-success');
                $(".step-one").addClass('progress-bar-inverse');
                $(".step-two").addClass('progress-bar-success');
                $(".step-two").removeClass('progress-bar-inverse');
                $("#step-twos").css('display', 'block');
                if (this.employeeDetials.employeeRole == undefined) {
                    this.employeeDetials.employeeRole = [{
                        yearRecorded: this.roleYear
                    }];
                }
            });
    }

    ngOnpreviouSubmit()
    {
        $("#step-twos").css('display', 'none');
        $(".step-two").removeClass('progress-bar-success');
        $(".step-two").addClass('progress-bar-inverse');
        $(".step-one").addClass('progress-bar-success');
        $(".step-one").removeClass('progress-bar-inverse');
        $("#step-one").css('display', 'block');
    }

    ngOnemployeeDetailsSubmit()
    {
        this.employeeDetials.employeeBaseDetail = this.employeeData;
        this.employeeDetials.type = "create";
        this.httpService.postRequest<CreateEmployeeComponent>(this.employeeDetials, '../ManageEmployee/CreateEmployeeDetails',true).subscribe(
            data => {
                this.responseEmpData = data.rsBody.resultsDb;
                this.customerData = data.rsBody.customer;
                if (data.rsBody.message == "sucess") {
                    setTimeout(function () {
                        if (data.rsBody.customer != "") {

                            this.modal.open('sucessCustomerPopup');
                        }
                        else {
                            this.modal.open('sucessPopup'); 
                        }
                    }, 1000);
                }
                else {
                    this.modal.open('customerDetailsForUpdateMonth');
                }                      
            });
    }

    ngOncloseSuccessPopUp(id)
    {
        this.modal.close(id);
        if (this.customerData != "") {
            //$('#sucessCustomerPopup').modal().hide();
            this.modal.open("sucessCustomerPopup");
            $('.modal-backdrop').remove();
            setTimeout(function () {
               // Utilities.transitionTo('create-employee', $scope);
                this.router.navigate(['create-employee'], this.responseEmpsData);
            }, 1000);
        }
        else {

            this.modal.open("sucessPopup");
            $('.modal-backdrop').remove();
            setTimeout(function () {
              //  Utilities.transitionTo('create-employee', $scope);
                this.router.navigate(['create-employee'], this.responseEmpsData);
            }, 1000);
        }
    }

    ngOnendExistRoleSubmit() {

        this.httpService.postRequest<CreateEmployeeComponent>(this.employeeDetials, '../ManageEmployee/EndExistingRoleAndCreateEmployeeDetails',true).subscribe(
            data => {
                this.responseEmpsData = data;
                this.modal.open('sucessPopup');
                //Utilities.transitionTo('view-employee', $scope);
                this.router.navigate(['view-employee'], this.responseEmpsData);
            })
    }

    ngOnnewEntry(obj, index)
    {
        this.keyvalue.keyType = this.channelCode;
        this.keyvalue.keyName = obj;
        if (obj == "SUBAREACODE") {
            this.keyvalue.relatedKeyType = "AREACODE";
            this.keyvalue.relatedKeyValue = this.employeeDetials.employeeRole[index].areaCode[0];
        }
        this.modal.open('myModal');
    }

    ngAfterViewInit()
    {
        
    }

    ngOnDestroy() {

    }
}
