﻿import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { NvD3Module } from 'ng2-nvd3';
import * as d3 from 'd3';
import * as d3hierarchy from 'd3-hierarchy';

declare var $: any;
var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'PieChart';

@Component({
    selector: 'PieChart',
    templateUrl: _templateURL
})
export class PieChartComponent implements OnInit, OnDestroy {

    private _graphData: any;
    private _graphXAxis: any;
    private _graphDesiredData: any = [];
    private _containerElement: string;

    constructor() {
        this._graphData = {};
    }
    @Input()
    set containerElement(containerElement: any) {
        //console.log(graphData);
        this._containerElement = containerElement;
    }

    @Input()
    set graphData(graphData: any) {
        //console.log(graphData);
        this._graphData = graphData;
    }

    @Input()
    set graphXAxis(graphXAxis: any) {
        //console.log(graphXAxis);
        this._graphXAxis = graphXAxis;
    }

    DeStructureData(): void {
        this._graphDesiredData = [];
       
        if (this._graphXAxis.length > 0) {
            for (let j = 0; j < this._graphXAxis.length; j++) {
                let temp = {};
                temp['label'] = this._graphXAxis[j];
                temp['value'] = ((this._graphData[this._graphXAxis[j]]['actual'] / this._graphData[this._graphXAxis[j]]['target']) * 100) || 0;

                this._graphDesiredData.push(temp);
            }
        } else {

        }

        this.drawGraph();
    }

    drawGraph(): void {
        let graphData = Object.assign([], this._graphDesiredData);
        let _containerElement = this._containerElement;
        var height = 400;
        nv.addGraph(function () {
            var chart = nv.models.pieChart()
                .x(function (d) { return d.label })
                .y(function (d) { return d.value })
                .height(height)
                .showLabels(true);

            d3.select('#' + _containerElement +' #PaiChart svg')
                .datum(graphData)
                .transition().duration(10000)
                .call(chart);

            //this.reDesignGraph("PaiChart");
            return chart;
        });
    }

    ngOnInit(): void {
        this.DeStructureData();
        //console.log(this._graphData);
    }

    ngOnDestroy(): void {
        console.log('destroying component..');
    }

}