﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { forEach } from '@angular/router/src/utils/collection';




declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureOpTemplate';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})
    
export class ConfigureOpTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    
    public isSpinner: boolean = true;
    public keyValueData: any = {};
    public metas: any=[] ;
    public meta: any =[];
    public fields: any=[] ;
    public field: any=[] ;
    public performance: any=[] ;
    public employeeComponent: any=[];
    public customerComponent: any=[];
    public performanceComponent: any=[];
    public businessVertical: any=[];
    public businessVerticalList: any=[];
    public newCustomer: any;
    public newEmployee: any;
    public column: any=[] ;
    public newColumn: any ;
    public id: any;
    public startMonth: any;
    public endMonth: any;
    public year: any;
    public removeColumnId: any;
    public addColumn: any;
    public filepath: any;
    public fileName: any;
    public relatedKeyValue: any;
    public temp: any = [];
    public metaData: any = {};
    public months: any = [];
    public columnData: any = {};
    public currentYear: any = [];
    public excelColumn: any = [];
    public columnsId: any = [];
    public finalExcel: any = [];
    public message: string ;
    public columnList: any = [];
    public monthList: any = [];
    public monthsList: any = [];
    //public BGtargetList: any = [];
    //public BUtargetList: any = [];
    public roleList: any = [];
    public salesList: any = [];
    public performanceList: any = [];
    public previousExcelColumn: any = [];
    public previousColumnsId: any = [];
    //public BGType: any = [];
    //public BUType: any = [];
    public title: any ;
    public employeeList: any ;
    public customerList: any ;
    public employeeAttributes: any = [];
    public customerAttributes: any = [];
    public salesHorizontals: any = [];
    public selectedEmpAttributes: any = [];
    public roleHeirarchyCount: any;
    public _secureFileService: any = [];
    public dropdownSettings: any = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        enableCheckAll: false
    };

    ngOnInit() {
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        $('#removeButton').css('display', 'block');
        $('#col').css('display', 'block');
        
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.keyValueData, '../ManageTemplate/getTemplateAttributes', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.metas = datas.rsBody.metas;
                    this.months = datas.rsBody.months;
                    this.monthsList = datas.rsBody.monthsList;
                    this.employeeList = datas.rsBody.employeeList;
                    this.customerList = datas.rsBody.customerList;
                    this.employeeComponent = datas.rsBody.employeeComponent;
                    this.customerComponent = datas.rsBody.customerComponent;
                    this.employeeComponent = datas.rsBody.employeeComponent;
                    this.customerComponent = datas.rsBody.customerComponent;
                    this.performanceComponent = datas.rsBody.performanceComponent;
                    this.businessVertical = datas.rsBody.businessVertical;
                    this.salesHorizontals = datas.rsBody.salesHorizontals;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;
                    this.currentYear = datas.rsBody.currentYear;
                    this.onSelectAllPerformance(datas.rsBody.performanceComponent);
                    this.onSelectAllBusinessVertical(datas.rsBody.businessVertical);
                    this.onSelectAllSales(datas.rsBody.salesHorizontals);
                    //this.onSelectAllBUTarget(datas.rsBody.BUType);
                }
            });

    }

    onItemSelectSales(item: any) {
        this.isSpinner = false;
       // this.salesList.push(item);
    }

    onSelectAllSales(items: any) {
        this.salesList = items;
        this.isSpinner = false;
    }

    onDeSelectAllSales(items: any) {

        this.isSpinner = false;
    }

    onItemDeSelectSales(item: any) {
        this.isSpinner = false;
        var i = this.salesList.indexOf(item); // RETURNS 1.
        //this.salesList.splice(i, 1);

    } 


    onItemSelectMonth(item: any, i: any) {
        this.isSpinner = false;
       this.monthList.push(item); 
    }

    onSelectAllMonth(items: any) {
        this.isSpinner = false;
    }

    onDeSelectAllMonth(items: any) {
        this.isSpinner = false;
    }

    onItemDeSelectMonth(item: any, i: any) {
        this.isSpinner = false;
        var i = this.monthList.indexOf(item); // RETURNS 1.
        this.monthList.splice(i, 1);
        
    }
    //------------------------------------

    onItemSelectBusinessVertical(item: any) {
        this.isSpinner = false;
        this.businessVerticalList.push(item);
    }

    onSelectAllBusinessVertical(items: any) {


        this.businessVerticalList = items;
      
        this.isSpinner = false;
    }

    onDeSelectAllBusinessVertical(items: any) {
        this.isSpinner = false;
    }

    onItemDeSelectBusinessVertical(item: any) {
        this.isSpinner = false;
        var i = this.businessVerticalList.indexOf(item); // RETURNS 1.
        this.businessVerticalList.splice(i, 1);
    }
    //------------------------------

    onItemSelectBUTarget(item: any) {
        this.isSpinner = false;
        //this.BUtargetList.push(item);
    }

    onSelectAllBUTarget(items: any) {
        

        //this.BUtargetList = items;
        this.isSpinner = false;
    }

    onDeSelectAllBUTarget(items: any) {
        this.isSpinner = false;
    }

    onItemDeSelectBUTarget(item: any) {
        this.isSpinner = false;
        //var i = this.BUtargetList.indexOf(item); // RETURNS 1.
        //this.BUtargetList.splice(i, 1);
    }
    
    //----------------------------------

    onItemSelectPerformance(item: any) {
        this.isSpinner = false;
        this.performanceList.push(item);
    }

    onSelectAllPerformance(items: any) {
        
       
        this.performanceList = items;
       
        this.isSpinner = false;
    }

    onDeSelectAllPerformance(items: any) {
        
        this.isSpinner = false;
    }

    onItemDeSelectPerformance(item: any) {
        this.isSpinner = false;
        var i = this.performanceList.indexOf(item); // RETURNS 1.
        this.performanceList.splice(i, 1);
       
    }
   
    OnDataTypeChange(i) {
        this.metaData.id = i;
        this.metaData.keyName = this.meta[this.metaData.id];
        $("#templateScope").css('display', 'contents');
        if (this.metaData.keyName == "RoleHierarchy") {
            
            $("#multifieldRoles" + i).css('display', 'block');
            $("#multifieldSales" + i).css('display', 'none');
            $("#multifieldPerformance" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'none');
            
        }
        else if (this.metaData.keyName == "PerformanceComponent") {

            $("#multifieldPerformance" + i).css('display', 'block');
            $("#multifieldSales" + i).css('display', 'none');
            $("#multifieldRoles" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'block');
           
        }
        else if (this.metaData.keyName == "salesHorizontal")
        {
            $("#multifieldSales" + i).css('display', 'block');
            $("#multifieldRoles" + i).css('display', 'none');
            $("#multifieldPerformance" + i).css('display', 'none');
            $("#performanceTable" + i).css('display', 'none');
        }

        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.metaData, '../ManageTemplate/getMetaDataFields', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.fields[i] = datas.rsBody.fields;
                    this.id = datas.rsBody.id;
                }
            });

    }

    AddPerformance(i) {
        this.metaData.id = i;
        this.metaData.keyName = this.meta[this.metaData.id];
        this.columnData.keyName = this.field[i];
        this.columnData.columnList = this.columnList;
        this.columnData.excelColumn = this.excelColumn;
        //this.columnData.BGtargetList = this.BGtargetList;
        //this.columnData.BUtargetList = this.BUtargetList;
        this.columnData.monthList = this.monthList;
        this.columnData.performanceList = this.performanceList;
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../ManageTemplate/addPerformance', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    var colList: any = [];
                    this.temp = datas.rsBody.columnList;
                    this.temp.forEach(function (value) {
                        colList.push(value);
                    });
                    $("#addPerformanceBtn" + i).css('display', 'none');

                }
            });
    } 
   
    removeColumn(i) {

        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../ManageTemplate/removeColumn', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.columnData.excelColumn = datas.rsBody.excelColumn;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnData.finalExcel = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;
                }
            });

    }


    OnAddcustomerComponent() {
        
        $('#custCompBtn').css('display', 'block');
        $('#custCompText').css('display', 'block');
       
    }
    OnAddemployeeComponent() {
        
        $('#empCompBtn').css('display', 'block');
        $('#empCompText').css('display', 'block');
       
    }
   

    OnColumnSelect(item) {
        
        this.addColumn = item;
       
    }
    OnStartMonthSelect(i) {

        var start = i;
        this.startMonth = i;
        
    }
    OnYearSelect(i) {

        var start = i;
        this.year = i;
        
    }
    OnEndMonthSelect(i) {

        var end = i;
        this.endMonth = i;
       
    }

    addNewColumn(newColumn) {

        this.newColumn = newColumn;
        this.columnData.addColumn = this.addColumn;
        this.columnData.newColumn = this.newColumn;
        this.columnData.excelColumn = this.columnData.finalExcel;
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../AdminDepartment/addNewColumn', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    
                    this.columnData.excelColumn = datas.rsBody.excelColumn;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnData.finalExcel = datas.rsBody.excelColumn;

                }
            });

    }
    

    viewEmpComponent() {
        $("#employeeComponentPopup").modal();
    }

    viewCustComponent() {
        $("#customerComponentPopup").modal();
    }
    
    createTemplate() {
        this.columnData.salesList = this.salesList;
        this.columnData.roleList = this.roleList;
        this.columnData.performance = this.temp;
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../ManageTemplate/createTemplate', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.message == "error") {
                        this.message = datas.rsBody.message;
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "Empty")
                    {
                        this.message = "Please Fill Values";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    else {
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.finalExcel == datas.rsBody.finalExcel;
                        this.relatedKeyValue = datas.rsBody.relatedKeyValue;
                        $("#sampleTemplate").css('display', 'block');
                        $("#downTempBtn").css('display', 'block');
                        $("#addNewColumnListBtn").addClass("ng-hide");
                     
                    }
                    
                }
            });
    }
    generateTemplate() {
        
       /*if (this.startMonth == "" || this.startMonth == null) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null) {
            this.columnData.endMonth = 12
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }*/
        //if (this.startMonth == "" || this.endMonth == "") {
            this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
       // }
        //else {
            this.columnData.startMonth = this.startMonth;
            this.columnData.endMonth = this.endMonth;
        //}
        this.columnData.year = this.year;
        this.columnData.performanceList = this.performanceList;
        //this.columnData.BGtargetList = this.BGtargetList;
        this.columnData.businessVerticalList = this.businessVerticalList;
        this.columnData.salesHorizontals = this.salesList;
        this.columnData.salesList = this.salesList;
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../ManageTemplate/generateOpTemplate', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success')
                {
                    if (datas.rsBody.message == "Month Error") {
                        this.message = "End month Should be greater than start month. Please Select Carefully";
                        this.title = "Select Month error";

                        $("#errorPopup").modal()
                    }

                    if (datas.rsBody.message == "OP Template for given period is already exist") {
                        this.title = "OP Template Already Exist";
                        this.message = "OP Template for given period is already exist";
                        this.previousExcelColumn = datas.rsBody.previousExcelColumn;
                        this.previousColumnsId = datas.rsBody.previousColumnId;
                        $("#previousTemplatePopup").modal();
                    }
                    if (datas.rsBody.message == "Please Select Start Month and End Month") {
                        this.title ="Select Month error";
                        this.message = "Please Select Start Month and End Month";
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "error") {
                        this.message = datas.rsBody.message;
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.title = "Duplicate Entry";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "Empty")
                    {
                        this.title = "Value Error";
                        this.message = "Please Fill Values";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    else {
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.columnData.columnsId = datas.rsBody.columnsId;
                        this.finalExcel == datas.rsBody.finalExcel;
                        this.columnsId = datas.rsBody.columnsId;
                        $("#sampleTemplate").css('display', 'block');
                        $("#downTempBtn").css('display', 'block');
                        $("#addNewColumnListBtn").addClass("ng-hide");
                     
                    }
                    
                }
            });
    }

    DownloadTemplate() {
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }

        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.columnData, '../ManageTemplate/downloadTemplate', true).subscribe(
            datas => {
                
                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.msg == "duplicate") {
                        this.message = datas.rsBody.message;
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();

                    }
                    else {
                        this.columnData.filepath = datas.rsBody.filepath;
                        this.filepath = datas.rsBody.filepath;
                        this.fileName = datas.rsBody.fileName;
                        $("#successPopup").modal();
                    }
                    
                }
            });
    }
    
    
    ExportDataSetToExcel() {
        
       
        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        $('#alertbottomleft').addClass('ng-hide'); 

        $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName + '&path=' + this.filepath);
        $('#ExportExcelData')[0].click();
        $('#text').html('-Select Month-');
        $("#collapse1").collapse('hide');
        $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
        this.keyValueData.keyType = "OP";
        this.keyValueData.keyName = "Template";
       

        if (this.startMonth != null || this.startMonth == "")
        {
            this.keyValueData.startMonth = this.startMonth;
        }
        else
        {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "")
        {
            this.keyValueData.endMonth = this.endMonth;
        }
        else
        {
            this.keyValueData.endMonth =12;
        }
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        $('#ExportExcelDataBtn').css('display', 'none'); 
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.keyValueData, '../ManageTemplate/insertOPTemplate', true).subscribe(
            datas => {
               
                if (datas.rsBody.result == 'success') {
                    
                    this.excelColumn = datas.rsBody.excelColumn;
                    $('#ExportExcelData').css('display', 'none'); 
                    $('#downTempBtn').css('display', 'none'); 
                    $('#afterDownload').css('display', 'block'); 
                    $('#beforeDownload').css('display', 'none'); 
                    $('#addNewColumnBtn').css('display', 'none'); 
                    $('#ExportExcelData').attr('disabled', true);
                    $('#addNewColumnBtn').attr('disabled', true);
                    //$('#downTempBtn').attr('disabled', true);
                }
            });
        }

    addemployeeComponent(newEmployee) {

        this.newEmployee = newEmployee;
        //this.columnData.removeColumnId = this.removeColumnId;
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.newEmployee, '../ManageTemplate/addemployeeComponent', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.employeeComponent = datas.rsBody.employeeComponent;
                    this.employeeList = datas.rsBody.employeeList;
                }
            });

    }
    addcustomerComponent(newCustomer) {

        this.newCustomer = newCustomer;
        //this.columnData.removeColumnId = this.removeColumnId;
        this.httpService.postRequest<ConfigureOpTemplateComponent>(this.newCustomer, '../ManageTemplate/addcustomerComponent', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.customerComponent = datas.rsBody.customerComponent;
                    this.customerList = datas.rsBody.customerList;
                }
            });

    }
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
