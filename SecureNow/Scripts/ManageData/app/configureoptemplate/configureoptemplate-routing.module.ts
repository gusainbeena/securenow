import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigureOpTemplateComponent } from './configureoptemplate.component';

const routes: Routes = [
    { path: '', component: ConfigureOpTemplateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigureOpTemplateRoutingModule { }
