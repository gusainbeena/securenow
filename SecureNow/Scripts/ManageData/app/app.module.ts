import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { HttpService } from '../service/http.service';
import { Global } from '../service/global.service';
import { AppComponent } from './app.component';
import { ModalService } from '../service/modelservice';
import { ModalComponent } from '../_directives/modal.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularDraggableModule } from 'angular2-draggable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TreeviewModule } from 'ngx-treeview';
import { D3BindingsService } from '../service/d3methodbinding.service';
import { NvD3Module } from 'ng2-nvd3';
import * as d3 from 'd3';
import * as c3 from 'c3';
import * as d3hierarchy from 'd3-hierarchy';
import { MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { CheckBoxModule, ButtonModule } from '@syncfusion/ej2-angular-buttons';
//import { tree } from 'd3-hierarchy';
import 'nvd3';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        HttpModule,
        AngularDraggableModule,
        BrowserAnimationsModule,        
        TreeviewModule.forRoot(),        
        NgbModule
    ],
    providers: [HttpService, Global, ModalService, D3BindingsService],
    bootstrap: [AppComponent]
})
export class AppModule {

}