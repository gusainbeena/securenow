import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActualUploadComponent } from './actualupload.component';

const routes: Routes = [
    { path: '', component: ActualUploadComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActualUploadRoutingModule { }
