import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ActualUploadRoutingModule } from './actualupload-routing.module';
import { ActualUploadComponent } from './actualupload.component';
import { FileSelectDirective } from 'ng2-file-upload';
@NgModule({
    declarations: [
        ActualUploadComponent, FileSelectDirective
    ],
    imports: [
        CommonModule,
        ActualUploadRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,        
        FormsModule,         
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ActualUploadComponent],
    exports: []
})
export class ActualUploadModule { }
