﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { FileUploader } from 'ng2-file-upload';
import { ModalService } from '../../service/modelservice';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs';
import * as c3 from 'c3';
import * as d3 from 'd3';
declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ActualUpload';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class ActualUploadComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private modal: ModalService,
        private route: ActivatedRoute) {
    }
    keyValueData: any = {};
    employeeActual: any = {};
    yearData: any = [];
    monthData: any = [];
    productList: any = [];
    userList: any = [];
    model: any = {};

    ngOnInit() {
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<ActualUploadComponent>(this.keyValueData, '../common/GetKeyValueData', true).subscribe(
            data => {
                this.yearData = data.rsBody.resulats;

            });
        this.ngUserType();
        //getMonth List			
        this.getMonthList();

    }

    ngActualRequestReject() {

        this.httpService.postRequest<ActualUploadComponent>(this.keyValueData, '../ManageUploadData/GetEmployeeList', true).subscribe(
            data => {
                this.userList = data.rsBody.data;
            });
    }

    ngUserType() {

            this.httpService.postRequest<ActualUploadComponent>(this.keyValueData, '../ManageUploadData/GetEmployeeList', true).subscribe(
                data => {
                    this.userList = data.rsBody.data;
                });
      
    }
    
    getMonthList() {
        this.keyValueData = {};
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<ActualUploadComponent>(this.keyValueData, '../common/GetKeyValueData', true).subscribe(
            data => {
                this.monthData = data.rsBody.resulats;
            });

        this.getProductList();
        
    }

    getProductList() {
        this.keyValueData = {};
        this.keyValueData.keyName = "BusinessVertical";
        this.httpService.postRequest<ActualUploadComponent>(this.keyValueData, '../ManageUploadData/GetProductList', true).subscribe(
            data => {
                this.productList = data.rsBody.data;
            });

    }
    ngActualRequestSubmit() {
        this.httpService.postRequest<ActualUploadComponent>(this.employeeActual, '../ManageUploadData/SubmitActualRequest', true).subscribe(
            data => {
                data = data.rsBody.data;
                this.employeeActual = {};
            });
    }
    ngAfterViewInit(): void {

     

    }
   

    ngOnDestroy(): void {

    }

}
   


