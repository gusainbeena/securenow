﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigureEmployeeTemplateComponent } from './configureemployeetemplate.component';

const routes: Routes = [
    { path: '', component: ConfigureEmployeeTemplateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigureEmployeeTemplateRoutingModule { }
