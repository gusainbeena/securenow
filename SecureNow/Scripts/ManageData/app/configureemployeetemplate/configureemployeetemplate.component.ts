﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';
import { forEach } from '@angular/router/src/utils/collection';




declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ConfigureEmployeeTemplate';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})

export class ConfigureEmployeeTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    public isSpinner: boolean = true;
    public keyValueData: any = {};
    
    public employeeComponent: any = [];
    //public customerComponent: any = [];
    //public performanceComponent: any = [];
    //public businessVertical: any = [];
    //public businessVerticalList: any = [];
    //public newCustomer: any;
    public newEmployee: any;
    public column: any = [];
    public newColumn: any;
    public id: any;
    public startMonth: any;
    public endMonth: any;
    public year: any;
    public removeColumnId: any;
    public addColumn: any;
    public filepath: any;
    public fileName: any;
    public relatedKeyValue: any;
    public temp: any = [];
    public metaData: any = {};
    public months: any = [];
    public columnData: any = {};
    public currentYear: any = [];
    public excelColumn: any = [];
    public columnsId: any = [];
    public finalExcel: any = [];
    public message: string;
    public columnList: any = [];
    public monthList: any = [];
    public monthsList: any = [];
    
    public previousExcelColumn: any = [];
    public previousColumnsId: any = [];
    private editHeader: any = [{}];
    public title: any;
    public employeeList: any;
    public employeeList2: any;
    
    public employeeAttributes: any = [];
    
    public _secureFileService: any = [];
    public dropdownSettings: any = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        enableCheckAll: false
    };

    ngOnInit() {
        $("#performanceTable").css('display', 'none');
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        $('#removeButton').css('display', 'block');
        $('#col').css('display', 'block');

        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.keyValueData, '../ManageTemplate/getEmployeeTemplateAttributes', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                  
                    this.months = datas.rsBody.months;
                    this.monthsList = datas.rsBody.monthsList;
                    this.employeeList = datas.rsBody.employeeList;
                    this.employeeList2 = datas.rsBody.employeeList;
                    
                    this.employeeComponent = datas.rsBody.employeeComponent;
                    
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;
                    this.currentYear = datas.rsBody.currentYear;
                    this.onSelectAllEmployee(datas.rsBody.employeeComponent);
                  
                    this.editHeader = datas.rsBody.excelColumn;
                }
            });

    }



    onItemSelectMonth(item: any, i: any) {
        this.isSpinner = false;
        this.monthList.push(item);
    }

    onSelectAllMonth(items: any) {
        this.isSpinner = false;
    }

    onDeSelectAllMonth(items: any) {
        this.isSpinner = false;
    }

    onItemDeSelectMonth(item: any, i: any) {
        this.isSpinner = false;
        var i = this.monthList.indexOf(item); // RETURNS 1.
        this.monthList.splice(i, 1);

    }
    //------------------------------------


    onItemSelectEmployee(item: any) {
        this.isSpinner = false;
        this.employeeList.push(item);
    }

    onSelectAllEmployee(items: any) {


        this.employeeList = items;

        this.isSpinner = false;
    }

    onDeSelectAllEmployee(items: any) {

        this.isSpinner = false;
    }

    onItemDeSelectEmployee(item: any) {
        this.isSpinner = false;
        var i = this.employeeList.indexOf(item); // RETURNS 1.
        this.employeeList.splice(i, 1);

    }

   

    removeColumn(i) {

        this.removeColumnId = i;
        this.columnData.removeColumnId = this.removeColumnId;
        this.columnData.finalExcel = this.excelColumn;
        this.columnData.columnsId = this.columnsId;
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.columnData, '../ManageTemplate/removeColumn', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.columnData.excelColumn = datas.rsBody.excelColumn;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnData.finalExcel = datas.rsBody.excelColumn;
                    this.columnsId = datas.rsBody.columnsId;
                }
            });

    }


   
    OnAddemployeeComponent() {

        $('#empCompBtn2').css('display', 'block');
        $('#empCompText').css('display', 'block');
        $('#empCompText2').css('display', 'block');

    }


    OnColumnSelect(item) {

        this.addColumn = item;

    }
    OnStartMonthSelect(i) {

        var start = i;
        this.startMonth = i;

    }
    OnYearSelect(i) {

        var start = i;
        this.year = i;

    }
    OnEndMonthSelect(i) {

        var end = i;
        this.endMonth = i;

    }

    addNewColumn(newColumn) {

        this.newColumn = newColumn;
        this.columnData.addColumn = this.addColumn;
        this.columnData.newColumn = this.newColumn;
        this.columnData.excelColumn = this.columnData.finalExcel;
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.columnData, '../AdminDepartment/addNewColumn', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {

                    this.columnData.excelColumn = datas.rsBody.excelColumn;
                    this.excelColumn = datas.rsBody.excelColumn;
                    this.columnData.finalExcel = datas.rsBody.excelColumn;

                }
            });

    }


    viewEmpComponent() {
        $("#employeeComponentPopup").modal();
    }

    viewCustComponent() {
        $("#customerComponentPopup").modal();
    }

    createTemplate() {
        
        this.columnData.performance = this.temp;
        $('#ExportExcelData').attr('disabled', false);
        $('#addNewColumnBtn').attr('disabled', false);
        $('#downTempBtn').attr('disabled', false);
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.columnData, '../ManageTemplate/createEmployeeTemplate', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.message == "error") {
                        this.message = datas.rsBody.message;
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;

                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "Empty") {
                        this.message = "Please Fill Values";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    else {
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.finalExcel == datas.rsBody.finalExcel;
                        this.relatedKeyValue = datas.rsBody.relatedKeyValue;
                        $("#sampleTemplate").css('display', 'block');
                        $("#downTempBtn").css('display', 'block');
                        $("#addNewColumnListBtn").addClass("ng-hide");

                    }

                }
            });
    }
    generateTemplate() {

        this.columnData.startMonth = 1;
        this.columnData.endMonth = 12;
        if (this.startMonth == undefined || this.endMonth == undefined) {
            this.startMonth = 0;
            this.endMonth = 0;
        }
       
        this.columnData.startMonth = this.startMonth;
        this.columnData.endMonth = this.endMonth;
       
        this.columnData.year = this.year;
        this.columnData.employeeList = this.employeeList;
       
        $('#ExportExcelDataBtn').css('display', 'block');
        $('#downTempBtn').css('display', 'block');
        $('#addNewColumnBtn').css('display', 'block');
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.columnData, '../ManageTemplate/generateEmployeeTemplate', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.message == "Month Error") {
                        this.message = "End month Should be greater than start month. Please Select Carefully";
                        this.title = "Select Month error";

                        $("#errorPopup").modal()
                    }

                  
                    if (datas.rsBody.message == "Please Select Start Month and End Month") {
                        this.title = "Select Month error";
                        this.message = "Please Select Start Month and End Month";
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "error") {
                        this.message = datas.rsBody.message;
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.title = "Duplicate Entry";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    if (datas.rsBody.message == "Empty") {
                        this.title = "Value Error";
                        this.message = "Please Fill Values";
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();
                    }
                    else {
                        this.excelColumn = datas.rsBody.finalExcel;
                        this.columnData.finalExcel = datas.rsBody.finalExcel;
                        this.columnData.columnsId = datas.rsBody.columnsId;
                        this.finalExcel == datas.rsBody.finalExcel;
                        this.columnsId = datas.rsBody.columnsId;
                        this.editHeader = datas.rsBody.excelColumn;
                        $("#sampleTemplate").css('display', 'block');
                        $("#downTempBtn").css('display', 'block');
                        $("#addNewColumnListBtn").addClass("ng-hide");

                    }

                }
            });
    }

    

    DownloadTemplate() {
        this.columnData.columnList = this.excelColumn;
        if (this.startMonth == "" || this.startMonth == null || this.startMonth == undefined || this.startMonth == 0) {
            this.columnData.startMonth = 1;
        }
        else {
            this.columnData.startMonth = this.startMonth;
        }
        if (this.endMonth == "" || this.endMonth == null || this.endMonth == undefined || this.endMonth == 0) {
            this.columnData.endMonth = 12
        }
        else {
            this.columnData.endMonth = this.endMonth;
        }

        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.columnData, '../ManageTemplate/downloadEmployeeTemplate', true).subscribe(
            datas => {

                if (datas.rsBody.result == 'success') {
                    if (datas.rsBody.msg == "duplicate") {
                        this.message = datas.rsBody.message;
                        $("#sampleTemplate").css('display', 'none');
                        $("#downTempBtn").css('display', 'none');
                        $("#errorPopup").modal();

                    }
                    else {
                        this.columnData.filepath = datas.rsBody.filepath;
                        this.filepath = datas.rsBody.filepath;
                        this.fileName = datas.rsBody.fileName;
                        $("#successPopup").modal();
                    }

                }
            });
    }


    ExportDataSetToExcel() {


        var path = this.filepath;
        var name = this.fileName;
        var values = "";
        $('#alertbottomleft').addClass('ng-hide');

        $('#ExportExcelData').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName + '&path=' + this.filepath);
        $('#ExportExcelData')[0].click();
        $('#text').html('-Select Month-');
        $("#collapse1").collapse('hide');
        $('#collapse1 ul li input[type=checkbox]').attr('checked', false);
        this.keyValueData.keyType = "EMPLOYEE_MASTER";
        this.keyValueData.keyName = "Template";


        if (this.startMonth != null || this.startMonth == "") {
            this.keyValueData.startMonth = this.startMonth;
        }
        else {
            this.keyValueData.startMonth = 1;
        }
        if (this.endMonth != null || this.endMonth == "") {
            this.keyValueData.endMonth = this.endMonth;
        }
        else {
            this.keyValueData.endMonth = 12;
        }
        this.columnData.columnList.forEach(function (value) {
            values = values + value + ",";
        });
        this.keyValueData.keyValue = values;
        this.keyValueData.relatedKeyValue = this.relatedKeyValue;
        $('#ExportExcelDataBtn').css('display', 'none');
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.keyValueData, '../ManageTemplate/insertEmployeeTemplate', true).subscribe(
            datas => {

                if (datas.rsBody.result == 'success') {

                    this.excelColumn = datas.rsBody.excelColumn;
                    $('#ExportExcelData').css('display', 'none');
                    $('#downTempBtn').css('display', 'none');
                    $('#afterDownload').css('display', 'inline-table');
                    $('#beforeDownload').css('display', 'none');
                    $('#addNewColumnBtn').css('display', 'none');
                    $('#ExportExcelData').attr('disabled', true);
                    $('#addNewColumnBtn').attr('disabled', true);
                    //$('#downTempBtn').attr('disabled', true);
                }
            });
    }

    addemployeeComponent(newEmployee) {

        this.newEmployee = newEmployee;
       
        this.httpService.postRequest<ConfigureEmployeeTemplateComponent>(this.newEmployee, '../ManageTemplate/addemployeeComponent', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.employeeComponent = datas.rsBody.employeeComponent;
                    this.employeeList2 = datas.rsBody.employeeList;
                    this.onSelectAllEmployee(datas.rsBody.employeeComponent);
                }
            });

    }
    
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
