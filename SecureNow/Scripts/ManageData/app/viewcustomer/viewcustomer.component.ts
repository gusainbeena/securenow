﻿import { Component, OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';


declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'ViewCustomer';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})



export class ViewCustomerComponent implements OnInit, OnDestroy, AfterViewInit {

    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    //public keyValueData: Object = {};
    public keyValueData: any = {};
    public shipToRequire: any = {};
    public customerDeatils: any = {};
    public custDetials: any = {};
    public customerTargets: any = [];
    public customerRelationship: any = {};
    public responseData: any = [];
    public getNextCustomerList: any;
    public getPreviousCustomerList: any = {};
    public responseStateData: any = [];
    public responseRelationData: any = {};
    public yearData: any = [];
    public yearRecorded: any =[];
    public dataTypeList: any = [];
    public BUData: any = [];
    public responseChannelData: any = [];
    public monthData: any = [];
    public regionData: any = {};
    public searchByBranch: any = {};
    public customerDetails: any = {};
    public customerBaseDetails: any = [];
    public soldToPartyCodes: any = [];
    public targetList: any = [];
    public targetData: any = [];
    public secondaryTargetList: any = {};
    public sucessResponseData: any = {};
    public getList: any = {};
    public subChannelList: any = [];
    public regionList: any = {};
    public branchListData: any = {};
    public subAreaData: any = [];
    public responseAreaData: any = [];
    public employeeKAMData: any = {};
    public employeeData: any = [];
    public getEmployeeList: any = {};
    public shipToPartyCodes: any = {};
    public lengthdata: any = {};
    public customerBaseDetailsFK: any = {};
    public deleteRel: any = {};
    public sucessData: any = {};
    public inputData: any = {};
    public errorMessage: any;
    public gradeData: Object = {};
    public doaStr: any = {};
    public success: string = '';
    public id: any;
    public customerId: any;
    public status: any;
    public date: string = '';
    
   

    //Get channel List//
    ngOnInit() {
       

        this.custDetials = {

            customerDeatils: {},
            customerRelationship: {},
            customerTargets: []

        };
        this.doaStr = {
           
            year: <string>null,
            month: <string>null,
            day: <string>null
        };

        
        $("#submit-basic-button").css('display', 'none');
        $(".h3-edit").css('display', 'block');
        $(".h3-create").css('display', 'none');
       // $(".docStrId").css('display', 'block');
        $(".viewCustTar").css('display', 'block');
        $(".addNewCustTar").css('display', 'block');
        $("#update-basic-button").css('display', 'block');
        $(".remove-disable").prop('disabled', false);
        $(".remove-disable-status").prop('disabled', false);
        this.getNextCustomerList = 0;
        this.httpService.postRequest<ViewCustomerComponent>({}, '../ManageCustomer/GetCustomerDetailsList', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {
                    this.responseData = datas.rsBody.resulats;

                    if (datas.rsBody.sucess == "No Data Found") {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display');
                    }

                    // next and previous 
                    if (this.responseData.length < 20) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getNextCustomerList > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getNextCustomerList == 0) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide")
                    }

                    this.loadState();
                }
            });

    }

    //Get State List
    loadState() {
        this.keyValueData.keyType = "STATE";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.responseStateData = data.rsBody.resulats;
                    this.getYearList();

                }
            });
    }
    
    //Get Year List
    getYearList() {
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.yearData = data.rsBody.resulats;
                    this.yearRecorded = this.yearData[0].keyValue;

                    this.getBUList();

                }
            });
    }

    //Get BU List
    getBUList() {
        this.keyValueData.keyType = "MetaData";//"OPERATIONAL";
        this.keyValueData.keyName = "businessVertical";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../ManageCustomer/GetBUBGListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.BUData = data.rsBody.resulats;
                    this.getChannel();
                    this.getDataTypeList();
                   
                }
            });
    }
    //Get Data Type List
    getDataTypeList() {
        this.keyValueData.keyType = "MetaData";// "OPERATIONAL";
        this.keyValueData.keyName = "businessVertical";//"BU";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../ManageCustomer/GetDataTypeList', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.dataTypeList = data.rsBody.dataTypeList;
                    this.getMonthList();

                }
            });
    }
    // get Channel List
    getChannel() {
        this.keyValueData.keyType = "Channel";//"CHANNEL";
        this.keyValueData.keyName = "SalesHorizontal";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.responseChannelData = data.rsBody.resulats;
                    this.getMonthList();

                }
            });
    }

    //Get Month List
    getMonthList() { 
        this.httpService.postRequest<ViewCustomerComponent>({}, '../Comman/GetMonthKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.monthData = data.rsBody.resulats;
                    this.getRegionList();

                }
            });
    }

    //Get REgion List
    getRegionList() {
        this.keyValueData.keyType = "Region";
        this.keyValueData.keyName = "salesHorizontal";
        this.httpService.postRequest<ViewCustomerComponent>(this.keyValueData, '../Comman/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.regionData = data.rsBody.resulats;
                    

                }
            });
    }

    // Search By Name And Adp ID
    searchBranch() {
        this.keyValueData.keyType = "MetaData";//"OPERATIONAL";
        this.keyValueData.keyName = "businessvertical";//"BU";
        this.httpService.postRequest<ViewCustomerComponent>(this.searchByBranch, '../ManageCustomer/GetCustomerDetailsList', true).subscribe(
            data => {
                if (data.rsBody.result == 'success') {
                    this.responseData = data.rsBody.resulats;

                    if (data.rsBody.sucess == "No Data Found") {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display');
                    }

                    // next and previous 
                    if (this.responseData.length < 20) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getNextCustomerList > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getNextCustomerList == 0) {
                        $("#example1_previous").addClass("disabled");
                    }

                    this.loadState();
                }
            });
    }


    //Next And Privious Data

    // next button 
    getNextCustomer() {
        this.getNextCustomerList = this.getNextCustomerList + 1;
        var inputData =
        {
            pageNumber: this.getNextCustomerList
        }
        this.httpService.postRequest<ViewCustomerComponent>(inputData, '../ManageCustomer/NextAndPriviousCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result == 'success') {
                    this.responseData = data.rsBody.resulats;

                    if (data.rsBody.sucess == "No Data Found") {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display');
                    }

                    // next and previous 
                    if (this.responseData.length < 20) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getNextCustomerList > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getNextCustomerList == 0) {
                        $("#example1_previous").addClass("disabled");
                    }

                    this.loadState();
                }
            });
    }

    // previous button 
    getPreviousCustomer() {
        this.getNextCustomerList = this.getNextCustomerList - 1;
        var inputData =
        {
            pageNumber: this.getNextCustomerList
        }
        this.httpService.postRequest<ViewCustomerComponent>(inputData, '../ManageCustomer/NextAndPriviousCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result == 'success') {
                    this.responseData = data.rsBody.resulats;

                    if (data.rsBody.sucess == "No Data Found") {
                        $("#row_not_found_id").css('display', 'block');
                        $("#row_table_id").css('display', 'none');
                    }
                    else {
                        $("#row_not_found_id").css('display', 'none');
                        $("#row_table_id").css('display', 'block');
                    }

                    // next and previous 
                    if (this.responseData.length < 20) {
                        $("#example1_previous").addClass("disabled");
                        $("#example1_next").addClass("disabled");
                        $("#example1_previous").addClass("ng-hide");
                        $("#example1_next").addClass("ng-hide");
                    }
                    else {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_next").removeClass("ng-hide");
                        $("#example1_next").removeClass("disabled");
                    }
                    if (this.getNextCustomerList > 0) {
                        $("#example1_previous").removeClass("ng-hide");
                        $("#example1_previous").removeClass("disabled");
                    }
                    if (this.getNextCustomerList == 0) {
                        $("#example1_previous").addClass("disabled");
                    }

                    this.loadState();
                }
            });
    }
   
    // edit Customer Details
    editCustomerDetailsSubmit(e) {
        this.id = this.responseData[e].id;
        this.customerId = this.responseData[e].id;
        this.getCustomerDetailsSubmit();
        
    }
    convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    getCustomerDetailsSubmit() {
        
        this.customerDetails.id = this.customerId;
        this.httpService.postRequest<ViewCustomerComponent>(this.customerDetails, '../ManageCustomer/EditCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    var s = data.rsBody.retVal.customerDeatils.doaStr;
                    //creating object date

                    var x = this.convert(data.rsBody.retVal.customerDeatils.doaStr);
                   
                    /*var fields = s.split(/-/);
                    var day = fields[0];
                    var month = fields[1];
                    var year = fields[2];
                    this.doaStr.year = parseInt(year);
                    this.doaStr.month = parseInt(month);
                    this.doaStr.day = parseInt(day);
                    this.customerBaseDetails.doaStr = this.doaStr;*/
                    if (data.rsBody.retVal.customerDeatils.status == "CLOSED") {
                        $(".docStrId").css('display', 'block');
                    }



                    //this.doaStr = data.rsBody.retVal.customerDeatils.doaStr.split(;
                    this.customerBaseDetails = data.rsBody.retVal.customerDeatils;
                    this.custDetials.responseRelationData = data.rsBody.retVal.customerRelationship;
                    this.targetList = data.rsBody.retVal.customerTargets;
                    this.secondaryTargetList = data.rsBody.retVal.secondaryCustomerTargets;
                    var dateString = data.rsBody.retVal.customerDeatils.doaStr;// "13/10/2014";
                    //var dataSplit = null;
                    //if (dateString != null) {
                    //     dataSplit = dateString.split('-');
                    //}
                   // else {
                        //var dataSplit = dateString.split('-');
                    //}
                    var dateConverted = {
                        day: 1,
                        month: 1,
                        year: 1001
                    }
                    var dateConverted2 = {
                        day: 1,
                        month: 1,
                        year: 1001
                    }

                    //dateConverted.day = parseInt(dataSplit[0]);
                    //dateConverted.month = parseInt(dataSplit[1]);
                    //dateConverted.year = parseInt(dataSplit[2]);
                    //this.customerBaseDetails.doaStr = dateConverted;

                    if (data.rsBody.retVal.customerDeatils.doaStr != null) {

                        var dateString = data.rsBody.retVal.customerDeatils.doaStr;
                        var dataSplit = dateString.split('-');
                        dateConverted.day = parseInt(dataSplit[0]);
                        dateConverted.month = parseInt(dataSplit[1]);
                        dateConverted.year = parseInt(dataSplit[2]);
                        this.customerBaseDetails.doaStr = dateConverted;
                    }
                    if (data.rsBody.retVal.customerDeatils.docStr != null) {
                       
                        var dateString2 = data.rsBody.retVal.customerDeatils.docStr;
                        var dataSplit2 = dateString2.split('-');
                        dateConverted2.day = parseInt(dataSplit2[0]);
                        dateConverted2.month = parseInt(dataSplit2[1]);
                        dateConverted2.year = parseInt(dataSplit2[2]);
                        this.customerBaseDetails.docStr = dateConverted2;
                    }
                    
                    if (this.customerBaseDetails.status == "CLOSED") {
                        $(".remove-disable").prop('disabled', true);
                        $(".remove-disable-status").prop('disabled', true);
                        $("#add-target-button").css('display', 'none');
                        $("#add-role-button").css('display', 'none');
                        $("#update-basic-button").css('display', 'block');
                        this.customerBaseDetails.status = "CLOSED";
                        this.custDetials.responseRelationData.status = "CLOSED";
                    }
                    else {
                        $("#view-customer-id").css('display', 'block');
                        $(".remove-disable").prop('disabled', false);
                        $(".remove-disable-status").prop('disabled', false);
                        $("#add-target-button").css('display', 'block');
                        $("#add-role-button").css('display', 'block');
                       //$("#update-basic-button").css('display', 'block');
                        this.customerBaseDetails.status = "ACTIVE";
                        $("#edit-table-id").css('display', 'block');
                        $("#view-customer-id").css('display', 'block');
                        $("#view-table-id").css('display', 'none');
                        this.custDetials.responseRelationData.status = "ACTIVE";
                        
                        
                    }

                    $("#edit-table-id").css('display', 'block');
                    $("#view-customer-id").css('display', 'block');
                    $("#view-table-id").css('display', 'none');

                }
            });
    }

    // Add new Targets
    // Add Targets For The Customer
    addtargetList() {
        $("#remove-role-button").css('display', 'block');
        this.targetList.push(
            {
                buCode: "",
                customerTargets: [
                    {
                        month: "1",
                    },
                    {
                        month: "2",
                    },
                    {
                        month: "3"
                    },
                    {
                        month: "4",
                    },
                    {
                        month: "5",
                    },
                    {
                        month: "6"
                    },
                    {
                        month: "7",
                    },
                    {
                        month: "8",
                    },
                    {
                        month: "9"
                    },
                    {
                        month: "10",
                    },
                    {
                        month: "11",
                    },
                    {
                        month: "12"
                    }


                ]
            }
        );
    }

    // Remove  Targets
    removetargetList(e) {
        this.targetList.splice(e, 1);
    }

    // Update Customer Details
    updateCustomerDetailsSubmit() {
        this.custDetials.customerTargets = this.targetList;
        delete this.custDetials.responseRelationData;
        delete this.custDetials.customerRelationship;
        this.custDetials.customerDeatils = this.customerBaseDetails;
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials, '../ManageCustomer/UpdateCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.sucessResponseData = data.rsBody.sucess;
                    $('#sucessPopup').bPopup();
                    //modal.open('updateSuccessPopup')
                    $("#update-basic-button").css('display', 'none');
                    $("#sucess-span").css('display', 'block');
                    //Utilities.transitionTo('view-customer', $scope);
                }
            });
    }

    // Add Customer Relation 
    addCustomerRelationDetails() {
        $("#add-relationship").css('display', 'block');
        $("#add-role-button").css('display', 'none');
        $("#updateCustomerBtn").css('display', 'none');
        // Create Array On Click
        this.custDetials.customerRelationship = {};
        this.custDetials.customerRelationship = [{
            yearRecorded: this.yearRecorded,
        }];
        $("#remove-role-button").css('display', 'block');
        $('html, body').animate({ scrollTop: 700 }, 'slow');
        return false;
    }

    // Remove Customer Relation Details
    removeCustomerRelationDetails() {
        $("#add-relationship").css('display', 'none');
        $("#add-role-button").css('display', 'block');
        this.custDetials.customerRelationship = {}
        this.custDetials.customerRelationship = [];
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }


    updateCustomer() {
        var day = this.customerBaseDetails.doaStr.day.toString();
        var month = this.customerBaseDetails.doaStr.month.toString();
        console.log(month.length);
        if (month.length == 1) {
            month = "0" + month;
        }
        if (day.length == 1) {
            day = "0" + day;
        }
        var year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;

        
        if (this.customerBaseDetails.docStr != null) {
            var day = this.customerBaseDetails.docStr.day.toString();
            var month = this.customerBaseDetails.docStr.month.toString();
            console.log(month.length);
            if (month.length == 1) {
                month = "0" + month;
            }
            if (day.length == 1) {
                day = "0" + day;
            }
            var year = this.customerBaseDetails.docStr.year.toString();
            this.customerBaseDetails.docStr = day + '-' + month + '-' + year;
        }

        this.custDetials.customerDeatils = this.customerBaseDetails;

        this.httpService.postRequest<ViewCustomerComponent>(this.customerBaseDetails, '../ManageCustomer/UpdateCustomerDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                        this.customerBaseDetails = data.rsBody.customerDetails;
                        var dateString = data.rsBody.customerDetails.doaStr;// "13/10/2014";
                        var dataSplit = dateString.split('-');
                        var dateConverted = {
                            day: 1,
                            month: 1,
                            year: 1001
                        }
                         var dateConverted2 = {
                            day: 1,
                            month: 1,
                            year: 1001
                        }

                        dateConverted.day = parseInt(dataSplit[0]);
                        dateConverted.month = parseInt(dataSplit[1]);
                        dateConverted.year = parseInt(dataSplit[2]);
                        this.customerBaseDetails.doaStr = dateConverted;
                        var dateString2 = data.rsBody.customerDetails.docStr;// "13/10/2014";
                        var dataSplit2 = dateString2.split('-');
                        dateConverted2.day = parseInt(dataSplit2[0]);
                        dateConverted2.month = parseInt(dataSplit2[1]);
                        dateConverted2.year = parseInt(dataSplit2[2]);
                        this.customerBaseDetails.docStr = dateConverted2;
                        $("#add-relationship").css('display', 'none');
                        $("#add-role-button").css('display', 'block');
                        $("#view-customer-id").css('display', 'block');
                       
                    

                }
            });
    }
    

    // Get Channel List for Select Channel
    onChangeGetList() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].subChannelCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].subArea = '';
        }
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.getList = data.rsBody.resulats;
                    this.custDetials.customerRelationship[0].branchDetailsFK = '';
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    if (this.getList.subChannel != null && this.getList.subChannel.length > 0) {
                        this.subChannelList = this.getList.subChannel;
                        $("#customerSubChannel").css('display', 'block');
                    }
                    else {
                        $("#customerSubChannel").css('display', 'none');
                    }
                    if (this.getList.regionList != null && this.getList.regionList.length > 0) {
                        this.regionList = this.getList.regionList;
                        $("#customerRegion").css('display', 'block');
                    }
                    else {
                        $("#customerRegion").css('display', 'none');
                    }
                    // Subarea
                    if (this.getList.subArea != null && this.getList.subArea.length > 0) {
                        this.subAreaData = this.getList.subArea;
                        $("#subarea").css('display', 'block');
                    }
                    else {
                        $("#subarea").css('display', 'none');
                    }
                    if (this.getList.branchList != null && this.getList.branchList.length > 0) {
                        this.branchListData = this.getList.branchList;
                        $("#customerBranch").css('display', 'block');
                    }
                    else {
                        $("#customerBranch").css('display', 'none');
                    }
                    if (this.getList.areaCode != null && this.getList.areaCode.length > 0) {
                        this.responseAreaData = this.getList.areaCode;
                        $("#customerArea").css('display', 'block');
                    }
                    else {
                        $("#customerArea").css('display', 'none');
                    }
                    if (this.getList.employeeBaseDetails != null && this.getList.employeeBaseDetails.length > 0) {
                        this.employeeKAMData = this.getList.employeeBaseDetails;
                        this.employeeData = this.getList.employeeBaseDetails;
                        $("#customerEmpKAMDetails").css('display', 'block');
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpKAMDetails").css('display', 'none');
                        if (data.rsBody.errorMessage != null) {
                            $("#customerEmpMessage").css('display', 'block');
                            this.errorMessage = data.rsBody.errorMessage;
                            $("#relationship-detail").addClass('ng-hide');
                        }
                        else {
                            $("#customerEmpMessage").css('display', 'none');
                            $("#relationship-detail").removeClass('ng-hide');
                        }
                    }
                   // $("#customerEmpDetails").css('display', 'none');
                    $("#shipTOPartyCode").css('display', 'none');

                }
            });
    }

    kamEmployeeList() {
        $("#customerArea").css('display', 'none');
    }
    
    // Get SubChannel List 
    onChangeGetSubChannelList() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
        }
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetCustomerListData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.getList = data.rsBody.resulats;
                    this.custDetials.customerRelationship[0].branchDetailsFK = '';
                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    if (this.getList.areaCode != null && this.getList.areaCode.length > 0) {
                        this.responseAreaData = this.getList.areaCode;
                        $("#customerArea").css('display', 'block');
                    }
                    else {
                        $("#customerArea").css('display', 'none');
                    }
                    if (this.getList.branchList != null && this.getList.branchList.length > 0) {
                        this.branchListData = this.getList.branchList;
                        $("#customerBranch").css('display', 'block');
                    }
                    else {
                        $("#customerBranch").css('display', 'none');
                    }
                    if (this.getList.employeeBaseDetails != null && this.getList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        if (data.rsBody.errorMessage != null && data.rsBody.errorMessage != "") {
                            $("#customerEmpMessage").css('display', 'block');
                            this.errorMessage = data.errorMessage;
                            $("#relationship-detail").addClass('ng-hide');
                        }
                        else {
                            $("#customerEmpMessage").css('display', 'none');
                            $("#relationship-detail").removeClass('ng-hide');
                        }
                    }
                    $("#shipTOPartyCode").css('display', 'none');

                }
            });
    }


    // Get Employee List Usin Brnch
    getEmployeeSelecedBranch() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].areaCode = '';
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetEmployeeListRelatedBranchData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    this.getEmployeeList = data.rsBody;
                    this.shipToRequire = data.rsBody.shipToRequire;
                    if (this.getEmployeeList.employeeBaseDetails != null && this.getEmployeeList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getEmployeeList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        $("#customerEmpMessage").css('display', 'block');
                        this.errorMessage = data.rsBody.errorMessage;
                        $("#relationship-detail").addClass('ng-hide');
                    }
                    if (this.shipToRequire != null) {
                        if (this.shipToRequire.keyName == "MTSE" && this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {
                            
                            this.lengthdata = this.shipToPartyCodes.length;
                            $("#shipTOPartyCode").css('display', 'block');
                        }
                        else {
                            $("#shipTOPartyCode").css('display', 'none');
                        }
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                    $("#customerEmpKAMDetails").css('display', 'none');
                }

                
            });
    }

    //Get Employee List Usin Brnch
    getEmployeeSelectedArea() {
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].branchDetailsFK = '';
            this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            this.custDetials.customerRelationship[i].employeeRoleFK = 0;
        }
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials.customerRelationship, '../ManageCustomer/GetEmployeeListRelatedBranchData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.custDetials.customerRelationship[0].employeeRoleFK = '';
                    this.getEmployeeList = data.rsBody;
                    this.shipToRequire = data.rsBody.shipToRequire;
                    if (this.getEmployeeList.employeeBaseDetails != null && this.getEmployeeList.employeeBaseDetails.length > 0) {
                        this.employeeData = this.getEmployeeList.employeeBaseDetails;
                        $("#customerEmpDetails").css('display', 'block');
                        $("#customerEmpMessage").css('display', 'none');
                        $("#relationship-detail").removeClass('ng-hide');
                    }
                    else {
                        $("#customerEmpDetails").css('display', 'none');
                        $("#customerEmpMessage").css('display', 'block');
                        this.errorMessage = data.rsBody.errorMessage;
                        $("#relationship-detail").addClass('ng-hide');
                    }
                    if (this.shipToRequire != null) {
                        if (this.shipToRequire.keyName == "MTSE" && this.shipToRequire.relatedKeyType == "SHIPTOPARTY") {

                            this.lengthdata = this.shipToPartyCodes.length;
                            $("#shipTOPartyCode").css('display', 'block');
                        }
                        else {
                            $("#shipTOPartyCode").css('display', 'none');
                        }
                    }
                    else {
                        $("#shipTOPartyCode").css('display', 'none');
                    }
                    $("#customerEmpKAMDetails").css('display', 'none');
                }
            });
    }
           
    // Add Ship To Party Codes
    addShipToCode() {
        this.shipToPartyCodes.push({});
        this.lengthdata = this.shipToPartyCodes.length;
    }

     // Remove Ship To party Code
    removeShipToCode(e) {
        this.shipToPartyCodes.splice(e, 1);
        this.lengthdata = this.shipToPartyCodes.length;
    }

    // Customer Relatonship Submit
    customerRelationshipSubmit() {

        //this.custDetials.customerRelationship = [{
        //    yearRecorded: this.yearRecorded,
        //}];
        for (var i = 0; i < this.custDetials.customerRelationship.length; i++) {
            this.custDetials.customerRelationship[i].customerGeoDetailsFK = this.custDetials.customerBaseDetailsFK;
            this.custDetials.customerRelationship[i].shipToPartyCodes = this.custDetials.shipToPartyCodes;
            if (this.custDetials.customerRelationship[i].branchDetailsFK == "") {
                this.custDetials.customerRelationship[i].branchDetailsFK = 0;
            }
            if (this.custDetials.customerRelationship[i].employeeRoleFK == "") {
                this.custDetials.customerRelationship[i].employeeRoleFK = 0;
            }
        }
        //this.customerRelationship = this.custDetials.customerRelationship[0];
        this.custDetials.customerDeatils = this.customerBaseDetails;
        //this.custDetials.customerRelationship = this.customerRelationship;
        //this.success = this.responseRelationData.status
        var day = this.customerBaseDetails.doaStr.day.toString();
        var month = this.customerBaseDetails.doaStr.month.toString();
        console.log(month.length);
        if (month.length == 1) {
            month = "0" + month;
        }
        var year = this.customerBaseDetails.doaStr.year.toString();
        this.customerBaseDetails.doaStr = day + '-' + month + '-' + year;
        this.custDetials.customerDeatils = this.customerBaseDetails;
        delete this.custDetials.responseRelationData;
        //this.success = "";
        
        this.httpService.postRequest<ViewCustomerComponent>(this.custDetials, '../ManageCustomer/CustomerRelationSubmit', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    if (data.rsBody.retVal.isValid == false) {
                        //this.custDetials.responseRelationData = data.rsBody.retData;
                        $("#add-relationship").css('display', 'none');
                        $("#add-role-button").css('display', 'block');
                        this.getCustomerDetailsSubmit();
                        $("#updateErrorPopup").modal();
                    }
                    else {
                        this.custDetials.responseRelationData = data.rsBody.retData;
                        this.custDetials.responseRelationData = data.rsBody.retData.customerRelationship;
                        var dateString = this.custDetials.customerDeatils.doaStr;// "13/10/2014";
                        var dataSplit = dateString.split('-');
                        var dateConverted = {
                            day: 1,
                            month: 1,
                            year: 1001
                        }

                        dateConverted.day = parseInt(dataSplit[0]);
                        dateConverted.month = parseInt(dataSplit[1]);
                        dateConverted.year = parseInt(dataSplit[2]);
                        this.customerBaseDetails.doaStr = dateConverted;
                        $("#add-relationship").css('display', 'none');
                        $("#add-role-button").css('display', 'block');
                        $("#view-customer-id").css('display', 'block');
                        //this.custDetials.customerRelationship = {};
                        //this.getCustomerDetailsSubmit();

                    }
                   
                }
            });
    }

    // Delete Customer Relationship
    deleteRelSubmit(e) {
        this.id = this.custDetials.responseRelationData[e].id;
        $('#deleteEmployeeRolePopup').modal();
    }
   

    deleteSubmit() {
        this.id = this.id;
        //$("#deleteEmployeeRolePopup").modal().close();
        //$("#deleteEmployeeRolePopup").close();
        this.deleteRelationship();
    }

    // Customer Relatonship Submit
   
    deleteRelationship() {
        /*pre populate data deleted*/
        
        this.deleteRel.id = this.id;

        this.httpService.postRequest<ViewCustomerComponent>(this.deleteRel, '../ManageCustomer/DeleteCustomerRelationship', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.sucessData = data.rsBody.result;
                    for (var i = 0; i < this.custDetials.responseRelationData.length; i++) {
                        if (this.custDetials.responseRelationData[i].id == this.inputData.id) {
                            this.custDetials.responseRelationData.splice(i, 1);
                            break;
                        }
                    }
                    this.getCustomerDetailsSubmit();
                }
            });
    }

    ngReginedSubmit() {
        var inputData = this.customerBaseDetails.status;
        if (inputData == "CLOSED") {
            $("#customerTargets").css('display', 'none');
            $("#view-customer-id").css('display', 'none');
            $("#add-role-button").css('display', 'none');
            $(".remove-disable").prop('disabled', true);
            $(".docStrId").css('display', 'block');
        }
        else {
            $("#customerTargets").css('display', 'block');
            $("#view-customer-id").css('display', 'block');
            $(".docStrId").css('display', 'none');
            $("#add-role-button").css('display', 'none');
            $(".remove-disable").prop('disabled', false);
        }
    }
    
    
    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
