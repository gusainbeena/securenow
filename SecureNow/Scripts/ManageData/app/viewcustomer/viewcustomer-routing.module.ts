import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewCustomerComponent } from './viewcustomer.component';

const routes: Routes = [
    { path: '', component: ViewCustomerComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ViewCustomerRoutingModule { }
