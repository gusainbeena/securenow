import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { setTimeout } from 'timers';

import { HttpService } from '../../service/http.service';
import { ModalService } from '../../service/modelservice';
import { ModalComponent } from '../../_directives/modal.service';
import { TreeviewItem, TreeviewConfig } from '../../lib';
import { D3BindingsService } from '../../service/d3methodbinding.service';
import { ViewCustomerRoutingModule } from './viewcustomer-routing.module';
import { ViewCustomerComponent } from './viewcustomer.component';
import { TreeviewModule } from 'ngx-treeview';
@NgModule({
    declarations: [
        ViewCustomerComponent
    ],
    imports: [
        CommonModule,
        ViewCustomerRoutingModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        FormsModule, 
        TreeviewModule.forRoot(),
        HttpModule
    ],
    providers: [HttpService, ModalService, D3BindingsService],
    bootstrap: [ViewCustomerComponent],
    exports: []
})
export class ViewCustomerModule { }
