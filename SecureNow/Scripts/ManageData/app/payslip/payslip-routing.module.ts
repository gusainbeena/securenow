import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaySlipComponent } from './payslip.component';

const routes: Routes = [
    { path: '', component: PaySlipComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PaySlipRoutingModule { }
