﻿import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../service/http.service';
import { BaseContainer } from '../../BaseContainer';
import { window } from 'rxjs/operators';
import { win32 } from 'path';
import { Window } from 'selenium-webdriver';

declare var $: any;

var path = require('path');

var _publicPath = path.resolve(__dirname, '../../PHIncentiveApp/Views');
var _templateURL = 'PaySlip';

@Component({
    selector: 'app-root',
    templateUrl: _templateURL
})



export class PaySlipComponent implements OnInit, OnDestroy, AfterViewInit {
    registered: any;
    attended: any;
    upcoming: any;
    rsBody: any;
    public dashboardData: Object = {};
    constructor(private httpService: HttpService,
        private router: Router,
        private route: ActivatedRoute) {
    }
    //public keyValueData: Object = {};
    public keyValueData: any = {};
    public responseData: any = [];
    public IncentiveCalculationRequest: any = {};
    public incentiveApproveDetails:any = {};
    public viewIncentiveDetails:any = [];
    public updateResponseData:any = {};
    public employeeRole: any = [];
    public responseStatus: any = {};
    public monthData: any = {};
    public employeeSheet: any = {};
    public incentiveDetails: any = {};
    public reportsType: any = {};
    public ChannelList: any = [];
    public incentivList: any = [];
    public channel: string = '';
    public subChannelList: string = '';
    public roleList: string = '';
    public actualTypeList: string = '';
    public weightageList: string = '';
    public keyType: string = '';
    

    public keyName: string = '';
    public subChannel: string = '';
    public actualType: string = '';
    public incentive: any = [];
    public selectedId: any;
    public reGePeSelectedId: any;
    public incentivId: any;
    public getNextEmployeeList: any;
    public pageNumber: any;
    public fileName: any;
    public currentQuarter: any;
    public generatePayslip: any;
    public selectID: any;
    public yearData: any = {};
    public qtrList: any = {};
    public MonthList: any = {};
    public id: any;
    public selectedAll: any;
    public path: any;

     

    ngOnInit() {
        this.getNextEmployeeList = 0;
        this.pageNumber = 0;
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<PaySlipComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(
            datas => {
                if (datas.rsBody.result == 'success') {

                    this.monthData = datas.rsBody;

                    this.getIncentiveList();

                }
            });

    }
   
    getIncentiveList() {
        this.getNextEmployeeList = 0;
        this.httpService.postRequest<PaySlipComponent>({}, '../Comman/GetIncentiveList', true).subscribe(
            data => {
                if (data.rsBody.result == "success") {
                    this.responseData = data.rsBody.resulats;

                    for (var i = 0; i < this.responseData.length; i++) {
                        for (var j = 0; j < this.monthData.length; j++) {
                            if (this.responseData[i].incentiveStartMonth == this.monthData[j].keyValue) {
                                this.responseData[i].incentiveStartMonth = this.monthData[j].description;
                            }
                            if (this.responseData[i].incentiveEndMonth == this.monthData[j].keyValue) {
                                this.responseData[i].incentiveEndMonth = this.monthData[j].description;
                            }
                        }

                    }
                }
               
                if (this.responseData.length < 20) {
                    $("#example1_previous").addClass("disabled");
                    $("#example1_next").addClass("disabled");
                    $("#example1_previous").addClass("ng-hide");
                    $("#example1_next").addClass("ng-hide");
                }
                else {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_next").removeClass("ng-hide");
                    $("#example1_next").removeClass("disabled");
                }
                if (this.getNextEmployeeList > 0) {
                    $("#example1_previous").removeClass("ng-hide");
                    $("#example1_previous").removeClass("disabled");
                }
                if (this.getNextEmployeeList == 0) {
                    $("#example1_previous").addClass("disabled");
                }
                if (data.rsBody.sucess == "No Data Found") {
                    $("#row_not_found_id").css('display', 'block');
                    $("#row_table_id").css('display', 'none');
                }
                else {
                    $("#row_not_found_id").css('display', 'none');
                    $("#row_table_id").css('display', 'block');
                }
                this.getYear();
            });
    }

    getQTR() {
        this.keyValueData.keyType = "QTR";
        this.httpService.postRequest<PaySlipComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.qtrList = data.rsBody;
                    this.getMonth();

                }
            });
    }

    getMonth() {
        this.keyValueData.keyType = "MOY";
        this.httpService.postRequest<PaySlipComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.MonthList = data.rsBody;

                }
            });
    }
  
    getYear() {
        this.keyValueData.keyType = "YEAR";
        this.httpService.postRequest<PaySlipComponent>(this.keyValueData, '../AdminDepartment/GetKeyValueData', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.yearData = data.rsBody;
                    this.getQTR();

                }
            });
    }
    
    viewIncentiveSubmit(e) {

       
        this.currentQuarter = this.responseData[e].quarter;
        this.generatePayslip = true;
        this.selectID = this.responseData[e].id;
        this.getIncentiveSubmit();
    }
    //Refresh List

    popUpData() {
        this.getIncentiveSubmit();
    }

    getIncentiveSubmit() {
        this.incentive = {};
        this.incentive.id = this.selectID;
        this.httpService.postRequest<PaySlipComponent>(this.incentive, '../AdminDepartment/GetViewIncentive', true).subscribe(
            data => {
                    if (data.rsBody.result === 'success') {
                    this.IncentiveCalculationRequest = data.rsBody.retval.incentiveRequest[0];
                    this.generatePayslip = true;
                    this.employeeRole = data.rsBody.retval.incentiveCofirmation;
                    for (var i = 0; i < this.employeeRole.length; i++) {
                        this.employeeRole[i].Selected = false;
                    }

                    if (this.IncentiveCalculationRequest.status === "CLOSED") {
                        $(".enable-disable").prop('disabled', true);
                        $("#view-emp-details-id").css('display', 'block');
                    }
                    else if (this.IncentiveCalculationRequest.status === "CANCELLED") {
                        $(".enable-disable").prop('disabled', true);
                        $("#view-emp-details-id").css('dispaly', 'none');
                    }
                    else {
                        $(".enable-disable").prop('disabled', false);
                        $("#view-emp-details-id").css('display', 'block');
                    }
                    $("#view-table-id").css('display', 'none');
                    $("#edit-details-id").css('display', 'block');

                }
            });
    }

    searchBranch() {

        this.httpService.postRequest<PaySlipComponent>(this.incentiveDetails, '../Comman/GetIncentiveList', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.qtrList = data.rsBody.resulats;
                    this.responseData = data.rsBody.resulats;
                    for (var i = 0; i < this.responseData.length; i++) {
                        for (var j = 0; j < this.monthData.length; j++) {
                            if (this.responseData[i].incentiveStartMonth == this.monthData[j].keyValue) {
                                this.responseData[i].incentiveStartMonth = this.monthData[j].description;
                            }
                            if (this.responseData[i].incentiveEndMonth == this.monthData[j].keyValue) {
                                this.responseData[i].incentiveEndMonth = this.monthData[j].description;
                            }
                        }

                        if (this.responseData.length < 20) {
                            $("#example1_previous").addClass("disabled");
                            $("#example1_next").addClass("disabled");
                            $("#example1_previous").addClass("ng-hide");
                            $("#example1_next").addClass("ng-hide");
                        }
                        else {
                            $("#example1_previous").removeClass("ng-hide");
                            $("#example1_next").removeClass("ng-hide");
                            $("#example1_next").removeClass("disabled");
                        }
                        if (this.getNextEmployeeList > 0) {
                            $("#example1_previous").removeClass("ng-hide");
                            $("#example1_previous").removeClass("disabled");
                        }
                        if (this.getNextEmployeeList == 0) {
                            $("#example1_previous").addClass("disabled");
                        }
                        if (data.rsBody == "No Data Found") {
                            $("#row_not_found_id").css('display', 'block');
                            $("#row_table_id").css('display', 'none');
                        }
                        else {
                            $("#row_not_found_id").css('display', 'none');
                            $("#row_table_id").css('display', 'block');
                        }
                        this.getYear();
                    }
                }
            });
    }
    

   /* reGenerateSheetSubmit() {

        this.httpService.postRequest<PaySlipComponent>({}, '/EmployeeDepartment/ApproveAndRejectIncentiveSheet').subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    if (this.keyType == "GRADE") {
                        $('#incentiveBasicPay').removeClass('ng-hide');
                    }
                    else {
                        $('#incentiveBasicPay').addClass('ng-hide');
                    }

                }
            });
    }*/

    getNextEmployee() {
        this.getNextEmployeeList = this.getNextEmployeeList + 1;
        this.pageNumber = this.getNextEmployeeList;
        this.incentivList['pageNumber'] = this.pageNumber
        this.httpService.postRequest<PaySlipComponent>(this.incentivList, '../AdminDepartment/NextAndPriviousIncentive', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.incentive = data.rsBody.incentive;
                    $("#example1_previous").removeClass("ng-hide");
                    if (this.incentive.length < 20) {
                        $("#example1_next").addClass("ng-hide");
                    }
                }
            });


    }

    getPreviousEmployee() {
        this.getNextEmployeeList = this.getNextEmployeeList - 1;
        this.pageNumber = this.getNextEmployeeList;
        this.incentivList['pageNumber'] = this.pageNumber

        this.httpService.postRequest<PaySlipComponent>(this.incentivList, '../AdminDepartment/NextAndPriviousIncentive', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.incentive = data.rsBody.incentive;
                    $("#example1_previous").removeClass("ng-hide");
                    if (this.pageNumber <= 0) {
                        $("#example1_previous").addClass("ng-hide");
                    }
                    //sthis.ngOnInit();
                }
            });


    }

    incentiveStatus() {
        if (this.IncentiveCalculationRequest.status == "CLOSED") {
            $("#update-basic-button").removeClass('ng-hide');
            $("#CommentBox").css('display', 'block');
        }
        else if (this.IncentiveCalculationRequest.status == "CANCELLED") {
            $("#update-basic-button").removeClass('ng-hide');
            $(".view-incentive-class").css('display', 'none');
            $("#CommentBox").css('display', 'block');
        }
        else {
            $("#update-basic-button").addClass('ng-hide');
            $("#CommentBox").css('display', 'none');
        }


    }

    //Approve request
   
    approveIncentive(e) {
        this.id = this.employeeRole[e].id;
        $("#responseDataPopup").modal();
        
    }
    
   
    approveIncentiveSubmit(e) {
        $("#responseDataPopup").modal('hide');
        this.incentiveApproveDetails = {};
        this.incentiveApproveDetails.id = this.id;
        this.incentiveApproveDetails.status = "APPROVED";
        this.httpService.postRequest<PaySlipComponent>(this.incentiveApproveDetails, '../EmployeeDepartment/ApproveAndRejectIncentiveSheet', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    $("#deleteCorporatePopup").modal('hide');
                    this.responseStatus = data.rsBody;
                    $("#sucessPopup").modal();
                }
            });


    }
   
    rejectIncentiveSubmit() {
        $("#deleteCorporatePopup").modal('hide');
        this.incentiveApproveDetails.id = this.id;
        this.incentiveApproveDetails.status = "REJECTED";
        this.httpService.postRequest<PaySlipComponent>(this.incentiveApproveDetails, '../EmployeeDepartment/ApproveAndRejectIncentiveSheet', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    $("#deleteCorporatePopup").modal('hide');
                    this.responseStatus = data.rsBody;
                    $("#sucessPopup").modal();
                }
            });


    }

    // Update Incentive Request
  
    updateIncentiveSubmit() {
        
        this.httpService.postRequest<PaySlipComponent>(this.IncentiveCalculationRequest, '../AdminDepartment/UpdateIncentive', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.updateResponseData = data.rsBody;
                    if (this.updateResponseData == "sucess") {
                        $("#sucessPopup").modal();
                        $("#update-basic-button").addClass('ng-hide');
                        $("#sucess-basic-button").removeClass('ng-hide');
                        $(".enable-disable").prop('disabled', true);
                    }
                    else {
                        $("#customerDetails").modal();
                    }
                }
            });


    }

    // Downloads Incentive Sheets
    // Download Incentive sheet for employees

    downloadPerformanceSheetSubmit(e) {
        this.employeeSheet = {};
        this.selectedId = this.employeeRole[e].id;
        this.employeeSheet.id = this.selectedId;
        this.httpService.postRequest<PaySlipComponent>(this.employeeSheet, '../EmployeeDepartment/DownloadPerformanceSheet', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.fileName = data;
                    if (this.fileName == "path") {
                        $("#NotExist").modal();
                    }
                    else {
                        $('#downloadSheet').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName);
                        $('#downloadSheet')[0].click();
                    }
                }
            });


    }


    SendPayslipMail(e) {
        this.reGePeSelectedId = this.employeeRole[e].id;
        $("#showCustRelationship").modal();


    }

    //User list for BM and Ch

    GetUserList(e) {
        this.incentiveDetails = {};
        this.incentivId = this.employeeRole[e].id;
        this.incentiveDetails.id = this.incentivId;
        this.httpService.postRequest<PaySlipComponent>(this.incentiveDetails, '../AdminDepartment/GetUserListViewIncentive', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.employeeRole = data.rsBody.retVal.incentiveCofirmation;
                    for (var i = 0; i < this.employeeRole.length; i++) {
                        this.employeeRole[i].Selected = false;
                    }
                    this.generatePayslip = false;
                    
                }
            });

    
    }


    //Send payslip To User
   
    SendMail(e) {
       
        this.httpService.postRequest<PaySlipComponent>(this.employeeRole, '../ExcelReport/PaySlipSend', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    $("#sucessPopup").modal();

                }
            });


    }
    // View For Comments 
    
    viewIncentiveDetailsSubmit(e) {
        this.incentiveDetails = {};
        this.incentivId = this.employeeRole[e].id;
        this.incentiveDetails.id = this.incentivId;
        this.httpService.postRequest<PaySlipComponent>(this.employeeRole, '../Comman/GetEmployeeIncentiveDetails', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    this.viewIncentiveDetails = data;
                    this.viewIncentiveDetails.incentiveError = JSON.parse(this.viewIncentiveDetails.incentiveError);
                    $("#viewsIncentiveDetails").bPopup();

                }
            });


    }

    // Downloads Incentive Sheet For Current Selected Quarter
   
    incentiveSheetForQuarter() {
        this.fileName = this.responseData[this.id].incentiveName + ".xlsx";
        this.httpService.postRequest<PaySlipComponent>(this.employeeRole, '../ExcelReport/CheckFileExistOrNot', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {

                    if (data.rsBody == "NotExist") {
                        $("#NotExist").bPopup();
                        this.fileName = "";
                    }
                    else {
                        $('#downloadSheet').attr('href', '../ExcelReport/ExportExcel?name=' + this.fileName);
                        $('#downloadSheet')[0].click();
                    }

                }
            });


    }

    //For Select All for RM Option



    checkAll() {
        if (this.selectedAll) {
            this.selectedAll = true;
        } else {
            this.selectedAll = false;
        }
        /*angular.forEach(this.employeeRole, function (item) {
            if (item.role == "Regional Sales Manager" && item.status == "PAYSLIP GENERATED") {
                item.Selected = this.selectedAll;
            }
            else {
                item.Selected = false;
            }
        });*/

    }


    //For Select All for BM , SE & MTSE Option
  

    checkAllBM() {
        if (this.selectedAll) {
            this.selectedAll = true;
        } else {
            this.selectedAll = false;
        }
        /*angular.forEach(this.employeeRole, function (item) {
            item.Selected = this.selectedAll;
        });*/


    }


    //get employee master excel sheet
   
    downloadPayslip(e) {
        this.reportsType = this.employeeRole[e];
        this.reportsType.quarter = this.currentQuarter;
        this.httpService.postRequest<PaySlipComponent>(this.reportsType, '../ExcelReport/DownLoadReport', true).subscribe(
            data => {
                if (data.rsBody.result === 'success') {
                    this.path = data.rsBody.filepath;
                   
                    this.httpService.postRequest<PaySlipComponent>(this.path, '../ExcelReport/CheckFileExistOrNot', true).subscribe(data => {
                        if (data.rsBody.message == "NotExist") {
                            $("#NotExist").modal();
                            this.fileName = "";
                        }

                        else {
                            $('#pdfDownload').attr('href', '../ExcelReport/downloadPDFPaySlip?name=' + this.path);
                            $('#pdfDownload')[0].click();
                        }

                        });
                    }
                
            });


    }


    incentiveInitArray = [{
        id: 'The performance numbers generated by the system are incorrect',
        text: 'The performance numbers generated by the system are incorrect'
    }, {
        id: 'There are items pending at my end that could impact the performance numbers',
        text: 'There are items pending at my end that could impact the performance numbers'
    }];

     





    ngAfterViewInit() {

    }

    ngOnDestroy() {
    }
}
