﻿import { Directive, ElementRef } from '@angular/core';

declare var $: any;

// https://angular.io/guide/attribute-directives
@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective {
  constructor(er: ElementRef) {
    $(er.nativeElement).tooltip();
  }
}