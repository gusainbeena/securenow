(function ($) {

    $.fn.typeahead = function (inputOptions, inputScope) {

        var options = inputOptions;
        var resultData = [];
        var scopeObj = inputScope;

       // console.log(scopeObj);

        /*
		 * add a div 1. around the control 2. below the control which will be
		 * hidden but will finally have lis and uls for showing the search
		 * results
		 */

        var inputElementId = $(this).attr("id");
        var inputElement = $(this);

        $(this)
				.wrap(
						"<div id='"
								+ inputElementId
								+ "_typeAheadContainer' class='typeaheadContainer'></div>");

        var scope = angular.element(
				document.getElementById('DynamicUserViewPanel')).scope();

        if (scope != undefined && scope.isMobileDevice) {
            $(this)
					.before(
							"<div id='"
									+ inputElementId
									+ "_resultSearchContainer' class='typeaheadSearchContainer ng-hide'></div>");
        } else {
            $(this)
					.after(
							"<div id='"
									+ inputElementId
									+ "_resultSearchContainer' class='typeaheadSearchContainer ng-hide'></div>");

        }

        /*
		 * start attaching functions on the typeahead plugin
		 */

        var typeAheadContainer = $("#" + inputElementId + "_typeAheadContainer");
        var resultSearchContainer = $("#" + inputElementId
				+ "_resultSearchContainer");

        resultSearchContainer.append("<ul id='" + inputElementId
				+ "_searchul' class='typeaheadSearchUl'></ul>");

        var resulSearchUl = $("#" + inputElementId + "_searchul");

        inputElement.on("focus click", function (event) {
            resultSearchContainer.addClass('ng-hide');
        });

        inputElement
				.on(
						"keyup",
						function (event) {
						    /*
							 * make a call to the server using the data URL also
							 * use the angular http
							 */

						    var value = inputElement.val();

						    var dataObj = {
						        'rqBody': {
						            'searchKey': value
						        }
						    };

						    /*
							 * make an HTTP call and start a spinner
							 */

						    $
									.ajax({
									    type: 'POST',
									    contentType: 'application/json',
									    url: options.dataUrl.url,
									    data: JSON.stringify(dataObj),
									    success: function (data) {

									        resultData = data.rsBody;

									        resulSearchUl.empty();

									        if (resultData.length > 0) {

									            $(resultData)
														.each(
																function (index,
																		val) {

																    resulSearchUl
																			.append("<a id='" + inputElementId + "_typeaheadLink' href='javascript:void(0);'><li style='list-style-type:none' name='"
																					+ inputElementId
																					+ "_searchli' class='typeaheadSearchLi' itemValue='" + val.id + "'>"
																					+ val.text
																					+ "</li></a>");
																});

									            resultSearchContainer
														.removeClass('ng-hide');

									            resultSearchContainer
														.width(inputElement
																.width());
									            resulSearchUl
														.width(inputElement
																.width());

									            var inputPos = inputElement
														.position();
									            var resulSearchUlPos = resulSearchUl
														.position();

									            resulSearchUlPos.left = inputPos.left;

									            if (scope != undefined
														&& scope.isMobileDevice) {
									                resulSearchUlPos.top = inputPos.top
															- resulSearchUl
																	.height();
									            }
									            resulSearchUl
														.position(resulSearchUlPos);

									            /*
												 * attach on click on li's
												 */
									            $(
														"#" + inputElementId
																+ "_searchul")
														.find(
																".typeaheadSearchLi")
														.on(
																"click",
																function (event) {
																    inputElement
																			.val($(
																					this)
																					.text());
																    inputElement
																			.trigger("change");

																    resultSearchContainer
																			.addClass('ng-hide');
                                                                    // Attached Id
																    var value = $(this).attr("itemValue");
																    // console.log(value);
																    
																   
																    if (options.dataUrl.rowIndex !== undefined) {
																        //console.log(scopeObj);
																        scopeObj.customerData.customerBaseDetails.id = value;
																        //scopeObj[options.dataUrl.model][options.dataUrl.rowIndex] = value;
																       
																    }
																    else {
																        /*
                                                                         * split on dot
                                                                         */
																        //console.log(scopeObj);
																        scopeObj.customerData.customerBaseDetails.id = value;
																        //[options.dataUrl.model][options.dataUrl.rowIndex] = value;


																    }

                                                                    // Attahed Id With Text
																    $("#" + inputElementId + "_mapValue").val(value);
													
																    /*if (inputElementId == 'customerName')
																    {
																        scopeObj.customerData.customerBaseDetails.id = value;

																    }*/
																    inputElement
                                                                                   .trigger("change");

																    $("#" + inputElementId + "_mapValue").trigger("change");


																    scopeObj.$emit("postTypeAhead");
																});

									        } else {
									            resultSearchContainer
														.addClass('ng-hide');
									        }

									    }
									});

						});

    }

}(jQuery));