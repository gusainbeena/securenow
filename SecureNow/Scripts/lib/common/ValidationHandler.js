var ValidationHandler = {

	validateDOM : function(target) {
		if (null == target) {
			return ValidationHandler
					.validateMandatoryFields("DynamicUserViewPanel");
		} else {
			return ValidationHandler.validateMandatoryFields(target);
		}
	},

	validateMandatoryFields : function(rootElem) {

		/*
		 * first check if need to do index based mandatory checks based on the
		 * target element that is coming in target element will be
		 */

		var foundError = false;
		var mandatoryElemList = $('#' + rootElem).find(
				'[checkMandatory="true"]');

		for (var i = 0; i < mandatoryElemList.length; i++) {
			var mandatoryElem = $(mandatoryElemList[i]);

			/*
			 * first check if the element is of the type radio group
			 */
			if (mandatoryElem.attr("elementType")
					&& mandatoryElem.attr("elementType") == "radiogroup") {

				if (mandatoryElem.find("input[type='radio']:checked").length == 0) {
					if (mandatoryElem.siblings('#' + mandatoryElem.attr("id")
							+ "_error").length > 0) {
						mandatoryElem.siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("select value");
					} else {
						mandatoryElem.parent().siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("select value");
					}

					foundError = true;
				}
			}

			else if (mandatoryElem.attr("elementType")
					&& mandatoryElem.attr("elementType") == "checkboxgroup") {

				if (mandatoryElem.find("input[type='checkbox']:checked").length == 0) {
					if (mandatoryElem.siblings('#' + mandatoryElem.attr("id")
							+ "_error").length > 0) {
						mandatoryElem.siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("select value");
					} else {
						mandatoryElem.parent().siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("select value");
					}

					foundError = true;
				}

			} else {

				/*
				 * then check if the attribute is of the type checkbox group
				 */

				if (mandatoryElem.val() == '' || mandatoryElem.val() == null
						|| mandatoryElem.val() == undefined) {
					if (mandatoryElem.siblings('#' + mandatoryElem.attr("id")
							+ "_error").length > 0) {
						mandatoryElem.siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("a value is required");
					    mandatoryElem.parent(
								'#' + mandatoryElem.attr("id"))
								.addClass("errorIntput");
					    $('#validationPopup').bPopup();
						//$(".form-error").addClass("errorIntput");
					} else {
						mandatoryElem.parent().siblings(
								'#' + mandatoryElem.attr("id") + "_error")
								.removeClass("ng-hide").text("a value is required");
						mandatoryElem.parent(
								'#' + mandatoryElem.attr("id"))
								.addClass("errorIntput").text("a value is required");
						$('#validationPopup').bPopup();
					}
					//$('#validationPopup').bPopup();
					foundError = true;
				}
			}
		}
		return foundError;
	},

	displayErrors : function(data,errorElemTarget,businessFlowName) {
		if (data.msg.validationException) {

			/*
			 * display the exceptions for each key
			 */

		
			
			$.each(data.msg.validationException, function(key, val) {

				
				if(errorElemTarget !=undefined && null != errorElemTarget && errorElemTarget != "")
				{

				    $('#' + errorElemTarget).find('#' + key + "_error").removeClass("ng-hide");
				    $('#' + errorElemTarget).find('#' + key).addClass("errorIntput");
                    $('#validationPopup').bPopup();
                    if (key == "invalidMonth" || key == "closedIncentiveMonth") {
                        $('#' + errorElemTarget).find('#' + key + "_error").text(
                            val.split("_").join(" "));
                        $("#serverVal").modal();
                    }
                    else {
                        $('#' + errorElemTarget).find('#' + key + "_error").text(
                            val.split("_").join(" ").toLowerCase());
                    }
				}
				else
					{
				
				    $('#' + key + "_error").removeClass("ng-hide");
				    $('#' + key).addClass("errorIntput");
				    $('#validationPopup').bPopup();
					$('#' + key + "_error").text(
							val.split("_").join(" ").toLowerCase());
					}
				
				
			});
		} else if (data.msg.businessException) {
			alert(data.msg.businessException);
		} else {
		    alert(data.msg.technicalException);
		}
	},

	hideErrors : function(mandatoryCheckTarget) {

		if (mandatoryCheckTarget == undefined || mandatoryCheckTarget == null
				|| mandatoryCheckTarget == '') {
            $("#DynamicUserViewPanel").find(".displayError").addClass("ng-hide");
            $("#DynamicUserViewPanel").find(".customError").addClass("ng-hide");
		    $("#DynamicUserViewPanel").find(".form-error").removeClass("errorIntput");
		} else {
			$("#" + mandatoryCheckTarget).find(".displayError").addClass(
                "ng-hide");
            $("#" + mandatoryCheckTarget).find(".customError").addClass(
                "ng-hide");
			$("#" + mandatoryCheckTarget).find(".form-error").removeClass(
					"errorIntput");
		}
	}
};