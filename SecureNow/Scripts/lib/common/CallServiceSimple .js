//make and AJAX POST call and in case of success call the success callback 
//which could be the default success/error callback or the custom success/error callback 
var CallServiceSimple = {

    //spinnerObject: null,
    alreadyInTransaction: false,
    errorElemTarget: null,
    businessFlowName: null,
    numOfActiveTransactions: 0,


    preCallSimple: function (doMandatoryChecks, mandatoryCheckTarget, businessFlowName) {
        CallServiceSimple.alreadyInTransaction = true;

        if (CallServiceSimple.numOfActiveTransactions == 0) {
            //CallServiceSimple.spinnerObject = SpinnerObject.start();
        }
        CallServiceSimple.numOfActiveTransactions++;

        ValidationHandler.hideErrors(mandatoryCheckTarget);
        var foundError = false;
        if (doMandatoryChecks) {
            foundError = ValidationHandler.validateDOM(mandatoryCheckTarget, businessFlowName);
        }
        return foundError;
    },

    postCallSimple: function () {
        CallServiceSimple.numOfActiveTransactions--;
        if (CallServiceSimple.numOfActiveTransactions <= 0) {
            //SpinnerObject.stop(CallService.spinnerObject);
            CallServiceSimple.alreadyInTransaction = false;

            if (CallServiceSimple.numOfActiveTransactions < 0) {
                CallServiceSimple.numOfActiveTransactions = 0;
            }
        }
    },


    serverCallSimple:
		function (httpObj, url, inputData, inputOutpustArgs,
			successCallBack, errorCallBack, elementId, doMandatoryChecks,
			mandatoryCheckTarget, flowName, forceInitiateTransaction) {

            if (CallServiceSimple.alreadyInTransaction && (forceInitiateTransaction == undefined || forceInitiateTransaction == false)) {
		        return;
		    }

		    if (mandatoryCheckTarget != undefined && mandatoryCheckTarget != null) {
                CallServiceSimple.errorElemTarget = mandatoryCheckTarget;
		    } else {
                CallServiceSimple.errorElemTarget = null;
		    }

		    if (flowName != undefined && flowName != null) {
                CallServiceSimple.businessFlowName = flowName;
		    } else {
                CallServiceSimple.businessFlowName = null;
		    }


		    /*
             * populate a config object
             */
		    var checkForMandatory = true;
		    if (doMandatoryChecks != undefined) {
		        checkForMandatory = doMandatoryChecks;
		    }

            if (CallServiceSimple.preCallSimple(checkForMandatory, CallServiceSimple.errorElemTarget, CallServiceSimple.businessFlowName)) {
		        /*
                 * error found
                 */
                CallServiceSimple.postCallSimple();
		        return;
		    }

            var configObject = CallServiceSimple.generateRequest(url, inputData);

		    /*
             * make a post call
             */
		    var responsePromise = httpObj(configObject);

		    var mandatoryCheckTargetInp = mandatoryCheckTarget;
		    responsePromise.success(function (data, status, headers, config) {
                CallServiceSimple.postCallSimple();
		        if (null == data.rsBody) {
		            data.rsBody = {};
		        }

		        if (data.rsBody.exceptionBlock) {
		            /*
                     * business error
                     */
                    CallServiceSimple.defaultErrorCallBack(data.rsBody.exceptionBlock, mandatoryCheckTargetInp);
		        } else {
		            /*
                     * success path
                     */
		            if (null != successCallBack) {
		                successCallBack(data.rsBody, inputOutpustArgs, inputData);
		            } else {
                        CallServiceSimple.defaultSuccessCallBack(data.rsBody);
		            }
		        }
		    });

		    responsePromise.error(function (data, status, headers, config) {
                CallServiceSimple.postCallSimple();
                CallServiceSimple.defaultErrorCallBack(data, mandatoryCheckTargetInp);
		    });
		},

    // server call for backprocess without spiner


    fdServerCall:
		function (httpObj, url, inputData,
			successCallBack, errorCallBack) {

		    var configObject = CallService.generateRequest(url, inputData);

		    /*
             * make a post call
             */
		    var responsePromise = httpObj(configObject);

		    //var mandatoryCheckTargetInp = mandatoryCheckTarget;
		    responsePromise.success(function (data, status, headers, config) {
		        CallService.postCall();
		        if (null == data.rsBody) {
		            data.rsBody = {};
		        }

		        if (data.rsBody.exceptionBlock) {
		            /*
                     * business error
                     */
		            CallService.defaultErrorCallBack(data.rsBody.exceptionBlock);
		        } else {
		            /*
                     * success path
                     */
		            if (null != successCallBack) {
		                successCallBack(data.rsBody, inputData);
		            } else {
		                CallService.defaultSuccessCallBack(data.rsBody);
		            }
		        }
		    });

		    responsePromise.error(function (data, status, headers, config) {
		        CallService.postCall();
		        CallService.defaultErrorCallBack(data);
		    });
		},

    //end without spiner call back

    generateRequest: function (inputUrl, dataObj) {
        var configObject = {
            method: "POST",
            url: inputUrl,
            data: {
                rqBody: dataObj
            }
        };

        return configObject;
    },

    defaultSuccessCallBack: function (data) {
        /*
		 * do nothing
		 */
    },

    defaultErrorCallBack: function (data, mandatoryCheckTarget) {
        /*
		 * show error message
		 */

        ValidationHandler.displayErrors(data, mandatoryCheckTarget, CallService.businessFlowName);
    }

};