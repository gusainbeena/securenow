var Utilities = {

    
 //urlStr: 'http://45.118.182.153:7901/',
   urlStr: 'http://localhost:49641/',
  //urlStr: 'http://45.118.182.153/',

    stateProvider: null,

    formatAmount: function (elementId) {

        // $('#' + elementId).trigger('change');
        var text = $('#' + elementId).val();

        if (text == undefined || text == null) {
            return;
        }

        if (text.indexOf(",") > -1) {
            text = text.split(",").join("");
        }
        var splittedVal = text.split(".");

        var arrayLen = splittedVal[0].length;
        if (arrayLen <= 3) {
            return;
        }

        var actionValue = (splittedVal[0]).split("");
        actionValue.splice(arrayLen - 3, 0, ",");
        var positionComma = arrayLen - 3;
        var countPosition = arrayLen - 3;

        while (true) {
            positionComma = positionComma - 2;

            if (positionComma > 0) {
                countPosition = countPosition - 2
                actionValue.splice(countPosition, 0, ",");
            } else {
                break;
            }
        }

        // join the decimal part with the mantissa

        if (splittedVal.length == 1) {
            $('#' + elementId).val(actionValue.join(""));
        } else {
            splittedVal[0] = actionValue.join("");
            $('#' + elementId).val(splittedVal.join("."));
            $('#' + elementId).trigger('change');
        }

    },


    defineDirectives: function () {


        //onclick for body 

        $("body").click(function () {
            $(".typeaheadSearchContainer").addClass("ng-hide");
        });


        angular
				.module('customDirectives', []).directive('ngEnter', function () {
				    return function (scope, element, attrs) {
				        element.bind("keydown keypress", function (event) {
				            if (event.which === 13) {
				                scope.$apply(function () {
				                    scope.$eval(attrs.ngEnter, { 'event': event });
				                });

				                event.preventDefault();
				            }
				        });
				    };
				})


				.directive(
						'hmScroll',
						function () {
						    return function (scope, elm, attr) {
						        var raw = elm[0];
						        var takeRows = scope.takeRows;

						        elm.scroll(function () {
						            var scrollTop = $(this).scrollTop();

						            var prevScrollPos = $(this).attr(
                                      'scrollPos');

						            if (prevScrollPos == undefined || (prevScrollPos != undefined
                                      && prevScrollPos < scrollTop)) {
						                var scrollHeight = this.scrollHeight
								          - this.offsetHeight;
						                if (scrollHeight - scrollTop < 10) {
						                    // alert('top of the div');
						                    scope.$apply(attr.hmScroll);
						                }

						                $(this).attr(
								        'scrollPos', scrollTop);
						            }

						        });
						    };
						})
				.directive('hmStateTransition', function () {
				    return {
				        restrict: 'A',

				        link: function (scope, elm, attr) {

				            var raw = elm[0];

				            elm.bind('click', function (event) {

				                var targetState = attr.hmStateTransition;
				                var scopeObj = angular.element(elm).scope();

				                Utilities.transitionTo(targetState, scopeObj);
				            }

							);
				        }
				    };
				})
				/*test*/
				.directive('hmTabOut', function () {
				    return {
				        restrict: 'A',
				        link: function (scope, elm, attr) {
				            elm.bind('keyup', function (e) {

				                //var tabindex = parseInt(attr.tabindex);	
				                var tabindex = attr.tabindex;
				                if (tabindex != 1 && tabindex != "") {
				                    var element = document.getElementById(attr.hmTabOut);
				                    if (element)
				                        element.focus();
				                }

				                else if (this.value.length === this.maxLength) {

				                    var element = document.getElementById(attr.hmTabOut);
				                    if (element)
				                        element.focus();
				                }
				            });
				        }
				    };
				})
				/*test end*/
				.directive(
						'hmCheckIsNameCharacter',
						function () {
						    return {
						        restrict: 'A',

						        link: function (scope, elm, attr) {

						            var raw = elm[0];

						            elm
											.bind(
													'keydown',
													function (event) {
													    if (((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 95 && event.keyCode <= 122))
																|| (event.keyCode == 8)
																|| (event.keyCode == 9)
																|| (event.keyCode == 32)) {
													    } else {
													        if (event.preventDefault) {
													            event
																		.preventDefault();
													        } else {
													            event.returnValue = false;
													        }
													    }
													}

											);
						        }
						    };
						})


				.directive('hmFadeOutMessage', function () {
				    return {
				        restrict: 'A',

				        link: function (scope, elm, attr) {

				            var raw = elm[0];

				            setTimeout(function () {
				                $(elm).remove();
				            }, 3000);
				        }
				    };
				})

				.directive(
						'hmCheckIsNumber',
						function () {
						    return {
						        restrict: 'A',

						        link: function (scope, elm, attr) {

						            var raw = elm[0];

						            elm
											.bind(
													'keypress',
													function (event) {

													    if ((event.charCode < 48 || event.charCode > 57)
																&& ((event.keyCode != 8) && (event.keyCode != 9))) {
													        if (event.preventDefault) {
													            event
																		.preventDefault();
													        } else {
													            event.returnValue = false;
													        }
													    }

													});
						        }
						    };
						})

				.directive(
						'hmCheckIsGolfScore',
						function () {
						    return {
						        restrict: 'A',

						        link: function (scope, elm, attr) {

						            var raw = elm[0];

						            elm
											.bind(
													'keypress',
													function (event) {

													    var charCode = (event.which) ? event.which
																: event.keyCode;
													    if ((charCode < 48 || charCode > 57)
																&& charCode != 88
																&& charCode != 120) {
													        if (event.preventDefault) {
													            event
																		.preventDefault();
													        } else {
													            event.returnValue = false;
													        }
													    }

													    var elemValue = $(elm)
																.val();
													    if (elemValue != ""
																&& ((elemValue
																		.indexOf("x") != -1) || (elemValue
																		.indexOf("X") != -1))) {

													        if (event.preventDefault) {
													            event
																		.preventDefault();
													        } else {
													            event.returnValue = false;
													        }

													    }

													    if (elemValue != ""
																&& !isNaN(elemValue)
																&& (charCode == 88 || charCode == 120)) {

													        if (event.preventDefault) {
													            event
																		.preventDefault();
													        } else {
													            event.returnValue = false;
													        }

													    }

													});
						        }
						    };
						})

				.directive('hmFormatAmount', function () {
				    return {
				        restrict: 'A',

				        link: function (scope, elm, attr) {

				            var raw = elm[0];

				            elm.bind('blur', function (event) {

				                Utilities.formatAmount(elm.attr('id'));
				            });
				        }
				    };
				})

            .directive('hmParseAmount', function () {
				    return {
				        restrict: 'A',
				        require: 'ngModel',
				        link: function (scope, element, attr, ngModel) {
				            function formatAmount(text) {

				                /*
								 * the values are already formatted from the
								 * server so logic is empty
								 */

				            }

				            function unFormatAmount(text) {

				                return text = text.split(",").join("");

				            }
				            ngModel.$parsers.push(unFormatAmount);
				            ngModel.$formatters.push(formatAmount);
				        }
				    };
				})

				.directive('hmTypeAhead', function () {
				    return {
				        restrict: 'A',

				        link: function (scope, elm, attr) {

				            var raw = elm[0];

				            var typeAheadObj = elm.typeahead({

				                "dataUrl": JSON.parse(attr.hmTypeAhead)
				            }, scope);

				        }
				    };
				});

    },

    disableDocumentElement: function (elementId) {
        var elemToBeDisabled = $('body');

        elemToBeDisabled
				.prepend("<div id='body_disableDiv' class='disableDiv'></div>");

        var disableDiv = $('#body_disableDiv');

        /*
		 * position the div on top of the element to be disabled
		 */
        disableDiv.position(elemToBeDisabled.position());

        disableDiv.height(elemToBeDisabled.height());

        disableDiv.width(elemToBeDisabled.width());

    },

    enableDocumentElement: function (elementId) {
        $('#body_disableDiv').remove();
    },

    transitionTo: function (stateName, scope) {
        scope.stateObj.transitionTo(stateName, {}, {
            reload: true,
            inherit: false,
            notify: true
        });

    }
};
