var SpinnerConfiguration = {
    targetElementId: "",

	opts : {
		lines : 14, // The number of lines to draw
		length : 9, // The length of each line
		width : 3, // The line thickness
		radius : 8, // The radius of the inner circle
		corners : 1.0, // Corner roundness (0..1)
		rotate : 0, // The rotation offset
		direction : 1, // 1: clockwise, -1: counterclockwise
		color: '#2c3e50', // #rgb or #rrggbb or array of colors
		speed : .7, // Rounds per second
		trail : 29, // Afterglow percentage
		shadow : false, // Whether to render a shadow
		hwaccel : false, // Whether to use hardware acceleration
		className : 'spinner', // The CSS class to assign to the spinner
		zIndex : 2e9, // The z-index (defaults to 2000000000)
		top : '350px', // Top position relative to parent
		left : '50%' // Left position relative to parent
	}
};

var SpinnerObject = {

    start: function (targetElementId) {
        pageLoaderOverlay = document.getElementById('hidepageoverlay');
        pageLoaderOverlay.style.display = "block";

	    var spinner = null;
	    var target = "";

	    if (SpinnerConfiguration.targetElementId != null && SpinnerConfiguration.targetElementId != "")
	    {
	        target = SpinnerConfiguration.targetElementId;
	    }
        else if (null != targetElementId && targetElementId !== undefined && targetElementId != "") {
            target = targetElementId;

        } else {

            target = "maincontent";
	}
		    var target = document.getElementById(target);
		    spinner = new Spinner(SpinnerConfiguration.opts).spin(target);

        //a div to hide the main 

		return spinner
	},

    stop: function (spinnerObject) {

		spinnerObject.stop();
		spinnerObject = null;
		SpinnerConfiguration.targetElementId = "";

		pageLoaderOverlay = document.getElementById('hidepageoverlay');
		pageLoaderOverlay.style.display = "none";
	}

};
