//make and AJAX POST call and in case of success call the success callback 
//which could be the default success/error callback or the custom success/error callback 
var CallService = {

    spinnerObject: null,
    alreadyInTransaction: false,
    errorElemTarget: null,
    businessFlowName: null,
    numOfActiveTransactions: 0,


    preCall: function (doMandatoryChecks, mandatoryCheckTarget, businessFlowName) {
        CallService.alreadyInTransaction = true;

        if (CallService.numOfActiveTransactions == 0) {
            CallService.spinnerObject = SpinnerObject.start();
        }
        CallService.numOfActiveTransactions++;

        ValidationHandler.hideErrors(mandatoryCheckTarget);
        var foundError = false;
        if (doMandatoryChecks) {
            foundError = ValidationHandler.validateDOM(mandatoryCheckTarget, businessFlowName);
        }
        return foundError;
    },

    postCall: function () {
        CallService.numOfActiveTransactions--;
        if (CallService.numOfActiveTransactions <= 0) {
            SpinnerObject.stop(CallService.spinnerObject);
            CallService.alreadyInTransaction = false;

            if (CallService.numOfActiveTransactions < 0) {
                CallService.numOfActiveTransactions = 0;
            }
        }
    },


    serverCall:
		function (httpObj, url, inputData, inputOutpustArgs,
			successCallBack, errorCallBack, elementId, doMandatoryChecks,
			mandatoryCheckTarget, flowName, forceInitiateTransaction) {

		    if (CallService.alreadyInTransaction && (forceInitiateTransaction == undefined || forceInitiateTransaction == false)) {
		        return;
		    }

		    if (mandatoryCheckTarget != undefined && mandatoryCheckTarget != null) {
		        CallService.errorElemTarget = mandatoryCheckTarget;
		    } else {
		        CallService.errorElemTarget = null;
		    }

		    if (flowName != undefined && flowName != null) {
		        CallService.businessFlowName = flowName;
		    } else {
		        CallService.businessFlowName = null;
		    }


		    /*
             * populate a config object
             */
		    var checkForMandatory = true;
		    if (doMandatoryChecks != undefined) {
		        checkForMandatory = doMandatoryChecks;
		    }

		    if (CallService.preCall(checkForMandatory, CallService.errorElemTarget, CallService.businessFlowName)) {
		        /*
                 * error found
                 */
		        CallService.postCall();
		        return;
		    }

		    var configObject = CallService.generateRequest(url, inputData);

		    /*
             * make a post call
             */
		    var responsePromise = httpObj(configObject);

		    var mandatoryCheckTargetInp = mandatoryCheckTarget;
		    responsePromise.success(function (data, status, headers, config) {
		        CallService.postCall();
		        if (null == data.rsBody) {
		            data.rsBody = {};
		        }

		        if (data.rsBody.exceptionBlock) {
		            /*
                     * business error
                     */
		            CallService.defaultErrorCallBack(data.rsBody.exceptionBlock, mandatoryCheckTargetInp);
		        } else {
		            /*
                     * success path
                     */
		            if (null != successCallBack) {
		                successCallBack(data.rsBody, inputOutpustArgs, inputData);
		            } else {
		                CallService.defaultSuccessCallBack(data.rsBody);
		            }
		        }
		    });

		    responsePromise.error(function (data, status, headers, config) {
		        CallService.postCall();
		        CallService.defaultErrorCallBack(data, mandatoryCheckTargetInp);
		    });
		},

    // server call for backprocess without spiner


    fdServerCall:
		function (httpObj, url, inputData,
			successCallBack, errorCallBack) {

		    var configObject = CallService.generateRequest(url, inputData);

		    /*
             * make a post call
             */
		    var responsePromise = httpObj(configObject);

		    //var mandatoryCheckTargetInp = mandatoryCheckTarget;
		    responsePromise.success(function (data, status, headers, config) {
		        CallService.postCall();
		        if (null == data.rsBody) {
		            data.rsBody = {};
		        }

		        if (data.rsBody.exceptionBlock) {
		            /*
                     * business error
                     */
		            CallService.defaultErrorCallBack(data.rsBody.exceptionBlock);
		        } else {
		            /*
                     * success path
                     */
		            if (null != successCallBack) {
		                successCallBack(data.rsBody, inputData);
		            } else {
		                CallService.defaultSuccessCallBack(data.rsBody);
		            }
		        }
		    });

		    responsePromise.error(function (data, status, headers, config) {
		        CallService.postCall();
		        CallService.defaultErrorCallBack(data);
		    });
		},

    //end without spiner call back

    generateRequest: function (inputUrl, dataObj) {
        var configObject = {
            method: "POST",
            url: inputUrl,
            data: {
                rqBody: dataObj
            }
        };

        return configObject;
    },

    defaultSuccessCallBack: function (data) {
        /*
		 * do nothing
		 */
    },

    defaultErrorCallBack: function (data, mandatoryCheckTarget) {
        /*
		 * show error message
		 */

        ValidationHandler.displayErrors(data, mandatoryCheckTarget, CallService.businessFlowName);
    }

};