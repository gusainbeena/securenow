﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecureNow.Controllers;

namespace SecureNow.Validators
{
    public class CustomerTargetsModificationValidate
    {
        static DBModels db = new DBModels();
       
        public static void IsValid(YearlyCustomerTargetsDTO inputObj,Dictionary<string,string> validationExceptionMap)
        {
         
            Dictionary<string, YearlyTargetsDTO> retData = new Dictionary<string, YearlyTargetsDTO>();
            YearlyCustomerTargets tar = null;
            string yearCFY = (from e in db.keyValueData where e.keyType == "FIN" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            int CFYYear = Convert.ToInt32(yearCFY);
            string currentQtr = (from e in db.keyValueData where e.keyType == "CURRENTQUARTER" && e.keyName == "CFY" select e.keyValue).FirstOrDefault();
            string recentIncQuarter = (from i in db.incentiveCalculationRequest where i.year == CFYYear && i.status.Equals(Codes.InStatus.CLOSED.ToString()) orderby i.id descending select i.quarter).FirstOrDefault();
            int disabledMonth = CommonController.getMonthList(recentIncQuarter).endMonth;
            int currentQtrEnd = CommonController.getMonthList(currentQtr).endMonth;
            foreach (YearlyTargetsDTO item in inputObj.customerTargets)
            {
                tar = (from t in db.yearlyCustomerTargets where t.dataType.Equals(item.dataType) && t.month == item.month && t.id == item.id && t.customerBaseDetailsFK == item.customerBaseDetailsFK select t).FirstOrDefault();
                if (tar == null && inputObj.customerTargets.IndexOf(item) >= currentQtrEnd && item.targetAmt>0)
                {
                    validationExceptionMap.Add("invalidMonth", "We can't update targets for invalid month due to un-availability");
                    break;
                }
                else if (tar != null)
                {
                    if (item.targetAmt != tar.targetAmt && item.month <= disabledMonth)
                    {
                        validationExceptionMap.Add("closedIncentiveMonth", "The request cannot be processed.The targets have been modified for months for which incentive has already been disbursed.");
                        break;
                    }
                    
                }
                

            }


        }

    }
}