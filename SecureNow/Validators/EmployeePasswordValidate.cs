﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SecureNow.Validators
{
    public class EmployeePasswordValidate
    {
        static DBModels db = new DBModels();
        private static bool isError;

        public static void IsValid(EmployeeDetailsDTO inputObj, Dictionary<string, string> validationExceptionMap)
        {

            //check if its a create or an update
            if (inputObj.id != 0)
            {
                string encPassword = null;
                var pass = inputObj.password;
                string EncryptionKey = "incentive_application";
                byte[] clearBytes = Encoding.Unicode.GetBytes(pass);
                using (TripleDES encryptor = TripleDES.Create())
                {

                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(24);
                    encryptor.IV = pdb.GetBytes(8);
                    using (System.IO.MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encPassword = Convert.ToBase64String(ms.ToArray());

                     }
                   }

                    bool userValid = db.employeeBaseDetails.Any(user => user.id == inputObj.id && user.password == encPassword && user.status == "ACTIVE");

                    if (userValid)
                    {
                        if(inputObj.password == inputObj.newPassword)
                        {

                           validationExceptionMap.Add("oldPassword", ExceptionMessage.getExceptionMessage("EmployeeDetailsDTO.Password.Unique"));
                           validationExceptionMap.Add("newPassword", ExceptionMessage.getExceptionMessage("EmployeeDetailsDTO.Password.Unique"));
                       }

                    }
                    else
                    {
                        if (!validationExceptionMap.ContainsKey("oldPassword"))
                        {
                            validationExceptionMap.Add("oldPassword", ExceptionMessage.getExceptionMessage("EmployeeDetailsDTO.oldPassword.Unique"));
                        }
                    }

            }
        }

        // Region Validation End 

    }
}