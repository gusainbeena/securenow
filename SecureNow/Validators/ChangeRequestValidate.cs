﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class ChangeRequestValidate
    {
        static DBModels db = new DBModels();

        public static void IsValid(CustomerBaseDetails inputObj,Dictionary<string,string> validationExceptionMap)
        {
            //check if its a create or an update
            if (inputObj.id == 0)
            {
                string currentDate = DateTime.Now.ToString("dd-mm-yyyy");
                string[] monthSplit = currentDate.Split('-');
                int day = Convert.ToInt32(monthSplit[0]);
                int month = Convert.ToInt32(monthSplit[1]);
                int year = Convert.ToInt32(monthSplit[2]);

                if (day > 26 && day < 30)
                {
                    validationExceptionMap.Add("endMonth", ExceptionMessage.getExceptionMessage("EmployeeRoleAndAreaDTO.endMonth.Unique"));
                }

            }
        }

    }
}