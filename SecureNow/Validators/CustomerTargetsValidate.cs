﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class CustomerTargetsValidate
    {
        static DBModels db = new DBModels();
       
        public static void IsValid(List<YearlyCustomerTargetsDTO> inputObj,Dictionary<string,string> validationExceptionMap)
        {
         
            Dictionary<string, YearlyCustomerTargetsDTO> retData = new Dictionary<string, YearlyCustomerTargetsDTO>();
            foreach (YearlyCustomerTargetsDTO item in inputObj)
            {
                if(item.dataType == null)
                {
                    validationExceptionMap.Add("dataType", ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                }
                else
                {
                    if(retData.Keys.Contains(item.dataType))
                    {
                        validationExceptionMap.Add("dataType", ExceptionMessage.getExceptionMessage("YearlyCustomerTargetsDTO.dataType.Unique"));
                    }
                    else
                    {
                        retData.Add(item.dataType, item);
                    }

                    
                }

            }


        }

    }
}