﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class CustomerRelationShipUpdateValidate
    {
        static DBModels db = new DBModels();
        private static bool isError;

        public static void IsValid(List<CustomerBasicRelationDTO> inputObj, Dictionary<string, string> validationExceptionMap)
        {

            //check if its a create or an update
            foreach (CustomerBasicRelationDTO item in inputObj)
            {
                if (item.toBeMergeOrVacent == true)
                {
                    decimal total_amount = 0M;
                    foreach (CustomerBasicRelationDTO i in item.otherEmployeesList)
                    {
                        total_amount += i.shareToBeMerge;
                    }
                    if (total_amount != 100)
                    {
                        validationExceptionMap.Add("splitValue", ExceptionMessage.getExceptionMessage("SplitTargetValueDTO.splitValue.Unique"));
                        break;
                    }
                }
            }

        }

        // Region Validation End 

    }
}