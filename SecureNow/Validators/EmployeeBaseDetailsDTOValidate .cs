﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class EmployeeBaseDetailsDTOValidate
    {
        static DBModels db = new DBModels();
        private static bool isError;

        public static void IsValid(EmployeeBaseDetailsDTO inputObj,Dictionary<string,string> validationExceptionMap)
        {

            EmployeeBaseDetails resultsDb = null;
            EmployeeBaseDetails adpIdDb = null;

            //check if its a create or an update
            if (inputObj.id == 0)
            {
                if (null != inputObj.email)
                {
                    //create case
                    resultsDb = (from e in db.employeeBaseDetails
                                 where (e.email.ToLower().Trim() == inputObj.email.ToLower().Trim())
                                 select e
                           ).FirstOrDefault();

                    adpIdDb = (from e in db.employeeBaseDetails
                               where (e.adpId.ToLower().Trim() == inputObj.adpId.ToLower().Trim())
                               select e
                          ).FirstOrDefault();


                    if (null != resultsDb)
                    {
                        if (!validationExceptionMap.ContainsKey("email"))
                        {
                            validationExceptionMap.Add("email", ExceptionMessage.getExceptionMessage("EmployeeBaseDetails.email.Unique"));
                            checkValidEmployeeStatus(inputObj);
                        }
                    }

                    if (null != adpIdDb)
                    {
                        if (!validationExceptionMap.ContainsKey("adpId"))
                        {
                            validationExceptionMap.Add("adpId", ExceptionMessage.getExceptionMessage("EmployeeBaseDetails.adpId.Unique"));
                            checkValidEmployeeStatus(inputObj);
                        }
                    }
                    string phone = inputObj.phoneNumber;
                    //string phoneNumber = phone.Length.ToString();
                    if (phone.Length != 10 && phone.Length != 0)
                    {
                        validationExceptionMap.Add("phoneNumber", ExceptionMessage.getExceptionMessage("PhoneAttribute"));
                    }
                }

            }
            else
            {
                if (inputObj.status == "RESIGNED")
                {
                    if (inputObj.dolStr == null)
                    {
                        if (!validationExceptionMap.ContainsKey("dolStr"))
                        {
                            validationExceptionMap.Add("dolStr", ExceptionMessage.getExceptionMessage("EmployeeBaseDetails.dolStr.Unique"));

                        }
                    }
                }

            }
        }

        //Employee Status
        private static Boolean checkValidEmployeeStatus(EmployeeBaseDetailsDTO inputObj)
        {
            Codes.employeeStatus validEmpStatus;

            if (!Enum.TryParse(inputObj.status, out validEmpStatus))
            {
                return false;

            }

            return true;
        }

    }
}