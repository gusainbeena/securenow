﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class CustomerDetailsValidate
    {
        static DBModels db = new DBModels();

        public static void IsValid(CustomerBaseDetailsDTO inputObj,Dictionary<string,string> validationExceptionMap)
        {
            CustomerBaseDetails resultsDb = null;
            //check if its a create or an update
            if (inputObj.id == 0)
            {
                if (null != inputObj.customerName)
                {
                    resultsDb = (from e in db.customerBaseDetails
                                          where (e.soldToCode.ToLower().Trim() == inputObj.soldToCode.ToLower().Trim()
                                          )
                                          select e
                           ).FirstOrDefault();

                    if (null != resultsDb)
                    {
                        if (!validationExceptionMap.ContainsKey("soldToCode"))
                        {
                            validationExceptionMap.Add("soldToCode", ExceptionMessage.getExceptionMessage("CustomerBaseDetails.soldToCode.Unique"));
                        }
                    }
                   
                }

            }
            else
            {
                if (inputObj.status == "CLOSED")
                {
                    if (inputObj.docStr == null)
                    {
                        if (!validationExceptionMap.ContainsKey("docStr"))
                        {
                            validationExceptionMap.Add("docStr", ExceptionMessage.getExceptionMessage("EmployeeBaseDetails.dolStr.Unique"));

                        }
                    }
                }
            }
        }

        // Channel Validation
        private static Boolean checkValidChannelValue(CustomerDetailsDTO inputObj)
        {
            string tempValue = inputObj.RP1.ToString();
            if (null == (tempValue = checkValueForChannel(tempValue)))
            {
                return false;
            }
            return true;
        }

        public static string checkValueForChannel(string ChannelName)
        {
            return (from a in db.keyValueData
                    where
                    a.keyType == "CHANNEL"
                    &&
                    a.keyValue.Equals(ChannelName, StringComparison.CurrentCultureIgnoreCase)
                    select a.keyName
                    ).FirstOrDefault();
        }
    }
}