﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;

namespace SecureNow.Validators
{
    public class ValidateMetaData
    {
        static DBModels db = DBModels.dynamicDB;
        private static bool isError;

        public static void IsValid(MetaDataSetupDTO inputObj, Dictionary<string, string> validationExceptionMap)
        {
            if(inputObj != null)
            {
                Dictionary<string, string> temp = new Dictionary<string, string>();
                temp = CheckForDuplicate(inputObj);
                if (temp.Count > 0)
                {
                    isError = false;
                    foreach (KeyValuePair<string, string> item in temp)
                    {
                        validationExceptionMap.Add(item.Key, item.Value);
                    }
                }

            }
        }

        public static Dictionary<string, string> checkForExistingRelationship(MetaDataSetupDTO inputObj)
        {
            Dictionary<string, string> validationExceptionMap = new Dictionary<string, string>();

            KeyValueData key = (from k in db.keyValueData
                                where k.keyType.Equals(inputObj.key)
                                && ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.endMonth)
                                || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth))
                                select k).FirstOrDefault();

            if (key != null)
            {
                validationExceptionMap.Add("value", "An entry with the similar parameters can not be created, " +
                    "Please end previous relation after that you can create the new one");
            }


            return validationExceptionMap;


        }

        public static Dictionary<string,string> CheckForDuplicate(MetaDataSetupDTO inputObj)
        {
            Dictionary<string, string> validationExceptionMap = new Dictionary<string, string>();

            switch (inputObj.key)
            {
                case "SalesHorizontal":
                    
                    if (inputObj.subkey != inputObj.key)
                    {
                        KeyValueData key = (from k in db.keyValueData where k.keyType.Equals(inputObj.subkey) && k.keyValue.Equals(inputObj.value) && k.keyName.Equals(inputObj.key) 
                                            && k.year == inputObj.year && ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.startMonth)
                                            || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth)
                                            || (k.startMonth >= inputObj.startMonth && k.startMonth <= inputObj.endMonth))
                                            select k).FirstOrDefault();
                        if(key != null)
                        {
                            validationExceptionMap.Add("value", "An entry with the similar parameters can not be created");
                        }

                    }
                    else
                    {
                        KeyValueData key = (from k in db.keyValueData
                                            where k.keyType.Equals("MetaData") && k.keyValue.Equals(inputObj.value) && k.keyName.Equals(inputObj.key)
                                              && k.year == inputObj.year && 
                                              ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.startMonth)
                                              || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth)
                                              || (k.startMonth >= inputObj.startMonth && k.startMonth <= inputObj.endMonth))
                                            select k).FirstOrDefault();

                        if (key != null)
                        {
                            validationExceptionMap.Add("value", "An entry with the similar parameters can not be created");
                        }
                    }
                    break;
                case "RoleHierarchy":
                    KeyValueData key1 = (from k in db.keyValueData
                                            where k.keyType.Equals("MetaData") && k.keyValue.Equals(inputObj.value) && k.keyName.Equals(inputObj.key)
                                              && k.year == inputObj.year && ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.startMonth)
                                              || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth)
                                              || (k.startMonth >= inputObj.startMonth && k.startMonth <= inputObj.endMonth))
                                            select k).FirstOrDefault();

                        if (key1 != null)
                        {
                            validationExceptionMap.Add("value", "An entry with the similar parameters can not be created");
                        }
                    break;
                case "SalesComponent":
                        key1 = (from k in db.keyValueData
                                         where k.keyType.Equals("MetaData") && k.keyValue.Equals(inputObj.value) && k.keyName.Equals(inputObj.key)
                                           && k.year == inputObj.year && ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.startMonth)
                                           || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth)
                                           || (k.startMonth >= inputObj.startMonth && k.startMonth <= inputObj.endMonth))
                                         select k).FirstOrDefault();

                    if (key1 != null)
                    {
                        validationExceptionMap.Add("value", "An entry with the similar parameters can not be created");
                    }
                    break;
                default:
                    key1 = (from k in db.keyValueData
                            where k.keyType.Equals("SalesTeam") && k.keyValue.Equals(inputObj.value) && k.keyName.Equals(inputObj.subkey + " Level")
                              && k.year == inputObj.year && ((k.startMonth <= inputObj.startMonth && k.endMonth >= inputObj.startMonth)
                              || (k.startMonth >= inputObj.startMonth && k.endMonth <= inputObj.endMonth)
                              || (k.startMonth >= inputObj.startMonth && k.startMonth <= inputObj.endMonth))
                            select k).FirstOrDefault();

                    if (key1 != null)
                    {
                        validationExceptionMap.Add("value", "An entry with the similar parameters can not be created");
                    }
                    break;

            }
            return validationExceptionMap;
        }

        public static Dictionary<string, string> getUpperLevelAttributes(List<EmployeeAttributesDTO> inputObj)
        {
            Dictionary<string, string> validationExceptionMap = new Dictionary<string, string>();

            foreach(EmployeeAttributesDTO data in inputObj)
            {
                if (data.key.Equals("CHANNEL"))
                {
                    KeyValueData chExist = (from k in db.keyValueData where k.keyType.Equals("CHANNELLIST") && k.keyName.Equals("CHANNEL") && k.keyValue.Equals(data.value) select k).FirstOrDefault();
                    if (chExist != null)
                    {
                        validationExceptionMap.Add("channel" + inputObj.IndexOf(data), "A Channel with selected pattern already exist");
                    }
                }
                else
                {
                    List<KeyValueData> required = (from k in db.keyValueData
                                                   where k.id <= ((from key in db.keyValueData where key.keyType.Equals("CHANNELWISEREQUIRED") && key.keyName.Equals(data.channel) && key.keyValue.Equals(data.key) select key.id).FirstOrDefault())
                     && k.keyType.Equals("CHANNELWISEREQUIRED") && k.keyName.Equals(data.channel)
                                                   orderby k.preferenceSerial descending
                                                   select k).ToList();

                   
                    string reqParam = data.key;
                    string reqVal = data.value;
                    int smonth = data.startMonth;
                    int emonth = data.endMonth;
                    Dictionary<string, string> existing = new Dictionary<string, string>();
                    foreach (KeyValueData s in required)
                    {
                        KeyValueData upperOne = new KeyValueData();
                        EmployeeAttributeLink temp = new EmployeeAttributeLink();
                        if (s.preferenceSerial > 0)
                        {
                            upperOne = (from k in db.keyValueData where k.keyType.Equals("CHANNELWISEREQUIRED") && k.preferenceSerial == s.preferenceSerial - 1 && k.keyName.Equals(s.keyName) select k).FirstOrDefault();
                            string upperOneValue = "";
                            switch (upperOne.keyValue)
                            {
                                case "CHANNELREQUIRED":
                                    upperOneValue = data.channel;
                                    break;
                                case "SUBCHANNELREQUIRED":
                                    upperOneValue = data.subChannel;
                                    break;
                                case "REGIONREQUIRED":
                                    upperOneValue = data.region;
                                    break;
                                case "AREACODEREQUIRED":
                                    upperOneValue = data.area;
                                    break;
                                case "SUBAREACODEREQUIRED":
                                    upperOneValue = data.subArea;
                                    break;
                                default:
                                    break;
                            }

                            temp = (from e in db.employeeAttributeLink
                                    where e.keyName.Equals(reqParam) && ((e.relatedKeyName2.Equals(upperOne.keyValue) && e.relatedKeyValue2.Equals(upperOneValue)) || (e.relatedKeyName1.Equals(upperOne.keyValue) && e.relatedKeyValue1.Equals(upperOneValue)))
                                    && e.keyValue.Equals(reqVal) && ((e.startMonth <= smonth && e.endMonth >= emonth) || (e.startMonth >= smonth && e.endMonth <= emonth))
                                    select e).FirstOrDefault();
                        }
                        else
                        {
                            temp = null;
                            upperOne = s;
                            existing.Add(s.keyValue, s.keyName);
                        }



                        if (temp != null)
                        {
                            existing.Add(temp.keyName, temp.keyValue);
                            if (temp.relatedKeyName2 != null)
                            {
                                reqParam = temp.relatedKeyName2;
                                reqVal = temp.relatedKeyValue2;
                            }
                            else if (temp.relatedKeyName1 != null)
                            {
                                reqParam = temp.relatedKeyName2;
                                reqVal = temp.relatedKeyValue2;
                            }
                            else
                            {
                                reqParam = temp.relatedKeyName2;
                                reqVal = temp.relatedKeyValue2;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (existing.Keys.Count > 0)
                    {
                        int count = 0;
                        foreach (string k in existing.Keys.ToList())
                        {
                            switch (k)
                            {
                                case "CHANNELREQUIRED":
                                    if (k.Equals(data.key))
                                    {
                                        if (existing[k].Equals(data.value))
                                        {
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        if (existing[k].Equals(data.channel))
                                        {
                                            count++;
                                        }
                                    }
                                    break;
                                case "SUBCHANNELREQUIRED":
                                    if (k.Equals(data.key))
                                    {
                                        if (existing[k].Equals(data.value))
                                        {
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        if (existing[k].Equals(data.subChannel))
                                        {
                                            count++;
                                        }
                                    }
                                    break;
                                case "REGIONREQUIRED":
                                    if (k.Equals(data.key))
                                    {
                                        if (existing[k].Equals(data.value))
                                        {
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        if (existing[k].Equals(data.region))
                                        {
                                            count++;
                                        }
                                    }
                                    break;
                                case "AREACODEREQUIRED":
                                    if (k.Equals(data.key))
                                    {
                                        if (existing[k].Equals(data.value))
                                        {
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        if (existing[k].Equals(data.area))
                                        {
                                            count++;
                                        }
                                    }
                                    break;
                                case "SUBAREACODEREQUIRED":
                                    if (k.Equals(data.key))
                                    {
                                        if (existing[k].Equals(data.value))
                                        {
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        if (existing[k].Equals(data.subArea))
                                        {
                                            count++;
                                        }
                                    }
                                    break;
                                default:

                                    break;
                            }
                        }

                        if (count == required.Count)
                        {
                            if (data.key.Equals("CHANNELREQUIRED"))
                            {
                                validationExceptionMap.Add("channel" + inputObj.IndexOf(data), "A Channel with selected pattern already exist");
                            }
                            if (data.key.Equals("SUBCHANNELREQUIRED"))
                            {
                                validationExceptionMap.Add("subchannel" + inputObj.IndexOf(data), "A Subchannel with selected pattern already exist");
                            }
                            if (data.key.Equals("REGIONREQUIRED"))
                            {
                                validationExceptionMap.Add("region" + inputObj.IndexOf(data), "A Region with selected pattern already exist");
                            }
                            if (data.key.Equals("AREACODEREQUIRED"))
                            {
                                validationExceptionMap.Add("area" + inputObj.IndexOf(data), "An Area with selected pattern already exist");
                            }
                            if (data.key.Equals("SUBAREACODEREQUIRED"))
                            {
                                validationExceptionMap.Add("subArea" + inputObj.IndexOf(data), "A Subarea with selected pattern already exist");
                            }
                        }

                    }
                }
            }

            return validationExceptionMap;
        }

        public static Dictionary<string, string> ReAssignValueMapped(MetaDataConfigureDTO inputObj)
        {
            Dictionary<string, string> validationExceptionMap = new Dictionary<string, string>();
            return validationExceptionMap;
        }

    }
}
