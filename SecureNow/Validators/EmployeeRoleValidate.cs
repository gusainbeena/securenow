﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class EmployeeRoleValidate
    {
        static DBModels db = new DBModels();
        private static bool isError;

        public static void IsValid(EmployeeRoleAndAreaDTO inputObj,Dictionary<string,string> validationExceptionMap)
        {

            //check if its a create or an update
            if (inputObj.id == 0)
            {
                List<EmployeeRoleAndAreaDTO> resultsDb = new List<EmployeeRoleAndAreaDTO>();
                if (inputObj.endMonth < inputObj.startMonth)
                {
                    validationExceptionMap.Add("endMonth", ExceptionMessage.getExceptionMessage("EmployeeRoleAndAreaDTO.endMonth.Unique"));
                }
                List<string> keyValueData = new List<string>();
                 keyValueData = (from k in db.keyValueData
                                    where k.keyType.Equals(inputObj.channelCode)
                                    && k.keyName.Equals(inputObj.role)
                                    && k.relatedKeyType.Equals("REQUIRED")
                                    select k.keyValue).ToList();
                if(keyValueData == null)
                {
                     keyValueData = (from k in db.keyValueData
                                        where k.keyType.Equals(inputObj.subChannelCode)
                                        && k.keyName.Equals(inputObj.role)
                                        && k.relatedKeyType.Equals("REQUIRED")
                                     select k.keyValue).ToList();
                }

                resultsDb = (from e in db.employeeRole
                             where e.role.Equals(inputObj.role)
                             && e.startMonth <= inputObj.startMonth
                             && e.endMonth >= inputObj.startMonth
                             && e.yearRecorded.Equals(inputObj.yearRecorded)
                             select new EmployeeRoleAndAreaDTO
                             {
                                 role = e.role,
                                 adpId = e.adpId,
                                 startMonth = e.startMonth,
                                 endMonth = e.endMonth
                             }).ToList();
                if (keyValueData != null)
                {
                    for (int i = 0; i < keyValueData.Count(); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                if (inputObj.RP1 == null || inputObj.RP1 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP1 == inputObj.RP1).ToList();
                                }
                                break;
                            case 1:
                                if (inputObj.RP2 == null || inputObj.RP2 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP2 == inputObj.RP2).ToList();
                                }
                                break;
                            case 2:
                                if (inputObj.RP3 == null || inputObj.RP3 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP3 == inputObj.RP3).ToList();
                                }
                                break;
                            case 3:
                                if (inputObj.RP4 == null || inputObj.RP4 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP4 == inputObj.RP4).ToList();
                                }
                                break;
                            case 4:
                                if (inputObj.RP5 == null || inputObj.RP5 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP5 == inputObj.RP5).ToList();
                                }
                                break;
                            case 5:
                                if (inputObj.RP6 == null || inputObj.RP6 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP6 == inputObj.RP6).ToList();
                                }
                                break;
                            case 6:
                                if (inputObj.RP7 == null || inputObj.RP7 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP7 == inputObj.RP7).ToList();
                                }
                                break;
                            case 7:
                                if (inputObj.RP8 == null || inputObj.RP8 == "")
                                {
                                    validationExceptionMap.Add(keyValueData[i], ExceptionMessage.getExceptionMessage("RequiredAttribute"));
                                }
                                else
                                {
                                    resultsDb = resultsDb.Where(x => x.RP8 == inputObj.RP8).ToList();
                                }
                                break;
                        }
                    }
                }

               if (resultsDb.Count > 0)
               {
                    if (!resultsDb[0].adpId.ToLower().Contains("vacant"))
                    {
                        if (resultsDb[0].startMonth == inputObj.startMonth)
                        {
                            if (!validationExceptionMap.ContainsKey("startMonth"))
                            {
                                validationExceptionMap.Add("startMonth", ExceptionMessage.getExceptionMessage("EmployeeRoleAndAreaDTO.startMonth.UniqueOverLapRole"));
                            }
                        }
                        else if (resultsDb[0].startMonth > inputObj.startMonth)
                        {
                            if (!validationExceptionMap.ContainsKey("startMonth"))
                            {
                                validationExceptionMap.Add("startMonth", ExceptionMessage.getExceptionMessage("EmployeeRoleAndAreaDTO.startMonth.UniqueOverLapRole"));
                            }
                        }                        
                    }
                }
            }
        }

        // Channel Validation
        private static Boolean checkValidChannelValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.channelCode.ToString();
            if (null == (tempValue = checkValueForChannel(tempValue)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForChannel(string ChannelName)
        {

            return (from a in db.keyValueData
                    where
                    a.keyType == "CHANNEL"
                    &&
                    a.keyValue.Equals(ChannelName, StringComparison.CurrentCultureIgnoreCase)
                    select a.keyName
                    ).FirstOrDefault();
        }
        // Channel Validation End
        // Sub Channel 
        private static Boolean checkValidSubChannelValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.channelCode.ToString();
            if (null == (tempValue = checkValueForSubChannel(tempValue, inputObj)))
            {
                return false;

            }


            return true;
        }

        public static string checkValueForSubChannel(string channelName, EmployeeRoleAndAreaDTO inputObj)
        {

                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(channelName, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "SUBCHANNEL"
                        &&
                        a.keyValue.Equals(inputObj.channelCode, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
        }
      
        // Month Validation
        public static string getMonthValue(string monthName)
        {
                return (from a in db.keyValueData
                        where
                        a.keyType == "MOY"
                        &&
                        a.keyValue.Equals(monthName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).First();
        }


        //Role Validation  
        private static Boolean checkValidRoleValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.role.ToString();
            if (null == (tempValue = checkValueForRole(tempValue, inputObj)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForRole(string roleName, EmployeeRoleAndAreaDTO inputObj)
        {
            if (null != inputObj.subChannelCode && inputObj.subChannelCode.Length > 0)
            {
                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(inputObj.subChannelCode, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "ROLE"
                        &&
                        a.keyValue.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
            }
            else
            {
                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(inputObj.channelCode, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "ROLE"
                        &&
                        a.keyValue.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
            }
        }
        // Role Validation End

        //Region Validation
        private static Boolean checkValidRegionValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.region.ToString();
            if (null == (tempValue = checkValueForRegion(tempValue)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForRegion(string regionName)
        {

                return (from a in db.keyValueData
                        where
                        a.keyType == "REGION"
                        &&
                        a.keyValue.Equals(regionName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyName
                        ).FirstOrDefault();
        }

        // Region Validation End 

    }
}