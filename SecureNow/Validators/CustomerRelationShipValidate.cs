﻿using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class CustomerRelationShipValidate
    {
        static DBModels db = new DBModels();
        private static bool isError;

        public static void IsValid(CustomerBasicRelationDTO inputObj,Dictionary<string,string> validationExceptionMap)
        {

            //check if its a create or an update
            if (inputObj.id == 0)
            {
                CustomerRelationshipDetails resultsDb = null;
                CustomerBaseDetails customerBaseDetails = null;
                if (inputObj.endMonth < inputObj.startMonth)
                {
                    validationExceptionMap.Add("endMonth", ExceptionMessage.getExceptionMessage("EmployeeRoleAndAreaDTO.endMonth.Unique"));
                }
                List<CustomerBasicRelationDTO> resultsDb1 = (from e in db.customerRelationshipDetails
                                                             join ce in db.employeeRole on e.employeeRoleFK equals ce.id
                           where e.customerBaseDetailsFK.Equals(inputObj.customerBaseDetailsFK)
                           && e.endMonth >= inputObj.endMonth
                           && e.yearRecorded.Equals(inputObj.yearRecorded)
                           select new CustomerBasicRelationDTO
                           {
                               employeeRoleFK = ce.id,

                           }).ToList();

                if (resultsDb1.Count > 0 )
                {
                    foreach(CustomerBasicRelationDTO item in resultsDb1)
                    {
                        EmployeeRole empRole = (from er in db.employeeRole where er.id == item.employeeRoleFK && er.adpId.Contains("VACANT") select er).FirstOrDefault();
                        if (!validationExceptionMap.ContainsKey("endMonth") && empRole == null)
                        {
                            validationExceptionMap.Add("endMonth", ExceptionMessage.getExceptionMessage("CustomerBasicRelationDTO.endMonth.UniqueOverLapRole"));
                        }
                    }
                    

                }
            }
        }

        // Channel Validation
        private static Boolean checkValidChannelValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.channelCode.ToString();
            if (null == (tempValue = checkValueForChannel(tempValue)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForChannel(string ChannelName)
        {

            return (from a in db.keyValueData
                    where
                    a.keyType == "CHANNEL"
                    &&
                    a.keyValue.Equals(ChannelName, StringComparison.CurrentCultureIgnoreCase)
                    select a.keyName
                    ).FirstOrDefault();
        }
        // Channel Validation End
        // Sub Channel 
        private static Boolean checkValidSubChannelValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.channelCode.ToString();
            if (null == (tempValue = checkValueForSubChannel(tempValue, inputObj)))
            {
                return false;

            }


            return true;
        }

        public static string checkValueForSubChannel(string channelName, EmployeeRoleAndAreaDTO inputObj)
        {

                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(channelName, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "SUBCHANNEL"
                        &&
                        a.keyValue.Equals(inputObj.channelCode, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
        }
        
        // Month Validation
        public static string getMonthValue(string monthName)
        {
                return (from a in db.keyValueData
                        where
                        a.keyType == "MOY"
                        &&
                        a.keyValue.Equals(monthName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).First();
        }


        //Role Validation  
        private static Boolean checkValidRoleValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.role.ToString();
            if (null == (tempValue = checkValueForRole(tempValue, inputObj)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForRole(string roleName, EmployeeRoleAndAreaDTO inputObj)
        {
            if (null != inputObj.subChannelCode && inputObj.subChannelCode.Length > 0)
            {
                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(inputObj.subChannelCode, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "ROLE"
                        &&
                        a.keyValue.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
            }
            else
            {
                return (from a in db.keyValueData
                        where
                        a.keyType.Equals(inputObj.channelCode, StringComparison.CurrentCultureIgnoreCase)
                        &&
                        a.keyName == "ROLE"
                        &&
                        a.keyValue.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyValue
                        ).FirstOrDefault();
            }
        }
        // Role Validation End

        //Region Validation
        private static Boolean checkValidRegionValue(EmployeeRoleAndAreaDTO inputObj)
        {
            string tempValue = inputObj.region.ToString();
            if (null == (tempValue = checkValueForRegion(tempValue)))
            {
                return false;

            }

            return true;
        }

        public static string checkValueForRegion(string regionName)
        {

                return (from a in db.keyValueData
                        where
                        a.keyType == "REGION"
                        &&
                        a.keyValue.Equals(regionName, StringComparison.CurrentCultureIgnoreCase)
                        select a.keyName
                        ).FirstOrDefault();
        }

        // Region Validation End 

    }
}