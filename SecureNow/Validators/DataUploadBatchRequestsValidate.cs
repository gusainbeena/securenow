﻿using SecureNow.Models;
using SecureNow.utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureNow.Validators
{
    public class DataUploadBatchRequestsValidate
    {
        static DBModels db = new DBModels();

        public static void IsValid(DataUploadBatchRequests inputObj,Dictionary<string,string> validationExceptionMap)
        {

            DataUploadBatchRequests resultsDb = null;

            //check if its a create or an update
            if (inputObj.id == 0)
            {
                if (null != inputObj.requestType)
                {
                    //create case
                   
                    resultsDb = (from e in db.dataUploadBatchRequests
                                 where (e.requestType.ToLower().Trim() == inputObj.requestType.ToLower().Trim())
                                 select e
                           ).FirstOrDefault();

                    if (null != resultsDb)
                    {
                        if (!validationExceptionMap.ContainsKey("name"))
                        {
                            validationExceptionMap.Add("name", ExceptionMessage.getExceptionMessage("DataUploadBatchRequests.name.Unique"));
                        }
                    }
                }
            }
        }
    }
}