﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecureNow.Container;
using SecureNow.Models;
using SecureNow.utilities;

namespace SecureNow.Validators
{
    public class MetaDataSetupValidator
    {
        static DBModels db = DBModels.dynamicDB;
        private static bool isError;

        public static void IsValid(MetaDataConfigureDTO inputObj, Dictionary<string, string> validationExceptionMap)
        {
            if(inputObj != null)
            {
                Dictionary<string, string> temp = new Dictionary<string, string>();
                temp = CheckForDuplicate(inputObj);
                if (temp.Count > 0)
                {
                    isError = false;
                    foreach (KeyValuePair<string, string> item in temp)
                    {
                        validationExceptionMap.Add(item.Key, item.Value);
                    }
                }

            }
        }

        public static Dictionary<string, string> CheckForDuplicate(MetaDataConfigureDTO inputObj)
        {
            Dictionary<string, string> validationExceptionMap = new Dictionary<string, string>();
            return validationExceptionMap;
        }

    }
}
