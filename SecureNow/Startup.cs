﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SecureNow.Startup))]
namespace SecureNow
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
