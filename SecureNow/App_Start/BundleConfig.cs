﻿using System.Web;
using System.Web.Optimization;

namespace SecureNow
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery-js").Include(
                        "~/Scripts/jquery-{version}.js"));

           bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-js").Include(
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-js").Include(
                     "~/Scripts/jquery.toast.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-js").Include(
                       "~/Scripts/lib/angular/jquery-1.9.1.js",
                       "~/Scripts/lib/angular/jquery-ui.js",
                       "~/Scripts/lib/angular/angular.min.js",
                       "~/Scripts/lib/angular/angular-ui-router.min.js",
                       "~/Scripts/lib/angular/checklist-model.js"));

            bundles.Add(new ScriptBundle("~/bundles/theme-js").Include(
                       "~/Scripts/theme/bootstrap.min.js",
                       "~/Scripts/theme/sidebar-nav.min.js",
                       "~/Scripts/theme/jquery.slimscroll.js",
                       "~/Scripts/theme/waves.js",
                       "~/Scripts/theme/all.min.js",
                       "~/Scripts/theme/custom.min.js",
                       "~/Scripts/theme/jQuery.style.switcher.js"));

            bundles.Add(new ScriptBundle("~/bundles/chart-js").Include(
                "~/Scripts/chart-js/highcharts.js",
                "~/Scripts/chart-js/highcharts-more.js",
                "~/Scripts/chart-js/map.js",
                "~/Scripts/chart-js/exporting.js",
                "~/Scripts/chart-js/motion.js",
                "~/Scripts/chart-js/disputed.js",
                "~/Scripts/chart-js/highcharts-3d.js",
                "~/Scripts/chart-js/solid-gauge.js",
                "~/Scripts/chart-js/drilldown.js",
                "~/Scripts/chart-js/jquery.toast.js",
                 "~/Scripts/chart-js/d3js.js",
                 "~/Scripts/chart-js/Chart.min.js",
                 "~/Scripts/chart-js/multiple-select.js"
                ));

            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/common-js").Include(
                        "~/Scripts/lib/plugins/spinner/spinner.js",
                        "~/Scripts/lib/plugins/fileupload/js/jquery.fileupload.js",
                        "~/Scripts/lib/plugins/typeahead/typeahead.js",
                        "~/Scripts/lib/common/CallService.js",
                        "~/Scripts/lib/common/SpinnerConfiguration.js",
                        "~/Scripts/lib/common/Utilities.js",
                        "~/Scripts/lib/common/ValidationHandler.js",
                        "~/Scripts/lib/common/Boot.js",
                        "~/Scripts/lib/common/popup.js",
                        "~/Scripts/lib/common/multiple-select.js",
                        "~/Scripts/lib/common/multiple-select.js"));

            bundles.Add(new ScriptBundle("~/bundles/javascript-js").Include(
                      "~/Scripts/assets/tether.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/assets/jquery.mousewheel.min.js",
                      "~/Scripts/assets/jquery.jscrollpane.min.js",
                      "~/Scripts/assets/spin.js",
                      "~/Scripts/assets/ladda.min.js",
                      "~/Scripts/assets/select2.full.min.js",
                      "~/Scripts/assets/jquery.validation.min.js",
                      "~/Scripts/assets/jquery.mask.min.js",
                      "~/Scripts/assets/autosize.min.js",
                      "~/Scripts/assets/bootstrap-show-password.min.js",
                      "~/Scripts/assets/moment.min.js",
                      "~/Scripts/assets/fullcalendar.min.js",
                      "~/Scripts/assets/sweetalert.min.js",
                      "~/Scripts/assets/bootstrap-notify.min.js",
                      "~/Scripts/assets/summernote.min.js",
                      "~/Scripts/assets/owl.carousel.min.js",
                      "~/Scripts/assets/jquery.nestable.js",
                      "~/Scripts/assets/jquery.dataTables.min.js",
                      "~/Scripts/assets/dataTables.bootstrap4.min.js",
                      "~/Scripts/assets/dataTables.fixedColumns.min.js",
                      "~/Scripts/assets/dataTables.responsive.min.js",
                      "~/Scripts/assets/mindmup-editabletable.js",
                      "~/Scripts/assets/jquery.peity.min.js",
                      "~/Scripts/assets/chartist-plugin-tooltip.min.js",
                      "~/Scripts/assets/TweenMax.min.js",
                      "~/Scripts/assets/common.js",
                      "~/Scripts/assets/demo.temp.js"));

              bundles.Add(new StyleBundle("~/Content/style-css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/multiple-select.css",
                      "~/Content/css/jquery.jscrollpane.css",
                      "~/Content/css/ladda-themeless.min.css",
                      "~/Content/css/select2.min.css",
                      "~/Content/css/fullcalendar.min.css",
                      "~/Content/css/sweetalert.css",
                      "~/Content/css/summernote.css",
                      "~/Content/css/owl.carousel.min.css",
                      "~/Content/css/dataTables.bootstrap4.css",
                      "~/Content/site.css",
                      "~/Content/css/jquery.toast.css"));

             bundles.Add(new StyleBundle("~/Content/theme-css").Include(
                    "~/Content/theme/bootstrap.min.css",
                    "~/Content/theme/sidebar-nav.min.css",
                    "~/Content/theme/animate.css",
                    "~/Content/theme/style.css",
                    "~/Content/theme/all.min.css",
                    "~/Content/multiple-select.css",
                    "~/Content/theme/blue-dark.css"));
 

            bundles.Add(new StyleBundle("~/Comman/style-comman").Include(
                     "~/Content/helpers/fonts.css",
                     "~/Content/helpers/typography.css",
                     "~/Content/helpers/base.default.css",
                     "~/Content/helpers/base.responsive.css",
                     "~/Content/helpers/rtl.version.css",
                     "~/Content/themes/theme-blue.css",
                     "~/Content/themes/theme-dark.css",
                     "~/Content/forms/basic-form-elements.css",
                     "~/Content/forms/buttons.css",
                     "~/Content/forms/dropdowns.css",
                     "~/Content/forms/checkboxes-radio.css",
                     "~/Content/forms/form-validation.css",
                      "~/Content/forms/autocomplete.css",
                     "~/Content/components/utilities.css",
                     "~/Content/components/tables.css",
                     "~/Content/components/steps.css",
                     "~/Content/components/tooltips-popovers.css",
                     "~/Content/components/modal.css",
                     "~/Content/components/progress-bars.css",
                     "~/Content/components/badges-labels.css",
                     "~/Content/components/pagination.css",
                     "~/Content/components/collapse.css",
                     "~/Content/components/notifications-alerts.css",
                     "~/Content/components/tabs.css",
                     "~/Content/components/carousel.css",
                     "~/Content/components/widgets.css",
                     "~/Content/vendors/jscrollpane.css",
                     "~/Content/vendors/select2.css",
                     "~/Content/css/multiple-select.css",
                      "~/Content/css/eonasdan-bootstrap-datetimepicker.css",
                     "~/Content/vendors/fullcalendar.css",
                     "~/Content/vendors/summernote.css",
                     "~/Content/vendors/ionrangeslider.css",
                     "~/Content/vendors/nestable.css",
                     "~/Content/vendors/datatables-fixedcolumns.css",
                     "~/Content/vendors/datatables-responsive.css",
                     "~/Content/vendors/editable-table.css",
                     "~/Content/vendors/chartist.css",
                     "~/Content/vendors/chartist-tooltip-plugin.css",
                     "~/Content/apps/profile.css",
                     "~/Content/apps/messaging.css",
                     "~/Content/apps/mail.css",
                     "~/Content/apps/calendar.css",
                     "~/Content/apps/gallery.css",
                     "~/Content/forms/selectboxes.css"));
        }
    }
}
