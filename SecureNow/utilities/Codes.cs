﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureNow.utilities
{
    public class Codes
    {
        public enum keyValue
        {
            MetaData, SalesHorizontal, BusinessVertical, Channel, TeamSetup,RoleHierarchy
        }
        public enum status
        {
            QUEUED, PROCESSING, CANCELLED,
            PARTIALLY_QUEUED_FOR_PAYOUT_CARD, PARTIALLY_APPROVED, PARTIALLY_REJECTED, SUCCESS,
            FAILED, TO_BE_PROCESSED, WAITING_FOR_APPROVAL, QUEUED_FOR_PAYOUT_CARD, QUEUED_FOR_APPROVAL,
            APPROVED, SEND_PAYSLIP, REJECTED
        }


        public enum accountingYear
        {
            AY, FY
        }


        public enum uploadType
        {
            SalesDataPort, EmployeeMasterDataPort, OperationalPlanDataPort, OverDueDataPort, SalesSecondaryDataPort,
            NonDMSDataPort, SecondaryOperationalPlanDataPort,MassUploadForSoldToCode,DisbursementSheet
        }

        public enum employeeStatus
        {
            ACTIVE, RESIGNED
        }

        public enum AccessControl
        {
            UPLOAD
        }

        public enum customerStatus
        {
            ACTIVE, CLOSED
        }

        public enum downloadType
        {
            DownloadReports, PDFPath , TabularData
        }

        public enum CrType
        {
            TARGET_MODIFICATION, SPLIT_TARGETS, CUSTOMER_DETAILS_MODIFICATION, INITIATE_INCENTIVE_OVERRIDE
        }

        public enum CrStatus
        {
            WAITING_FOR_APPROVAL, APPROVED, REJECTED, QUEUED_FOR_PROCESSING, TO_BE_PROCESSED
        }

        public enum InStatus
        {
            OPEN,QUEUED_FOR_APPROVAL, APPROVED, CANCELLED, CLOSED, FAILED, REJECTED, TO_BE_PROCESSED, QUEUED_FOR_DATA, QUEUED_FOR_PAYOUT_CARD, DISBURSE
        }

        public enum message
        {
            Success, Failed
        }

    }
}
