﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using SecureNow.Models;
using SecureNow.Container;

namespace SecureNow.utilities
{
    class DBOperations
    {

        public static string getMonthDescription(int monthValue)
        {
            using (DBModels db = new DBModels())
            {
                return (from a in db.keyValueData
                        where a.keyType == "MOY"
                        &&
                        a.keyValue == Convert.ToString(monthValue)
                        select a.description).First();
            }

        }
        public static List<string> listAssociatedBUForBG(string bgName)
        {
            using (DBModels db = new DBModels())
            {
                return (from c in db.keyValueData
                        where (
                        c.keyType == "OPERATIONAL"
                        &&
                        c.keyName == "BU"
                        &&
                        c.relatedKeyType == "BG"
                        &&
                        c.relatedKeyValue == bgName)
                        select c.keyValue).ToList();
            }
        }
       


        public static List<YearlyCustomerTargets> listExistingCustomerTargetsForTheDuration(
                                                            int yearRecorded,
                                                            int id,
                                                            int startMonth,
                                                            int endMonth,
                                                            string buName)
        {

            using (DBModels db = new DBModels())
            {
                return (from c in db.yearlyCustomerTargets
                        where (
                        c.customerBaseDetailsFK == id
                        &&
                        c.month >= startMonth
                        &&
                        c.month <= endMonth
                        &&
                        c.buCode == buName
                        &&
                        c.yearRecorded ==  yearRecorded)
                        select c).ToList();
            }
        }

        public static void deleteExistingCustomerTargetsForTheDuration(int yearRecorded,
                                                            int id,
                                                            int startMonth,
                                                            int endMonth,
                                                            string buName)
        {
            using (DBModels db = new DBModels())
            {
                List<YearlyCustomerTargets> result = (from c in db.yearlyCustomerTargets
                                                             where (
                                                             c.customerBaseDetailsFK == id
                                                             &&
                                                             c.month >= startMonth
                                                             &&
                                                             c.month <= endMonth
                                                             &&
                                                             c.buCode == buName
                                                             &&
                                                             c.yearRecorded == yearRecorded)
                                                             select c).ToList();

                db.yearlyCustomerTargets.RemoveRange(result);
                db.SaveChanges();
            }

        }

        public static void createCustomerTargetsForExistingDuration(List<YearlyCustomerTargets> newTargets)
        {
            using (DBModels db = new DBModels())
            {

                db.yearlyCustomerTargets.AddRange(newTargets);
                db.SaveChanges();
            }
        }

        public static void deleteCustomerTargetsForTheYear(int yearRecorded, int customerBaseDetailsFK)
        {

            using (DBModels db = new DBModels())
            {
                IQueryable<YearlyCustomerTargets> results = (from a in db.yearlyCustomerTargets
                                                             where
                                                             (a.yearRecorded == yearRecorded
                                                             &&
                                                             a.customerBaseDetailsFK == customerBaseDetailsFK)
                                                             select a);
                db.yearlyCustomerTargets.RemoveRange(results);
                db.SaveChanges();

            }

        }
        public static void createCustomerTargetsForTheYear(int yearRecorded , int customerBaseDetailsFK, List<CustomerTargetsDTO> targetList)
        {
            //for each BU create a row in the Database
            using (DBModels db = new DBModels())
            {
                foreach(CustomerTargetsDTO item in targetList)
                {
                    foreach(YearlyCustomerTargets targetItem in item.customerTargets)
                    {
                        targetItem.buCode = item.buCode;
                        targetItem.yearRecorded = yearRecorded;
                        targetItem.customerBaseDetailsFK = customerBaseDetailsFK;

                        db.yearlyCustomerTargets.Add(targetItem);
                    }
                }

                db.SaveChanges();
            }

        }
        public static List<string> GetOuList()
        {
            using (DBModels db = new DBModels())

            {
                return (from u in db.keyValueData
                        where u.keyType == "OPERATIONALUNIT"
                        select u.keyValue).ToList();
            }
        }


        
    }
}
