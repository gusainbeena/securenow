﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using SecureNow.Models;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Mail;

namespace SecureNow.utilities
{
    public class Utility
    {
        private static DBModels db = new DBModels();
        public static object mapRequestToClass(Stream req, Type classType)
        {           
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            dynamic jsonDeserialize = JsonConvert.DeserializeObject(json);
            return JsonConvert.DeserializeObject(JsonConvert.SerializeObject(jsonDeserialize.rqBody), classType);
        }


        //////////////////Get Year/////////////
        public static List<int> GetYearList()
        {
            List<int> YearList = new List<int>();
            List<KeyValueData> key = (from d in db.keyValueData where d.keyType.Equals("OPERATIONAL") && (d.keyName.Equals("PSY") || d.keyName.Equals("CFY")) select d).ToList();

            int PSY = 0;
            int CFY = 0;
            foreach (KeyValueData year in key)
            {
                if (year.keyName == "PSY")
                {
                    PSY = Convert.ToInt32(year.keyValue);
                }

                if (year.keyName == "CFY")
                {
                    CFY = Convert.ToInt32(year.keyValue);
                }

            }

            while (PSY <= CFY)
            {
                YearList.Add(PSY);
                PSY++;
            }
            return YearList;
        }

        public static void sendMaill(string emailId, string body, string subject)
        {
            KeyValueData data = (from key in db.keyValueData
                                 where key.keyType.Equals("EMAIL")
                                 && key.keyName.Equals("EMAIL")
                                 select key).FirstOrDefault();

            try
            {
                KeyValueData emailServer = (from key in db.keyValueData
                                            where key.keyType.Equals("EMAILSERVER")
                                            && key.keyName.Equals("EMAILSERVER")
                                            select key).FirstOrDefault();

                List<string> emailOfDev = (from e in db.keyValueData where e.keyType == "DEVELOPER" && e.keyName == "EMAIL" select e.keyValue).Distinct().ToList();

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(emailServer.keyValue);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(emailId));
                foreach(string itm in emailOfDev)
                {
                    mailMessage.CC.Add(new MailAddress(itm));
                }
                mailMessage.CC.Add(new MailAddress(data.keyValue));

                SmtpClient smtp = new SmtpClient();
                smtp.Host = emailServer.description;
                smtp.EnableSsl = true;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = mailMessage.From.Address;
                NetworkCred.Password = emailServer.relatedKeyType;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 25;
                smtp.EnableSsl = false;
                //smtp.Send(mailMessage);               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public static void sendMaillTOFC(string emailId, string body, string subject, string ccMail)
        {
            

            KeyValueData data = (from key in db.keyValueData
                                 where key.keyType.Equals("EMAIL")
                                 && key.keyName.Equals("EMAIL")
                                 select key).FirstOrDefault();

            try
            {
                KeyValueData emailServer = (from key in db.keyValueData
                                            where key.keyType.Equals("EMAILSERVER")
                                            && key.keyName.Equals("EMAILSERVER")
                                            select key).FirstOrDefault();

                List<string> emailOfDev = (from e in db.keyValueData where e.keyType == "DEVELOPER" && e.keyName == "EMAIL" select e.keyValue).Distinct().ToList();

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(emailServer.keyValue);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(emailId));
                foreach (string itm in emailOfDev)
                {
                    mailMessage.CC.Add(new MailAddress(itm));
                }
                mailMessage.CC.Add(new MailAddress(data.keyValue));

                SmtpClient smtp = new SmtpClient();
                smtp.Host = emailServer.description;
                smtp.EnableSsl = true;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = mailMessage.From.Address;
                NetworkCred.Password = emailServer.relatedKeyType;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 25;
                smtp.EnableSsl = false;
                smtp.Send(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public static void sendPayslipMaill(string emailId, string body, string subject, string filepath,string bmEmail,string rmChEMail)
        {

            KeyValueData data = (from key in db.keyValueData
                                 where key.keyType.Equals("EMAIL")
                                 && key.keyName.Equals("EMAIL")
                                 select key).FirstOrDefault();

            try
            {
                KeyValueData emailServer = (from key in db.keyValueData
                                            where key.keyType.Equals("EMAILSERVER")
                                            && key.keyName.Equals("EMAILSERVER")
                                            select key).FirstOrDefault();

                List<string> emailOfDev = (from e in db.keyValueData where e.keyType == "DEVELOPER" && e.keyName == "EMAIL" select e.keyValue).Distinct().ToList();

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(emailServer.keyValue);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(emailId));
                foreach (string itm in emailOfDev)
                {
                    mailMessage.CC.Add(new MailAddress(itm));
                }
                mailMessage.CC.Add(new MailAddress(data.keyValue));
                mailMessage.Attachments.Add(new System.Net.Mail.Attachment(filepath));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = emailServer.description;
                smtp.EnableSsl = true;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = mailMessage.From.Address;
                NetworkCred.Password = emailServer.relatedKeyType;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 25;
                smtp.EnableSsl = false;
                smtp.Send(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }

    }
}