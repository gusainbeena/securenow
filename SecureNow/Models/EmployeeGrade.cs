﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("EmployeeGrade")]
    public class EmployeeGrade
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int employeeRoleFK { get; set; }
        [Required]
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int yearRecorded { get; set; }
        public String grade { get; set; }
        public Boolean currentlyAssigned { get; set; }

    }
}