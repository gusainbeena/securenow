﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("IncentivePartiallyApproved")]
    public class IncentivePartiallyApproved
    {
        [Key]
        public int id { get; set; }

        public int incentiveConfirmationFK { get; set; }
        public string fileName { get; set; }
        public long actionTaken { get; set; }
        public string comment { get; set; }
        public string action { get; set; }
    }
}