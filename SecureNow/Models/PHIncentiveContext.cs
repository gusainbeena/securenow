﻿//using Calculate.models;
using SecureNow.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
namespace SecureNow.Models
{
    public class DBModels : DbContext
    {
        public static DBModels dynamicDB;

        public DBModels()
        {

        } 
      /* public DBModels(string database) : base("Data Source =.; initial catalog = " + database + "; Database=" + database + ";Integrated Security=True")
        {

        }*/
        public DbSet<ActualApprovalRequest> actualApprovalRequest { get; set; }
        public DbSet<TeamSetup> teamSetup { get; set; }
        public DbSet<DSOMisc> dsoMisc { get; set; }
        public DbSet<BatchRunningProcess> batchRunningProcess { get; set; }
        public DbSet<UserActivity> userActivity { get; set; }
        public DbSet<IncentiveParameter> IncentiveParameter { get; set; }
        public DbSet<EmployeeActualsAndTargetsFortnight> employeeActualAndTargetFortnight { get; set; }
        public DbSet<EmployeeBaseDetails> employeeBaseDetails { get; set; }
        public DbSet<IncentivePartiallyApproved> incentivePartiallyApproved { get; set; }
        public DbSet<DataUploadBatchParameters> dataUploadBatchParameters { get; set; }
        public DbSet<DisbursmentData> disbursmentdata { get; set; }
        public DbSet<DataUploadBatchRequests> dataUploadBatchRequests { get; set; }
        public DbSet<EmployeeRole> employeeRole { get; set; }
        public DbSet<IncentivePolicy> incentivePolicy { get; set; }
        public DbSet<EmployeeActualsAndTargets> EmployeeActualAndTargets { get; set; }
        public DbSet<KeyValueData> keyValueData { get; set; }
        public DbSet<CustomerBaseDetails> customerBaseDetails { get; set; }
        public DbSet<YearlyCustomerTargets> yearlyCustomerTargets { get; set; }
        public DbSet<UserAuditData> userAuditData { get; set; }
        public DbSet<OverDueAmount> overDueActuals { get; set; }
        public DbSet<ReportRequest> reportRequest{get;set; }
        public DbSet<CustomerRelationshipDetails> customerRelationshipDetails { get; set; }
        public DbSet<IncentiveCalculationRequest> incentiveCalculationRequest { get; set; }
        public DbSet<IncentiveConfirmations> incentiveConfirmations { get; set; }
        public DbSet<IncentiveException> incentiveException { get; set; }
        public DbSet<IncentiveExceptionApproval> incentiveExceptionApproval { get; set; }
        public DbSet<IncentiveDisbursedAmount> incentiveDisbursedAmount { get; set; }
        public DbSet<EmployeeGrade> employeeGrade { get; set; }
        public DbSet<YTDActualData> YTDActualData { get; set; }
        public DbSet<EmployeeAttributeLink> employeeAttributeLink { get; set; }
        public DbSet<OverDueData> OverDueData { get; set; }
        public DbSet<DSOData> DSOData { get; set; }
        public DbSet<FortNightTarget> fortNightTarget { get; set; }
        public DbSet<AttributesLink> attributesLink { get; set; }
        public DbSet<KeyValueDataLink> keyValueDataLink { get; set; }
        public DbSet<TemplateData> templateData { get; set; }
        public DbSet<UserAccessLinking> userAccessLinkings { get; set; }
        public DbSet<UserExceptionPermissionFlow> UserExceptionPermissionFlow { get; set; }
        public DbSet<DashboardGraphs> dashboardGraphs { get; set; }
        public DbSet<ActualRequest>actualRequest { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<YearlyCustomerTargets>().Property(product => product.targetAmt).HasPrecision(38, 15);
            modelBuilder.Entity<YTDActualData>().Property(product => product.value).HasPrecision(38, 15);
            modelBuilder.Entity<DSOData>().Property(product => product.netAR).HasPrecision(38, 15);
            modelBuilder.Entity<OverDueData>().Property(product => product.totalOD).HasPrecision(38, 15);
            modelBuilder.Entity<OverDueData>().Property(product => product.totalOS).HasPrecision(38, 15);
            modelBuilder.Entity<FortNightTarget>().Property(product => product.target).HasPrecision(38, 15);
        }
    }
}