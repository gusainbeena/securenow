﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("DashboardGraphs")]
    public class DashboardGraphs
    {
        public int id { set; get; }
        public string chartName { set; get; }
        public string xAxis { set; get; }
        public string yAxis { set; get; }
        public string adpId { set; get; }
        public string chartType { set; get; }
        public long calculatingUnit { set; get; }
        public string xAxisSubKey { set; get; }
        public string yAxisSubKey { set; get; }
    }
}