﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("IncentiveConfirmations")]
    public class IncentiveConfirmations
    {
        [Key]
        public int id { get; set; }

        public int incentiveRequestId { get; set; }
        public string adpId { get; set; }
        public string channelCode { get; set; }
        public string role { get; set; }
        public int branchDetailsFK { get; set; }
        public string region { get; set; }
        public string status { get; set; }
        public long actionTakenDate { get; set; }
        public string comments { get; set; }
        public string pathToFile { get; set; }
        public string incentiveError { get; set; }
        public int mainIdPartial { get; set; }
        public string quarter { get; set; }
        public int yearRecorded { get; set; }

    }
}