﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("ActualRequest")]
    public class ActualRequest
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int year { set; get; }
        public int month { get; set; }        
        public decimal amount { get; set; }
        public string bg { set; get; }
        public string status { get; set; }
        public string requestRaisedByAdpId { get; set; }
        public long dateTime { get; set; }
        public string type { get; set; }
        public string requestRaisedForAdpId { get; set; }
        
    }
}