﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("EmployeeRole")]
    public class EmployeeRole
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string adpId { get; set; }
        public string role { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int yearRecorded { get; set; }
        public string reportsToAdpId { get; set; }
        public string customCalculationStringOP { get; set; }
        public string customCalculationStringActuals { get; set; }
        public string policyApply { get; set; }


      //  public string channelCode { get; set; }
       // public string subChannelCode { get; set; }
       // public string region { get; set; }
       // public string territory { get; set; }
       // public string subTerritory { get; set; }


        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3 { get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string RP6 { get; set; }
        public string RP7 { get; set; }
        
    }
}