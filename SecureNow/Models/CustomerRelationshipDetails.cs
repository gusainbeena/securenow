﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("CustomerRelationshipDetails")]
    public class CustomerRelationshipDetails
    {
        [Key]
        public int id { get; set; }

        public int customerBaseDetailsFK { get; set; }
        //public string subArea { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int employeeRoleFK { get; set; }
        public int yearRecorded { get; set; }
        //public string startMonthDescription { get; set; }
        //public string endMonthsDescription { get; set; }


    }
}