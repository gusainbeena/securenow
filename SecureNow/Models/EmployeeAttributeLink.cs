﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("EmployeeAttributeLink")]
    public class EmployeeAttributeLink
    {
        [Key]
        public int id { set; get; }

        public int channelFk { set; get; }
        public string keyName { set; get; }
        public string keyValue { set; get; }
        public string relatedKeyName1 { set; get; }
        public string relatedKeyValue1 { set; get; }
        public string relatedKeyName2 { set; get; }
        public string relatedKeyValue2 { set; get; }
        public int startMonth { set; get; }
        public int endMonth { set; get; }
    }
}