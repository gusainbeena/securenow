﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("DSOData")]
    public class DSOData
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string code { set; get; }
        public string customerName { set; get; }
        public string amCode { set; get; }
        public string soCode { set; get; }
        public decimal netAR { set; get; }
        public int month { set; get; }
        public int year { set; get; }
        public string kamCode { set; get; }


    }
}