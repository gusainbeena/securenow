﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("EmployeeBaseDetails")]
    public class EmployeeBaseDetails
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string adpId { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }
        [Required]
        public string status { get; set; }
        public string fullName { get; set; }
        [Required]
        public string firstName { get; set; }
        public string middleName { get; set; }
        [Required]
        public string lastName { get; set; }        
        public string phoneNumber { get; set; }
        [Required]
        public long doj { get; set; }
        public long dol { get; set; }
        public long lastPasswordDate { get; set; }
        public string dojStr { get; set; }
        public string dolStr { get; set; }
        public bool toBeConsidered { get; set; }
        public int yearRecorded { get; set; }
        [Display(Name = "Remember me?")]
        public bool rememberMe { get; set; }
        public string operationalUnit { get; set; }
    }
}