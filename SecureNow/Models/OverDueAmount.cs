﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("OverDueActual")]
    public class OverDueAmount
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public string role { get; set; }
        public int month { get; set; }
        public decimal totalOD { get; set; }
        public decimal totalOS { get; set; }
        public int yearRecorded { get; set; }
        public string channelCode { get; set; }
        public string subChannel { get; set; }
        public Decimal netAR { set; get; }
        public Decimal netSale { set; get; }

    }
}