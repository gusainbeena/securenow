﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("UserActivity")]
    public class UserActivity
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string date { get; set; }
        public string service { get; set; }
        public string email { get; set; }
        public string time { get; set; }
        public string relatedKeyValue { get; set; }
        public string operationalUnit { get; set; }
    }
}