﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("ActualApprovalRequest")]
    public class ActualApprovalRequest
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int actualRequestIdFK { get; set; }
        public string approverAdpId { set; get; }       
        public string status { set; get; }
        public long dateTime { set; get; }
    }
}