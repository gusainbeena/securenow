﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("IncentiveException")]
    public class IncentiveException
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public string status { get; set; }
        public string channelCode { get; set; }
        public string role { get; set; }
        public string areaCode { get; set; }
        public int branchDetailsFK { get; set; }
        public string region { get; set; }
        public long initiatedDate { get; set; }
        public long approvedDate { get; set; }
        public string fullName { get; set; }
        public string raisedOnAdpId { get; set; }
        public string raisedOnRole { get; set; }
        public string comments { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int incentiveRequestFK { get; set; }
        public int yearRecorded { get; set; }

    }
}