﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureNow.Models
{

    [Table("IncentiveParameter")]
    public class IncentiveParameter
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string role { get; set; }
        public string channel { get; set; }
        public string subChannel { get; set; }
        public string keyName { get; set; }
        public string keyType { get; set; }
        public string keyValue { get; set; }
        public string relatedKeyType { get; set; }
        public string relatedKeyValue { get; set; }
        public string actualType { get; set; }
        public string onTargetPayoutPercentage { get; set; }
        public string diffPolicyParam { get; set; }
        public string quarter { get; set; }
        public int year { get; set; }
    }

}
