﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("IncentiveExceptionApproval")]
    public class IncentiveExceptionApproval
    {
        [Key]
        public int id { get; set; }
        public string assignAdpId { get; set; }
        public string actionTaken { get; set; }
        public long actionDate { get; set; }
        public bool assignTo { get; set; }
        public int approvalSequence { get; set; }
        public int incentiveExceptionFK { get; set; }
        public string comment { get; set; }
    }
}