﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("UserAuditTrail")]
    public class UserAuditData
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string adpId { get; set; }
        public string email { get; set; }
        public string operationName { get; set; }
        public long operationTime { get; set; }
        public string oldData { get; set; }
    }
}