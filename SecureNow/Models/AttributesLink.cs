﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("AttributeLink")]
    public class AttributesLink
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int startMonth { get; set; }
        public int endMonth { set; get; }
        public int year { set; get; }
        public int rootId { set; get; }
        public int parentId { set; get; }
        public int childId { set; get; }
        public string childlabel { set; get; }
        public string attributeSequence { set; get; }
    }
}