﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("YearlyCustomerTargets")]
    public class YearlyCustomerTargets
    {
        [Key]
        public int id { get; set; }
        public int customerBaseDetailsFK { get; set; }
        public int customerEmployeeLinkFK { get; set; }
        public int month { get; set; }
        public string buCode { get; set; }
        public decimal targetAmt { get; set; }
        public int yearRecorded { get; set; }
        public string dataType { set; get; }
        public string frequencyType { set; get; }
    }
}