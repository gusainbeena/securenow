﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("IncentiveDisbursedAmount")]
    public class IncentiveDisbursedAmount
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public int incentiveRequestFK { get; set; }
        public decimal disbursedAmt { get; set; }
        public int yearRecorded { get; set; }

    }
}