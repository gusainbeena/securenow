﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("UserAccessLinking")]
    public class UserAccessLinking
    {
        [Key]
        public int id { set; get; }
        public int profileId { set; get; }
        public int permissionId { set; get; }
        public int functionId { set; get; }
        public string OUs { set; get; }
    }
}