﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    public class ForgotPassword
    {
        [Display(Name = "Email")]
        public string email { get; set; }
    }
}