﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("KeyValueDataLink")]
    public class KeyValueDataLink
    {
        [Key]
        public int id { set; get; }
        public int keyValueFK { get; set; }
        public string keyType { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public string labelOnScreen { get; set; }
        public string description { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { get; set; }
        public string requiredSalesHorizontals { set; get; }
        public Boolean optional { set; get; }
        public string uploadFrequency { set; get; }
        public int attrSequence { set; get; }
    }
}