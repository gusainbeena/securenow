﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("ReportRequest")]
    public class ReportRequest
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string adpId { get; set; }
        public string status { get; set; }
        public string role { get; set; }
        public long requestedDate { get; set; }
        public string requestedFilename { get; set; }
        public string generatedFilename { get; set; }
        public string requestParameters { get; set; }

    }
}