﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("incentivePolicy")]
    public class IncentivePolicy
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string channel { get; set; }
        public string policyName { get; set; }
        public string type { get; set; }
        public int year { get; set; }
    }
}