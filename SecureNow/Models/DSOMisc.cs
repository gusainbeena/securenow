﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("DSOAchievement")]
    public class DSOMisc
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public decimal achievement { get; set; }
        public decimal days { get; set; }
    }
}