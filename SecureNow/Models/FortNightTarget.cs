﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("FortnightTarget")]
    public class FortNightTarget
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int yearlyTargetFK { get; set; }
        public decimal target { get; set; }
        public string type { get; set; }
    }
}