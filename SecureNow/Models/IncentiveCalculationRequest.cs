﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("IncentiveCalculationRequests")]
    public class IncentiveCalculationRequest
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public int year { get; set; }
        public string quarter { get; set; }
        public string incentiveName { get; set; }
        public string status { get; set; }
        public string incentiveSheetPath { get; set; }
        public bool outOfTarn { get; set; }
        public string comments { get; set; }
        public string disbursementSheet { get; set; }
    }
}