﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("CustomerBaseDetails")]
    public class CustomerBaseDetails
    {
        [Key]
        public int id { get; set; }
        public string customerName { get; set; }
        public string soldToCode { get; set; }
        public string areaOfOperation { get; set; }
        public string groupName { get; set; }
        public string districtOrTown { get; set; }
        public string stateCode { get; set; }
        public string status { get; set; }
        public long doa { get; set; }
        public long doc { get; set; }
        public string doaStr { get; set; }
        public string docStr { get; set; }
        //public string operationalUnit { get; set; }
        public int yearRecorded { get; set; }
    }
}