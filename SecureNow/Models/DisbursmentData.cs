﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("DisbursmentData")]
    public class DisbursmentData
    {
        [Key]
        public int id { get; set; }
        public string adpId { get; set; }
        public string role { get; set; }
        public string channelCode { get; set; }
        public string branchCode { get; set; }
        public string region { get; set; }
        public decimal incentiveAmount { get; set; }
        public int yearRecorded { get; set; }
        public string quarter { get; set; }
        public string finalStatus { get; set; }
        public decimal finalAmount { get; set; }
        public string reasonForDaviation { set; get; }

    }
}