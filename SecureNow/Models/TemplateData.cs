﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("TemplateDataTable")]
    public class TemplateData
    {
        [Key]
        public int id { get; set; }
        [Required]
	    public int keyValueFK{ get; set; }
        public string keyType { get; set; }
        public string keyName { get; set; }
        public string keyValue { get; set; }
        public string keyValueModel { get; set; }
        public string labelOnScreen { get; set; }
        public string description { get; set; }
        public string relatedKeyType { get; set; }
        public string relatedKeyValue { get; set; }
        public int preferenceValue { get; set; }
        public int preferenceSerial { get; set; }
        public int year { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public string relatedDataJson { get; set; }

    }
}