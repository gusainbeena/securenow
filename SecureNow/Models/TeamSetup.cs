﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("TeamSetup")]
    public class TeamSetup
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int keyValueDataFkId { get; set; }
        public int attributeLinkFkId { set; get; }
        public int salesComponentFkId { set; get; }
        public int year { set; get; }
    }
}