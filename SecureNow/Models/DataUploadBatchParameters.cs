﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("DataUploadBatchParameters")]
    public class DataUploadBatchParameters
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string uploadType { get; set; }
        public string fileUploadFolder { get; set; }
        public string errorFileFolder { get; set; }
        public string ProgramFilePath { get; set; }
        public string status { get; set; }
    }
}
