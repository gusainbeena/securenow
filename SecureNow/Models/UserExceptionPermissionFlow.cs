﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("UserExceptionPermissionFlow")]
    public class UserExceptionPermissionFlow
    {
        [Key]
        public int id { set; get; }
        public int profileId { set; get; }
        public int empId { set; get; }
        public int roleId { set; get; }
        public string OUs { set; get; }
        public int permissionId { set; get; }
        public int functionId { set; get; }
        
    }
}