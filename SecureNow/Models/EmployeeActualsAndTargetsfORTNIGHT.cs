﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("EmployeeActualsAndTargetsForFortnight")]
    public class EmployeeActualsAndTargetsFortnight
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int employeeRoleFK { get; set; }
        public decimal targetAmt { get; set; }
        public decimal grossActualAmt { get; set; }
        public decimal netActualAmt { get; set; }
        public string type { get; set; }
    }
}