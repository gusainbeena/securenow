﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SecureNow.Models
{
    [Table("DataUploadBatchRequests")]
  
    public class DataUploadBatchRequests 
    {
        [Key]
        public int id { get; set; }
       
        public string requestType { get; set; }
        public string requestorAdpId { get; set; }
        public string status { get; set; }
        public long timeOfRequest { get; set; }
        public int startMonth { get; set; }
        public int endMonth { get; set; }
        public int year { get; set; }
        public string quarter { get; set; }
        public bool fullUpload { get; set; }
        public int recordsProcessed { get; set; }
        public int totalRecords { get; set; }
        [Required]
        public string dataSourceFileName { get; set; }
        public string errorLogFileName { get; set; }
        public string downloadReportType { get; set; }
        public string additionalParameters { get; set; }

        public string operationalUnit { get; set; }
        public string batchName { get; set; }
        public string scheduleTime { get; set; }
    }
}