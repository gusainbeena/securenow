﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SecureNow.Models
{
    [Table("OverDueData")]
    public class OverDueData
    {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
        public string customerName { get; set; }
        public decimal totalOS { get; set; }
        public decimal totalOD { get; set; }
        public int yearRecorded { get; set; }
        public string channelName { get; set; }
        public string AMCode { get; set; }
        public string SOCode { get; set; }
        public string regionAndASM { get; set; }
        public string quarter { get; set; }
        public string region { get; set; }
    }
}