﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("EmployeeActualsAndTargets")]
    public class EmployeeActualsAndTargets
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int monthCode { get; set; }
        public string RP1 { get; set; }
        public string RP2 { get; set; }
        public string RP3{ get; set; }
        public string RP4 { get; set; }
        public string RP5 { get; set; }
        public string bgCode { get; set; }
        public string buCode { get; set; }   
        public decimal targetAmt { get; set; }
        public decimal grossActualAmt { get; set; }
        public decimal netActualAmt { get; set; }
        public int yearRecorded { get; set; }
        public string drivenFrom { get; set; }
        public string operationalUnit { get; set; }
    }
}