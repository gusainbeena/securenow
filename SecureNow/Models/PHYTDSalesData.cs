﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecureNow.Models
{
    [Table("YTDActualData")]
    public class YTDActualData
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int month { get; set; }
        public string bg { get; set; }
        public decimal value { get; set; }
        public int year { get; set; }
        public string code { get; set; }
    }
}