﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SecureNow.Models
{
    [Table("batchRunningProcess")]
    public class BatchRunningProcess
    {
        [Key]
        public int id { get; set; }
        [Required]
        
        public int syncId { set; get; }
        public int sequence { set; get; }
        public string date { set; get; }
        public string batchName { set; get; }
        public string description { set; get; }
        public string startTime { set; get; }
        public string endTime { set; get; }
        public string executionTime { set; get; }
        public string status { set; get; }
        public string operationalUnit { set; get; }
    }
}