﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CaptchaMvc;

namespace $rootnamespace$.Models
{
	public class CaptchaModel
	{
		[ValidateCaptcha]
		[UIHint("Captcha")]
		public string Captcha { get; set; }
	}
}